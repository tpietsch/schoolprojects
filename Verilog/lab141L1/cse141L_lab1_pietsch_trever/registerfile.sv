`timescale 1ps / 1ps
/*
 * CSE141L Lab1: Tools of the Trade
 * University of California, San Diego
 * 
 * Written by Donghwan Jeon, 4/1/2007
 * Modified by Vikram Bhatt, 30/3/2011
 * Modified by Nikolaos Strikos, 4/8/2012
 */

module registerfile#(parameter rs = 256,parameter r = 32)
(
 input clk,
 input wen_i,
 input[rs:0] wa_i,
 input[rs:0] wd_i,
 input [rs:0] ra0_i,ra1_i,
 output [rs:0] rd0_o,rd1_o
);

/*
   logic [:0] a_r, b_r;
   logic [64:0]   sum_r;
   logic         is_odd_r;
   logic [64:0]  sum_next;

   assign sum_next = a_r + b_r;
   assign sum_o = sum_r;
   assign is_odd_o= is_odd_r;
*/
//reg size rf num reg
   logic [rs:0] rf[r:0];
	
	 always_ff @(posedge clk) 
     begin
	  
	  //write
	  if(wen_i)
	  begin
	  rf[wa_i] <= wd_i;
	  
	  end
	  
     end
	  
	  always_ff @(posedge clk) 
     begin
		rd0_o = rf[ra0_i];
		rd1_o = rf[ra1_i];
	  end
	 
endmodule