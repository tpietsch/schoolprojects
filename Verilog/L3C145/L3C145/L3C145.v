module L3C145( // where yyy=your CID. For example, L3C079 if your CID=079 
input [9:0] sw, // ten up-down switches, SW9 - SW0
input [3:0] key, // four pushbutton switches, KEY3 - KEY0 
input clock, // 24MHz clock source on Altera DE1 board
output [9:0] ledr, // ten Red LEDs, LEDR9 - LEDR0
output [7:0] ledg, // eight Green LEDs, LEDG8 - LEDG0
output reg [6:0] hex3, hex2, hex1, hex0 // four 7-segment, HEX3 - HEX0
); 
reg [3:0] dispense,green,bill,credit,coin,start,disp;
reg [4:0] totalDisp,pass;
reg [32:0] count,half;
reg [32:0] total,prev;
reg [32:0] hert;
reg [4:0] state,prevs;

assign ledg[0] = green;
assign ledg[1] = green;
assign ledg[2] = green;
assign ledg[3] = green;
assign ledg[4] = green;
assign ledg[5] = green;
assign ledg[6] = green;
assign ledg[7] = green;

task display;
input [32:0] hx3, hx2, hx1, hx0;
output reg [6:0] hex3, hex2, hex1, hex0;
case(hx0)
  0:begin
	hex0[0]=0;
	hex0[1]=0;
	hex0[2]=0;
	hex0[3]=0;
	hex0[4]=0;
	hex0[5]=0;
	hex0[6]=1;
	end
   1:begin
	hex0[0]=1;
	hex0[1]=0;
	hex0[2]=0;
	hex0[3]=1;
	hex0[4]=1;
	hex0[5]=1;
	hex0[6]=1;
	end
  2:begin
	hex0[0]=0;
	hex0[1]=0;
	hex0[2]=1;
	hex0[3]=0;
	hex0[4]=0;
	hex0[5]=1;
	hex0[6]=0;
	end
   3:begin
	hex0[0]=0;
	hex0[1]=0;
	hex0[2]=0;
	hex0[3]=0;
	hex0[4]=1;
	hex0[5]=1;
	hex0[6]=0;
	end
	 4:begin
	hex0[0]=1;
	hex0[1]=0;
	hex0[2]=0;
	hex0[3]=1;
	hex0[4]=1;
	hex0[5]=0;
	hex0[6]=0;
	end
	 5:begin
	hex0[0]=0;
	hex0[1]=1;
	hex0[2]=0;
	hex0[3]=0;
	hex0[4]=1;
	hex0[5]=0;
	hex0[6]=0;
	end
	 6:begin
	hex0[0]=0;
	hex0[1]=1;
	hex0[2]=0;
	hex0[3]=0;
	hex0[4]=0;
	hex0[5]=0;
	hex0[6]=0;
	end
	 7:begin
	hex0[0]=0;
	hex0[1]=0;
	hex0[2]=0;
	hex0[3]=1;
	hex0[4]=1;
	hex0[5]=1;
	hex0[6]=1;
	end
	 8:begin
	hex0[0]=0;
	hex0[1]=0;
	hex0[2]=0;
	hex0[3]=0;
	hex0[4]=0;
	hex0[5]=0;
	hex0[6]=0;
	end
	 9:begin
	hex0[0]=0;
	hex0[1]=0;
	hex0[2]=0;
	hex0[3]=1;
	hex0[4]=1;
	hex0[5]=0;
	hex0[6]=0;
	end
	 10:begin
	hex0[0]=0;
	hex0[1]=0;
	hex0[2]=0;
	hex0[3]=1;
	hex0[4]=0;
	hex0[5]=0;
	hex0[6]=0;
	end
	 11:begin
	hex0[0]=1;
	hex0[1]=1;
	hex0[2]=0;
	hex0[3]=0;
	hex0[4]=0;
	hex0[5]=0;
	hex0[6]=0;
	end
	 12:begin
	hex0[0]=0;
	hex0[1]=1;
	hex0[2]=1;
	hex0[3]=0;
	hex0[4]=0;
	hex0[5]=0;
	hex0[6]=1;
	end
	 13:begin
	hex0[0]=1;
	hex0[1]=0;
	hex0[2]=0;
	hex0[3]=0;
	hex0[4]=0;
	hex0[5]=1;
	hex0[6]=0;
	end
	
	 14:begin
	hex0[0]=0;
	hex0[1]=1;
	hex0[2]=1;
	hex0[3]=0;
	hex0[4]=0;
	hex0[5]=0;
	hex0[6]=0;
	end
	 15:begin
	hex0[0]=0;
	hex0[1]=1;
	hex0[2]=1;
	hex0[3]=1;
	hex0[4]=0;
	hex0[5]=0;
	hex0[6]=0;
	end
	 16:begin
	hex0[0]=1;
	hex0[1]=1;
	hex0[2]=1;
	hex0[3]=1;
	hex0[4]=0;
	hex0[5]=1;
	hex0[6]=0;
	end
	
	
default:begin
    hex0[0]=1;
	hex0[1]=1;
	hex0[2]=1;
	hex0[3]=1;
	hex0[4]=1;
	hex0[5]=1;
	hex0[6]=1;
	end
endcase
case(hx1)
  0:begin
	hex1[0]=0;
	hex1[1]=0;
	hex1[2]=0;
	hex1[3]=0;
	hex1[4]=0;
	hex1[5]=0;
	hex1[6]=1;
	end
   1:begin
	hex1[0]=1;
	hex1[1]=0;
	hex1[2]=0;
	hex1[3]=1;
	hex1[4]=1;
	hex1[5]=1;
	hex1[6]=1;
	end
  2:begin
	hex1[0]=0;
	hex1[1]=0;
	hex1[2]=1;
	hex1[3]=0;
	hex1[4]=0;
	hex1[5]=1;
	hex1[6]=0;
	end
   3:begin
	hex1[0]=0;
	hex1[1]=0;
	hex1[2]=0;
	hex1[3]=0;
	hex1[4]=1;
	hex1[5]=1;
	hex1[6]=0;
	end
	 4:begin
	hex1[0]=1;
	hex1[1]=0;
	hex1[2]=0;
	hex1[3]=1;
	hex1[4]=1;
	hex1[5]=0;
	hex1[6]=0;
	end
	 5:begin
	hex1[0]=0;
	hex1[1]=1;
	hex1[2]=0;
	hex1[3]=0;
	hex1[4]=1;
	hex1[5]=0;
	hex1[6]=0;
	end
	 6:begin
	hex1[0]=0;
	hex1[1]=1;
	hex1[2]=0;
	hex1[3]=0;
	hex1[4]=0;
	hex1[5]=0;
	hex1[6]=0;
	end
	 7:begin
	hex1[0]=0;
	hex1[1]=0;
	hex1[2]=0;
	hex1[3]=1;
	hex1[4]=1;
	hex1[5]=1;
	hex1[6]=1;
	end
	 8:begin
	hex1[0]=0;
	hex1[1]=0;
	hex1[2]=0;
	hex1[3]=0;
	hex1[4]=0;
	hex1[5]=0;
	hex1[6]=0;
	end
	 9:begin
	hex1[0]=0;
	hex1[1]=0;
	hex1[2]=0;
	hex1[3]=1;
	hex1[4]=1;
	hex1[5]=0;
	hex1[6]=0;
	end
	 10:begin
	hex1[0]=0;
	hex1[1]=0;
	hex1[2]=0;
	hex1[3]=1;
	hex1[4]=0;
	hex1[5]=0;
	hex1[6]=0;
	end
	 11:begin
	hex1[0]=1;
	hex1[1]=1;
	hex1[2]=0;
	hex1[3]=0;
	hex1[4]=0;
	hex1[5]=0;
	hex1[6]=0;
	end
	 12:begin
	hex1[0]=0;
	hex1[1]=1;
	hex1[2]=1;
	hex1[3]=0;
	hex1[4]=0;
	hex1[5]=0;
	hex1[6]=1;
	end
	 13:begin
	hex1[0]=1;
	hex1[1]=0;
	hex1[2]=0;
	hex1[3]=0;
	hex1[4]=0;
	hex1[5]=1;
	hex1[6]=0;
	end
	
	 14:begin
	hex1[0]=0;
	hex1[1]=1;
	hex1[2]=1;
	hex1[3]=0;
	hex1[4]=0;
	hex1[5]=0;
	hex1[6]=0;
	end
	 15:begin
	hex1[0]=0;
	hex1[1]=1;
	hex1[2]=1;
	hex1[3]=1;
	hex1[4]=0;
	hex1[5]=0;
	hex1[6]=0;
	end
	 16:begin
	hex1[0]=1;
	hex1[1]=1;
	hex1[2]=1;
	hex1[3]=1;
	hex1[4]=0;
	hex1[5]=1;
	hex1[6]=0;
	end
	
	
default:begin
    hex1[0]=1;
	hex1[1]=1;
	hex1[2]=1;
	hex1[3]=1;
	hex1[4]=1;
	hex1[5]=1;
	hex1[6]=1;
	end
endcase
case(hx2)
  0:begin
	hex2[0]=0;
	hex2[1]=0;
	hex2[2]=0;
	hex2[3]=0;
	hex2[4]=0;
	hex2[5]=0;
	hex2[6]=1;
	end
   1:begin
	hex2[0]=1;
	hex2[1]=0;
	hex2[2]=0;
	hex2[3]=1;
	hex2[4]=1;
	hex2[5]=1;
	hex2[6]=1;
	end
  2:begin
	hex2[0]=0;
	hex2[1]=0;
	hex2[2]=1;
	hex2[3]=0;
	hex2[4]=0;
	hex2[5]=1;
	hex2[6]=0;
	end
   3:begin
	hex2[0]=0;
	hex2[1]=0;
	hex2[2]=0;
	hex2[3]=0;
	hex2[4]=1;
	hex2[5]=1;
	hex2[6]=0;
	end
	 4:begin
	hex2[0]=1;
	hex2[1]=0;
	hex2[2]=0;
	hex2[3]=1;
	hex2[4]=1;
	hex2[5]=0;
	hex2[6]=0;
	end
	 5:begin
	hex2[0]=0;
	hex2[1]=1;
	hex2[2]=0;
	hex2[3]=0;
	hex2[4]=1;
	hex2[5]=0;
	hex2[6]=0;
	end
	 6:begin
	hex2[0]=0;
	hex2[1]=1;
	hex2[2]=0;
	hex2[3]=0;
	hex2[4]=0;
	hex2[5]=0;
	hex2[6]=0;
	end
	 7:begin
	hex2[0]=0;
	hex2[1]=0;
	hex2[2]=0;
	hex2[3]=1;
	hex2[4]=1;
	hex2[5]=1;
	hex2[6]=1;
	end
	 8:begin
	hex2[0]=0;
	hex2[1]=0;
	hex2[2]=0;
	hex2[3]=0;
	hex2[4]=0;
	hex2[5]=0;
	hex2[6]=0;
	end
	 9:begin
	hex2[0]=0;
	hex2[1]=0;
	hex2[2]=0;
	hex2[3]=1;
	hex2[4]=1;
	hex2[5]=0;
	hex2[6]=0;
	end
	 10:begin
	hex2[0]=0;
	hex2[1]=0;
	hex2[2]=0;
	hex2[3]=1;
	hex2[4]=0;
	hex2[5]=0;
	hex2[6]=0;
	end
	 11:begin
	hex2[0]=1;
	hex2[1]=1;
	hex2[2]=0;
	hex2[3]=0;
	hex2[4]=0;
	hex2[5]=0;
	hex2[6]=0;
	end
	 12:begin
	hex2[0]=0;
	hex2[1]=1;
	hex2[2]=1;
	hex2[3]=0;
	hex2[4]=0;
	hex2[5]=0;
	hex2[6]=1;
	end
	 13:begin
	hex2[0]=1;
	hex2[1]=0;
	hex2[2]=0;
	hex2[3]=0;
	hex2[4]=0;
	hex2[5]=1;
	hex2[6]=0;
	end
	
	 14:begin
	hex2[0]=0;
	hex2[1]=1;
	hex2[2]=1;
	hex2[3]=0;
	hex2[4]=0;
	hex2[5]=0;
	hex2[6]=0;
	end
	 15:begin
	hex2[0]=0;
	hex2[1]=1;
	hex2[2]=1;
	hex2[3]=1;
	hex2[4]=0;
	hex2[5]=0;
	hex2[6]=0;
	end
	 16:begin
	hex2[0]=1;
	hex2[1]=1;
	hex2[2]=1;
	hex2[3]=1;
	hex2[4]=0;
	hex2[5]=1;
	hex2[6]=0;
	end
	
	
default:begin
    hex2[0]=1;
	hex2[1]=1;
	hex2[2]=1;
	hex2[3]=1;
	hex2[4]=1;
	hex2[5]=1;
	hex2[6]=1;
	end
endcase
case(hx3)
  0:begin
	hex3[0]=0;
	hex3[1]=0;
	hex3[2]=0;
	hex3[3]=0;
	hex3[4]=0;
	hex3[5]=0;
	hex3[6]=1;
	end
   1:begin
	hex3[0]=1;
	hex3[1]=0;
	hex3[2]=0;
	hex3[3]=1;
	hex3[4]=1;
	hex3[5]=1;
	hex3[6]=1;
	end
  2:begin
	hex3[0]=0;
	hex3[1]=0;
	hex3[2]=1;
	hex3[3]=0;
	hex3[4]=0;
	hex3[5]=1;
	hex3[6]=0;
	end
   3:begin
	hex3[0]=0;
	hex3[1]=0;
	hex3[2]=0;
	hex3[3]=0;
	hex3[4]=1;
	hex3[5]=1;
	hex3[6]=0;
	end
	 4:begin
	hex3[0]=1;
	hex3[1]=0;
	hex3[2]=0;
	hex3[3]=1;
	hex3[4]=1;
	hex3[5]=0;
	hex3[6]=0;
	end
	 5:begin
	hex3[0]=0;
	hex3[1]=1;
	hex3[2]=0;
	hex3[3]=0;
	hex3[4]=1;
	hex3[5]=0;
	hex3[6]=0;
	end
	 6:begin
	hex3[0]=0;
	hex3[1]=1;
	hex3[2]=0;
	hex3[3]=0;
	hex3[4]=0;
	hex3[5]=0;
	hex3[6]=0;
	end
	 7:begin
	hex3[0]=0;
	hex3[1]=0;
	hex3[2]=0;
	hex3[3]=1;
	hex3[4]=1;
	hex3[5]=1;
	hex3[6]=1;
	end
	 8:begin
	hex3[0]=0;
	hex3[1]=0;
	hex3[2]=0;
	hex3[3]=0;
	hex3[4]=0;
	hex3[5]=0;
	hex3[6]=0;
	end
	 9:begin
	hex3[0]=0;
	hex3[1]=0;
	hex3[2]=0;
	hex3[3]=1;
	hex3[4]=1;
	hex3[5]=0;
	hex3[6]=0;
	end
	 10:begin
	hex3[0]=0;
	hex3[1]=0;
	hex3[2]=0;
	hex3[3]=1;
	hex3[4]=0;
	hex3[5]=0;
	hex3[6]=0;
	end
	 11:begin
	hex3[0]=1;
	hex3[1]=1;
	hex3[2]=0;
	hex3[3]=0;
	hex3[4]=0;
	hex3[5]=0;
	hex3[6]=0;
	end
	 12:begin
	hex3[0]=0;
	hex3[1]=1;
	hex3[2]=1;
	hex3[3]=0;
	hex3[4]=0;
	hex3[5]=0;
	hex3[6]=1;
	end
	 13:begin
	hex3[0]=1;
	hex3[1]=0;
	hex3[2]=0;
	hex3[3]=0;
	hex3[4]=0;
	hex3[5]=1;
	hex3[6]=0;
	end
	
	 14:begin
	hex3[0]=0;
	hex3[1]=1;
	hex3[2]=1;
	hex3[3]=0;
	hex3[4]=0;
	hex3[5]=0;
	hex3[6]=0;
	end
	 15:begin
	hex3[0]=0;
	hex3[1]=1;
	hex3[2]=1;
	hex3[3]=1;
	hex3[4]=0;
	hex3[5]=0;
	hex3[6]=0;
	end
	 16:begin
	hex3[0]=1;
	hex3[1]=1;
	hex3[2]=1;
	hex3[3]=1;
	hex3[4]=0;
	hex3[5]=1;
	hex3[6]=0;
	end
	
	
default:begin
    hex3[0]=1;
	hex3[1]=1;
	hex3[2]=1;
	hex3[3]=1;
	hex3[4]=1;
	hex3[5]=1;
	hex3[6]=1;
	end
endcase
endtask




always@(negedge key[0])
begin
if((state == 1 || state == 2 || state == 3) && (!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8]))
begin
end
else if(!sw[9])
begin
case(state)
0:
begin
prev = 0;
//totalDisp = 0;
state = 11;
end


1:
begin
prev = 1;
if(total > 34)
begin
if(!coin)
begin
//totalDisp = totalDisp +1;
end
total = 0;
end
else
pass = 0;
if(sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
total = total + 5;
state = 1;
end
else if(!sw[0]&&sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
total = total + 10;
state = 1;
end
else if(!sw[0]&&!sw[1]&&sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
total = total + 25;
state = 1;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&sw[3]&&!sw[4]&&!sw[8])
begin
coin = 1;
total = total + 100;
state = 2;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&sw[4]&&!sw[8])
begin
coin = 1;
total = 35;
state = 3;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&sw[8])
begin
total = 0;
coin = 0;
state = 11;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
state = state;
else
state = 5;
end

2:
begin
prev = 2;
if(sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
coin = 0;
total = 5;
state = 1;
end
else if(!sw[0]&&sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
coin = 0;
total = 10;
state = 1;
end
else if(!sw[0]&&!sw[1]&&sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
coin = 0;
total = 25;
state = 1;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&sw[3]&&!sw[4]&&!sw[8])
begin
state = 5;
end
if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&sw[4]&&!sw[8])
begin
coin = 1;
state = 3;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&sw[8])
begin
coin = 0;
state = 11;
end
/*else if(sw[0] || sw[1] || sw[2] || sw[3] && sw[4] || sw[8])
state = 5;
else if(sw[0] || sw[1] || sw[2] || sw[3] || sw[4] && sw[8])
state = 5;
else if(sw[0] && sw[1] || sw[2] || sw[3] || sw[4] || sw[8])
state = 5;
else if(sw[0] || sw[1] && sw[2] || sw[3] || sw[4] || sw[8])
state = 5;
else if(sw[0] || sw[1] || sw[2] && sw[3] || sw[4] || sw[8])
state = 5;
*/
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
state = state;
end

else
begin
//state = 5;
end
end

3:

begin
prev = 3;
if(sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
coin = 0;
total = 5;
state = 1;
end
else if(!sw[0]&&sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
coin = 0;
total = 10;
state = 1;
end
else if(!sw[0]&&!sw[1]&&sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
coin = 0;
total = 25;
state = 1;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&sw[4]&&!sw[8])
begin
state = 5;
end
if(!sw[0]&&!sw[1]&&!sw[2]&&sw[3]&&!sw[4]&&!sw[8])
begin
total = 100;
coin = 1;
state = 2;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&sw[8])
begin
total = 0;
coin = 0;
state = 11;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
state = state;
/*else if(sw[0] || sw[1] || sw[2] || sw[3] && sw[4] || sw[8])
state = 5;
else if(sw[0] || sw[1] || sw[2] || sw[3] || sw[4] && sw[8])
state = 5;
else if(sw[0] && sw[1] || sw[2] || sw[3] || sw[4] || sw[8])
state = 5;
else if(sw[0] || sw[1] && sw[2] || sw[3] || sw[4] || sw[8])
state = 5;
else if(sw[0] || sw[1] || sw[2] && sw[3] || sw[4] || sw[8])
state = 5;
*/
else 
begin
//state = 5;
end
end

5:begin
prev = 5;
coin = 0;
total = 0;
state = 1;
end


11:
begin
prev = 11;
if(total > 34)
begin
total = 0;
end
if(sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
total = total + 5;
state = 1;
end
else if(!sw[0]&&sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
total = total + 10;
state = 1;
end
else if(!sw[0]&&!sw[1]&&sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
total = total + 25;
state = 1;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&sw[3]&&!sw[4]&&!sw[8])
begin
coin = 1;
total = total + 100;
state = 2;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&sw[4]&&!sw[8])
begin
coin = 1;
total = 35;
state = 3;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&sw[8])
begin
total = 0;
coin = 0;
state = 11;
end
else if(!sw[0]&&!sw[1]&&!sw[2]&&!sw[3]&&!sw[4]&&!sw[8])
begin
state = state;
end
else
state = 5;
end

default:
begin
total = 0;
coin = 0;
state = 11;
prev = 11;
totalDisp = 0;
end
endcase
end

end

always@(negedge key[0])
begin
if(total > 34)
begin
if(!coin)
begin
totalDisp = totalDisp +1;
end
end
end

always@(state)
begin
if(!sw[9])
begin
case(state)

0:
begin
disp = 0;
display(0,1,4,5,hex3,hex2,hex1,hex0);
end

//coin
1:
begin
if(total < 35)
begin
disp = 0;
display(total/10,total%10,0,0,hex3,hex2,hex1,hex0);
end
else
begin
disp = 1;
display(3,5,(total-35)/10,(total-35)%10,hex3,hex2,hex1,hex0);
end
end

2:
begin
disp = 1;
display(3,5,(total-35)/10,(total-35)%10,hex3,hex2,hex1,hex0);
end

3:
begin
disp = 1;
display(3,5,0,0,hex3,hex2,hex1,hex0);
end

5:
begin
//if((prev == 2 || prev == 3)) //&& (!(sw[3]&&sw[4])) && !(sw[0]||sw[1]||sw[2]))
//disp = 1;
//else
disp = 0;
display(14,16,16,99,hex3,hex2,hex1,hex0);
end


11:
begin
disp = 0;
display(0,0,0,0,hex3,hex2,hex1,hex0);
end
endcase
end
else
begin
display(88,88,88,totalDisp%16,hex3,hex2,hex1,hex0);
disp = 0;
end
end

always@(negedge clock)
begin
half = 12000000;
if(disp)
begin
count = count + 1;
if(hert < half)
begin
green = 1;
end

else
begin
green = 0;
end

hert = hert + 1;
if(hert == (2*half))
begin
hert = 0;
count = 0;
end
end

if(!disp)
begin
green = 0;
end
end


endmodule 