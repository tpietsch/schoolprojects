%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helpers
head([H|T],H,T).
sumList([],0).
sumList(L,Acc):- head(L,H,T),sumList(T,Acc2),cost(H,C),Acc is Acc2 + C.

checkList(L1,[]).
checkList(L1,L2):- head(L2,H2,T2),checkList(L1,T2),isin(H2,L1).

notCheckList(L1,[]).
notCheckList(L1,L2):- head(L2,H2,T2),notCheckList(L1,T2),not(isin(H2,L1)).

%isin(X,L) is true if X appears in L
isin(X,[X|_]).
isin(X,[_|T]) :- isin(X,T).

%hint: use append, reverse, bagof judiciously.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem 1: Rules

zip(L1,L2,L3) :- isin(X,L1),isin(X,L3),isin(Y,L2),isin(Y,L3).

assoc(L,X,Y) :- L = L | X = X | Y = Y.

remove_duplicates([],[]).
remove_duplicates([H|T],[H|TT]) :- not(isin(H,T)),remove_duplicates(T,TT).

intersection([ ], X, [ ]).
intersection(L1, L2, L3) :- head(L1,H1,T1),head(L3,H1,T2),isin(H1, L2), !, intersection(T1, L2, T2).
intersection(L1, L2, L3) :- head(L1,H,T),intersection(T, L2, L3). 

union([ ], L2, L2).
union([H|T],[H|TT], L3):- union([H|T], TT, L3).
union([H|R], L2, [H|TT]):- union(R, L2, TT). 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem 2: Facts

cost(carne_asada,3).
cost(lengua,2).
cost(birria,2).
cost(carnitas,2).
cost(adobado,2).
cost(al_pastor,2).
cost(guacamole,1).
cost(rice,1).
cost(beans,1).
cost(salsa,1).
cost(cheese,1).
cost(sour_cream,1).
cost(taco,1).
cost(tortilla,1).


ingredients(carnitas_taco, [taco,carnitas, salsa, guacamole]).
ingredients(birria_taco, [taco,birria, salsa, guacamole]).
ingredients(al_pastor_taco, [taco,al_pastor, salsa, guacamole, cheese]).
ingredients(guacamole_taco, [taco,guacamole, salsa,sour_cream]).
ingredients(al_pastor_burrito, [tortilla,al_pastor, salsa]).
ingredients(carne_asada_burrito, [tortilla,carne_asada, guacamole, rice, beans]).
ingredients(adobado_burrito, [tortilla,adobado, guacamole, rice, beans]).
ingredients(carnitas_sopa, [sopa,carnitas, guacamole, salsa,sour_cream]).
ingredients(lengua_sopa, [sopa,lengua,beans,sour_cream]).
ingredients(combo_plate, [al_pastor, carne_asada,rice, tortilla, beans, salsa, guacamole, cheese]).
ingredients(adobado_plate, [adobado, guacamole, rice, tortilla, beans, cheese]).

taqueria(el_cuervo, [ana,juan,maria], 
        [carnitas_taco, combo_plate, al_pastor_taco, carne_asada_burrito]).

taqueria(la_posta, 
        [victor,maria,carla], [birria_taco, adobado_burrito, carnitas_sopa, combo_plate, adobado_plate]).

taqueria(robertos, [hector,carlos,miguel],
        [adobado_plate, guacamole_taco, al_pastor_burrito, carnitas_taco, carne_asada_burrito]).

taqueria(la_milpas_quatros, [jiminez, martin, antonio, miguel],  
        [lengua_sopa, adobado_plate, combo_plate]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem 2: Rules

available_at(X,L) :- taqueria(L,Z,S),isin(X,S).

multi_available(X) :- taqueria(L,Z,S),taqueria(B,D,G),isin(X,S),isin(X,G),not(S = G),not(L = B).

overworked(X) :- taqueria(L,Z,S),taqueria(B,D,G),isin(X,Z),isin(X,D),Z\=D,L\=B.

total_cost(X,K) :- ingredients(X,S),sumList(S,Acc),K is Acc. 

has_ingredients(X,Is) :- ingredients(X,S),checkList(S,Is).

avoids_ingredients(X,Is) :- ingredients(X,S),notCheckList(S,Is).

p1([H|T],X) :-not(isin(H,T)),has_ingredients(H,X).
p2([H|T],Y) :-not(isin(H,T)),avoids_ingredients(H,Y).

find_items(L,X,Y) :- p1(L1,X), p2(L2,Y),intersection(L1,L2,L).    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
