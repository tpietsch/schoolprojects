! ---Default String Formatters---
		.section	".rodata"
		.align		4
__int_fmt:	.asciz		"%d"
__endl:		.asciz		"\n"
__bool_t:	.asciz		"true"
__bool_f:	.asciz		"false"
__string_fmt:	.asciz		"%s"
__bound:	.asciz		"Index value of %d is outside legal range [0,%d).\n"

		.section	".data"
		.align		4

! ---Global Variables---
		.section	".bss"
		.align		4
__init_main:		.skip		4

! ---Global Initializations---


		.section	".text"
		.global		main
		.align		4
main:
		set		SAVE.main, %g1
		save		%sp, %g1, %sp

! ---Global initialization guard---
		set		__init_main, %l0
		ld		[%l0], %l1
		cmp		%l1, %g0
		bne		__init_done_main
		nop
		set		1, %l1
		st		%l1, [%l0]

! ---Static Initializations---



! ---Funtion body---


__init_done_main:
		! ---Initializing int x---
		set		5, %l3
		! --Store %l3 into x (%l7)--
		! -Copy address of x to %l7-
		set		-8, %l5
		add		%fp, %l5, %l7
		st		%l3, [%l7]


		ret
		restore

SAVE.main = -(92 + 8) & -8

__exitArrayIndex:! -Calling printf-
		call		printf
		nop
! -Calling exit-
		call		exit
		nop

! ---End Function main---


