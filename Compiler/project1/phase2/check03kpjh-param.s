.global	i, b1, foo, __________.MEMLEAK,main
.section	".data"
.align	4
_____3: .asciz " "
.align	4
_____6: .asciz " "
.align	4
_____8: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____9: .asciz " "
.align	4
_____12: .asciz " "
.align	4
_____13: .asciz " "
.align	4
_____16: .asciz "\n"
.align	4
______17: .word	16
______19: .word	14
_____22: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______23: .word	4
_____24: .single	0r3.994
______25: .single	0r3.994
______27: .word	0
______29: .word	12
______31: .word	-12
______32: .word	1
______33: .word	0
______34: .word	1
_____39: .asciz "\n"
.align	4
_____40: .asciz "\n"
.align	4
______45: .word	0
_____50: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::i
i:	.skip	4
!CodeGen::doVarDecl::b1
b1:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	4,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	12,%l5
sub	%l4, %l5, %l5
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-12,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-44,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-8, %l1
add	%fp, %l1, %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f3
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o2
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o3
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o4
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 48) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%f3, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
st	%i2, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
st	%i3, [%l0]
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
st	%i4, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____4 
 nop
set	_boolF,%o1
ba  _____5 
 nop
_____4:
set	_boolT,%o1
_____5:
call printf
nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____7 
 nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	1, %o0
call exit
nop
_____7:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____10 
 nop
set	_boolF,%o1
ba  _____11 
 nop
_____10:
set	_boolT,%o1
_____11:
call printf
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____14 
 nop
set	_boolF,%o1
ba  _____15 
 nop
_____14:
set	_boolT,%o1
_____15:
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	16,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-32,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	14,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-36,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____21 
 nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	1, %o0
call exit
nop
_____21:
st	%f1, [%l0]
ret
restore

SAVE.foo = -(92 + 36) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____46 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____47 
 nop
_____46:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____47:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____48 
 nop
ba  _____49 
 nop
_____48:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
_____49:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

