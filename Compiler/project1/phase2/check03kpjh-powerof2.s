.global	powerof2, __________.MEMLEAK,main
.section	".data"
.align	4
______4: .word	1
______12: .word	0
______15: .word	0
______21: .word	100
_____24: .asciz " is a power of 2: "
.align	4
_____28: .asciz "\n"
.align	4
______30: .word	1
______38: .word	0
_____43: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____16,_____17) 
_____16:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	100,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	100,%l2
cmp	%l1, %l2
bl  _____22 
 nop
set	-8,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____23 
 nop
_____22:
set	1,%l3
set	-8,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____23:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____17 
 nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	powerof2, %l1
call %l1
nop
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____26 
 nop
set	_boolF,%o1
ba  _____27 
 nop
_____26:
set	_boolT,%o1
_____27:
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____16 
 nop
_____17:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

powerof2: 
set SAVE.powerof2, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	1,%l5
sub	%l4, %l5, %l5
set	-16,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
and	%l4, %l5, %l5
set	-28,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____13 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____14 
 nop
_____13:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____14:
set	-32,%l1
add 	%fp, %l1, %l1
ld	[%l1], %i0
ret
restore

ret
restore

SAVE.powerof2 = -(92 + 40) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____39 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____40 
 nop
_____39:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____40:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____41 
 nop
ba  _____42 
 nop
_____41:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
_____42:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

