.global	a, b, foo, x1, x2, y1, z1, z2, z3, complex, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "parameter j = "
.align	4
_____3: .asciz "\n"
.align	4
______7: .word	0
_____12: .asciz "b = j>0, "
.align	4
_____15: .asciz "\n"
.align	4
______18: .word	1
______21: .word	2
______22: .word	5
_____23: .single	0r9.4
______24: .single	0r9.4
______25: .word	1
______26: .word	0
______27: .word	5
_____29: .asciz "int x = "
.align	4
_____30: .asciz "\n"
.align	4
_____31: .asciz "float y = "
.align	4
_____32: .asciz "\n"
.align	4
_____33: .asciz "bool z = "
.align	4
_____36: .asciz "\n"
.align	4
______43: .word	3
______47: .word	12
______58: .word	3
______65: .word	12
______80: .word	0
_____85: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	4
!CodeGen::doVarDecl::b
b:	.skip	4
!CodeGen::doVarDecl::x1
x1:	.skip	4
!CodeGen::doVarDecl::x2
x2:	.skip	4
!CodeGen::doVarDecl::y1
y1:	.skip	4
!CodeGen::doVarDecl::z1
z1:	.skip	4
!CodeGen::doVarDecl::z2
z2:	.skip	4
!CodeGen::doVarDecl::z3
z3:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	2,%l1
set	x1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	x2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______24,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	y1,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	z1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	z2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	z3,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	3,%o0
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	x1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	x1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	x2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-12,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-24,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	y1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	y1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	12,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-32,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-36,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	y1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-44,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-48,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	z1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	z2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____53 
 nop
set	-52,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____54 
 nop
_____53:
set	1,%l3
set	-52,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____54:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f3
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
add	%sp, -0, %sp
set	complex, %l1
call %l1
nop
set	x2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	x2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-64,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	x1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	x1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-76,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	y1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	y1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	12,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-84,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-80,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-84,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-88,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	y1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-92,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-96,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-100,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	x1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	x1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	z3,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____73 
 nop
set	-104,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____74 
 nop
_____73:
set	1,%l3
set	-104,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____74:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f3
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
add	%sp, -0, %sp
set	complex, %l1
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 112) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bg  _____8 
 nop
set	-8,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____9 
 nop
_____8:
set	1,%l3
set	-8,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____9:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	b,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____10 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	b,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____13 
 nop
set	_boolF,%o1
ba  _____14 
 nop
_____13:
set	_boolT,%o1
_____14:
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	1,%l5
sub	%l4, %l5, %l5
set	-28,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
ba  _____11 
 nop
_____10:
_____11:
ret
restore

SAVE.foo = -(92 + 28) & -8

complex: 
set SAVE.complex, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%f3, [%l0]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____34 
 nop
set	_boolF,%o1
ba  _____35 
 nop
_____34:
set	_boolT,%o1
_____35:
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
ret
restore

SAVE.complex = -(92 + 16) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____81 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____82 
 nop
_____81:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____82:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____83 
 nop
ba  _____84 
 nop
_____83:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
_____84:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

