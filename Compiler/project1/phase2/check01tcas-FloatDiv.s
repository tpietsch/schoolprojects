.global	i1, i2, f1, f2, f3, f4, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	1
_____4: .single	0r1.0
______5: .single	0r1.0
_____6: .single	0r1.0
______7: .single	0r1.0
______8: .single	0r1.0
_____9: .single	0r1.0
______10: .single	0r1.0
_____11: .asciz "a: "
.align	4
_____12: .asciz "\n"
.align	4
______14: .word	1
_____17: .asciz "b: "
.align	4
_____18: .asciz "\n"
.align	4
_____19: .single	0r1.0
______21: .single	0r1.0
_____24: .asciz "c: "
.align	4
_____25: .asciz "\n"
.align	4
_____29: .asciz "d: "
.align	4
_____30: .asciz "\n"
.align	4
_____34: .asciz "e: "
.align	4
_____35: .asciz "\n"
.align	4
______38: .word	1
______48: .single	0r1.0
______52: .word	1
_____54: .single	0r1.0
______57: .single	0r1.0
_____59: .asciz "f1: "
.align	4
_____60: .asciz "\n"
.align	4
______63: .word	1
_____68: .single	0r1.0
______71: .single	0r1.0
_____82: .single	0r1.0
______85: .single	0r1.0
_____87: .asciz "f2: "
.align	4
_____88: .asciz "\n"
.align	4
______93: .word	0
_____98: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::i1
i1:	.skip	4
!CodeGen::doVarDecl::i2
i2:	.skip	4
!CodeGen::doVarDecl::f1
f1:	.skip	4
!CodeGen::doVarDecl::f2
f2:	.skip	4
!CodeGen::doVarDecl::f3
f3:	.skip	4
!CodeGen::doVarDecl::f4
f4:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	i1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	i2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f1,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f2,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______7,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f3,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______8,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f4,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
_init_done:
set	______10,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-24,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-32,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	______21,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	i1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-40,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-44,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	i1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-52,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-56,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-68,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-76,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-80,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	i1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-88,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-84,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-92,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-100,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-96,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-100,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-104,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______48,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-112,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-112,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-116,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-124,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-120,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-124,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-128,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______57,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-136,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-140,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	f1,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-148,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-144,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-148,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-152,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-160,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-156,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-160,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-164,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-168,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-168,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______71,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-172,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-168,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-172,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-176,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-180,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-180,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-184,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-180,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-184,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-188,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-192,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-192,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-196,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-192,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-196,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-200,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-204,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-204,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-208,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-204,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-208,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-212,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-216,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-216,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______85,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-220,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-216,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-220,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-224,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	f2,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	f2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 224) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____94 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____95 
 nop
_____94:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____95:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____96 
 nop
ba  _____97 
 nop
_____96:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
_____97:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

