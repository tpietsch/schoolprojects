.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .single	0r4.0
______3: .single	0r4.0
_____4: .single	0r6.0
______5: .single	0r6.0
______6: .word	4
______7: .word	6
_____15: .asciz "\n"
.align	4
_____23: .asciz "\n"
.align	4
_____31: .asciz "\n"
.align	4
_____39: .asciz "\n"
.align	4
_____47: .asciz "\n"
.align	4
_____55: .asciz "\n"
.align	4
_____63: .asciz "\n"
.align	4
_____71: .asciz "\n"
.align	4
_____79: .asciz "\n"
.align	4
_____87: .asciz "\n"
.align	4
______92: .word	0
_____97: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	______3,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	4,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____11 
 nop
set	-20,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____12 
 nop
_____11:
set	1,%l3
set	-20,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____12:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____13 
 nop
set	_boolF,%o1
ba  _____14 
 nop
_____13:
set	_boolT,%o1
_____14:
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-40,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____19 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____20 
 nop
_____19:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____20:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____21 
 nop
set	_boolF,%o1
ba  _____22 
 nop
_____21:
set	_boolT,%o1
_____22:
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-48,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-48,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____27 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____28 
 nop
_____27:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____28:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____29 
 nop
set	_boolF,%o1
ba  _____30 
 nop
_____29:
set	_boolT,%o1
_____30:
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-60,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-60,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____35 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____36 
 nop
_____35:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____36:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____37 
 nop
set	_boolF,%o1
ba  _____38 
 nop
_____37:
set	_boolT,%o1
_____38:
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____43 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____44 
 nop
_____43:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____44:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____45 
 nop
set	_boolF,%o1
ba  _____46 
 nop
_____45:
set	_boolT,%o1
_____46:
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	0, %l0
set	-80,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____51 
 nop
set	-80,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____52 
 nop
_____51:
set	1,%l3
set	-80,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____52:
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____53 
 nop
set	_boolF,%o1
ba  _____54 
 nop
_____53:
set	_boolT,%o1
_____54:
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
set	0, %l0
set	-92,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-100,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____59 
 nop
set	-92,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____60 
 nop
_____59:
set	1,%l3
set	-92,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____60:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____61 
 nop
set	_boolF,%o1
ba  _____62 
 nop
_____61:
set	_boolT,%o1
_____62:
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-112,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-108,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____67 
 nop
set	-104,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____68 
 nop
_____67:
set	1,%l3
set	-104,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____68:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____69 
 nop
set	_boolF,%o1
ba  _____70 
 nop
_____69:
set	_boolT,%o1
_____70:
call printf
nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	0, %l0
set	-116,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-124,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-120,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-120,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____75 
 nop
set	-116,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____76 
 nop
_____75:
set	1,%l3
set	-116,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____76:
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____77 
 nop
set	_boolF,%o1
ba  _____78 
 nop
_____77:
set	_boolT,%o1
_____78:
call printf
nop
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
set	0, %l0
set	-128,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-136,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____83 
 nop
set	-128,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____84 
 nop
_____83:
set	1,%l3
set	-128,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____84:
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____85 
 nop
set	_boolF,%o1
ba  _____86 
 nop
_____85:
set	_boolT,%o1
_____86:
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 136) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____93 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____94 
 nop
_____93:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____94:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____95 
 nop
ba  _____96 
 nop
_____95:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
_____96:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

