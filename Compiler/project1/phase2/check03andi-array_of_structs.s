.global	globalfoo, array_of_foo, __________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	12
______5: .word	2
______10: .word	48
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "2"
.align	4
_____18: .asciz " is outside legal range [0,4).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "2"
.align	4
_____30: .asciz " is outside legal range [0,4).\n"
.align	4
_____34: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______35: .word	5
_____37: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______38: .word	0
______44: .word	4
______48: .word	12
______54: .word	48
_____60: .asciz "Index value of "
.align	4
_____61: .asciz " is outside legal range [0,4).\n"
.align	4
______66: .word	0
_____71: .asciz "Index value of "
.align	4
_____72: .asciz " is outside legal range [0,4).\n"
.align	4
_____76: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____78: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____79: .asciz "\n"
.align	4
______81: .word	12
______87: .word	48
_____93: .asciz "Index value of "
.align	4
_____94: .asciz " is outside legal range [0,4).\n"
.align	4
______99: .word	0
_____104: .asciz "Index value of "
.align	4
_____105: .asciz " is outside legal range [0,4).\n"
.align	4
_____109: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____111: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____114: .asciz "\n"
.align	4
______116: .word	12
______122: .word	48
_____128: .asciz "Index value of "
.align	4
_____129: .asciz " is outside legal range [0,4).\n"
.align	4
______134: .word	0
_____139: .asciz "Index value of "
.align	4
_____140: .asciz " is outside legal range [0,4).\n"
.align	4
_____144: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____146: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____147: .asciz "\n"
.align	4
_____148: .asciz "\n"
.align	4
______150: .word	1
______158: .word	0
_____163: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::globalfoo
globalfoo:	.skip	12
!CodeGen::doVarDecl::array_of_foo
array_of_foo:	.skip	48
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	12,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-12,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	48,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	48,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-16,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-16,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array_of_foo, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____33 
 nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	1, %o0
call exit
nop
_____33:
ld	[%l1], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-60,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	5,%l1
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____36 
 nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	1, %o0
call exit
nop
_____36:
st	%l1, [%l0]
set	0,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____39,_____40) 
_____39:
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	4,%l2
cmp	%l1, %l2
bl  _____45 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____46 
 nop
_____45:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____46:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____40 
 nop
set	12,%l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%o0
set	-84,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-88,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-92,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	48,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	48,%l1
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____56 
 nop
set	-92,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____57 
 nop
_____56:
set	1,%l3
set	-92,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____57:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____58 
 nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____59 
 nop
_____58:
_____59:
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____67 
 nop
set	-104,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____68 
 nop
_____67:
set	1,%l3
set	-104,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____68:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____69 
 nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____70 
 nop
_____69:
_____70:
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array_of_foo, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-124, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____75 
 nop
set	_strFmt,%o0
set	_____76,%o1
call printf
nop
set	1, %o0
call exit
nop
_____75:
ld	[%l1], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-136,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____77 
 nop
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
set	1, %o0
call exit
nop
_____77:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
set	12,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%o0
set	-144,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-148,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-152,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	48,%l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	48,%l1
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____89 
 nop
set	-152,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____90 
 nop
_____89:
set	1,%l3
set	-152,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____90:
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____91 
 nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____92 
 nop
_____91:
_____92:
set	0, %l0
set	-164,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____100 
 nop
set	-164,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____101 
 nop
_____100:
set	1,%l3
set	-164,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____101:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____102 
 nop
set	_strFmt,%o0
set	_____104,%o1
call printf
nop
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____103 
 nop
_____102:
_____103:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array_of_foo, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-184,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-184, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____108 
 nop
set	_strFmt,%o0
set	_____109,%o1
call printf
nop
set	1, %o0
call exit
nop
_____108:
ld	[%l1], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4, %l2
add	%l2, %l1, %l1
set	-196,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____110 
 nop
set	_strFmt,%o0
set	_____111,%o1
call printf
nop
set	1, %o0
call exit
nop
_____110:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____112 
 nop
set	_boolF,%o1
ba  _____113 
 nop
_____112:
set	_boolT,%o1
_____113:
call printf
nop
set	_strFmt,%o0
set	_____114,%o1
call printf
nop
set	12,%l1
set	-200,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%o0
set	-204,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-208,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-212,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	48,%l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-220,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	48,%l1
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____124 
 nop
set	-212,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____125 
 nop
_____124:
set	1,%l3
set	-212,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____125:
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____126 
 nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____129,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____127 
 nop
_____126:
_____127:
set	0, %l0
set	-224,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____135 
 nop
set	-224,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____136 
 nop
_____135:
set	1,%l3
set	-224,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____136:
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____137 
 nop
set	_strFmt,%o0
set	_____139,%o1
call printf
nop
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____140,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____138 
 nop
_____137:
_____138:
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array_of_foo, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-244,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-244, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____143 
 nop
set	_strFmt,%o0
set	_____144,%o1
call printf
nop
set	1, %o0
call exit
nop
_____143:
ld	[%l1], %l1
set	-256,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8, %l2
add	%l2, %l1, %l1
set	-256,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-256,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____145 
 nop
set	_strFmt,%o0
set	_____146,%o1
call printf
nop
set	1, %o0
call exit
nop
_____145:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____147,%o1
call printf
nop
set	_strFmt,%o0
set	_____148,%o1
call printf
nop
set	1,%l1
set	-260,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-264,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-264,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-268,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-272,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-268,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____39 
 nop
_____40:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 272) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____159 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____160 
 nop
_____159:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____160:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____161 
 nop
ba  _____162 
 nop
_____161:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____163,%o1
call printf
nop
_____162:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

