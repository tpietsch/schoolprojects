.global	bT, bF, _0bTopbF19, bA, _0bTopbT120, bB, _0bFopbF131, bC, _0bFopbT142, bD, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	0
______13: .word	0
______14: .word	1
______24: .word	0
______25: .word	1
______35: .word	0
______36: .word	1
______46: .word	0
______47: .word	1
_____48: .asciz "testing global bools"
.align	4
_____49: .asciz "\n"
.align	4
_____52: .asciz "== true01"
.align	4
_____53: .asciz "\n"
.align	4
_____56: .asciz "== false02"
.align	4
_____57: .asciz "\n"
.align	4
_____60: .asciz "== true03"
.align	4
_____61: .asciz "\n"
.align	4
_____64: .asciz "== true04"
.align	4
_____65: .asciz "\n"
.align	4
_____68: .asciz "== false05"
.align	4
_____69: .asciz "\n"
.align	4
_____72: .asciz "== true06"
.align	4
_____73: .asciz "\n"
.align	4
______82: .word	0
______83: .word	1
_____86: .asciz " == true07"
.align	4
_____87: .asciz "\n"
.align	4
______96: .word	0
______97: .word	1
_____100: .asciz " == true08"
.align	4
_____101: .asciz "\n"
.align	4
______102: .word	1
______103: .word	0
______112: .word	0
______113: .word	1
_____116: .asciz "ia || ia is true09"
.align	4
_____117: .asciz "\n"
.align	4
______126: .word	0
______127: .word	1
_____130: .asciz "ia || true is true10"
.align	4
_____131: .asciz "\n"
.align	4
_____142: .asciz "true ||true is true11"
.align	4
_____143: .asciz "\n"
.align	4
______152: .word	0
______153: .word	1
_____156: .asciz "false || ia is true12"
.align	4
_____157: .asciz "\n"
.align	4
_____168: .asciz "false || false is false"
.align	4
_____169: .asciz "\n"
.align	4
_____180: .asciz "true || false is true13"
.align	4
_____181: .asciz "\n"
.align	4
_____192: .asciz "false || true is true14"
.align	4
_____193: .asciz "\n"
.align	4
______202: .word	0
______203: .word	1
_____206: .asciz "ia || ib is true15"
.align	4
_____207: .asciz "\n"
.align	4
______216: .word	0
______217: .word	1
_____220: .asciz "ib || ia is true16"
.align	4
_____221: .asciz "\n"
.align	4
______226: .word	0
_____231: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::bT
bT:	.skip	4
!CodeGen::doVarDecl::bF
bF:	.skip	4
!CodeGen::doVarDecl::bTopbF
_0bTopbF19:	.skip	4
!CodeGen::doVarDecl::bA
bA:	.skip	4
!CodeGen::doVarDecl::bTopbT
_0bTopbT120:	.skip	4
!CodeGen::doVarDecl::bB
bB:	.skip	4
!CodeGen::doVarDecl::bFopbF
_0bFopbF131:	.skip	4
!CodeGen::doVarDecl::bC
bC:	.skip	4
!CodeGen::doVarDecl::bFopbT
_0bFopbT142:	.skip	4
!CodeGen::doVarDecl::bD
bD:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	bT,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	bF,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____4 
 nop
ba  _____6 
 nop
ba  _____5 
 nop
_____4:
_____5:
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____7 
 nop
ba  _____6 
 nop
ba  _____8 
 nop
_____7:
_____8:
set	0,%l1
set	_0bTopbF19,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
ba  _____12 
 nop
_____6:
set	1,%l1
set	_0bTopbF19,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_____12:
set	_0bTopbF19,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bA,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____15 
 nop
ba  _____17 
 nop
ba  _____16 
 nop
_____15:
_____16:
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____18 
 nop
ba  _____17 
 nop
ba  _____19 
 nop
_____18:
_____19:
set	0,%l1
set	_0bTopbT120,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
ba  _____23 
 nop
_____17:
set	1,%l1
set	_0bTopbT120,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_____23:
set	_0bTopbT120,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bB,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
ba  _____28 
 nop
ba  _____27 
 nop
_____26:
_____27:
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____29 
 nop
ba  _____28 
 nop
ba  _____30 
 nop
_____29:
_____30:
set	0,%l1
set	_0bFopbF131,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
ba  _____34 
 nop
_____28:
set	1,%l1
set	_0bFopbF131,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_____34:
set	_0bFopbF131,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bC,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____37 
 nop
ba  _____39 
 nop
ba  _____38 
 nop
_____37:
_____38:
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____40 
 nop
ba  _____39 
 nop
ba  _____41 
 nop
_____40:
_____41:
set	0,%l1
set	_0bFopbT142,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
ba  _____45 
 nop
_____39:
set	1,%l1
set	_0bFopbT142,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_____45:
set	_0bFopbT142,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bD,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____50 
 nop
set	_boolF,%o1
ba  _____51 
 nop
_____50:
set	_boolT,%o1
_____51:
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____54 
 nop
set	_boolF,%o1
ba  _____55 
 nop
_____54:
set	_boolT,%o1
_____55:
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
set	bA,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____58 
 nop
set	_boolF,%o1
ba  _____59 
 nop
_____58:
set	_boolT,%o1
_____59:
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	bB,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____62 
 nop
set	_boolF,%o1
ba  _____63 
 nop
_____62:
set	_boolT,%o1
_____63:
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	bC,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____66 
 nop
set	_boolF,%o1
ba  _____67 
 nop
_____66:
set	_boolT,%o1
_____67:
call printf
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	bD,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____70 
 nop
set	_boolF,%o1
ba  _____71 
 nop
_____70:
set	_boolT,%o1
_____71:
call printf
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____74 
 nop
ba  _____76 
 nop
ba  _____75 
 nop
_____74:
_____75:
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____77 
 nop
ba  _____76 
 nop
ba  _____78 
 nop
_____77:
_____78:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____81 
 nop
_____76:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____81:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____84 
 nop
set	_boolF,%o1
ba  _____85 
 nop
_____84:
set	_boolT,%o1
_____85:
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____88 
 nop
ba  _____90 
 nop
ba  _____89 
 nop
_____88:
_____89:
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____91 
 nop
ba  _____90 
 nop
ba  _____92 
 nop
_____91:
_____92:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____95 
 nop
_____90:
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____95:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____98 
 nop
set	_boolF,%o1
ba  _____99 
 nop
_____98:
set	_boolT,%o1
_____99:
call printf
nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____104 
 nop
ba  _____106 
 nop
ba  _____105 
 nop
_____104:
_____105:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____107 
 nop
ba  _____106 
 nop
ba  _____108 
 nop
_____107:
_____108:
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____111 
 nop
_____106:
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____111:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____114 
 nop
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
ba  _____115 
 nop
_____114:
_____115:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____118 
 nop
ba  _____120 
 nop
ba  _____119 
 nop
_____118:
_____119:
set	1,%l1
cmp	%l1, %g0
be  _____121 
 nop
ba  _____120 
 nop
ba  _____122 
 nop
_____121:
_____122:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____125 
 nop
_____120:
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____125:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____128 
 nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
ba  _____129 
 nop
_____128:
_____129:
set	1,%l1
cmp	%l1, %g0
be  _____132 
 nop
ba  _____134 
 nop
ba  _____133 
 nop
_____132:
_____133:
set	1,%l1
cmp	%l1, %g0
be  _____135 
 nop
ba  _____134 
 nop
ba  _____136 
 nop
_____135:
_____136:
ba  _____139 
 nop
_____134:
_____139:
set	1,%l1
cmp	%l1, %g0
be  _____140 
 nop
set	_strFmt,%o0
set	_____142,%o1
call printf
nop
set	_strFmt,%o0
set	_____143,%o1
call printf
nop
ba  _____141 
 nop
_____140:
_____141:
set	0,%l1
cmp	%l1, %g0
be  _____144 
 nop
ba  _____146 
 nop
ba  _____145 
 nop
_____144:
_____145:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____147 
 nop
ba  _____146 
 nop
ba  _____148 
 nop
_____147:
_____148:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____151 
 nop
_____146:
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____151:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____154 
 nop
set	_strFmt,%o0
set	_____156,%o1
call printf
nop
set	_strFmt,%o0
set	_____157,%o1
call printf
nop
ba  _____155 
 nop
_____154:
_____155:
set	0,%l1
cmp	%l1, %g0
be  _____158 
 nop
ba  _____160 
 nop
ba  _____159 
 nop
_____158:
_____159:
set	0,%l1
cmp	%l1, %g0
be  _____161 
 nop
ba  _____160 
 nop
ba  _____162 
 nop
_____161:
_____162:
ba  _____165 
 nop
_____160:
_____165:
set	0,%l1
cmp	%l1, %g0
be  _____166 
 nop
set	_strFmt,%o0
set	_____168,%o1
call printf
nop
set	_strFmt,%o0
set	_____169,%o1
call printf
nop
ba  _____167 
 nop
_____166:
_____167:
set	1,%l1
cmp	%l1, %g0
be  _____170 
 nop
ba  _____172 
 nop
ba  _____171 
 nop
_____170:
_____171:
set	0,%l1
cmp	%l1, %g0
be  _____173 
 nop
ba  _____172 
 nop
ba  _____174 
 nop
_____173:
_____174:
ba  _____177 
 nop
_____172:
_____177:
set	1,%l1
cmp	%l1, %g0
be  _____178 
 nop
set	_strFmt,%o0
set	_____180,%o1
call printf
nop
set	_strFmt,%o0
set	_____181,%o1
call printf
nop
ba  _____179 
 nop
_____178:
_____179:
set	0,%l1
cmp	%l1, %g0
be  _____182 
 nop
ba  _____184 
 nop
ba  _____183 
 nop
_____182:
_____183:
set	1,%l1
cmp	%l1, %g0
be  _____185 
 nop
ba  _____184 
 nop
ba  _____186 
 nop
_____185:
_____186:
ba  _____189 
 nop
_____184:
_____189:
set	1,%l1
cmp	%l1, %g0
be  _____190 
 nop
set	_strFmt,%o0
set	_____192,%o1
call printf
nop
set	_strFmt,%o0
set	_____193,%o1
call printf
nop
ba  _____191 
 nop
_____190:
_____191:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____194 
 nop
ba  _____196 
 nop
ba  _____195 
 nop
_____194:
_____195:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____197 
 nop
ba  _____196 
 nop
ba  _____198 
 nop
_____197:
_____198:
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____201 
 nop
_____196:
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____201:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____204 
 nop
set	_strFmt,%o0
set	_____206,%o1
call printf
nop
set	_strFmt,%o0
set	_____207,%o1
call printf
nop
ba  _____205 
 nop
_____204:
_____205:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____208 
 nop
ba  _____210 
 nop
ba  _____209 
 nop
_____208:
_____209:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____211 
 nop
ba  _____210 
 nop
ba  _____212 
 nop
_____211:
_____212:
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____215 
 nop
_____210:
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____215:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____218 
 nop
set	_strFmt,%o0
set	_____220,%o1
call printf
nop
set	_strFmt,%o0
set	_____221,%o1
call printf
nop
ba  _____219 
 nop
_____218:
_____219:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____227 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____228 
 nop
_____227:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____228:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____229 
 nop
ba  _____230 
 nop
_____229:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____231,%o1
call printf
nop
_____230:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

