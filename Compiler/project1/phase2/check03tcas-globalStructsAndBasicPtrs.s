.global	gstr, i, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	3
_____3: .single	0r1.23
______6: .single	0r1.23
______7: .word	1
_____11: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____15: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____19: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____20: .asciz "*(gstr.ip) = 3 = "
.align	4
_____23: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____25: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____26: .asciz "\n"
.align	4
_____27: .asciz "*(gstr.fp) = 1.23 = "
.align	4
_____30: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____32: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____33: .asciz "\n"
.align	4
_____34: .asciz "*(gstr.bp) = true = "
.align	4
_____37: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____39: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____42: .asciz "\n"
.align	4
______47: .word	0
_____52: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::gstr
gstr:	.skip	12
!CodeGen::doVarDecl::i
i:	.skip	4
!CodeGen::doVarDecl::f
main__0f24staticFlag:	.skip	4
main__0f24:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	3,%l1
set	i,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	main__0f24staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	______6,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	main__0f24,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	1, %l0
set	main__0f24staticFlag, %l1
st	%l0, [%l1]
_____5:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	gstr, %l1
add	%g0, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-16,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	i, %l1
add	%g0, %l1, %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____10 
 nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	1, %o0
call exit
nop
_____10:
st	%l1, [%l0]
set	gstr, %l1
add	%g0, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-32,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	main__0f24, %l1
add	%g0, %l1, %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____14 
 nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	1, %o0
call exit
nop
_____14:
st	%l1, [%l0]
set	gstr, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-48,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4, %l1
add	%fp, %l1, %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____18 
 nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	1, %o0
call exit
nop
_____18:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-64,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____22 
 nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	1, %o0
call exit
nop
_____22:
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____24 
 nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
_____24:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-76,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____29 
 nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
_____29:
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____31 
 nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	1, %o0
call exit
nop
_____31:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-88,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____36 
 nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	1, %o0
call exit
nop
_____36:
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____38 
 nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	1, %o0
call exit
nop
_____38:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____40 
 nop
set	_boolF,%o1
ba  _____41 
 nop
_____40:
set	_boolT,%o1
_____41:
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 88) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____48 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____49 
 nop
_____48:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____49:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____50 
 nop
ba  _____51 
 nop
_____50:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
_____51:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

