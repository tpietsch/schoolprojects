.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Output format (expected, actual)"
.align	4
_____3: .asciz "\n"
.align	4
______4: .word	6
______6: .word	4
______12: .word	32
_____18: .asciz "Index value of "
.align	4
_____19: .asciz " is outside legal range [0,8).\n"
.align	4
______24: .word	0
_____29: .asciz "Index value of "
.align	4
_____30: .asciz " is outside legal range [0,8).\n"
.align	4
______32: .word	7
_____34: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______35: .word	3
______37: .word	4
______43: .word	32
_____49: .asciz "Index value of "
.align	4
_____50: .asciz " is outside legal range [0,8).\n"
.align	4
______55: .word	0
_____60: .asciz "Index value of "
.align	4
_____61: .asciz " is outside legal range [0,8).\n"
.align	4
______63: .word	9
_____65: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____66: .asciz "array[i] = (9, "
.align	4
______68: .word	4
______74: .word	32
_____80: .asciz "Index value of "
.align	4
_____81: .asciz " is outside legal range [0,8).\n"
.align	4
______86: .word	0
_____91: .asciz "Index value of "
.align	4
_____92: .asciz " is outside legal range [0,8).\n"
.align	4
_____95: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____96: .asciz ")"
.align	4
_____97: .asciz "\n"
.align	4
_____98: .asciz "array[6] = (7, "
.align	4
______100: .word	4
______102: .word	6
______107: .word	32
_____113: .asciz "Index value of "
.align	4
_____114: .asciz "6"
.align	4
_____115: .asciz " is outside legal range [0,8).\n"
.align	4
______120: .word	0
_____125: .asciz "Index value of "
.align	4
_____126: .asciz "6"
.align	4
_____127: .asciz " is outside legal range [0,8).\n"
.align	4
_____130: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____131: .asciz ")"
.align	4
_____132: .asciz "\n"
.align	4
_____133: .asciz "array[3] = (3, "
.align	4
______135: .word	4
______137: .word	3
______142: .word	32
_____148: .asciz "Index value of "
.align	4
_____149: .asciz "3"
.align	4
_____150: .asciz " is outside legal range [0,8).\n"
.align	4
______155: .word	0
_____160: .asciz "Index value of "
.align	4
_____161: .asciz "3"
.align	4
_____162: .asciz " is outside legal range [0,8).\n"
.align	4
_____165: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____166: .asciz ")"
.align	4
_____167: .asciz "\n"
.align	4
______172: .word	0
_____177: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	6,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-48,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	32,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	32,%l1
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____14 
 nop
set	-52,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____15 
 nop
_____14:
set	1,%l3
set	-52,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____15:
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____16 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____17 
 nop
_____16:
_____17:
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____25 
 nop
set	-64,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____26 
 nop
_____25:
set	1,%l3
set	-64,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____26:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____27 
 nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____28 
 nop
_____27:
_____28:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	7,%l1
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____33 
 nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	1, %o0
call exit
nop
_____33:
st	%l1, [%l0]
set	3,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-84,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-88,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-92,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	32,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	32,%l1
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____45 
 nop
set	-92,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____46 
 nop
_____45:
set	1,%l3
set	-92,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____46:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____47 
 nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____48 
 nop
_____47:
_____48:
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____56 
 nop
set	-104,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____57 
 nop
_____56:
set	1,%l3
set	-104,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____57:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____58 
 nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____59 
 nop
_____58:
_____59:
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	9,%l1
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____64 
 nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	1, %o0
call exit
nop
_____64:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____66,%o1
call printf
nop
set	4,%l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-124,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-128,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-132,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	32,%l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	32,%l1
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____76 
 nop
set	-132,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____77 
 nop
_____76:
set	1,%l3
set	-132,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____77:
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____78 
 nop
set	_strFmt,%o0
set	_____80,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____81,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____79 
 nop
_____78:
_____79:
set	0, %l0
set	-144,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____87 
 nop
set	-144,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____88 
 nop
_____87:
set	1,%l3
set	-144,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____88:
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____89 
 nop
set	_strFmt,%o0
set	_____91,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____92,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____90 
 nop
_____89:
_____90:
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____94 
 nop
set	_strFmt,%o0
set	_____95,%o1
call printf
nop
set	1, %o0
call exit
nop
_____94:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
set	4,%l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	6,%o1
call .mul
nop
mov	%o0, %l2
set	-168,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-172,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	32,%l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	32,%l1
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____109 
 nop
set	-172,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____110 
 nop
_____109:
set	1,%l3
set	-172,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____110:
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____111 
 nop
set	_strFmt,%o0
set	_____113,%o1
call printf
nop
set	_strFmt,%o0
set	_____114,%o1
call printf
nop
set	_strFmt,%o0
set	_____115,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____112 
 nop
_____111:
_____112:
set	0, %l0
set	-184,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-188,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-188,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____121 
 nop
set	-184,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____122 
 nop
_____121:
set	1,%l3
set	-184,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____122:
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____123 
 nop
set	_strFmt,%o0
set	_____125,%o1
call printf
nop
set	_strFmt,%o0
set	_____126,%o1
call printf
nop
set	_strFmt,%o0
set	_____127,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____124 
 nop
_____123:
_____124:
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____129 
 nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	1, %o0
call exit
nop
_____129:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
set	_strFmt,%o0
set	_____132,%o1
call printf
nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	4,%l1
set	-200,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-208,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-212,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	32,%l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-220,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	32,%l1
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____144 
 nop
set	-212,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____145 
 nop
_____144:
set	1,%l3
set	-212,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____145:
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____146 
 nop
set	_strFmt,%o0
set	_____148,%o1
call printf
nop
set	_strFmt,%o0
set	_____149,%o1
call printf
nop
set	_strFmt,%o0
set	_____150,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____147 
 nop
_____146:
_____147:
set	0, %l0
set	-224,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____156 
 nop
set	-224,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____157 
 nop
_____156:
set	1,%l3
set	-224,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____157:
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____158 
 nop
set	_strFmt,%o0
set	_____160,%o1
call printf
nop
set	_strFmt,%o0
set	_____161,%o1
call printf
nop
set	_strFmt,%o0
set	_____162,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____159 
 nop
_____158:
_____159:
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-236,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-236,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____164 
 nop
set	_strFmt,%o0
set	_____165,%o1
call printf
nop
set	1, %o0
call exit
nop
_____164:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____166,%o1
call printf
nop
set	_strFmt,%o0
set	_____167,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 236) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____173 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____174 
 nop
_____173:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____174:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____175 
 nop
ba  _____176 
 nop
_____175:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____177,%o1
call printf
nop
_____176:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

