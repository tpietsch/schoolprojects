.global	MINI.foo, STR.foo, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Hello from MINI "
.align	4
_____3: .asciz "\n"
.align	4
_____4: .asciz "Hello!"
.align	4
_____5: .asciz "\n"
.align	4
______7: .word	1
_____9: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______11: .word	2
_____14: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______16: .word	1
_____18: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______20: .word	567
_____22: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____23: .asciz "str.i = 1 = "
.align	4
_____26: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____27: .asciz "\n"
.align	4
_____28: .asciz "str2.i = 567 = "
.align	4
_____31: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____32: .asciz "\n"
.align	4
_____33: .asciz "str.f = 2.00 = "
.align	4
_____36: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____37: .asciz " = str2.f =  "
.align	4
_____40: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____41: .asciz "\n"
.align	4
_____42: .asciz "str.b = true = "
.align	4
_____45: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____48: .asciz " = str2.b =  "
.align	4
_____51: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____54: .asciz "\n"
.align	4
______56: .word	0
_____58: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____59: .asciz "str.b = true = "
.align	4
_____62: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____65: .asciz " != str2.b = "
.align	4
_____68: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____71: .asciz "\n"
.align	4
______76: .word	0
_____81: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-52, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-124,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	1,%l1
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____8 
 nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	1, %o0
call exit
nop
_____8:
st	%l1, [%l0]
set	-52, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-176,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	2,%l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-180,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____13 
 nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	1, %o0
call exit
nop
_____13:
st	%f1, [%l0]
set	-52, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-232,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	1,%l1
set	-232,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____17 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
_____17:
st	%l1, [%l0]
set	52, %o2
set	-52, %o1
add	%fp, %o1, %o1
set	-284, %o0
add	%fp, %o0, %o0
call memmove
nop
set	-284, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-336,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	567,%l1
set	-336,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____21 
 nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	1, %o0
call exit
nop
_____21:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	-52, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-388,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-388,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____25 
 nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	1, %o0
call exit
nop
_____25:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	-284, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-440,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-440,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____30 
 nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	1, %o0
call exit
nop
_____30:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	-52, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-492,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-492,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____35 
 nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1, %o0
call exit
nop
_____35:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	-284, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-544,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-544,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____39 
 nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	1, %o0
call exit
nop
_____39:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	-52, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-596,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-596,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____44 
 nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	1, %o0
call exit
nop
_____44:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____46 
 nop
set	_boolF,%o1
ba  _____47 
 nop
_____46:
set	_boolT,%o1
_____47:
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	-284, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-648,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-648,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____50 
 nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	1, %o0
call exit
nop
_____50:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____52 
 nop
set	_boolF,%o1
ba  _____53 
 nop
_____52:
set	_boolT,%o1
_____53:
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	-284, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-700,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	0,%l1
set	-700,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____57 
 nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	1, %o0
call exit
nop
_____57:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	-52, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-752,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-752,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____61 
 nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	1, %o0
call exit
nop
_____61:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____63 
 nop
set	_boolF,%o1
ba  _____64 
 nop
_____63:
set	_boolT,%o1
_____64:
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	-284, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-804,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-804,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____67 
 nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	1, %o0
call exit
nop
_____67:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____69 
 nop
set	_boolF,%o1
ba  _____70 
 nop
_____69:
set	_boolT,%o1
_____70:
call printf
nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 804) & -8

MINI.foo: 
set SAVE.MINI.foo, %g1
save %sp, %g1, %sp
set	0,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
ret
restore

SAVE.MINI.foo = -(92 + 4) & -8

STR.foo: 
set SAVE.STR.foo, %g1
save %sp, %g1, %sp
set	-12,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
ret
restore

SAVE.STR.foo = -(92 + 12) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____77 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____78 
 nop
_____77:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____78:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____79 
 nop
ba  _____80 
 nop
_____79:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____81,%o1
call printf
nop
_____80:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

