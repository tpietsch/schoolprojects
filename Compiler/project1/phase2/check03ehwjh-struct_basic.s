.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Output format (expected, actual)"
.align	4
_____3: .asciz "\n"
.align	4
______5: .word	4
_____7: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____9: .single	0r2.56
______10: .single	0r2.56
_____12: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____13: .asciz "mys.x = (4, "
.align	4
_____16: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____17: .asciz ")"
.align	4
_____18: .asciz "\n"
.align	4
_____19: .asciz "mys.y = (2.56, "
.align	4
_____22: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____23: .asciz ")"
.align	4
_____24: .asciz "\n"
.align	4
______26: .word	9
_____28: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____30: .single	0r9.01
______31: .single	0r9.01
_____33: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____34: .asciz "mys.x = (9, "
.align	4
_____37: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____38: .asciz ")"
.align	4
_____39: .asciz "\n"
.align	4
_____40: .asciz "mys.y = (9.01, "
.align	4
_____43: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____44: .asciz ")"
.align	4
_____45: .asciz "\n"
.align	4
______50: .word	0
_____55: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	-12, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-24,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	4,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	1, %o0
call exit
nop
_____6:
st	%l1, [%l0]
set	-12, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-36,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	______10,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____11 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1, %o0
call exit
nop
_____11:
st	%f1, [%l0]
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	-12, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-48,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____15 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	1, %o0
call exit
nop
_____15:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	-12, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-60,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____21 
 nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	1, %o0
call exit
nop
_____21:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	-12, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-72,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	9,%l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____27 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	1, %o0
call exit
nop
_____27:
st	%l1, [%l0]
set	-12, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-84,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	______31,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____32 
 nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	1, %o0
call exit
nop
_____32:
st	%f1, [%l0]
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	-12, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-96,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____36 
 nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	1, %o0
call exit
nop
_____36:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	-12, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-108,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____42 
 nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	1, %o0
call exit
nop
_____42:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 108) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____51 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____52 
 nop
_____51:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____52:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____53 
 nop
ba  _____54 
 nop
_____53:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
_____54:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

