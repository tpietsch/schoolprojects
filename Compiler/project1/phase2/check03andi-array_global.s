.global	x, y, z, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______8: .word	5
______12: .word	4
______18: .word	20
_____24: .asciz "Index value of "
.align	4
_____25: .asciz " is outside legal range [0,5).\n"
.align	4
______30: .word	0
_____35: .asciz "Index value of "
.align	4
_____36: .asciz " is outside legal range [0,5).\n"
.align	4
_____39: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____40: .asciz "\t\t(expected 0)"
.align	4
_____41: .asciz "\n"
.align	4
______43: .word	1
______47: .word	0
______53: .word	5
______57: .word	4
______63: .word	20
_____69: .asciz "Index value of "
.align	4
_____70: .asciz " is outside legal range [0,5).\n"
.align	4
______75: .word	0
_____80: .asciz "Index value of "
.align	4
_____81: .asciz " is outside legal range [0,5).\n"
.align	4
_____84: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____87: .asciz "\t\t(expected false)"
.align	4
_____88: .asciz "\n"
.align	4
______90: .word	1
______94: .word	0
______100: .word	5
______104: .word	4
______110: .word	20
_____116: .asciz "Index value of "
.align	4
_____117: .asciz " is outside legal range [0,5).\n"
.align	4
______122: .word	0
_____127: .asciz "Index value of "
.align	4
_____128: .asciz " is outside legal range [0,5).\n"
.align	4
_____131: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____132: .asciz "\t\t(expected 0.00)"
.align	4
_____133: .asciz "\n"
.align	4
______135: .word	1
______143: .word	0
_____148: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
x:	.skip	20
!CodeGen::doVarDecl::y
y:	.skip	20
!CodeGen::doVarDecl::z
z:	.skip	20
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____3,_____4) 
_____3:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	5,%l2
cmp	%l1, %l2
bl  _____9 
 nop
set	-8,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____10 
 nop
_____9:
set	1,%l3
set	-8,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____10:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____4 
 nop
set	4,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-28,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	20,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	20,%l1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____20 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____21 
 nop
_____20:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____21:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____22 
 nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____23 
 nop
_____22:
_____23:
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____31 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____32 
 nop
_____31:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____32:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____33 
 nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____34 
 nop
_____33:
_____34:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	x, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____38 
 nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	1, %o0
call exit
nop
_____38:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	1,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-68,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____3 
 nop
_____4:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____48,_____49) 
_____48:
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	5,%l2
cmp	%l1, %l2
bl  _____54 
 nop
set	-76,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____55 
 nop
_____54:
set	1,%l3
set	-76,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____55:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____49 
 nop
set	4,%l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-92,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-96,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-100,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	20,%l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	20,%l1
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____65 
 nop
set	-100,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____66 
 nop
_____65:
set	1,%l3
set	-100,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____66:
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____67 
 nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____68 
 nop
_____67:
_____68:
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____76 
 nop
set	-112,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____77 
 nop
_____76:
set	1,%l3
set	-112,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____77:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____78 
 nop
set	_strFmt,%o0
set	_____80,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____81,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____79 
 nop
_____78:
_____79:
set	0, %l0
set	-124,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	y, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____83 
 nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	1, %o0
call exit
nop
_____83:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____85 
 nop
set	_boolF,%o1
ba  _____86 
 nop
_____85:
set	_boolT,%o1
_____86:
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	1,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-136,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____48 
 nop
_____49:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____95,_____96) 
_____95:
set	0, %l0
set	-144,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	5,%l2
cmp	%l1, %l2
bl  _____101 
 nop
set	-144,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____102 
 nop
_____101:
set	1,%l3
set	-144,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____102:
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____96 
 nop
set	4,%l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-160,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-164,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-168,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	20,%l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	20,%l1
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____112 
 nop
set	-168,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____113 
 nop
_____112:
set	1,%l3
set	-168,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____113:
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____114 
 nop
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____115 
 nop
_____114:
_____115:
set	0, %l0
set	-180,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-184,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-184,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-188,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____123 
 nop
set	-180,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____124 
 nop
_____123:
set	1,%l3
set	-180,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____124:
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____125 
 nop
set	_strFmt,%o0
set	_____127,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____126 
 nop
_____125:
_____126:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	z, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____130 
 nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
set	1, %o0
call exit
nop
_____130:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____132,%o1
call printf
nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	1,%l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-200,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-200,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-204,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-208,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____95 
 nop
_____96:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 208) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____144 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____145 
 nop
_____144:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____145:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____146 
 nop
ba  _____147 
 nop
_____146:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____148,%o1
call printf
nop
_____147:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

