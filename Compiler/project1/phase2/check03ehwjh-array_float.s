.global	farray, __________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	4
______5: .word	0
______10: .word	40
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "0"
.align	4
_____18: .asciz " is outside legal range [0,10).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "0"
.align	4
_____30: .asciz " is outside legal range [0,10).\n"
.align	4
_____32: .single	0r0.1
______33: .single	0r0.1
_____35: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______37: .word	4
______39: .word	1
______44: .word	40
_____50: .asciz "Index value of "
.align	4
_____51: .asciz "1"
.align	4
_____52: .asciz " is outside legal range [0,10).\n"
.align	4
______57: .word	0
_____62: .asciz "Index value of "
.align	4
_____63: .asciz "1"
.align	4
_____64: .asciz " is outside legal range [0,10).\n"
.align	4
_____66: .single	0r1.2
______67: .single	0r1.2
_____69: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______71: .word	4
______73: .word	2
______78: .word	40
_____84: .asciz "Index value of "
.align	4
_____85: .asciz "2"
.align	4
_____86: .asciz " is outside legal range [0,10).\n"
.align	4
______91: .word	0
_____96: .asciz "Index value of "
.align	4
_____97: .asciz "2"
.align	4
_____98: .asciz " is outside legal range [0,10).\n"
.align	4
_____100: .single	0r4.56
______101: .single	0r4.56
_____103: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____104: .asciz "Output format  (expected, actual)"
.align	4
_____105: .asciz "\n"
.align	4
_____106: .asciz "farray[0] = (0.10, "
.align	4
______108: .word	4
______110: .word	0
______115: .word	40
_____121: .asciz "Index value of "
.align	4
_____122: .asciz "0"
.align	4
_____123: .asciz " is outside legal range [0,10).\n"
.align	4
______128: .word	0
_____133: .asciz "Index value of "
.align	4
_____134: .asciz "0"
.align	4
_____135: .asciz " is outside legal range [0,10).\n"
.align	4
_____138: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____139: .asciz ")"
.align	4
_____140: .asciz "\n"
.align	4
_____141: .asciz "farray[1] = (1.20, "
.align	4
______143: .word	4
______145: .word	1
______150: .word	40
_____156: .asciz "Index value of "
.align	4
_____157: .asciz "1"
.align	4
_____158: .asciz " is outside legal range [0,10).\n"
.align	4
______163: .word	0
_____168: .asciz "Index value of "
.align	4
_____169: .asciz "1"
.align	4
_____170: .asciz " is outside legal range [0,10).\n"
.align	4
_____173: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____174: .asciz ")"
.align	4
_____175: .asciz "\n"
.align	4
_____176: .asciz "farray[2] = (4.56, "
.align	4
______178: .word	4
______180: .word	2
______185: .word	40
_____191: .asciz "Index value of "
.align	4
_____192: .asciz "2"
.align	4
_____193: .asciz " is outside legal range [0,10).\n"
.align	4
______198: .word	0
_____203: .asciz "Index value of "
.align	4
_____204: .asciz "2"
.align	4
_____205: .asciz " is outside legal range [0,10).\n"
.align	4
_____208: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____209: .asciz ")"
.align	4
_____210: .asciz "\n"
.align	4
______215: .word	0
_____220: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::farray
farray:	.skip	40
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	4,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-12,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-16,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-16,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	farray, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______33,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____34 
 nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	1, %o0
call exit
nop
_____34:
st	%f1, [%l0]
set	4,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-52,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____46 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____47 
 nop
_____46:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____47:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____48 
 nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____49 
 nop
_____48:
_____49:
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____58 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____59 
 nop
_____58:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____59:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____60 
 nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____61 
 nop
_____60:
_____61:
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	farray, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______67,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____68 
 nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	1, %o0
call exit
nop
_____68:
st	%f1, [%l0]
set	4,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-92,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-96,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____80 
 nop
set	-96,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____81 
 nop
_____80:
set	1,%l3
set	-96,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____81:
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____82 
 nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____83 
 nop
_____82:
_____83:
set	0, %l0
set	-108,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____92 
 nop
set	-108,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____93 
 nop
_____92:
set	1,%l3
set	-108,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____93:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____94 
 nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____95 
 nop
_____94:
_____95:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	farray, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______101,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____102 
 nop
set	_strFmt,%o0
set	_____103,%o1
call printf
nop
set	1, %o0
call exit
nop
_____102:
st	%f1, [%l0]
set	_strFmt,%o0
set	_____104,%o1
call printf
nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
set	_strFmt,%o0
set	_____106,%o1
call printf
nop
set	4,%l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-132,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-136,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____117 
 nop
set	-136,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____118 
 nop
_____117:
set	1,%l3
set	-136,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____118:
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____119 
 nop
set	_strFmt,%o0
set	_____121,%o1
call printf
nop
set	_strFmt,%o0
set	_____122,%o1
call printf
nop
set	_strFmt,%o0
set	_____123,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____120 
 nop
_____119:
_____120:
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____129 
 nop
set	-148,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____130 
 nop
_____129:
set	1,%l3
set	-148,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____130:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____131 
 nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	_strFmt,%o0
set	_____134,%o1
call printf
nop
set	_strFmt,%o0
set	_____135,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____132 
 nop
_____131:
_____132:
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	farray, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____137 
 nop
set	_strFmt,%o0
set	_____138,%o1
call printf
nop
set	1, %o0
call exit
nop
_____137:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____139,%o1
call printf
nop
set	_strFmt,%o0
set	_____140,%o1
call printf
nop
set	_strFmt,%o0
set	_____141,%o1
call printf
nop
set	4,%l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-172,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-176,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-184,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____152 
 nop
set	-176,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____153 
 nop
_____152:
set	1,%l3
set	-176,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____153:
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____154 
 nop
set	_strFmt,%o0
set	_____156,%o1
call printf
nop
set	_strFmt,%o0
set	_____157,%o1
call printf
nop
set	_strFmt,%o0
set	_____158,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____155 
 nop
_____154:
_____155:
set	0, %l0
set	-188,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____164 
 nop
set	-188,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____165 
 nop
_____164:
set	1,%l3
set	-188,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____165:
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____166 
 nop
set	_strFmt,%o0
set	_____168,%o1
call printf
nop
set	_strFmt,%o0
set	_____169,%o1
call printf
nop
set	_strFmt,%o0
set	_____170,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____167 
 nop
_____166:
_____167:
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	farray, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-200,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____172 
 nop
set	_strFmt,%o0
set	_____173,%o1
call printf
nop
set	1, %o0
call exit
nop
_____172:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____174,%o1
call printf
nop
set	_strFmt,%o0
set	_____175,%o1
call printf
nop
set	_strFmt,%o0
set	_____176,%o1
call printf
nop
set	4,%l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-208,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-212,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-216,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-220,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-224,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____187 
 nop
set	-216,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____188 
 nop
_____187:
set	1,%l3
set	-216,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____188:
set	-216,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____189 
 nop
set	_strFmt,%o0
set	_____191,%o1
call printf
nop
set	_strFmt,%o0
set	_____192,%o1
call printf
nop
set	_strFmt,%o0
set	_____193,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____190 
 nop
_____189:
_____190:
set	0, %l0
set	-228,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-236,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-232,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____199 
 nop
set	-228,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____200 
 nop
_____199:
set	1,%l3
set	-228,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____200:
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____201 
 nop
set	_strFmt,%o0
set	_____203,%o1
call printf
nop
set	_strFmt,%o0
set	_____204,%o1
call printf
nop
set	_strFmt,%o0
set	_____205,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____202 
 nop
_____201:
_____202:
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	farray, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-240,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-240,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____207 
 nop
set	_strFmt,%o0
set	_____208,%o1
call printf
nop
set	1, %o0
call exit
nop
_____207:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____209,%o1
call printf
nop
set	_strFmt,%o0
set	_____210,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 240) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____216 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____217 
 nop
_____216:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____217:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____218 
 nop
ba  _____219 
 nop
_____218:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____220,%o1
call printf
nop
_____219:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

