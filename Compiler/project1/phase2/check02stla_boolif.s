.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______3: .word	1
_____4: .asciz "a && b = false, "
.align	4
______13: .word	0
______14: .word	1
_____17: .asciz "\n"
.align	4
_____18: .asciz "a||b = true, "
.align	4
______27: .word	0
______28: .word	1
_____31: .asciz "\n"
.align	4
_____32: .asciz "!a = true, "
.align	4
_____36: .asciz "\n"
.align	4
______45: .word	0
______46: .word	1
_____49: .asciz "a&&b = true, wrong"
.align	4
_____50: .asciz "\n"
.align	4
______59: .word	0
______60: .word	1
_____63: .asciz "b&&a = true, wrong"
.align	4
_____64: .asciz "\n"
.align	4
______73: .word	0
______74: .word	1
_____77: .asciz "a||b = true, ok"
.align	4
_____78: .asciz "\n"
.align	4
______87: .word	0
______88: .word	1
_____91: .asciz "b||a = true, ok"
.align	4
_____92: .asciz "\n"
.align	4
_____96: .asciz "!a = true, ok"
.align	4
_____97: .asciz "\n"
.align	4
______106: .word	0
______107: .word	1
_____110: .asciz "b&&a = true, wrong"
.align	4
_____111: .asciz "\n"
.align	4
_____112: .asciz "b&&a = false, inside else condition, ok"
.align	4
_____113: .asciz "\n"
.align	4
______124: .word	0
______125: .word	1
______132: .word	0
______133: .word	1
_____136: .asciz "b || bool expr, true"
.align	4
_____137: .asciz "\n"
.align	4
______146: .word	0
______147: .word	1
______157: .word	0
______158: .word	1
_____161: .asciz "!(a||b)->false && false, ok"
.align	4
_____162: .asciz "\n"
.align	4
_____168: .asciz "a!=b, ok"
.align	4
_____169: .asciz "\n"
.align	4
_____175: .asciz "a==b, wrong"
.align	4
_____176: .asciz "\n"
.align	4
______177: .word	1
______186: .word	0
______187: .word	1
_____191: .asciz "c = a&&b = false, "
.align	4
_____194: .asciz "\n"
.align	4
______196: .word	0
______201: .word	0
_____206: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____5 
 nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____7 
 nop
ba  _____9 
 nop
ba  _____8 
 nop
_____7:
ba  _____8 
 nop
_____5:
_____8:
_____6:
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____12 
 nop
_____9:
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____12:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____15 
 nop
set	_boolF,%o1
ba  _____16 
 nop
_____15:
set	_boolT,%o1
_____16:
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____19 
 nop
ba  _____21 
 nop
ba  _____20 
 nop
_____19:
_____20:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____22 
 nop
ba  _____21 
 nop
ba  _____23 
 nop
_____22:
_____23:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____26 
 nop
_____21:
set	1,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____26:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____29 
 nop
set	_boolF,%o1
ba  _____30 
 nop
_____29:
set	_boolT,%o1
_____30:
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-4,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____34 
 nop
set	_boolF,%o1
ba  _____35 
 nop
_____34:
set	_boolT,%o1
_____35:
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____37 
 nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____39 
 nop
ba  _____41 
 nop
ba  _____40 
 nop
_____39:
ba  _____40 
 nop
_____37:
_____40:
_____38:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____44 
 nop
_____41:
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____44:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____47 
 nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
ba  _____48 
 nop
_____47:
_____48:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____51 
 nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____53 
 nop
ba  _____55 
 nop
ba  _____54 
 nop
_____53:
ba  _____54 
 nop
_____51:
_____54:
_____52:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____58 
 nop
_____55:
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____58:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____61 
 nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
ba  _____62 
 nop
_____61:
_____62:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____65 
 nop
ba  _____67 
 nop
ba  _____66 
 nop
_____65:
_____66:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____68 
 nop
ba  _____67 
 nop
ba  _____69 
 nop
_____68:
_____69:
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____72 
 nop
_____67:
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____72:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____75 
 nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
ba  _____76 
 nop
_____75:
_____76:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____79 
 nop
ba  _____81 
 nop
ba  _____80 
 nop
_____79:
_____80:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____82 
 nop
ba  _____81 
 nop
ba  _____83 
 nop
_____82:
_____83:
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____86 
 nop
_____81:
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____86:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____89 
 nop
set	_strFmt,%o0
set	_____91,%o1
call printf
nop
set	_strFmt,%o0
set	_____92,%o1
call printf
nop
ba  _____90 
 nop
_____89:
_____90:
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-4,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-40,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____94 
 nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
ba  _____95 
 nop
_____94:
_____95:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____98 
 nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____100 
 nop
ba  _____102 
 nop
ba  _____101 
 nop
_____100:
ba  _____101 
 nop
_____98:
_____101:
_____99:
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____105 
 nop
_____102:
set	1,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____105:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____108 
 nop
set	_strFmt,%o0
set	_____110,%o1
call printf
nop
set	_strFmt,%o0
set	_____111,%o1
call printf
nop
ba  _____109 
 nop
_____108:
set	_strFmt,%o0
set	_____112,%o1
call printf
nop
set	_strFmt,%o0
set	_____113,%o1
call printf
nop
_____109:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____114 
 nop
ba  _____116 
 nop
ba  _____115 
 nop
_____114:
_____115:
set	1,%l1
cmp	%l1, %g0
be  _____117 
 nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____119 
 nop
ba  _____116 
 nop
ba  _____120 
 nop
_____119:
ba  _____120 
 nop
_____117:
_____120:
_____118:
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____123 
 nop
_____116:
set	1,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____123:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____126 
 nop
ba  _____128 
 nop
ba  _____127 
 nop
_____126:
_____127:
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____131 
 nop
_____128:
set	1,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____131:
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____134 
 nop
set	_strFmt,%o0
set	_____136,%o1
call printf
nop
set	_strFmt,%o0
set	_____137,%o1
call printf
nop
ba  _____135 
 nop
_____134:
_____135:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____138 
 nop
ba  _____140 
 nop
ba  _____139 
 nop
_____138:
_____139:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____141 
 nop
ba  _____140 
 nop
ba  _____142 
 nop
_____141:
_____142:
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____145 
 nop
_____140:
set	1,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____145:
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-60,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____149 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____151 
 nop
ba  _____153 
 nop
ba  _____152 
 nop
_____151:
ba  _____152 
 nop
_____149:
_____152:
_____150:
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____156 
 nop
_____153:
set	1,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____156:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____159 
 nop
set	_strFmt,%o0
set	_____161,%o1
call printf
nop
set	_strFmt,%o0
set	_____162,%o1
call printf
nop
ba  _____160 
 nop
_____159:
_____160:
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____164 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____165 
 nop
_____164:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____165:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____166 
 nop
set	_strFmt,%o0
set	_____168,%o1
call printf
nop
set	_strFmt,%o0
set	_____169,%o1
call printf
nop
ba  _____167 
 nop
_____166:
_____167:
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____171 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____172 
 nop
_____171:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____172:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____173 
 nop
set	_strFmt,%o0
set	_____175,%o1
call printf
nop
set	_strFmt,%o0
set	_____176,%o1
call printf
nop
ba  _____174 
 nop
_____173:
_____174:
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____178 
 nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____180 
 nop
ba  _____182 
 nop
ba  _____181 
 nop
_____180:
ba  _____181 
 nop
_____178:
_____181:
_____179:
set	0, %l0
set	-80,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____185 
 nop
_____182:
set	1,%l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____185:
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-84,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____189 
 nop
set	_strFmt,%o0
set	_____191,%o1
call printf
nop
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____192 
 nop
set	_boolF,%o1
ba  _____193 
 nop
_____192:
set	_boolT,%o1
_____193:
call printf
nop
set	_strFmt,%o0
set	_____194,%o1
call printf
nop
ba  _____190 
 nop
_____189:
_____190:
call __________.MEMLEAK
nop
set	0,%l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%i0
ret
restore

call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 88) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____202 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____203 
 nop
_____202:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____203:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____204 
 nop
ba  _____205 
 nop
_____204:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____206,%o1
call printf
nop
_____205:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

