.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .single	0r8.3
______3: .single	0r8.3
_____4: .single	0r2.5
______5: .single	0r2.5
______6: .word	3
_____7: .single	0r2.4
_____8: .single	0r5.9
_____9: .single 0r14.160001
_____10: .asciz "\n"
.align	4
_____14: .asciz "\n"
.align	4
_____18: .asciz "\n"
.align	4
______23: .word	0
_____28: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	______3,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	3,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_____9,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-24,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-32,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-36,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____24 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
ba  _____27 
 nop
_____26:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
_____27:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

