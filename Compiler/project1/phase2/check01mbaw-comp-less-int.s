.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	10
______3: .word	5
_____4: .asciz "testing greater than"
.align	4
_____5: .asciz "\n"
.align	4
_____13: .asciz " == false1"
.align	4
_____14: .asciz "\n"
.align	4
_____22: .asciz " == true2"
.align	4
_____23: .asciz "\n"
.align	4
_____31: .asciz "ia < ib is false"
.align	4
_____32: .asciz "\n"
.align	4
_____40: .asciz "ib < ia is true3"
.align	4
_____41: .asciz "\n"
.align	4
_____42: .asciz "testing greater than or equal to"
.align	4
_____43: .asciz "\n"
.align	4
_____51: .asciz " == false4"
.align	4
_____52: .asciz "\n"
.align	4
_____60: .asciz " == true5"
.align	4
_____61: .asciz "\n"
.align	4
_____69: .asciz "ia <= ib is false"
.align	4
_____70: .asciz "\n"
.align	4
_____78: .asciz "ib <= ia is true6"
.align	4
_____79: .asciz "\n"
.align	4
_____80: .asciz "testing greater than or equal to (both at same value)"
.align	4
_____81: .asciz "\n"
.align	4
______82: .word	10
_____90: .asciz " == true7"
.align	4
_____91: .asciz "\n"
.align	4
_____99: .asciz " == true8"
.align	4
_____100: .asciz "\n"
.align	4
_____108: .asciz "ia <= ic is true9"
.align	4
_____109: .asciz "\n"
.align	4
_____117: .asciz "ic <= ia is true10"
.align	4
_____118: .asciz "\n"
.align	4
______123: .word	0
_____128: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	10,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____9 
 nop
set	-12,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____10 
 nop
_____9:
set	1,%l3
set	-12,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____10:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____11 
 nop
set	_boolF,%o1
ba  _____12 
 nop
_____11:
set	_boolT,%o1
_____12:
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____18 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____19 
 nop
_____18:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____19:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____20 
 nop
set	_boolF,%o1
ba  _____21 
 nop
_____20:
set	_boolT,%o1
_____21:
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____27 
 nop
set	-36,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____28 
 nop
_____27:
set	1,%l3
set	-36,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____28:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____29 
 nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
ba  _____30 
 nop
_____29:
_____30:
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____36 
 nop
set	-48,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____37 
 nop
_____36:
set	1,%l3
set	-48,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____37:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____38 
 nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
ba  _____39 
 nop
_____38:
_____39:
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____47 
 nop
set	-60,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____48 
 nop
_____47:
set	1,%l3
set	-60,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____48:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____49 
 nop
set	_boolF,%o1
ba  _____50 
 nop
_____49:
set	_boolT,%o1
_____50:
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____56 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____57 
 nop
_____56:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____57:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____58 
 nop
set	_boolF,%o1
ba  _____59 
 nop
_____58:
set	_boolT,%o1
_____59:
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____65 
 nop
set	-84,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____66 
 nop
_____65:
set	1,%l3
set	-84,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____66:
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____67 
 nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
ba  _____68 
 nop
_____67:
_____68:
set	0, %l0
set	-96,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____74 
 nop
set	-96,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____75 
 nop
_____74:
set	1,%l3
set	-96,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____75:
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____76 
 nop
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
ba  _____77 
 nop
_____76:
_____77:
set	_strFmt,%o0
set	_____80,%o1
call printf
nop
set	_strFmt,%o0
set	_____81,%o1
call printf
nop
set	10,%l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____86 
 nop
set	-112,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____87 
 nop
_____86:
set	1,%l3
set	-112,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____87:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____88 
 nop
set	_boolF,%o1
ba  _____89 
 nop
_____88:
set	_boolT,%o1
_____89:
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	_strFmt,%o0
set	_____91,%o1
call printf
nop
set	0, %l0
set	-124,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____95 
 nop
set	-124,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____96 
 nop
_____95:
set	1,%l3
set	-124,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____96:
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____97 
 nop
set	_boolF,%o1
ba  _____98 
 nop
_____97:
set	_boolT,%o1
_____98:
call printf
nop
set	_strFmt,%o0
set	_____99,%o1
call printf
nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	0, %l0
set	-136,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____104 
 nop
set	-136,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____105 
 nop
_____104:
set	1,%l3
set	-136,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____105:
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____106 
 nop
set	_strFmt,%o0
set	_____108,%o1
call printf
nop
set	_strFmt,%o0
set	_____109,%o1
call printf
nop
ba  _____107 
 nop
_____106:
_____107:
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____113 
 nop
set	-148,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____114 
 nop
_____113:
set	1,%l3
set	-148,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____114:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____115 
 nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	_strFmt,%o0
set	_____118,%o1
call printf
nop
ba  _____116 
 nop
_____115:
_____116:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 156) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____124 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____125 
 nop
_____124:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____125:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____126 
 nop
ba  _____127 
 nop
_____126:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
_____127:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

