.global	foo, MYS.foo, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "wrong function!"
.align	4
_____3: .asciz "\n"
.align	4
_____4: .asciz "okay!"
.align	4
_____5: .asciz "\n"
.align	4
_____13: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____16: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____22: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______27: .word	0
_____32: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0, %l1
add	%fp, %l1, %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	MYS.foo, %l0
add	%g0, %l0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	0, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-16, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____10 
 nop
mov	%l1, %o0
set	1,%o1
add	%sp, -0, %sp
ba  _____11 
 nop
_____10:
set	1,%o0
add	%sp, -0, %sp
_____11:
_____9:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____12 
 nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	1, %o0
call exit
nop
_____12:
call %l1
nop
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____15 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	1, %o0
call exit
nop
_____15:
ld	[%l1], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	MYS.foo, %l0
add	%g0, %l0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-16, %l1
add	%fp, %l1, %l1
ld	[%l1], %l1
st	%l1, [%l0]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____19 
 nop
mov	%l1, %o0
set	1,%o1
add	%sp, -0, %sp
ba  _____20 
 nop
_____19:
set	1,%o0
add	%sp, -0, %sp
_____20:
_____18:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____21 
 nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	1, %o0
call exit
nop
_____21:
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 24) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
ret
restore

SAVE.foo = -(92 + 4) & -8

MYS.foo: 
set SAVE.MYS.foo, %g1
save %sp, %g1, %sp
set	0,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
ret
restore

SAVE.MYS.foo = -(92 + 4) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____28 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____29 
 nop
_____28:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____29:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____30 
 nop
ba  _____31 
 nop
_____30:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
_____31:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

