.global	FOO.hello, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "this.d: "
.align	4
_____5: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____7: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____8: .asciz "\n"
.align	4
_____9: .asciz "this.x[0]: "
.align	4
_____12: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______14: .word	4
______16: .word	0
______21: .word	12
_____27: .asciz "Index value of "
.align	4
_____28: .asciz "0"
.align	4
_____29: .asciz " is outside legal range [0,3).\n"
.align	4
______34: .word	0
_____39: .asciz "Index value of "
.align	4
_____40: .asciz "0"
.align	4
_____41: .asciz " is outside legal range [0,3).\n"
.align	4
_____43: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____46: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____47: .asciz "\n"
.align	4
_____50: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______51: .word	7
_____53: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______55: .word	3
_____57: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______60: .word	4
______62: .word	0
______67: .word	12
_____73: .asciz "Index value of "
.align	4
_____74: .asciz "0"
.align	4
_____75: .asciz " is outside legal range [0,3).\n"
.align	4
______80: .word	0
_____85: .asciz "Index value of "
.align	4
_____86: .asciz "0"
.align	4
_____87: .asciz " is outside legal range [0,3).\n"
.align	4
_____89: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______91: .word	5
_____93: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____100: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____101: .asciz "this should be 7 "
.align	4
_____104: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____105: .asciz "\n"
.align	4
______110: .word	0
_____115: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-16, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-32,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	3,%l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____56 
 nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
set	1, %o0
call exit
nop
_____56:
st	%l1, [%l0]
set	-16, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-48,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	4,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-60,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____69 
 nop
set	-64,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____70 
 nop
_____69:
set	1,%l3
set	-64,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____70:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____71 
 nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
set	_strFmt,%o0
set	_____75,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____72 
 nop
_____71:
_____72:
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____81 
 nop
set	-76,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____82 
 nop
_____81:
set	1,%l3
set	-76,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____82:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____83 
 nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____84 
 nop
_____83:
_____84:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48, %l2
add	%fp, %l2, %l2
cmp	%l2, %g0
bne  _____88 
 nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	1, %o0
call exit
nop
_____88:
ld	[%l2], %l2
add	%l1, %l2, %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____92 
 nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	1, %o0
call exit
nop
_____92:
st	%l1, [%l0]
set	FOO.hello, %l0
add	%g0, %l0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-112, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-16, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-112, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____97 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____98 
 nop
_____97:
add	%sp, -0, %sp
_____98:
_____96:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____99 
 nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	1, %o0
call exit
nop
_____99:
call %l1
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	-16, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-128,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____103 
 nop
set	_strFmt,%o0
set	_____104,%o1
call printf
nop
set	1, %o0
call exit
nop
_____103:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 128) & -8

FOO.hello: 
set SAVE.FOO.hello, %g1
save %sp, %g1, %sp
set	-16,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	-16, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____4 
 nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	1, %o0
call exit
nop
_____4:
ld	[%l1], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-32,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	1, %o0
call exit
nop
_____6:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	-16, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____11 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1, %o0
call exit
nop
_____11:
ld	[%l1], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4, %l2
add	%l2, %l1, %l1
set	-48,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	4,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-60,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____23 
 nop
set	-64,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____24 
 nop
_____23:
set	1,%l3
set	-64,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____24:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____25 
 nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____26 
 nop
_____25:
_____26:
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____35 
 nop
set	-76,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____36 
 nop
_____35:
set	1,%l3
set	-76,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____36:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____37 
 nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____38 
 nop
_____37:
_____38:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48, %l2
add	%fp, %l2, %l2
cmp	%l2, %g0
bne  _____42 
 nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	1, %o0
call exit
nop
_____42:
ld	[%l2], %l2
add	%l1, %l2, %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____45 
 nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	1, %o0
call exit
nop
_____45:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	-16, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____49 
 nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	1, %o0
call exit
nop
_____49:
ld	[%l1], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-104,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	7,%l1
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____52 
 nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	1, %o0
call exit
nop
_____52:
st	%l1, [%l0]
ret
restore

SAVE.FOO.hello = -(92 + 104) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____111 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____112 
 nop
_____111:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____112:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____113 
 nop
ba  _____114 
 nop
_____113:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____115,%o1
call printf
nop
_____114:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

