.global	oneParam, __________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	-1
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____10: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____12: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____14: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____15: .asciz "\n"
.align	4
______16: .word	5
_____19: .asciz "\n"
.align	4
______24: .word	0
_____29: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	oneParam, %l1
call %l1
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 8) & -8

oneParam: 
set SAVE.oneParam, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-16,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____9 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1, %o0
call exit
nop
_____9:
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____11 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1, %o0
call exit
nop
_____11:
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____13 
 nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	1, %o0
call exit
nop
_____13:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
ret
restore

SAVE.oneParam = -(92 + 20) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____25 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____26 
 nop
_____25:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____26:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____27 
 nop
ba  _____28 
 nop
_____27:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
_____28:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

