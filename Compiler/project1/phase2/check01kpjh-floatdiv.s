.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	0
______5: .word	4
______7: .word	-4
______9: .word	0
______11: .word	3
______13: .word	-3
_____14: .single	0r6.3
______15: .single	0r6.3
_____16: .single	0r3.4
______18: .word	0
______20: .single	0r3.4
______22: .single	0r-3.4
______24: .word	0
______28: .word	0
_____34: .asciz "\n"
.align	4
______36: .word	0
_____42: .asciz "\n"
.align	4
______44: .word	0
______48: .word	0
_____54: .asciz "\n"
.align	4
______56: .word	0
______60: .word	0
_____66: .asciz "\n"
.align	4
______71: .word	0
_____76: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	4,%l5
sub	%l4, %l5, %l5
set	-12,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	3,%l5
sub	%l4, %l5, %l5
set	-28,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-3,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______15,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______20,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-44,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-40,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-48,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	______22,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-64,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-76,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-84,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
set	-88,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	0,%l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-96,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-100,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-104,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-104,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-108,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-104,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-108,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-112,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	0,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-120,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-124,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-128,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-128,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-136,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-140,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-140,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-140,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-144,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-148,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	0,%l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-152,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-152,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-156,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-160,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	0,%l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-168,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-164,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-164,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-168,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-172,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-176,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-176,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-180,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-176,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-180,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-184,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____66,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 184) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____72 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____73 
 nop
_____72:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____73:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____74 
 nop
ba  _____75 
 nop
_____74:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____76,%o1
call printf
nop
_____75:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

