.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____4: .asciz "9>5, correct"
.align	4
_____5: .asciz "\n"
.align	4
_____8: .asciz "5>9, wrong"
.align	4
_____9: .asciz "\n"
.align	4
______11: .word	0
______13: .word	9
_____17: .asciz "5>-9, correct"
.align	4
_____18: .asciz "\n"
.align	4
______20: .word	0
______22: .word	3
_____26: .asciz "5+12-9>-3, correct"
.align	4
_____27: .asciz "\n"
.align	4
______29: .word	0
______31: .word	3
_____35: .asciz "-3>5-12+20, wrong"
.align	4
_____36: .asciz "\n"
.align	4
______41: .word	0
_____46: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	1,%l1
cmp	%l1, %g0
be  _____2 
 nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
ba  _____3 
 nop
_____2:
_____3:
set	0,%l1
cmp	%l1, %g0
be  _____6 
 nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
ba  _____7 
 nop
_____6:
_____7:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	9,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	9,%l5
sub	%l4, %l5, %l5
set	-12,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	1,%l1
cmp	%l1, %g0
be  _____15 
 nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
ba  _____16 
 nop
_____15:
_____16:
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	3,%l5
sub	%l4, %l5, %l5
set	-24,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	1,%l1
cmp	%l1, %g0
be  _____24 
 nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
ba  _____25 
 nop
_____24:
_____25:
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	3,%l5
sub	%l4, %l5, %l5
set	-36,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0,%l1
cmp	%l1, %g0
be  _____33 
 nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
ba  _____34 
 nop
_____33:
_____34:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____42 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____43 
 nop
_____42:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____43:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____44 
 nop
ba  _____45 
 nop
_____44:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
_____45:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

