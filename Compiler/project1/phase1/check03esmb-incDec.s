.global	gi_x, gi_xx, gf_y, gf_yy, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	11
______3: .word	22
_____4: .single	0r11.1
______5: .single	0r11.1
_____6: .single	0r22.2
______7: .single	0r22.2
______10: .word	33
______13: .word	44
_____14: .single	0r33.3
______17: .single	0r33.3
_____18: .single	0r44.4
______21: .single	0r44.4
______22: .word	55
______23: .word	66
_____24: .single	0r55.5
______25: .single	0r55.5
_____26: .single	0r66.6
______27: .single	0r66.6
______29: .word	1
______34: .word	-1
______39: .word	1
______44: .word	-1
______49: .word	1
______54: .word	-1
______59: .word	1
______64: .word	-1
______69: .word	1
______74: .word	-1
______79: .word	1
______84: .word	-1
_____88: .asciz "Global Int gi_x: "
.align	4
_____89: .asciz " (should print 12)"
.align	4
_____90: .asciz "\n"
.align	4
_____91: .asciz "Global Int gi_xx: "
.align	4
_____92: .asciz " (should print 21)"
.align	4
_____93: .asciz "\n"
.align	4
_____94: .asciz "Global Float gf_y: "
.align	4
_____95: .asciz " (should print 12.10)"
.align	4
_____96: .asciz "\n"
.align	4
_____97: .asciz "Global Float gf_yy: "
.align	4
_____98: .asciz " (should print 21.20)"
.align	4
_____99: .asciz "\n"
.align	4
_____100: .asciz "Static Int si_x: "
.align	4
_____101: .asciz " (should print 34)"
.align	4
_____102: .asciz "\n"
.align	4
_____103: .asciz "Static Int si_xx: "
.align	4
_____104: .asciz " (should print 43)"
.align	4
_____105: .asciz "\n"
.align	4
_____106: .asciz "Static Float sf_y: "
.align	4
_____107: .asciz " (should print 34.30)"
.align	4
_____108: .asciz "\n"
.align	4
_____109: .asciz "Static Float sf_yy: "
.align	4
_____110: .asciz " (should print 43.40)"
.align	4
_____111: .asciz "\n"
.align	4
_____112: .asciz "Local Int li_x: "
.align	4
_____113: .asciz " (should print 56)"
.align	4
_____114: .asciz "\n"
.align	4
_____115: .asciz "Local Int li_xx: "
.align	4
_____116: .asciz " (should print 65)"
.align	4
_____117: .asciz "\n"
.align	4
_____118: .asciz "Local Float lf_y: "
.align	4
_____119: .asciz " (should print 56.50)"
.align	4
_____120: .asciz "\n"
.align	4
_____121: .asciz "Local Float lf_yy: "
.align	4
_____122: .asciz " (should print 65.60)"
.align	4
_____123: .asciz "\n"
.align	4
______125: .word	-1
______130: .word	1
______138: .word	-1
_____145: .asciz "Result of z: "
.align	4
_____146: .asciz " (should print 102)"
.align	4
_____147: .asciz "\n"
.align	4
______152: .word	0
_____157: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::gi_x
gi_x:	.skip	4
!CodeGen::doVarDecl::gi_xx
gi_xx:	.skip	4
!CodeGen::doVarDecl::gf_y
gf_y:	.skip	4
!CodeGen::doVarDecl::gf_yy
gf_yy:	.skip	4
!CodeGen::doVarDecl::si_x
_0si_x18staticFlag:	.skip	4
_0si_x18:	.skip	4
!CodeGen::doVarDecl::si_xx
_0si_xx111staticFlag:	.skip	4
_0si_xx111:	.skip	4
!CodeGen::doVarDecl::sf_y
_0sf_y115staticFlag:	.skip	4
_0sf_y115:	.skip	4
!CodeGen::doVarDecl::sf_yy
_0sf_yy119staticFlag:	.skip	4
_0sf_yy119:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	11,%l1
set	gi_x,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	22,%l1
set	gi_xx,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	gf_y,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______7,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	gf_yy,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_0si_x18staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____9 
 nop
set	33,%l1
set	_0si_x18,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	_0si_x18staticFlag, %l1
st	%l0, [%l1]
_____9:
set	_0si_xx111staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____12 
 nop
set	44,%l1
set	_0si_xx111,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	_0si_xx111staticFlag, %l1
st	%l0, [%l1]
_____12:
set	_0sf_y115staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____16 
 nop
set	______17,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	_0sf_y115,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	1, %l0
set	_0sf_y115staticFlag, %l1
st	%l0, [%l1]
_____16:
set	_0sf_yy119staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____20 
 nop
set	______21,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	_0sf_yy119,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	1, %l0
set	_0sf_yy119staticFlag, %l1
st	%l0, [%l1]
_____20:
_init_done:
set	55,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	66,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______27,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	gi_x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-28,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	gi_x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	gi_x,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	-1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	gi_xx,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-44,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	gi_xx,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	gi_xx,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	gf_y,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-56,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-52,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-60,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	gf_y,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	gf_y,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	-1,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	gf_yy,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-68,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-68,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-76,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	gf_yy,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	gf_yy,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_0si_x18,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-92,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_0si_x18,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_0si_x18,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	-1,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_0si_xx111,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l4
set	-104,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-108,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_0si_xx111,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_0si_xx111,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_0sf_y115,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-116,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-116,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-120,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-124,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	_0sf_y115,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-128,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_0sf_y115,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	-1,%l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_0sf_yy119,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-136,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-132,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-140,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	_0sf_yy119,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_0sf_yy119,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-152,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-156,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l4
set	-168,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-172,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-184,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-180,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-180,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-184,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-188,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-192,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-200,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-196,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-196,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-200,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-204,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-208,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	gi_x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	_strFmt,%o0
set	_____91,%o1
call printf
nop
set	gi_xx,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____92,%o1
call printf
nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
set	gf_y,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____95,%o1
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	gf_yy,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
set	_strFmt,%o0
set	_____99,%o1
call printf
nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	_0si_x18,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	_strFmt,%o0
set	_____102,%o1
call printf
nop
set	_strFmt,%o0
set	_____103,%o1
call printf
nop
set	_0si_xx111,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____104,%o1
call printf
nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
set	_strFmt,%o0
set	_____106,%o1
call printf
nop
set	_0sf_y115,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____107,%o1
call printf
nop
set	_strFmt,%o0
set	_____108,%o1
call printf
nop
set	_strFmt,%o0
set	_____109,%o1
call printf
nop
set	_0sf_yy119,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____110,%o1
call printf
nop
set	_strFmt,%o0
set	_____111,%o1
call printf
nop
set	_strFmt,%o0
set	_____112,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____113,%o1
call printf
nop
set	_strFmt,%o0
set	_____114,%o1
call printf
nop
set	_strFmt,%o0
set	_____115,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	_strFmt,%o0
set	_____118,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____119,%o1
call printf
nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
set	_strFmt,%o0
set	_____121,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____122,%o1
call printf
nop
set	_strFmt,%o0
set	_____123,%o1
call printf
nop
set	-1,%l1
set	-212,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	gi_x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l4
set	-216,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-220,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	gi_x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-224,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	gi_x,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_0si_x18,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-232,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-236,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_0si_x18,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-240,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-236,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_0si_x18,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-244,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-244,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-240,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-248,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-244,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-248,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-252,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-1,%l1
set	-256,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-260,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l4
set	-260,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-264,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-268,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-264,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-272,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-272,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-268,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-276,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-272,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-276,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-280,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-280,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-284,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____145,%o1
call printf
nop
set	-284,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____146,%o1
call printf
nop
set	_strFmt,%o0
set	_____147,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 284) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____153 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____154 
 nop
_____153:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____154:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____155 
 nop
ba  _____156 
 nop
_____155:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____157,%o1
call printf
nop
_____156:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

