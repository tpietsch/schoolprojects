.global	a, b, c, d, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______3: .word	1
______4: .word	1
______5: .word	0
______6: .word	1
______15: .word	0
______16: .word	1
_____17: .asciz "y: "
.align	4
_____20: .asciz "\n"
.align	4
______29: .word	0
______30: .word	1
_____31: .asciz "y: "
.align	4
_____34: .asciz "\n"
.align	4
______43: .word	1
_____44: .asciz "x: "
.align	4
_____47: .asciz "\n"
.align	4
______56: .word	0
______57: .word	1
_____58: .asciz "x: "
.align	4
_____61: .asciz "\n"
.align	4
______78: .word	0
______79: .word	1
_____80: .asciz "a: "
.align	4
_____83: .asciz "\n"
.align	4
______92: .word	0
______93: .word	1
______102: .word	0
______103: .word	1
_____104: .asciz "a: "
.align	4
_____107: .asciz "\n"
.align	4
______124: .word	0
_____125: .asciz "a: "
.align	4
_____128: .asciz "\n"
.align	4
______133: .word	0
_____138: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	4
!CodeGen::doVarDecl::b
b:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
!CodeGen::doVarDecl::d
d:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	0,%l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	b,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	d,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____7 
 nop
ba  _____9 
 nop
ba  _____8 
 nop
_____7:
_____8:
set	1,%l1
cmp	%l1, %g0
be  _____10 
 nop
ba  _____9 
 nop
ba  _____11 
 nop
_____10:
_____11:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____14 
 nop
_____9:
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____14:
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____18 
 nop
set	_boolF,%o1
ba  _____19 
 nop
_____18:
set	_boolT,%o1
_____19:
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____21 
 nop
ba  _____23 
 nop
ba  _____22 
 nop
_____21:
_____22:
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____24 
 nop
ba  _____23 
 nop
ba  _____25 
 nop
_____24:
_____25:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____28 
 nop
_____23:
set	1,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____28:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____32 
 nop
set	_boolF,%o1
ba  _____33 
 nop
_____32:
set	_boolT,%o1
_____33:
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____35 
 nop
ba  _____37 
 nop
ba  _____36 
 nop
_____35:
_____36:
set	0,%l1
cmp	%l1, %g0
be  _____38 
 nop
ba  _____37 
 nop
ba  _____39 
 nop
_____38:
_____39:
ba  _____42 
 nop
_____37:
_____42:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____45 
 nop
set	_boolF,%o1
ba  _____46 
 nop
_____45:
set	_boolT,%o1
_____46:
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____48 
 nop
ba  _____50 
 nop
ba  _____49 
 nop
_____48:
_____49:
set	0,%l1
cmp	%l1, %g0
be  _____51 
 nop
ba  _____50 
 nop
ba  _____52 
 nop
_____51:
_____52:
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____55 
 nop
_____50:
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____55:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____59 
 nop
set	_boolF,%o1
ba  _____60 
 nop
_____59:
set	_boolT,%o1
_____60:
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____62 
 nop
ba  _____64 
 nop
ba  _____63 
 nop
_____62:
_____63:
set	0,%l1
cmp	%l1, %g0
be  _____65 
 nop
ba  _____64 
 nop
ba  _____66 
 nop
_____65:
_____66:
ba  _____69 
 nop
_____64:
_____69:
set	1,%l1
cmp	%l1, %g0
be  _____70 
 nop
ba  _____72 
 nop
ba  _____71 
 nop
_____70:
_____71:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____73 
 nop
ba  _____72 
 nop
ba  _____74 
 nop
_____73:
_____74:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____77 
 nop
_____72:
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____77:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____80,%o1
call printf
nop
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____81 
 nop
set	_boolF,%o1
ba  _____82 
 nop
_____81:
set	_boolT,%o1
_____82:
call printf
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____84 
 nop
ba  _____86 
 nop
ba  _____85 
 nop
_____84:
_____85:
set	0,%l1
cmp	%l1, %g0
be  _____87 
 nop
ba  _____86 
 nop
ba  _____88 
 nop
_____87:
_____88:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____91 
 nop
_____86:
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____91:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____94 
 nop
ba  _____96 
 nop
ba  _____95 
 nop
_____94:
_____95:
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____97 
 nop
ba  _____96 
 nop
ba  _____98 
 nop
_____97:
_____98:
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____101 
 nop
_____96:
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____101:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____104,%o1
call printf
nop
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____105 
 nop
set	_boolF,%o1
ba  _____106 
 nop
_____105:
set	_boolT,%o1
_____106:
call printf
nop
set	_strFmt,%o0
set	_____107,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____108 
 nop
ba  _____110 
 nop
ba  _____109 
 nop
_____108:
_____109:
set	0,%l1
cmp	%l1, %g0
be  _____111 
 nop
ba  _____110 
 nop
ba  _____112 
 nop
_____111:
_____112:
ba  _____115 
 nop
_____110:
_____115:
set	0,%l1
cmp	%l1, %g0
be  _____116 
 nop
ba  _____118 
 nop
ba  _____117 
 nop
_____116:
_____117:
set	0,%l1
cmp	%l1, %g0
be  _____119 
 nop
ba  _____118 
 nop
ba  _____120 
 nop
_____119:
_____120:
ba  _____123 
 nop
_____118:
_____123:
set	0,%l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____125,%o1
call printf
nop
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____126 
 nop
set	_boolF,%o1
ba  _____127 
 nop
_____126:
set	_boolT,%o1
_____127:
call printf
nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 32) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____134 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____135 
 nop
_____134:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____135:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____136 
 nop
ba  _____137 
 nop
_____136:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____138,%o1
call printf
nop
_____137:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

