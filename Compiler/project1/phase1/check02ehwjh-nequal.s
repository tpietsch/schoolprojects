.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	2
_____4: .single	0r1.1
______5: .single	0r1.1
_____6: .single	0r2.2
______7: .single	0r2.2
_____8: .single	0r1.0
______9: .single	0r1.0
______10: .word	1
______11: .word	0
_____12: .asciz "1 != 2 = (true, "
.align	4
_____13: .asciz "true"
.align	4
_____14: .asciz ")"
.align	4
_____15: .asciz "\n"
.align	4
_____16: .asciz "1 != 1 = (false, "
.align	4
_____17: .asciz "false"
.align	4
_____18: .asciz ")"
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "i1 != 2 = (true, "
.align	4
______24: .word	2
_____29: .asciz ")"
.align	4
_____30: .asciz "\n"
.align	4
_____31: .asciz "i1 != 1 = (false, "
.align	4
______35: .word	1
_____40: .asciz ")"
.align	4
_____41: .asciz "\n"
.align	4
_____42: .asciz "1 != i2 = (true, "
.align	4
______45: .word	1
_____51: .asciz ")"
.align	4
_____52: .asciz "\n"
.align	4
_____53: .asciz "2 != i2 = (false, "
.align	4
______56: .word	2
_____62: .asciz ")"
.align	4
_____63: .asciz "\n"
.align	4
_____64: .asciz "i1 != i2 = (true, "
.align	4
_____72: .asciz ")"
.align	4
_____73: .asciz "\n"
.align	4
_____74: .asciz "i1 != i1 = (false, "
.align	4
_____82: .asciz ")"
.align	4
_____83: .asciz "\n"
.align	4
_____84: .asciz "1.1 != 2.2 = (true, "
.align	4
_____85: .single	0r1.1
_____86: .single	0r2.1
_____87: .asciz "true"
.align	4
_____88: .asciz ")"
.align	4
_____89: .asciz "\n"
.align	4
_____90: .asciz "1.1 != 1.1 = (false, "
.align	4
_____91: .single	0r1.1
_____92: .single	0r1.1
_____93: .asciz "false"
.align	4
_____94: .asciz ")"
.align	4
_____95: .asciz "\n"
.align	4
_____96: .asciz "f1 !=  2.2 = (true, "
.align	4
_____97: .single	0r2.2
______101: .single	0r2.2
_____106: .asciz ")"
.align	4
_____107: .asciz "\n"
.align	4
_____108: .asciz "f1 != 1.1 = (false, "
.align	4
_____109: .single	0r1.1
______113: .single	0r1.1
_____118: .asciz ")"
.align	4
_____119: .asciz "\n"
.align	4
_____120: .asciz "1.1 != f2  = (true, "
.align	4
_____121: .single	0r1.1
______124: .single	0r1.1
_____130: .asciz ")"
.align	4
_____131: .asciz "\n"
.align	4
_____132: .asciz "1.1 != f1  = (false, "
.align	4
_____133: .single	0r1.1
______136: .single	0r1.1
_____142: .asciz ")"
.align	4
_____143: .asciz "\n"
.align	4
_____144: .asciz " f1 != f2 = (true, "
.align	4
_____152: .asciz ")"
.align	4
_____153: .asciz "\n"
.align	4
_____154: .asciz " f1 != f1 = (false, "
.align	4
_____162: .asciz ")"
.align	4
_____163: .asciz "\n"
.align	4
_____164: .asciz " true != false = (true, "
.align	4
_____165: .asciz "true"
.align	4
_____166: .asciz ")"
.align	4
_____167: .asciz "\n"
.align	4
_____168: .asciz " true != true = (false, "
.align	4
_____169: .asciz "false"
.align	4
_____170: .asciz ")"
.align	4
_____171: .asciz "\n"
.align	4
_____172: .asciz " _true != false = (true, "
.align	4
_____178: .asciz ")"
.align	4
_____179: .asciz "\n"
.align	4
_____180: .asciz " _true != true = (false, "
.align	4
_____186: .asciz ")"
.align	4
_____187: .asciz "\n"
.align	4
_____188: .asciz " true != _false = (true, "
.align	4
_____194: .asciz ")"
.align	4
_____195: .asciz "\n"
.align	4
_____196: .asciz " true != _true = (false, "
.align	4
_____202: .asciz ")"
.align	4
_____203: .asciz "\n"
.align	4
_____204: .asciz " _true != _false = (true, "
.align	4
_____210: .asciz ")"
.align	4
_____211: .asciz "\n"
.align	4
_____212: .asciz " _true != _true = (false, "
.align	4
_____218: .asciz ")"
.align	4
_____219: .asciz "\n"
.align	4
_____220: .asciz " 1 != 1.1 = (true, "
.align	4
_____221: .single	0r1.1
_____222: .asciz "true"
.align	4
_____223: .asciz ")"
.align	4
_____224: .asciz "\n"
.align	4
_____225: .asciz " 1 != 1.0 = (false, "
.align	4
_____226: .single	0r1.0
_____227: .asciz "false"
.align	4
_____228: .asciz ")"
.align	4
_____229: .asciz "\n"
.align	4
_____230: .asciz " i1 != 1.1 = (true, "
.align	4
_____231: .single	0r1.1
______235: .single	0r1.1
_____240: .asciz ")"
.align	4
_____241: .asciz "\n"
.align	4
_____242: .asciz " i1 != 1.0 = (false, "
.align	4
_____243: .single	0r1.0
______247: .single	0r1.0
_____252: .asciz ")"
.align	4
_____253: .asciz "\n"
.align	4
_____254: .asciz " 1 != f1 = (true, "
.align	4
______257: .word	1
_____263: .asciz ")"
.align	4
_____264: .asciz "\n"
.align	4
_____265: .asciz " 1 != f0 = (false, "
.align	4
______268: .word	1
_____274: .asciz ")"
.align	4
_____275: .asciz "\n"
.align	4
_____276: .asciz " i1 != f1 = (true, "
.align	4
_____284: .asciz ")"
.align	4
_____285: .asciz "\n"
.align	4
_____286: .asciz " i1 != f0 = (false, "
.align	4
_____294: .asciz ")"
.align	4
_____295: .asciz "\n"
.align	4
_____296: .asciz " 1.1 != 1 = (true, "
.align	4
_____297: .single	0r1.1
_____298: .asciz "true"
.align	4
_____299: .asciz ")"
.align	4
_____300: .asciz "\n"
.align	4
_____301: .asciz " 1.0 != 1 = (false, "
.align	4
_____302: .single	0r1.0
_____303: .asciz "false"
.align	4
_____304: .asciz ")"
.align	4
_____305: .asciz "\n"
.align	4
_____306: .asciz " 1.1 != i1 = (true, "
.align	4
_____307: .single	0r1.1
______310: .single	0r1.1
_____316: .asciz ")"
.align	4
_____317: .asciz "\n"
.align	4
_____318: .asciz " 1.0 != i1 = (false, "
.align	4
_____319: .single	0r1.0
______322: .single	0r1.0
_____328: .asciz ")"
.align	4
_____329: .asciz "\n"
.align	4
_____330: .asciz " f1 != 1 = (true, "
.align	4
______334: .word	1
_____339: .asciz ")"
.align	4
_____340: .asciz "\n"
.align	4
_____341: .asciz " f0 != 1 = (false, "
.align	4
______345: .word	1
_____350: .asciz ")"
.align	4
_____351: .asciz "\n"
.align	4
_____352: .asciz " f1 != i1 = (true, "
.align	4
_____360: .asciz ")"
.align	4
_____361: .asciz "\n"
.align	4
_____362: .asciz " f0 != i1 = (false, "
.align	4
_____370: .asciz ")"
.align	4
_____371: .asciz "\n"
.align	4
______376: .word	0
_____381: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______7,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______9,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	2,%l2
cmp	%l1, %l2
bne  _____25 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____26 
 nop
_____25:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____26:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____27 
 nop
set	_boolF,%o1
ba  _____28 
 nop
_____27:
set	_boolT,%o1
_____28:
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
bne  _____36 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____37 
 nop
_____36:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____37:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____38 
 nop
set	_boolF,%o1
ba  _____39 
 nop
_____38:
set	_boolT,%o1
_____39:
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____47 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____48 
 nop
_____47:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____48:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____49 
 nop
set	_boolF,%o1
ba  _____50 
 nop
_____49:
set	_boolT,%o1
_____50:
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	2,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____58 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____59 
 nop
_____58:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____59:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____60 
 nop
set	_boolF,%o1
ba  _____61 
 nop
_____60:
set	_boolT,%o1
_____61:
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	0, %l0
set	-80,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____68 
 nop
set	-80,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____69 
 nop
_____68:
set	1,%l3
set	-80,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____69:
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____70 
 nop
set	_boolF,%o1
ba  _____71 
 nop
_____70:
set	_boolT,%o1
_____71:
call printf
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
set	0, %l0
set	-92,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____78 
 nop
set	-92,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____79 
 nop
_____78:
set	1,%l3
set	-92,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____79:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____80 
 nop
set	_boolF,%o1
ba  _____81 
 nop
_____80:
set	_boolT,%o1
_____81:
call printf
nop
set	_strFmt,%o0
set	_____82,%o1
call printf
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
set	_strFmt,%o0
set	_____95,%o1
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______101,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-112,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____102 
 nop
set	-104,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____103 
 nop
_____102:
set	1,%l3
set	-104,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____103:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____104 
 nop
set	_boolF,%o1
ba  _____105 
 nop
_____104:
set	_boolT,%o1
_____105:
call printf
nop
set	_strFmt,%o0
set	_____106,%o1
call printf
nop
set	_strFmt,%o0
set	_____107,%o1
call printf
nop
set	_strFmt,%o0
set	_____108,%o1
call printf
nop
set	0, %l0
set	-116,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______113,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-124,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____114 
 nop
set	-116,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____115 
 nop
_____114:
set	1,%l3
set	-116,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____115:
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____116 
 nop
set	_boolF,%o1
ba  _____117 
 nop
_____116:
set	_boolT,%o1
_____117:
call printf
nop
set	_strFmt,%o0
set	_____118,%o1
call printf
nop
set	_strFmt,%o0
set	_____119,%o1
call printf
nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
set	0, %l0
set	-128,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	______124,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-136,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____126 
 nop
set	-128,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____127 
 nop
_____126:
set	1,%l3
set	-128,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____127:
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____128 
 nop
set	_boolF,%o1
ba  _____129 
 nop
_____128:
set	_boolT,%o1
_____129:
call printf
nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
set	_strFmt,%o0
set	_____132,%o1
call printf
nop
set	0, %l0
set	-140,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	______136,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-148,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____138 
 nop
set	-140,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____139 
 nop
_____138:
set	1,%l3
set	-140,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____139:
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____140 
 nop
set	_boolF,%o1
ba  _____141 
 nop
_____140:
set	_boolT,%o1
_____141:
call printf
nop
set	_strFmt,%o0
set	_____142,%o1
call printf
nop
set	_strFmt,%o0
set	_____143,%o1
call printf
nop
set	_strFmt,%o0
set	_____144,%o1
call printf
nop
set	0, %l0
set	-152,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-160,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____148 
 nop
set	-152,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____149 
 nop
_____148:
set	1,%l3
set	-152,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____149:
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____150 
 nop
set	_boolF,%o1
ba  _____151 
 nop
_____150:
set	_boolT,%o1
_____151:
call printf
nop
set	_strFmt,%o0
set	_____152,%o1
call printf
nop
set	_strFmt,%o0
set	_____153,%o1
call printf
nop
set	_strFmt,%o0
set	_____154,%o1
call printf
nop
set	0, %l0
set	-164,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-168,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-168,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-172,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____158 
 nop
set	-164,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____159 
 nop
_____158:
set	1,%l3
set	-164,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____159:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____160 
 nop
set	_boolF,%o1
ba  _____161 
 nop
_____160:
set	_boolT,%o1
_____161:
call printf
nop
set	_strFmt,%o0
set	_____162,%o1
call printf
nop
set	_strFmt,%o0
set	_____163,%o1
call printf
nop
set	_strFmt,%o0
set	_____164,%o1
call printf
nop
set	_strFmt,%o0
set	_____165,%o1
call printf
nop
set	_strFmt,%o0
set	_____166,%o1
call printf
nop
set	_strFmt,%o0
set	_____167,%o1
call printf
nop
set	_strFmt,%o0
set	_____168,%o1
call printf
nop
set	_strFmt,%o0
set	_____169,%o1
call printf
nop
set	_strFmt,%o0
set	_____170,%o1
call printf
nop
set	_strFmt,%o0
set	_____171,%o1
call printf
nop
set	_strFmt,%o0
set	_____172,%o1
call printf
nop
set	0, %l0
set	-176,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bne  _____174 
 nop
set	-176,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____175 
 nop
_____174:
set	1,%l3
set	-176,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____175:
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____176 
 nop
set	_boolF,%o1
ba  _____177 
 nop
_____176:
set	_boolT,%o1
_____177:
call printf
nop
set	_strFmt,%o0
set	_____178,%o1
call printf
nop
set	_strFmt,%o0
set	_____179,%o1
call printf
nop
set	_strFmt,%o0
set	_____180,%o1
call printf
nop
set	0, %l0
set	-180,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
bne  _____182 
 nop
set	-180,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____183 
 nop
_____182:
set	1,%l3
set	-180,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____183:
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____184 
 nop
set	_boolF,%o1
ba  _____185 
 nop
_____184:
set	_boolT,%o1
_____185:
call printf
nop
set	_strFmt,%o0
set	_____186,%o1
call printf
nop
set	_strFmt,%o0
set	_____187,%o1
call printf
nop
set	_strFmt,%o0
set	_____188,%o1
call printf
nop
set	0, %l0
set	-184,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____190 
 nop
set	-184,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____191 
 nop
_____190:
set	1,%l3
set	-184,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____191:
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____192 
 nop
set	_boolF,%o1
ba  _____193 
 nop
_____192:
set	_boolT,%o1
_____193:
call printf
nop
set	_strFmt,%o0
set	_____194,%o1
call printf
nop
set	_strFmt,%o0
set	_____195,%o1
call printf
nop
set	_strFmt,%o0
set	_____196,%o1
call printf
nop
set	0, %l0
set	-188,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____198 
 nop
set	-188,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____199 
 nop
_____198:
set	1,%l3
set	-188,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____199:
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____200 
 nop
set	_boolF,%o1
ba  _____201 
 nop
_____200:
set	_boolT,%o1
_____201:
call printf
nop
set	_strFmt,%o0
set	_____202,%o1
call printf
nop
set	_strFmt,%o0
set	_____203,%o1
call printf
nop
set	_strFmt,%o0
set	_____204,%o1
call printf
nop
set	0, %l0
set	-192,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____206 
 nop
set	-192,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____207 
 nop
_____206:
set	1,%l3
set	-192,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____207:
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____208 
 nop
set	_boolF,%o1
ba  _____209 
 nop
_____208:
set	_boolT,%o1
_____209:
call printf
nop
set	_strFmt,%o0
set	_____210,%o1
call printf
nop
set	_strFmt,%o0
set	_____211,%o1
call printf
nop
set	_strFmt,%o0
set	_____212,%o1
call printf
nop
set	0, %l0
set	-196,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____214 
 nop
set	-196,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____215 
 nop
_____214:
set	1,%l3
set	-196,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____215:
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____216 
 nop
set	_boolF,%o1
ba  _____217 
 nop
_____216:
set	_boolT,%o1
_____217:
call printf
nop
set	_strFmt,%o0
set	_____218,%o1
call printf
nop
set	_strFmt,%o0
set	_____219,%o1
call printf
nop
set	_strFmt,%o0
set	_____220,%o1
call printf
nop
set	_strFmt,%o0
set	_____222,%o1
call printf
nop
set	_strFmt,%o0
set	_____223,%o1
call printf
nop
set	_strFmt,%o0
set	_____224,%o1
call printf
nop
set	_strFmt,%o0
set	_____225,%o1
call printf
nop
set	_strFmt,%o0
set	_____227,%o1
call printf
nop
set	_strFmt,%o0
set	_____228,%o1
call printf
nop
set	_strFmt,%o0
set	_____229,%o1
call printf
nop
set	_strFmt,%o0
set	_____230,%o1
call printf
nop
set	0, %l0
set	-200,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______235,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-208,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-204,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-204,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____236 
 nop
set	-200,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____237 
 nop
_____236:
set	1,%l3
set	-200,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____237:
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____238 
 nop
set	_boolF,%o1
ba  _____239 
 nop
_____238:
set	_boolT,%o1
_____239:
call printf
nop
set	_strFmt,%o0
set	_____240,%o1
call printf
nop
set	_strFmt,%o0
set	_____241,%o1
call printf
nop
set	_strFmt,%o0
set	_____242,%o1
call printf
nop
set	0, %l0
set	-212,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______247,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-220,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-216,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-216,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-216,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____248 
 nop
set	-212,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____249 
 nop
_____248:
set	1,%l3
set	-212,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____249:
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____250 
 nop
set	_boolF,%o1
ba  _____251 
 nop
_____250:
set	_boolT,%o1
_____251:
call printf
nop
set	_strFmt,%o0
set	_____252,%o1
call printf
nop
set	_strFmt,%o0
set	_____253,%o1
call printf
nop
set	_strFmt,%o0
set	_____254,%o1
call printf
nop
set	0, %l0
set	-224,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-232,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-228,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-232,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____259 
 nop
set	-224,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____260 
 nop
_____259:
set	1,%l3
set	-224,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____260:
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____261 
 nop
set	_boolF,%o1
ba  _____262 
 nop
_____261:
set	_boolT,%o1
_____262:
call printf
nop
set	_strFmt,%o0
set	_____263,%o1
call printf
nop
set	_strFmt,%o0
set	_____264,%o1
call printf
nop
set	_strFmt,%o0
set	_____265,%o1
call printf
nop
set	0, %l0
set	-236,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-240,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-244,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-240,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-240,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-240,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-244,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____270 
 nop
set	-236,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____271 
 nop
_____270:
set	1,%l3
set	-236,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____271:
set	-236,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____272 
 nop
set	_boolF,%o1
ba  _____273 
 nop
_____272:
set	_boolT,%o1
_____273:
call printf
nop
set	_strFmt,%o0
set	_____274,%o1
call printf
nop
set	_strFmt,%o0
set	_____275,%o1
call printf
nop
set	_strFmt,%o0
set	_____276,%o1
call printf
nop
set	0, %l0
set	-248,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-252,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-252,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-256,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-252,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-252,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-256,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____280 
 nop
set	-248,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____281 
 nop
_____280:
set	1,%l3
set	-248,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____281:
set	-248,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____282 
 nop
set	_boolF,%o1
ba  _____283 
 nop
_____282:
set	_boolT,%o1
_____283:
call printf
nop
set	_strFmt,%o0
set	_____284,%o1
call printf
nop
set	_strFmt,%o0
set	_____285,%o1
call printf
nop
set	_strFmt,%o0
set	_____286,%o1
call printf
nop
set	0, %l0
set	-260,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-264,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-264,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-268,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-264,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-264,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-264,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-268,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____290 
 nop
set	-260,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____291 
 nop
_____290:
set	1,%l3
set	-260,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____291:
set	-260,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____292 
 nop
set	_boolF,%o1
ba  _____293 
 nop
_____292:
set	_boolT,%o1
_____293:
call printf
nop
set	_strFmt,%o0
set	_____294,%o1
call printf
nop
set	_strFmt,%o0
set	_____295,%o1
call printf
nop
set	_strFmt,%o0
set	_____296,%o1
call printf
nop
set	_strFmt,%o0
set	_____298,%o1
call printf
nop
set	_strFmt,%o0
set	_____299,%o1
call printf
nop
set	_strFmt,%o0
set	_____300,%o1
call printf
nop
set	_strFmt,%o0
set	_____301,%o1
call printf
nop
set	_strFmt,%o0
set	_____303,%o1
call printf
nop
set	_strFmt,%o0
set	_____304,%o1
call printf
nop
set	_strFmt,%o0
set	_____305,%o1
call printf
nop
set	_strFmt,%o0
set	_____306,%o1
call printf
nop
set	0, %l0
set	-272,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	______310,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-276,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-280,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-280,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-280,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-276,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-280,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____312 
 nop
set	-272,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____313 
 nop
_____312:
set	1,%l3
set	-272,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____313:
set	-272,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____314 
 nop
set	_boolF,%o1
ba  _____315 
 nop
_____314:
set	_boolT,%o1
_____315:
call printf
nop
set	_strFmt,%o0
set	_____316,%o1
call printf
nop
set	_strFmt,%o0
set	_____317,%o1
call printf
nop
set	_strFmt,%o0
set	_____318,%o1
call printf
nop
set	0, %l0
set	-284,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	______322,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-288,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-292,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-292,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-292,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-288,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-292,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____324 
 nop
set	-284,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____325 
 nop
_____324:
set	1,%l3
set	-284,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____325:
set	-284,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____326 
 nop
set	_boolF,%o1
ba  _____327 
 nop
_____326:
set	_boolT,%o1
_____327:
call printf
nop
set	_strFmt,%o0
set	_____328,%o1
call printf
nop
set	_strFmt,%o0
set	_____329,%o1
call printf
nop
set	_strFmt,%o0
set	_____330,%o1
call printf
nop
set	0, %l0
set	-296,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-300,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-300,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	-304,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-304,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-304,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-300,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-304,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____335 
 nop
set	-296,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____336 
 nop
_____335:
set	1,%l3
set	-296,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____336:
set	-296,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____337 
 nop
set	_boolF,%o1
ba  _____338 
 nop
_____337:
set	_boolT,%o1
_____338:
call printf
nop
set	_strFmt,%o0
set	_____339,%o1
call printf
nop
set	_strFmt,%o0
set	_____340,%o1
call printf
nop
set	_strFmt,%o0
set	_____341,%o1
call printf
nop
set	0, %l0
set	-308,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-312,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-312,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	-316,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-316,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-316,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-312,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-316,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____346 
 nop
set	-308,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____347 
 nop
_____346:
set	1,%l3
set	-308,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____347:
set	-308,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____348 
 nop
set	_boolF,%o1
ba  _____349 
 nop
_____348:
set	_boolT,%o1
_____349:
call printf
nop
set	_strFmt,%o0
set	_____350,%o1
call printf
nop
set	_strFmt,%o0
set	_____351,%o1
call printf
nop
set	_strFmt,%o0
set	_____352,%o1
call printf
nop
set	0, %l0
set	-320,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-324,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-324,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-328,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-328,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-328,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-324,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-328,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____356 
 nop
set	-320,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____357 
 nop
_____356:
set	1,%l3
set	-320,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____357:
set	-320,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____358 
 nop
set	_boolF,%o1
ba  _____359 
 nop
_____358:
set	_boolT,%o1
_____359:
call printf
nop
set	_strFmt,%o0
set	_____360,%o1
call printf
nop
set	_strFmt,%o0
set	_____361,%o1
call printf
nop
set	_strFmt,%o0
set	_____362,%o1
call printf
nop
set	0, %l0
set	-332,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-336,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-336,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-340,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-340,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-340,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-336,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-340,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____366 
 nop
set	-332,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____367 
 nop
_____366:
set	1,%l3
set	-332,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____367:
set	-332,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____368 
 nop
set	_boolF,%o1
ba  _____369 
 nop
_____368:
set	_boolT,%o1
_____369:
call printf
nop
set	_strFmt,%o0
set	_____370,%o1
call printf
nop
set	_strFmt,%o0
set	_____371,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 340) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____377 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____378 
 nop
_____377:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____378:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____379 
 nop
ba  _____380 
 nop
_____379:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____381,%o1
call printf
nop
_____380:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

