.global	_0bGlobalUnit1, _0bGlobalInit1, _0bGlobalInit21, _0bGlobalConst1, _0bGlobalConst21,main
.section	".data"
.align	4
______0: .word	1

______1: .word	0

______2: .word	0

______3: .word	0

______8: .word	1

______15: .word	0

_____16: .asciz "false"
.align	4
______17: .word	0

_____18: .asciz "false"
.align	4
______19: .word	0

_____20: .asciz "false"
.align	4
_____21: .asciz "true"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::bGlobalUnit
_0bGlobalUnit1:	.skip	4
!CodeGen::doVarDecl::bGlobalInit
_0bGlobalInit1:	.skip	4
!CodeGen::doVarDecl::bGlobalInit2
_0bGlobalInit21:	.skip	4
!CodeGen::doVarDecl::bGlobalConst
_0bGlobalConst1:	.skip	4
!CodeGen::doVarDecl::bGlobalConst2
_0bGlobalConst21:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld[%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
! Doing a vardeclass 
! Generating STO 
! declaring a variable true 
! Doing a vardeclass 
! Generating STO 
! doConst ( true,______0 ) 
! sto  true (______0) 
! STO's name is( true, %g0,______0 ) 
set	1,%l1
! STO's store name is( bGlobalInit, %g0,_0bGlobalInit1 ) 
set	_0bGlobalInit1,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
! declaring a variable bGlobalInit 
! Doing a vardeclass 
! Generating STO 
! STO's name is( bGlobalInit, %g0,_0bGlobalInit1 ) 
set	_0bGlobalInit1,%i0
add 	%g0, %i0, %i0
ld	[%i0], %l1
! STO's store name is( bGlobalInit2, %g0,_0bGlobalInit21 ) 
set	_0bGlobalInit21,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
! declaring a variable false 
! Doing a vardeclass 
! Generating STO 
! doConst ( false,______1 ) 
! sto  false (______1) 
! STO's name is( false, %g0,______1 ) 
set	0,%l1
! STO's store name is( bGlobalConst, %g0,_0bGlobalConst1 ) 
set	_0bGlobalConst1,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
! declaring a variable bGlobalConst 
! Doing a vardeclass 
! Generating STO 
! doConst ( bGlobalConst,______2 ) 
! sto  bGlobalConst (______2) 
! STO's name is( bGlobalConst, %g0,______2 ) 
set	0,%l1
! STO's store name is( bGlobalConst2, %g0,_0bGlobalConst21 ) 
set	_0bGlobalConst21,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
_init_done:
! handleFuncParams()  
! handleFuncParams() end 
! Doing a vardeclass 
! Generating STO 
! Doing a vardeclass 
! Generating STO 
! doConst ( false,______3 ) 
! sto  false (______3) 
! STO's name is( false, %g0,______3 ) 
set	0,%l1
! STO's store name is( bLocalUnit, %fp,-4 ) 
set	-4,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
! STO's name is( bLocalUnit, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _________4 
 nop
set	_boolF,%o1
ba  __________5 
 nop
_________4:
set	_boolT,%o1
__________5:
call printf
nop
! STO's name is( bLocalUnit, %fp,-4 ) 
set	-4,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( bLocalUnit2, %fp,-8 ) 
set	-8,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
! STO's name is( bLocalUnit2, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  ___________6 
 nop
set	_boolF,%o1
ba  ____________7 
 nop
___________6:
set	_boolT,%o1
____________7:
call printf
nop
! declaring a variable true 
! Doing a vardeclass 
! Generating STO 
! doConst ( true,______8 ) 
! sto  true (______8) 
! STO's name is( true, %g0,______8 ) 
set	1,%l1
! STO's store name is( bLocalInit, %fp,-12 ) 
set	-12,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
! STO's name is( bLocalInit, %fp,-12 ) 
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  ______________9 
 nop
set	_boolF,%o1
ba  _______________10 
 nop
______________9:
set	_boolT,%o1
_______________10:
call printf
nop
! declaring a variable bLocalInit 
! Doing a vardeclass 
! Generating STO 
! STO's name is( bLocalInit, %fp,-12 ) 
set	-12,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( bLocalInit2, %fp,-16 ) 
set	-16,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
! STO's name is( bLocalInit2, %fp,-16 ) 
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  ________________11 
 nop
set	_boolF,%o1
ba  _________________12 
 nop
________________11:
set	_boolT,%o1
_________________12:
call printf
nop
! declaring a variable bGlobalInit 
! Doing a vardeclass 
! Generating STO 
! STO's name is( bGlobalInit, %g0,_0bGlobalInit1 ) 
set	_0bGlobalInit1,%i0
add 	%g0, %i0, %i0
ld	[%i0], %l1
! STO's store name is( bLocalInit3, %fp,-20 ) 
set	-20,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
! STO's name is( bLocalInit3, %fp,-20 ) 
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  __________________13 
 nop
set	_boolF,%o1
ba  ___________________14 
 nop
__________________13:
set	_boolT,%o1
___________________14:
call printf
nop
! declaring a variable false 
! Doing a vardeclass 
! Generating STO 
! doConst ( false,______15 ) 
! sto  false (______15) 
! STO's name is( false, %g0,______15 ) 
set	0,%l1
! STO's store name is( bLocalConst, %fp,-24 ) 
set	-24,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
! declaring a variable bLocalConst 
! Doing a vardeclass 
! Generating STO 
! doConst ( bLocalConst,______17 ) 
! sto  bLocalConst (______17) 
! STO's name is( bLocalConst, %g0,______17 ) 
set	0,%l1
! STO's store name is( bLocalConst2, %fp,-28 ) 
set	-28,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
! declaring a variable bGlobalConst 
! Doing a vardeclass 
! Generating STO 
! doConst ( bGlobalConst,______19 ) 
! sto  bGlobalConst (______19) 
! STO's name is( bGlobalConst, %g0,______19 ) 
set	0,%l1
! STO's store name is( bLocalConst3, %fp,-32 ) 
set	-32,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
ret
restore

SAVE.main = -(92 + 32) & -8

