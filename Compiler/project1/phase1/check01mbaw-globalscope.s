.global	_0ix1,main
.section	".data"
.align	4
______0: .word	6

______1: .word	5

.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::ix
_0ix1:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld[%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
! declaring a variable 6 
! Doing a vardeclass 
! Generating STO 
! doConst ( 6,______0 ) 
! sto  6 (______0) 
! STO's name is( 6, %g0,______0 ) 
set	6,%l1
! STO's store name is( ix, %g0,_0ix1 ) 
set	_0ix1,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
_init_done:
! handleFuncParams()  
! handleFuncParams() end 
! declaring a variable 5 
! Doing a vardeclass 
! Generating STO 
! doConst ( 5,______1 ) 
! sto  5 (______1) 
! STO's name is( 5, %g0,______1 ) 
set	5,%l1
! STO's store name is( ix, %fp,-4 ) 
set	-4,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! declaring a variable ix 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ix, %fp,-4 ) 
set	-4,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( a, %fp,-8 ) 
set	-8,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_intFmt,%o0
! STO's name is( a, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
ret
restore

SAVE.main = -(92 + 8) & -8

