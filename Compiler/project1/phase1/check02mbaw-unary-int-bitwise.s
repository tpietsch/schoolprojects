.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	5
______3: .word	10
______6: .word	5
_____8: .asciz " == 5"
.align	4
_____9: .asciz "\n"
.align	4
_____13: .asciz " == 0"
.align	4
_____14: .asciz "\n"
.align	4
_____15: .asciz "10 & 10: 10 == "
.align	4
_____16: .asciz "10"
.align	4
_____17: .asciz "\n"
.align	4
______20: .word	0
_____22: .asciz " == 5"
.align	4
_____23: .asciz "\n"
.align	4
_____27: .asciz " == 15"
.align	4
_____28: .asciz "\n"
.align	4
_____29: .asciz "0"
.align	4
_____30: .asciz " == 0"
.align	4
_____31: .asciz "\n"
.align	4
______34: .word	0
_____36: .asciz " == 5"
.align	4
_____37: .asciz "\n"
.align	4
_____41: .asciz " == 15"
.align	4
_____42: .asciz "\n"
.align	4
_____43: .asciz "10"
.align	4
_____44: .asciz " == 10"
.align	4
_____45: .asciz "\n"
.align	4
______50: .word	0
_____55: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	5,%l5
and	%l4, %l5, %l5
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
and	%l4, %l5, %l5
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	0,%l5
xor	%l4, %l5, %l5
set	-44,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-56,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	0,%l5
or	%l4, %l5, %l5
set	-68,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	-80,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 80) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____51 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____52 
 nop
_____51:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____52:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____53 
 nop
ba  _____54 
 nop
_____53:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
_____54:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

