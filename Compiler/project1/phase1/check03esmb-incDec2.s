.global	z, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	10
______3: .word	4
______4: .word	5
______6: .word	1
______11: .word	1
_____18: .asciz "RESULT: "
.align	4
_____19: .asciz " (should be 10.00)"
.align	4
_____20: .asciz "\n"
.align	4
______22: .word	-1
______27: .word	-1
_____34: .asciz "RESULT: "
.align	4
_____35: .asciz " (should be 0)"
.align	4
_____36: .asciz "\n"
.align	4
______38: .word	1
______43: .word	-1
_____50: .asciz "RESULT: "
.align	4
_____51: .asciz " (should be 13)"
.align	4
_____52: .asciz "\n"
.align	4
______57: .word	0
_____62: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::z
z:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	10,%l1
set	z,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	4,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-8,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-28,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-36,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-44,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-44,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-52,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-56,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	-1,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l4
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-68,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-76,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-80,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-84,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-92,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-92,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-92,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-96,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-100,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-56,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1,%l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	z,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-108,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-112,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	z,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	z,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	-1,%l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l4
set	-124,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-128,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-140,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-144,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 144) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____58 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____59 
 nop
_____58:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____59:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____60 
 nop
ba  _____61 
 nop
_____60:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
_____61:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

