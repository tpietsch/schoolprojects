.global	,main
.section	".data"
.align	4
______0: .word	5

______1: .word	10

______3: .word	0

_____9: .asciz " == 5"
.align	4
______11: .word	0

_____17: .asciz " == -15"
.align	4
______19: .word	0

_____25: .asciz " == -50"
.align	4
______27: .word	0

_____33: .asciz " == -2"
.align	4
______35: .word	0

_____41: .asciz " == 0"
.align	4
_____45: .asciz " == 15"
.align	4
_____49: .asciz " == -5"
.align	4
_____53: .asciz " == 50"
.align	4
_____57: .asciz " == 2"
.align	4
______58: .word	10

______60: .word	0

______62: .word	10

_____64: .asciz "-10"
.align	4
_____65: .asciz " == -10"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld[%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
! handleFuncParams()  
! handleFuncParams() end 
! declaring a variable 5 
! Doing a vardeclass 
! Generating STO 
! doConst ( 5,______0 ) 
! sto  5 (______0) 
! STO's name is( 5, %g0,______0 ) 
set	5,%l1
! STO's store name is( ia, %fp,-4 ) 
set	-4,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! declaring a variable 10 
! Doing a vardeclass 
! Generating STO 
! doConst ( 10,______1 ) 
! sto  10 (______1) 
! STO's name is( 10, %g0,______1 ) 
set	10,%l1
! STO's store name is( ib, %fp,-8 ) 
set	-8,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! typeCoercion 
! a is Const 
! declaring a variable 0 
! Doing a vardeclass 
! Generating STO 
! doConst ( 0,______3 ) 
! sto  0 (______3) 
! STO's name is( 0, %g0,______3 ) 
set	0,%l1
! STO's store name is( 02, %fp,-12 ) 
set	-12,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ia ( %fp,-4, 0 ) ,_____4 ( %fp,-16, 0 ) ) 
! STO's name is( ia, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____4, %fp,-16 ) 
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! IntType@e94e92 IntType@12558d6 
! declaring a variable -ia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( 0, %fp,-12 ) 
set	0,%l4
! STO's name is( _____4, %fp,-16 ) 
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
! STO's store name is( _____5, %fp,-20 ) 
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
! typeCoercion 
! declaring a variable _____5 
! Doing a vardeclass 
! Generating STO 
! STO's name is( _____5, %fp,-20 ) 
set	-20,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( _____56, %fp,-24 ) 
set	-24,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! loadStoreAss (_____5 ( %fp,-20, 0 ) ,_____56 ( %fp,-24, 0 ) ) 
! STO's name is( _____5, %fp,-20 ) 
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____56, %fp,-24 ) 
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ib ( %fp,-8, 0 ) ,_____7 ( %fp,-28, 0 ) ) 
! STO's name is( ib, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____7, %fp,-28 ) 
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! IntType@30e280 IntType@16672d6 
! declaring a variable _____5+ib 
! Doing a vardeclass 
! Generating STO 
! STO's name is( _____56, %fp,-24 ) 
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
! STO's name is( _____7, %fp,-28 ) 
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
! STO's store name is( _____8, %fp,-32 ) 
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_intFmt,%o0
! STO's name is( _____8, %fp,-32 ) 
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
! typeCoercion 
! a is Const 
! declaring a variable 0 
! Doing a vardeclass 
! Generating STO 
! doConst ( 0,______11 ) 
! sto  0 (______11) 
! STO's name is( 0, %g0,______11 ) 
set	0,%l1
! STO's store name is( 010, %fp,-36 ) 
set	-36,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ia ( %fp,-4, 0 ) ,_____12 ( %fp,-40, 0 ) ) 
! STO's name is( ia, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____12, %fp,-40 ) 
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! IntType@1415de6 IntType@7bd9f2 
! declaring a variable -ia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( 0, %fp,-36 ) 
set	0,%l4
! STO's name is( _____12, %fp,-40 ) 
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
! STO's store name is( _____13, %fp,-44 ) 
set	-44,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
! typeCoercion 
! declaring a variable _____13 
! Doing a vardeclass 
! Generating STO 
! STO's name is( _____13, %fp,-44 ) 
set	-44,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( _____1314, %fp,-48 ) 
set	-48,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! loadStoreAss (_____13 ( %fp,-44, 0 ) ,_____1314 ( %fp,-48, 0 ) ) 
! STO's name is( _____13, %fp,-44 ) 
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____1314, %fp,-48 ) 
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ib ( %fp,-8, 0 ) ,_____15 ( %fp,-52, 0 ) ) 
! STO's name is( ib, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____15, %fp,-52 ) 
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! IntType@1e893df IntType@443226 
! declaring a variable _____13-ib 
! Doing a vardeclass 
! Generating STO 
! STO's name is( _____1314, %fp,-48 ) 
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
! STO's name is( _____15, %fp,-52 ) 
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
! STO's store name is( _____16, %fp,-56 ) 
set	-56,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_intFmt,%o0
! STO's name is( _____16, %fp,-56 ) 
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
! typeCoercion 
! a is Const 
! declaring a variable 0 
! Doing a vardeclass 
! Generating STO 
! doConst ( 0,______19 ) 
! sto  0 (______19) 
! STO's name is( 0, %g0,______19 ) 
set	0,%l1
! STO's store name is( 018, %fp,-60 ) 
set	-60,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ia ( %fp,-4, 0 ) ,_____20 ( %fp,-64, 0 ) ) 
! STO's name is( ia, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____20, %fp,-64 ) 
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! IntType@1662dc8 IntType@147c5fc 
! declaring a variable -ia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( 0, %fp,-60 ) 
set	0,%l4
! STO's name is( _____20, %fp,-64 ) 
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
! STO's store name is( _____21, %fp,-68 ) 
set	-68,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
! typeCoercion 
! declaring a variable _____21 
! Doing a vardeclass 
! Generating STO 
! STO's name is( _____21, %fp,-68 ) 
set	-68,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( _____2122, %fp,-72 ) 
set	-72,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! loadStoreAss (_____21 ( %fp,-68, 0 ) ,_____2122 ( %fp,-72, 0 ) ) 
! STO's name is( _____21, %fp,-68 ) 
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____2122, %fp,-72 ) 
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ib ( %fp,-8, 0 ) ,_____23 ( %fp,-76, 0 ) ) 
! STO's name is( ib, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____23, %fp,-76 ) 
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! declaring a variable _____21*ib 
! Doing a vardeclass 
! Generating STO 
! STO's name is( _____2122, %fp,-72 ) 
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
! STO's name is( _____23, %fp,-76 ) 
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
! STO's store name is( _____24, %fp,-80 ) 
set	-80,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	_intFmt,%o0
! STO's name is( _____24, %fp,-80 ) 
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
! typeCoercion 
! a is Const 
! declaring a variable 0 
! Doing a vardeclass 
! Generating STO 
! doConst ( 0,______27 ) 
! sto  0 (______27) 
! STO's name is( 0, %g0,______27 ) 
set	0,%l1
! STO's store name is( 026, %fp,-84 ) 
set	-84,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ia ( %fp,-4, 0 ) ,_____28 ( %fp,-88, 0 ) ) 
! STO's name is( ia, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____28, %fp,-88 ) 
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! IntType@64dc11 IntType@1ac1fe4 
! declaring a variable -ia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( 0, %fp,-84 ) 
set	0,%l4
! STO's name is( _____28, %fp,-88 ) 
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
! STO's store name is( _____29, %fp,-92 ) 
set	-92,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
! typeCoercion 
! declaring a variable ib 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ib, %fp,-8 ) 
set	-8,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( ib30, %fp,-96 ) 
set	-96,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! loadStoreAss (ib ( %fp,-8, 0 ) ,ib30 ( %fp,-96, 0 ) ) 
! STO's name is( ib, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( ib30, %fp,-96 ) 
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (_____29 ( %fp,-92, 0 ) ,_____31 ( %fp,-100, 0 ) ) 
! STO's name is( _____29, %fp,-92 ) 
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____31, %fp,-100 ) 
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! declaring a variable ib/_____29 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ib30, %fp,-96 ) 
set	-96,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
! STO's name is( _____31, %fp,-100 ) 
set	-100,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
! STO's store name is( _____32, %fp,-104 ) 
set	-104,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	_intFmt,%o0
! STO's name is( _____32, %fp,-104 ) 
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
! typeCoercion 
! a is Const 
! declaring a variable 0 
! Doing a vardeclass 
! Generating STO 
! doConst ( 0,______35 ) 
! sto  0 (______35) 
! STO's name is( 0, %g0,______35 ) 
set	0,%l1
! STO's store name is( 034, %fp,-108 ) 
set	-108,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ia ( %fp,-4, 0 ) ,_____36 ( %fp,-112, 0 ) ) 
! STO's name is( ia, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____36, %fp,-112 ) 
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! IntType@17f1ba3 IntType@1ef8cf3 
! declaring a variable -ia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( 0, %fp,-108 ) 
set	0,%l4
! STO's name is( _____36, %fp,-112 ) 
set	-112,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
! STO's store name is( _____37, %fp,-116 ) 
set	-116,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
! typeCoercion 
! declaring a variable ib 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ib, %fp,-8 ) 
set	-8,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( ib38, %fp,-120 ) 
set	-120,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! loadStoreAss (ib ( %fp,-8, 0 ) ,ib38 ( %fp,-120, 0 ) ) 
! STO's name is( ib, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( ib38, %fp,-120 ) 
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (_____37 ( %fp,-116, 0 ) ,_____39 ( %fp,-124, 0 ) ) 
! STO's name is( _____37, %fp,-116 ) 
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____39, %fp,-124 ) 
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! declaring a variable ib%_____37 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ib38, %fp,-120 ) 
set	-120,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
! STO's name is( _____39, %fp,-124 ) 
set	-124,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
! STO's store name is( _____40, %fp,-128 ) 
set	-128,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	_intFmt,%o0
! STO's name is( _____40, %fp,-128 ) 
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
! typeCoercion 
! declaring a variable ia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ia, %fp,-4 ) 
set	-4,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( ia42, %fp,-132 ) 
set	-132,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! loadStoreAss (ia ( %fp,-4, 0 ) ,ia42 ( %fp,-132, 0 ) ) 
! STO's name is( ia, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( ia42, %fp,-132 ) 
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ib ( %fp,-8, 0 ) ,_____43 ( %fp,-136, 0 ) ) 
! STO's name is( ib, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____43, %fp,-136 ) 
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! IntType@ecd7e IntType@1d520c4 
! declaring a variable ia+ib 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ia42, %fp,-132 ) 
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
! STO's name is( _____43, %fp,-136 ) 
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
! STO's store name is( _____44, %fp,-140 ) 
set	-140,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_intFmt,%o0
! STO's name is( _____44, %fp,-140 ) 
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
! typeCoercion 
! declaring a variable ia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ia, %fp,-4 ) 
set	-4,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( ia46, %fp,-144 ) 
set	-144,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! loadStoreAss (ia ( %fp,-4, 0 ) ,ia46 ( %fp,-144, 0 ) ) 
! STO's name is( ia, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( ia46, %fp,-144 ) 
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ib ( %fp,-8, 0 ) ,_____47 ( %fp,-148, 0 ) ) 
! STO's name is( ib, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____47, %fp,-148 ) 
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! IntType@1764be1 IntType@16fd0b7 
! declaring a variable ia-ib 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ia46, %fp,-144 ) 
set	-144,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
! STO's name is( _____47, %fp,-148 ) 
set	-148,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
! STO's store name is( _____48, %fp,-152 ) 
set	-152,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_intFmt,%o0
! STO's name is( _____48, %fp,-152 ) 
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
! typeCoercion 
! declaring a variable ia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ia, %fp,-4 ) 
set	-4,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( ia50, %fp,-156 ) 
set	-156,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! loadStoreAss (ia ( %fp,-4, 0 ) ,ia50 ( %fp,-156, 0 ) ) 
! STO's name is( ia, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( ia50, %fp,-156 ) 
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ib ( %fp,-8, 0 ) ,_____51 ( %fp,-160, 0 ) ) 
! STO's name is( ib, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____51, %fp,-160 ) 
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! declaring a variable ia*ib 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ia50, %fp,-156 ) 
set	-156,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
! STO's name is( _____51, %fp,-160 ) 
set	-160,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
! STO's store name is( _____52, %fp,-164 ) 
set	-164,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	_intFmt,%o0
! STO's name is( _____52, %fp,-164 ) 
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
! typeCoercion 
! declaring a variable ib 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ib, %fp,-8 ) 
set	-8,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( ib54, %fp,-168 ) 
set	-168,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! loadStoreAss (ib ( %fp,-8, 0 ) ,ib54 ( %fp,-168, 0 ) ) 
! STO's name is( ib, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( ib54, %fp,-168 ) 
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! b is Const 
! Doing a vardeclass 
! Generating STO 
! loadStoreAss (ia ( %fp,-4, 0 ) ,_____55 ( %fp,-172, 0 ) ) 
! STO's name is( ia, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
! STO's store name is( _____55, %fp,-172 ) 
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! declaring a variable ib/ia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( ib54, %fp,-168 ) 
set	-168,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
! STO's name is( _____55, %fp,-172 ) 
set	-172,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
! STO's store name is( _____56, %fp,-176 ) 
set	-176,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	_intFmt,%o0
! STO's name is( _____56, %fp,-176 ) 
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
! declaring a variable 10 
! Doing a vardeclass 
! Generating STO 
! doConst ( 10,______58 ) 
! sto  10 (______58) 
! STO's name is( 10, %g0,______58 ) 
set	10,%l1
! STO's store name is( cia, %fp,-180 ) 
set	-180,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! typeCoercion 
! a is Const 
! declaring a variable 0 
! Doing a vardeclass 
! Generating STO 
! doConst ( 0,______60 ) 
! sto  0 (______60) 
! STO's name is( 0, %g0,______60 ) 
set	0,%l1
! STO's store name is( 059, %fp,-184 ) 
set	-184,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! b is Const 
! declaring a variable cia 
! Doing a vardeclass 
! Generating STO 
! doConst ( cia,______62 ) 
! sto  cia (______62) 
! STO's name is( cia, %g0,______62 ) 
set	10,%l1
! STO's store name is( cia61, %fp,-188 ) 
set	-188,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! IntType@1cb25f1 IntType@2808b3 
! declaring a variable -cia 
! Doing a vardeclass 
! Generating STO 
! STO's name is( 0, %fp,-184 ) 
set	0,%l4
! STO's name is( cia, %fp,-188 ) 
set	10,%l5
sub	%l4, %l5, %l5
! STO's store name is( _____63, %fp,-192 ) 
set	-192,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
ret
restore

SAVE.main = -(92 + 192) & -8

