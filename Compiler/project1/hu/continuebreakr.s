.global	A.foo, A.foo1, A.foo2, A.foo3, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______8: .word	10
______11: .word	0
______17: .word	100
_____20: .asciz " should not print this "
.align	4
_____21: .asciz "\n"
.align	4
______23: .word	1
______27: .word	0
______33: .word	10
______36: .word	0
______42: .word	100
______46: .word	1
_____50: .asciz " should not print this "
.align	4
_____51: .asciz "\n"
.align	4
_____52: .asciz " should not print this "
.align	4
_____53: .asciz "\n"
.align	4
______55: .word	1
______59: .word	0
______65: .word	10
______84: .word	0
______90: .word	100
______93: .word	0
______99: .word	100
______102: .word	0
______108: .word	100
______111: .word	0
______117: .word	100
______121: .word	1
_____125: .asciz " should not print "
.align	4
_____126: .asciz "\n"
.align	4
______128: .word	1
_____132: .asciz "shoudl not print"
.align	4
_____133: .asciz "\n"
.align	4
______135: .word	1
______142: .word	0
_____147: .asciz "this should print"
.align	4
_____148: .asciz "\n"
.align	4
______150: .word	1
_____154: .asciz "this should not print"
.align	4
_____155: .asciz "\n"
.align	4
_____162: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____169: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____176: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____183: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______188: .word	0
_____193: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	A.foo, %l0
add	%g0, %l0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	0, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-8, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____159 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____160 
 nop
_____159:
add	%sp, -0, %sp
_____160:
_____158:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____161 
 nop
set	_strFmt,%o0
set	_____162,%o1
call printf
nop
set	1, %o0
call exit
nop
_____161:
call %l1
nop
set	A.foo1, %l0
add	%g0, %l0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	0, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-16, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____166 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____167 
 nop
_____166:
add	%sp, -0, %sp
_____167:
_____165:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____168 
 nop
set	_strFmt,%o0
set	_____169,%o1
call printf
nop
set	1, %o0
call exit
nop
_____168:
call %l1
nop
set	A.foo2, %l0
add	%g0, %l0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	0, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____173 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____174 
 nop
_____173:
add	%sp, -0, %sp
_____174:
_____172:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____175 
 nop
set	_strFmt,%o0
set	_____176,%o1
call printf
nop
set	1, %o0
call exit
nop
_____175:
call %l1
nop
set	A.foo3, %l0
add	%g0, %l0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-32, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	0, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-32, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____180 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____181 
 nop
_____180:
add	%sp, -0, %sp
_____181:
_____179:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____182 
 nop
set	_strFmt,%o0
set	_____183,%o1
call printf
nop
set	1, %o0
call exit
nop
_____182:
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 32) & -8

A.foo: 
set SAVE.A.foo, %g1
save %sp, %g1, %sp
set	0,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____3,_____4) 
_____3:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	10,%l2
cmp	%l1, %l2
bl  _____9 
 nop
set	-8,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____10 
 nop
_____9:
set	1,%l3
set	-8,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____10:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____4 
 nop
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____12,_____13) 
_____12:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	100,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	100,%l2
cmp	%l1, %l2
bl  _____18 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____19 
 nop
_____18:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____19:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____13 
 nop
ba  _____13 
 nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
ba  _____12 
 nop
_____13:
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-44,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____3 
 nop
_____4:
ret
restore

SAVE.A.foo = -(92 + 48) & -8

A.foo1: 
set SAVE.A.foo1, %g1
save %sp, %g1, %sp
set	0,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____28,_____29) 
_____28:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	10,%l2
cmp	%l1, %l2
bl  _____34 
 nop
set	-8,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____35 
 nop
_____34:
set	1,%l3
set	-8,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____35:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____29 
 nop
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____37,_____38) 
_____37:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	100,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	100,%l2
cmp	%l1, %l2
bl  _____43 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____44 
 nop
_____43:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____44:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____38 
 nop
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-44,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____37 
 nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
ba  _____37 
 nop
_____38:
ba  _____29 
 nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	1,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-60,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____28 
 nop
_____29:
ret
restore

SAVE.A.foo1 = -(92 + 64) & -8

A.foo2: 
set SAVE.A.foo2, %g1
save %sp, %g1, %sp
set	0,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____60,_____61) 
_____60:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	10,%l2
cmp	%l1, %l2
bl  _____66 
 nop
set	-8,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____67 
 nop
_____66:
set	1,%l3
set	-8,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____67:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____61 
 nop
! beginning of while loop(_____68,_____69) 
_____68:
set	1,%l1
cmp	%l1, %g0
be  _____69 
 nop
! beginning of while loop(_____70,_____71) 
_____70:
set	1,%l1
cmp	%l1, %g0
be  _____71 
 nop
! beginning of while loop(_____72,_____73) 
_____72:
set	1,%l1
cmp	%l1, %g0
be  _____73 
 nop
! beginning of while loop(_____74,_____75) 
_____74:
set	1,%l1
cmp	%l1, %g0
be  _____75 
 nop
! beginning of while loop(_____76,_____77) 
_____76:
set	1,%l1
cmp	%l1, %g0
be  _____77 
 nop
! beginning of while loop(_____78,_____79) 
_____78:
set	1,%l1
cmp	%l1, %g0
be  _____79 
 nop
! beginning of while loop(_____80,_____81) 
_____80:
set	1,%l1
cmp	%l1, %g0
be  _____81 
 nop
set	1,%l1
cmp	%l1, %g0
be  _____82 
 nop
ba  _____81 
 nop
ba  _____83 
 nop
_____82:
_____83:
ba  _____80 
 nop
_____81:
ba  _____79 
 nop
ba  _____78 
 nop
_____79:
ba  _____77 
 nop
ba  _____76 
 nop
_____77:
ba  _____75 
 nop
ba  _____74 
 nop
_____75:
ba  _____73 
 nop
ba  _____72 
 nop
_____73:
ba  _____71 
 nop
ba  _____70 
 nop
_____71:
ba  _____69 
 nop
ba  _____68 
 nop
_____69:
ba  _____61 
 nop
ba  _____60 
 nop
_____61:
ret
restore

SAVE.A.foo2 = -(92 + 16) & -8

A.foo3: 
set SAVE.A.foo3, %g1
save %sp, %g1, %sp
set	0,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____85,_____86) 
_____85:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	100,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	100,%l2
cmp	%l1, %l2
bl  _____91 
 nop
set	-8,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____92 
 nop
_____91:
set	1,%l3
set	-8,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____92:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____86 
 nop
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____94,_____95) 
_____94:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	100,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	100,%l2
cmp	%l1, %l2
bl  _____100 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____101 
 nop
_____100:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____101:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____95 
 nop
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____103,_____104) 
_____103:
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	100,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	100,%l2
cmp	%l1, %l2
bl  _____109 
 nop
set	-40,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____110 
 nop
_____109:
set	1,%l3
set	-40,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____110:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____104 
 nop
set	0,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____112,_____113) 
_____112:
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	100,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	100,%l2
cmp	%l1, %l2
bl  _____118 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____119 
 nop
_____118:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____119:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____113 
 nop
set	1,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-76,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____112 
 nop
set	_strFmt,%o0
set	_____125,%o1
call printf
nop
set	_strFmt,%o0
set	_____126,%o1
call printf
nop
ba  _____112 
 nop
_____113:
set	1,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-92,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____103 
 nop
set	_strFmt,%o0
set	_____132,%o1
call printf
nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
ba  _____103 
 nop
_____104:
set	1,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-104,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-108,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-116,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bg  _____143 
 nop
set	-116,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____144 
 nop
_____143:
set	1,%l3
set	-116,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____144:
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____145 
 nop
ba  _____94 
 nop
ba  _____146 
 nop
_____145:
_____146:
set	_strFmt,%o0
set	_____147,%o1
call printf
nop
set	_strFmt,%o0
set	_____148,%o1
call printf
nop
ba  _____94 
 nop
_____95:
set	1,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-136,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____85 
 nop
set	_strFmt,%o0
set	_____154,%o1
call printf
nop
set	_strFmt,%o0
set	_____155,%o1
call printf
nop
ba  _____85 
 nop
_____86:
ret
restore

SAVE.A.foo3 = -(92 + 140) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____189 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____190 
 nop
_____189:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____190:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____191 
 nop
ba  _____192 
 nop
_____191:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____193,%o1
call printf
nop
_____192:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

