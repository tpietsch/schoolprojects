.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	35
______3: .word	1
______7: .word	0
______13: .word	0
______14: .word	1
______19: .word	4
______25: .word	12
_____31: .asciz "Index value of "
.align	4
_____32: .asciz " is outside legal range [0,3).\n"
.align	4
______37: .word	0
_____42: .asciz "Index value of "
.align	4
_____43: .asciz " is outside legal range [0,3).\n"
.align	4
_____48: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______52: .word	0
______53: .word	1
_____56: .asciz "fa = false, "
.align	4
_____59: .asciz "\n"
.align	4
______60: .word	0
______65: .word	4
______71: .word	12
_____77: .asciz "Index value of "
.align	4
_____78: .asciz " is outside legal range [0,3).\n"
.align	4
______83: .word	0
_____88: .asciz "Index value of "
.align	4
_____89: .asciz " is outside legal range [0,3).\n"
.align	4
_____94: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______98: .word	0
______99: .word	1
_____102: .asciz " fa = false, "
.align	4
_____105: .asciz "\n"
.align	4
______106: .word	1
______110: .word	1
______116: .word	0
______117: .word	1
______121: .word	1
______127: .word	0
______128: .word	1
_____131: .asciz "fa = true, "
.align	4
_____134: .asciz "\n"
.align	4
______135: .word	4
______136: .word	1
______140: .word	1
______146: .word	0
______147: .word	1
______158: .word	1
______164: .word	0
______165: .word	1
______169: .word	1
______172: .word	0
______178: .word	0
______179: .word	1
______186: .word	0
______187: .word	1
_____190: .asciz "fa = false, "
.align	4
_____193: .asciz "\n"
.align	4
______194: .word	1
______199: .word	4
______205: .word	12
_____211: .asciz "Index value of "
.align	4
_____212: .asciz " is outside legal range [0,3).\n"
.align	4
______217: .word	0
_____222: .asciz "Index value of "
.align	4
_____223: .asciz " is outside legal range [0,3).\n"
.align	4
_____228: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______232: .word	0
______233: .word	1
_____236: .asciz "fa = true "
.align	4
_____237: .asciz "\n"
.align	4
______242: .word	0
_____247: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	35,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____4 
 nop
ba  _____6 
 nop
ba  _____5 
 nop
_____4:
_____5:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____8 
 nop
ba  _____6 
 nop
ba  _____9 
 nop
_____8:
_____9:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____12 
 nop
_____6:
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____12:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____15 
 nop
ba  _____17 
 nop
ba  _____16 
 nop
_____15:
_____16:
set	4,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-36,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____27 
 nop
set	-40,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____28 
 nop
_____27:
set	1,%l3
set	-40,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____28:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____29 
 nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____30 
 nop
_____29:
_____30:
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____38 
 nop
set	-52,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____39 
 nop
_____38:
set	1,%l3
set	-52,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____39:
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____40 
 nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____41 
 nop
_____40:
_____41:
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____47 
 nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	1, %o0
call exit
nop
_____47:
ld	[%l0], %l1
cmp	%l1, %g0
be  _____45 
 nop
ba  _____17 
 nop
ba  _____46 
 nop
_____45:
_____46:
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____51 
 nop
_____17:
set	1,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____51:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____54 
 nop
ba  _____55 
 nop
_____54:
_____55:
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____57 
 nop
set	_boolF,%o1
ba  _____58 
 nop
_____57:
set	_boolT,%o1
_____58:
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____61 
 nop
ba  _____63 
 nop
ba  _____62 
 nop
_____61:
_____62:
set	4,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-80,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____73 
 nop
set	-84,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____74 
 nop
_____73:
set	1,%l3
set	-84,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____74:
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____75 
 nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____76 
 nop
_____75:
_____76:
set	0, %l0
set	-96,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____84 
 nop
set	-96,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____85 
 nop
_____84:
set	1,%l3
set	-96,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____85:
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____86 
 nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____87 
 nop
_____86:
_____87:
set	0, %l0
set	-108,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____93 
 nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
set	1, %o0
call exit
nop
_____93:
ld	[%l0], %l1
cmp	%l1, %g0
be  _____91 
 nop
ba  _____63 
 nop
ba  _____92 
 nop
_____91:
_____92:
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____97 
 nop
_____63:
set	1,%l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____97:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____100 
 nop
ba  _____101 
 nop
_____100:
_____101:
set	_strFmt,%o0
set	_____102,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____103 
 nop
set	_boolF,%o1
ba  _____104 
 nop
_____103:
set	_boolT,%o1
_____104:
call printf
nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____107 
 nop
ba  _____109 
 nop
ba  _____108 
 nop
_____107:
_____108:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____111 
 nop
ba  _____109 
 nop
ba  _____112 
 nop
_____111:
_____112:
set	0, %l0
set	-116,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____115 
 nop
_____109:
set	1,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____115:
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____118 
 nop
ba  _____120 
 nop
ba  _____119 
 nop
_____118:
_____119:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____122 
 nop
ba  _____120 
 nop
ba  _____123 
 nop
_____122:
_____123:
set	0, %l0
set	-120,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____126 
 nop
_____120:
set	1,%l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____126:
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____129 
 nop
ba  _____130 
 nop
_____129:
_____130:
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____132 
 nop
set	_boolF,%o1
ba  _____133 
 nop
_____132:
set	_boolT,%o1
_____133:
call printf
nop
set	_strFmt,%o0
set	_____134,%o1
call printf
nop
set	4,%l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____137 
 nop
ba  _____139 
 nop
ba  _____138 
 nop
_____137:
_____138:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____141 
 nop
ba  _____139 
 nop
ba  _____142 
 nop
_____141:
_____142:
set	0, %l0
set	-128,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____145 
 nop
_____139:
set	1,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____145:
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____148 
 nop
ba  _____150 
 nop
ba  _____149 
 nop
_____148:
_____149:
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-136,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bne  _____154 
 nop
set	-136,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____155 
 nop
_____154:
set	1,%l3
set	-136,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____155:
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____156 
 nop
set	1,%l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____157 
 nop
_____156:
_____157:
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____159 
 nop
ba  _____150 
 nop
ba  _____160 
 nop
_____159:
_____160:
set	0, %l0
set	-140,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____163 
 nop
_____150:
set	1,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____163:
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____166 
 nop
ba  _____168 
 nop
ba  _____167 
 nop
_____166:
_____167:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____170 
 nop
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____173 
 nop
ba  _____168 
 nop
ba  _____174 
 nop
_____173:
ba  _____174 
 nop
_____170:
_____174:
_____171:
set	0, %l0
set	-144,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____177 
 nop
_____168:
set	1,%l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____177:
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____180 
 nop
ba  _____182 
 nop
ba  _____181 
 nop
_____180:
_____181:
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____185 
 nop
_____182:
set	1,%l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____185:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____188 
 nop
ba  _____189 
 nop
_____188:
_____189:
set	_strFmt,%o0
set	_____190,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____191 
 nop
set	_boolF,%o1
ba  _____192 
 nop
_____191:
set	_boolT,%o1
_____192:
call printf
nop
set	_strFmt,%o0
set	_____193,%o1
call printf
nop
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____195 
 nop
ba  _____197 
 nop
ba  _____196 
 nop
_____195:
_____196:
set	4,%l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-156,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-160,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-164,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____207 
 nop
set	-164,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____208 
 nop
_____207:
set	1,%l3
set	-164,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____208:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____209 
 nop
set	_strFmt,%o0
set	_____211,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____212,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____210 
 nop
_____209:
_____210:
set	0, %l0
set	-176,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-184,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____218 
 nop
set	-176,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____219 
 nop
_____218:
set	1,%l3
set	-176,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____219:
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____220 
 nop
set	_strFmt,%o0
set	_____222,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____223,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____221 
 nop
_____220:
_____221:
set	0, %l0
set	-188,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-188,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____227 
 nop
set	_strFmt,%o0
set	_____228,%o1
call printf
nop
set	1, %o0
call exit
nop
_____227:
ld	[%l0], %l1
cmp	%l1, %g0
be  _____225 
 nop
ba  _____197 
 nop
ba  _____226 
 nop
_____225:
_____226:
set	0, %l0
set	-192,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____231 
 nop
_____197:
set	1,%l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____231:
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____234 
 nop
ba  _____235 
 nop
_____234:
_____235:
set	_strFmt,%o0
set	_____236,%o1
call printf
nop
set	_strFmt,%o0
set	_____237,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 192) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____243 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____244 
 nop
_____243:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____244:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____245 
 nop
ba  _____246 
 nop
_____245:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____247,%o1
call printf
nop
_____246:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

