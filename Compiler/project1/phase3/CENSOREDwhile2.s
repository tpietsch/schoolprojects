.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______8: .word	5
______11: .word	0
______17: .word	5
______21: .word	1
_____25: .asciz ","
.align	4
_____26: .asciz "\n"
.align	4
______28: .word	1
______36: .word	0
_____41: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____3,_____4) 
_____3:
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	5,%l2
cmp	%l1, %l2
bl  _____9 
 nop
set	-12,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____10 
 nop
_____9:
set	1,%l3
set	-12,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____10:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____4 
 nop
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____12,_____13) 
_____12:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	5,%l2
cmp	%l1, %l2
bl  _____18 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____19 
 nop
_____18:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____19:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____13 
 nop
set	1,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-48,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
ba  _____12 
 nop
_____13:
set	1,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-64,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____3 
 nop
_____4:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 68) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____37 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____38 
 nop
_____37:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____38:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____39 
 nop
ba  _____40 
 nop
_____39:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
_____40:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

