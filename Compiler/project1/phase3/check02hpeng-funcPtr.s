.global	foo, bar, __________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	1
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____10: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____12: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______14: .word	1
______16: .word	-1
_____19: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____23: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____25: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______27: .word	2
_____28: .single	0r0.2
______29: .single	0r0.2
_____30: .asciz "1: "
.align	4
_____36: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____37: .asciz "\n"
.align	4
_____38: .asciz "1.20: "
.align	4
_____39: .asciz "\n"
.align	4
_____40: .asciz "2: "
.align	4
_____46: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____47: .asciz "\n"
.align	4
_____48: .asciz "0.20: "
.align	4
_____49: .asciz "\n"
.align	4
______54: .word	0
_____59: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	______29,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	foo, %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____33 
 nop
mov	%l1, %o0
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
add	%sp, -0, %sp
ba  _____34 
 nop
_____33:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
_____34:
_____32:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____35 
 nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1, %o0
call exit
nop
_____35:
call %l1
nop
set	-24,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	bar, %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____43 
 nop
mov	%l1, %o0
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
add	%sp, -0, %sp
ba  _____44 
 nop
_____43:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
_____44:
_____42:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____45 
 nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	1, %o0
call exit
nop
_____45:
call %l1
nop
set	-32,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 32) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-8,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-16,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____9 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1, %o0
call exit
nop
_____9:
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____11 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1, %o0
call exit
nop
_____11:
st	%l1, [%l0]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%i0
ret
restore

ret
restore

SAVE.foo = -(92 + 24) & -8

bar: 
set SAVE.bar, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____18 
 nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	1, %o0
call exit
nop
_____18:
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-8,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-16,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____22 
 nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	1, %o0
call exit
nop
_____22:
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____24 
 nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
_____24:
st	%l1, [%l0]
set	2,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%i0
ret
restore

ret
restore

SAVE.bar = -(92 + 24) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____55 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____56 
 nop
_____55:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____56:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____57 
 nop
ba  _____58 
 nop
_____57:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
_____58:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

