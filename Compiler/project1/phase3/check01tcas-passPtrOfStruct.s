.global	MINI.foo, STR.foo, fooStr, printStr, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Hello from MINI"
.align	4
_____3: .asciz "\n"
.align	4
_____4: .asciz "Hello from STR"
.align	4
_____5: .asciz "\n"
.align	4
_____8: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______9: .word	123
_____11: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____14: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______15: .word	456
_____18: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____21: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______22: .word	1
_____24: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____25: .asciz "i  = "
.align	4
_____28: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____30: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____31: .asciz "\n"
.align	4
_____32: .asciz "f  = "
.align	4
_____35: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____37: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____38: .asciz "\n"
.align	4
_____39: .asciz "b  = "
.align	4
_____42: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____44: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____47: .asciz "\n"
.align	4
_____50: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____56: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____59: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____62: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____68: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____71: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______80: .word	0
_____85: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-16, %l1
add	%fp, %l1, %l1
set	12, %l2
add	%l2, %l1, %l1
set	-32,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	0, %o2
set	-16, %o1
add	%fp, %o1, %o1
set	-32, %o0
add	%fp, %o0, %o0
cmp	%o0, %g0
bne  _____70 
 nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	1, %o0
call exit
nop
_____70:
ld	[%o0], %o0
call memmove
nop
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16, %l1
add	%fp, %l1, %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	fooStr, %l1
call %l1
nop
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16, %l1
add	%fp, %l1, %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	printStr, %l1
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 40) & -8

MINI.foo: 
set SAVE.MINI.foo, %g1
save %sp, %g1, %sp
set	0,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
ret
restore

SAVE.MINI.foo = -(92 + 0) & -8

STR.foo: 
set SAVE.STR.foo, %g1
save %sp, %g1, %sp
set	-12,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
ret
restore

SAVE.STR.foo = -(92 + 12) & -8

fooStr: 
set SAVE.fooStr, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____7 
 nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	1, %o0
call exit
nop
_____7:
ld	[%l1], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-20,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	123,%l1
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____10 
 nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	1, %o0
call exit
nop
_____10:
st	%l1, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____13 
 nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	1, %o0
call exit
nop
_____13:
ld	[%l1], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4, %l2
add	%l2, %l1, %l1
set	-36,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	456,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-40,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____17 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
_____17:
st	%f1, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____20 
 nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	1, %o0
call exit
nop
_____20:
ld	[%l1], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8, %l2
add	%l2, %l1, %l1
set	-56,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	1,%l1
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____23 
 nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	1, %o0
call exit
nop
_____23:
st	%l1, [%l0]
ret
restore

SAVE.fooStr = -(92 + 56) & -8

printStr: 
set SAVE.printStr, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____27 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	1, %o0
call exit
nop
_____27:
ld	[%l1], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-20,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____29 
 nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
_____29:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____34 
 nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	1, %o0
call exit
nop
_____34:
ld	[%l1], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4, %l2
add	%l2, %l1, %l1
set	-36,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____36 
 nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	1, %o0
call exit
nop
_____36:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____41 
 nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	1, %o0
call exit
nop
_____41:
ld	[%l1], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8, %l2
add	%l2, %l1, %l1
set	-52,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____43 
 nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	1, %o0
call exit
nop
_____43:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____45 
 nop
set	_boolF,%o1
ba  _____46 
 nop
_____45:
set	_boolT,%o1
_____46:
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____49 
 nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	1, %o0
call exit
nop
_____49:
ld	[%l1], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	STR.foo, %l0
add	%g0, %l0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-76, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-68, %l1
add	%fp, %l1, %l1
ld	[%l1], %l1
st	%l1, [%l0]
set	-76, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____53 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____54 
 nop
_____53:
add	%sp, -0, %sp
_____54:
_____52:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____55 
 nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	1, %o0
call exit
nop
_____55:
call %l1
nop
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____58 
 nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	1, %o0
call exit
nop
_____58:
ld	[%l1], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12, %l2
add	%l2, %l1, %l1
set	-92,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-92, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____61 
 nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	1, %o0
call exit
nop
_____61:
ld	[%l1], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	MINI.foo, %l0
add	%g0, %l0, %l0
set	-100,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-100, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-92, %l1
add	%fp, %l1, %l1
ld	[%l1], %l1
st	%l1, [%l0]
set	-100, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____65 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____66 
 nop
_____65:
add	%sp, -0, %sp
_____66:
_____64:
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____67 
 nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	1, %o0
call exit
nop
_____67:
call %l1
nop
ret
restore

SAVE.printStr = -(92 + 100) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____81 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____82 
 nop
_____81:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____82:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____83 
 nop
ba  _____84 
 nop
_____83:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
_____84:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

