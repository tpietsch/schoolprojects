.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	4
______5: .word	0
______10: .word	12
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "0"
.align	4
_____18: .asciz " is outside legal range [0,3).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "0"
.align	4
_____30: .asciz " is outside legal range [0,3).\n"
.align	4
______32: .word	3
_____34: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______36: .word	4
______38: .word	0
______43: .word	12
_____49: .asciz "Index value of "
.align	4
_____50: .asciz "0"
.align	4
_____51: .asciz " is outside legal range [0,3).\n"
.align	4
______56: .word	0
_____61: .asciz "Index value of "
.align	4
_____62: .asciz "0"
.align	4
_____63: .asciz " is outside legal range [0,3).\n"
.align	4
_____67: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____68: .asciz "\n"
.align	4
______70: .word	4
______72: .word	0
_____76: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____77: .asciz "\n"
.align	4
______79: .word	4
______81: .word	1
_____85: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____86: .asciz "\n"
.align	4
______91: .word	0
_____96: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	4,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-24,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-40,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-40,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____33 
 nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	1, %o0
call exit
nop
_____33:
st	%l1, [%l0]
set	4,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-64,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____45 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____46 
 nop
_____45:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____46:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____47 
 nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____48 
 nop
_____47:
_____48:
set	0, %l0
set	-80,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____57 
 nop
set	-80,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____58 
 nop
_____57:
set	1,%l3
set	-80,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____58:
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____59 
 nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____60 
 nop
_____59:
_____60:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-96,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-92, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____66 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	1, %o0
call exit
nop
_____66:
ld	[%l1], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-100,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	4,%l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-112,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100, %l2
add	%fp, %l2, %l2
ld	[%l2], %l2
add	%l1, %l2, %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____75 
 nop
set	_strFmt,%o0
set	_____76,%o1
call printf
nop
set	1, %o0
call exit
nop
_____75:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
set	4,%l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-128,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100, %l2
add	%fp, %l2, %l2
ld	[%l2], %l2
add	%l1, %l2, %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____84 
 nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	1, %o0
call exit
nop
_____84:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 132) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____92 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____93 
 nop
_____92:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____93:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____94 
 nop
ba  _____95 
 nop
_____94:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
_____95:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

