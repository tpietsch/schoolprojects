.global	i1, i2, f1, f2, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	2
_____4: .single	0r1.0
______5: .single	0r1.0
_____6: .single	0r2.0
______7: .single	0r2.0
______8: .word	5
______12: .word	6
_____16: .asciz "***pppa: "
.align	4
_____18: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____20: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____22: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____23: .asciz "\n"
.align	4
_____24: .asciz "***pppb: "
.align	4
_____26: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____28: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____30: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____31: .asciz "\n"
.align	4
______36: .word	0
_____41: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::i1
i1:	.skip	4
!CodeGen::doVarDecl::i2
i2:	.skip	4
!CodeGen::doVarDecl::f1
f1:	.skip	4
!CodeGen::doVarDecl::f2
f2:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	i1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	i2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f1,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______7,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f2,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
_init_done:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4, %l1
add	%fp, %l1, %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12, %l1
add	%fp, %l1, %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20, %l1
add	%fp, %l1, %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-32,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-32, %l1
add	%fp, %l1, %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-40, %l1
add	%fp, %l1, %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-48, %l1
add	%fp, %l1, %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____17 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
_____17:
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____19 
 nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	1, %o0
call exit
nop
_____19:
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____21 
 nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	1, %o0
call exit
nop
_____21:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____25 
 nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	1, %o0
call exit
nop
_____25:
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____27 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	1, %o0
call exit
nop
_____27:
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____29 
 nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
_____29:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 56) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____37 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____38 
 nop
_____37:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____38:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____39 
 nop
ba  _____40 
 nop
_____39:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
_____40:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

