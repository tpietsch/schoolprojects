.global	MYS.foo, foo, fooref, __________.MEMLEAK,main
.section	".data"
.align	4
_____4: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____9: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____11: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____13: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______15: .word	3
_____21: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____23: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____25: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____26: .asciz "\n"
.align	4
______31: .word	0
_____36: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4, %l1
add	%fp, %l1, %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	foo, %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36, %l1
add	%fp, %l1, %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24, %l0
add	%fp, %l0, %l0
cmp	%l0, %g0
bne  _____20 
 nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	1, %o0
call exit
nop
_____20:
ld	[%l0], %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____18 
 nop
mov	%l1, %o0
set	3,%o1
add	%sp, -0, %sp
ba  _____19 
 nop
_____18:
set	3,%o0
add	%sp, -0, %sp
_____19:
_____17:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____22 
 nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	1, %o0
call exit
nop
_____22:
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____24 
 nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
_____24:
call %l1
nop
set	-44,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 44) & -8

MYS.foo: 
set SAVE.MYS.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____3 
 nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	1, %o0
call exit
nop
_____3:
ld	[%l1], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-12,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
st	%l1, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____8 
 nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	1, %o0
call exit
nop
_____8:
ld	[%l1], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-16,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-16,%l1
add 	%fp, %l1, %l1
ld	[%l1], %l0
cmp	%l0, %g0
bne  _____10 
 nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	1, %o0
call exit
nop
_____10:
ld	[%l0], %i0
ret
restore

ret
restore

SAVE.MYS.foo = -(92 + 16) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-4,%l1
add 	%fp, %l1, %l1
ld	[%l1], %i0
ret
restore

ret
restore

SAVE.foo = -(92 + 4) & -8

fooref: 
set SAVE.fooref, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-4, %i0
add	%fp, %i0, %i0
cmp	%i0, %g0
bne  _____12 
 nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	1, %o0
call exit
nop
_____12:
ld	[%i0], %i0
ret
restore

ret
restore

SAVE.fooref = -(92 + 4) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____32 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____33 
 nop
_____32:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____33:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____34 
 nop
ba  _____35 
 nop
_____34:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
_____35:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

