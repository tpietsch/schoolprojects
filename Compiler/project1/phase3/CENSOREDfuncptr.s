.global	MYS.foo, foo, __________.MEMLEAK,main
.section	".data"
.align	4
_____4: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____9: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____11: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______14: .word	1
______16: .word	4
______20: .word	5
_____31: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____32: .asciz "\n"
.align	4
_____37: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____38: .asciz "\n"
.align	4
_____45: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____46: .asciz "\n"
.align	4
______51: .word	0
_____56: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	4,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	5,%l2
cmp	%l1, %l2
bl  _____21 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____22 
 nop
_____21:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____22:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____23 
 nop
set	foo, %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
ba  _____24 
 nop
_____23:
set	MYS.foo, %l0
add	%g0, %l0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-44, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-16, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
set	-44, %l0
add	%fp, %l0, %l0
add	%l0, 4, %l0
ld	[%l0], %l0
st	%l0, [%l1]
_____24:
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____28 
 nop
mov	%l1, %o0
set	1,%o1
add	%sp, -0, %sp
ba  _____29 
 nop
_____28:
set	1,%o0
add	%sp, -0, %sp
_____29:
_____27:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____30 
 nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	1, %o0
call exit
nop
_____30:
call %l1
nop
set	-48,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	foo, %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____34 
 nop
mov	%l1, %o0
set	1,%o1
add	%sp, -0, %sp
ba  _____35 
 nop
_____34:
set	1,%o0
add	%sp, -0, %sp
_____35:
_____33:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____36 
 nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	1, %o0
call exit
nop
_____36:
call %l1
nop
set	-52,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	MYS.foo, %l0
add	%g0, %l0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-64, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-16, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
set	-64, %l0
add	%fp, %l0, %l0
add	%l0, 4, %l0
ld	[%l0], %l0
st	%l0, [%l1]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____42 
 nop
mov	%l1, %o0
set	1,%o1
add	%sp, -0, %sp
ba  _____43 
 nop
_____42:
set	1,%o0
add	%sp, -0, %sp
_____43:
_____41:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____44 
 nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	1, %o0
call exit
nop
_____44:
call %l1
nop
set	-68,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 68) & -8

MYS.foo: 
set SAVE.MYS.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____3 
 nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	1, %o0
call exit
nop
_____3:
ld	[%l1], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-12,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
st	%l1, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____8 
 nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	1, %o0
call exit
nop
_____8:
ld	[%l1], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-16,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-16,%l1
add 	%fp, %l1, %l1
ld	[%l1], %l0
cmp	%l0, %g0
bne  _____10 
 nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	1, %o0
call exit
nop
_____10:
ld	[%l0], %i0
ret
restore

ret
restore

SAVE.MYS.foo = -(92 + 16) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	1,%l5
add	%l4, %l5, %l5
set	-16,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-16,%l1
add 	%fp, %l1, %l1
ld	[%l1], %i0
ret
restore

ret
restore

SAVE.foo = -(92 + 16) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____52 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____53 
 nop
_____52:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____53:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____54 
 nop
ba  _____55 
 nop
_____54:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
_____55:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

