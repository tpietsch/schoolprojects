.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	4
______5: .word	1
______10: .word	12
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "1"
.align	4
_____18: .asciz " is outside legal range [0,3).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "1"
.align	4
_____30: .asciz " is outside legal range [0,3).\n"
.align	4
______33: .word	4
______35: .word	2
______40: .word	12
_____46: .asciz "Index value of "
.align	4
_____47: .asciz "2"
.align	4
_____48: .asciz " is outside legal range [0,3).\n"
.align	4
______53: .word	0
_____58: .asciz "Index value of "
.align	4
_____59: .asciz "2"
.align	4
_____60: .asciz " is outside legal range [0,3).\n"
.align	4
______62: .word	3
_____64: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____66: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____68: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______70: .word	4
______72: .word	1
______77: .word	12
_____83: .asciz "Index value of "
.align	4
_____84: .asciz "1"
.align	4
_____85: .asciz " is outside legal range [0,3).\n"
.align	4
______90: .word	0
_____95: .asciz "Index value of "
.align	4
_____96: .asciz "1"
.align	4
_____97: .asciz " is outside legal range [0,3).\n"
.align	4
_____100: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____101: .asciz " "
.align	4
______103: .word	4
______105: .word	2
______110: .word	12
_____116: .asciz "Index value of "
.align	4
_____117: .asciz "2"
.align	4
_____118: .asciz " is outside legal range [0,3).\n"
.align	4
______123: .word	0
_____128: .asciz "Index value of "
.align	4
_____129: .asciz "2"
.align	4
_____130: .asciz " is outside legal range [0,3).\n"
.align	4
_____133: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____134: .asciz "\n"
.align	4
______139: .word	0
_____144: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	4,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-24,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-40,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-40,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-64,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____42 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____43 
 nop
_____42:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____43:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____44 
 nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____45 
 nop
_____44:
_____45:
set	0, %l0
set	-80,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____54 
 nop
set	-80,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____55 
 nop
_____54:
set	1,%l3
set	-80,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____55:
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____56 
 nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____57 
 nop
_____56:
_____57:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____63 
 nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	1, %o0
call exit
nop
_____63:
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____65 
 nop
set	_strFmt,%o0
set	_____66,%o1
call printf
nop
set	1, %o0
call exit
nop
_____65:
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____67 
 nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	1, %o0
call exit
nop
_____67:
st	%l1, [%l0]
set	4,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-104,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-108,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____79 
 nop
set	-108,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____80 
 nop
_____79:
set	1,%l3
set	-108,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____80:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____81 
 nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____82 
 nop
_____81:
_____82:
set	0, %l0
set	-120,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____91 
 nop
set	-120,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____92 
 nop
_____91:
set	1,%l3
set	-120,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____92:
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____93 
 nop
set	_strFmt,%o0
set	_____95,%o1
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____94 
 nop
_____93:
_____94:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____99 
 nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	1, %o0
call exit
nop
_____99:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	4,%l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-144,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____112 
 nop
set	-148,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____113 
 nop
_____112:
set	1,%l3
set	-148,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____113:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____114 
 nop
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	_strFmt,%o0
set	_____118,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____115 
 nop
_____114:
_____115:
set	0, %l0
set	-160,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____124 
 nop
set	-160,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____125 
 nop
_____124:
set	1,%l3
set	-160,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____125:
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____126 
 nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
set	_strFmt,%o0
set	_____129,%o1
call printf
nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____127 
 nop
_____126:
_____127:
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____132 
 nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	1, %o0
call exit
nop
_____132:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____134,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 172) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____140 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____141 
 nop
_____140:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____141:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____142 
 nop
ba  _____143 
 nop
_____142:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____144,%o1
call printf
nop
_____143:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

