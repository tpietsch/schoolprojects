.global	a, __________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	4
_____5: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____6: .asciz "a.b should be 4, but it is actually "
.align	4
_____9: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____10: .asciz "\n"
.align	4
______12: .word	7
_____14: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____15: .asciz "b.b should be 7, but it is actually "
.align	4
_____18: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____19: .asciz "\n"
.align	4
_____22: .single	0r3.6
_____25: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____27: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______29: .single	0r3.6
_____32: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____33: .asciz "b.c should be 7.60, but it is actually "
.align	4
_____36: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____37: .asciz "\n"
.align	4
_____43: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____45: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____48: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____51: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____52: .asciz "a.c should be 57.76, but it is actually "
.align	4
_____55: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____56: .asciz "\n"
.align	4
______60: .word	1
_____63: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____67: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____69: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____71: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____72: .asciz "a.c should be 58.76, but it is actually "
.align	4
_____75: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____76: .asciz "\n"
.align	4
_____83: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____85: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____88: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____92: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____93: .asciz "a.hi should be false "
.align	4
_____96: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____99: .asciz "\n"
.align	4
_____100: .asciz "this should be true "
.align	4
_____105: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____107: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____113: .asciz "\n"
.align	4
_____117: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______118: .word	1
_____120: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____121: .asciz "a.fa.freespeech should be 1, but it is actually "
.align	4
_____125: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____127: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____128: .asciz "\n"
.align	4
______133: .word	0
_____138: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	36
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	a, %l1
add	%g0, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-72,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	4,%l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____4 
 nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	1, %o0
call exit
nop
_____4:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-108,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____8 
 nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	1, %o0
call exit
nop
_____8:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	-36, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-144,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	7,%l1
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____13 
 nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	1, %o0
call exit
nop
_____13:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	-36, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-180,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____17 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
_____17:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	-36, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-216,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	a, %l1
add	%g0, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-252,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____24 
 nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
_____24:
ld	[%l0], %l1
set	-256,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____26 
 nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	1, %o0
call exit
nop
_____26:
ld	[%l0], %l1
set	-256,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______29,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-260,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-256,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-256,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-256,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-260,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-264,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-264,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-216,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____31 
 nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	1, %o0
call exit
nop
_____31:
st	%f1, [%l0]
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	-36, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-300,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-300,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____35 
 nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1, %o0
call exit
nop
_____35:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-336,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-36, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-372,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-36, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-408,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-372,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____42 
 nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	1, %o0
call exit
nop
_____42:
ld	[%l0], %f1
set	-412,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-372,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____44 
 nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	1, %o0
call exit
nop
_____44:
ld	[%l0], %f1
set	-412,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-408,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____47 
 nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	1, %o0
call exit
nop
_____47:
ld	[%l0], %f1
set	-416,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-412,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-416,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-420,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-420,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-336,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____50 
 nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	1, %o0
call exit
nop
_____50:
st	%f1, [%l0]
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-456,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-456,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____54 
 nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
set	1, %o0
call exit
nop
_____54:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-492,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	a, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-528,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	1,%l1
set	-532,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-528,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____62 
 nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	1, %o0
call exit
nop
_____62:
ld	[%l0], %f1
set	-536,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-532,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-532,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-532,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-536,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-540,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-528,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____66 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	1, %o0
call exit
nop
_____66:
ld	[%l0], %f1
set	-544,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-540,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-528,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____68 
 nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	1, %o0
call exit
nop
_____68:
st	%l1, [%l0]
set	-544,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-492,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____70 
 nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	1, %o0
call exit
nop
_____70:
st	%f1, [%l0]
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-580,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-580,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____74 
 nop
set	_strFmt,%o0
set	_____75,%o1
call printf
nop
set	1, %o0
call exit
nop
_____74:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____76,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	20, %l2
add	%l2, %l1, %l1
set	-616,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-36, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-652,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	a, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-688,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	0, %l0
set	-692,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-652,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____82 
 nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	1, %o0
call exit
nop
_____82:
ld	[%l0], %f1
set	-696,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-652,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____84 
 nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	1, %o0
call exit
nop
_____84:
ld	[%l0], %f1
set	-696,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-688,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____87 
 nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	1, %o0
call exit
nop
_____87:
ld	[%l0], %f1
set	-700,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-696,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-700,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____89 
 nop
set	-692,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____90 
 nop
_____89:
set	1,%l3
set	-692,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____90:
set	-692,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-616,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____91 
 nop
set	_strFmt,%o0
set	_____92,%o1
call printf
nop
set	1, %o0
call exit
nop
_____91:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	20, %l2
add	%l2, %l1, %l1
set	-736,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-736,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____95 
 nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	1, %o0
call exit
nop
_____95:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____97 
 nop
set	_boolF,%o1
ba  _____98 
 nop
_____97:
set	_boolT,%o1
_____98:
call printf
nop
set	_strFmt,%o0
set	_____99,%o1
call printf
nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	20, %l2
add	%l2, %l1, %l1
set	-772,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	a, %l1
add	%g0, %l1, %l1
set	20, %l2
add	%l2, %l1, %l1
set	-808,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	0, %l0
set	-812,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-772,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____104 
 nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
set	1, %o0
call exit
nop
_____104:
ld	[%l0], %l1
set	-808,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____106 
 nop
set	_strFmt,%o0
set	_____107,%o1
call printf
nop
set	1, %o0
call exit
nop
_____106:
ld	[%l0], %l2
cmp	%l1, %l2
be  _____108 
 nop
set	-812,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____109 
 nop
_____108:
set	1,%l3
set	-812,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____109:
set	0, %l0
set	-816,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-812,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-816,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-816,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____111 
 nop
set	_boolF,%o1
ba  _____112 
 nop
_____111:
set	_boolT,%o1
_____112:
call printf
nop
set	_strFmt,%o0
set	_____113,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	24, %l2
add	%l2, %l1, %l1
set	-852,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-852, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____116 
 nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	1, %o0
call exit
nop
_____116:
ld	[%l1], %l1
set	-864,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-864,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	1,%l1
set	-864,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____119 
 nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
set	1, %o0
call exit
nop
_____119:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____121,%o1
call printf
nop
set	a, %l1
add	%g0, %l1, %l1
set	24, %l2
add	%l2, %l1, %l1
set	-900,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-900, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____124 
 nop
set	_strFmt,%o0
set	_____125,%o1
call printf
nop
set	1, %o0
call exit
nop
_____124:
ld	[%l1], %l1
set	-912,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-912,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-912,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____126 
 nop
set	_strFmt,%o0
set	_____127,%o1
call printf
nop
set	1, %o0
call exit
nop
_____126:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 912) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____134 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____135 
 nop
_____134:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____135:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____136 
 nop
ba  _____137 
 nop
_____136:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____138,%o1
call printf
nop
_____137:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

