.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	13
______4: .word	4
______10: .word	60
_____16: .asciz "Index value of "
.align	4
_____17: .asciz " is outside legal range [0,15).\n"
.align	4
______22: .word	0
_____27: .asciz "Index value of "
.align	4
_____28: .asciz " is outside legal range [0,15).\n"
.align	4
_____32: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______34: .word	4
______40: .word	60
_____46: .asciz "Index value of "
.align	4
_____47: .asciz " is outside legal range [0,15).\n"
.align	4
______52: .word	0
_____57: .asciz "Index value of "
.align	4
_____58: .asciz " is outside legal range [0,15).\n"
.align	4
_____61: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____62: .asciz "\n"
.align	4
______67: .word	0
_____72: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	13,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-76,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-80,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	60,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	60,%l1
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-80,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-80,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-92,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____23 
 nop
set	-92,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____24 
 nop
_____23:
set	1,%l3
set	-92,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____24:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____25 
 nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____26 
 nop
_____25:
_____26:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-108,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____31 
 nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	1, %o0
call exit
nop
_____31:
st	%f1, [%l0]
set	4,%l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-116,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-120,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-124,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	60,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	60,%l1
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____42 
 nop
set	-124,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____43 
 nop
_____42:
set	1,%l3
set	-124,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____43:
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____44 
 nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____45 
 nop
_____44:
_____45:
set	0, %l0
set	-136,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____53 
 nop
set	-136,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____54 
 nop
_____53:
set	1,%l3
set	-136,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____54:
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____55 
 nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____56 
 nop
_____55:
_____56:
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____60 
 nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	1, %o0
call exit
nop
_____60:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 148) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____68 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____69 
 nop
_____68:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____69:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____70 
 nop
ba  _____71 
 nop
_____70:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
_____71:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

