.global	func, __________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	1
_____7: .asciz "\n"
.align	4
______9: .word	4
______11: .word	0
______16: .word	8
_____22: .asciz "Index value of "
.align	4
_____23: .asciz "0"
.align	4
_____24: .asciz " is outside legal range [0,2).\n"
.align	4
______29: .word	0
_____34: .asciz "Index value of "
.align	4
_____35: .asciz "0"
.align	4
_____36: .asciz " is outside legal range [0,2).\n"
.align	4
______38: .word	5
_____40: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______42: .word	4
______44: .word	1
______49: .word	8
_____55: .asciz "Index value of "
.align	4
_____56: .asciz "1"
.align	4
_____57: .asciz " is outside legal range [0,2).\n"
.align	4
______62: .word	0
_____67: .asciz "Index value of "
.align	4
_____68: .asciz "1"
.align	4
_____69: .asciz " is outside legal range [0,2).\n"
.align	4
______72: .word	4
______74: .word	0
______79: .word	8
_____85: .asciz "Index value of "
.align	4
_____86: .asciz "0"
.align	4
_____87: .asciz " is outside legal range [0,2).\n"
.align	4
______92: .word	0
_____97: .asciz "Index value of "
.align	4
_____98: .asciz "0"
.align	4
_____99: .asciz " is outside legal range [0,2).\n"
.align	4
_____103: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____105: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______107: .word	4
______109: .word	0
______114: .word	8
_____120: .asciz "Index value of "
.align	4
_____121: .asciz "0"
.align	4
_____122: .asciz " is outside legal range [0,2).\n"
.align	4
______127: .word	0
_____132: .asciz "Index value of "
.align	4
_____133: .asciz "0"
.align	4
_____134: .asciz " is outside legal range [0,2).\n"
.align	4
_____137: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____138: .asciz ", "
.align	4
______140: .word	4
______142: .word	1
______147: .word	8
_____153: .asciz "Index value of "
.align	4
_____154: .asciz "1"
.align	4
_____155: .asciz " is outside legal range [0,2).\n"
.align	4
______160: .word	0
_____165: .asciz "Index value of "
.align	4
_____166: .asciz "1"
.align	4
_____167: .asciz " is outside legal range [0,2).\n"
.align	4
_____170: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____171: .asciz "\n"
.align	4
______176: .word	0
_____181: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	4,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-20,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____18 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____19 
 nop
_____18:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____19:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____20 
 nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____21 
 nop
_____20:
_____21:
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____30 
 nop
set	-36,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____31 
 nop
_____30:
set	1,%l3
set	-36,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____31:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____32 
 nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____33 
 nop
_____32:
_____33:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____39 
 nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	1, %o0
call exit
nop
_____39:
st	%l1, [%l0]
set	4,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-60,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____51 
 nop
set	-64,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____52 
 nop
_____51:
set	1,%l3
set	-64,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____52:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____53 
 nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____54 
 nop
_____53:
_____54:
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____63 
 nop
set	-76,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____64 
 nop
_____63:
set	1,%l3
set	-76,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____64:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____65 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____66 
 nop
_____65:
_____66:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-100,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____81 
 nop
set	-104,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____82 
 nop
_____81:
set	1,%l3
set	-104,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____82:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____83 
 nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____84 
 nop
_____83:
_____84:
set	0, %l0
set	-116,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____93 
 nop
set	-116,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____94 
 nop
_____93:
set	1,%l3
set	-116,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____94:
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____95 
 nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
set	_strFmt,%o0
set	_____99,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____96 
 nop
_____95:
_____96:
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____102 
 nop
set	_strFmt,%o0
set	_____103,%o1
call printf
nop
set	1, %o0
call exit
nop
_____102:
ld	[%l0], %o0
add	%sp, -0, %sp
set	func, %l1
call %l1
nop
set	-132,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____104 
 nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
set	1, %o0
call exit
nop
_____104:
st	%l1, [%l0]
set	4,%l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-144,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____116 
 nop
set	-148,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____117 
 nop
_____116:
set	1,%l3
set	-148,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____117:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____118 
 nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
set	_strFmt,%o0
set	_____121,%o1
call printf
nop
set	_strFmt,%o0
set	_____122,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____119 
 nop
_____118:
_____119:
set	0, %l0
set	-160,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____128 
 nop
set	-160,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____129 
 nop
_____128:
set	1,%l3
set	-160,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____129:
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____130 
 nop
set	_strFmt,%o0
set	_____132,%o1
call printf
nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	_strFmt,%o0
set	_____134,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____131 
 nop
_____130:
_____131:
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____136 
 nop
set	_strFmt,%o0
set	_____137,%o1
call printf
nop
set	1, %o0
call exit
nop
_____136:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____138,%o1
call printf
nop
set	4,%l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-184,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-188,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____149 
 nop
set	-188,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____150 
 nop
_____149:
set	1,%l3
set	-188,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____150:
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____151 
 nop
set	_strFmt,%o0
set	_____153,%o1
call printf
nop
set	_strFmt,%o0
set	_____154,%o1
call printf
nop
set	_strFmt,%o0
set	_____155,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____152 
 nop
_____151:
_____152:
set	0, %l0
set	-200,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-208,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____161 
 nop
set	-200,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____162 
 nop
_____161:
set	1,%l3
set	-200,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____162:
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____163 
 nop
set	_strFmt,%o0
set	_____165,%o1
call printf
nop
set	_strFmt,%o0
set	_____166,%o1
call printf
nop
set	_strFmt,%o0
set	_____167,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____164 
 nop
_____163:
_____164:
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-212,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____169 
 nop
set	_strFmt,%o0
set	_____170,%o1
call printf
nop
set	1, %o0
call exit
nop
_____169:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____171,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 212) & -8

func: 
set SAVE.func, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-16,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	-4,%l1
add 	%fp, %l1, %l1
ld	[%l1], %i0
ret
restore

ret
restore

SAVE.func = -(92 + 20) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____177 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____178 
 nop
_____177:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____178:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____179 
 nop
ba  _____180 
 nop
_____179:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____181,%o1
call printf
nop
_____180:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

