.global	g, f, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .single	0r0.0
______3: .single	0r0.0
_____4: .single	0r1.0
______5: .single	0r1.0
______6: .word	2
_____7: .asciz "g "
.align	4
_____8: .asciz "\n"
.align	4
______11: .word	1
_____13: .asciz "g + 1 "
.align	4
_____14: .asciz "\n"
.align	4
______16: .word	1
_____20: .asciz "g++ "
.align	4
_____21: .asciz "\n"
.align	4
_____22: .asciz "f "
.align	4
_____23: .asciz "\n"
.align	4
_____27: .asciz "g + f "
.align	4
_____28: .asciz "\n"
.align	4
_____32: .asciz "g - x "
.align	4
_____33: .asciz "\n"
.align	4
_____34: .single	0r1.5
______37: .single	0r1.5
_____39: .asciz "g + 1.5 "
.align	4
_____40: .asciz "\n"
.align	4
______42: .word	1
_____46: .asciz "g++ "
.align	4
_____47: .asciz "\n"
.align	4
______52: .word	0
_____57: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::g
g:	.skip	4
!CodeGen::doVarDecl::f
f:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	______3,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	g,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
_init_done:
set	2,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-12,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-16,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	g,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-20,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-28,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	g,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-44,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	g,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-52,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-56,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	g,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______37,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-68,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	g,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	1,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-72,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-80,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	g,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 84) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____53 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____54 
 nop
_____53:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____54:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____55 
 nop
ba  _____56 
 nop
_____55:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
_____56:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

