.global	TEST.foo1, foo2, y, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Inside foo1()"
.align	4
_____3: .asciz "\n"
.align	4
_____4: .asciz "Inside foo2()"
.align	4
_____5: .asciz "\n"
.align	4
_____12: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____17: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____24: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____29: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____30: .asciz "RESULT"
.align	4
_____31: .asciz "\n"
.align	4
______36: .word	0
_____41: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::y
y:	.skip	8
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	TEST.foo1, %l0
add	%g0, %l0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-4, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
set	-24, %l0
add	%fp, %l0, %l0
add	%l0, 4, %l0
ld	[%l0], %l0
st	%l0, [%l1]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____9 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____10 
 nop
_____9:
add	%sp, -0, %sp
_____10:
_____8:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____11 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1, %o0
call exit
nop
_____11:
call %l1
nop
set	foo2, %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
set	-12, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____14 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____15 
 nop
_____14:
add	%sp, -0, %sp
_____15:
_____13:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____16 
 nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	1, %o0
call exit
nop
_____16:
call %l1
nop
set	TEST.foo1, %l0
add	%g0, %l0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-4, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	y,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	y, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
set	-36, %l0
add	%fp, %l0, %l0
add	%l0, 4, %l0
ld	[%l0], %l0
st	%l0, [%l1]
set	y, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____21 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____22 
 nop
_____21:
add	%sp, -0, %sp
_____22:
_____20:
set	y,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____23 
 nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	1, %o0
call exit
nop
_____23:
call %l1
nop
set	foo2, %l1
set	y,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	y, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
set	y, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____26 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____27 
 nop
_____26:
add	%sp, -0, %sp
_____27:
_____25:
set	y,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____28 
 nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	1, %o0
call exit
nop
_____28:
call %l1
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

TEST.foo1: 
set SAVE.TEST.foo1, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
ret
restore

SAVE.TEST.foo1 = -(92 + 4) & -8

foo2: 
set SAVE.foo2, %g1
save %sp, %g1, %sp
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
ret
restore

SAVE.foo2 = -(92 + 0) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____37 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____38 
 nop
_____37:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____38:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____39 
 nop
ba  _____40 
 nop
_____39:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
_____40:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

