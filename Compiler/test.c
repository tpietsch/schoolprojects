#include <stdio.h>

extern int foo();

int main(void) {
  volatile int d = -2;
  int x[5];
  int i;

  foo();
  printf("%p\n", &d);
  for (i = 0; i <= 8; ++i) {
    x[i] = i;
    printf("%p\n", &(x[i]));
  }

  printf("%d\n", d);
  printf("%p\n", &d);
}
