import java_cup.runtime.*;
import java.util.Vector;

//--------------------------------------------------------------------
//
//--------------------------------------------------------------------

parser code
{:
:};

scan with {: return new Symbol (sym.EOF); :};


//--------------------------------------------------------------------
//
//--------------------------------------------------------------------

terminal
    T_NEW,
    T_DELETE,

    // DEFAULT
    T_AMPERSAND,
    T_AND,
    T_ARROW,
    T_ASSIGN,
    T_BAR,
    T_BOOL,
    T_BREAK,
    T_CARET,
    T_CIN,
    T_COLON,
    T_COLONCOLON,
    T_COMMA,
    T_CONST,
    T_CONTINUE,
    T_COUT,
    T_DOT,
    T_ELSE,
    T_ENDL,
    T_EQU,
    T_EXIT,
    T_EXTERN,
    T_FALSE,
    T_FLOAT,
    T_FOR,
    T_FOREACH,
    T_FUNCPTR,
    T_FUNCTION,
    T_GT,
    T_GTE,
    T_IF,
    T_INT,
    T_ISTREAM,
    T_LBRACE,
    T_LBRACKET,
    T_LPAREN,
    T_LT,
    T_LTE,
    T_MINUS,
    T_MINUSMINUS,
    T_MOD,
    T_NEQ,
    T_NOT,
    T_NULLPTR,
    T_OR,
    T_OSTREAM,
    T_PLUS,
    T_PLUSPLUS,
    T_RBRACE,
    T_RBRACKET,
    T_RETURN,
    T_RPAREN,
    T_SEMI,
    T_SIZEOF,
    T_SLASH,
    T_STAR,
    T_STATIC,
    T_STRUCTDEF,
    T_THIS,
    T_TRUE,
    T_TYPEDEF,
    T_WHILE,
    T_VOID,
    T_AUTO,
    T_DECLTYPE
    ;

terminal String
    T_FLOAT_LITERAL,
    T_INT_LITERAL,
    T_STR_LITERAL,
    T_ID_U,
    T_ID
    ;

//--------------------------------------------------------------------
//
//--------------------------------------------------------------------

non terminal

    NewStmt,
    DeleteStmt,

    // DEFAULTS
    Program,
    OptGlobalDecls,
    GlobalDecls,
    GlobalDecl,
    ExternDecl,
    VarDecl,
    ConstDecl,
    TypedefDecl,
    OptStmtList,
    StmtList,
    Stmt,
    CodeBlock,
    IfStmt,
   
    ForStmt,
    ForeachStmt,
    WhileStmt,
    BreakStmt,
    ContinueStmt,
    ExitStmt,
    ReturnStmt,
    ReadStmt,
    WriteStmt,
    UnarySign,
    FieldList,
    FieldsList,
    Modifier;

non terminal Integer
    OptModifierList,
    ModifierList;

non terminal Boolean
    OptRef,
    OptStatic
    ;

non terminal CoutGenerator
    WritePairList
    ;

non terminal ParameterSTO ParamDecl;

non terminal UnaryOperator
    IncDecOp
    ;


non terminal STO
    IterationVarDecl,
    OptArrayDef,
    ConstExpr,
    ArrElemsList,
    OptExpr,
    Expr,
    Expr0,
    Expr1,
    Expr2,
    Expr3,
    Expr4,
    Expr5,
    Expr6,
    Expr7,
    Expr8,
    Designator,
    Designator2,
    Designator3,
    QualIdent,
    OptInit,
    FuncDecl,
    WritePair,
    FuncDef;

non terminal Type
    BasicType,
    SubType,
    Type,
    UndecoratedType,
    DeclType,
    ReturnType
    ;


non terminal Vector<ParameterSTO>
        OptParamList
    ,   ParamList
    ;

non terminal Vector<IdInfo>
        IdListWOptInit
    ,   IdListWInit
    ,   IdList
    ,   IdListUpper
    ;


non terminal Vector<STO>
        OptExprList
    ,   ExprList
    ;


non terminal BinaryOperator
    AddOp,
    MulOp,
    Equality,
    Relation
    ;

non terminal String
 OptElse
    ;
 

//--------------------------------------------------------------------
//
//--------------------------------------------------------------------


// Defaults

Program ::= {: ((MyParser) parser).DoProgramStart(); :}
            OptGlobalDecls
            {: ((MyParser) parser).DoProgramEnd(); :}
          ;

OptGlobalDecls ::=  GlobalDecls
    |                     /* empty */
    ;

GlobalDecls ::= GlobalDecl
              | GlobalDecls GlobalDecl
              ;


GlobalDecl ::= ExternDecl
             | VarDecl
             | ConstDecl
             | TypedefDecl
             | FuncDecl
             | FuncDef
             ;

ExternDecl ::= T_EXTERN UndecoratedType:t IdList:ids T_SEMI
               {: ((MyParser) parser).DoExternDecl (t, ids); :}
               ;

VarDecl ::= OptStatic:s UndecoratedType:t IdListWOptInit:ids T_SEMI
            {:
                System.out.println("type = id");
                boolean isStatic = s.booleanValue();
                ((MyParser) parser).DoVarDecl(t, isStatic, ids);
            :}
            | OptStatic:s T_AUTO T_ID:_1 T_ASSIGN Expr:_2 T_SEMI
            {:
                boolean isStatic = s.booleanValue();
                System.out.println("DoAutoVarDecl");
                ((MyParser) parser).DoAutoVarDecl(_1, _2.getType(), isStatic);
            :}
            ;

ConstDecl ::=  OptStatic:s T_CONST UndecoratedType:t IdListWInit:idExprs T_SEMI
               {:
                  boolean isStatic = s.booleanValue();
                  ((MyParser) parser).DoConstDecl (t, isStatic, idExprs);
               :}
            |  OptStatic:s T_CONST T_AUTO T_ID:_1 T_ASSIGN Expr:_2 T_SEMI
               {:
                  boolean isStatic = s.booleanValue();
                  ((MyParser) parser).DoAutoVarDecl(_1,_2.getType(), isStatic);
               :}
            ;


OptStatic ::=   T_STATIC
           {: RESULT = new Boolean(true); :}
    |
           {: RESULT = new Boolean(false); :}
    ;


TypedefDecl ::= T_TYPEDEF Type:t IdListUpper:id T_SEMI
                    {: ((MyParser) parser).DoTypedefDecl (id, t); :}
              | T_STRUCTDEF T_ID_U:id T_LBRACE
                    {: ((MyParser) parser).DoStructBlockOpen(id); :}
                FieldsList T_RBRACE T_SEMI
                    {:
                        ((MyParser) parser).DoStructdefDecl(id);
                    :}
              ;


FieldsList ::= FieldList
             | FieldsList FieldList
             ;


FieldList ::= UndecoratedType:t IdList:ids T_SEMI
                {: ((MyParser) parser).DoVarFieldDecl(t, ids); :}
            | FuncDef:f
                {: ((MyParser) parser).DoFuncFieldDecl(f); :}
            ;


FuncDef ::= T_FUNCTION T_COLON ReturnType:rt OptRef:isRef T_ID:id
                {:
                    ((MyParser) parser).SaveLineNum ();
                    RESULT = ((MyParser) parser).DoFuncDecl_1(id, rt, isRef.booleanValue());
                :}

            T_LPAREN OptParamList:ps T_RPAREN
            {: ((MyParser) parser).DoFormalParams(ps); :}
            T_LBRACE OptStmtList:ss T_RBRACE
            {: ((MyParser) parser).DoFuncDecl_2(); :}
          ;


FuncDecl ::= T_EXTERN T_FUNCTION T_COLON ReturnType:rt T_ID:id
                {: ((MyParser) parser).DoFuncDecl_1(id, rt, false); :}
             T_LPAREN OptParamList:ps T_RPAREN
                {: ((MyParser) parser).DoFormalParams(ps); :}
             T_SEMI
                {: ((MyParser) parser).DoFuncDecl_2(); :}
           ;


Type ::= SubType:st OptModifierList OptArrayDef:_2
         {:

          if(_2 == null){
                  RESULT = st;
          }
          else{
            if(_2.isConst()){
            RESULT = new ArrayType(st,((Consty) _2).getConstSTO()
                .getIntValue());
        }
        else{

            RESULT=st;
        }
          }


          :}
       | T_FUNCPTR T_COLON ReturnType OptRef T_LPAREN OptParamList:_3 T_RPAREN
       ;

UndecoratedType ::= SubType:t
                    {:
                        System.out.println("subtyep");
                        RESULT = t; :}
                    |
                    T_FUNCPTR T_COLON ReturnType OptRef T_LPAREN OptParamList:_3 T_RPAREN
                    {: RESULT = new VoidType(); :}
                    ;

SubType ::= QualIdent:sto
            {:  
                 System.out.println("ident");
                RESULT = sto.getType(); :}
          | BasicType:t
            {: 
                 System.out.println("basix");
                RESULT = t; :}
          | DeclType:t
            {:  System.out.println("declsty");
                RESULT = t; :}
          ;

DeclType ::= T_DECLTYPE T_LPAREN Expr:e T_RPAREN
             {: RESULT = e.getType(); :}
           ;


BasicType ::=   T_INT
                {: RESULT = new IntType(); :}
                |
                T_FLOAT
                {: RESULT = new FloatType(); :}
                |
                T_BOOL
                {: RESULT = new BoolType(); :}
                ;


OptModifierList ::= ModifierList:ms
                    {: RESULT = ms; :}
    |               {: RESULT = new Integer(0); :}
    ;


ModifierList ::=    Modifier:m
                    {: RESULT = new Integer(1); :}
    |               ModifierList:ms Modifier:m
                    {: RESULT = new Integer(ms.intValue() + 1); :}
    ;


Modifier ::=        T_STAR
    ;


OptArrayDef ::=     T_LBRACKET ConstExpr:_1 T_RBRACKET
                    {:
                         RESULT = ((MyParser) parser).arrayDec(_1);
                    :}
    |               /* empty */
    ;


ReturnType ::=      SubType:t OptModifierList:oms
                    {:
                        RESULT = t;
                    :}
    |               T_VOID

                    {:
                        RESULT=new VoidType();
                    :}
    ;


CodeBlock ::=       T_LBRACE
                      {: ((MyParser) parser).DoBlockOpen(); :}
                    OptStmtList:_2
                      {: ((MyParser) parser).DoBlockClose(); :}
                    T_RBRACE
    ;


OptStmtList ::=     StmtList
    |               /* empty */
    ;


StmtList ::=        Stmt
    |               StmtList Stmt
    ;


Stmt ::=        VarDecl
    |               ConstDecl
    |               TypedefDecl
    |               CodeBlock
    |               Expr:e T_SEMI
                    {: ((MyParser) parser).DoExpressionStatement(e); :}
    |               IfStmt
    |               WhileStmt
    |               ForStmt
    |               ForeachStmt
    |               BreakStmt
    |               ContinueStmt
    |               ExitStmt
    |               ReturnStmt
    |               ReadStmt
    |               WriteStmt
    |               NewStmt
    |               DeleteStmt
    ;


OptParamList ::= ParamList:ps
                 {: RESULT = ps; :}
               | // otherwise:
                 {: RESULT = new Vector<ParameterSTO>(); :}
               ;


ParamList ::= ParamDecl:p
              {:
                RESULT = new Vector<ParameterSTO>();
                RESULT.addElement(p);
              :}
            | ParamList:ps T_COMMA ParamDecl:p
              {:
                RESULT = ps;
                RESULT.addElement(p);
              :}
            ;


ParamDecl ::= Type:t OptRef:r T_ID:n
              {: RESULT = new ParameterSTO(n, t, r.booleanValue()); :}
            ;


OptRef ::= T_AMPERSAND
           {: RESULT = new Boolean(true); :}
         | // otherwise:
           {: RESULT = new Boolean(false); :}
         ;


IdList ::=          OptModifierList:ms OptArrayDef:ad T_ID:id
                    {:
                        STO sto = ((MyParser) parser).DoModifyId(ms, ad, id);
                        RESULT = new Vector<IdInfo> ();
                        RESULT.addElement(
                            new IdInfo(id, ms, ((Consty) ad).getConstSTO()));
                    :}
    |               IdList:ids T_COMMA OptModifierList:ms OptArrayDef:ad T_ID:id
                    {:
                        RESULT = ids;
                        RESULT.addElement(
                            new IdInfo(id, ms, ((Consty)
                        ad).getConstSTO()));
                    :}
    ;


IdListUpper ::=     T_ID_U:id
                    {:
                        RESULT = new Vector<IdInfo>();
                        RESULT.addElement(new IdInfo(id));
                    :}
    |               IdListUpper:ids T_COMMA T_ID_U:id
                    {:
                        RESULT = ids;
                        RESULT.addElement (new IdInfo(id));
                    :}
    ;


IdListWOptInit ::= OptModifierList:ms OptArrayDef:a T_ID:id OptInit:e
                   {:
                        RESULT = new Vector<IdInfo>();
                        RESULT.addElement(new IdInfo(id, ms, a, e));
                   :}
                 | IdListWOptInit:ids T_COMMA OptModifierList:ms OptArrayDef:a T_ID:id OptInit:e
                   {:
                        RESULT = ids;
                        RESULT.addElement(new IdInfo(id, ms, a, e));
                    :}
                 ;

IdListWInit ::= OptModifierList:ms OptArrayDef:a T_ID:id T_ASSIGN ConstExpr:e
                {:
                    RESULT = new Vector<IdInfo>();
                    RESULT.addElement(new IdInfo(id, ms, a, e));
                :}
              | IdListWInit:ids T_COMMA OptModifierList:ms OptArrayDef:a T_ID:id T_ASSIGN ConstExpr:e
                {:
                    RESULT = ids;
                    RESULT.addElement(new IdInfo(id, ms, a, e));
                :}
              ;


OptInit ::= T_ASSIGN Expr:e
            {:
                RESULT = e;

                :}
          | T_ASSIGN T_LBRACE ArrElemsList:e T_RBRACE
            {:

                RESULT = e;
                 :}
          | {: RESULT = new EmptySTO(); :}
          ;

ArrElemsList ::=   Expr:e
               {:
                RESULT = new ArraySTO();
                RESULT.addSTO(e);

               :}
     |   ArrElemsList:e T_COMMA Expr:add
            {:
                e.addSTO(add);
                RESULT=e;
            :}

    ;

IfStmt ::=          T_IF Expr:_1
                    {: ((MyParser) parser).DoCondCheck(_1);
                       ((MyParser) parser).setIfTag(true); :}
                    
                    CodeBlock {:    :}

                    OptElse:_2
                    {:
                      
                                       //     String f = ((MyParser) parser).generateLabel("IfL1");
                      if(_2 == null){
                        ((MyParser) parser).setIfTag(false);
                        ((MyParser) parser).ExitLabel();
                        }
                      else{

                     // ((MyParser) parser).ExitLabel();
                      }
                      
                      :}
                
;


OptElse ::=         T_ELSE
                   {:
                                          ((MyParser) parser).BranchOverElse();

                    ((MyParser) parser).setIfTag(true);

                  :}

                    CodeBlock
                      {:((MyParser) parser).setIfTag(false);
                                               ((MyParser) parser).ExitLabel();

                        RESULT = "";
                        :}
    |           
    ;


WhileStmt ::=       T_WHILE Expr:_1
                    {:
                        ((MyParser) parser).DoBlockOpen();
                       // ((MyParser) parser).DoCondCheck(_1);
                       ((MyParser) parser).DoCondCheckWhile(_1);
                        ((MyParser) parser).setWhile(true);

                        ((MyParser) parser).setIfTag(true);

                    :}
                    CodeBlock
                    {:
                        ((MyParser) parser).setIfTag(false);
                        ((MyParser) parser). DoWhileClose();
                        ((MyParser) parser).DoBlockClose();
                        ((MyParser) parser).setWhile(false);


                    :}

    ;


/**** Fall 2013: DO NOT IMPLEMENT THE FOR-LOOP */
ForStmt ::=        T_FOR T_LPAREN OptExpr T_SEMI OptExpr T_SEMI OptExpr T_RPAREN CodeBlock
    ;
/**** Fall 2013: DO NOT IMPLEMENT THE FOR-LOOP */

IterationVarDecl ::= Type:_3 OptRef:_2 T_ID:_1
                     {: RESULT=((MyParser) parser).DoIterationVarDecl(_3, _2, _1); :}
                   ;

ForeachStmt ::=     T_FOREACH T_LPAREN IterationVarDecl:_1 T_COLON Expr:_2 T_RPAREN
                    {:
                        ((MyParser) parser).DoBlockOpen();
                        ((MyParser) parser).setFor(true);
                        ((MyParser) parser).DoIterationVarDecl (_1,_2);
                        ((MyParser) parser).setIfTag(true);
                    :}
                    CodeBlock
                    {:

                        ((MyParser) parser).DoBlockClose();
                        ((MyParser) parser).DoWhileClose();
                           ((MyParser) parser).setFor(false);
                           ((MyParser) parser).setIfTag(false);

                    :}
    ;


BreakStmt ::=       T_BREAK T_SEMI
                    {:
                        ((MyParser) parser).setBreak(true);

                    :}
    ;


ContinueStmt ::=    T_CONTINUE T_SEMI
                     {:
                        ((MyParser) parser).setCont(true);

                    :}
    ;


ExitStmt ::=        T_EXIT T_LPAREN Expr:_1 T_RPAREN T_SEMI
                    {:
                        ((MyParser) parser).DoExitCheck(_1);
                    :}
;


ReturnStmt ::=   T_RETURN T_SEMI
                 {:

                 STO s = new ExprSTO("void", new VoidType(), 0);

                 ((MyParser) parser).DoReturnStmtCheck(s);

                 :}
    |            T_RETURN Expr:_1 T_SEMI
                 {:
                 ((MyParser) parser).DoReturnStmtCheck(_1);

                 :}
    ;


ReadStmt ::=        T_CIN T_ISTREAM Designator:_1 T_SEMI
                {:

                 ((MyParser) parser).DoCin(_1);

                 
                 :}
    ;

NewStmt ::= T_NEW Designator:_1 T_SEMI
                {:

                 ((MyParser) parser).DoNewStmtCheck(_1);


                 :}
    ;

DeleteStmt ::= T_DELETE Designator:_1 T_SEMI
 {:
                 ((MyParser) parser).DoDeleteStmtCheck(_1);

                 :}
    ;

WriteStmt ::=       T_COUT T_OSTREAM WritePairList:_1 T_SEMI
                    {: 
                    
                      ((MyParser) parser).DoWriteStmt(_1); 

                      :}
    ;


// takes each elemend of STO type and gerenates the equivalent assembly
WritePairList ::=   WritePair:_1
                    {: 
                      RESULT = ((MyParser) parser).DoCout(); 
                       RESULT.add(_1); 
                       :}
    |               WritePairList:_1 T_OSTREAM WritePair:_2
                    {: 

                    RESULT = _1.add(_2); 
                    :}
    ;


WritePair ::=    Expr:_1
                    {:  

                      RESULT=_1; 
                       :}
    |            T_ENDL
                    {: RESULT = new EndlSTO("endl"); :}
    ;


ConstExpr ::= Expr:e
              {: 
                System.out.println("constexpr");
                RESULT = e; 
                :}
            ;


OptExprList ::= ExprList:es
                {: RESULT = es; :}
              | // otherwise:
                {: RESULT = new Vector<STO>(); :}
              ;


ExprList ::= Expr:e
             {:
                RESULT = new Vector<STO>();
                RESULT.addElement(e);
             :}
           | ExprList:es T_COMMA Expr:e
             {:
                RESULT = es;
                RESULT.addElement(e);
             :}
           ;

OptExpr ::= Expr
          |              /* empty */
          ;



Expr ::= Designator:d T_ASSIGN Expr:e
         {:
            System.out.println("Expr");
          RESULT = ((MyParser) parser).DoAssignExpr(d, e); :}
         | Expr0:e
            {:	RESULT = e; :}
       ;



Expr0 ::=        Expr0:_1 T_OR Expr1:_3
                    {:
                    OrOp _2 = new OrOp("||",_1,_3);
			         _2.setOperands(_1, _3);
                        RESULT = ((MyParser) parser).DoOperation(_2);
                    :}
    |               Expr1:_1
                    {:
			RESULT = _1;
                    :}
    ;


Expr1 ::=        Expr1:_1 T_AND Expr2:_3
                    {:
                    AndOp _2 = new AndOp("&&",_1,_3);
			             _2.setOperands(_1, _3);
                        RESULT = ((MyParser) parser).DoOperation(_2);
                    :}
    |               Expr2:_1
                    {:
			RESULT = _1;
                    :}
    ;


Expr2 ::=        Expr2:_1 T_BAR Expr3:_3
                    {:
                    OrBwOp _2 = new OrBwOp("|",_1,_3);
			         _2.setOperands(_1, _3);
                        RESULT = ((MyParser) parser).DoOperation(_2);
                    :}
    |               Expr3:_1
                    {:
                        RESULT = _1;
                    :}
    ;


Expr3 ::=        Expr3:_1 T_CARET Expr4:_3
                    {:
                     XorOp _2 = new XorOp("^",_1,_3);
			            _2.setOperands(_1, _3);
                        RESULT = ((MyParser) parser).DoOperation(_2);
                    :}
    |               Expr4:_1
                    {:
                        RESULT = _1;
                    :}
    ;


Expr4 ::=        Expr4:_1 T_AMPERSAND Expr5:_3
                    {:
                     AndBwOp _2 = new AndBwOp("&",_1,_3);
			            _2.setOperands(_1, _3);
                        RESULT = ((MyParser) parser).DoOperation(_2);
                    :}
    |               Expr5:_1
                    {:
                        RESULT = _1;
                    :}
    ;


Expr5 ::=           Expr6:operand1 Equality:o Expr7:operand2
                    {:
                        o.setOperands(operand1, operand2);
                        RESULT = ((MyParser) parser).DoOperation(o);
                    :}
    |               Expr6:_1
                    {:

                        RESULT = _1;
                    :}
    ;


Expr6 ::=           Expr6:operand1 Relation:o Expr7:operand2
                    {:
                        o.setOperands(operand1, operand2);
                        RESULT = ((MyParser) parser).DoOperation(o);
                    :}
    |               Expr7:_1
                    {:
                        RESULT = _1;
                    :}
    ;


Expr7 ::=           Expr7:operand1 AddOp:o Expr8:operand2
                    {:
                        o.setOperands(operand1, operand2);
                        RESULT = ((MyParser) parser).DoOperation(o);
                    :}
    |               Expr8:_1
                    {:
                        RESULT = _1;
                    :}
    ;


Expr8 ::=        Expr8:_1 MulOp:_2 Designator:_3
                    {:
                        _2.setOperands(_1, _3);
                        RESULT = ((MyParser) parser).DoOperation(_2);
                    :}
    |               Designator:_1
                    {:
                        RESULT = _1;
                    :}
    ;


Equality ::= T_EQU
             {:
                RESULT = new EqualToOp("==");
                :}
           | T_NEQ
             {: RESULT = new NEqualToOp("!="); :}
           ;


Relation ::= T_LT
             {: RESULT = new LessThanOp("<"); :}
           | T_GT
             {: RESULT = new GreaterThanOp(">"); :}
           | T_LTE
             {: RESULT = new LessThanOrEqualToOp("<="); :}
           | T_GTE
             {: RESULT = new GreaterThanOrEqualToOp("<="); :}
           ;


AddOp ::=       T_PLUS
                    {:
                        RESULT = new AddOp("+");
                    :}
    |               T_MINUS
                    {:
                        RESULT = new MinusOp("-");
                    :}
    ;


MulOp ::=       T_STAR
                    {:
                        RESULT = new MulOp("*");
                    :}
    |               T_SLASH
                    {:
                        RESULT = new DivOp("/");
                    :}
    |               T_MOD
                    {:
                        RESULT = new ModOp("%");
                    :}
    ;


IncDecOp ::=        T_PLUSPLUS
                    {:
                        RESULT = new IncOp("++");
                    :}
    |               T_MINUSMINUS
                    {:
                        RESULT = new DecOp("--");
                    :}
    ;


UnarySign ::=       T_PLUS
                    {:
                        RESULT = "+";
                    :}
    |               T_MINUS
                    {:
                        RESULT = "-";
                    :}
    ;


Designator ::= T_STAR Designator:sto
                    {:
			             RESULT = ((MyParser) parser).DoDereference(sto);
                    :}
    |               T_AMPERSAND Designator:sto
                    {:
			                 RESULT = ((MyParser) parser).DoAddressOf(sto);
                    :}
    |               UnarySign:_2 Designator:_1
                    {:
                     if(_2.equals("+")){

                        AddOp a = new AddOp("+");
                        a.setOperands(new ConstSTO("",new IntType(),"0"),_1);
                        RESULT =  ((MyParser) parser).DoOperation(a);
                     }
			        else{

                       MinusOp a = new MinusOp("-");
                       a.setOperands(new ConstSTO("",new IntType(),"0"),_1);
                       RESULT =  ((MyParser) parser).DoOperation(a);
                     }

                    :}
    |       T_NOT Designator:o {:
	            RESULT = ((MyParser) parser).DoOperation(new NotOp("!", o));
            :}
    |               T_SIZEOF T_LPAREN Designator:_2 T_RPAREN
                    {:
                        if(_2.isError()){
                            RESULT=_2;

                         }
                  else{

                   int size =  ((MyParser) parser).checkSizeOf(_2);

                   if(size < 0){

                    RESULT = new ErrorSTO(ErrorMsg.error19_Sizeof);

                   }
                   else{

                  RESULT = new ConstSTO(size+"", new IntType());

                      }
                    }

                    :}
    |               T_SIZEOF T_LPAREN Type:_2 T_RPAREN
                    {:


                 if(_2 == null){
                    RESULT= new ErrorSTO(ErrorMsg.error19_Sizeof);;
                 }
                 else{
                   int size =  ((MyParser) parser).checkSizeOf(_2);
                   if(size < 0){
                    RESULT = new ErrorSTO(ErrorMsg.error19_Sizeof);
                   }
                   else{
                 RESULT = new ConstSTO(size+"", new IntType());
                     }
                    }

                    :}
    |               T_LPAREN Type T_RPAREN Designator:_2
                    {:
			         RESULT = _2;
                    :}
    |               IncDecOp:op Designator:d {:

                         op.setOperand(d);

            RESULT = ((MyParser) parser).DoOperation(op);

        :}
    |               Designator2:_1
                    {:
                        System.out.println("des2");
                        RESULT = _1;
                    :}
    ;


Designator2 ::=     Designator2:struct T_DOT T_ID:id
                    {:
                        RESULT = ((MyParser) parser).DoDesignator2_Dot (struct, id);
                    :}
    |               Designator2:_1 T_LBRACKET Expr:_2 T_RBRACKET
                    {:
                        RESULT = ((MyParser) parser).DoDesignator2_Array (_1,_2);
                    :}
    |               Designator2:struct T_ARROW T_ID:id
                    {:
                        RESULT = ((MyParser) parser).DoDesignator2_Arrow (struct, id);
                    :}
    |               Designator2:_1 IncDecOp:_2
                    {:
                       _2.setOperand(_1);
                       ((DecIncOp) _2).setPost(true);
                       RESULT = ((MyParser) parser).DoOperation(_2);
                    :}
    |               Designator2:func T_LPAREN OptExprList:es T_RPAREN
                    {:
                        RESULT = ((MyParser) parser).DoFuncCall(func, es);
                    :}
    |               Designator3:_1
                    {:
                        System.out.println("des3");
                        RESULT = _1;
                    :}
    ;


Designator3 ::=     T_LPAREN Expr:_2 T_RPAREN
                    {:
                        RESULT = _2;
                    :}
    |               T_INT_LITERAL:_1
                    {:
                        System.out.println("conststo");
                        RESULT = new ConstSTO (_1, new IntType());
                    :}
    |               T_FLOAT_LITERAL:_1
                    {:
                        RESULT = ((MyParser) parser).DoFloatLit(_1);
                    :}
    |               T_STR_LITERAL:_1
                    {:
                        RESULT = new StringSTO (_1);
                    :}
    |               T_TRUE
                    {:
                        RESULT = new ConstSTO ("true", new BoolType());
                    :}
    |               T_FALSE
                    {:
                        RESULT = new ConstSTO ("false", new BoolType());
                    :}
    |               T_NULLPTR
                    {:
                        RESULT = new ConstSTO ("nullptr", new NullPtrType());
                    :}
    |               T_THIS
                    {:
                        RESULT = new ThisSTO();
                    :}
    |               T_COLONCOLON T_ID:_1
                    {:
                        RESULT = ((MyParser) parser).DoDesignatorGlobal3_ID (_1);
                    :}
    |               T_ID:_1
                    {:
                        System.out.println("id");
                        RESULT = ((MyParser) parser).DoDesignator3_ID (_1);
                    :}
    ;

QualIdent ::=       T_ID_U:id
                    {:
                        System.out.println("id");
                       RESULT = ((MyParser) parser).DoQualIdent(id);
                    :}
    ;
