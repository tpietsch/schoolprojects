abstract class NumericType extends BasicType {

  public NumericType(String strName, int size) {
    super(strName, size);
  }

  @Override public boolean isNumeric() { return true; }
}
