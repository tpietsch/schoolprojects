class PointerType extends PtrGrpType {

    private Type baseType;

    public PointerType(Type bt) {
        this(bt.getName() + "*", bt);
    }

    public PointerType(String n, Type t) {
        super(n);
        setBaseType(t);
    }

    public PointerType(PointerType pt) {
        this(pt.getBaseType());
    }

    @Override
    public boolean isEquivalent(Type t) {
        return super.isEquivalent(t) &&
            getBaseType().isEquivalent(((PointerType) t).getBaseType());
    }

    @Override
    public boolean isAssignable(Type t) {
        if (t.isArray()) {
            Type baseType = t.getType();
            return getBaseType().isEquivalent(baseType);
        }

        return isEquivalent(t) || t.isNullPtr();
    }

    @Override
    public PointerType deepCopy() {
        return new PointerType(this);
    }

    @Override
    public boolean isPointer() {
        return true;
    }

    private void setBaseType(Type t) {
        baseType = t;
    }

    public Type getBaseType() {
        return baseType;
    }

    public VarSTO deref(String pname) {
        System.out.println("\tWARNING(PointerType#deref()): " +
                "Stack pointer logic not yet implemented. Implement " +
                " before using.");

        return new VarSTO("(*" + pname + ")", getBaseType(), 0);
    }
}
