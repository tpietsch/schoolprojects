class StructType extends CompositeType {

    private StructFields fields;

    public StructType(String n, int s, StructFields fs) {
        super(n, s);
        setFields(fs);
    }

    public StructType(StructType t) {
        this(t.getName(), t.getSize(), t.getFields());
    }

    @Override
    public Type deepCopy() {
        return new StructType(this);
    }

    @Override
    public boolean isAssignable(Type t) {
        return false;
    }

    @Override
    public boolean isEquivalent(Type t) {
        return getName().equals(t.getName());
    }

    @Override
    public boolean isStruct() {
        return true;
    }

    public StructFields getFields() {
        return fields;
    }

    public void setFields(StructFields fs) {
        fields = fs;
    }
}
