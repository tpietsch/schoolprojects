class BoolType extends BasicType {

  static final int BOOL_SIZE = 4;

  public BoolType() {
    super("bool", BOOL_SIZE);
  }

  public BoolType(BoolType t) {
    super(t.getName(), t.getSize());
  }

  @Override
  public boolean isBool() { return true; }
  @Override
  public Type deepCopy() {
    return new BoolType(this);
  }
}
