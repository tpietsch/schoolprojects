class CharType extends BasicType {

  static final int CHAR_SIZE = 1;

  public CharType() {
    super("char", CHAR_SIZE);
  }

  public CharType(CharType t) {
    super(t.getName(), t.getSize());
  }

  @Override
  public boolean isChar() { return true; }

  @Override
  public Type deepCopy() {
    return new CharType(this);
  }
}
