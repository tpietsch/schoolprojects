import java.util.Vector;

class FuncPtrType extends PtrGrpType {

    private Type returnType;
    private boolean isReturnByReference;
    private Vector<ParameterSTO> parameters;

    private static final int FUNC_PTR_SIZE = 0;
    private static final String FUNC_NAME  = "function";

    public FuncPtrType(Type retTyp, boolean isRBR) {
        super(FUNC_NAME, FUNC_PTR_SIZE);
        setIsReturnByReference(isRBR);
        setReturnType(retTyp);
    }

    public FuncPtrType(FuncPtrType fpt) {
        this(fpt.getReturnType(), fpt.getIsReturnByReference());
    }

    @Override
    public FuncPtrType deepCopy() {
        return new FuncPtrType(this);
    }

    @Override
    public boolean isAssignable(Type t) {
        return false;
    }

    private Type setReturnType(Type rt) {
        return returnType = rt;
    }

    public Type getReturnType() {
        return returnType;
    }

    private boolean setIsReturnByReference(boolean isRBR) {
        return isReturnByReference = isRBR;
    }

    public boolean getIsReturnByReference() {
        return isReturnByReference;
    }

    public Vector<ParameterSTO> setParameters(Vector<ParameterSTO> ps) {
        parameters = new Vector<ParameterSTO>(ps);
        return parameters;
    }

    public Vector<ParameterSTO> getParameters() {
        return parameters;
    }
}
