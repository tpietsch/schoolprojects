/**
  * A `VoidType` fulfills every type.
  */
class VoidType extends Type {

  static final int VOID_SIZE = 0;

  public VoidType() {
    super("void", VOID_SIZE);
  }

  public VoidType(VoidType t) {
    super(t.getName(), t.getSize());
  }

  @Override
  public Type deepCopy() {
    return new VoidType(this);
  }

  @Override public boolean isEquivalent(Type t)  { return false; }
  @Override public boolean isAssignable(Type t)  { return false; }

  @Override public boolean isVoid()         { return true; }
  @Override public boolean isInt()         { return false; }
  @Override public boolean isNumeric()     { return false; }
  @Override public boolean isFloat()       { return false; }
  @Override public boolean isChar()        { return false; }
  @Override public boolean isBool()        { return false; }
  @Override public boolean isArray()       { return false; }
  @Override public boolean isStruct()      { return false; }
  @Override public boolean isPointer()     { return false; }
  @Override public boolean isFuncPointer() { return false; }
}
