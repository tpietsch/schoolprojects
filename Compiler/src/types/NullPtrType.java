class NullPtrType extends PointerType {

    public NullPtrType() {
        super("nullptr", new VoidType());
    }

    @Override
    public boolean isNullPtr() {
        return true;
    }
}
