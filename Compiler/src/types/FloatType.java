class FloatType extends NumericType {

  static final int FLOAT_SIZE = 4;

  public FloatType() {
    super("float", FLOAT_SIZE);
  }

  public FloatType(FloatType t) {
    super(t.getName(), t.getSize());
  }

  @Override
  public boolean isAssignable(Type t) {
    return isEquivalent(t) || t.isInt();
  }

  @Override
  public boolean isFloat() { return true; }

  @Override
  public Type deepCopy() {
    return new FloatType(this);
  }
}
