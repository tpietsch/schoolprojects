abstract class BasicType extends Type {

  public BasicType(String strName, int size) {
    super(strName, size);
  }

  @Override
  public boolean isAssignable(Type t) {
    return isEquivalent(t);
  }
}
