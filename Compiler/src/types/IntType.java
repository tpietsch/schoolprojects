class IntType extends NumericType {

  static final int INT_SIZE = 4;

  public IntType() {
    super("int", INT_SIZE);
  }

  public IntType(IntType t) {
    super(t.getName(), t.getSize());
  }

  @Override
  public boolean isInt () { return true; }

  @Override
  public Type deepCopy() {
    return new IntType(this);
  }
}
