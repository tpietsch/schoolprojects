class ArrayType extends CompositeType {

    private Type type;
    private int length;

    public ArrayType(Type type, int length) {
        super(type.getName() + "[" + length + "]", type.getSize() * length);

        this.type   = type;
        this.length = length;
    }

    public ArrayType(ArrayType t) {
        this(t.getType(), t.getLength());
    }

    public Type getType() {
        return type;
    }

    public int getLength() {
        return length;
    }

    @Override
    public boolean isAssignable(Type t) {
        return isEquivalent(t);
    }

    @Override
    public boolean isArray() {
        return true;
    }

    @Override
    public Type deepCopy() {
        return new ArrayType(this);
    }
}
