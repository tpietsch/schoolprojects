abstract class PtrGrpType extends CompositeType {

    private static final int PTR_SIZE = 4;

    public PtrGrpType(String strName) {
        this(strName, PTR_SIZE);
    }

    public PtrGrpType(String strName, int size) {
        super(strName, size);
    }

    @Override
    public boolean isPointer() {
        return true;
    }
}
