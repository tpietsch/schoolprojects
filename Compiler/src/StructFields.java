import java.util.Vector;

class StructFields extends Scope {

    public StructFields(Scope s) {
        super((Vector<STO>) s.getLocals().clone());
    }

    public int getSize() {
        int size = 0;
        for (STO field : getLocals()) {
            if (null != field.getType()) {
                size += field.getType().getSize();
            }
        }

        return size;
    }
}
