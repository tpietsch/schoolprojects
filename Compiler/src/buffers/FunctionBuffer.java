/**
 * TextBuffer for a function
 */

class FunctionBuffer extends TextBuffer {

    public final String INIT_DONE_LABEL = "__init_done";
    public final String INIT_LABEL = "__init";

    private Buffer body;
    private String funcName;
    private int stackPointer = 4;
    private String customInitDone;
    private String customInit;

    private Buffer staticVarStos;

    public FunctionBuffer(String name) {
        super();

        setFuncName(name);
        setCustomInitLabel(name);
        setCustomInitDoneLabel(name);

        writeHeader();
        writeGlobalInitializationGuard();

        body = new Buffer();
        body.writeComment("---Funtion body---\n\n");
        body.writeLabel(getCustomInitDoneLabel());
        body.writeBlankLine();

        staticVarStos = new Buffer();
        staticVarStos.writeComment("---Static Initializations---\n\n");
    }

    public void writeStaticVarInitialization(VarSTO var) {
        staticVarStos.writeln(var.generateInitialization());
    }

    @Override
    public String toString() {
        writeln(staticVarStos.toString());
        writeln(body.toString());
        writeFooter();
        return super.toString();
    }

    /**
     * Adds a statement to the body buffer.
     */
    public void addBodyBuffer(String st) {
        body.write(st + "\n"); 
    }

    public void writeLocalVarInitialization(VarSTO var) {
        addBodyBuffer(var.generateInitialization());
    }

    /**
     * Returns the current stackPointer of the function, while
     * incrementing the stack pointer to accomidate the size
     * of the value that will be stored there.
     */
    public int getStackPointer(int allocatedSize) {
        stackPointer += allocatedSize;

        return -stackPointer;
    }

    public String getFuncName() {
        return funcName;
    }

    public String getCustomInitDoneLabel() {
        return customInitDone;
    }

    public String getCustomInitLabel() {
        return customInit;
    }

    public void writeRet() {
        String op = "ret";
        writeZeroOpCommand(op);
    }

    /////////////////////////////////////////////////////////////////////////
    // Privates
    /////////////////////////////////////////////////////////////////////////

    private void writeGlobalInitializationGuard() {
        writeComment("---Global initialization guard---");
        writeSet(getCustomInitLabel(), "%l0");
        writeLoad("%l0", "%l1");
        writeCmp("%l1", "%g0");
        writeBne(getCustomInitDoneLabel());
        writeSet("1", "%l1");
        writeStore("%l1", "%l0");
    }

    private void setFuncName(String n) {
        funcName = n;
    }

    /**
     * Writes initial instructions to set up the function.
     */
    private void writeHeader() {
        writeGlobal(funcName);
        writeAlignment("4");
        writeLabel(funcName);
        writeBlankLine();

        writeSave(funcName);
        writeBlankLine();
    }

    /**
     * Writes final instructions to close down the function.
     */
    private void writeFooter() {
        writeRet();
        writeRestore();
        writeBlankLine();
        writeSaveVariable();
    }

    /**
     * Assigns the local assembly variable.
     *
     * The local assembly variable, named after the function, is
     * defined here, assigned the total size of the function.
     */
    private void writeSaveVariable() {
        writeln("SAVE." + getFuncName() + " = -(92 + " +
                stackPointer + ") & -8");
    }

    private void writeRestore() {
        String op = "restore";
        writeZeroOpCommand(op);
    }

    private void setCustomInitLabel(String name) {
        customInit = INIT_LABEL + "_" + name;
    }

    private void setCustomInitDoneLabel(String name) {
        customInitDone = INIT_DONE_LABEL + "_" + name;
    }
}
