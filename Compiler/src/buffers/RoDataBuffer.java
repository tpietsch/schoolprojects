/**
 * Prints assembly for storage in data that will be read only.
 */

class RoDataBuffer extends Buffer {

    public final static String SECTION_NAME = "\".rodata\"";
    public final static String DEFAULT_ALIGNMENT = "4";

    private final static String STR_ENCODING = ".asciz";
    protected final static String STRING_FMT = "\"%s\"";
    protected final static String INT_FMT = "\"%d\"";
    protected final static String ENDL = "\"\\n\"";
    protected final static String BOOL_T_CHAR = "\"true\"";
    protected final static String BOOL_F_CHAR = "\"false\"";
    protected final static String BOUND = "\"Index value of %d is outside legal range [0,%d).\\n\"";

    public RoDataBuffer() {
        super();
        writeComment("---Default String Formatters---");
        writeSection();
        writeAlignment();
        writeLabels();
    }

    private void writeLabels() {
        writeln("__int_fmt:\t" + STR_ENCODING + "\t\t" + INT_FMT);
        writeln("__endl:\t\t" + STR_ENCODING + "\t\t" + ENDL);
        writeln("__bool_t:\t" + STR_ENCODING + "\t\t" + BOOL_T_CHAR);
        writeln("__bool_f:\t" + STR_ENCODING + "\t\t" + BOOL_F_CHAR);
        writeln("__string_fmt:\t" + STR_ENCODING + "\t\t" + STRING_FMT);
        writeln("__bound:\t" + STR_ENCODING + "\t\t" + BOUND);

    }

    public void writeSection() {
        writeSection(SECTION_NAME);
    }

    public void writeAlignment() {
        writeAlignment(DEFAULT_ALIGNMENT);
    }
}

