/**
 * Prints assembly for storage in data that will be read only.
 */

class DataBuffer extends Buffer {

    private static int floatLabelCounter = 0;

    public final static String SECTION_NAME = "\".data\"";
    public final static String DEFAULT_ALIGNMENT = "4";

    protected final static String INT_FMT = "\"%d\"";
    protected final static String ENDL = "\"\\n\"";
    protected final static String BOOL_T_CHAR = "\"true\"";
    protected final static String BOOL_F_CHAR = "\"false\"";

    public DataBuffer() {
        super();
    
        writeSection();
        writeAlignment();
    }

    // Returns float name.
    public String addFloat(String val) {
        String serializedValue = "0r" + val;
        String comment = "Constant value for " + val;
        String name = "__float_" + floatLabelCounter++;

        writeTabbedComment(comment);
        writeln(name + ":\t\t.single\t\t" + serializedValue);
        writeln("");

        return name;
    }

    private void writeSection() {
        writeSection(SECTION_NAME);
    }

    private void writeAlignment() {
        writeAlignment(DEFAULT_ALIGNMENT);
    }
}

