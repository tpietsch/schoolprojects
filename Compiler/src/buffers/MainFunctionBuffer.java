/**
 * TextBuffer for a main function.
 *
 * A main function has special characteristics in that
 * it initializes global variables.
 */

import java.io.StringWriter;

class MainFunctionBuffer extends FunctionBuffer {

    private final static String MAIN_NAME = "main";

    private StringWriter globalVarStos;

    public MainFunctionBuffer() {
        super(MAIN_NAME);

        writeBlankLine();

        globalVarStos = new StringWriter();
        globalVarStos.write("! ---Global Initializations---\n\n");
    }

    public void writeGlobalVarInitialization(VarSTO var) {
        globalVarStos.write(var.generateInitialization());
    }

    @Override
    public String toString() {
        Buffer buf = new Buffer();
        buf.writeln(globalVarStos.toString());
        buf.writeln(super.toString());
        /// exit
        buf.writeLabel("__exitArrayIndex");
        buf.writeCall("printf");

       // buf.writeSet("__endl","%o0");
        //buf.writeCall("printf");
        buf.writeCall("exit");

        return buf.toString();
    }

    /////////////////////////////////////////////////////////////////////////
    // Privates
    /////////////////////////////////////////////////////////////////////////
}

