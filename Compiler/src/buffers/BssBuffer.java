/**
 * Stores assembly code to be printed in the ".data" section.
 *
 * At the end of the program DataBuffer will be written to a target
 * assembly file.
 */

class BssBuffer extends Buffer {

    public final static String SECTION_NAME = "\".bss\"";
    public final static String INIT_LABEL = "__init";
    public final static String DEFAULT_ALIGNMENT = "4";

    public BssBuffer() {
        super();
        writeComment("---Global Variables---");
        writeSection();
        writeAlignment();
    }

    /**
     * Places a label for a global or static variable.
     */
    public void varStoDecl(VarSTO var) {
        int size = var.getType().getSize();

        if (var.getIsGlobal() && !var.getIsStatic()) {
            writeGlobal(var.getOffset());
        }

        writeVarDecl(var.getOffset(), size);
    }

    public void writeGuard(String name) {
        writeLabel(name);
        writeSkip("4");
    }

    private void writeSection() {
        writeSection(SECTION_NAME);
    }

    private void writeAlignment() {
        writeAlignment(DEFAULT_ALIGNMENT);
    }
}

