/**
 * Buffer for text sections.
 *
 * Text sections normally contain function implementations.
 */

class TextBuffer extends Buffer {
    
    private final String SECTION_NAME = "\".text\"";

    public TextBuffer() {
        super();
        writeSection(SECTION_NAME);
    }

    protected void writeSave(String name) {
        String comName = "save";
        writeSet(saveSpace(name), "%g1");
        writeThreeOpCommand(comName, "%sp", "%g1", "%sp");
    }

    private String saveSpace(String name) {
        return "SAVE." + name;
    }
}

