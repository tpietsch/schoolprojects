/**
 * Stores assembly code in a StringWriter.
 *
 * At the end of program, assembly will be dumpt to a file.
 */

import java.io.StringWriter;

class Buffer {

    public final static String DEFAULT_ALIGNMENT = "4";
    private StringWriter buffer;

    public Buffer() {
        buffer = new StringWriter();
    }

    public String toString() {
        return buffer.toString();
    }

    public void writeComment(String str) {
        writeln("! " + str);
    }

    public void writeAdd(String op1, String op2, String dest) {
        String name = "add";
        writeThreeOpCommand(name, op1, op2, dest);
    }

    public void writeSet(String op, String dest) {
        String name = "set";
        writeTwoOpCommand(name, op, dest);
    }

    public void writeMov(String op, String dest) {
        String name = "mov";

        writeTwoOpCommand(name, op, dest);
    }

    public void writeTabbedComment(String str) {
        writeTabbedln("! " + str);
    }

    public void writeSection(String str) {
        writeTabbedln(".section\t" + str);
    }
    
    public void writeBlankLine() {
        writeln("");
    }

    public void writeLoad(String addr, String dest) {
        String name = "ld";
        writeTwoOpCommand(name, "[" + addr + "]", dest);
    }

    public void writeStore(String source, String addr) {
        String name = "st";
        writeTwoOpCommand(name, source, "[" + addr + "]");
    }

    public void writeStoreIndex(String source, String addr,String index) {
        
        String name = "st";
        writeAdd(index,addr,addr);
        writeTwoOpCommand(name, source, "[" + addr + "]");
    }


    public void writeVarDecl(String name, int size) {
        write(name + ":");
        writeSkip("" + size);
    }


    ////trever did this for vars withi init vals
    public void writeVarDeclWithInit(String name) {
        writeln(name);
        writeSkip("4");
    }

    protected void writeBne(String label) {
        String name = "bne";

        writeOneOpCommand(name, label);
        writeNop();
    }

    protected void writeBl(String label) {
        String name = "bl";

        writeOneOpCommand(name, label);
        writeNop();
    }

    protected void writeBge(String label) {
        String name = "bge";

        writeOneOpCommand(name, label);
        writeNop();
    }

    protected void writeBg(String label) {
        String name = "bg";

        writeOneOpCommand(name, label);
        writeNop();
    }


    protected void writeB(String label) {
        String name = "b";

        writeOneOpCommand(name, label);
        writeNop();
    }

    public void writeCall(String func) {
        writeComment("-Calling " + func + "-");
        writeOneOpCommand("call", func);
        writeNop();
    }

    public void writeCmp(String reg1, String reg2) {
        String name = "cmp";

        writeTwoOpCommand(name, reg1, reg2);
    }

    public void writeNop() {
        String name = "nop";

        writeZeroOpCommand(name);
    }

    public void writeBe(String label) {
        String name = "be";

        writeOneOpCommand(name, label);
        writeNop();
    }

    public void writeLabel(String str) {
        write(str + ":");
    }
    
    public void writeFitos(String reg, String dest) {
        String name = "fitos";

        writeTwoOpCommand(name, reg, dest);
    }

    public void writeFstoi(String reg, String dest) {
        String name = "fstoi";

        writeTwoOpCommand(name, reg, dest);
    }

    public void writeGlobal(String n) {
        String com = ".global";
    
        writeOneOpCommand(com, n);
    }

    public void writeAlignment(String str) {
        writeTabbedln(".align\t\t" + str);
    }

    public void writeSkip() {
        writeSkip(DEFAULT_ALIGNMENT);
    }

    public void writeSkip(String str) {
        writeTabbedln(".skip\t\t" + str);
    }

    public void writeTabbedln(String str) {
        writeln("\t\t" + str);
    }

    public void writeln(String str) {
        write(str + "\n");
    }

    public void write(String str) {
        buffer.write(str);
    }

    public void writeZeroOpCommand(String op) {
        writeTabbedln(op);
    }

    public void writeOneOpCommand(String name, String op) {
        writeTabbedln(name + "\t\t" + op);
    }

    public void writeTwoOpCommand(String name, String op, String dest) {
       writeTabbedln(name + "\t\t" + op + ", " + dest); 
    }

    public void writeThreeOpCommand(String name, String op1, String op2,
            String dest) {
       writeTabbedln(name + "\t\t" + op1 + ", " + op2 + ", " + dest); 
    }
}

