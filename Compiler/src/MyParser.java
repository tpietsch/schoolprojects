//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

import java_cup.runtime.*;
import java.util.Vector;
import java.util.HashMap;
import java.lang.IllegalStateException;
import java.util.*;

class MyParser extends parser {

    private static int staticLabeler = 0;
    private Lexer m_lexer;
    private ErrorPrinter m_errors;
    private int m_nNumErrors;
    private String m_strLastLexeme;
    private boolean m_bSyntaxError = true;
    private int m_nSavedLineNum;

    private int labelCounter = 0;
 
    private SourceGenerator generator;
    private VarSourceBuffer varSourceBuffer;
    private BssBuffer bssBuffer;
    private RoDataBuffer roDataBuffer;
    private MainFunctionBuffer mainBuffer;
    private Buffer allNonMainFunctions;
    private DataBuffer dataBuffer;

    privat te Stack<String> labels = new Stack<String>();
    private Stack<String> loopLabels = new Stack<String>();
    private Stack<String> loopBackLabels = new Stack<String>();

    private boolean ifFlag = false;
    private boolean retFlag = false;

    private boolean forflag = false;
    private boolean whileflag = false;
    private boolean cont = false;
    private boolean brk = false;

    private int whilecount = 0;


    private SymbolTable m_symtab;

    public MyParser(Lexer lexer, ErrorPrinter errors)
    {
        m_lexer = lexer;
        m_symtab = new SymbolTable();
        m_errors = errors;
        m_nNumErrors = 0;

        generator = new SourceGenerator();
        varSourceBuffer = new VarSourceBuffer();

    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void DoNewStmtCheck(STO s){
        if(s == null){
            //handleError(ErrorMsg.error16_New_var);
            return;
        }
        if(s.isError()){
            return;
        }
        if(!s.isModLValue()){
            handleError(ErrorMsg.error16_New_var);
        }

        if(!s.getType().isPointer()){

            handleError(Formatter.toString(
                        ErrorMsg.error16_New, s.getType().getName()));
        }
    }

    public void DoDeleteStmtCheck(STO s){

        if(s == null){
            //handleError(ErrorMsg.error16_Delete_var);
            return;
        }
        if(s.isError()){
            return;
        }
        if(!s.isModLValue()){
            handleError(ErrorMsg.error16_Delete_var);
            return;
        }
        if(!s.getType().isPointer()){

            handleError(Formatter.toString(ErrorMsg.error16_Delete, s.getType().getName()));
        }

    }

    void DoExpressionStatement(STO s) {
        if (!s.isExpr()) {
            return;
        }

        ExprSTO expr = (ExprSTO) s;
        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
        fnBuf.addBodyBuffer(expr.loadVarInReg("%g0"));
    }

    void GenerateDes(ExprSTO d) {
        STO sto = m_symtab.access(d.getName());
        if (!sto.isVar()) {
            System.out.println("MyParser#GenerateDes(): Not a var.");
            return;
        }

        VarSTO var = (VarSTO) sto;

    }

    public int checkSizeOf(Type z){


        if(z == null){
            handleError(ErrorMsg.error19_Sizeof);

            return -1;
        }
        Type t = z;
        if(t.isArray()){
            return t.getLength() * t.getType().getSize( );
        }


        return t.getSize();
    }
    public int checkSizeOf(STO z){


        if(z == null){
            handleError(ErrorMsg.error19_Sizeof);

            return -1;
        }
        Type t = z.getType();
        if(t == null){
            handleError(ErrorMsg.error19_Sizeof);

            return -1;
        }
        if(t.isArray()){
            return t.getLength() * t.getType().getSize();
        }

        if(!z.getIsAddressable()){
            handleError(ErrorMsg.error19_Sizeof);
            return -1;
        }

        return t.getSize();
    }

    public void setCont(boolean t){
        //branch to the peek;
        FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();
        Buffer b = new Buffer();
        b.writeB(loopBackLabels.peek());
        fnBuffer.addBodyBuffer(b.toString());
        if(whilecount == 0){
            handleError(ErrorMsg.error12_Continue);
            return;
        }
    }

    public void setBreak(boolean t){

        //branch to the peek;
        FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();
        Buffer b = new Buffer();
        b.writeB(loopLabels.peek());
        fnBuffer.addBodyBuffer(b.toString());
        
        if(whilecount == 0){
            handleError(ErrorMsg.error12_Break);
            return;
        }
    }

    public void setWhile(boolean t){

        if(t){
            whilecount++;
        }
        else{
            whilecount--;
        }


    }

    public void setFor(boolean t){
        if(t){
            whilecount++;
        }
        else{
            whilecount--;
        }

    }


    public void setIfTag(boolean f){
        ifFlag = f;
    }

    public void setRetTag(boolean f){
        retFlag = f;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public boolean Ok()
    {
        return m_nNumErrors == 0;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public Symbol scan()
    {
        Token t = m_lexer.GetToken();

        //	We'll save the last token read for error messages.
        //	Sometimes, the token is lost reading for the next
        //	token which can be null.
        m_strLastLexeme = t.GetLexeme();

        switch (t.GetCode())
        {
            case sym.T_ID:
            case sym.T_ID_U:
            case sym.T_STR_LITERAL:
            case sym.T_FLOAT_LITERAL:
            case sym.T_INT_LITERAL:
                return new Symbol(t.GetCode(), t.GetLexeme());
            default:
                return new Symbol(t.GetCode());
        }
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void syntax_error(Symbol s)
    {
    }


    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void report_fatal_error(Symbol s)
    {
        m_nNumErrors++;
        if (m_bSyntaxError)
        {
            m_nNumErrors++;

            //	It is possible that the error was detected
            //	at the end of a line - in which case, s will
            //	be null.  Instead, we saved the last token
            //	read in to give a more meaningful error
            //	message.
            m_errors.print(Formatter.toString(ErrorMsg.syntax_error, m_strLastLexeme));
        }
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void unrecovered_syntax_error(Symbol s) {
        report_fatal_error(s);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void DisableSyntaxError()
    {
        m_bSyntaxError = false;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void EnableSyntaxError()
    {
        m_bSyntaxError = true;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public String GetFile()
    {
        return m_lexer.getEPFilename();
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public int GetLineNum()
    {
        return m_lexer.getLineNumber();
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void SaveLineNum()
    {
        m_nSavedLineNum = m_lexer.getLineNumber();
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public int GetSavedLineNum()
    {
        return m_nSavedLineNum;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    void DoProgramStart() {
        // Opens the global scope.
        m_symtab.openScope();
        bssBuffer = new BssBuffer();
        roDataBuffer = new RoDataBuffer();
        mainBuffer = new MainFunctionBuffer();
        allNonMainFunctions = new Buffer();
        dataBuffer = new DataBuffer();
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    void DoProgramEnd() {
        m_symtab.closeScope();
        generator.println(roDataBuffer.toString());
        generator.println(dataBuffer.toString());
        generator.println(bssBuffer.toString());
        generator.println(allNonMainFunctions.toString());
    }

    /**
     * Writes the instructions for a cout statement to the
     * body of the current function.
     */
    void DoWriteStmt(CoutGenerator coutGen) {

        if (null == coutGen) {
            return;
        } 

        coutGen.print();
        String coutInstructions = coutGen.toString();

        FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();
        fnBuffer.addBodyBuffer(coutInstructions);
    }

    CoutGenerator DoCout() {
        return new CoutGenerator(dataBuffer);
    }

    void DoCin(STO s){

        if(s == null){
            //handleError(ErrorMsg.error16_New_var);
            return;
        }
        if(s.isError()){
            return;
        }
        if(!s.isModLValue()){
            //handleError(ErrorMsg.error16_New_var);
            return;
        }

        if (!s.isData()) {
            return;
        }

        DataSTO dsto = (DataSTO) s;
        Type t = s.getType();

        FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();

        Buffer code = new Buffer();

        if(t.isInt()){
            //call input int take res from %o0 to use
            code.writeCall("inputInt");
            code.write(dsto.storeRegInVar("%o0"));
            
        } else if (t.isFloat()) {
            //call input float result in %oO to use
            code.writeCall("inputFloat");
            code.write(dsto.storeRegInVar("%f0"));
        }

        fnBuffer.addBodyBuffer(code.toString());

    }

    void arrayInitCheck(Type array, STO elemList){

        if (!elemList.isEmpty()) {
            Vector<STO> k;
            k = elemList.getList();
            int s = array.getLength();
            Type t = array.getType();



            if(s < k.size()){

                handleError(ErrorMsg.error11_TooManyInitExpr);
                return;
            }

            for(int i = 0;i < k.size();i++){

                if(i > s){
                    handleError(Formatter.toString(
                                ErrorMsg.error11b_ArrExp,
                                i,s));
                    return;
                }

                if(k.elementAt(i).isConst()){

                    if (!t.isAssignable(((Consty) k.elementAt(i)).getConstSTO().getType())){
                        handleError(Formatter.toString(
                                    ErrorMsg.error3b_Assign,
                                    ((Consty) k.elementAt(i)).getConstSTO().getType().getName(),t.getName()));
                        return;
                    }
                }
                else{
                    handleError(ErrorMsg.error11_NonConstInitExpr);
                    return;
                }
            }
        }
    }

    ConstSTO DoFloatLit(String val) {
        String name = dataBuffer.addFloat(val);

        ConstSTO csto = new ConstSTO(name, new FloatType(), val);  

        return csto;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    void DoVarDecl(Type type, boolean isStatic, Vector<IdInfo> idInfos) {
        Vector<STO> vars = new Vector<STO>();

        for (IdInfo idInfo : idInfos) {
            String id = idInfo.getId();
            STO expr  = idInfo.getExpr();

            if (m_symtab.accessLocal(id) != null) {
                handleError(Formatter.toString(ErrorMsg.redeclared_id, id));
                return;
            }

            Type varType = idInfo.generateType(type);

            VarSTO var;
            if (m_symtab.isGlobal()) {
                var = new VarSTO(id, varType);
            } else if (isStatic) {
                String curFuncName = m_symtab.getFunc().getName();
                String varLabel = id + "_" + curFuncName + staticLabeler++;
                var = new VarSTO(id, varType, varLabel);
            } else {
                FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();
                int sp = fnBuffer.getStackPointer(varType.getSize());

                //System.out.println("\t" + id + " stack addr: " + sp);
                var = new VarSTO(id, varType, sp);
            }

            var.setIsStatic(isStatic);

            if (varType.isVoid()) {
                return;
            }

            if (null != expr && !expr.isEmpty()) {
                if (expr.isError()) {
                    return;
                }

                if (expr.isExpr() || expr.isVar()) {
                    if (!varType.isAssignable(expr.getType())) {
                        handleError(Formatter.toString(
                                    ErrorMsg.error3b_Assign,
                                    expr.getType().getName(),
                                    varType.getName()));
                        return;
                    }
                }

                if (varType.isArray() && expr.isArraySTO()) {
                    arrayInitCheck(varType, expr);
                }

                var.setValue(expr);

                // Now that expr is sane, initialize.
                if (var.getIsGlobal() ) {
                    mainBuffer.writeGlobalVarInitialization(var);
                } else if (var.getIsStatic()) {
                    FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();
                    fnBuffer.writeStaticVarInitialization(var);
                } else {
                    FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();
                    fnBuffer.writeLocalVarInitialization(var);
                }
            }

            if (m_symtab.isGlobal() || isStatic) {
                bssBuffer.varStoDecl(var);
            }


            m_symtab.insert(var);
        }
    }

    STO DoDereference(STO sto) {
        if(sto.isError()){
            return sto;
        }
        if (!sto.getType().isPointer()) {
            String errMsg = Formatter.toString(ErrorMsg.error15_Receiver,
                    sto.getType().getName());
            handleError(errMsg);
            return new ErrorSTO(errMsg);
        }

        PointerType ptrType = (PointerType) sto.getType();

        return ptrType.deref(sto.getName());
    }

    STO DoAddressOf(STO sto) {
        if (sto.isError()) {
            return sto;
        }

        if (!sto.getIsAddressable()) {
            handleError(Formatter.toString(
                        ErrorMsg.error21_AddressOf,
                        sto.getType().getName()));
            return new ErrorSTO("");
        }

        Type ptrType = new PointerType(sto.getType());

        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
        int sp = fnBuf.getStackPointer(ptrType.getSize());

        return new ExprSTO("(&" + sto.getName() + ")", ptrType, sp);
    }

    void bufferVarDecl(STO sto) {
        if (sto.isError()) {
            throw new IllegalStateException(sto.getName() + " is an ErrorSTO");
        }
        if (!sto.isVar()) {
            throw new IllegalStateException(sto.getName() + " is not a VarSTO");
        }

        VarSTO var = (VarSTO) sto;
        varSourceBuffer.add(var);
    }

    STO DoModifyId(Integer ms, STO opt, String id) {
        return null;
    }

    void DoFuncFieldDecl(STO f) {
        String id = f.getName();
        m_symtab.insert(f);
    }

    STO DoIterationVarDecl(Type t, boolean isRef, String id) {
        VarSTO var;
        boolean isGlobal = m_symtab.isGlobal();
        if (isGlobal) {
            var = new VarSTO(id, t);
        } else {
            FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();
            int sp = fnBuffer.getStackPointer(t.getSize());

            var = new VarSTO(id, t, sp);
        }

        var.setIsRef(isRef);
       // m_symtab.insert(var);
        return var;
    }

    void DoVarFieldDecl(Type t, Vector<IdInfo> idInfos) {
        if (t == null) { // Error.
            return;
        }
        for (IdInfo idInfo : idInfos) {
            if (!insertIdInfoIntoTable(t, idInfo)) {
                return;
            }
        }
    }

    private boolean insertIdInfoIntoTable(Type t, IdInfo idInfo) {
        String id = idInfo.getId();
        if (m_symtab.hasLocal(id)) {
            handleError(Formatter.toString(ErrorMsg.error13a_Struct, id));
            return false;
        }
        Type varType = idInfo.generateType(t);

        System.out.println(
                "\tWARNING(MyParser#insertIdInfoIntoTable()): " +
                "Struct fields has not been implemented. " +
                "Implement stack pointer logic before continuing.");
        m_symtab.insert(new VarSTO(id, varType, 0));

        return true;
    }

    HashMap<String, STO> DoFieldValidation(HashMap<String, STO> fields,
            HashMap<String, STO> newField) {
        for (String id : newField.keySet()) {
            if (fields.containsKey(id)) {
                handleError(Formatter.toString(ErrorMsg.error13a_Struct, id));
                return fields;
            }

            fields.put(id, newField.get(id));
        }

        return fields;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    void DoFieldDecl(Vector<String> ids, Type type) {
        for (String id : ids) {
            if (m_symtab.accessLocal(id) != null) {
                handleError(Formatter.toString(ErrorMsg.error13a_Struct, id));
            } else {
                System.out.println(
                        "\tWARNING(MyParser#DoFieldDecl()): " +
                        "Struct fields has not been implemented. " +
                        "Implement stack pointer logic before continuing.");
                VarSTO sto = new VarSTO(id, type, 0);
                m_symtab.insert(sto);
            }
        }
    }


    //----------------------------------------------------------------
    //
    void DoWhileClose(){
        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
        String bFalse = closingLabel();
        String bTrue = closingLabel();
        Buffer b = new Buffer();

        loopLabels.pop();
        loopBackLabels.pop();

        b.writeB(bTrue);
        b.writeLabel(bFalse);
         b.writeln("");
         b.writeComment("--------loop closing------");
        fnBuf.addBodyBuffer(b.toString());

      

    }
    STO DoCondCheckWhile(STO t){

            if (t == null) {
            return t;
        }
        if (t.isError()) {
            return t;
        }



       boolean isWellFormed = t.getType().isBool() || t.getType().isInt();
        if (isWellFormed) {

            //writeCondAsm(t);
         FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();

        if (null == fnBuf || !t.isData()) {
            return t;
        }

     

        DataSTO bool = (DataSTO) t;

        Buffer b = new Buffer();

        String ttt = generateLabel("IfL1");
        String f = generateLabel("IfL1");

        b.writeLabel(ttt);
        
        b.writeln("");
        b.write(bool.loadVarInReg("%l0"));
        b.writeCmp("%l0", "%g0");

        b.writeBe(f);
        
        loopLabels.push(f);
        loopBackLabels.push(ttt);

        fnBuf.addBodyBuffer(b.toString());

        }
        
         return t;

     }

     void BranchOverElse(){

        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();

        String endIf =  labels.pop();
        String endElse = generateLabel("IfL1");
        
       
        

        Buffer b = new Buffer();
         b.writeB(endElse);

        b.writeLabel(endIf);
         b.writeln("");
        fnBuf.addBodyBuffer(b.toString());

     }
   

    STO DoCondCheck(STO t) {
        if (t == null) {
            return t;
        }
        if (t.isError()) {
            return t;
        }

        boolean isWellFormed = t.getType().isBool() || t.getType().isInt();

        if (isWellFormed) {
            writeCondAsm(t);

            return t;
        } else {
            m_nNumErrors++;
            m_errors.print(Formatter.toString(
                        ErrorMsg.error4_Test,
                        t.getType().getName()));

            return new ErrorSTO(Formatter.toString(
                        ErrorMsg.error4_Test,
                        t.getType().getName()));
        }
    }

    String ExitLabel() {
        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
        if (null == fnBuf) {
            return null;
        }
        String s = closingLabel();
        fnBuf.addBodyBuffer(s+":");
        return s;
    }

    private void writeCondAsm(STO t) {
        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();

        if (null == fnBuf || !t.isData()) {
            return;
        }

     

        DataSTO bool = (DataSTO) t;

        Buffer b = new Buffer();
        
        b.write(bool.loadVarInReg("%l0"));
        b.writeCmp("%l0", "%g0");

        b.writeBe(generateLabel("IfL1"));
        

        fnBuf.addBodyBuffer(b.toString());
 
        
    }

    /*
     * The label counter will always increment to ensure that there
     * are no duplicate labels. The label will be added to the a label
     * stack for cases such as nested if statements or loops.
     */
    private String generateLabel(String label) {
        labelCounter++;
        String currentLabel = label + labelCounter;
        labels.add(currentLabel);

        return currentLabel;
    }

    /*
     * Returns and pops a label from the labels stack.
     *
     * This is for exit labels for if statements and loops.
     */
    private String closingLabel() {
        return labels.pop();
    }

     private String closingLabelPeek() {
        return labels.peek();
    }

    void DoReturnStmtCheck(STO o)
    {
        if(o == null){
            return;
        }



        if(o.isError()){
            if (ifFlag) {
                this.setRetTag(false);

            }
            if (!ifFlag) {
                this.setRetTag(true);
            }
            return;
        }

        FuncSTO fnc = m_symtab.getFunc();

        Type funcT = fnc.getReturnType();
        Type retT = o.getType();
        boolean bothVoid = funcT.isVoid() && retT.isVoid();

        if (ifFlag) {
            this.setRetTag(false);
        }
        if (!ifFlag) {
            this.setRetTag(true);
        }

        //func type not void and no ret type specifiied ( therfore return void type)
        if(!bothVoid) {
            if (!funcT.isVoid() && retT.isVoid()) {
                m_nNumErrors++;
                m_errors.print(ErrorMsg.error6a_Return_expr);
                return;
            }

            if (!fnc.getIsReturnByReference()) {

                if (!funcT.isAssignable(retT)){
                    m_nNumErrors++;
                    m_errors.print(Formatter.toString(ErrorMsg.error6a_Return_type, retT.getName(),funcT.getName()));
                    return;
                }

            }

            if(fnc.getIsReturnByReference()){
                if (!funcT.isAssignable(retT)) {
                    m_nNumErrors++;
                    m_errors.print(Formatter.toString(ErrorMsg.error6b_Return_equiv, retT.getName(),funcT.getName()));
                    return;
                }

                if (!o.isModLValue()) {
                    m_nNumErrors++;
                    m_errors.print(ErrorMsg.error6b_Return_modlval);
                    return;
                }

            }
        }

        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
     

        DataSTO tt = (DataSTO) o;


        fnBuf.addBodyBuffer(tt.loadVarInReg("%o0"));
        fnBuf.addBodyBuffer("\tret\n");
        fnBuf.addBodyBuffer("\trestore\n");
      

    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    void DoFuncDecl_2()
    {
        //check for return statement somwhere

        boolean hasReturnStatement =
            !retFlag && !m_symtab.getFunc().getReturnType().isVoid();

        if (hasReturnStatement) {
            handleError(ErrorMsg.error6c_Return_missing);
        }

        m_symtab.closeScope();
        this.setRetTag(false);

        // End function buffer.
        FuncSTO f = m_symtab.getFunc();
        FunctionBuffer fnBuf = f.getBuffer();
        allNonMainFunctions.writeln(fnBuf.toString());
        allNonMainFunctions.writeComment("---End Function " + f.getName() +
                "---");
        allNonMainFunctions.writeln("");

        m_symtab.setFunc(null);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    STO DoExitCheck(STO t) {
        if (t.isError()) {
            return t;
        }

        boolean isWellFormed =
            t.getType().isInt() ||
            t.getType().isAssignable(new IntType());

        if (isWellFormed) {
            writeExitAsm(t);
            return t;
        } else {
            m_nNumErrors++;
            m_errors.print(Formatter.toString(ErrorMsg.error7_Exit,
                        t.getType().getName()));
            return new ErrorSTO(Formatter.toString(ErrorMsg.error4_Test,
                        t.getType().getName()));
        }
    }

    private void writeExitAsm(STO t) {
        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
        if (null == fnBuf || !t.isData()) {
            return;
        }

        DataSTO exitCode = (DataSTO) t;
         Buffer b = new Buffer();
        b.write(exitCode.loadVarInReg("%o0"));
        b.writeCall("exit");

        fnBuf.addBodyBuffer(b.toString());
 
    }

    void DoAutoVarDecl(String id, Type type, boolean isStatic) {
        if (m_symtab.accessLocal(id) != null) {
            m_nNumErrors++;
            m_errors.print(Formatter.toString(ErrorMsg.redeclared_id, id));
        }

        VarSTO sto;
        boolean isGlobal = isStatic || m_symtab.isGlobal();
        if (isGlobal) {
            sto = new VarSTO(id, type);
        } else {
            FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
            int sp = fnBuf.getStackPointer(type.getSize());

            sto = new VarSTO(id, type, sp);
        }

        m_symtab.insert(sto);
        bufferVarDecl(sto);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    void DoIterationVarDecl(STO var,STO s)
    {
      
        m_symtab.insert(var);
        if(var == null){
              

            return;
        }
        if(s == null){
             
            return;
        }
        if(var.isError()){
              
            return;
        }
        if(s.isError()){
           

            return;
        }


      

        if(var.isRef()){
            if(var.getType().isEquivalent(s.getType().getType())){
            return;

            }
            else{
                m_nNumErrors++;
                m_errors.print(Formatter.toString(ErrorMsg.error12r_Foreach,s.getType().getType().getName(),var.getName(),var.getType().getName() ));
                return;
            }

        }


        if(s.getType().isArray()){
            
       
        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
        

        DataSTO varr = (DataSTO) var;
        
        DataSTO array = (DataSTO) s;

        Buffer b = new Buffer();

        b.writeComment("------foreach loop------");

        b.writeMov("%g0","%i0");
        String ttt = generateLabel("IfL1");
        String f = generateLabel("IfL1");

        b.writeLabel(ttt);
        b.write("\n");
        b.writeSet(""+s.getType().getSize(),"%l1");

        b.writeCmp("%i0","%l1");
        b.writeBe(f);
        loopLabels.push(f);
        loopBackLabels.push(ttt);
        b.writeMov("%i0","%l4");
       //b.writeSet(""+s.getType().getType().getSize(),"%o1");

      //  b.writeCall(".mul");

        b.write(array.varAddrInReg("%l2"));

        

        b.writeAdd("%l4","%l2","%l2");

        //load from "%l2" into x
        b.writeLoad("%l2","%l2");
        b.write(varr.storeRegInVar("%l2"));


        b.writeAdd("%i0",s.getType().getType().getSize()+"","%i0");
      

         fnBuf.addBodyBuffer(b.toString());



            if(var.getType().isAssignable(s.getType().getType())){

                return;

            }
            else{
                m_nNumErrors++;
                m_errors.print(Formatter.toString(ErrorMsg.error12v_Foreach,s.getType().getType().getName(),var.getName(),var.getType().getName() ));
                return;
            }





        }
        else{

            m_nNumErrors++;
            m_errors.print(ErrorMsg.error12a_Foreach);
            return;
        }



    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    void DoExternDecl(Type t, Vector<IdInfo> idInfos)
    {
        for (IdInfo idInfo : idInfos) {
            String id = idInfo.getId();

            if (m_symtab.accessLocal(id) != null) {
                handleError(Formatter.toString(ErrorMsg.redeclared_id, id));
            }

            Type varType = idInfo.generateType(t);
            VarSTO sto = new VarSTO(id, varType);
            m_symtab.insert(sto);
        }
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    void DoConstDecl(Type typ, boolean isStatic, Vector<IdInfo> idInfos) {
        for (IdInfo idInfo : idInfos) {
            String id = idInfo.getId();
            STO expr  = idInfo.getExpr();

            if (expr.isError()) { return; }

            if (m_symtab.accessLocal(id) != null) {
                handleError(Formatter.toString(ErrorMsg.redeclared_id, id));
            }

            if (!expr.isConst()) {
                String fmt = Formatter.toString(
                        ErrorMsg.error8_CompileTime,
                        id);
                handleError(fmt);
                        
                return;
            }

            if (!typ.isAssignable(expr.getType())) {
                handleError(Formatter.toString(
                            ErrorMsg.error3b_Assign,
                            typ.getName(),
                            expr.getType().getName()));

                return;
            }

            if (!expr.isConsty()) {
                System.out.println(
                        "\tERROR(MyParser#DoConstDecl()): " +
                        "Not consty");
                return;
            }

            ConstSTO csto = ((Consty) expr).getConstSTO();

            ConstVarSTO var;
            boolean isGlobal = isStatic || m_symtab.isGlobal();
            if (isGlobal) {
                var = new ConstVarSTO(id, typ, csto);
            } else {
                FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();
                int sp = fnBuffer.getStackPointer(typ.getSize());

                var = new ConstVarSTO(id, typ, csto, sp);
            }

            m_symtab.insert(var);
        }
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    void DoTypedefDecl(Vector<IdInfo> idInfos, Type basicType) {
        for (IdInfo idInfo : idInfos) {
            String id = idInfo.getId();
            if (m_symtab.accessLocal(id) != null) {
                handleError(Formatter.toString(ErrorMsg.redeclared_id, id));
            }
            Type aliasType = idInfo.generateType(basicType);
            aliasType.setName(id);
            TypedefSTO sto = new TypedefSTO(id, aliasType);
            m_symtab.insert(sto);
        }
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    void DoStructdefDecl(String id) {
        STO sto = StructSTO.getStructSTO(id, m_symtab.getCurrentScope());

        if (sto.isError()) {
            handleError(Formatter.toString(ErrorMsg.error13b_Struct, id));
            return;
        }

        DoBlockClose();
        m_symtab.insert(sto);
        m_symtab.setCurrentStruct(null);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    FuncSTO DoFuncDecl_1(String id, Type retType, boolean isRetRef) {
        FunctionBuffer fnBuffer; // Function buffer for the function.

        // Prints struct duplicate message since duplicate id's won't be
        // checked.
        if (m_symtab.accessLocal(id) != null) {
            handleError(Formatter.toString(ErrorMsg.error13a_Struct, id));
        }

        if (id.equals("main")) {
            fnBuffer = mainBuffer;
        } else {
            fnBuffer = new FunctionBuffer(id);
        }

        bssBuffer.writeGuard(fnBuffer.getCustomInitLabel());

        FuncSTO sto = new FuncSTO(id, retType, isRetRef, fnBuffer);

        m_symtab.insert(sto);

        m_symtab.openScope();

        setRetTag(false);

        m_symtab.setFunc(sto);

        return sto;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    void DoFormalParams(Vector<ParameterSTO> params)
    {
        FuncSTO func = m_symtab.getFunc();
        if (null == func) {
            m_nNumErrors++;
            m_errors.print ("internal: DoFormalParams says no proc!");
        }

        func.setParameters(params);

        for (ParameterSTO p : params) {
            m_symtab.insert(p);
        }
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    void DoBlockOpen() {
        // Open a scope.

        //this.setIfTag(true);
        m_symtab.openScope();
    }

    void DoStructBlockOpen(String id) {
        // Open a scope.
        m_symtab.setCurrentStruct(id);
        DoBlockOpen();
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    void DoBlockClose() {

        //this.setIfTag(false);
        m_symtab.closeScope();
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    STO DoAssignExpr(STO stoDes, STO expr) {
        if (stoDes.isError()) {
            return stoDes;
        }
        if (expr.isError()){
            return expr;
        }
        if (!stoDes.isModLValue()) {
            m_nNumErrors++;
            m_errors.print(ErrorMsg.error3a_Assign);
            return new ErrorSTO(stoDes.getName());
        }

        if (stoDes.getType().isArray()) {
            if (!stoDes.getType().getType().isAssignable(expr.getType())) {
                m_nNumErrors++;
                m_errors.print(Formatter.toString(
                            ErrorMsg.error3b_Assign,
                            expr.getType().getName(),
                            stoDes.getType().getType().getName()));
                return new ErrorSTO(stoDes.getName());
            }
        } else {
            if (!stoDes.getType().isAssignable(expr.getType())) {
                m_nNumErrors++;
                m_errors.print(Formatter.toString(
                            ErrorMsg.error3b_Assign,
                            expr.getType().getName(),
                            stoDes.getType().getName()));
                return new ErrorSTO(stoDes.getName());
            }
        }


        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
     

        DataSTO varS = (DataSTO) stoDes;
        DataSTO assS = (DataSTO) expr;
        Buffer b = new Buffer();

        if(varS.getType().isFloat()){

            b.write(assS.loadVarInFloatReg("%f0"));
            b.write("\n");


             b.write(varS.storeRegInVar("%f0"));
             b.write("\n");

      
            fnBuf.addBodyBuffer(b.toString());

        }else{

            b.write(assS.loadVarInReg("%l1"));
            b.write("\n");


             b.write(varS.storeRegInVar("%l1"));
             b.write("\n");

      
            fnBuf.addBodyBuffer(b.toString());
        }
        
        
       
        return stoDes;
    }

    /**
     * Operation application is handled here so that an error will
     * be handled properly so that `MyParser` can keep track of the
     * number of errors and print the string.
     */
    STO DoOperation(Operator o) {
        int sp;
        if (m_symtab.isGlobal()) {
            // Global, so store expr in main.
            sp = mainBuffer.getStackPointer(o.getType().getSize());
        } else {
            sp = m_symtab
                .getFunc()
                .getBuffer()
                .getStackPointer(o.getType().getSize());
        }

        STO sto = o.checkOperands(sp);

        if (sto.isConst() && sto.getType().isFloat()) {
            ConstSTO csto = (ConstSTO) sto;
            String name = dataBuffer.addFloat(csto.getStringValue());
            sto.setName(name);
        }

        if (o.isNot() && sto.isError()){
            m_nNumErrors++;
            m_errors.print(
                    Formatter.toString(
                        ErrorMsg.error1u_Expr,
                        o.getOperand().getType().getName(),
                        o.getName(),
                        o.getType().getName()));
            return sto;
        } else if (sto.isError() && !sto.getName().equals("")) {
            m_nNumErrors++;
            m_errors.print(sto.getName());
        }

        if (sto.isExpr()) {
            ExprSTO expr = (ExprSTO) sto;
            expr.setOperator(o);

            return expr;
        }

        return sto;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    STO DoFuncCall(STO sto, Vector<STO> args) {
        if (sto.isError()) {
            return sto;
        }
        if (!sto.isFunc()) {
            m_nNumErrors++;
            m_errors.print(
                    Formatter.toString(
                        ErrorMsg.not_function, sto.getName()));
            return new ErrorSTO(sto.getName());
        }
        FuncSTO funcSTO = (FuncSTO) sto;
        int argsLength = args.size();
        int paramsLength = funcSTO.getNumberOfParameters();

        STO errorExpr = errorInExprList(args);
        if (null != errorExpr) {
            return errorExpr;
        }

        if (argsLength != paramsLength) {
            String eMsg = Formatter.toString(
                    ErrorMsg.error5n_Call, argsLength, paramsLength);
            return handleError(eMsg);
        }

        STO f = areArgumentsOfCorrectType(funcSTO, args);

        if (f.isError()) {
            return sto;
        }

        generateFunctionCall(sto.getName(), args);

        return f;
    }

    private void generateFunctionCall(String name, Vector<STO> args) {
        Buffer buf = new Buffer();
        String outReg;
        FunctionBuffer fnBuf = m_symtab.getFunc().getBuffer();
        for (int i = 0; i < args.size(); i++) {
            STO arg = args.get(i); 
            if (!arg.isData()) {
                continue;
            }
            DataSTO dsto = (DataSTO) arg;
            outReg = "%o" + i;

            buf.write(dsto.loadVarInReg(outReg));
        }

        buf.writeCall(name);

        fnBuf.addBodyBuffer(buf.toString());
    }

    STO DoDesignator2_Arrow(STO sto, String strID) {
        if (sto.isError()) {
            return sto;
        }

        if (!sto.getType().isPointer()) {
            handleError(Formatter.toString(
                        ErrorMsg.error15_ReceiverArrow,
                        sto.getType().getName()));
            return new ErrorSTO("");
        }

        PointerType ptrType = (PointerType) sto.getType();
        if (ptrType.isStruct()) {
            handleError(Formatter.toString(
                        ErrorMsg.error15_ReceiverArrow,
                        ptrType.getName()));
            return new ErrorSTO("");
        }

        return DoDesignator2_Dot(ptrType.deref(sto.getName()), strID);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    STO DoDesignator2_Dot(STO sto, String strID) {


        if(strID == null || sto.isError()){
            return sto;
        }

        boolean isAccessedWithThis = sto.isThis() &&
            null != m_symtab.getFunc();

        if (isAccessedWithThis) {
            STO val = m_symtab.accessParent(strID);
            if (null == val) {
                String errMsg = Formatter.toString(
                        ErrorMsg.error14b_StructExpThis,
                        strID);
                handleError(errMsg);
                return new ErrorSTO(errMsg);
            } else {
                return val;
            }
        }


        if (sto.getType().isArray()) {

            if(!sto.getType().getType().isStruct()){

                String errMsg = Formatter.toString(
                        ErrorMsg.error14t_StructExp,
                        sto.getType().getName());
                handleError(errMsg);
                return new ErrorSTO(errMsg);
            }

            StructType type = (StructType) sto.getType().getType();

            Scope scope = type.getFields();
            STO val = scope.access(strID);

            if (null == val) {
                String errMsg = Formatter.toString(
                        ErrorMsg.error14f_StructExp,
                        strID,
                        sto.getType().getName());
                handleError(errMsg);
                return new ErrorSTO(errMsg);
            }

            return val;

        }

        if (!sto.getType().isStruct()) {
            String errMsg = Formatter.toString(
                    ErrorMsg.error14t_StructExp,
                    sto.getType().getName());
            handleError(errMsg);
            return new ErrorSTO(errMsg);
        }

        StructType type = (StructType) sto.getType();

        Scope scope = type.getFields();
        STO val = scope.access(strID);

        if (null == val) {
            String errMsg = Formatter.toString(
                    ErrorMsg.error14f_StructExp,
                    strID,
                    sto.getType().getName());
            handleError(errMsg);
            return new ErrorSTO(errMsg);
        }

        return val;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public STO arrayDec(STO s){
        if(s.isError()){

            return s;
        }
        else if(s.isConst()){
            if(s.getType().isInt()){
                if (((Consty) s).getConstSTO().getValue() <= 0){
                    m_nNumErrors++;
                    m_errors.print(Formatter.toString(ErrorMsg.error10z_Array, ((Consty) s).getConstSTO().getIntValue()));
                    return new ErrorSTO(Formatter.toString(ErrorMsg.error10z_Array, ((Consty) s).getConstSTO().getIntValue()));
                }
                else{

                    return s;

                }
            }
            else{
                m_nNumErrors++;
                m_errors.print(Formatter.toString(ErrorMsg.error10i_Array,s.getType().getName()));
                return new ErrorSTO(Formatter.toString(ErrorMsg.error10i_Array,s.getType().getName()));
            }
        }
        else{
            m_nNumErrors++;
            m_errors.print(ErrorMsg.error10c_Array);
            return new ErrorSTO(ErrorMsg.error10c_Array);
        }

    }

    STO DoDesignator2_Array(STO array,STO index) {
        // Good place to do the array checks
        //varSTO needs to become of arrayType
        if (array.isError()){
            return array;
        }
        if (index.isError()){
            return index;
        }

        if (array.getType().isArray()){
            if (index.getType().isInt()){

                int leng = array.getType().getLength();

                if (index.isConst()){
                    int ind = ((Consty) index).getConstSTO().getIntValue();

                    if(ind > leng - 1 || ind < 0){

                        //outof bounds
                        m_nNumErrors++;
                        m_errors.print(Formatter.toString(ErrorMsg.error11b_ArrExp,ind ,leng));
                        return new ErrorSTO(Formatter.toString(ErrorMsg.error11b_ArrExp,ind,leng));
                    }
                }
                /*
                System.out.println(
                        "\tWARNING(MyParser#DoDesignator2_Array()): " +
                        "Arrays has not been implemented. " +
                        "Implement stack pointer logic before continuing.");
               
        */  
           

       

            boolean isGlobal =  m_symtab.isGlobal();
            VarArraySTO  sto = new VarArraySTO(
                        array.getName() + "[" + index.getName() + "]",
                        array.getType().getType(),
                        0,array,index);

            if (isGlobal) {
               sto = new VarArraySTO(
                        array.getName() + "[" + index.getName() + "]",
                        array.getType().getType(),
                        0,array,index);
               
            } else {
                
                FunctionBuffer fnBuffer = m_symtab.getFunc().getBuffer();
              //  int sp = fnBuffer.getStackPointer(sto.getType().getSize());

                //System.out.println("\t" + id + " stack addr: " + sp);
                //System.out.println(sp);
               DataSTO a = (DataSTO)array;
               sto = new VarArraySTO(
                        array.getName() + "[" + index.getName() + "]",
                        array.getType().getType(),
                        0,array,index);
                
            }

           
            //here is the prob
            



                return sto;
            } else {
                //not of int type
                m_nNumErrors++;
                m_errors.print(Formatter.toString(ErrorMsg.error11i_ArrExp,index.getType().getName()));
                return new ErrorSTO(Formatter.toString(ErrorMsg.error11i_ArrExp,index.getType().getName()));
            }
        } else {
            //not fo array or pointer type
            m_nNumErrors++;
            m_errors.print(Formatter.toString(ErrorMsg.error11t_ArrExp,array.getType().getName()));
            return new ErrorSTO(Formatter.toString(ErrorMsg.error11t_ArrExp,array.getType().getName()));
        }
    }

 
    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    STO DoDesignator3_ID(String strID)
    {
        STO sto = new STO(strID);

        if ((sto = m_symtab.access(strID)) == null)
        {
            m_nNumErrors++;
            m_errors.print(Formatter.toString(ErrorMsg.undeclared_id, strID));
            sto = new ErrorSTO(strID);
        }

        return sto;
    }

    STO DoDesignatorGlobal3_ID(String strID) {
        STO sto;

        if ((sto = m_symtab.accessGlobal(strID)) == null)
        {
            m_nNumErrors++;
            m_errors.print(Formatter.toString(ErrorMsg.error0g_Scope, strID));
            sto = new ErrorSTO(strID);
        }

        return sto;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    STO DoQualIdent(String strID)
    {
        if (null != m_symtab.getCurrentStruct() &&
                m_symtab.getCurrentStruct().equals(strID)) {
            String errorStr = Formatter.toString(
                    ErrorMsg.error13b_Struct, strID);
            handleError(errorStr);
            return new ErrorSTO(errorStr);
                }
        STO sto = m_symtab.access(strID);

        if (null == sto) {
            m_nNumErrors++;
            m_errors.print(Formatter.toString(ErrorMsg.undeclared_id, strID));
            return new ErrorSTO(strID);
        }

        if (!sto.isTypedef())
        {
            m_nNumErrors++;
            m_errors.print(Formatter.toString(ErrorMsg.not_type, sto.getName()));
            return new ErrorSTO(sto.getName());
        }

        return sto;
    }

    /**
     * Checks if there is an error expression (if so, return same ErrorExpr)
     * otherwise it converts STO to ConstSTO
     */
    STO ExprToConstExpr(STO expr) {
        if (expr.isError()) {
            return expr;
        }
        if (!expr.isExpr()) {
            return new ErrorSTO("Expr must be error.");
        }

        return null;  // TODO: Needs to be implemented.
    }

    private STO areArgumentsOfCorrectType(FuncSTO funcSto, Vector<STO> args) {
        int i = 0;
        Vector<ParameterSTO> params = funcSto.getParameters();
        for (STO a : args) {
            ParameterSTO p = params.get(i);
            STO sto = isErrorWithArgAndParam(a, p);
            if (sto.isError()) {
                return sto;
            }
            i++;
        }

        Type rt = funcSto.getReturnType();
        int sp = m_symtab
            .getFunc()
            .getBuffer()
            .getStackPointer(rt.getSize());

        ExprSTO expr = new ExprSTO("", rt, sp);
        expr.setIsModLValue(funcSto.getIsReturnByReference());

        return expr;
    }

    private STO isErrorWithArgAndParam(STO arg, ParameterSTO param) {
        if (arg.isError())   { return arg; }
        if (param.isError()) { return param; }
        Type argType = arg.getType();
        Type paramType = param.getType();
        if (param.getIsPassByReference()) {
            if (paramType.isEquivalent(argType)) {
                if (!arg.isModLValue()) {
                    String eMsg = Formatter.toString(
                            ErrorMsg.error5c_Call,
                            param.getName(),
                            param.getType().getName());
                    return handleError(eMsg);
                }
                return arg;
            }

            String eMsg = Formatter.toString(
                    ErrorMsg.error5r_Call,
                    argType.getName(),
                    param.getName(),
                    paramType.getName());
            return handleError(eMsg);
        }

        if (paramType.isAssignable(argType)) {
            return arg;
        }

        String eMsg = Formatter.toString(
                ErrorMsg.error5a_Call,
                argType.getName(),
                param.getName(),
                paramType.getName());
        return handleError(eMsg);
    }

    private ErrorSTO errorInExprList(Vector<STO> args) {
        for (STO a : args) {
            if (a.isError()) {
                return (ErrorSTO) a;
            }
        }
        return null;
    }

    private ErrorSTO handleError(String errMsg) {
        m_nNumErrors++;
        m_errors.print(errMsg);
        return new ErrorSTO(errMsg);
    }

}
