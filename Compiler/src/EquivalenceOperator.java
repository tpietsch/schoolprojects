abstract class EquivalenceOperator extends ComparisonOp {

    public EquivalenceOperator(String n) {
        super(n);
    }

    public EquivalenceOperator(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    @Override
    public STO checkOperands(STO o1, STO o2, int sp) {
        boolean bothBoolOrNumeric =
            o1.getType().isBool() && o2.getType().isBool() ||
            o1.getType().isNumeric() && o2.getType().isNumeric();

        if (bothBoolOrNumeric) {
            return new ExprSTO(
                o1.getName() + " " + getName() + " " + o2.getName(),
                new BoolType(),
                sp);
        }

        return new ErrorSTO(getErrorString(o1.getType(), o2.getType()));
    }
}
