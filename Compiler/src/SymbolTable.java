hasNextByte//---------------------------------------------------------------------
//
//---------------------------------------------------------------------
import java.util.*;

class SymbolTable
{
    private Stack<Scope> m_stkScopes;
    private int m_nLevel;
    private Scope m_scopeGlobal;
    private FuncSTO m_func = null;
    private String currentStruct = null;

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public SymbolTable()
    {
        m_nLevel = 0;
        m_stkScopes = new Stack<Scope>();
        m_scopeGlobal = null;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void insert(STO sto)
    {
        Scope scope = m_stkScopes.peek();
        scope.InsertLocal(sto);
    }

    /**
     * Used to determine whether a variable is global or local.
     */
    public boolean isGlobal() {
        return m_stkScopes.peek() == m_scopeGlobal;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public STO accessGlobal(String strName)
    {
        return m_scopeGlobal.access(strName);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public STO accessLocal(String strName)
    {
        Scope scope = m_stkScopes.peek();
        return scope.accessLocal(strName);
    }

    public STO accessParent(String strName) {
        Scope local = m_stkScopes.pop();
        Scope parent = m_stkScopes.peek();
        m_stkScopes.push(local);

        return parent.accessLocal(strName);
    }

    public boolean hasLocal(String strName) {
        return null != accessLocal(strName);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public STO access(String strName)
    {
        Stack<Scope> stk = new Stack<Scope>();
        Scope scope;
        STO stoReturn = null;


        while(!m_stkScopes.empty())
        {
            scope = m_stkScopes.pop();
            stk.push(scope);
            if ((stoReturn = scope.access(strName)) != null){
                while(!stk.empty()){

                    scope = stk.pop();
                    m_stkScopes.push(scope);
                }
                return stoReturn;
            }
        }

        while(!stk.empty()){

            scope = stk.pop();
            m_stkScopes.push(scope);
        }
        return stoReturn;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void openScope()
    {
        Scope scope = new Scope();

        // The first scope created will be the global scope.
        if (m_scopeGlobal == null)
            m_scopeGlobal = scope;

        m_stkScopes.push(scope);
        m_nLevel++;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void closeScope()
    {
        m_stkScopes.pop();
        m_nLevel--;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public int getLevel()
    {
        return m_nLevel;
    }


    //----------------------------------------------------------------
    //	This is the function currently being parsed.
    //----------------------------------------------------------------
    public FuncSTO getFunc() { return m_func; }
    public void setFunc(FuncSTO sto) { m_func = sto; }
    public Scope getCurrentScope() {
        return m_stkScopes.peek();
    }

    public void setCurrentStruct(String id) {
        currentStruct = id;
    }

    public String getCurrentStruct() {
        return currentStruct;
    }
}
