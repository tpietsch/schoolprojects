abstract class DecIncOp extends UnaryArithmeticOp {

    private boolean post = false;

    public DecIncOp(String n) {
        super(n);
        setPost(post);
    }

    public DecIncOp(String n, STO o) {
        super(n, o);
        setPost(post);
    }

    public boolean getPost() {
        return post;
    }

    public void setPost(boolean p) {
        post = p;
    }

    /*
     * Stores result in `reg`.
     */
    @Override
    protected String loadLoadComputeStore(DataSTO op, DataSTO dest,
            boolean isFloat) {
        Buffer buf = new Buffer();

        if (isFloat) {
            buf.write(floatCompute(op, dest));
        } else {
            buf.write(compute(op, dest));
        }

        return buf.toString();
    }

    public String floatCompute(DataSTO operand, DataSTO dest) {
        String cmd = getFloatName();
        Buffer buf = new Buffer();

        String operandFloat = "%f1";
        String tmpFloat = "%f2";
        String tmpAddr = "%fp-4";

        // Place 1 in %f2
        buf.write(operand.loadVarInFloatReg(operandFloat));
        buf.writeSet("1", "%l0");
        buf.writeStore("%l0", tmpAddr);
        buf.writeLoad(tmpAddr, tmpFloat);
        buf.writeFitos(tmpFloat, tmpFloat);

        if (getPost()) {
            buf.writeTabbedComment("post op.");
            buf.write(dest.storeRegInVar(operandFloat));
            buf.writeThreeOpCommand(cmd, "%f1", tmpFloat, tmpFloat);
            buf.write(operand.storeRegInVar(tmpFloat));
        } else {
            buf.writeTabbedComment("pre op.");
            buf.writeThreeOpCommand(cmd, operandFloat, tmpFloat, tmpFloat);
            buf.write(dest.storeRegInVar(tmpFloat));
            buf.write(operand.storeRegInVar(tmpFloat));
        }


        return buf.toString();
    }

    private String compute(DataSTO operand, DataSTO dest) {
        String cmd = getAsmOp();
        Buffer buf = new Buffer();
        String tmp = "%l1";

        buf.write(operand.loadVarInReg(tmp));
        if (getPost()) {
            buf.writeTabbedComment("post op.");
            buf.write(dest.storeRegInVar(tmp));
            buf.writeOneOpCommand(cmd, tmp);
            buf.write(operand.storeRegInVar(tmp));
        } else {
            buf.writeTabbedComment("pre op.");
            buf.writeOneOpCommand(cmd, tmp);
            buf.write(dest.storeRegInVar(tmp));
            buf.write(operand.storeRegInVar(tmp));
        }

        return buf.toString();
    }
}
