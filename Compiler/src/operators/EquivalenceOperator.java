abstract class EquivalenceOperator extends ComparisonOp {
    public EquivalenceOperator(String n) {
        super(n);
    }

    public EquivalenceOperator(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    @Override public STO checkOperands(STO o1, STO o2, int sp) {
        Type type1 = o1.getType();
        Type type2 = o2.getType();
        boolean bothBoolOrNumeric =
            type1.isBool() && type2.isBool() ||
            type1.isNumeric() && type2.isNumeric();
        if (bothBoolOrNumeric) {
            return new ExprSTO(
                    o1.getName() + " " + getName() + " " + o2.getName(),
                    new BoolType(),
                    sp);
        }
        if (type1.isPointer() && type2.isPointer()) {
            boolean equivalentPointers =
                type1.isEquivalent(type2) ||
                type1.isNullPtr() || type2.isNullPtr();
            if (equivalentPointers) {
                return new ExprSTO(
                        o1.getName() + " " + getName() + " " + o2.getName(),
                        new BoolType(),
                        sp);
            }
            boolean bothNullPointers =
                type1.isNullPtr() && type2.isNullPtr();
            if (bothNullPointers) {
                return new ConstSTO(
                        o1.getName() + " " + getName() + " " + o2.getName(),
                        new BoolType(),
                        "true");
            }
            return new ErrorSTO(Formatter.toString(
                        ErrorMsg.error17_Expr,
                        getName(),
                        type1.getName(),
                        type2.getName()));
        }
        return new ErrorSTO(getErrorString(type1, type2));
    }

    @Override
    public String getAsmOp() {
        System.out.println("\tERROR: NOT YET IMPLEMENTED");
        String op = "NOT YET IMPLMENTED";

        return op;
    }
}
