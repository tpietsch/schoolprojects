class DecOp extends DecIncOp {

    public DecOp(String n) {
        super(n);
    }

    public DecOp(String n, STO o) {
        super(n, o);
    }

    @Override
    protected ConstSTO performOperation(ConstSTO o) {
        String val;
        if (getType().isInt()) {
            val = "" + (o.getValue().intValue() + 1);
        } else {
            val = "" + (o.getValue().floatValue() + 1);
        }
        return new ConstSTO(val, getType());
    }

    @Override
    public String getAsmOp() {
        String op = "dec";

        return op;
    }

    @Override
    public String getFloatName() {
        String name = "fsubs";

        return name;
    }
}
