class MulOp extends ArithmeticOp{

    public MulOp(String n) {
        super(n);
    }

    public MulOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    @Override
    public String getFloatName() {
        String name = "fmuls";

        return name;
    }

    @Override
    protected ConstSTO performOperation(ConstSTO o1, ConstSTO o2) {
        String val;
        if (o1.getType().isInt() && o2.getType().isInt()) {
            val = "" + (o1.getValue().intValue() * o2.getValue().intValue());
        } else {
            val = "" +
                (o1.getValue().floatValue() * o2.getValue().floatValue());
        }

        return new ConstSTO(val, getType());
    }

    @Override
    public String getAsmOp() {
        String op = ".mul";

        return op;
    }

    @Override
    public boolean isFunction() {
        return true;
    }
}
