class OrOp extends BooleanOp {

    public OrOp(String n) {
        super(n);
    }

    public OrOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    @Override
    protected ConstSTO performOperation(ConstSTO o1, ConstSTO o2) {
        boolean val;
        if (o1.getValue().intValue() != 0 || o2.getValue().intValue() != 0) {
            val = true;
        } else {
            val = false;
        }

        return new ConstSTO("" + val, getType());
    }

    @Override
    public String getAsmOp() {
        String op = "or";

        return op;
    }
}
