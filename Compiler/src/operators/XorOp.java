class XorOp extends BitwiseOp {

    public XorOp(String n) {
        super(n);
    }

    public XorOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    @Override
    protected ConstSTO performOperation(ConstSTO o1, ConstSTO o2) {
        int val = o1.getValue().intValue() ^ o2.getValue().intValue();

        return new ConstSTO("" + val, getType());
    }

    @Override
    public String getAsmOp() {
        String op = "xor";

        return op;
    }
}
