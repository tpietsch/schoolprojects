class ModOp extends ArithmeticOp{

    public ModOp(String n) {
        super(n);
    }

    public ModOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    private String getErrorString(Type type) {
        return Formatter.toString(
                ErrorMsg.error1w_Expr,
                type.getName(),
                getName(),
                "int");
    }

    @Override public STO checkOperands(STO o1, STO o2, int sp) {
        if (!(o1.getType().isInt())) {
            return new ErrorSTO(getErrorString(o1.getType()));
        }
        if (!(o2.getType().isInt())) {
            return new ErrorSTO(getErrorString(o2.getType()));
        }

        return new ExprSTO("", new IntType(), sp);
    }

    @Override
    protected ConstSTO performOperation(ConstSTO o1, ConstSTO o2) {
        String val = "" +
            (o1.getValue().intValue() % o2.getValue().intValue());

        return new ConstSTO(val, getType());
    }

    @Override
    public String getAsmOp() {
        String op = ".rem";

        return op;
    }

    @Override
    public boolean isFunction() {
        return true;
    }
}
