abstract class ComparisonOp extends BinaryOperator {

    abstract public String getBranchInstr();
    abstract public String getEqualityLabel();

    protected static int equalityCounter = 0;

    Type type = new BoolType();

    public ComparisonOp(String n) {
        super(n);
    }

    public ComparisonOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    @Override
    protected Type getType() {
        return type;
    }

    protected String getErrorString(Type type1, Type type2) {
        return Formatter.toString(
                ErrorMsg.error1b_Expr,
                type1.getName(),
                getName(),
                type2.getName());
    }

    @Override public STO checkOperands(STO o1, STO o2, int sp) {
        boolean isNumeric =
            o1.getType().isNumeric() && o2.getType().isNumeric();

        if (isNumeric) {
            return new ExprSTO(
                    o1.getName() + " " + getName() + " " + o2.getName(),
                    getType(),
                    sp);
        }

        return new ErrorSTO(getErrorString(o1.getType(), o2.getType()));
    }

    private String genLabel() {
        return getEqualityLabel() + equalityCounter++;
    }

    @Override
    protected String compute(String reg) {
        DataSTO op1 = (DataSTO) getOperand1();
        DataSTO op2 = (DataSTO) getOperand2();
        String tmpReg0 = "%l3";
        String tmpReg1 = "%l4";
        String branchInstr = getBranchInstr();

        String cmt = "---equality test---";
        Buffer buf = new Buffer();

        buf.writeTabbedComment(cmt);
        if (op1.getType().isFloat() || op2.getType().isFloat()) {
            buf.write(op1.loadVarInFloatReg("%f0"));
            buf.writeStore("%f0", "%fp-4");
            buf.writeLoad("%fp-4", tmpReg0);

            buf.write(op2.loadVarInFloatReg("%f0"));
            buf.writeStore("%f0", "%fp-4");
            buf.writeLoad("%fp-4", tmpReg1);
        } else {
            buf.write(op1.loadVarInReg(tmpReg0));
            buf.write(op2.loadVarInReg(tmpReg1));
        }
        
        String branchLabel = genLabel();
        String branchExit = branchLabel + "_exit";

        // cmp %l3, %l4
        buf.writeCmp("%l3", "%l4");
        buf.writeOneOpCommand(branchInstr, branchLabel);
        buf.writeNop();

        buf.writeBlankLine();
        buf.writeTabbedComment("-" + op1.getName() + " " + getName() +
                " " + op2.getName());

        buf.writeSet("0", reg);
        buf.writeBlankLine();

        buf.writeB(branchExit);
        buf.writeBlankLine();

        buf.writeLabel(branchLabel);
        buf.writeBlankLine();
        buf.writeSet("1", reg);
        buf.writeBlankLine();

        buf.writeLabel(branchExit);
        buf.writeBlankLine();
        buf.writeBlankLine();

        return buf.toString();
    }
}
