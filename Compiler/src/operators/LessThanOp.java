class LessThanOp extends ComparisonOp {

    public LessThanOp(String n) {
        super(n);
    }

    public LessThanOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    @Override
    protected ConstSTO performOperation(ConstSTO o1, ConstSTO o2) {
        String val;
        if (o1.getType().isFloat() || o2.getType().isFloat()) {
            val = "" +
                (o1.getValue().floatValue() < o2.getValue().floatValue());
        } else {
            val = "" + (o1.getValue().intValue() < o2.getValue().intValue());
        }

        return new ConstSTO(val, getType());
    }

    @Override
    public String getAsmOp() {
        String op = "";

        return op;
    }

    @Override
    public String getEqualityLabel() {
        return "__less_than_";
    }

    @Override
    public String getBranchInstr() {
        return "bl";
    }
}
