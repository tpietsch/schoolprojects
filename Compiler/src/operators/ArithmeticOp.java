import java.lang.IllegalStateException;

abstract class ArithmeticOp extends BinaryOperator {

    public ArithmeticOp(String n) {
        super(n);
    }

    public ArithmeticOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    private String getErrorString(Type type) {
        return Formatter.toString(
                ErrorMsg.error1n_Expr,
                type.getName(),
                getName());
    }

    protected Type getType() {
        return getType(getOperand1(), getOperand2());
    }

    /**
     * Assumes there is no error.
     */
    protected Type getType(STO o1, STO o2) {
        if (o1.getType().isInt() && o2.getType().isInt()) {
            return new IntType();
        }

        return new FloatType();
    }

    @Override
    public STO checkOperands(STO o1, STO o2, int stackPointer) {
        if (!(o1.getType().isNumeric())) {
            return new ErrorSTO(getErrorString(o1.getType()));
        }

        if (!(o2.getType().isNumeric())) {
            return new ErrorSTO(getErrorString(o2.getType()));
        }

        String name =
            "(" + o1.getName() + " " + getName() + " " + o2.getName() + ")";

        if ( (o1.getType().isInt()) && (o2.getType().isInt())) {
            return new ExprSTO(name, new IntType(), stackPointer);
        }

        return new ExprSTO(name, new FloatType(), stackPointer);
    }
}
