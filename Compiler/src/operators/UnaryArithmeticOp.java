abstract class UnaryArithmeticOp extends UnaryOperator {

    Type type = new IntType();
    public UnaryArithmeticOp(String n) {
        super(n);
    }

    public UnaryArithmeticOp(String n, STO o) {
        super(n, o);
    }

    private String getErrorString(int type,Type t) {
        if (type == 1) {
            return Formatter.toString(
                    ErrorMsg.error2_Type,
                    t.getName(),
                    getName());
        }
        return Formatter.toString(ErrorMsg.error2_Lval,this.getName());
    }

    @Override
    public STO checkOperands(STO o, int sp) {
        if (o.isError()) {
            return new ErrorSTO("");
        }

        if (!o.getType().isNumeric()) {
            return new ErrorSTO(getErrorString(1, getType()));
        }

        if(!o.getIsModifiable()) {
            return new ErrorSTO(getErrorString(2, getType()));
        }

        return new ExprSTO(o.getName(), getType(), sp);
    }
}
