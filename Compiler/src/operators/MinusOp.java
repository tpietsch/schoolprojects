class MinusOp extends ArithmeticOp{

    public MinusOp(String n) {
        super(n);
    }

    public MinusOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    @Override public String getFloatName() {
        return "fsubs";
    }

    @Override
    protected ConstSTO performOperation(ConstSTO o1, ConstSTO o2) {
        String val;
        if (o1.getType().isInt() && o2.getType().isInt()) {
            val = "" + (o1.getValue().intValue() - o2.getValue().intValue());
        } else {
            val = "" +
                (o1.getValue().floatValue() - o2.getValue().floatValue());
        }

        return new ConstSTO(val, getType());
    }

    @Override
    public String getAsmOp() {
        String op = "sub";

        return op;
    }
}
