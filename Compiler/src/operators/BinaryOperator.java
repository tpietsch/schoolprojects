import java.lang.IllegalStateException;

abstract class BinaryOperator extends Operator {

    private STO operand1 = null,
            operand2 = null;

    abstract protected STO checkOperands(STO o1, STO o2, int stackPointer);
    abstract protected ConstSTO performOperation(ConstSTO o1, ConstSTO o2);

    public BinaryOperator(String n) {
        super(n);
    }

    public BinaryOperator(String n, STO o1, STO o2) {
        super(n);
    }

    public void setOperands(STO o1, STO o2) {
        operand1 = o1;
        operand2 = o2;
        setIsConst(o1.isConst() && o2.isConst());
    }

    public STO getOperand1() {
        return operand1;
    }

    public STO getOperand2() {
        return operand2;
    }

    @Override
    public STO checkOperands(int stackPointer) {
        if (operand1 == null || operand2 == null) {
            throw new IllegalStateException("operands must be set");
        }

        if (operand1.isError() || operand2.isError()){
            return new ErrorSTO("");
        }

        STO sto = checkOperands(operand1, operand2, stackPointer);

        if (sto.isError()) {
            return sto;
        }

        if (getIsConst()) {
            if (this instanceof DivOp &&
                    ((Consty) operand2).getConstSTO().getValue() == 0) {

                return new ErrorSTO(ErrorMsg.error8_Arithmetic);
            }

            return performOperation();
        }

        return sto;
    }

    @Override
    public STO performOperation() {
        if (operand1 == null || operand2 == null) {
            throw new IllegalStateException("operands must be set");
        }
        if (operand1.isError() || operand2.isError()) {
            return new ErrorSTO("");
        }

        if (!operand1.isConsty() || !operand2.isConsty()) {
            return new ErrorSTO("");
        }

        ConstSTO csto1 = ((Consty) operand1).getConstSTO();
        ConstSTO csto2 = ((Consty) operand2).getConstSTO();

        return performOperation(csto1, csto2);
    }

    /*
     * Stores finalized operation in `reg`.
     */
    @Override
    public String generateOperationAsm(DataSTO dest, boolean isFloat) {
        if (!(getOperand1().isData() && getOperand2().isData())) {
            System.out.println(
                    "\tERROR: (" +
                    getClass().getName() + "#generateOperationAsm()) " +
                    "An operand does not extend Data");
            return "";
        }

        DataSTO op1 = (DataSTO) getOperand1();
        DataSTO op2 = (DataSTO) getOperand2();

        return loadLoadComputeStore(op1, op2, dest, isFloat);
    }

    private String loadLoadComputeStore(DataSTO op1, DataSTO op2,
            DataSTO dest, boolean isFloat) {
        Buffer buf = new Buffer();
        String comment =
            "--Processing op " + getName() + " into " + dest.getName() + "--";
        String tmp;

        buf.writeTabbedComment(comment);

        if (isFloat) {
            tmp = "%f3";
            buf.write(op1.loadVarInFloatReg("%f1"));
            buf.write(op2.loadVarInFloatReg("%f2"));
            buf.write(floatCompute(tmp));
        } else {
            tmp = "%l6";
            buf.write(op1.loadVarInReg("%l1"));
            buf.write(op2.loadVarInReg("%l2"));
            buf.write(compute(tmp));
        }

        buf.write(dest.storeRegInVar(tmp));

        return buf.toString();
    }
    
    /**
     * Generates assembly for computing a binary operation.
     *
     * NOTE: Assumes that operands are in `%l1` and `%l2`.
     * Result is stored in `%l1`.
     */
    protected String compute(String reg) {
        String cmd = getAsmOp();
        String cmt = "---Crazy equality test---";
        Buffer buf = new Buffer();

        if (isFunction()) {
            buf.write(performFunctionOp(reg));
        } else {
            buf.writeThreeOpCommand(cmd, "%l1", "%l2", reg);
        }

        return buf.toString();
    }

    /**
     * Generates assembly for computing a binary floating point operation.
     *
     * NOTE: Assumes that operands are in `%f1` and `%f2`.
     */
    public String floatCompute(String dest) {
        String cmd = getFloatName();
        Buffer buf = new Buffer();

        buf.writeThreeOpCommand(cmd, "%f1", "%f2", dest);

        return buf.toString();
    }

    private String performFunctionOp(String dest) {
        String cmd = getAsmOp();
        Buffer buf = new Buffer();

        buf.writeMov("%l1", "%o0");
        buf.writeMov("%l2", "%o1");
        buf.writeCall(cmd);
        buf.writeMov("%o0", dest);

        return buf.toString();
    }
}
