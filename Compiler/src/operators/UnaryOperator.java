import java.lang.IllegalStateException;

abstract class UnaryOperator extends Operator {

    private Type type;
    private STO operand;

    private boolean isConst = false;

    abstract public STO checkOperands(STO o, int sp);
    abstract protected ConstSTO performOperation(ConstSTO o);

    public UnaryOperator(String n) {
        super(n);
    }

    public UnaryOperator(String n, STO o) {
        super(n);
        setOperand(o);
    }

    public void setOperand(STO o) {
        operand = o;
        type = o.getType();
        setIsConst(o.isConst());
    }

    public STO getOperand() {
        return operand;
    }

    @Override
    public boolean isNot() {
        return false;
    }

    @Override
    protected Type getType() {
        return type;
    }

    @Override
    public STO checkOperands(int stackPointer) {
        if (operand == null) {
            throw new IllegalStateException("operand must be set");
        }
        STO sto = checkOperands(operand, stackPointer);


        if (sto.isError()) {
            return sto;
        }

        if (getIsConst()) {
            return performOperation();
        }

        return sto;
    }

    @Override
    public STO performOperation() {
        if (operand == null) {
            throw new IllegalStateException("operand must be set");
        }
        if (operand.isError()) {
            return new ErrorSTO(operand.getName());
        }

        if (!operand.isConsty()) {
            return new ErrorSTO("");
        }

        ConstSTO csto = ((Consty) operand).getConstSTO();

        return performOperation(csto);
    }

    /*
     * Stores finalized operation in `reg`.
     */
    @Override
    public String generateOperationAsm(DataSTO dest, boolean isFloat) {
        if (!getOperand().isData()) {
            System.out.println(
                    "\tERROR: (" +
                    getClass().getName() + "#generateOperationAsm()) " +
                    "Operand does not extend Data");
            return "";
        }

        DataSTO op = (DataSTO) getOperand();

        return loadLoadComputeStore(op, dest, isFloat);
    }

    /*
     * Stores result in `reg`.
     */
    protected String loadLoadComputeStore(DataSTO op, DataSTO dest,
            boolean isFloat) {
        Buffer buf = new Buffer();
        String tmp = "%l6";

        if (isFloat) {
            buf.write(op.loadVarInReg("%f1"));
            buf.write(floatCompute(tmp));
        } else {
            buf.write(op.loadVarInReg("%l1"));
            buf.write(compute(tmp));
        }

        buf.write(dest.storeRegInVar(tmp));

        return buf.toString();
    }

    /**
     * Generates assembly for computing a unary operation.
     *
     * NOTE: Assumes that operands are in `%l1`
     * Result is stored in `%l1`.
     */
    protected String compute(String reg) {
        String cmd = getAsmOp();
        Buffer buf = new Buffer();

        if (isFunction()) {
            buf.write(performFunctionOp(reg));
        } else {
            buf.writeOneOpCommand(cmd, "%l1");
            buf.writeMov("%l1", reg);
        }

        return buf.toString();
    }

    /**
     * Generates assembly for computing a unary floating point operation.
     *
     * NOTE: Assumes that operands are in `%f1`.
     */
    public String floatCompute(String dest) {
        String cmd = getFloatName();
        Buffer buf = new Buffer();

        buf.writeThreeOpCommand(cmd, "%f1", "%f2", dest);

        return buf.toString();
    }

    private String performFunctionOp(String reg) {
        String cmd = getAsmOp();
        Buffer buf = new Buffer();

        buf.writeMov("%l1", "%o0");
        buf.writeCall(cmd);
        buf.writeMov("%o0", reg);

        return buf.toString();
    }
}
