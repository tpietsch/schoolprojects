/**
 * This is the root of the Operator hierarchy.
 */

abstract class Operator {

    private String name;
    private boolean isConst = false;

    public Operator(String n) {
        name = n;
    }

    public String getName() {
        return name;
    }

    public boolean getIsConst() {
        return isConst;
    }

    public boolean setIsConst(boolean b) {
        return isConst = b;
    }

    public STO getOperand() {
        return null;
    }

    public boolean isNot() {
        return false;
    }

    /**
     * Some operations (eg Division) require a function call instead
     * of just executing a native instruciton.
     */
    public boolean isFunction() {
        return false;
    }

    public String getFloatName() {
        System.out.println(
                "\t\tWARNING: Operator " + getName() +
                " has no float equivalent");

        return "";
    }

    abstract public String getAsmOp();
    abstract public String generateOperationAsm(DataSTO reg, boolean isFloat);

    abstract public STO checkOperands(int sp);
    abstract public STO performOperation();
    abstract protected Type getType();
}
