abstract class BitwiseOp extends BinaryOperator {

    public BitwiseOp(String n) {
        super(n);
    }

    public BitwiseOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    private String getErrorString(Type type) throws IllegalStateException {
        return Formatter.toString(
                ErrorMsg.error1w_Expr,
                type.getName(),
                getName(),
                "int");
    }

    protected Type getType() {
        return new IntType();
    }

    @Override
    public STO checkOperands(STO o1, STO o2, int sp) {
        //not sure about this
        if (o1.getType().isInt() && o2.getType().isInt()){
            return new ExprSTO("", getType(), sp);
        }

        if (!o1.getType().isInt()) {
            return new ErrorSTO(getErrorString(o1.getType()));
        }

        return new ErrorSTO(getErrorString(o2.getType()));
    }
}
