abstract class BooleanOp extends BinaryOperator {

    public BooleanOp(String n) {
        super(n);
    }

    public BooleanOp(String n, STO o1, STO o2) {
        super(n, o1, o2);
    }

    private String getErrorString(Type type) throws IllegalStateException {
        return Formatter.toString(
                ErrorMsg.error1w_Expr,
                type.getName(),
                getName(),
                "bool");
    }

    @Override
    protected Type getType() {
        return new BoolType();
    }

    @Override public STO checkOperands(STO o1, STO o2, int sp) {
        if(o1.getType().isBool() && o2.getType().isBool()){
            return new ExprSTO("", getType(), sp);
        }
        else if(!o1.getType().isBool())
            return new ErrorSTO(getErrorString(o1.getType()));
        else
            return new ErrorSTO(getErrorString(o2.getType()));
    }
}
