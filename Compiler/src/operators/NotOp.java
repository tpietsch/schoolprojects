class NotOp extends UnaryOperator {

    private Type type = new BoolType();

    public NotOp(String n) {
        super(n);
    }

    public NotOp(String n, STO o) {
        super(n);
        setOperand(o);
    }

    @Override
    protected Type getType() {
        return type;
    }

    @Override public STO checkOperands(STO o, int sp) {
        if (!o.getType().isBool()) {
            return new ErrorSTO(o.getName());
        }

        return new ExprSTO(o.getName(), getType(), sp);
    }

    @Override
    protected ConstSTO performOperation(ConstSTO o) {
        String val;
        int opVal = o.getValue().intValue();

        if (opVal == 0) {
            val = "" + true;
        } else {
            val = "" + false;
        }

        return new ConstSTO(val, getType());
    }

    public boolean isNot(){
        return true;
    }

    @Override
    public String getAsmOp() {
        System.out.println("\tERROR: NOT YET IMPLEMENTED");
        String op = "NOT YET IMPLMENTED";

        return op;
    }
}
