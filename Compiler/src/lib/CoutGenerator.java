import java.util.*;

class CoutGenerator extends SourceGenerator{

  	String assembly = "";

  	static String varTmp = "_cout";

  	Vector<STO> stoList = new Vector<STO>();

    Buffer code = new Buffer();
    DataBuffer data;

    @Override
    public String toString() {
        return code.toString();
    }

    public String totoString(){
        return stoList.toString();
    }

    public CoutGenerator(DataBuffer d) {
    	super();
        data = d;
    }

    //adding sto to list to be generated to print assembly
    public CoutGenerator add(STO s){

    	if (s == null) return this;

    	if (s.isError()) return this;

    	stoList.add(s);

    	return this;
    }

    //converts sto list to string the prints out the string
    public void print() {
    	for (STO item : stoList) {

    		if (item == null) {
                continue;
    		}

   			if (item.isError()) {
                continue;
   			}

            if (item.isEndl()) {
                PrintAssemblyEndl();

                continue;
            }

   			if (item.isConst()) {
   				PrintAssemblyConstant(((Consty) item).getConstSTO());

                continue;
   			}

            if (item.isExpr()) {

                PrintAssemblyExpr((ExprSTO) item);

                continue;
            }

   			if (item.isVar()) {
   				PrintAssemblyVariable((VarSTO) item);

                continue;
   			}

   			if (item.isString()) {

   				PrintAssemblyStringLit((StringSTO) item);

                continue;
   			}

   		    // other cases to be complete Expressions,Arrays,Structs...
            System.out.println("Case in CoutGenerator#Print() not covered");
            continue;
		}
    }

    public Vector<STO> list() {

        return stoList;

    }

    ///////////////////////////////////
    // Privates
    ///////////////////////////////////


    private void PrintAssemblyVariable(VarSTO s) {

    	Type t = s.getType();

    	if ( t == null ) {
    	    return;
    	}

    	//print var versions
    	if ( t.isInt() ) {

            code.writeln(s.loadVarInReg("%o1"));
            code.writeSet("__int_fmt", "%o0");
            code.writeCall("printf");
    	}

    	if ( t.isFloat() ) {
            code.writeln(s.loadVarInReg("%f0"));
    		code.writeCall("printFloat");
    	}

    	if ( t.isBool() ) {

            code.write(s.loadVarInReg("%l0"));
    		code.writeCmp("%l0", "%g0");

    		varTmp = varTmp + "T";
    		//label to branch to if false
    		code.writeBe(varTmp);
    		PrintAssemblyBool(true);
    		code.writeB(varTmp + "T");

    		code.writeLabel(varTmp);
            varTmp = varTmp + "T";
    		PrintAssemblyBool(false);
    		code.writeLabel(varTmp);

            return;
    	}

    	if ( t.isStruct() ) {
    		//struct printing handle how???

            return;
    	}

    	if ( t.isArray() ) {
    		//array printing handle how?????

            return;
    	}
    }

    private void PrintAssemblyConstant(ConstSTO s) {
    	Type t = s.getType();

    	if ( t == null ) {
    		return;
    	}

    	if ( t.isInt() ) {
    		PrintAssemblyInt(s.getIntValue());

            return;
    	}

    	if ( t.isFloat() ) {
    		PrintAssemblyFloat(s.getFloatValue());

            return;
    	}

    	if ( t.isBool() ) {
            if(s.getBoolValue() != 0){
                PrintAssemblyBool(true);
            }
            else{
                PrintAssemblyBool(false);
            }


            return;
    	}

        //struct printing handle how???
    	if ( t.isStruct() ) {
    		return;
    	}

        //array printing handle how?????
    	if ( t.isArray() ) {
    		return;
    	}

        // other cases to be complete Expressions,Arrays,Structs...
        System.out.println(
                "Case in CoutGenerator#PrintAssemblyConstant() not covered");
        return;
    }

    private void PrintAssemblyFloat(float f) {
        varTmp = varTmp + "T";
        data.writeVarDeclWithInit(varTmp + ": .single 0r" + f);

    	code.writeSet(varTmp,"%l0");
    	code.writeLoad("%l0","%f0");
    	code.writeCall("printFloat");
    }

    private void PrintAssemblyInt(int i) {
        code.writeSet("__int_fmt","%o0");
        code.writeSet(i+"","%o1");
 		code.writeCall("printf");
    }

    private void PrintAssemblyBool(boolean b) {
    	if (b) {
            code.writeSet("__bool_t","%o0");
            code.writeCall("printf");
    	} else {
            code.writeSet("__bool_f","%o0");
            code.writeCall("printf");
    	}
    }

    private void PrintAssemblyAddress() {

    }

    private void PrintAssemblyExpr(ExprSTO s) {

        Type t = s.getType();

        if ( t == null ) {
            return;
        }

        //print var versions
        if ( t.isInt() ) {

            code.writeln(s.loadVarInReg("%o1"));
            code.writeSet("__int_fmt", "%o0");
            code.writeCall("printf");
        }

        if ( t.isFloat() ) {
            code.writeln(s.loadVarInReg("%f0"));
            code.writeCall("printFloat");
        }

        if ( t.isBool() ) {

            code.writeln(s.loadVarInReg("%l0"));
            code.writeCmp("%l0","%g0");

            varTmp = varTmp + "T";
            //label to branch to if false
            code.writeBe(varTmp); // Branch if l0 == 0, false.
            PrintAssemblyBool(true);
            code.writeB(varTmp + "T");

            code.writeLabel(varTmp);
            varTmp = varTmp + "T";
            PrintAssemblyBool(false);
            code.writeLabel(varTmp);

            return;
        }

        if ( t.isStruct() ) {
            //struct printing handle how???

            return;
        }

        if ( t.isArray() ) {
            //array printing handle how?????

            return;
        }
    }

    private void PrintAssemblyEndl() {
        code.writeSet("__endl","%o0");
        code.writeCall("printf");
    }

    private void PrintAssemblyStringLit(StringSTO s) {
    	String str = s.getString();

        varTmp = varTmp + "T";

        data.writeLabel(varTmp);
        data.writeln(" .asciz \"" + str + "\"");
        data.writeAlignment("4");
        code.writeSet("__string_fmt","%o0");
        code.writeSet(varTmp,"%o1");

        code.writeCall("printf");

    }
}
