class IdExprPair {
    private String id;
    private STO    expr;
    private STO		arrLength;
    private boolean arr = false;

    public IdExprPair(String i, STO e) {
        id   = i;
        expr = e;
    }
     public IdExprPair(String i, STO e,STO c) {
        id   = i;
        expr = e;
        arrLength = c;
        arr = true;
    }
    
    public String getId()   { return id; }
    public STO    getExpr() { return expr; }
    public boolean    isArr() { return arr; }
    public STO    getConst() { return arrLength; }

}
