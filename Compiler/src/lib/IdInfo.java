class IdInfo {

    private String id;
    private Integer pointers;
    private STO arrSize;
    private STO expr;

    public IdInfo(String i) {
        this(i, new Integer(0), null);
    }

    public IdInfo(String i, Integer ps, STO s) {
        this(i, ps, s, null);
    }

    public IdInfo(String i, Integer ps, STO s, STO expr) {
        id = i;
        pointers = ps;
        arrSize = s;
        setExpr(expr);
    }

    public Type generateType(Type t) {
        if (null != arrSize) {
            if (arrSize.isError()) {
                return new VoidType();
            }
    
            if (arrSize.isConst()) {
                t = new ArrayType(
                        t,
                        ((Consty) arrSize).getConstSTO().getIntValue());
            }
        }

        if (null != pointers) {
            for (int i = 0; i < pointers.intValue(); i++) {
                t = new PointerType(t);
            }
        }

        return t;
    }

    public String getId() {
        return id;
    }

    public Integer getPointers() {
        return pointers;
    }

    public STO getArrSize() {
        return arrSize;
    }

    public STO getExpr() {
        return expr;
    }

    public STO setExpr(STO e) {
        return expr = e;
    }

    public boolean hasExpr() {
        return null != getExpr();
    }
}
