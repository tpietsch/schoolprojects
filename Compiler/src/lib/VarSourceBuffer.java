import java.util.LinkedList;

class VarSourceBuffer {

    LinkedList<VarSTO> buffer;
    LinkedList<String> names;

    public VarSourceBuffer() {
        buffer = new LinkedList<VarSTO>();
        names = new LinkedList<String>();
    }

    public LinkedList<VarSTO> getBuffer() {
        return buffer;
    }

    public void add(VarSTO var) {
        buffer.add(var);
        names.add(var.getName());
    }

    public void generate(SourceGenerator sg) {
        sg.printGlobalVarDecl(names);
        for (VarSTO var : buffer) {
            sg.printVarSTO(var);
        }
        sg.println("");
    }
}

