/**
 * This is reponsible for all code generating and formatting.
 */

import java.util.LinkedList;
import java.io.PrintStream;
import java.io.FileOutputStream;
import java.io.IOException;

class SourceGenerator {

    protected final static String GLOBAL_VAR_DECL_COM =
        "Global Variables";
    public final static String BSS_SECTION  = "\".bss\"";
    public final static String RO_SECTION   = "\".rodata\"";
    public final static String TEXT_SECTION = "\".text\"";

    protected final static String ALIGNMENT = "4";
    protected final static String INIT_LABEL = "__init";
    protected final static String STR_ENCODING = "asciz";

    protected final static String INT_FMT = "\"%d\"";
    protected final static String ENDL = "\"\\n\"";
    protected final static String BOOL_T_CHAR = "\"true\"";
    protected final static String BOOL_F_CHAR = "\"false\"";

    protected final static String OUT_FILE_NAME = "rc.s";
    protected static PrintStream out = null;

    public SourceGenerator() {
        if (out == null) {
            try {
                out = new PrintStream(new FileOutputStream(OUT_FILE_NAME));
            } catch (IOException ex) {
                System.err.format("IOException: %s%n", ex);
            }
        }
    }

    /**
     * An "__init_done" label must be present.
     */
    public void printGlobalInitializationGuard() {
        printlnComment("Global Initialization Guard.");
        printSet("__init", "%l0");
        printLoad("%l0", "%l1");
        printCmp("%l1", "%g0");
        printBne("__init_done");
        printSet("1", "%l1");
        printStore("%l1", "%l0");
        println("");
    }

    /**
     * Contains instructions that should be used for every program.
     */
    public void programInitialization() {
        printlnComment("Default String Formatters");
        printSection(RO_SECTION);
        printAlignment(ALIGNMENT);
        println("__int_fmt:\t" + STR_ENCODING + "\t" + INT_FMT);
        println("__endl:\t\t" + STR_ENCODING + "\t" + ENDL);
        println("__bool_t:\t" + STR_ENCODING + "\t" + BOOL_T_CHAR);
        println("__bool_f:\t" + STR_ENCODING + "\t" + BOOL_F_CHAR);
        println("");

        printlnComment(GLOBAL_VAR_DECL_COM);
        printSection(BSS_SECTION);
        // Init label. This is where global vars will be declared.
        print(INIT_LABEL + ":");
        printSkip(ALIGNMENT);
    }

    public void printVarSTO(VarSTO sto) {
        if (sto.getIsGlobal()) {
            printUnInitializedGlobalVarSTO(sto);
            return;
        }
    }

    public void printSave(String funcName) {
        printSet("SAVE." + funcName, "%g1");
        println("\tsave\t%sp, %g1, %sp");
        println("");
    }

    public void printAdd(String op1, String op2, String dest) {
        println("\tadd\t" + op1 + ", " + op2 + ", " + dest);
    }

    protected void printBne(String label) {
        println("\tbne\t" + label);
    }

    protected void printSet(String op1, String op2) {
        println("\tset\t" + op1 + ", " + op2);
    }

    protected void printLoad(String op1, String op2) {
        println("\tld\t" + "[" + op1 + "], " + op2);
    }

    protected void printStore(String op1, String op2) {
        println("\tst\t" + op1 + ", [" + op2 + "]");
    }

    protected void printCmp(String op1, String op2) {
        println("\tcmp\t" + op1 + ", " + op2);
    }

    protected void printNop() {
        printTabbedLn("nop");
    }

    protected void printCall(String str) {
        printTabbedLn("call\t" + str);
        printNop();
    }

    protected void printBne(String label,String comment) {
        println("\tbne\t" + label + "\t\t! " + comment);
    }

    protected void printSet(String op1, String op2,String comment) {
        println("\tset\t" + op1 + ", " + op2 + "\t\t! " + comment);
    }

    protected void printLoad(String op1, String op2,String comment) {
        println("\tld\t" + "[" + op1 + "], " + op2 + "\t\t! " + comment);
    }

    protected void printStore(String op1, String op2,String comment) {

        println("\tst\t" + op1 + ", [" + op2 + "]" + "\t\t! " +comment);
    }

    protected void printCmp(String op1, String op2,String comment) {

        println("\tcmp\t" + op1 + ", " + op2 + "\t\t! " +comment);
    }

    protected void printCall(String str,String comment) {

        printTabbedLn("call\t" + str + "\t\t! " + comment);
        printNop();
    }

    protected void printUnInitializedGlobalVarSTO(VarSTO sto) {
        print(sto.getName() + ":");
        printSkip("4");
    }

    protected void printWord() {
        printWord("0");
    }

    protected void printWord(String word) {
        printTabbedLn(".align " + word);
    }

    public void printGlobalVarDecl(LinkedList<String> names) {
        if (!names.isEmpty()) {
            String globalNames = ".global " + names.poll();
            for (String name : names) {
                globalNames += ", " + name;
            }

            printTabbedLn(globalNames);
        }
    }

    protected void printSkip(String str) {
        printTabbedLn(".skip\t" + str);
    }
    protected void printSection(String str) {
        println("");
        printTabbedLn(".section\t" + str);
        println("");
    }

    protected void printAlignment(String str) {
        printTabbedLn(".align\t" + str);
        println("");
    }

    protected void printSkip(String str, String comment) {

        printTabbedLn(".skip\t" + str + "\t\t! " + comment);
    }
    protected void printSection(String str, String comment) {

        printTabbedLn(".section\t" + str + "\t\t! " + comment);
        println("");
    }

    protected void printAlignment(String str, String comment) {

        printTabbedLn(".align\t" + str + "\t\t! " + comment);
        println("");
    }

    public void printlnComment(String str) {
        println("! ---- " + str + " ---- ");
    }

    protected void printTabbed(String str) {
        print("\t" + str);
    }

    protected void printTabbedLn(String str) {
        printTabbed(str + "\n");
    }

    public void print(String str) {
        out.print(str);
    }

    public void println(String str) {
        out.println(str);
    }

    public void println(String str, String comment) {

        out.println(str +  "\t\t! " + comment);
    }
}
