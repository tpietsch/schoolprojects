/**
 * Instances of this class build a buffer a code that will be printed.
 * This is due to the fact that context of a expression or statement
 * can effect how the assembly will be generated.
 */

import java.util.LinkedList;

interface SourceBuffer {
    public LinkedList<STO> getBuffer();
}
