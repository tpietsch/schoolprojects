import java.lang.UnsupportedOperationException;
import java.lang.IllegalStateException;


//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class StringSTO extends STO {

    private String s;

    public StringSTO(String c) {
        super(
                "__str_" + c,
                new ArrayType(new IntType(), c.length()));

        s = c;
    }

    public String toString(){
        return s;
    }

    public String getString(){
        return s;
    }

    @Override
    public boolean isString() { 
        return true; 
    }
}
