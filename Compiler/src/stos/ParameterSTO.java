class ParameterSTO extends VarSTO {

    private boolean isPassByReference;

    public ParameterSTO(String strName) {
        this(strName, null, false);
    }

    public ParameterSTO(String strName, Type t) {
        this(strName, t, false);
    }

    public ParameterSTO(String strName, Type t, boolean pbr) {
        super(strName, t, 0);
        System.out.println("\tWARNING(ParameterSTO()): " +
                "Parameter addressing isn't implemented yet. " +
                "Implment before passing parameters.");
        isPassByReference = pbr;
    }

    public boolean getIsPassByReference() {
        return isPassByReference;
    }
}
