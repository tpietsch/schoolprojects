import java.lang.UnsupportedOperationException;
import java.lang.IllegalStateException;


//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class ConstSTO extends ExprSTO implements Consty {

    private static int constCounter = 0;
    //----------------------------------------------------------------
    //	Constants have a value, so you should store them here.
    //	Note: We suggest using Java's Double class, which can hold
    //	floats and ints. You can then do .floatValue() or
    //	.intValue() to get the corresponding value based on the
    //	type. Booleans/Ptrs can easily be handled by ints.
    //	Feel free to change this if you don't like it!
    //----------------------------------------------------------------
    private Double		m_value;
    private String stringValue;

    public ConstSTO(String val, Type typ) {
        this("__const_" + ++constCounter + val, typ, val);
    }

    public ConstSTO(String strName, Type typ, String val) {
        super(strName, typ, -1);
        setStringValue(val);
        assignValueByType(typ, val);
    }

    public ConstSTO getConstSTO() {
        return this;
    }

    public void setStringValue(String v) {
        stringValue = v;
    }

    public String getStringValue() {
        return stringValue;
    }

    @Override
    public String loadVarInFloatReg(String dest) {
        Buffer buf = new Buffer();

        if (getType().isFloat()) {
            buf.write(loadFloatInReg(dest));
        } else {
            buf.write(setIntInFloatReg(dest));
        }

        return buf.toString();
    }

    private String setIntInFloatReg(String dest) {
        String tmpReg = "%i0";
        String tmpAddr = "%fp-4";
        Buffer buf = new Buffer();

        buf.writeSet("" + getIntValue(), tmpReg);
        buf.writeStore(tmpReg, tmpAddr);
        buf.writeLoad(tmpAddr, dest);
        buf.writeFitos(dest, dest);

        return buf.toString();
    }

    @Override
    public String loadVarInReg(String dest) {
        if (getType().isFloat()) {
            return loadFloatInReg(dest);
        }

        return loadIntInReg(dest);
    }

    public String loadFloatInReg(String dest) {
        Buffer buf = new Buffer();
        String comment =
            "-Loading float " + getValue() + " into reg " + dest + "-";
        String temp = "%l0";

        buf.writeTabbedComment(comment);
        buf.writeSet(getName(), temp);
        buf.writeLoad(temp, dest);

        return buf.toString();
    }

    public String getValByType() {
        Type t = getType();
        if (t.isInt()) {
            return "" + getIntValue();
        }

        if (t.isFloat()) {
            return "" + getFloatValue();
        }

        if (t.isBool()) {
            return "" + getBoolValue();
        }

        if (null == t) {
            return "" + 0;
        }

        return "";
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    @Override
    public boolean isConst()
    {
        return true;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void setValue(double val)
    {
        m_value = new Double(val);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public Double getValue()
    {
        return m_value;
    }


    public void setIntValue(String v) {
        setValue(Float.parseFloat(v));
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public int getIntValue()
    {
        return m_value.intValue();
    }

    public void setFloatValue(String v) {
        setValue(Float.parseFloat(v));
    }

    /*
     * Because of constant folding we only need to set the precomputed
     * value into `reg`.
     */
    public String loadIntInReg(String reg) {
        Buffer buffer = new Buffer();
        String cmt =
            "-Setting int " + getValue() + " into reg " + reg + "-";

        buffer.writeTabbedComment(cmt);
        buffer.writeSet(getValByType(), reg);

        return buffer.toString();
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public float getFloatValue()
    {
        return m_value.floatValue();
    }

    public void setBoolValue(String v) {
        if (Boolean.parseBoolean(v)) {
            setValue(1);
        } else {
            setValue(0);
        }
    }

    public void setNullValue() {
        setValue(0);
    }

    public int getNullValue() {
        return getIntValue();
    }

    public int getBoolValue() {
        return m_value.intValue();
    }

    private void assignValueByType(Type typ, String val) {
        String typNam = typ.getName();
        if (typNam.equals("int")) {
            setIntValue(val);
        } else if (typNam.equals("float")) {
            setFloatValue(val);
        } else if (typNam.equals("bool")) {
            setBoolValue(val);
        } else if (typNam.equals("nullptr")) {
            setNullValue();
        } else {
            throw new IllegalStateException(
                    "Type " + val + " not recognized.");
        }
    }

    private String genConstName() {
        constCounter++;

        return "__const_" + constCounter;
    }
}
