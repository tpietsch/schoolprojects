class ConstVarSTO extends VarSTO implements Consty {

    private ConstSTO constVal;

    public ConstVarSTO(String id, Type typ, ConstSTO csto) {
        super(id, typ);
        constVal = csto;
    }

    public ConstVarSTO(String id, Type typ, ConstSTO csto, int sp) {
        super(id, typ, sp);
        constVal = csto;
    }

    @Override
    public boolean isConst() {
        return true;
    }

    public ConstSTO getConstSTO() {
        return constVal;
    }
}
