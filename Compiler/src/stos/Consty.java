/*
 * Interface for ConstSTO and ConstVarSTOs.
 */
interface Consty {
    public ConstSTO getConstSTO();
}
