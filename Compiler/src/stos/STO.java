//---------------------------------------------------------------------
// Not abstract class so empty STO's can be assigned.
// This is generally so that we can avoid using `null`.
//---------------------------------------------------------------------
import java.util.*;
class STO
{
    private String m_strName;
    private Type m_type;
    private boolean m_isAddressable;
    private boolean m_isModifiable;

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public STO(String strName) {
        this(strName, null);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public STO(String strName, Type typ)
    {
        setName(strName);
        setType(typ);
        setIsAddressable(false);
        setIsModifiable(false);
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public String getName()
    {
        return m_strName;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public void setName(String str)
    {
        m_strName = str;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public Type getType()
    {
        return m_type;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    private void setType(Type type)
    {
        m_type = type;
    }

    //----------------------------------------------------------------
    // Addressable refers to if the object has an address. Variables
    // and declared constants have an address, whereas results from
    // expression like (x + y) and literal constants like 77 do not
    // have an address.
    //----------------------------------------------------------------
    public boolean getIsAddressable()
    {
        return m_isAddressable;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    protected void setIsAddressable(boolean addressable)
    {
        m_isAddressable = addressable;
    }

    //----------------------------------------------------------------
    // You shouldn't need to use these two routines directly
    //----------------------------------------------------------------
    public boolean getIsModifiable()
    {
        return m_isModifiable;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    protected void setIsModifiable(boolean modifiable)
    {
        m_isModifiable = modifiable;
    }

    //----------------------------------------------------------------
    // A modifiable L-value is an object that is both addressable and
    // modifiable. Objects like constants are not modifiable, so they
    // are not modifiable L-values.
    //----------------------------------------------------------------
    public boolean isModLValue()
    {
        return getIsModifiable() && getIsAddressable();
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    protected void setIsModLValue(boolean m)
    {
        setIsModifiable(m);
        setIsAddressable(m);
    }

    protected void setIsNonModLValue()
    {
        setIsModifiable(false);
        setIsAddressable(true);
    }

    public void addSTO(STO s){

    }
    public Vector<STO> getList(){
        return null;
    }
    public boolean isRef()
    {
        return false;
    }

    public boolean isConsty() {
        return this instanceof Consty;
    }

    public void setIsRef(boolean b)
    {

    }
    //----------------------------------------------------------------
    //	It will be helpful to ask a STO what specific STO it is.
    //	The Java operator instanceof will do this, but these methods
    //	will allow more flexibility (ErrorSTO is an example of the
    //	flexibility needed).
    //----------------------------------------------------------------
    public boolean isVar() { return false; }
    public boolean isConst() { return false; }
    public boolean isExpr() { return false; }
    public boolean isFunc() { return false; }
    public boolean isTypedef() { return false; }
    public boolean isError() { return false; }
    public boolean isEmpty() { return false; }
    public boolean isStruct() { return false; }
    public boolean isArraySTO(){ return false; }
    public boolean isThis() { return false; }
    public boolean isString() { return false; }
    public boolean isEndl() { return false; }
    public boolean isData() { return false; }
    public boolean isVarArray() { return false; }
}
