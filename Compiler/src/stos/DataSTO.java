/**
 * This abstract class stores values in memory.
 *
 * All DataSTO's have a base and offset to determine their location
 * in memory.
 */

import java.io.StringWriter;

abstract class DataSTO extends STO {

    private String base;
    private String offset;
    private StringWriter buffer;

    public static final String GLOBAL_BASE = "%g0";
    public static final String LOCAL_BASE = "%fp";

    public DataSTO(String n) {
        this(n, null);
    }

    /**
     * If it's given no stackpointer, then its a global variable
     * and its offset is its name.
     */
    public DataSTO(String n, Type t) {
        super(n, t);
        setBase(GLOBAL_BASE);
        setOffset(n);
    }

    public DataSTO(String n, Type t, int sp) {
        super(n, t);
        setBase(LOCAL_BASE);
        setOffset("" + sp);
    }

    public DataSTO(String n, Type t, String off) {
        super(n, t);
        setBase(GLOBAL_BASE);
        setOffset(off);
    }

    public String getBase() {
        return base;
    }

    private void setBase(String b) {
        base = b;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String o) {
        offset = o;
    }

    public int baseMinuesOffset(){
        //return Integer.parseInt(getBase()) + Integer.parseInt(getOffset());
        return 0;
    }


    @Override
    public boolean isData() {
        return true;
    }

    public String computeMemAddr(String destinationRegister) {
        buffer = new StringWriter();

        buffer.write("\t\tset\t\t" + getOffset() + ", " +
                destinationRegister + "\n");
        buffer.write("\t\tadd\t\t" + getBase() + ", " +
                destinationRegister + ", " + destinationRegister);

        return buffer.toString();
    }

    /**
     * For floats.
     *
     * FIXME: This won't work.
     */
    public String varAddrInF0() {
        Buffer buffer = new Buffer();
        String cmt = "-Copy address of " + getName() + " to %f0-";

        buffer.writeTabbedComment(cmt);
        buffer.writeSet(getOffset(), "%f0");
        buffer.writeAdd(getBase(), "%f0", "%f0");

        return buffer.toString();
    }

    public String loadVarInL1() {
        return loadVarInReg("%l1");
    }

    public String loadVarInFloatReg(String reg) {
        Buffer buf = new Buffer();

        buf.write(loadVarInReg(reg));

        if (!getType().isFloat()) {
            buf.writeFitos(reg, reg);
        }

        return buf.toString();
    }

    public String loadVarInReg(String reg) {
        String tempReg = "%l5";
        Buffer buffer = new Buffer();
        String comment = "--Load " + getName() + " into " + reg + "--";

        buffer.writeTabbedComment(comment);
        buffer.write(varAddrInReg(tempReg));
        buffer.writeLoad(tempReg, reg);

        return buffer.toString();
    }

    public String varAddrInL0() {
        return varAddrInReg("%l0");
    }

    public String varAddrInReg(String reg) {
        String tempReg = "%l5";
        Buffer buffer = new Buffer();
        buffer.writeTabbedComment(
                "-Copy address of " + getName() + " to " + reg + "-");
        buffer.writeSet(getOffset(), tempReg);
        buffer.writeAdd(getBase(), tempReg, reg);
        return buffer.toString();
    }

    /*
     * TODO: Floats are more complicated than this.
     */
    public String storeF1InVar() {
        Buffer buffer = new Buffer();

        buffer.writeTabbedComment("--Store %l1 into " + getName() + "--");
        buffer.write(varAddrInF0());
        buffer.writeStore("%f1", "%f0");

        return buffer.toString();
    }

    // Using register "%l7"
    public String storeRegInVar(String init) {
        Buffer buffer = new Buffer();
        String temp = "%l7";
        String comment = "--Store " + init + " into " + getName() +
            " (" + temp + ")--";

        buffer.writeTabbedComment(comment);
        buffer.write(varAddrInReg(temp));
        buffer.writeStore(init, temp);

        return buffer.toString();
    }

    public static String generatAltReg(String reg) {
        return reg.equals("%l0") ? "%l1" : "%l0";
    }
    public static String generatAltOutMulReg(String reg){

        if(reg.equals("%o0")){
            return "%o1";
        }

        return "%o0";
    }
}
