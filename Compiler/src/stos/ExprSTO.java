//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

import java.io.StringWriter;

class ExprSTO extends DataSTO {

    private boolean printed = false;

    // Can be null, implying that there is no operator for this expression.
    private Operator operator;

    public ExprSTO(String strName, Type typ, int sp) {
        super(strName, typ, sp);
    }

    @Override
    public String loadVarInReg(String dest) {
        if (!isPrinted() && hasOperator()) {
            Buffer buf = new Buffer();

            buf.write(generateAsm(dest));
            buf.write(super.loadVarInReg(dest));

            setPrinted(true);

            return buf.toString();
        }

        return super.loadVarInReg(dest);
    }

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean isExpr() {
		return true;
	}

    public boolean hasOperator() {
        return null != operator;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator o) {
        operator = o;
    }

    private boolean isPrinted() {
        return printed;
    }

    private void setPrinted(boolean p) {
        printed = p;
    }

    private String generateAsm(String destReg) {
        Operator op = getOperator();
        Buffer buf = new Buffer();
        String cmt = "-generating code to perform " +
            op.getName() + " op on " + getName() + " expression.";
        boolean isFloat = getType().isFloat();
        String tempReg = isFloat ? "%f0" : "%l0";

        if (null == op) {
            System.out.println("\tERROR(ExprSTO#generateAsm()): " +
                    "Operator is null");
            return "";
        }

        buf.writeTabbedComment(cmt);
        buf.write(op.generateOperationAsm(this, isFloat));

        return buf.toString();
    }
}
