import java.util.Vector;

//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class FuncSTO extends STO {

	private FuncPtrType type;
    FunctionBuffer buffer;

	public FuncSTO(String strName, Type returnType, boolean isRetRef,
            FunctionBuffer buf) {
		super(strName, new FuncPtrType(returnType, isRetRef));
        buffer = buf;
		type = (FuncPtrType) getType();
	}

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public boolean isFunc() {
		return true;
	}

    public FunctionBuffer getBuffer() {
        return buffer;
    }

	//----------------------------------------------------------------
	//
	//----------------------------------------------------------------
	public Type getReturnType () {
		return type.getReturnType();
	}

	public void setParameters(Vector<ParameterSTO> ps) {
		type.setParameters(ps);
	}

	public Vector<ParameterSTO> getParameters() {
		return type.getParameters();
	}

    public boolean isMain() {
        return getName().equals("main");
    }

	public int getNumberOfParameters() {
		Vector<ParameterSTO> ps = getParameters();
		if (null == ps) {
			return 0;
		}
		return ps.size();
	}

	public boolean getIsReturnByReference() {
		return type.getIsReturnByReference();
	}
}
