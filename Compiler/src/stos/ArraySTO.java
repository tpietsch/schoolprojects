import java.lang.UnsupportedOperationException;
import java.lang.IllegalStateException;
import java.util.*;


//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class ArraySTO extends DataSTO
{
    public int size;
    public Vector<STO> list = new Vector<STO>();


    public ArraySTO(){
    	super("");
    }
     public ArraySTO(String id, Type t){
    	super(id,t);
    }
    public Vector<STO> getList(){
    	return list;
    }
	public void addSTO(STO s){
    	list.add(s);
    }
    public boolean isArraySTO(){
    	return true;
    }

    public String generateAsm(String currAsm,String reg){

        //memory addres is located in reg
        Buffer b = new Buffer();
       

      
        int i = 0;
        for(STO s : list){
            if(s.getType().isInt() || s.getType().isBool() ){
               b.writeComment("Array Memory Address 0 into "+reg);
               b.write(currAsm);
               b.write("\n");
               b.writeAdd(reg,"%g0","%l0");

               b.write(((ConstSTO)s).loadIntInReg("%l2"));      
               b.writeStoreIndex("%l2","%l0",""+i*s.getType().getSize()); 
               i++;
               continue;
            }
            if(s.getType().isFloat()){

                ///to be done
               b.write(currAsm);
               b.write("\n");
               //b.writeAdd(reg,"%g0","%l0");

               b.write(((ConstSTO)s).loadFloatInReg("%l2"));      
               b.writeStoreIndex("%l2",reg,"-"+i*s.getType().getSize()); 
               i++;
               continue;
            }

             if(s.getType().isArray()){
               //do array code gen
                i++;
                continue;
            }

             if(s.getType().isStruct()){
               //do struct code gen
                i++;
                continue;
            }
          
        }

        return b.toString();
    }

}
