//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

import java.io.StringWriter;

class VarArraySTO extends VarSTO {

    private DataSTO array;
    private DataSTO index;
    public static int num = 0;

    public VarArraySTO(String strName, Type typ, int sp, STO a, STO i){
        super(strName, typ, sp);
        this.setOffset(((DataSTO)a).getOffset());
        
        array = (DataSTO) a;
        index = (DataSTO) i;
    }

    public String getNum(){
        return "" + (num = num + 1);
    }
    public String getNumCurr(){
        return ""+num ;
    }


    @Override
    public String loadVarInReg(String reg) {

        Buffer b = new Buffer();
        b.writeComment("-------LOADING ARRAY VAR INTO REG");
        

        b.write(array.varAddrInReg("%l0"));
        b.write("\n");

        b.writeMov("%l0","%l4");

        b.write(index.loadVarInReg("%i4"));
        b.write("\n");

        

        b.writeComment("------print f out of bounds error trever ------");
        b.writeComment("------ i2 is set to 0 for negative cmp ------");
        b.writeComment("------ i3 is the array size ------");
        b.writeComment("------ i4 is index ------");

        ///index value
        

        //array index highest
        b.writeSet(""+array.getType().getSize()/getType().getSize(),"%i3");
        b.writeSet(""+0,"%i2");
         
       //__exitArrayIndex  branch label

         //arrary index i4 and array size i3 0 in i2

            ///this works
         //compare index and 0
         b.writeCmp("%i4","%i3");

         b.writeSet("__bound","%o0");
         b.writeMov("%i4","%o1");
         b.writeMov("%i3","%o2");

         b.writeBge("__exitArrayIndex");


         //this is broken
         b.writeCmp("%g0","%i4");

         b.writeSet("__bound","%o0");
         b.writeMov("%i4","%o1");
         b.writeMov("%i3","%o2");


         b.writeBg("__exitArrayIndex");

         //compare index and size



        
        b.writeComment("------ print f error end ------");

        

        b.writeMov("%i4","%o0");

        b.writeSet(""+getType().getSize(),"%o1");
        b.writeCall(".mul");

        b.writeAdd("%l4","%o0","%l4");
        b.writeLoad("%l4",reg);
         

        return b.toString();

    }

    @Override
    public String varAddrInReg(String reg) {

        String tempReg = "%l5";
        Buffer b = new Buffer();

        b.writeSet(getOffset(), tempReg);
        b.writeAdd(getBase(), tempReg, tempReg);

        b.write(index.loadVarInReg("%o0"));
        b.write("\n");

        //must be a mul
        b.writeSet(""+getType().getSize(),"%o1");
        //b.writeAdd("%l1","%l4","%o0");

        b.writeCall(".mul");

        b.writeAdd("%o0",tempReg,reg);



        return b.toString();
    }

    @Override
    public String storeRegInVar(String init) {
        Buffer buffer = new Buffer();
        String temp = "%l7";

        buffer.write(varAddrInReg(temp));
        buffer.writeStore(init, temp);

        return buffer.toString();
    }

    @Override
    public boolean isVarArray(){
        return true;
    }
}
