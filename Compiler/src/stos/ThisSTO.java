class ThisSTO extends STO {

    public ThisSTO() {
        super("this", new VoidType());
    }
    public boolean isThis() {
        return true;
    }
}
