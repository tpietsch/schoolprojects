class StructSTO extends TypedefSTO {

    private StructSTO(String id, Type type) {
        super(id, type);
    }

    @Override
    public boolean isStruct() {
        return true;
    }

    public static STO getStructSTO(String id, Scope s) {
        StructFields fs = new StructFields(s);
        if (hasErrorSTO(id, fs)) {
            return new ErrorSTO("");
        }
        Type type = new StructType(id, fs.getSize(), fs);
        return new StructSTO(id, type);
    }

    private static boolean hasErrorSTO(String id, StructFields fields) {
        for (STO field : fields.getLocals()) {
            if (field.isError()) {
                return true;
            }
        }
        return false;
    }
}
