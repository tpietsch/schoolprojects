//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

import java.io.StringWriter;

class VarSTO extends DataSTO {
    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------

    public static int offsetPointer = 0;

    private boolean isRef;
    private boolean isGlobal = false;
    private boolean isStatic = false;
    private STO value;
    private boolean arrayInit = false;

    /*
     * Global variable constructor without type.
     * TODO: Maybe get rid of this.
     */
    public VarSTO(String strName) {
        this(strName, null);
    }

    /*
     * Global variable constructor.
     */
    public VarSTO(String strName, Type typ) {
        super(strName, typ);
        setIsModLValue(true);
        setIsGlobal(true);
        isRef = false;
    }

    public VarSTO(String strName, Type typ, String offset) {
        super(strName, typ, offset);
        setIsModLValue(true);
        isRef = false;
    }

    /*
     * Global variable constructor with init.
     */
    public VarSTO(String strName, Type typ, ExprSTO v) {
        super(strName, typ);
        setIsModLValue(true);
        setIsGlobal(true);
        isRef = false;
        setValue(v);
    }

    /*
     * Local variable constructor.
     */
    public VarSTO(String strName, Type typ, int sp) {
        super(strName, typ, sp);
        setIsModLValue(true);
        isRef = false;
    }

    /*
     * Local variable constructor with init.
     */
    public VarSTO(String strName, Type typ, int sp, ExprSTO v) {
        super(strName, typ, sp);
        setIsModLValue(true);
        isRef = false;
        setValue(v);
    }

    public void setValue(STO v) {
        value = v;
    }

    public STO getValue() {
        return value;
    }

    public boolean hasValue() {
        return null != getValue();
    }

    private void setIsGlobal(boolean isG) {
        isGlobal = isG;
    }

    public void setIsStatic(boolean isS) {
        isStatic = isS;
    }

    public boolean getIsStatic() {
        return isStatic;
    }

    /*
     * Pass in register which final init val will be stored.
     */
    public String generateInitialization() {
        String destReg = "%l2";

        if (!hasValue()) {
            return "";
        }

        if (getType().isFloat()) {
            String initReg = "%f1";

            
            if (getValue().isArraySTO()) {
                String initAsm = generateFloatInitialization(initReg);
                return initAsm;
            }
            return generateFloatInitialization(initReg) +
                storeRegInVar(initReg);
        }

        String initialValReg = "%l3";
        if (getValue().isArraySTO()) {
            String initAsm = generateIntInitialization(initialValReg);
            return initAsm;
        }
        
        String initAsm = generateIntInitialization(initialValReg) +
            storeRegInVar(initialValReg);

        return initAsm;
    }

    private String generateIntInitialization(String reg) {
        Buffer buffer = new Buffer();

        buffer.writeTabbedComment("---Initializing int " + getName() + "---");
        buffer.write(getInit(reg));

        return buffer.toString();
    }

    private String generateFloatInitialization(String dest) {
        Buffer buffer = new Buffer();
        String cmt = "---Initializing float " + getName() + "---";

        buffer.writeTabbedComment(cmt);
        buffer.write(getFloatInit(dest));

        return buffer.toString();
    }


    public boolean getIsGlobal() {
        return isGlobal;
    }

    //----------------------------------------------------------------
    //
    //----------------------------------------------------------------
    public boolean isVar()
    {
        return true;
    }

    public boolean isRef()
    {
        return isRef;
    }

    public void setIsRef(boolean b)
    {
        isRef = b;
    }

    public String doArrayInit(ArraySTO s,String r) {

        return s.generateAsm(this.computeMemAddr(r),r);
    }

       public String doArrayFloatInit(ArraySTO s,String r) {

        return s.generateAsm(this.computeMemAddr(r),r);
    }

    /**
     * Receives the initialized var sto, if it exists.
     */
    private String getInit(String destReg) {
        Buffer buf = new Buffer();

        if (getValue().isConst()) {
            ConstSTO con = ((Consty) getValue()).getConstSTO();

            if (con.getType().isInt()) {
                buf.writeSet("" + con.getIntValue(), destReg);

                return buf.toString();
            }

            if (con.getType().isFloat()) {
                buf.writeSet("" + con.getFloatValue(), destReg);

                return buf.toString();
            }

            if (con.getType().isBool()) {
                buf.writeSet("" + con.getBoolValue(), destReg);

                return buf.toString();
            }
        }

        if (getValue().isArraySTO()) {
           
        
           if(this.getType().getType().isFloat()){
            return doArrayFloatInit((ArraySTO)getValue(),destReg);
           } 
           return doArrayInit((ArraySTO)getValue(),destReg);
           
        }

        if (getValue().isVar()) {
            VarSTO var = (VarSTO) getValue();
           
            return var.loadVarInReg(destReg);
        }

        if (getValue().isExpr()) {
             
            ExprSTO expr = (ExprSTO) getValue();

            return generateExprAsm(expr, destReg);
        }

        System.out.println("\tERROR: No case found for Initialization");
        System.out.println("\t\t" + getValue());
        return "";
    }

    private String generateExprAsm(ExprSTO expr, String destReg) {
        Buffer buf = new Buffer();

        buf.write(expr.loadVarInReg(destReg));

        return buf.toString();
    }

    private String getFloatInit(String dest) {
        Buffer buf = new Buffer();
        
        if (getValue().isConst()) {
            ConstSTO con = ((Consty) getValue()).getConstSTO();
            buf.write(con.loadVarInFloatReg(dest));

            return buf.toString();
        } 

        if (getValue().isVar()) {
            VarSTO var = (VarSTO) getValue();

            return var.loadVarInFloatReg(dest);
        }

        if (getValue().isExpr()) {
            ExprSTO expr = (ExprSTO) getValue();

            return expr.loadVarInFloatReg(dest);
        }

        System.out.println("No case found for Initialization of float");
        return "";
    }
}
