import java.lang.UnsupportedOperationException;
import java.lang.IllegalStateException;


//---------------------------------------------------------------------
//
//---------------------------------------------------------------------

class EndlSTO extends StringSTO {

     public EndlSTO(String c){
        super(c);
    }

    public boolean isEndl() { 
        return true; 
    }
     public boolean isString() { 
        return false; 
    }
}
