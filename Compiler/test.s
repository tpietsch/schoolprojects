	.section	__TEXT,__text,regular,pure_instructions
	.file	1 "test.c"
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
Lfunc_begin0:
	.loc	1 3 0                   ## test.c:3:0
## BB#0:
	pushq	%rbp
Ltmp2:
	.cfi_def_cfa_offset 16
Ltmp3:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp4:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -8(%rbp)
	movl	$0, -12(%rbp)
	.loc	1 4 0 prologue_end      ## test.c:4:0
Ltmp5:
	movl	$-2, -16(%rbp)
	.loc	1 7 0                   ## test.c:7:0
Ltmp6:
	movl	$0, -52(%rbp)
LBB0_1:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$6, -52(%rbp)
	jg	LBB0_4
## BB#2:                                ##   in Loop: Header=BB0_1 Depth=1
	.loc	1 8 0                   ## test.c:8:0
Ltmp7:
	movl	-52(%rbp), %eax
	movslq	-52(%rbp), %rcx
	movl	%eax, -48(%rbp,%rcx,4)
Ltmp8:
## BB#3:                                ##   in Loop: Header=BB0_1 Depth=1
	.loc	1 7 0                   ## test.c:7:0
	movl	-52(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -52(%rbp)
	jmp	LBB0_1
Ltmp9:
LBB0_4:
	leaq	L_.str(%rip), %rdi
	.loc	1 11 0                  ## test.c:11:0
	movl	-16(%rbp), %esi
	movb	$0, %al
	callq	_printf
	leaq	L_.str1(%rip), %rdi
	leaq	-16(%rbp), %rsi
	.loc	1 12 0                  ## test.c:12:0
	movl	%eax, -56(%rbp)         ## 4-byte Spill
	movb	$0, %al
	callq	_printf
	movq	___stack_chk_guard@GOTPCREL(%rip), %rsi
	.loc	1 13 0                  ## test.c:13:0
	movl	-12(%rbp), %ecx
	movq	(%rsi), %rsi
	movq	-8(%rbp), %rdi
	cmpq	%rdi, %rsi
	movl	%eax, -60(%rbp)         ## 4-byte Spill
	movl	%ecx, -64(%rbp)         ## 4-byte Spill
	jne	LBB0_6
## BB#5:                                ## %SP_return
	movl	-64(%rbp), %eax         ## 4-byte Reload
	addq	$64, %rsp
	popq	%rbp
	ret
Ltmp10:
LBB0_6:                                 ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
Lfunc_end0:
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	 "%d\n"

L_.str1:                                ## @.str1
	.asciz	 "%p\n"

	.section	__TEXT,__text,regular,pure_instructions
Ltext_end:
	.section	__DATA,__data
Ldata_end:
	.section	__TEXT,__text,regular,pure_instructions
Lsection_end1:
	.section	__DWARF,__debug_info,regular,debug
Lsection_info:
	.section	__DWARF,__debug_abbrev,regular,debug
Lsection_abbrev:
	.section	__DWARF,__debug_aranges,regular,debug
	.section	__DWARF,__debug_macinfo,regular,debug
	.section	__DWARF,__debug_line,regular,debug
Lsection_line:
	.section	__DWARF,__debug_loc,regular,debug
	.section	__DWARF,__debug_pubtypes,regular,debug
	.section	__DWARF,__debug_str,regular,debug
Linfo_string:
	.section	__DWARF,__debug_ranges,regular,debug
Ldebug_range:
	.section	__DWARF,__debug_loc,regular,debug
Lsection_debug_loc:
	.section	__TEXT,__text,regular,pure_instructions
Ltext_begin:
	.section	__DATA,__data
	.section	__DWARF,__debug_info,regular,debug
L__DWARF__debug_info_begin0:
	.long	135                     ## Length of Compilation Unit Info
	.short	2                       ## DWARF version number
Lset0 = L__DWARF__debug_abbrev_begin-Lsection_abbrev ## Offset Into Abbrev. Section
	.long	Lset0
	.byte	8                       ## Address Size (in bytes)
	.byte	1                       ## Abbrev [1] 0xb:0x80 DW_TAG_compile_unit
Lset1 = Linfo_string0-Linfo_string      ## DW_AT_producer
	.long	Lset1
	.short	12                      ## DW_AT_language
Lset2 = Linfo_string1-Linfo_string      ## DW_AT_name
	.long	Lset2
	.quad	0                       ## DW_AT_low_pc
	.long	0                       ## DW_AT_stmt_list
Lset3 = Linfo_string2-Linfo_string      ## DW_AT_comp_dir
	.long	Lset3
	.byte	2                       ## Abbrev [2] 0x26:0x4a DW_TAG_subprogram
Lset4 = Linfo_string3-Linfo_string      ## DW_AT_name
	.long	Lset4
	.byte	1                       ## DW_AT_decl_file
	.byte	3                       ## DW_AT_decl_line
	.byte	1                       ## DW_AT_prototyped
	.long	112                     ## DW_AT_type
	.byte	1                       ## DW_AT_external
	.quad	Lfunc_begin0            ## DW_AT_low_pc
	.quad	Lfunc_end0              ## DW_AT_high_pc
	.byte	1                       ## DW_AT_frame_base
	.byte	86
	.byte	3                       ## Abbrev [3] 0x45:0xe DW_TAG_variable
Lset5 = Linfo_string5-Linfo_string      ## DW_AT_name
	.long	Lset5
	.byte	1                       ## DW_AT_decl_file
	.byte	4                       ## DW_AT_decl_line
	.long	112                     ## DW_AT_type
	.byte	2                       ## DW_AT_location
	.byte	145
	.byte	112
	.byte	3                       ## Abbrev [3] 0x53:0xe DW_TAG_variable
Lset6 = Linfo_string6-Linfo_string      ## DW_AT_name
	.long	Lset6
	.byte	1                       ## DW_AT_decl_file
	.byte	5                       ## DW_AT_decl_line
	.long	126                     ## DW_AT_type
	.byte	2                       ## DW_AT_location
	.byte	145
	.byte	80
	.byte	3                       ## Abbrev [3] 0x61:0xe DW_TAG_variable
Lset7 = Linfo_string7-Linfo_string      ## DW_AT_name
	.long	Lset7
	.byte	1                       ## DW_AT_decl_file
	.byte	6                       ## DW_AT_decl_line
	.long	112                     ## DW_AT_type
	.byte	2                       ## DW_AT_location
	.byte	145
	.byte	76
	.byte	0                       ## End Of Children Mark
	.byte	4                       ## Abbrev [4] 0x70:0x7 DW_TAG_base_type
Lset8 = Linfo_string4-Linfo_string      ## DW_AT_name
	.long	Lset8
	.byte	5                       ## DW_AT_encoding
	.byte	4                       ## DW_AT_byte_size
	.byte	5                       ## Abbrev [5] 0x77:0x7 DW_TAG_base_type
Lset9 = Linfo_string4-Linfo_string      ## DW_AT_name
	.long	Lset9
	.byte	4                       ## DW_AT_byte_size
	.byte	5                       ## DW_AT_encoding
	.byte	6                       ## Abbrev [6] 0x7e:0xc DW_TAG_array_type
	.long	112                     ## DW_AT_type
	.byte	7                       ## Abbrev [7] 0x83:0x6 DW_TAG_subrange_type
	.long	119                     ## DW_AT_type
	.byte	4                       ## DW_AT_upper_bound
	.byte	0                       ## End Of Children Mark
	.byte	0                       ## End Of Children Mark
L__DWARF__debug_info_end0:
	.section	__DWARF,__debug_abbrev,regular,debug
L__DWARF__debug_abbrev_begin:
	.byte	1                       ## Abbreviation Code
	.byte	17                      ## DW_TAG_compile_unit
	.byte	1                       ## DW_CHILDREN_yes
	.byte	37                      ## DW_AT_producer
	.byte	14                      ## DW_FORM_strp
	.byte	19                      ## DW_AT_language
	.byte	5                       ## DW_FORM_data2
	.byte	3                       ## DW_AT_name
	.byte	14                      ## DW_FORM_strp
	.byte	17                      ## DW_AT_low_pc
	.byte	1                       ## DW_FORM_addr
	.byte	16                      ## DW_AT_stmt_list
	.byte	6                       ## DW_FORM_data4
	.byte	27                      ## DW_AT_comp_dir
	.byte	14                      ## DW_FORM_strp
	.byte	0                       ## EOM(1)
	.byte	0                       ## EOM(2)
	.byte	2                       ## Abbreviation Code
	.byte	46                      ## DW_TAG_subprogram
	.byte	1                       ## DW_CHILDREN_yes
	.byte	3                       ## DW_AT_name
	.byte	14                      ## DW_FORM_strp
	.byte	58                      ## DW_AT_decl_file
	.byte	11                      ## DW_FORM_data1
	.byte	59                      ## DW_AT_decl_line
	.byte	11                      ## DW_FORM_data1
	.byte	39                      ## DW_AT_prototyped
	.byte	12                      ## DW_FORM_flag
	.byte	73                      ## DW_AT_type
	.byte	19                      ## DW_FORM_ref4
	.byte	63                      ## DW_AT_external
	.byte	12                      ## DW_FORM_flag
	.byte	17                      ## DW_AT_low_pc
	.byte	1                       ## DW_FORM_addr
	.byte	18                      ## DW_AT_high_pc
	.byte	1                       ## DW_FORM_addr
	.byte	64                      ## DW_AT_frame_base
	.byte	10                      ## DW_FORM_block1
	.byte	0                       ## EOM(1)
	.byte	0                       ## EOM(2)
	.byte	3                       ## Abbreviation Code
	.byte	52                      ## DW_TAG_variable
	.byte	0                       ## DW_CHILDREN_no
	.byte	3                       ## DW_AT_name
	.byte	14                      ## DW_FORM_strp
	.byte	58                      ## DW_AT_decl_file
	.byte	11                      ## DW_FORM_data1
	.byte	59                      ## DW_AT_decl_line
	.byte	11                      ## DW_FORM_data1
	.byte	73                      ## DW_AT_type
	.byte	19                      ## DW_FORM_ref4
	.byte	2                       ## DW_AT_location
	.byte	10                      ## DW_FORM_block1
	.byte	0                       ## EOM(1)
	.byte	0                       ## EOM(2)
	.byte	4                       ## Abbreviation Code
	.byte	36                      ## DW_TAG_base_type
	.byte	0                       ## DW_CHILDREN_no
	.byte	3                       ## DW_AT_name
	.byte	14                      ## DW_FORM_strp
	.byte	62                      ## DW_AT_encoding
	.byte	11                      ## DW_FORM_data1
	.byte	11                      ## DW_AT_byte_size
	.byte	11                      ## DW_FORM_data1
	.byte	0                       ## EOM(1)
	.byte	0                       ## EOM(2)
	.byte	5                       ## Abbreviation Code
	.byte	36                      ## DW_TAG_base_type
	.byte	0                       ## DW_CHILDREN_no
	.byte	3                       ## DW_AT_name
	.byte	14                      ## DW_FORM_strp
	.byte	11                      ## DW_AT_byte_size
	.byte	11                      ## DW_FORM_data1
	.byte	62                      ## DW_AT_encoding
	.byte	11                      ## DW_FORM_data1
	.byte	0                       ## EOM(1)
	.byte	0                       ## EOM(2)
	.byte	6                       ## Abbreviation Code
	.byte	1                       ## DW_TAG_array_type
	.byte	1                       ## DW_CHILDREN_yes
	.byte	73                      ## DW_AT_type
	.byte	19                      ## DW_FORM_ref4
	.byte	0                       ## EOM(1)
	.byte	0                       ## EOM(2)
	.byte	7                       ## Abbreviation Code
	.byte	33                      ## DW_TAG_subrange_type
	.byte	0                       ## DW_CHILDREN_no
	.byte	73                      ## DW_AT_type
	.byte	19                      ## DW_FORM_ref4
	.byte	47                      ## DW_AT_upper_bound
	.byte	11                      ## DW_FORM_data1
	.byte	0                       ## EOM(1)
	.byte	0                       ## EOM(2)
	.byte	0                       ## EOM(3)
L__DWARF__debug_abbrev_end:
	.section	__DWARF,__debug_aranges,regular,debug
	.section	__DWARF,__debug_ranges,regular,debug
	.section	__DWARF,__debug_macinfo,regular,debug
	.section	__DWARF,__debug_inlined,regular,debug
Lset10 = Ldebug_inlined_end1-Ldebug_inlined_begin1 ## Length of Debug Inlined Information Entry
	.long	Lset10
Ldebug_inlined_begin1:
	.short	2                       ## Dwarf Version
	.byte	8                       ## Address Size (in bytes)
Ldebug_inlined_end1:
	.section	__DWARF,__apple_names,regular,debug
Lnames_begin:
	.long	1212240712              ## Header Magic
	.short	1                       ## Header Version
	.short	0                       ## Header Hash Function
	.long	1                       ## Header Bucket Count
	.long	1                       ## Header Hash Count
	.long	12                      ## Header Data Length
	.long	0                       ## HeaderData Die Offset Base
	.long	1                       ## HeaderData Atom Count
	.short	1                       ## eAtomTypeDIEOffset
	.short	6                       ## DW_FORM_data4
	.long	0                       ## Bucket 0
	.long	2090499946              ## Hash in Bucket 0
	.long	LNames0-Lnames_begin    ## Offset in Bucket 0
LNames0:
Lset11 = Linfo_string3-Linfo_string     ## main
	.long	Lset11
	.long	1                       ## Num DIEs
	.long	38
	.long	0
	.section	__DWARF,__apple_objc,regular,debug
Lobjc_begin:
	.long	1212240712              ## Header Magic
	.short	1                       ## Header Version
	.short	0                       ## Header Hash Function
	.long	1                       ## Header Bucket Count
	.long	0                       ## Header Hash Count
	.long	12                      ## Header Data Length
	.long	0                       ## HeaderData Die Offset Base
	.long	1                       ## HeaderData Atom Count
	.short	1                       ## eAtomTypeDIEOffset
	.short	6                       ## DW_FORM_data4
	.long	-1                      ## Bucket 0
	.section	__DWARF,__apple_namespac,regular,debug
Lnamespac_begin:
	.long	1212240712              ## Header Magic
	.short	1                       ## Header Version
	.short	0                       ## Header Hash Function
	.long	1                       ## Header Bucket Count
	.long	0                       ## Header Hash Count
	.long	12                      ## Header Data Length
	.long	0                       ## HeaderData Die Offset Base
	.long	1                       ## HeaderData Atom Count
	.short	1                       ## eAtomTypeDIEOffset
	.short	6                       ## DW_FORM_data4
	.long	-1                      ## Bucket 0
	.section	__DWARF,__apple_types,regular,debug
Ltypes_begin:
	.long	1212240712              ## Header Magic
	.short	1                       ## Header Version
	.short	0                       ## Header Hash Function
	.long	1                       ## Header Bucket Count
	.long	1                       ## Header Hash Count
	.long	20                      ## Header Data Length
	.long	0                       ## HeaderData Die Offset Base
	.long	3                       ## HeaderData Atom Count
	.short	1                       ## eAtomTypeDIEOffset
	.short	6                       ## DW_FORM_data4
	.short	3                       ## eAtomTypeTag
	.short	5                       ## DW_FORM_data2
	.short	5                       ## eAtomTypeTypeFlags
	.short	11                      ## DW_FORM_data1
	.long	0                       ## Bucket 0
	.long	193495088               ## Hash in Bucket 0
	.long	Ltypes0-Ltypes_begin    ## Offset in Bucket 0
Ltypes0:
Lset12 = Linfo_string4-Linfo_string     ## int
	.long	Lset12
	.long	1                       ## Num DIEs
	.long	112
	.short	36
	.byte	0
	.long	0
	.section	__DWARF,__debug_pubtypes,regular,debug
Lset13 = Lpubtypes_end0-Lpubtypes_begin0 ## Length of Public Types Info
	.long	Lset13
Lpubtypes_begin0:
	.short	2                       ## DWARF Version
Lset14 = L__DWARF__debug_info_begin0-Lsection_info ## Offset of Compilation Unit Info
	.long	Lset14
Lset15 = L__DWARF__debug_info_end0-L__DWARF__debug_info_begin0 ## Compilation Unit Length
	.long	Lset15
	.long	0                       ## End Mark
Lpubtypes_end0:
	.section	__DWARF,__debug_str,regular,debug
Linfo_string0:
	.asciz	 "Apple LLVM version 5.0 (clang-500.2.79) (based on LLVM 3.3svn)"
Linfo_string1:
	.asciz	 "test.c"
Linfo_string2:
	.asciz	 "/Users/adamkuipers/Development/UCSD/cse131/131compiler/starterCode"
Linfo_string3:
	.asciz	 "main"
Linfo_string4:
	.asciz	 "int"
Linfo_string5:
	.asciz	 "d"
Linfo_string6:
	.asciz	 "x"
Linfo_string7:
	.asciz	 "i"

.subsections_via_symbols
