.global	foo, __________.MEMLEAK,main
.section	".data"
.align	4
______4: .word	4
______10: .word	0
______17: .word	1
______22: .word	3
_____23: .asciz "\n"
.align	4
_____25: .asciz "\n"
.align	4
______30: .word	0
_____35: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
_0x12staticFlag:	.skip	4
_0x12:	.skip	4
!CodeGen::doVarDecl::y
foo__0y25staticFlag:	.skip	4
foo__0y25:	.skip	4
!CodeGen::doVarDecl::x
main__0x220staticFlag:	.skip	4
main__0x220:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	_0x12staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____3 
 nop
set	4,%l1
set	_0x12,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	_0x12staticFlag, %l1
st	%l0, [%l1]
_____3:
_init_done:
set	main__0x220staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____21 
 nop
set	3,%l1
set	main__0x220,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	main__0x220staticFlag, %l1
st	%l0, [%l1]
_____21:
set	main__0x220,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_0x12,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	3,%o0
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 0) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	foo__0y25staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	foo__0y25,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	foo__0y25staticFlag, %l1
st	%l0, [%l1]
_____6:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	foo__0y25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bg  _____11 
 nop
set	-8,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____12 
 nop
_____11:
set	1,%l3
set	-8,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____12:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____13 
 nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	1,%l5
sub	%l4, %l5, %l5
set	-28,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
ba  _____14 
 nop
_____13:
_____14:
ret
restore

SAVE.foo = -(92 + 28) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____31 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____32 
 nop
_____31:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____32:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____33 
 nop
ba  _____34 
 nop
_____33:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
_____34:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

