.global	calledFunc, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "calledFunc has been called\n"
.align	4
______3: .word	0
______6: .word	1
_____10: .asciz ", "
.align	4
______12: .word	1
_____16: .asciz "\n"
.align	4
_____17: .asciz "\n"
.align	4
______19: .word	1
______26: .word	2
_____31: .asciz "i should be 3 but it is actually "
.align	4
_____32: .asciz "\n"
.align	4
______37: .word	0
_____42: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-4,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
add	%sp, -0, %sp
set	calledFunc, %l1
call %l1
nop
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-8,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-16,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-24,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-32,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	1,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-44,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-40,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-48,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	2,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-64,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____27 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____28 
 nop
_____27:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____28:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____29 
 nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
ba  _____30 
 nop
_____29:
_____30:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 64) & -8

calledFunc: 
set SAVE.calledFunc, %g1
save %sp, %g1, %sp
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
ret
restore

SAVE.calledFunc = -(92 + 0) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____38 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____39 
 nop
_____38:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____39:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____40 
 nop
ba  _____41 
 nop
_____40:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
_____41:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

