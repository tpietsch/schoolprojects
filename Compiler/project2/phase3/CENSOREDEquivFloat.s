.global	g, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	3
______6: .word	3
_____9: .asciz "temp should be false "
.align	4
_____12: .asciz "\n"
.align	4
_____13: .asciz "g == 3 "
.align	4
______17: .word	3
_____22: .asciz "\n"
.align	4
______26: .word	3
_____29: .asciz "temp should be true "
.align	4
_____32: .asciz "\n"
.align	4
_____33: .asciz "temp should be true "
.align	4
______37: .word	3
_____42: .asciz "\n"
.align	4
______46: .word	3
_____49: .asciz "temp should be false "
.align	4
_____52: .asciz "\n"
.align	4
_____53: .asciz "temp should be false "
.align	4
______57: .word	3
_____62: .asciz "\n"
.align	4
_____63: .single	0r3.0
______67: .single	0r3.0
_____70: .asciz "temp should be false "
.align	4
_____73: .asciz "\n"
.align	4
_____74: .asciz "temp should be false "
.align	4
_____75: .single	0r3.0
______79: .single	0r3.0
_____84: .asciz "\n"
.align	4
_____85: .asciz "temp should be true "
.align	4
_____86: .single	0r3.0
_____87: .asciz "true"
.align	4
_____88: .asciz "\n"
.align	4
_____89: .asciz "temp should be false "
.align	4
_____90: .single	0r3.0
_____91: .asciz "false"
.align	4
_____92: .asciz "\n"
.align	4
_____93: .asciz "temp should be false "
.align	4
_____94: .single	0r3.0
_____96: .asciz "false"
.align	4
_____97: .asciz "\n"
.align	4
_____98: .asciz "temp should be true "
.align	4
_____100: .asciz "true"
.align	4
_____101: .asciz "\n"
.align	4
______106: .word	0
_____111: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::g
g:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	3,%l1
set	g,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	g,%l1
add 	%g0, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	g,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	3,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-12,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____7 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____8 
 nop
_____7:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____8:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____10 
 nop
set	_boolF,%o1
ba  _____11 
 nop
_____10:
set	_boolT,%o1
_____11:
call printf
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	3,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-28,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____18 
 nop
set	-20,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____19 
 nop
_____18:
set	1,%l3
set	-20,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____19:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____20 
 nop
set	_boolF,%o1
ba  _____21 
 nop
_____20:
set	_boolT,%o1
_____21:
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	3,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-40,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____27 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____28 
 nop
_____27:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____28:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____30 
 nop
set	_boolF,%o1
ba  _____31 
 nop
_____30:
set	_boolT,%o1
_____31:
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	3,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-52,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____38 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____39 
 nop
_____38:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____39:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____40 
 nop
set	_boolF,%o1
ba  _____41 
 nop
_____40:
set	_boolT,%o1
_____41:
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	3,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-64,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____47 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____48 
 nop
_____47:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____48:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____50 
 nop
set	_boolF,%o1
ba  _____51 
 nop
_____50:
set	_boolT,%o1
_____51:
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	3,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-76,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____58 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____59 
 nop
_____58:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____59:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____60 
 nop
set	_boolF,%o1
ba  _____61 
 nop
_____60:
set	_boolT,%o1
_____61:
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	0, %l0
set	-80,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______67,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____68 
 nop
set	-80,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____69 
 nop
_____68:
set	1,%l3
set	-80,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____69:
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____71 
 nop
set	_boolF,%o1
ba  _____72 
 nop
_____71:
set	_boolT,%o1
_____72:
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
set	0, %l0
set	-92,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______79,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-100,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____80 
 nop
set	-92,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____81 
 nop
_____80:
set	1,%l3
set	-92,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____81:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____82 
 nop
set	_boolF,%o1
ba  _____83 
 nop
_____82:
set	_boolT,%o1
_____83:
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	_strFmt,%o0
set	_____91,%o1
call printf
nop
set	_strFmt,%o0
set	_____92,%o1
call printf
nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	1,%l5
xor	%l4, %l5, %l5
set	-104,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
set	0, %l0
set	-108,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	0,%l5
xor	%l4, %l5, %l5
set	-108,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 108) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____107 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____108 
 nop
_____107:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____108:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____109 
 nop
ba  _____110 
 nop
_____109:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____111,%o1
call printf
nop
_____110:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

