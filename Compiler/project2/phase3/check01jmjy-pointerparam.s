.global	foo, foo1, __________.MEMLEAK,main
.section	".data"
.align	4
_____3: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____4: .asciz "\n"
.align	4
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____8: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____11: .asciz "\n"
.align	4
______12: .word	3
______15: .word	4
______17: .word	5
_____20: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______21: .word	1
______28: .word	0
_____33: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	3,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4, %l1
add	%fp, %l1, %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	4,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-16,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16, %l1
add	%fp, %l1, %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-28,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____19 
 nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	1, %o0
call exit
nop
_____19:
st	%f1, [%l0]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-32, %l1
add	%fp, %l1, %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
add	%sp, -0, %sp
set	foo1, %l1
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 40) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____2 
 nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	1, %o0
call exit
nop
_____2:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
ret
restore

SAVE.foo = -(92 + 4) & -8

foo1: 
set SAVE.foo1, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
ld	[%l0], %f0
call printFloat
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____7 
 nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	1, %o0
call exit
nop
_____7:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____9 
 nop
set	_boolF,%o1
ba  _____10 
 nop
_____9:
set	_boolT,%o1
_____10:
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
ret
restore

SAVE.foo1 = -(92 + 8) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____29 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____30 
 nop
_____29:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____30:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____31 
 nop
ba  _____32 
 nop
_____31:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
_____32:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

