.global	func, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	10
_____4: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______7: .word	100
______10: .word	30
_____12: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____13: .asciz "\n"
.align	4
______18: .word	0
_____23: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	100,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	func, %l1
call %l1
nop
set	-12,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	30,%l1
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____11 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1, %o0
call exit
nop
_____11:
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 12) & -8

func: 
set SAVE.func, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	10,%l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____3 
 nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	1, %o0
call exit
nop
_____3:
st	%l1, [%l0]
set	-4, %i0
add	%fp, %i0, %i0
cmp	%i0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
ld	[%i0], %i0
ret
restore

ret
restore

SAVE.func = -(92 + 4) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____19 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____20 
 nop
_____19:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____20:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____21 
 nop
ba  _____22 
 nop
_____21:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
_____22:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

