.global	g, __________.MEMLEAK,main
.section	".data"
.align	4
_____3: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____5: .asciz "Double delete detected. Memory region has already been released in heap space.\n"
.align	4
______7: .word	0
______12: .word	0
_____17: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::g
g:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	1, %o0
set	44, %o1
call calloc
nop
set	0x07041A1F, %l0
st	%l0, [%o0]
add	%o0, 4, %o0
set	g,%l0
add 	%g0, %l0, %l0
st	%o0, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
add	%l1, 1, %l1
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
call __________.MEMLEAK
nop
set	1,%o0
call exit
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o0
cmp	%o0, %g0
bne  _____2 
 nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	1, %o0
call exit
nop
_____2:
sub	%o0, 4, %o0
ld	[%o0], %l0
cmp	%l0, %g0
bne  _____4 
 nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	1, %o0
call exit
nop
_____4:
st	%g0, [%o0]
call free
nop
set	0,%l1
set	g,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
sub	%l1, 1, %l1
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
call __________.MEMLEAK
nop
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%i0
ret
restore

call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 4) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____13 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____14 
 nop
_____13:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____14:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____15 
 nop
ba  _____16 
 nop
_____15:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
_____16:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

