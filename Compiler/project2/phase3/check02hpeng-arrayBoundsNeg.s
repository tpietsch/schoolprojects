.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	0
______5: .word	1
______7: .word	-1
______9: .word	4
______15: .word	12
_____21: .asciz "Index value of "
.align	4
_____22: .asciz " is outside legal range [0,3).\n"
.align	4
______27: .word	0
_____32: .asciz "Index value of "
.align	4
_____33: .asciz " is outside legal range [0,3).\n"
.align	4
______39: .word	0
_____44: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	1,%l5
sub	%l4, %l5, %l5
set	-24,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-40,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____17 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____18 
 nop
_____17:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____18:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____19 
 nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____20 
 nop
_____19:
_____20:
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____28 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____29 
 nop
_____28:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____29:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____30 
 nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____31 
 nop
_____30:
_____31:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 68) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____40 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____41 
 nop
_____40:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____41:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____42 
 nop
ba  _____43 
 nop
_____42:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
_____43:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

