.global	MYS.foo, test, __________.MEMLEAK,main
.section	".data"
.align	4
_____4: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____8: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____9: .asciz " "
.align	4
______11: .word	8
______13: .word	2
______18: .word	24
_____24: .asciz "Index value of "
.align	4
_____25: .asciz "2"
.align	4
_____26: .asciz " is outside legal range [0,3).\n"
.align	4
______31: .word	0
_____36: .asciz "Index value of "
.align	4
_____37: .asciz "2"
.align	4
_____38: .asciz " is outside legal range [0,3).\n"
.align	4
_____42: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____48: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______50: .word	8
______52: .word	2
______57: .word	24
_____63: .asciz "Index value of "
.align	4
_____64: .asciz "2"
.align	4
_____65: .asciz " is outside legal range [0,3).\n"
.align	4
______70: .word	0
_____75: .asciz "Index value of "
.align	4
_____76: .asciz "2"
.align	4
_____77: .asciz " is outside legal range [0,3).\n"
.align	4
_____81: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____83: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____84: .asciz "\n"
.align	4
______89: .word	0
_____94: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	8,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-44,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	24,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	24,%l1
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____20 
 nop
set	-48,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____21 
 nop
_____20:
set	1,%l3
set	-48,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____21:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____22 
 nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____23 
 nop
_____22:
_____23:
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____32 
 nop
set	-60,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____33 
 nop
_____32:
set	1,%l3
set	-60,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____33:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____34 
 nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____35 
 nop
_____34:
_____35:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____41 
 nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	1, %o0
call exit
nop
_____41:
ld	[%l1], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	MYS.foo, %l0
add	%g0, %l0, %l0
set	-92,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-92, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-84, %l1
add	%fp, %l1, %l1
ld	[%l1], %l1
st	%l1, [%l0]
set	-92, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____45 
 nop
mov	%l1, %o0
set	3,%o1
add	%sp, -0, %sp
ba  _____46 
 nop
_____45:
set	3,%o0
add	%sp, -0, %sp
_____46:
_____44:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____47 
 nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	1, %o0
call exit
nop
_____47:
call %l1
nop
set	8,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-104,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-108,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	24,%l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	24,%l1
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____59 
 nop
set	-108,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____60 
 nop
_____59:
set	1,%l3
set	-108,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____60:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____61 
 nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____62 
 nop
_____61:
_____62:
set	0, %l0
set	-120,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____71 
 nop
set	-120,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____72 
 nop
_____71:
set	1,%l3
set	-120,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____72:
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____73 
 nop
set	_strFmt,%o0
set	_____75,%o1
call printf
nop
set	_strFmt,%o0
set	_____76,%o1
call printf
nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____74 
 nop
_____73:
_____74:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____80 
 nop
set	_strFmt,%o0
set	_____81,%o1
call printf
nop
set	1, %o0
call exit
nop
_____80:
ld	[%l1], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4, %l2
add	%l2, %l1, %l1
set	-144,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____82 
 nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	1, %o0
call exit
nop
_____82:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 144) & -8

MYS.foo: 
set SAVE.MYS.foo, %g1
save %sp, %g1, %sp
set	-8,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-8, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____3 
 nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	1, %o0
call exit
nop
_____3:
ld	[%l1], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4, %l2
add	%l2, %l1, %l1
set	-20,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
st	%l1, [%l0]
ret
restore

SAVE.MYS.foo = -(92 + 20) & -8

test: 
set SAVE.test, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____7 
 nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	1, %o0
call exit
nop
_____7:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
ret
restore

SAVE.test = -(92 + 8) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____90 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____91 
 nop
_____90:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____91:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____92 
 nop
ba  _____93 
 nop
_____92:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
_____93:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

