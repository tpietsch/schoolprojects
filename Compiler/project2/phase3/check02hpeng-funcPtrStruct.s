.global	MYS.foo, __________.MEMLEAK,main
.section	".data"
.align	4
_____4: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____7: .asciz "\n"
.align	4
______9: .word	3
_____11: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______13: .word	6
_____15: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____18: .asciz "6: "
.align	4
_____25: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____26: .asciz "3: "
.align	4
_____31: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____34: .asciz "6: "
.align	4
_____39: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______44: .word	0
_____49: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-4, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-12,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	3,%l1
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____10 
 nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	1, %o0
call exit
nop
_____10:
st	%l1, [%l0]
set	-8, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-16,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	6,%l1
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____14 
 nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	1, %o0
call exit
nop
_____14:
st	%l1, [%l0]
set	MYS.foo, %l0
add	%g0, %l0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-4, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
set	-36, %l0
add	%fp, %l0, %l0
add	%l0, 4, %l0
ld	[%l0], %l0
st	%l0, [%l1]
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	MYS.foo, %l0
add	%g0, %l0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-48, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-8, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-48, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____22 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____23 
 nop
_____22:
add	%sp, -0, %sp
_____23:
_____21:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____24 
 nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
_____24:
call %l1
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____28 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____29 
 nop
_____28:
add	%sp, -0, %sp
_____29:
_____27:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____30 
 nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	1, %o0
call exit
nop
_____30:
call %l1
nop
set	MYS.foo, %l0
add	%g0, %l0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-8, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
set	-60, %l0
add	%fp, %l0, %l0
add	%l0, 4, %l0
ld	[%l0], %l0
st	%l0, [%l1]
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____36 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____37 
 nop
_____36:
add	%sp, -0, %sp
_____37:
_____35:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____38 
 nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	1, %o0
call exit
nop
_____38:
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 60) & -8

MYS.foo: 
set SAVE.MYS.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____3 
 nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	1, %o0
call exit
nop
_____3:
ld	[%l1], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-8,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
ret
restore

SAVE.MYS.foo = -(92 + 8) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____45 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____46 
 nop
_____45:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____46:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____47 
 nop
ba  _____48 
 nop
_____47:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
_____48:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

