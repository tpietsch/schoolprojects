.global	i1, i2, _0____415, pi, f1, f2, _0____10111, pf, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	2
_____6: .single	0r1.0
______7: .single	0r1.0
_____8: .single	0r2.0
______9: .single	0r2.0
______12: .word	5
______14: .word	6
_____16: .asciz "*pi: "
.align	4
_____18: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "*pf: "
.align	4
_____22: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____23: .asciz "\n"
.align	4
_____24: .asciz "*pa: "
.align	4
_____26: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____27: .asciz "\n"
.align	4
_____28: .asciz "*pb: "
.align	4
_____30: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____31: .asciz "\n"
.align	4
______36: .word	0
_____41: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::i1
i1:	.skip	4
!CodeGen::doVarDecl::i2
i2:	.skip	4
!CodeGen::doVarDecl::____4
_0____415:	.skip	4
!CodeGen::doVarDecl::pi
pi:	.skip	4
!CodeGen::doVarDecl::f1
f1:	.skip	4
!CodeGen::doVarDecl::f2
f2:	.skip	4
!CodeGen::doVarDecl::____10
_0____10111:	.skip	4
!CodeGen::doVarDecl::pf
pf:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	i1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	i2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	i1, %l1
add	%g0, %l1, %l1
set	_0____415,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0____415,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	pi,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______7,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f1,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______9,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f2,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	f1, %l1
add	%g0, %l1, %l1
set	_0____10111,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0____10111,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	pf,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4, %l1
add	%fp, %l1, %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-16,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16, %l1
add	%fp, %l1, %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	pi,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____17 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
_____17:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	pf,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____21 
 nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	1, %o0
call exit
nop
_____21:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____25 
 nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	1, %o0
call exit
nop
_____25:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____29 
 nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
_____29:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 24) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____37 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____38 
 nop
_____37:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____38:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____39 
 nop
ba  _____40 
 nop
_____39:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
_____40:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

