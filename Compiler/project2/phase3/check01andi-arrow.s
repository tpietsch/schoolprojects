.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	55
_____5: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______7: .word	41
_____9: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____13: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____15: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____16: .asciz "\n"
.align	4
_____19: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____21: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____22: .asciz "\n"
.align	4
_____25: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____26: .asciz "\n"
.align	4
______31: .word	0
_____36: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-8, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-16,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	55,%l1
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____4 
 nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	1, %o0
call exit
nop
_____4:
st	%l1, [%l0]
set	-8, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-24,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	41,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____8 
 nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	1, %o0
call exit
nop
_____8:
st	%l1, [%l0]
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8, %l1
add	%fp, %l1, %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8, %o2
set	-8, %o1
add	%fp, %o1, %o1
set	-40, %o0
add	%fp, %o0, %o0
call memmove
nop
set	-32, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____12 
 nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	1, %o0
call exit
nop
_____12:
ld	[%l1], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-48,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____14 
 nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	1, %o0
call exit
nop
_____14:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	-32, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____18 
 nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	1, %o0
call exit
nop
_____18:
ld	[%l1], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4, %l2
add	%l2, %l1, %l1
set	-56,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____20 
 nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	1, %o0
call exit
nop
_____20:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-64,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____24 
 nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
_____24:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 64) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____32 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____33 
 nop
_____32:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____33:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____34 
 nop
ba  _____35 
 nop
_____34:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
_____35:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

