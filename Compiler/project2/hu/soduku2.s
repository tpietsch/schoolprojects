	.section 	".rodata"
._endl:		.asciz "\n"
._intFmt:	.asciz	"%d"
._strFmt:	.asciz	"%s"
._bounds:	.asciz	"Index value of %d is outside legal range [0,%d).\n"
._deleteError:	.asciz	"Attempt to dereference NULL pointer.\n"
._boolT:		.asciz	"true"
._boolF:		.asciz	"false"
	.section	".data"
._floatOne:	.single	0r1.0
._floatNOne:	.single	0r-1.0
._intNOne:	.word	-1

	.section	".data"
._init:	.word	0

