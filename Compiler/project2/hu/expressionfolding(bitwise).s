.global	a1, a2, b1, b2, c1, c2, _0a1819, _0_____10111, _0_____12113, a, _0b114115, _0_____16117, _0_____18119, b, _0c120121, _0_____22123, _0_____24125, c, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	6
______3: .word	13
______4: .word	5
______5: .word	3
______6: .word	2
______7: .word	10
_____26: .asciz "\n"
.align	4
_____27: .asciz "\n"
.align	4
_____28: .asciz "\n"
.align	4
______33: .word	0
_____38: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a1
a1:	.skip	4
!CodeGen::doVarDecl::a2
a2:	.skip	4
!CodeGen::doVarDecl::b1
b1:	.skip	4
!CodeGen::doVarDecl::b2
b2:	.skip	4
!CodeGen::doVarDecl::c1
c1:	.skip	4
!CodeGen::doVarDecl::c2
c2:	.skip	4
!CodeGen::doVarDecl::a18
_0a1819:	.skip	4
!CodeGen::doVarDecl::_____10
_0_____10111:	.skip	4
!CodeGen::doVarDecl::_____12
_0_____12113:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	4
!CodeGen::doVarDecl::b114
_0b114115:	.skip	4
!CodeGen::doVarDecl::_____16
_0_____16117:	.skip	4
!CodeGen::doVarDecl::_____18
_0_____18119:	.skip	4
!CodeGen::doVarDecl::b
b:	.skip	4
!CodeGen::doVarDecl::c120
_0c120121:	.skip	4
!CodeGen::doVarDecl::_____22
_0_____22123:	.skip	4
!CodeGen::doVarDecl::_____24
_0_____24125:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	6,%l1
set	a1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	13,%l1
set	a2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	b1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	b2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	c1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	c2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	a1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0a1819,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	a1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0a1819,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	a2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____10111,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0a1819,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l4
set	_0_____10111,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l5
and	%l4, %l5, %l5
set	_0_____12113,%l3
add 	%g0, %l3, %l3
st	%l5, [%l3]
set	_0_____12113,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0b114115,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0b114115,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____16117,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0b114115,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l4
set	_0_____16117,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	_0_____18119,%l3
add 	%g0, %l3, %l3
st	%l5, [%l3]
set	_0_____18119,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	b,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	c1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0c120121,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	c1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0c120121,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	c2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____22123,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0c120121,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l4
set	_0_____22123,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	_0_____24125,%l3
add 	%g0, %l3, %l3
st	%l5, [%l3]
set	_0_____24125,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	b,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 0) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____34 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____35 
 nop
_____34:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____35:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____36 
 nop
ba  _____37 
 nop
_____36:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
_____37:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

