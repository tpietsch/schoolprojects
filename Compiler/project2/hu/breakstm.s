.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______3: .word	0
______4: .word	0
______10: .word	2
_____13: .asciz "loop1"
.align	4
_____14: .asciz "\n"
.align	4
______15: .word	0
______21: .word	1
_____24: .asciz "loop2"
.align	4
_____25: .asciz "\n"
.align	4
______26: .word	0
______32: .word	1
_____35: .asciz "loop3 "
.align	4
_____36: .asciz "\n"
.align	4
______38: .word	1
______43: .word	1
______48: .word	1
______56: .word	0
_____61: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____5,_____6) 
_____5:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	2,%l2
cmp	%l1, %l2
bl  _____11 
 nop
set	-16,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____12 
 nop
_____11:
set	1,%l3
set	-16,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____12:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____6 
 nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____16,_____17) 
_____16:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
bl  _____22 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____23 
 nop
_____22:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____23:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____17 
 nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____17 
 nop
! beginning of while loop(_____27,_____28) 
_____27:
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
bl  _____33 
 nop
set	-40,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____34 
 nop
_____33:
set	1,%l3
set	-40,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____34:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____28 
 nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-60,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____28 
 nop
ba  _____27 
 nop
_____28:
set	1,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-76,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____16 
 nop
_____17:
set	1,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-92,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____6 
 nop
ba  _____5 
 nop
_____6:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 96) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____57 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____58 
 nop
_____57:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____58:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____59 
 nop
ba  _____60 
 nop
_____59:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
_____60:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

