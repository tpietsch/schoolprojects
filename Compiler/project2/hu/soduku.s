	.section 	".rodata"
._endl:		.asciz "\n"
._intFmt:	.asciz	"%d"
._strFmt:	.asciz	"%s"
._bounds:	.asciz	"Index value of %d is outside legal range [0,%d).\n"
._deleteError:	.asciz	"Attempt to dereference NULL pointer.\n"
._boolT:		.asciz	"true"
._boolF:		.asciz	"false"
	.section	".data"
._floatOne:	.single	0r1.0
._floatNOne:	.single	0r-1.0
._intNOne:	.word	-1

	.section	".data"
._init:	.word	0

	.section	".text"
	.align	4
	.global	printBoard
printBoard:
	set	printBoard.SIZE,%g1
	save	%sp,%g1,%sp
	

	set	-4,%l0
	add	%fp,%l0,%l0
	set	0,%l1
	st	%l1,[%l0]
	set	-4,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-8,%l2
	add	%fp,%l2,%l2
	st	%l1,[%l2]

._Labelcmp1:
set	-12,%l0
add	%fp,%l0,%l0
set	9,%l1
st	%l1,[%l0]
	set	-8,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-12,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bl	._Labelcmp3
	nop
	set	-16,%l4
	add	%fp,%l4,%l4
	set	0,%l5
	st	%l5,[%l4]
	set	._Labelcmp4,%l6
	jmp	%l6
	nop
	._Labelcmp3:
	set	-16,%l4
	add	%fp,%l4,%l4
	set	1,%l5
	st	%l5,[%l4]
	set	._Labelcmp4,%l6
	jmp	%l6
	nop
	._Labelcmp4:
set	-16,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp2
nop
	set	._strFmt,%o0
	set	._endl,%o1
	call	printf
	nop

	set	-20,%l0
	add	%fp,%l0,%l0
	set	0,%l1
	st	%l1,[%l0]
	set	-20,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-24,%l2
	add	%fp,%l2,%l2
	st	%l1,[%l2]

._Labelcmp5:
set	-28,%l0
add	%fp,%l0,%l0
set	9,%l1
st	%l1,[%l0]
	set	-24,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-28,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bl	._Labelcmp7
	nop
	set	-32,%l4
	add	%fp,%l4,%l4
	set	0,%l5
	st	%l5,[%l4]
	set	._Labelcmp8,%l6
	jmp	%l6
	nop
	._Labelcmp7:
	set	-32,%l4
	add	%fp,%l4,%l4
	set	1,%l5
	st	%l5,[%l4]
	set	._Labelcmp8,%l6
	jmp	%l6
	nop
	._Labelcmp8:
set	-32,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp6
nop
set	-36,%l0
add	%fp,%l0,%l0
set	9,%l1
st	%l1,[%l0]
	set	-8,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%o0
	set	-36,%l1
	add	%fp,%l1,%l1
	ld	[%l1],%o1
	call	.mul
	nop
	set	-40,%l2
	add	%fp,%l2,%l2
	st	%o0,[%l2]
	set	-40,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-24,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	add	%l1,%l3,%l4
	set	-44,%l5
	add	%fp,%l5,%l5
	st	%l4,[%l5]
set	board,%l0
add	%g0,%l0,%l0
set	-44,%l2
add	%fp,%l2,%l2
ld	[%l2],%l1
set	81,%l6
cmp	%l6,%l1
bg	._Labelcmp9
nop
set	._bounds,%o0
ld	[%l2],%o1
set	81,%o2
call	printf
nop
set	1,%o0
call	exit
nop
._Labelcmp9:
set	0,%l3
cmp	%l1,%l3
bge	._Labelcmp10
nop
set	._bounds,%o0
ld	[%l2],%o1
set	81,%o2
call	printf
nop
set	1,%o0
call	exit
nop
._Labelcmp10:
sll	%l1,2,%l3
add	%l0,%l3,%l4
set	-48,%l5
add	%fp,%l5,%l5
st	%l4,[%l5]
	set	._intFmt,%o0
	set	-48,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l0
	ld	[%l0],%o1
	call	printf
	nop

set	-52,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
	set	-24,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-52,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	be	._Labelcmp11
	nop
	set	-56,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp12,%l4
	jmp	%l4
	nop
	._Labelcmp11:
	set	-56,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp12,%l4
	jmp	%l4
	nop
	._Labelcmp12:
set	-56,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp13
nop
set	-60,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
	set	-8,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-60,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	be	._Labelcmp15
	nop
	set	-64,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp16,%l4
	jmp	%l4
	nop
	._Labelcmp15:
	set	-64,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp16,%l4
	jmp	%l4
	nop
	._Labelcmp16:
	set	-64,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp13
	nop
	set	-68,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp14,%l4
	jmp	%l4
	nop
	._Labelcmp13:
	set	-68,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp14,%l4
	jmp	%l4
	nop
	._Labelcmp14:
set	-68,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp17
nop
	set	._strFmt,%o0
	set	._endl,%o1
	call	printf
	nop

set	._Labelcmp18,%l4
jmp	%l4
nop
._Labelcmp17:
	set	._Label8,%o0
	call	printf
	nop

set	._Labelcmp18,%l4
jmp	%l4
nop
._Labelcmp18:
	set	-24,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-72,%l2
	add	%fp,%l2,%l2
	st	%l1,[%l2]
	set	1,%l3
	add	%l1,%l3,%l4
	set	-24,%l0
	add	%fp,%l0,%l0
	st	%l4,[%l0]
set	._Labelcmp5,%l0
jmp	%l0
nop
._Labelcmp6:
	set	-8,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-76,%l2
	add	%fp,%l2,%l2
	st	%l1,[%l2]
	set	1,%l3
	add	%l1,%l3,%l4
	set	-8,%l0
	add	%fp,%l0,%l0
	st	%l4,[%l0]
set	._Labelcmp1,%l0
jmp	%l0
nop
._Labelcmp2:
	ret
	restore
printBoard.SIZE = -(92 + 76) & -8

	.section	".text"
	.align	4
	.global	validNumForBox
validNumForBox:
	set	validNumForBox.SIZE,%g1
	save	%sp,%g1,%sp
	
	set	68,%l0
	add	%fp,%l0,%l0
	st	%i0,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	st	%i1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	st	%i2,[%l0]






set	-24,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-24,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp19
	nop
	set	-28,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp20,%l4
	jmp	%l4
	nop
	._Labelcmp19:
	set	-28,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp20,%l4
	jmp	%l4
	nop
	._Labelcmp20:
set	-28,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp21
nop
set	-32,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-32,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp23
	nop
	set	-36,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp24,%l4
	jmp	%l4
	nop
	._Labelcmp23:
	set	-36,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp24,%l4
	jmp	%l4
	nop
	._Labelcmp24:
	set	-36,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp21
	nop
	set	-40,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp22,%l4
	jmp	%l4
	nop
	._Labelcmp21:
	set	-40,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp22,%l4
	jmp	%l4
	nop
	._Labelcmp22:
set	-40,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp25
nop
set	-44,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-44,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp27
	nop
	set	-48,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp28,%l4
	jmp	%l4
	nop
	._Labelcmp27:
	set	-48,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp28,%l4
	jmp	%l4
	nop
	._Labelcmp28:
	set	-48,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp25
	nop
	set	-52,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp26,%l4
	jmp	%l4
	nop
	._Labelcmp25:
	set	-52,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp26,%l4
	jmp	%l4
	nop
	._Labelcmp26:
set	-52,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp29
nop
set	-56,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-56,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp31
	nop
	set	-60,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp32,%l4
	jmp	%l4
	nop
	._Labelcmp31:
	set	-60,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp32,%l4
	jmp	%l4
	nop
	._Labelcmp32:
	set	-60,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp29
	nop
	set	-64,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp30,%l4
	jmp	%l4
	nop
	._Labelcmp29:
	set	-64,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp30,%l4
	jmp	%l4
	nop
	._Labelcmp30:
set	-64,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp33
nop
set	-68,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
set	-68,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-4,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-72,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
set	-72,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-12,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-76,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
set	-76,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-80,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
set	-80,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-16,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	._Labelcmp34,%l4
jmp	%l4
nop
._Labelcmp33:
._Labelcmp34:
set	-84,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-84,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp35
	nop
	set	-88,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp36,%l4
	jmp	%l4
	nop
	._Labelcmp35:
	set	-88,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp36,%l4
	jmp	%l4
	nop
	._Labelcmp36:
set	-88,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp37
nop
set	-92,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-92,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp39
	nop
	set	-96,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp40,%l4
	jmp	%l4
	nop
	._Labelcmp39:
	set	-96,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp40,%l4
	jmp	%l4
	nop
	._Labelcmp40:
	set	-96,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp37
	nop
	set	-100,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp38,%l4
	jmp	%l4
	nop
	._Labelcmp37:
	set	-100,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp38,%l4
	jmp	%l4
	nop
	._Labelcmp38:
set	-100,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp41
nop
set	-104,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-104,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp43
	nop
	set	-108,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp44,%l4
	jmp	%l4
	nop
	._Labelcmp43:
	set	-108,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp44,%l4
	jmp	%l4
	nop
	._Labelcmp44:
	set	-108,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp41
	nop
	set	-112,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp42,%l4
	jmp	%l4
	nop
	._Labelcmp41:
	set	-112,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp42,%l4
	jmp	%l4
	nop
	._Labelcmp42:
set	-112,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp45
nop
set	-116,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-116,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp47
	nop
	set	-120,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp48,%l4
	jmp	%l4
	nop
	._Labelcmp47:
	set	-120,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp48,%l4
	jmp	%l4
	nop
	._Labelcmp48:
	set	-120,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp45
	nop
	set	-124,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp46,%l4
	jmp	%l4
	nop
	._Labelcmp45:
	set	-124,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp46,%l4
	jmp	%l4
	nop
	._Labelcmp46:
set	-124,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp49
nop
set	-128,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
set	-128,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-4,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-132,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
set	-132,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-12,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-136,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
set	-136,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-140,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
set	-140,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-16,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	._Labelcmp50,%l4
jmp	%l4
nop
._Labelcmp49:
._Labelcmp50:
set	-144,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-144,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp51
	nop
	set	-148,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp52,%l4
	jmp	%l4
	nop
	._Labelcmp51:
	set	-148,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp52,%l4
	jmp	%l4
	nop
	._Labelcmp52:
set	-148,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp53
nop
set	-152,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-152,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp55
	nop
	set	-156,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp56,%l4
	jmp	%l4
	nop
	._Labelcmp55:
	set	-156,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp56,%l4
	jmp	%l4
	nop
	._Labelcmp56:
	set	-156,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp53
	nop
	set	-160,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp54,%l4
	jmp	%l4
	nop
	._Labelcmp53:
	set	-160,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp54,%l4
	jmp	%l4
	nop
	._Labelcmp54:
set	-160,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp57
nop
set	-164,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-164,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp59
	nop
	set	-168,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp60,%l4
	jmp	%l4
	nop
	._Labelcmp59:
	set	-168,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp60,%l4
	jmp	%l4
	nop
	._Labelcmp60:
	set	-168,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp57
	nop
	set	-172,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp58,%l4
	jmp	%l4
	nop
	._Labelcmp57:
	set	-172,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp58,%l4
	jmp	%l4
	nop
	._Labelcmp58:
set	-172,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp61
nop
set	-176,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-176,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp63
	nop
	set	-180,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp64,%l4
	jmp	%l4
	nop
	._Labelcmp63:
	set	-180,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp64,%l4
	jmp	%l4
	nop
	._Labelcmp64:
	set	-180,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp61
	nop
	set	-184,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp62,%l4
	jmp	%l4
	nop
	._Labelcmp61:
	set	-184,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp62,%l4
	jmp	%l4
	nop
	._Labelcmp62:
set	-184,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp65
nop
set	-188,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
set	-188,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-4,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-192,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
set	-192,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-12,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-196,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
set	-196,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-200,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
set	-200,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-16,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	._Labelcmp66,%l4
jmp	%l4
nop
._Labelcmp65:
._Labelcmp66:
set	-204,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-204,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp67
	nop
	set	-208,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp68,%l4
	jmp	%l4
	nop
	._Labelcmp67:
	set	-208,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp68,%l4
	jmp	%l4
	nop
	._Labelcmp68:
set	-208,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp69
nop
set	-212,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-212,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp71
	nop
	set	-216,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp72,%l4
	jmp	%l4
	nop
	._Labelcmp71:
	set	-216,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp72,%l4
	jmp	%l4
	nop
	._Labelcmp72:
	set	-216,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp69
	nop
	set	-220,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp70,%l4
	jmp	%l4
	nop
	._Labelcmp69:
	set	-220,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp70,%l4
	jmp	%l4
	nop
	._Labelcmp70:
set	-220,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp73
nop
set	-224,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-224,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp75
	nop
	set	-228,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp76,%l4
	jmp	%l4
	nop
	._Labelcmp75:
	set	-228,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp76,%l4
	jmp	%l4
	nop
	._Labelcmp76:
	set	-228,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp73
	nop
	set	-232,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp74,%l4
	jmp	%l4
	nop
	._Labelcmp73:
	set	-232,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp74,%l4
	jmp	%l4
	nop
	._Labelcmp74:
set	-232,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp77
nop
set	-236,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-236,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp79
	nop
	set	-240,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp80,%l4
	jmp	%l4
	nop
	._Labelcmp79:
	set	-240,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp80,%l4
	jmp	%l4
	nop
	._Labelcmp80:
	set	-240,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp77
	nop
	set	-244,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp78,%l4
	jmp	%l4
	nop
	._Labelcmp77:
	set	-244,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp78,%l4
	jmp	%l4
	nop
	._Labelcmp78:
set	-244,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp81
nop
set	-248,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
set	-248,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-4,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-252,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
set	-252,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-12,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-256,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
set	-256,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-260,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
set	-260,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-16,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	._Labelcmp82,%l4
jmp	%l4
nop
._Labelcmp81:
._Labelcmp82:
set	-264,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-264,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp83
	nop
	set	-268,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp84,%l4
	jmp	%l4
	nop
	._Labelcmp83:
	set	-268,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp84,%l4
	jmp	%l4
	nop
	._Labelcmp84:
set	-268,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp85
nop
set	-272,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-272,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp87
	nop
	set	-276,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp88,%l4
	jmp	%l4
	nop
	._Labelcmp87:
	set	-276,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp88,%l4
	jmp	%l4
	nop
	._Labelcmp88:
	set	-276,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp85
	nop
	set	-280,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp86,%l4
	jmp	%l4
	nop
	._Labelcmp85:
	set	-280,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp86,%l4
	jmp	%l4
	nop
	._Labelcmp86:
set	-280,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp89
nop
set	-284,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-284,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp91
	nop
	set	-288,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp92,%l4
	jmp	%l4
	nop
	._Labelcmp91:
	set	-288,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp92,%l4
	jmp	%l4
	nop
	._Labelcmp92:
	set	-288,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp89
	nop
	set	-292,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp90,%l4
	jmp	%l4
	nop
	._Labelcmp89:
	set	-292,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp90,%l4
	jmp	%l4
	nop
	._Labelcmp90:
set	-292,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp93
nop
set	-296,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-296,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp95
	nop
	set	-300,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp96,%l4
	jmp	%l4
	nop
	._Labelcmp95:
	set	-300,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp96,%l4
	jmp	%l4
	nop
	._Labelcmp96:
	set	-300,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp93
	nop
	set	-304,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp94,%l4
	jmp	%l4
	nop
	._Labelcmp93:
	set	-304,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp94,%l4
	jmp	%l4
	nop
	._Labelcmp94:
set	-304,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp97
nop
set	-308,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
set	-308,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-4,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-312,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
set	-312,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-12,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-316,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
set	-316,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-320,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
set	-320,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-16,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	._Labelcmp98,%l4
jmp	%l4
nop
._Labelcmp97:
._Labelcmp98:
set	-324,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-324,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp99
	nop
	set	-328,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp100,%l4
	jmp	%l4
	nop
	._Labelcmp99:
	set	-328,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp100,%l4
	jmp	%l4
	nop
	._Labelcmp100:
set	-328,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp101
nop
set	-332,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-332,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp103
	nop
	set	-336,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp104,%l4
	jmp	%l4
	nop
	._Labelcmp103:
	set	-336,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp104,%l4
	jmp	%l4
	nop
	._Labelcmp104:
	set	-336,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp101
	nop
	set	-340,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp102,%l4
	jmp	%l4
	nop
	._Labelcmp101:
	set	-340,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp102,%l4
	jmp	%l4
	nop
	._Labelcmp102:
set	-340,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp105
nop
set	-344,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-344,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp107
	nop
	set	-348,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp108,%l4
	jmp	%l4
	nop
	._Labelcmp107:
	set	-348,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp108,%l4
	jmp	%l4
	nop
	._Labelcmp108:
	set	-348,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp105
	nop
	set	-352,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp106,%l4
	jmp	%l4
	nop
	._Labelcmp105:
	set	-352,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp106,%l4
	jmp	%l4
	nop
	._Labelcmp106:
set	-352,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp109
nop
set	-356,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-356,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp111
	nop
	set	-360,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp112,%l4
	jmp	%l4
	nop
	._Labelcmp111:
	set	-360,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp112,%l4
	jmp	%l4
	nop
	._Labelcmp112:
	set	-360,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp109
	nop
	set	-364,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp110,%l4
	jmp	%l4
	nop
	._Labelcmp109:
	set	-364,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp110,%l4
	jmp	%l4
	nop
	._Labelcmp110:
set	-364,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp113
nop
set	-368,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
set	-368,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-4,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-372,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
set	-372,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-12,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-376,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
set	-376,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-380,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
set	-380,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-16,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	._Labelcmp114,%l4
jmp	%l4
nop
._Labelcmp113:
._Labelcmp114:
set	-384,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-384,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp115
	nop
	set	-388,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp116,%l4
	jmp	%l4
	nop
	._Labelcmp115:
	set	-388,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp116,%l4
	jmp	%l4
	nop
	._Labelcmp116:
set	-388,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp117
nop
set	-392,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-392,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp119
	nop
	set	-396,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp120,%l4
	jmp	%l4
	nop
	._Labelcmp119:
	set	-396,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp120,%l4
	jmp	%l4
	nop
	._Labelcmp120:
	set	-396,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp117
	nop
	set	-400,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp118,%l4
	jmp	%l4
	nop
	._Labelcmp117:
	set	-400,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp118,%l4
	jmp	%l4
	nop
	._Labelcmp118:
set	-400,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp121
nop
set	-404,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-404,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp123
	nop
	set	-408,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp124,%l4
	jmp	%l4
	nop
	._Labelcmp123:
	set	-408,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp124,%l4
	jmp	%l4
	nop
	._Labelcmp124:
	set	-408,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp121
	nop
	set	-412,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp122,%l4
	jmp	%l4
	nop
	._Labelcmp121:
	set	-412,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp122,%l4
	jmp	%l4
	nop
	._Labelcmp122:
set	-412,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp125
nop
set	-416,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-416,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp127
	nop
	set	-420,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp128,%l4
	jmp	%l4
	nop
	._Labelcmp127:
	set	-420,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp128,%l4
	jmp	%l4
	nop
	._Labelcmp128:
	set	-420,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp125
	nop
	set	-424,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp126,%l4
	jmp	%l4
	nop
	._Labelcmp125:
	set	-424,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp126,%l4
	jmp	%l4
	nop
	._Labelcmp126:
set	-424,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp129
nop
set	-428,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
set	-428,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-4,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-432,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
set	-432,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-12,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-436,%l0
add	%fp,%l0,%l0
set	0,%l1
st	%l1,[%l0]
set	-436,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-440,%l0
add	%fp,%l0,%l0
set	2,%l1
st	%l1,[%l0]
set	-440,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-16,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	._Labelcmp130,%l4
jmp	%l4
nop
._Labelcmp129:
._Labelcmp130:
set	-444,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-444,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp131
	nop
	set	-448,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp132,%l4
	jmp	%l4
	nop
	._Labelcmp131:
	set	-448,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp132,%l4
	jmp	%l4
	nop
	._Labelcmp132:
set	-448,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp133
nop
set	-452,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-452,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp135
	nop
	set	-456,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp136,%l4
	jmp	%l4
	nop
	._Labelcmp135:
	set	-456,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp136,%l4
	jmp	%l4
	nop
	._Labelcmp136:
	set	-456,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp133
	nop
	set	-460,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp134,%l4
	jmp	%l4
	nop
	._Labelcmp133:
	set	-460,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp134,%l4
	jmp	%l4
	nop
	._Labelcmp134:
set	-460,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp137
nop
set	-464,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-464,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp139
	nop
	set	-468,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp140,%l4
	jmp	%l4
	nop
	._Labelcmp139:
	set	-468,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp140,%l4
	jmp	%l4
	nop
	._Labelcmp140:
	set	-468,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp137
	nop
	set	-472,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp138,%l4
	jmp	%l4
	nop
	._Labelcmp137:
	set	-472,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp138,%l4
	jmp	%l4
	nop
	._Labelcmp138:
set	-472,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp141
nop
set	-476,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-476,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp143
	nop
	set	-480,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp144,%l4
	jmp	%l4
	nop
	._Labelcmp143:
	set	-480,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp144,%l4
	jmp	%l4
	nop
	._Labelcmp144:
	set	-480,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp141
	nop
	set	-484,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp142,%l4
	jmp	%l4
	nop
	._Labelcmp141:
	set	-484,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp142,%l4
	jmp	%l4
	nop
	._Labelcmp142:
set	-484,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp145
nop
set	-488,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
set	-488,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-4,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-492,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
set	-492,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-12,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-496,%l0
add	%fp,%l0,%l0
set	3,%l1
st	%l1,[%l0]
set	-496,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-500,%l0
add	%fp,%l0,%l0
set	5,%l1
st	%l1,[%l0]
set	-500,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-16,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	._Labelcmp146,%l4
jmp	%l4
nop
._Labelcmp145:
._Labelcmp146:
set	-504,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-504,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp147
	nop
	set	-508,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp148,%l4
	jmp	%l4
	nop
	._Labelcmp147:
	set	-508,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp148,%l4
	jmp	%l4
	nop
	._Labelcmp148:
set	-508,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp149
nop
set	-512,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
	set	72,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-512,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp151
	nop
	set	-516,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp152,%l4
	jmp	%l4
	nop
	._Labelcmp151:
	set	-516,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp152,%l4
	jmp	%l4
	nop
	._Labelcmp152:
	set	-516,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp149
	nop
	set	-520,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp150,%l4
	jmp	%l4
	nop
	._Labelcmp149:
	set	-520,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp150,%l4
	jmp	%l4
	nop
	._Labelcmp150:
set	-520,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp153
nop
set	-524,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-524,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	bge	._Labelcmp155
	nop
	set	-528,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp156,%l4
	jmp	%l4
	nop
	._Labelcmp155:
	set	-528,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp156,%l4
	jmp	%l4
	nop
	._Labelcmp156:
	set	-528,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp153
	nop
	set	-532,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp154,%l4
	jmp	%l4
	nop
	._Labelcmp153:
	set	-532,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp154,%l4
	jmp	%l4
	nop
	._Labelcmp154:
set	-532,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l4
cmp	%l1,%l4
be	._Labelcmp157
nop
set	-536,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
	set	76,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-536,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp159
	nop
	set	-540,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp160,%l4
	jmp	%l4
	nop
	._Labelcmp159:
	set	-540,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp160,%l4
	jmp	%l4
	nop
	._Labelcmp160:
	set	-540,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	set	0,%l4
	cmp	%l3,%l4
	be	._Labelcmp157
	nop
	set	-544,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp158,%l4
	jmp	%l4
	nop
	._Labelcmp157:
	set	-544,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp158,%l4
	jmp	%l4
	nop
	._Labelcmp158:
set	-544,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp161
nop
set	-548,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
set	-548,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-4,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-552,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
set	-552,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-12,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-556,%l0
add	%fp,%l0,%l0
set	6,%l1
st	%l1,[%l0]
set	-556,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	-560,%l0
add	%fp,%l0,%l0
set	8,%l1
st	%l1,[%l0]
set	-560,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-16,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
set	._Labelcmp162,%l4
jmp	%l4
nop
._Labelcmp161:
set	._Labelcmp162,%l4
jmp	%l4
nop
._Labelcmp162:
set	-8,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-20,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
._Labelcmp163:
	set	-4,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-12,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp165
	nop
	set	-564,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp166,%l4
	jmp	%l4
	nop
	._Labelcmp165:
	set	-564,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp166,%l4
	jmp	%l4
	nop
	._Labelcmp166:
set	-564,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp164
nop
set	-20,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	-8,%l2
add	%fp,%l2,%l2
st	%l1,[%l2]
._Labelcmp167:
	set	-8,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-16,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	ble	._Labelcmp169
	nop
	set	-568,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp170,%l4
	jmp	%l4
	nop
	._Labelcmp169:
	set	-568,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp170,%l4
	jmp	%l4
	nop
	._Labelcmp170:
set	-568,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp168
nop
	set	-4,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%o0
	set	width,%l1
	add	%g0,%l1,%l1
	ld	[%l1],%o1
	call	.mul
	nop
	set	-572,%l2
	add	%fp,%l2,%l2
	st	%o0,[%l2]
	set	-572,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l1
	set	-8,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	add	%l1,%l3,%l4
	set	-576,%l5
	add	%fp,%l5,%l5
	st	%l4,[%l5]
set	board,%l0
add	%g0,%l0,%l0
set	-576,%l2
add	%fp,%l2,%l2
ld	[%l2],%l1
set	81,%l6
cmp	%l6,%l1
bg	._Labelcmp171
nop
set	._bounds,%o0
ld	[%l2],%o1
set	81,%o2
call	printf
nop
set	1,%o0
call	exit
nop
._Labelcmp171:
set	0,%l3
cmp	%l1,%l3
bge	._Labelcmp172
nop
set	._bounds,%o0
ld	[%l2],%o1
set	81,%o2
call	printf
nop
set	1,%o0
call	exit
nop
._Labelcmp172:
sll	%l1,2,%l3
add	%l0,%l3,%l4
set	-580,%l5
add	%fp,%l5,%l5
st	%l4,[%l5]
	set	-580,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%l0
	ld	[%l0],%l1
	set	68,%l2
	add	%fp,%l2,%l2
	ld	[%l2],%l3
	cmp	%l1,%l3
	be	._Labelcmp173
	nop
	set	-584,%l2
	add	%fp,%l2,%l2
	set	0,%l3
	st	%l3,[%l2]
	set	._Labelcmp174,%l4
	jmp	%l4
	nop
	._Labelcmp173:
	set	-584,%l2
	add	%fp,%l2,%l2
	set	1,%l3
	st	%l3,[%l2]
	set	._Labelcmp174,%l4
	jmp	%l4
	nop
	._Labelcmp174:
set	-584,%l0
add	%fp,%l0,%l0
ld	[%l0],%l1
set	0,%l2
cmp	%l1,%l2
be	._Labelcmp175
nop
	set	-588,%l0
	add	%fp,%l0,%l0
	set	0,%l1
	st	%l1,[%l0]
	set	-588,%l0
	add	%fp,%l0,%l0
	ld	[%l0],%i0
		ret
		restore
	set	._Labelcmp176,%l4
	jmp	%l4
	nop
	._Labelcmp175:
	._Labelcmp176:
		set	-8,%l0
		add	%fp,%l0,%l0
		ld	[%l0],%l1
		set	-592,%l2
		add	%fp,%l2,%l2
		st	%l1,[%l2]
		set	1,%l3
		add	%l1,%l3,%l4
		set	-8,%l0
		add	%fp,%l0,%l0
		st	%l4,[%l0]
	set	._Labelcmp167,%l0
	jmp	%l0
	nop
	._Labelcmp168:
		set	-4,%l0
		add	%fp,%l0,%l0
		ld	[%l0],%l1
		set	-596,%l2
		add	%fp,%l2,%l2
		st	%l1,[%l2]
		set	1,%l3
		add	%l1,%l3,%l4
		set	-4,%l0
		add	%fp,%l0,%l0
		st	%l4,[%l0]
	set	._Labelcmp163,%l0
	jmp	%l0
	nop
	._Labelcmp164:
		set	-600,%l0
		add	%fp,%l0,%l0
		set	1,%l1
		st	%l1,[%l0]
		set	-600,%l0
		add	%fp,%l0,%l0
		ld	[%l0],%i0
			ret
			restore
			ret
			restore
		validNumForBox.SIZE = -(92 + 600) & -8
		
			.section	".text"
			.align	4
			.global	validNumForCol
		validNumForCol:
			set	validNumForCol.SIZE,%g1
			save	%sp,%g1,%sp
			
			set	68,%l0
			add	%fp,%l0,%l0
			st	%i0,[%l0]
			set	72,%l0
			add	%fp,%l0,%l0
			st	%i1,[%l0]
		
			set	-4,%l0
			add	%fp,%l0,%l0
			set	0,%l1
			st	%l1,[%l0]
			set	-4,%l0
			add	%fp,%l0,%l0
			ld	[%l0],%l1
			set	-8,%l2
			add	%fp,%l2,%l2
			st	%l1,[%l2]
		
		._Labelcmp177:
			set	-8,%l0
			add	%fp,%l0,%l0
			ld	[%l0],%l1
			set	row_length,%l2
			add	%g0,%l2,%l2
			ld	[%l2],%l3
			cmp	%l1,%l3
			bl	._Labelcmp179
			nop
			set	-12,%l4
			add	%fp,%l4,%l4
			set	0,%l5
			st	%l5,[%l4]
			set	._Labelcmp180,%l6
			jmp	%l6
			nop
			._Labelcmp179:
			set	-12,%l4
			add	%fp,%l4,%l4
			set	1,%l5
			st	%l5,[%l4]
			set	._Labelcmp180,%l6
			jmp	%l6
			nop
			._Labelcmp180:
		set	-12,%l0
		add	%fp,%l0,%l0
		ld	[%l0],%l1
		set	0,%l2
		cmp	%l1,%l2
		be	._Labelcmp178
		nop
			set	-8,%l0
			add	%fp,%l0,%l0
			ld	[%l0],%o0
			set	width,%l1
			add	%g0,%l1,%l1
			ld	[%l1],%o1
			call	.mul
			nop
			set	-16,%l2
			add	%fp,%l2,%l2
			st	%o0,[%l2]
			set	-16,%l0
			add	%fp,%l0,%l0
			ld	[%l0],%l1
			set	72,%l2
			add	%fp,%l2,%l2
			ld	[%l2],%l3
			add	%l1,%l3,%l4
			set	-20,%l5
			add	%fp,%l5,%l5
			st	%l4,[%l5]
		set	board,%l0
		add	%g0,%l0,%l0
		set	-20,%l2
		add	%fp,%l2,%l2
		ld	[%l2],%l1
		set	81,%l6
		cmp	%l6,%l1
		bg	._Labelcmp181
		nop
		set	._bounds,%o0
		ld	[%l2],%o1
		set	81,%o2
		call	printf
		nop
		set	1,%o0
		call	exit
		nop
		._Labelcmp181:
		set	0,%l3
		cmp	%l1,%l3
		bge	._Labelcmp182
		nop
		set	._bounds,%o0
		ld	[%l2],%o1
		set	81,%o2
		call	printf
		nop
		set	1,%o0
		call	exit
		nop
		._Labelcmp182:
		sll	%l1,2,%l3
		add	%l0,%l3,%l4
		set	-24,%l5
		add	%fp,%l5,%l5
		st	%l4,[%l5]
			set	-24,%l0
			add	%fp,%l0,%l0
			ld	[%l0],%l0
			ld	[%l0],%l1
			set	68,%l2
			add	%fp,%l2,%l2
			ld	[%l2],%l3
			cmp	%l1,%l3
			be	._Labelcmp183
			nop
			set	-28,%l2
			add	%fp,%l2,%l2
			set	0,%l3
			st	%l3,[%l2]
			set	._Labelcmp184,%l4
			jmp	%l4
			nop
			._Labelcmp183:
			set	-28,%l2
			add	%fp,%l2,%l2
			set	1,%l3
			st	%l3,[%l2]
			set	._Labelcmp184,%l4
			jmp	%l4
			nop
			._Labelcmp184:
		set	-28,%l0
		add	%fp,%l0,%l0
		ld	[%l0],%l1
		set	0,%l2
		cmp	%l1,%l2
		be	._Labelcmp185
		nop
			set	-32,%l0
			add	%fp,%l0,%l0
			set	0,%l1
			st	%l1,[%l0]
			set	-32,%l0
			add	%fp,%l0,%l0
			ld	[%l0],%i0
				ret
				restore
			set	._Labelcmp186,%l4
			jmp	%l4
			nop
			._Labelcmp185:
			._Labelcmp186:
				set	-8,%l0
				add	%fp,%l0,%l0
				ld	[%l0],%l1
				set	-36,%l2
				add	%fp,%l2,%l2
				st	%l1,[%l2]
				set	1,%l3
				add	%l1,%l3,%l4
				set	-8,%l0
				add	%fp,%l0,%l0
				st	%l4,[%l0]
			set	._Labelcmp177,%l0
			jmp	%l0
			nop
			._Labelcmp178:
				set	-40,%l0
				add	%fp,%l0,%l0
				set	1,%l1
				st	%l1,[%l0]
				set	-40,%l0
				add	%fp,%l0,%l0
				ld	[%l0],%i0
					ret
					restore
					ret
					restore
				validNumForCol.SIZE = -(92 + 40) & -8
				
					.section	".text"
					.align	4
					.global	validNumForRow
				validNumForRow:
					set	validNumForRow.SIZE,%g1
					save	%sp,%g1,%sp
					
					set	68,%l0
					add	%fp,%l0,%l0
					st	%i0,[%l0]
					set	72,%l0
					add	%fp,%l0,%l0
					st	%i1,[%l0]
				
					set	-4,%l0
					add	%fp,%l0,%l0
					set	0,%l1
					st	%l1,[%l0]
					set	-4,%l0
					add	%fp,%l0,%l0
					ld	[%l0],%l1
					set	-8,%l2
					add	%fp,%l2,%l2
					st	%l1,[%l2]
				
				._Labelcmp187:
					set	-8,%l0
					add	%fp,%l0,%l0
					ld	[%l0],%l1
					set	col_length,%l2
					add	%g0,%l2,%l2
					ld	[%l2],%l3
					cmp	%l1,%l3
					bl	._Labelcmp189
					nop
					set	-12,%l4
					add	%fp,%l4,%l4
					set	0,%l5
					st	%l5,[%l4]
					set	._Labelcmp190,%l6
					jmp	%l6
					nop
					._Labelcmp189:
					set	-12,%l4
					add	%fp,%l4,%l4
					set	1,%l5
					st	%l5,[%l4]
					set	._Labelcmp190,%l6
					jmp	%l6
					nop
					._Labelcmp190:
				set	-12,%l0
				add	%fp,%l0,%l0
				ld	[%l0],%l1
				set	0,%l2
				cmp	%l1,%l2
				be	._Labelcmp188
				nop
					set	72,%l0
					add	%fp,%l0,%l0
					ld	[%l0],%o0
					set	width,%l1
					add	%g0,%l1,%l1
					ld	[%l1],%o1
					call	.mul
					nop
					set	-16,%l2
					add	%fp,%l2,%l2
					st	%o0,[%l2]
					set	-16,%l0
					add	%fp,%l0,%l0
					ld	[%l0],%l1
					set	-8,%l2
					add	%fp,%l2,%l2
					ld	[%l2],%l3
					add	%l1,%l3,%l4
					set	-20,%l5
					add	%fp,%l5,%l5
					st	%l4,[%l5]
				set	board,%l0
				add	%g0,%l0,%l0
				set	-20,%l2
				add	%fp,%l2,%l2
				ld	[%l2],%l1
				set	81,%l6
				cmp	%l6,%l1
				bg	._Labelcmp191
				nop
				set	._bounds,%o0
				ld	[%l2],%o1
				set	81,%o2
				call	printf
				nop
				set	1,%o0
				call	exit
				nop
				._Labelcmp191:
				set	0,%l3
				cmp	%l1,%l3
				bge	._Labelcmp192
				nop
				set	._bounds,%o0
				ld	[%l2],%o1
				set	81,%o2
				call	printf
				nop
				set	1,%o0
				call	exit
				nop
				._Labelcmp192:
				sll	%l1,2,%l3
				add	%l0,%l3,%l4
				set	-24,%l5
				add	%fp,%l5,%l5
				st	%l4,[%l5]
					set	-24,%l0
					add	%fp,%l0,%l0
					ld	[%l0],%l0
					ld	[%l0],%l1
					set	68,%l2
					add	%fp,%l2,%l2
					ld	[%l2],%l3
					cmp	%l1,%l3
					be	._Labelcmp193
					nop
					set	-28,%l2
					add	%fp,%l2,%l2
					set	0,%l3
					st	%l3,[%l2]
					set	._Labelcmp194,%l4
					jmp	%l4
					nop
					._Labelcmp193:
					set	-28,%l2
					add	%fp,%l2,%l2
					set	1,%l3
					st	%l3,[%l2]
					set	._Labelcmp194,%l4
					jmp	%l4
					nop
					._Labelcmp194:
				set	-28,%l0
				add	%fp,%l0,%l0
				ld	[%l0],%l1
				set	0,%l2
				cmp	%l1,%l2
				be	._Labelcmp195
				nop
					set	-32,%l0
					add	%fp,%l0,%l0
					set	0,%l1
					st	%l1,[%l0]
					set	-32,%l0
					add	%fp,%l0,%l0
					ld	[%l0],%i0
						ret
						restore
					set	._Labelcmp196,%l4
					jmp	%l4
					nop
					._Labelcmp195:
					._Labelcmp196:
						set	-8,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%l1
						set	-36,%l2
						add	%fp,%l2,%l2
						st	%l1,[%l2]
						set	1,%l3
						add	%l1,%l3,%l4
						set	-8,%l0
						add	%fp,%l0,%l0
						st	%l4,[%l0]
					set	._Labelcmp187,%l0
					jmp	%l0
					nop
					._Labelcmp188:
						set	-40,%l0
						add	%fp,%l0,%l0
						set	1,%l1
						st	%l1,[%l0]
						set	-40,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%i0
							ret
							restore
							ret
							restore
						validNumForRow.SIZE = -(92 + 40) & -8
						
							.section	".text"
							.align	4
							.global	validForCell
						validForCell:
							set	validForCell.SIZE,%g1
							save	%sp,%g1,%sp
							
							set	68,%l0
							add	%fp,%l0,%l0
							st	%i0,[%l0]
							set	72,%l0
							add	%fp,%l0,%l0
							st	%i1,[%l0]
							set	76,%l0
							add	%fp,%l0,%l0
							st	%i2,[%l0]
						
						
						
						
						set	76,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%o0
						set	68,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%o1
						call	validNumForRow
						nop
						set	-16,%l0
						add	%fp,%l0,%l0
						st	%o0,[%l0]
						set	-16,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%l1
						set	-4,%l2
						add	%fp,%l2,%l2
						st	%l1,[%l2]
						set	76,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%o0
						set	72,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%o1
						call	validNumForCol
						nop
						set	-20,%l0
						add	%fp,%l0,%l0
						st	%o0,[%l0]
						set	-20,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%l1
						set	-8,%l2
						add	%fp,%l2,%l2
						st	%l1,[%l2]
						set	76,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%o0
						set	68,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%o1
						set	72,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%o2
						call	validNumForBox
						nop
						set	-24,%l0
						add	%fp,%l0,%l0
						st	%o0,[%l0]
						set	-24,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%l1
						set	-12,%l2
						add	%fp,%l2,%l2
						st	%l1,[%l2]
						set	-28,%l0
						add	%fp,%l0,%l0
						set	1,%l1
						st	%l1,[%l0]
							set	-4,%l0
							add	%fp,%l0,%l0
							ld	[%l0],%l1
							set	-28,%l2
							add	%fp,%l2,%l2
							ld	[%l2],%l3
							cmp	%l1,%l3
							be	._Labelcmp197
							nop
							set	-32,%l2
							add	%fp,%l2,%l2
							set	0,%l3
							st	%l3,[%l2]
							set	._Labelcmp198,%l4
							jmp	%l4
							nop
							._Labelcmp197:
							set	-32,%l2
							add	%fp,%l2,%l2
							set	1,%l3
							st	%l3,[%l2]
							set	._Labelcmp198,%l4
							jmp	%l4
							nop
							._Labelcmp198:
						set	-32,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%l1
						set	0,%l4
						cmp	%l1,%l4
						be	._Labelcmp199
						nop
						set	-36,%l0
						add	%fp,%l0,%l0
						set	1,%l1
						st	%l1,[%l0]
							set	-8,%l0
							add	%fp,%l0,%l0
							ld	[%l0],%l1
							set	-36,%l2
							add	%fp,%l2,%l2
							ld	[%l2],%l3
							cmp	%l1,%l3
							be	._Labelcmp201
							nop
							set	-40,%l2
							add	%fp,%l2,%l2
							set	0,%l3
							st	%l3,[%l2]
							set	._Labelcmp202,%l4
							jmp	%l4
							nop
							._Labelcmp201:
							set	-40,%l2
							add	%fp,%l2,%l2
							set	1,%l3
							st	%l3,[%l2]
							set	._Labelcmp202,%l4
							jmp	%l4
							nop
							._Labelcmp202:
							set	-40,%l2
							add	%fp,%l2,%l2
							ld	[%l2],%l3
							set	0,%l4
							cmp	%l3,%l4
							be	._Labelcmp199
							nop
							set	-44,%l2
							add	%fp,%l2,%l2
							set	1,%l3
							st	%l3,[%l2]
							set	._Labelcmp200,%l4
							jmp	%l4
							nop
							._Labelcmp199:
							set	-44,%l2
							add	%fp,%l2,%l2
							set	0,%l3
							st	%l3,[%l2]
							set	._Labelcmp200,%l4
							jmp	%l4
							nop
							._Labelcmp200:
						set	-44,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%l1
						set	0,%l4
						cmp	%l1,%l4
						be	._Labelcmp203
						nop
						set	-48,%l0
						add	%fp,%l0,%l0
						set	1,%l1
						st	%l1,[%l0]
							set	-12,%l0
							add	%fp,%l0,%l0
							ld	[%l0],%l1
							set	-48,%l2
							add	%fp,%l2,%l2
							ld	[%l2],%l3
							cmp	%l1,%l3
							be	._Labelcmp205
							nop
							set	-52,%l2
							add	%fp,%l2,%l2
							set	0,%l3
							st	%l3,[%l2]
							set	._Labelcmp206,%l4
							jmp	%l4
							nop
							._Labelcmp205:
							set	-52,%l2
							add	%fp,%l2,%l2
							set	1,%l3
							st	%l3,[%l2]
							set	._Labelcmp206,%l4
							jmp	%l4
							nop
							._Labelcmp206:
							set	-52,%l2
							add	%fp,%l2,%l2
							ld	[%l2],%l3
							set	0,%l4
							cmp	%l3,%l4
							be	._Labelcmp203
							nop
							set	-56,%l2
							add	%fp,%l2,%l2
							set	1,%l3
							st	%l3,[%l2]
							set	._Labelcmp204,%l4
							jmp	%l4
							nop
							._Labelcmp203:
							set	-56,%l2
							add	%fp,%l2,%l2
							set	0,%l3
							st	%l3,[%l2]
							set	._Labelcmp204,%l4
							jmp	%l4
							nop
							._Labelcmp204:
						set	-56,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%l1
						set	0,%l4
						cmp	%l1,%l4
						be	._Labelcmp207
						nop
						set	-60,%l0
						add	%fp,%l0,%l0
						set	1,%l1
						st	%l1,[%l0]
							set	-60,%l2
							add	%fp,%l2,%l2
							ld	[%l2],%l3
							set	0,%l4
							cmp	%l3,%l4
							be	._Labelcmp207
							nop
							set	-64,%l2
							add	%fp,%l2,%l2
							set	1,%l3
							st	%l3,[%l2]
							set	._Labelcmp208,%l4
							jmp	%l4
							nop
							._Labelcmp207:
							set	-64,%l2
							add	%fp,%l2,%l2
							set	0,%l3
							st	%l3,[%l2]
							set	._Labelcmp208,%l4
							jmp	%l4
							nop
							._Labelcmp208:
						set	-64,%l0
						add	%fp,%l0,%l0
						ld	[%l0],%l1
						set	0,%l2
						cmp	%l1,%l2
						be	._Labelcmp209
						nop
							set	-68,%l0
							add	%fp,%l0,%l0
							set	1,%l1
							st	%l1,[%l0]
							set	-68,%l0
							add	%fp,%l0,%l0
							ld	[%l0],%i0
								ret
								restore
							set	._Labelcmp210,%l4
							jmp	%l4
							nop
							._Labelcmp209:
							._Labelcmp210:
								set	-72,%l0
								add	%fp,%l0,%l0
								set	0,%l1
								st	%l1,[%l0]
								set	-72,%l0
								add	%fp,%l0,%l0
								ld	[%l0],%i0
									ret
									restore
									ret
									restore
								validForCell.SIZE = -(92 + 72) & -8
								
									.section	".text"
									.align	4
									.global	findCellValue
								findCellValue:
									set	findCellValue.SIZE,%g1
									save	%sp,%g1,%sp
									
									set	68,%l0
									add	%fp,%l0,%l0
									st	%i0,[%l0]
									set	72,%l0
									add	%fp,%l0,%l0
									st	%i1,[%l0]
								
									set	-4,%l0
									add	%fp,%l0,%l0
									set	1,%l1
									st	%l1,[%l0]
									set	-4,%l0
									add	%fp,%l0,%l0
									ld	[%l0],%l1
									set	-8,%l2
									add	%fp,%l2,%l2
									st	%l1,[%l2]
								
									set	-12,%l0
									add	%fp,%l0,%l0
									set	0,%l1
									st	%l1,[%l0]
									set	-12,%l0
									add	%fp,%l0,%l0
									ld	[%l0],%l1
									set	-16,%l2
									add	%fp,%l2,%l2
									st	%l1,[%l2]
								
								
								._Labelcmp211:
									set	-8,%l0
									add	%fp,%l0,%l0
									ld	[%l0],%l1
									set	last_valid_num,%l2
									add	%g0,%l2,%l2
									ld	[%l2],%l3
									cmp	%l1,%l3
									ble	._Labelcmp213
									nop
									set	-24,%l2
									add	%fp,%l2,%l2
									set	0,%l3
									st	%l3,[%l2]
									set	._Labelcmp214,%l4
									jmp	%l4
									nop
									._Labelcmp213:
									set	-24,%l2
									add	%fp,%l2,%l2
									set	1,%l3
									st	%l3,[%l2]
									set	._Labelcmp214,%l4
									jmp	%l4
									nop
									._Labelcmp214:
								set	-24,%l0
								add	%fp,%l0,%l0
								ld	[%l0],%l1
								set	0,%l2
								cmp	%l1,%l2
								be	._Labelcmp212
								nop
								set	68,%l0
								add	%fp,%l0,%l0
								ld	[%l0],%o0
								set	72,%l0
								add	%fp,%l0,%l0
								ld	[%l0],%o1
								set	-8,%l0
								add	%fp,%l0,%l0
								ld	[%l0],%o2
								call	validForCell
								nop
								set	-28,%l0
								add	%fp,%l0,%l0
								st	%o0,[%l0]
								set	-28,%l0
								add	%fp,%l0,%l0
								ld	[%l0],%l1
								set	0,%l2
								cmp	%l1,%l2
								be	._Labelcmp215
								nop
									set	68,%l0
									add	%fp,%l0,%l0
									ld	[%l0],%o0
									set	width,%l1
									add	%g0,%l1,%l1
									ld	[%l1],%o1
									call	.mul
									nop
									set	-32,%l2
									add	%fp,%l2,%l2
									st	%o0,[%l2]
									set	-32,%l0
									add	%fp,%l0,%l0
									ld	[%l0],%l1
									set	72,%l2
									add	%fp,%l2,%l2
									ld	[%l2],%l3
									add	%l1,%l3,%l4
									set	-36,%l5
									add	%fp,%l5,%l5
									st	%l4,[%l5]
								set	board,%l0
								add	%g0,%l0,%l0
								set	-36,%l2
								add	%fp,%l2,%l2
								ld	[%l2],%l1
								set	81,%l6
								cmp	%l6,%l1
								bg	._Labelcmp217
								nop
								set	._bounds,%o0
								ld	[%l2],%o1
								set	81,%o2
								call	printf
								nop
								set	1,%o0
								call	exit
								nop
								._Labelcmp217:
								set	0,%l3
								cmp	%l1,%l3
								bge	._Labelcmp218
								nop
								set	._bounds,%o0
								ld	[%l2],%o1
								set	81,%o2
								call	printf
								nop
								set	1,%o0
								call	exit
								nop
								._Labelcmp218:
								sll	%l1,2,%l3
								add	%l0,%l3,%l4
								set	-40,%l5
								add	%fp,%l5,%l5
								st	%l4,[%l5]
								set	-8,%l0
								add	%fp,%l0,%l0
								ld	[%l0],%l1
								set	-40,%l2
								add	%fp,%l2,%l2
								ld	[%l2],%l2
								st	%l1,[%l2]
								call	solvedBoard
								nop
								set	-44,%l0
								add	%fp,%l0,%l0
								st	%o0,[%l0]
								set	-44,%l0
								add	%fp,%l0,%l0
								ld	[%l0],%l1
								set	-20,%l2
								add	%fp,%l2,%l2
								st	%l1,[%l2]
								set	-48,%l0
								add	%fp,%l0,%l0
								set	1,%l1
								st	%l1,[%l0]
									set	-20,%l0
									add	%fp,%l0,%l0
									ld	[%l0],%l1
									set	-48,%l2
									add	%fp,%l2,%l2
									ld	[%l2],%l3
									cmp	%l1,%l3
									be	._Labelcmp219
									nop
									set	-52,%l2
									add	%fp,%l2,%l2
									set	0,%l3
									st	%l3,[%l2]
									set	._Labelcmp220,%l4
									jmp	%l4
									nop
									._Labelcmp219:
									set	-52,%l2
									add	%fp,%l2,%l2
									set	1,%l3
									st	%l3,[%l2]
									set	._Labelcmp220,%l4
									jmp	%l4
									nop
									._Labelcmp220:
								set	-52,%l0
								add	%fp,%l0,%l0
								ld	[%l0],%l1
								set	0,%l2
								cmp	%l1,%l2
								be	._Labelcmp221
								nop
									set	-56,%l0
									add	%fp,%l0,%l0
									set	1,%l1
									st	%l1,[%l0]
									set	-56,%l0
									add	%fp,%l0,%l0
									ld	[%l0],%i0
										ret
										restore
									set	._Labelcmp222,%l4
									jmp	%l4
									nop
									._Labelcmp221:
									._Labelcmp222:
									set	._Labelcmp216,%l4
									jmp	%l4
									nop
									._Labelcmp215:
									._Labelcmp216:
										set	-8,%l0
										add	%fp,%l0,%l0
										ld	[%l0],%l1
										set	-60,%l2
										add	%fp,%l2,%l2
										st	%l1,[%l2]
										set	1,%l3
										add	%l1,%l3,%l4
										set	-8,%l0
										add	%fp,%l0,%l0
										st	%l4,[%l0]
									set	._Labelcmp211,%l0
									jmp	%l0
									nop
									._Labelcmp212:
										set	68,%l0
										add	%fp,%l0,%l0
										ld	[%l0],%o0
										set	width,%l1
										add	%g0,%l1,%l1
										ld	[%l1],%o1
										call	.mul
										nop
										set	-64,%l2
										add	%fp,%l2,%l2
										st	%o0,[%l2]
										set	-64,%l0
										add	%fp,%l0,%l0
										ld	[%l0],%l1
										set	72,%l2
										add	%fp,%l2,%l2
										ld	[%l2],%l3
										add	%l1,%l3,%l4
										set	-68,%l5
										add	%fp,%l5,%l5
										st	%l4,[%l5]
									set	board,%l0
									add	%g0,%l0,%l0
									set	-68,%l2
									add	%fp,%l2,%l2
									ld	[%l2],%l1
									set	81,%l6
									cmp	%l6,%l1
									bg	._Labelcmp223
									nop
									set	._bounds,%o0
									ld	[%l2],%o1
									set	81,%o2
									call	printf
									nop
									set	1,%o0
									call	exit
									nop
									._Labelcmp223:
									set	0,%l3
									cmp	%l1,%l3
									bge	._Labelcmp224
									nop
									set	._bounds,%o0
									ld	[%l2],%o1
									set	81,%o2
									call	printf
									nop
									set	1,%o0
									call	exit
									nop
									._Labelcmp224:
									sll	%l1,2,%l3
									add	%l0,%l3,%l4
									set	-72,%l5
									add	%fp,%l5,%l5
									st	%l4,[%l5]
									set	-76,%l0
									add	%fp,%l0,%l0
									set	0,%l1
									st	%l1,[%l0]
									set	-76,%l0
									add	%fp,%l0,%l0
									ld	[%l0],%l1
									set	-72,%l2
									add	%fp,%l2,%l2
									ld	[%l2],%l2
									st	%l1,[%l2]
										set	-16,%l0
										add	%fp,%l0,%l0
										ld	[%l0],%i0
											ret
											restore
											ret
											restore
										findCellValue.SIZE = -(92 + 76) & -8
										
											.section	".text"
											.align	4
											.global	main
										main:
											set	main.SIZE,%g1
											save	%sp,%g1,%sp
											
											set	._init,%l0
											ld	[%l0],%l1
											cmp	%l1,%g0
											bne	._init_done
											mov	1,%l1
											st	%l1,[%l0]
										._init_done:
										
										call	initializeBoard
										nop
										call	solvedBoard
										nop
										set	-4,%l0
										add	%fp,%l0,%l0
										st	%o0,[%l0]
										call	printBoard
										nop
											ret
											restore
										main.SIZE = -(92 + 4) & -8
										
											.section	".data"
											.align	4
										._Label8:	.asciz	", "
										
