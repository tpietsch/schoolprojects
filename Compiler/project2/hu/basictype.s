.global	a, b, c, a1, b1, c1, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	3
______3: .word	4
______4: .word	1
_____5: .asciz "\n"
.align	4
_____6: .asciz "\n"
.align	4
_____9: .asciz "\n"
.align	4
_____10: .asciz "3"
.align	4
_____11: .asciz "\n"
.align	4
_____12: .single 0r4.0
_____13: .asciz "\n"
.align	4
_____14: .asciz "true"
.align	4
_____15: .asciz "\n"
.align	4
______20: .word	0
_____25: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	4
!CodeGen::doVarDecl::b
b:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
!CodeGen::doVarDecl::a1
a1:	.skip	4
!CodeGen::doVarDecl::b1
b1:	.skip	4
!CodeGen::doVarDecl::c1
c1:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	3,%l1
set	a1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	b1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	b1,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
set	1,%l1
set	c1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	b,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____7 
 nop
set	_boolF,%o1
ba  _____8 
 nop
_____7:
set	_boolT,%o1
_____8:
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	_____12,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 0) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____21 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____22 
 nop
_____21:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____22:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____23 
 nop
ba  _____24 
 nop
_____23:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
_____24:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

