.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______7: .word	1
_____12: .asciz " type cast of bool to int works. a1 = "
.align	4
_____13: .asciz "\n"
.align	4
_____14: .asciz "type cast of bool to bool int not work. a1 = "
.align	4
_____15: .asciz "\n"
.align	4
______16: .word	0
______21: .word	0
_____26: .asciz "type cast of bool to int does work. b1 = "
.align	4
_____27: .asciz "\n"
.align	4
_____28: .asciz "type cast of bool to int does not work. b1 = "
.align	4
_____29: .asciz "\n"
.align	4
______30: .word	1
______32: .word	0
_____34: .single	0r1.0
______38: .single	0r1.0
_____43: .asciz "type cast of bool to float does work. c1 = "
.align	4
_____44: .asciz "\n"
.align	4
_____45: .asciz "type cast of bool to float does not work. c1 = "
.align	4
_____46: .asciz "\n"
.align	4
_____47: .single	0r0.0
______51: .single	0r0.0
_____56: .asciz "type cast of bool to float does work. d1 = "
.align	4
_____57: .asciz "\n"
.align	4
_____58: .asciz "type cast of bool to float does not work. d1 = "
.align	4
_____59: .asciz "\n"
.align	4
______60: .word	3
______68: .word	1
_____71: .asciz "type cast of int to bool does work. f1 = "
.align	4
_____74: .asciz "\n"
.align	4
_____75: .asciz "type cast of int to bool does not work. f1 = "
.align	4
_____78: .asciz "\n"
.align	4
______79: .word	0
______87: .word	1
_____90: .asciz "type cast of int to bool does not work. f1 = "
.align	4
_____93: .asciz "\n"
.align	4
_____94: .asciz "type cast of int to bool does work. f1 = "
.align	4
_____97: .asciz "\n"
.align	4
_____98: .single	0r4.5
______99: .single	0r4.5
______107: .word	1
_____110: .asciz " type cast of float to bool works. r1 = "
.align	4
_____113: .asciz "\n"
.align	4
_____114: .asciz "type cast of float to bool does not work. r1 = "
.align	4
_____117: .asciz "\n"
.align	4
______118: .word	0
______127: .word	1
_____130: .asciz " type cast of float to bool does not works. r1 = "
.align	4
_____133: .asciz "\n"
.align	4
_____134: .asciz "type cast of float to bool does work. r1 = "
.align	4
_____137: .asciz "\n"
.align	4
_____138: .single	0r5.7
______139: .single	0r5.7
______144: .word	5
_____149: .asciz " type cast of float to int does works. fr1 = "
.align	4
_____150: .asciz "\n"
.align	4
_____151: .asciz " type cast of float to int does not works. fr1 = "
.align	4
_____152: .asciz "\n"
.align	4
______157: .word	0
_____162: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
be  _____8 
 nop
set	-16,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____9 
 nop
_____8:
set	1,%l3
set	-16,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____9:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____10 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
ba  _____11 
 nop
_____10:
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
_____11:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____22 
 nop
set	-40,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____23 
 nop
_____22:
set	1,%l3
set	-40,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____23:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____24 
 nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
ba  _____25 
 nop
_____24:
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
_____25:
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-56,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-68,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______38,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____39 
 nop
set	-76,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____40 
 nop
_____39:
set	1,%l3
set	-76,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____40:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____41 
 nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
ba  _____42 
 nop
_____41:
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
_____42:
set	0, %l0
set	-88,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______51,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____52 
 nop
set	-88,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____53 
 nop
_____52:
set	1,%l3
set	-88,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____53:
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____54 
 nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
ba  _____55 
 nop
_____54:
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
_____55:
set	3,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-108,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bne  _____64 
 nop
set	-108,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____65 
 nop
_____64:
set	1,%l3
set	-108,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____65:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____66 
 nop
set	1,%l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____67 
 nop
_____66:
_____67:
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____69 
 nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____72 
 nop
set	_boolF,%o1
ba  _____73 
 nop
_____72:
set	_boolT,%o1
_____73:
call printf
nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
ba  _____70 
 nop
_____69:
set	_strFmt,%o0
set	_____75,%o1
call printf
nop
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____76 
 nop
set	_boolF,%o1
ba  _____77 
 nop
_____76:
set	_boolT,%o1
_____77:
call printf
nop
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
_____70:
set	0,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-120,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bne  _____83 
 nop
set	-120,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____84 
 nop
_____83:
set	1,%l3
set	-120,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____84:
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____85 
 nop
set	1,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____86 
 nop
_____85:
_____86:
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____88 
 nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____91 
 nop
set	_boolF,%o1
ba  _____92 
 nop
_____91:
set	_boolT,%o1
_____92:
call printf
nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
ba  _____89 
 nop
_____88:
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____95 
 nop
set	_boolF,%o1
ba  _____96 
 nop
_____95:
set	_boolT,%o1
_____96:
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
_____89:
set	______99,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-124,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-128,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fstoi %f2, %f2
set	-128,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	0, %l0
set	-132,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bne  _____103 
 nop
set	-132,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____104 
 nop
_____103:
set	1,%l3
set	-132,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____104:
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____105 
 nop
set	1,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____106 
 nop
_____105:
_____106:
set	0, %l0
set	-136,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____108 
 nop
set	_strFmt,%o0
set	_____110,%o1
call printf
nop
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____111 
 nop
set	_boolF,%o1
ba  _____112 
 nop
_____111:
set	_boolT,%o1
_____112:
call printf
nop
set	_strFmt,%o0
set	_____113,%o1
call printf
nop
ba  _____109 
 nop
_____108:
set	_strFmt,%o0
set	_____114,%o1
call printf
nop
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____115 
 nop
set	_boolF,%o1
ba  _____116 
 nop
_____115:
set	_boolT,%o1
_____116:
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
_____109:
set	0,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-140,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-124,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fstoi %f2, %f2
set	-144,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bne  _____123 
 nop
set	-148,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____124 
 nop
_____123:
set	1,%l3
set	-148,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____124:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____125 
 nop
set	1,%l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____126 
 nop
_____125:
_____126:
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____128 
 nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____131 
 nop
set	_boolF,%o1
ba  _____132 
 nop
_____131:
set	_boolT,%o1
_____132:
call printf
nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
ba  _____129 
 nop
_____128:
set	_strFmt,%o0
set	_____134,%o1
call printf
nop
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____135 
 nop
set	_boolF,%o1
ba  _____136 
 nop
_____135:
set	_boolT,%o1
_____136:
call printf
nop
set	_strFmt,%o0
set	_____137,%o1
call printf
nop
_____129:
set	______139,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-152,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fstoi %f2, %f2
set	-156,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-164,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	5,%l2
cmp	%l1, %l2
be  _____145 
 nop
set	-164,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____146 
 nop
_____145:
set	1,%l3
set	-164,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____146:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____147 
 nop
set	_strFmt,%o0
set	_____149,%o1
call printf
nop
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____150,%o1
call printf
nop
ba  _____148 
 nop
_____147:
set	_strFmt,%o0
set	_____151,%o1
call printf
nop
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____152,%o1
call printf
nop
_____148:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 172) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____158 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____159 
 nop
_____158:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____159:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____160 
 nop
ba  _____161 
 nop
_____160:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____162,%o1
call printf
nop
_____161:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

