.global	x, __________.MEMLEAK,main
.section	".data"
.align	4
______4: .word	12
______5: .word	567
______7: .word	1
______13: .word	45
______15: .word	1
______19: .word	34
_____21: .asciz "Input numeric value for z, current value for z is "
.align	4
_____22: .asciz " : "
.align	4
_____23: .asciz "NEW value of z: "
.align	4
_____24: .asciz "\n"
.align	4
_____25: .asciz "Input integer value for x, current value for x is "
.align	4
_____26: .asciz " : "
.align	4
_____27: .asciz "NEW value of x: "
.align	4
_____28: .asciz "\n"
.align	4
_____29: .asciz "Input numeric value for a, current value for a is "
.align	4
_____30: .asciz " : "
.align	4
_____31: .asciz "NEW value of a: "
.align	4
_____32: .asciz "\n"
.align	4
_____33: .asciz "Input integer value for b, current value for b is "
.align	4
_____34: .asciz " : "
.align	4
_____35: .asciz "NEW value of b: "
.align	4
_____36: .asciz "\n"
.align	4
_____37: .asciz "Input numeric value for a, current value for a is "
.align	4
_____38: .asciz " : "
.align	4
_____39: .asciz "NEW value of a: "
.align	4
_____40: .asciz "\n"
.align	4
______45: .word	0
_____50: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
x:	.skip	4
!CodeGen::doVarDecl::z
_0z12staticFlag:	.skip	4
_0z12:	.skip	4
!CodeGen::doVarDecl::b
main__0b211staticFlag:	.skip	4
main__0b211:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	_0z12staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____3 
 nop
set	12,%l1
set	_0z12,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	_0z12staticFlag, %l1
st	%l0, [%l1]
_____3:
set	_0z12,%l1
add 	%g0, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	_0z12,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
_init_done:
set	567,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-4,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-8,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-16,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0b211staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____12 
 nop
set	45,%l1
set	main__0b211,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	main__0b211staticFlag, %l1
st	%l0, [%l1]
_____12:
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0b211,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	main__0b211,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	main__0b211,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	34,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-40,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	_0z12,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_0z12,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
call inputFloat
nop
set	_0z12,%l0
add 	%g0, %l0, %l0
st	%f0, [%l0]
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_0z12,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
call inputInt
nop
set	x,%l0
add 	%g0, %l0, %l0
st	%o0, [%l0]
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
call inputFloat
nop
set	-4,%l0
add 	%fp, %l0, %l0
st	%f0, [%l0]
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	main__0b211,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
call inputInt
nop
set	main__0b211,%l0
add 	%g0, %l0, %l0
st	%o0, [%l0]
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	main__0b211,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
call inputFloat
nop
set	-4,%l0
add 	%fp, %l0, %l0
st	%f0, [%l0]
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 40) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____46 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____47 
 nop
_____46:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____47:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____48 
 nop
ba  _____49 
 nop
_____48:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
_____49:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

