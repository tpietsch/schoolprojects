.global	STR.foo, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Hello from STR!"
.align	4
_____3: .asciz "\n"
.align	4
______5: .word	123
_____7: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____11: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____12: .asciz "str2.ptr->i = 123 ="
.align	4
_____16: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____18: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____20: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____21: .asciz "\n"
.align	4
_____22: .asciz "sizeof(*(str2.sptr)) = 8 = "
.align	4
_____24: .asciz "8"
.align	4
_____25: .asciz "\n"
.align	4
_____29: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____31: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____37: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______42: .word	0
_____47: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-8, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-24,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	123,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	1, %o0
call exit
nop
_____6:
st	%l1, [%l0]
set	-16, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-32,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8, %l1
add	%fp, %l1, %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____10 
 nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	1, %o0
call exit
nop
_____10:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	-16, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-44,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-44, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____15 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	1, %o0
call exit
nop
_____15:
ld	[%l1], %l1
cmp	%l1, %g0
bne  _____17 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
_____17:
ld	[%l1], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-52,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____19 
 nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	1, %o0
call exit
nop
_____19:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	-16, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-60,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	-16, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-68,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-68, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____28 
 nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	1, %o0
call exit
nop
_____28:
ld	[%l1], %l1
cmp	%l1, %g0
bne  _____30 
 nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	1, %o0
call exit
nop
_____30:
ld	[%l1], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	STR.foo, %l0
add	%g0, %l0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-84, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-76, %l1
add	%fp, %l1, %l1
ld	[%l1], %l1
st	%l1, [%l0]
set	-84, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____34 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____35 
 nop
_____34:
add	%sp, -0, %sp
_____35:
_____33:
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____36 
 nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	1, %o0
call exit
nop
_____36:
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 84) & -8

STR.foo: 
set SAVE.STR.foo, %g1
save %sp, %g1, %sp
set	-8,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
ret
restore

SAVE.STR.foo = -(92 + 8) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____43 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____44 
 nop
_____43:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____44:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____45 
 nop
ba  _____46 
 nop
_____45:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
_____46:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

