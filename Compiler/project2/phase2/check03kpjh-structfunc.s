.global	PAK.foo1, PAK.foo2, __________.MEMLEAK,main
.section	".data"
.align	4
_____4: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______5: .word	5
_____7: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____10: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____12: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____13: .asciz "\n"
.align	4
_____16: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____22: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______24: .word	1
_____31: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____34: .asciz "\n"
.align	4
_____37: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____38: .asciz "\n"
.align	4
______43: .word	0
_____48: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	PAK.foo2, %l0
add	%g0, %l0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-8, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____28 
 nop
mov	%l1, %o0
set	0,%o1
add	%sp, -0, %sp
ba  _____29 
 nop
_____28:
set	0,%o0
add	%sp, -0, %sp
_____29:
_____27:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____30 
 nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	1, %o0
call exit
nop
_____30:
call %l1
nop
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____32 
 nop
set	_boolF,%o1
ba  _____33 
 nop
_____32:
set	_boolT,%o1
_____33:
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-36,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____36 
 nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	1, %o0
call exit
nop
_____36:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

PAK.foo1: 
set SAVE.PAK.foo1, %g1
save %sp, %g1, %sp
set	-8,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____3 
 nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	1, %o0
call exit
nop
_____3:
ld	[%l1], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-16,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	5,%l1
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	1, %o0
call exit
nop
_____6:
st	%l1, [%l0]
set	-8, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____9 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1, %o0
call exit
nop
_____9:
ld	[%l1], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-24,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____11 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1, %o0
call exit
nop
_____11:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
ret
restore

SAVE.PAK.foo1 = -(92 + 24) & -8

PAK.foo2: 
set SAVE.PAK.foo2, %g1
save %sp, %g1, %sp
set	-8,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-8, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____15 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	1, %o0
call exit
nop
_____15:
ld	[%l1], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	PAK.foo1, %l0
add	%g0, %l0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-20, %l1
add	%fp, %l1, %l1
ld	[%l1], %l1
st	%l1, [%l0]
set	-28, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____19 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____20 
 nop
_____19:
add	%sp, -0, %sp
_____20:
_____18:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____21 
 nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	1, %o0
call exit
nop
_____21:
call %l1
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%i0
ret
restore

ret
restore

SAVE.PAK.foo2 = -(92 + 32) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____44 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____45 
 nop
_____44:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____45:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____46 
 nop
ba  _____47 
 nop
_____46:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
_____47:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

