.global	bT, bF, _0bTopbF19, bA, _0bTopbT120, bB, _0bFopbF131, bC, _0bFopbT142, bD, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	0
______13: .word	0
______14: .word	1
______24: .word	0
______25: .word	1
______35: .word	0
______36: .word	1
______46: .word	0
______47: .word	1
_____48: .asciz "testing global bools"
.align	4
_____49: .asciz "\n"
.align	4
_____52: .asciz "== true01"
.align	4
_____53: .asciz "\n"
.align	4
_____56: .asciz "== false02"
.align	4
_____57: .asciz "\n"
.align	4
_____60: .asciz "== false03"
.align	4
_____61: .asciz "\n"
.align	4
_____64: .asciz "== true04"
.align	4
_____65: .asciz "\n"
.align	4
_____68: .asciz "== false05"
.align	4
_____69: .asciz "\n"
.align	4
_____72: .asciz "== false06"
.align	4
_____73: .asciz "\n"
.align	4
______82: .word	0
______83: .word	1
_____86: .asciz " == true07"
.align	4
_____87: .asciz "\n"
.align	4
______96: .word	0
______97: .word	1
_____100: .asciz " == false08"
.align	4
_____101: .asciz "\n"
.align	4
______102: .word	1
______103: .word	0
______112: .word	0
______113: .word	1
_____116: .asciz "ia && ib is false"
.align	4
_____117: .asciz "\n"
.align	4
______126: .word	0
______127: .word	1
_____130: .asciz "ib && ia is false"
.align	4
_____131: .asciz "\n"
.align	4
______140: .word	0
______141: .word	1
_____144: .asciz "ia && ia is true09"
.align	4
_____145: .asciz "\n"
.align	4
______154: .word	0
______155: .word	1
_____158: .asciz "ia && true is true10"
.align	4
_____159: .asciz "\n"
.align	4
______168: .word	0
______169: .word	1
_____172: .asciz "false && ia is false"
.align	4
_____173: .asciz "\n"
.align	4
_____184: .asciz "true &&true is true11"
.align	4
_____185: .asciz "\n"
.align	4
_____196: .asciz "false && false is false"
.align	4
_____197: .asciz "\n"
.align	4
_____208: .asciz "true && false is false"
.align	4
_____209: .asciz "\n"
.align	4
_____220: .asciz "false && true is false"
.align	4
_____221: .asciz "\n"
.align	4
______226: .word	0
_____231: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::bT
bT:	.skip	4
!CodeGen::doVarDecl::bF
bF:	.skip	4
!CodeGen::doVarDecl::bTopbF
_0bTopbF19:	.skip	4
!CodeGen::doVarDecl::bA
bA:	.skip	4
!CodeGen::doVarDecl::bTopbT
_0bTopbT120:	.skip	4
!CodeGen::doVarDecl::bB
bB:	.skip	4
!CodeGen::doVarDecl::bFopbF
_0bFopbF131:	.skip	4
!CodeGen::doVarDecl::bC
bC:	.skip	4
!CodeGen::doVarDecl::bFopbT
_0bFopbT142:	.skip	4
!CodeGen::doVarDecl::bD
bD:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	bT,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	bF,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____4 
 nop
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____6 
 nop
ba  _____8 
 nop
ba  _____7 
 nop
_____6:
ba  _____7 
 nop
_____4:
_____7:
_____5:
set	0,%l1
set	_0bTopbF19,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
ba  _____12 
 nop
_____8:
set	1,%l1
set	_0bTopbF19,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_____12:
set	_0bTopbF19,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bA,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____15 
 nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____17 
 nop
ba  _____19 
 nop
ba  _____18 
 nop
_____17:
ba  _____18 
 nop
_____15:
_____18:
_____16:
set	0,%l1
set	_0bTopbT120,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
ba  _____23 
 nop
_____19:
set	1,%l1
set	_0bTopbT120,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_____23:
set	_0bTopbT120,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bB,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____28 
 nop
ba  _____30 
 nop
ba  _____29 
 nop
_____28:
ba  _____29 
 nop
_____26:
_____29:
_____27:
set	0,%l1
set	_0bFopbF131,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
ba  _____34 
 nop
_____30:
set	1,%l1
set	_0bFopbF131,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_____34:
set	_0bFopbF131,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bC,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____37 
 nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____39 
 nop
ba  _____41 
 nop
ba  _____40 
 nop
_____39:
ba  _____40 
 nop
_____37:
_____40:
_____38:
set	0,%l1
set	_0bFopbT142,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
ba  _____45 
 nop
_____41:
set	1,%l1
set	_0bFopbT142,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_____45:
set	_0bFopbT142,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bD,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____50 
 nop
set	_boolF,%o1
ba  _____51 
 nop
_____50:
set	_boolT,%o1
_____51:
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____54 
 nop
set	_boolF,%o1
ba  _____55 
 nop
_____54:
set	_boolT,%o1
_____55:
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
set	bA,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____58 
 nop
set	_boolF,%o1
ba  _____59 
 nop
_____58:
set	_boolT,%o1
_____59:
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	bB,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____62 
 nop
set	_boolF,%o1
ba  _____63 
 nop
_____62:
set	_boolT,%o1
_____63:
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	bC,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____66 
 nop
set	_boolF,%o1
ba  _____67 
 nop
_____66:
set	_boolT,%o1
_____67:
call printf
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	bD,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____70 
 nop
set	_boolF,%o1
ba  _____71 
 nop
_____70:
set	_boolT,%o1
_____71:
call printf
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____74 
 nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____76 
 nop
ba  _____78 
 nop
ba  _____77 
 nop
_____76:
ba  _____77 
 nop
_____74:
_____77:
_____75:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____81 
 nop
_____78:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____81:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____84 
 nop
set	_boolF,%o1
ba  _____85 
 nop
_____84:
set	_boolT,%o1
_____85:
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____88 
 nop
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____90 
 nop
ba  _____92 
 nop
ba  _____91 
 nop
_____90:
ba  _____91 
 nop
_____88:
_____91:
_____89:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____95 
 nop
_____92:
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____95:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____98 
 nop
set	_boolF,%o1
ba  _____99 
 nop
_____98:
set	_boolT,%o1
_____99:
call printf
nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____104 
 nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____106 
 nop
ba  _____108 
 nop
ba  _____107 
 nop
_____106:
ba  _____107 
 nop
_____104:
_____107:
_____105:
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____111 
 nop
_____108:
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____111:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____114 
 nop
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
ba  _____115 
 nop
_____114:
_____115:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____118 
 nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____120 
 nop
ba  _____122 
 nop
ba  _____121 
 nop
_____120:
ba  _____121 
 nop
_____118:
_____121:
_____119:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____125 
 nop
_____122:
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____125:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____128 
 nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
ba  _____129 
 nop
_____128:
_____129:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____132 
 nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____134 
 nop
ba  _____136 
 nop
ba  _____135 
 nop
_____134:
ba  _____135 
 nop
_____132:
_____135:
_____133:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____139 
 nop
_____136:
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____139:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____142 
 nop
set	_strFmt,%o0
set	_____144,%o1
call printf
nop
set	_strFmt,%o0
set	_____145,%o1
call printf
nop
ba  _____143 
 nop
_____142:
_____143:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____146 
 nop
set	1,%l1
cmp	%l1, %g0
be  _____148 
 nop
ba  _____150 
 nop
ba  _____149 
 nop
_____148:
ba  _____149 
 nop
_____146:
_____149:
_____147:
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____153 
 nop
_____150:
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____153:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____156 
 nop
set	_strFmt,%o0
set	_____158,%o1
call printf
nop
set	_strFmt,%o0
set	_____159,%o1
call printf
nop
ba  _____157 
 nop
_____156:
_____157:
set	0,%l1
cmp	%l1, %g0
be  _____160 
 nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____162 
 nop
ba  _____164 
 nop
ba  _____163 
 nop
_____162:
ba  _____163 
 nop
_____160:
_____163:
_____161:
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____167 
 nop
_____164:
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____167:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____170 
 nop
set	_strFmt,%o0
set	_____172,%o1
call printf
nop
set	_strFmt,%o0
set	_____173,%o1
call printf
nop
ba  _____171 
 nop
_____170:
_____171:
set	1,%l1
cmp	%l1, %g0
be  _____174 
 nop
set	1,%l1
cmp	%l1, %g0
be  _____176 
 nop
ba  _____178 
 nop
ba  _____177 
 nop
_____176:
ba  _____177 
 nop
_____174:
_____177:
_____175:
ba  _____181 
 nop
_____178:
_____181:
set	1,%l1
cmp	%l1, %g0
be  _____182 
 nop
set	_strFmt,%o0
set	_____184,%o1
call printf
nop
set	_strFmt,%o0
set	_____185,%o1
call printf
nop
ba  _____183 
 nop
_____182:
_____183:
set	0,%l1
cmp	%l1, %g0
be  _____186 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____188 
 nop
ba  _____190 
 nop
ba  _____189 
 nop
_____188:
ba  _____189 
 nop
_____186:
_____189:
_____187:
ba  _____193 
 nop
_____190:
_____193:
set	0,%l1
cmp	%l1, %g0
be  _____194 
 nop
set	_strFmt,%o0
set	_____196,%o1
call printf
nop
set	_strFmt,%o0
set	_____197,%o1
call printf
nop
ba  _____195 
 nop
_____194:
_____195:
set	1,%l1
cmp	%l1, %g0
be  _____198 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____200 
 nop
ba  _____202 
 nop
ba  _____201 
 nop
_____200:
ba  _____201 
 nop
_____198:
_____201:
_____199:
ba  _____205 
 nop
_____202:
_____205:
set	0,%l1
cmp	%l1, %g0
be  _____206 
 nop
set	_strFmt,%o0
set	_____208,%o1
call printf
nop
set	_strFmt,%o0
set	_____209,%o1
call printf
nop
ba  _____207 
 nop
_____206:
_____207:
set	0,%l1
cmp	%l1, %g0
be  _____210 
 nop
set	1,%l1
cmp	%l1, %g0
be  _____212 
 nop
ba  _____214 
 nop
ba  _____213 
 nop
_____212:
ba  _____213 
 nop
_____210:
_____213:
_____211:
ba  _____217 
 nop
_____214:
_____217:
set	0,%l1
cmp	%l1, %g0
be  _____218 
 nop
set	_strFmt,%o0
set	_____220,%o1
call printf
nop
set	_strFmt,%o0
set	_____221,%o1
call printf
nop
ba  _____219 
 nop
_____218:
_____219:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____227 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____228 
 nop
_____227:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____228:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____229 
 nop
ba  _____230 
 nop
_____229:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____231,%o1
call printf
nop
_____230:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

