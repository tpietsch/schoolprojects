.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
_____5: .asciz "pass1"
.align	4
_____6: .asciz "\n"
.align	4
______8: .word	1
_____12: .asciz "fail1"
.align	4
_____13: .asciz "\n"
.align	4
_____16: .asciz "fail2"
.align	4
_____17: .asciz "\n"
.align	4
_____18: .asciz "pass2"
.align	4
_____19: .asciz "\n"
.align	4
______21: .word	1
_____27: .asciz "pass3"
.align	4
_____28: .asciz "\n"
.align	4
______30: .word	1
_____34: .asciz "fail3"
.align	4
_____35: .asciz "\n"
.align	4
_____38: .asciz "pass4"
.align	4
_____39: .asciz "\n"
.align	4
______41: .word	1
_____45: .asciz "fail4"
.align	4
_____46: .asciz "\n"
.align	4
_____49: .asciz "fail5"
.align	4
_____50: .asciz "\n"
.align	4
_____51: .asciz "pass5"
.align	4
_____52: .asciz "\n"
.align	4
______54: .word	1
_____58: .asciz "Number of passed test is: "
.align	4
_____59: .asciz "/5"
.align	4
_____60: .asciz "\n"
.align	4
______65: .word	0
_____70: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
cmp	%l1, %g0
be  _____3 
 nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-16,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____4 
 nop
_____3:
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
_____4:
set	0,%l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
ba  _____15 
 nop
_____14:
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____15:
set	1,%l1
cmp	%l1, %g0
be  _____25 
 nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	1,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-48,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____26 
 nop
_____25:
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
_____26:
set	6000,%l1
cmp	%l1, %g0
be  _____36 
 nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	1,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-64,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____37 
 nop
_____36:
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
_____37:
set	0,%l1
cmp	%l1, %g0
be  _____47 
 nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
ba  _____48 
 nop
_____47:
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	1,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-80,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____48:
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 84) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____66 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____67 
 nop
_____66:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____67:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____68 
 nop
ba  _____69 
 nop
_____68:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
_____69:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

