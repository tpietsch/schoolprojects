.global	gstr, foo, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Inside foo"
.align	4
_____3: .asciz "\n"
.align	4
_____4: .asciz "gstr.i = 0 = "
.align	4
_____7: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____8: .asciz "\n"
.align	4
_____9: .asciz "gstr.f = 0.00 = "
.align	4
_____12: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____13: .asciz "\n"
.align	4
_____14: .asciz "gstr.b = false = "
.align	4
_____17: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____20: .asciz "\n"
.align	4
_____21: .asciz "Inside main"
.align	4
_____22: .asciz "\n"
.align	4
_____23: .asciz "gstr.i = 0 = "
.align	4
_____26: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____27: .asciz "\n"
.align	4
_____28: .asciz "gstr.f = 0.00 = "
.align	4
_____31: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____32: .asciz "\n"
.align	4
_____33: .asciz "gstr.b = false = "
.align	4
_____36: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____39: .asciz "\n"
.align	4
______41: .word	2
_____43: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______45: .word	3
_____48: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______50: .word	1
_____52: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______59: .word	0
_____64: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::gstr
gstr:	.skip	12
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-12,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____25 
 nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	1, %o0
call exit
nop
_____25:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-24,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____30 
 nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	1, %o0
call exit
nop
_____30:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-36,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____35 
 nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1, %o0
call exit
nop
_____35:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____37 
 nop
set	_boolF,%o1
ba  _____38 
 nop
_____37:
set	_boolT,%o1
_____38:
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-48,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	2,%l1
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____42 
 nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	1, %o0
call exit
nop
_____42:
st	%l1, [%l0]
set	gstr, %l1
add	%g0, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-60,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	3,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-64,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____47 
 nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	1, %o0
call exit
nop
_____47:
st	%f1, [%l0]
set	gstr, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-76,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	1,%l1
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____51 
 nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	1, %o0
call exit
nop
_____51:
st	%l1, [%l0]
set	gstr, %l1
add	%g0, %l1, %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 88) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-12,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-24,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	1, %o0
call exit
nop
_____6:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-36,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____11 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1, %o0
call exit
nop
_____11:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	gstr, %l1
add	%g0, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-48,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____16 
 nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	1, %o0
call exit
nop
_____16:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____18 
 nop
set	_boolF,%o1
ba  _____19 
 nop
_____18:
set	_boolT,%o1
_____19:
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
ret
restore

SAVE.foo = -(92 + 48) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____60 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____61 
 nop
_____60:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____61:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____62 
 nop
ba  _____63 
 nop
_____62:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
_____63:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

