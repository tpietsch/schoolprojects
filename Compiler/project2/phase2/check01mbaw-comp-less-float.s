.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .single	0r10.0
______3: .single	0r10.0
_____4: .single	0r5.0
______5: .single	0r5.0
_____6: .asciz "testing greater than"
.align	4
_____7: .asciz "\n"
.align	4
_____15: .asciz " == false1"
.align	4
_____16: .asciz "\n"
.align	4
_____24: .asciz " == true2"
.align	4
_____25: .asciz "\n"
.align	4
_____33: .asciz "ia < ib is false"
.align	4
_____34: .asciz "\n"
.align	4
_____42: .asciz "ib < ia is true3"
.align	4
_____43: .asciz "\n"
.align	4
_____44: .asciz "testing greater than or equal to"
.align	4
_____45: .asciz "\n"
.align	4
_____53: .asciz " == false4"
.align	4
_____54: .asciz "\n"
.align	4
_____62: .asciz " == true5"
.align	4
_____63: .asciz "\n"
.align	4
_____71: .asciz "ia <= ib is false"
.align	4
_____72: .asciz "\n"
.align	4
_____80: .asciz "ib <= ia is true6"
.align	4
_____81: .asciz "\n"
.align	4
_____82: .asciz "testing greater than or equal to (both at same value)"
.align	4
_____83: .asciz "\n"
.align	4
______84: .word	10
_____92: .asciz " == true7"
.align	4
_____93: .asciz "\n"
.align	4
_____101: .asciz " == true8"
.align	4
_____102: .asciz "\n"
.align	4
_____110: .asciz "ia <= ic is true9"
.align	4
_____111: .asciz "\n"
.align	4
_____119: .asciz "ic <= ia is true10"
.align	4
_____120: .asciz "\n"
.align	4
_____128: .asciz " == true11"
.align	4
_____129: .asciz "\n"
.align	4
_____137: .asciz " == true12"
.align	4
_____138: .asciz "\n"
.align	4
______143: .word	0
_____148: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	______3,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____11 
 nop
set	-12,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____12 
 nop
_____11:
set	1,%l3
set	-12,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____12:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____13 
 nop
set	_boolF,%o1
ba  _____14 
 nop
_____13:
set	_boolT,%o1
_____14:
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____20 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____21 
 nop
_____20:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____21:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____22 
 nop
set	_boolF,%o1
ba  _____23 
 nop
_____22:
set	_boolT,%o1
_____23:
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-44,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____29 
 nop
set	-36,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____30 
 nop
_____29:
set	1,%l3
set	-36,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____30:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____31 
 nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
ba  _____32 
 nop
_____31:
_____32:
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-56,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____38 
 nop
set	-48,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____39 
 nop
_____38:
set	1,%l3
set	-48,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____39:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____40 
 nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
ba  _____41 
 nop
_____40:
_____41:
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-68,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____49 
 nop
set	-60,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____50 
 nop
_____49:
set	1,%l3
set	-60,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____50:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____51 
 nop
set	_boolF,%o1
ba  _____52 
 nop
_____51:
set	_boolT,%o1
_____52:
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____58 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____59 
 nop
_____58:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____59:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____60 
 nop
set	_boolF,%o1
ba  _____61 
 nop
_____60:
set	_boolT,%o1
_____61:
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____67 
 nop
set	-84,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____68 
 nop
_____67:
set	1,%l3
set	-84,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____68:
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____69 
 nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
ba  _____70 
 nop
_____69:
_____70:
set	0, %l0
set	-96,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-100,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-100,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-104,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____76 
 nop
set	-96,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____77 
 nop
_____76:
set	1,%l3
set	-96,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____77:
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____78 
 nop
set	_strFmt,%o0
set	_____80,%o1
call printf
nop
set	_strFmt,%o0
set	_____81,%o1
call printf
nop
ba  _____79 
 nop
_____78:
_____79:
set	_strFmt,%o0
set	_____82,%o1
call printf
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	10,%l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-116,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-116,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-120,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____88 
 nop
set	-112,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____89 
 nop
_____88:
set	1,%l3
set	-112,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____89:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____90 
 nop
set	_boolF,%o1
ba  _____91 
 nop
_____90:
set	_boolT,%o1
_____91:
call printf
nop
set	_strFmt,%o0
set	_____92,%o1
call printf
nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	0, %l0
set	-124,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-128,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-128,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-132,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____97 
 nop
set	-124,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____98 
 nop
_____97:
set	1,%l3
set	-124,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____98:
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____99 
 nop
set	_boolF,%o1
ba  _____100 
 nop
_____99:
set	_boolT,%o1
_____100:
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	_strFmt,%o0
set	_____102,%o1
call printf
nop
set	0, %l0
set	-136,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-140,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-140,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-144,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-144,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____106 
 nop
set	-136,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____107 
 nop
_____106:
set	1,%l3
set	-136,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____107:
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____108 
 nop
set	_strFmt,%o0
set	_____110,%o1
call printf
nop
set	_strFmt,%o0
set	_____111,%o1
call printf
nop
ba  _____109 
 nop
_____108:
_____109:
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-152,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-152,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____115 
 nop
set	-148,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____116 
 nop
_____115:
set	1,%l3
set	-148,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____116:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____117 
 nop
set	_strFmt,%o0
set	_____119,%o1
call printf
nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
ba  _____118 
 nop
_____117:
_____118:
set	0, %l0
set	-160,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-168,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-164,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-164,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____124 
 nop
set	-160,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____125 
 nop
_____124:
set	1,%l3
set	-160,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____125:
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____126 
 nop
set	_boolF,%o1
ba  _____127 
 nop
_____126:
set	_boolT,%o1
_____127:
call printf
nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
set	_strFmt,%o0
set	_____129,%o1
call printf
nop
set	0, %l0
set	-172,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-176,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-176,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-180,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-180,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____133 
 nop
set	-172,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____134 
 nop
_____133:
set	1,%l3
set	-172,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____134:
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____135 
 nop
set	_boolF,%o1
ba  _____136 
 nop
_____135:
set	_boolT,%o1
_____136:
call printf
nop
set	_strFmt,%o0
set	_____137,%o1
call printf
nop
set	_strFmt,%o0
set	_____138,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 180) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____144 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____145 
 nop
_____144:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____145:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____146 
 nop
ba  _____147 
 nop
_____146:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____148,%o1
call printf
nop
_____147:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

