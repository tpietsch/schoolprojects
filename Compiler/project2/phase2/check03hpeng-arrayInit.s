.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	4
______5: .word	0
______10: .word	8
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "0"
.align	4
_____18: .asciz " is outside legal range [0,2).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "0"
.align	4
_____30: .asciz " is outside legal range [0,2).\n"
.align	4
_____32: .single	0r12.02
______33: .single	0r12.02
_____35: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______37: .word	4
______39: .word	1
______44: .word	8
_____50: .asciz "Index value of "
.align	4
_____51: .asciz "1"
.align	4
_____52: .asciz " is outside legal range [0,2).\n"
.align	4
______57: .word	0
_____62: .asciz "Index value of "
.align	4
_____63: .asciz "1"
.align	4
_____64: .asciz " is outside legal range [0,2).\n"
.align	4
_____66: .single	0r33.03
______67: .single	0r33.03
_____69: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______71: .word	4
______73: .word	0
______78: .word	8
_____84: .asciz "Index value of "
.align	4
_____85: .asciz "0"
.align	4
_____86: .asciz " is outside legal range [0,2).\n"
.align	4
______91: .word	0
_____96: .asciz "Index value of "
.align	4
_____97: .asciz "0"
.align	4
_____98: .asciz " is outside legal range [0,2).\n"
.align	4
_____101: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____102: .asciz "\n"
.align	4
______104: .word	4
______106: .word	1
______111: .word	8
_____117: .asciz "Index value of "
.align	4
_____118: .asciz "1"
.align	4
_____119: .asciz " is outside legal range [0,2).\n"
.align	4
______124: .word	0
_____129: .asciz "Index value of "
.align	4
_____130: .asciz "1"
.align	4
_____131: .asciz " is outside legal range [0,2).\n"
.align	4
_____134: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____135: .asciz "\n"
.align	4
______140: .word	0
_____145: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	4,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-20,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-36,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-36,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______33,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____34 
 nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	1, %o0
call exit
nop
_____34:
st	%f1, [%l0]
set	4,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-60,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____46 
 nop
set	-64,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____47 
 nop
_____46:
set	1,%l3
set	-64,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____47:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____48 
 nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____49 
 nop
_____48:
_____49:
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____58 
 nop
set	-76,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____59 
 nop
_____58:
set	1,%l3
set	-76,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____59:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____60 
 nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____61 
 nop
_____60:
_____61:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______67,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____68 
 nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	1, %o0
call exit
nop
_____68:
st	%f1, [%l0]
set	8, %o2
set	-8, %o1
add	%fp, %o1, %o1
set	-96, %o0
add	%fp, %o0, %o0
call memmove
nop
set	4,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-108,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____80 
 nop
set	-112,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____81 
 nop
_____80:
set	1,%l3
set	-112,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____81:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____82 
 nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____83 
 nop
_____82:
_____83:
set	0, %l0
set	-124,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____92 
 nop
set	-124,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____93 
 nop
_____92:
set	1,%l3
set	-124,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____93:
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____94 
 nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____95 
 nop
_____94:
_____95:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____100 
 nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	1, %o0
call exit
nop
_____100:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____102,%o1
call printf
nop
set	4,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-148,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-152,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____113 
 nop
set	-152,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____114 
 nop
_____113:
set	1,%l3
set	-152,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____114:
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____115 
 nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	_strFmt,%o0
set	_____118,%o1
call printf
nop
set	_strFmt,%o0
set	_____119,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____116 
 nop
_____115:
_____116:
set	0, %l0
set	-164,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____125 
 nop
set	-164,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____126 
 nop
_____125:
set	1,%l3
set	-164,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____126:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____127 
 nop
set	_strFmt,%o0
set	_____129,%o1
call printf
nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____128 
 nop
_____127:
_____128:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____133 
 nop
set	_strFmt,%o0
set	_____134,%o1
call printf
nop
set	1, %o0
call exit
nop
_____133:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____135,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 176) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____141 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____142 
 nop
_____141:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____142:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____143 
 nop
ba  _____144 
 nop
_____143:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____145,%o1
call printf
nop
_____144:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

