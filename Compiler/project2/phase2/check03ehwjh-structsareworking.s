.global	FOO.fubar, FOO.foo, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "in fubar"
.align	4
_____3: .asciz "\n"
.align	4
_____4: .asciz "this.x = (5, "
.align	4
_____7: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____9: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____10: .asciz ")"
.align	4
_____11: .asciz "\n"
.align	4
_____12: .asciz "in foo"
.align	4
_____13: .asciz "\n"
.align	4
_____14: .asciz "this.x = (5, "
.align	4
_____17: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____19: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____20: .asciz ")"
.align	4
_____21: .asciz "\n"
.align	4
_____22: .asciz "calling this.fubar()"
.align	4
_____23: .asciz "\n"
.align	4
_____26: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____32: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______34: .word	5
_____36: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____37: .asciz "in main"
.align	4
_____38: .asciz "\n"
.align	4
_____39: .asciz "me.x = (5, "
.align	4
_____42: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____43: .asciz ")"
.align	4
_____44: .asciz "\n"
.align	4
_____51: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______56: .word	0
_____61: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-4, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-8,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	5,%l1
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____35 
 nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1, %o0
call exit
nop
_____35:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-12,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____41 
 nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	1, %o0
call exit
nop
_____41:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	FOO.foo, %l0
add	%g0, %l0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-4, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-24, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____48 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____49 
 nop
_____48:
add	%sp, -0, %sp
_____49:
_____47:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____50 
 nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	1, %o0
call exit
nop
_____50:
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 24) & -8

FOO.fubar: 
set SAVE.FOO.fubar, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____6 
 nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	1, %o0
call exit
nop
_____6:
ld	[%l1], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-8,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____8 
 nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	1, %o0
call exit
nop
_____8:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
ret
restore

SAVE.FOO.fubar = -(92 + 8) & -8

FOO.foo: 
set SAVE.FOO.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____16 
 nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	1, %o0
call exit
nop
_____16:
ld	[%l1], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-8,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____18 
 nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	1, %o0
call exit
nop
_____18:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	-4, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____25 
 nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	1, %o0
call exit
nop
_____25:
ld	[%l1], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	FOO.fubar, %l0
add	%g0, %l0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-12, %l1
add	%fp, %l1, %l1
ld	[%l1], %l1
st	%l1, [%l0]
set	-20, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____29 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____30 
 nop
_____29:
add	%sp, -0, %sp
_____30:
_____28:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____31 
 nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	1, %o0
call exit
nop
_____31:
call %l1
nop
ret
restore

SAVE.FOO.foo = -(92 + 20) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____57 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____58 
 nop
_____57:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____58:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____59 
 nop
ba  _____60 
 nop
_____59:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
_____60:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

