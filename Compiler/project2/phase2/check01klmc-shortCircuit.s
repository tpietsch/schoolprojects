.global	foo, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "bad"
.align	4
_____3: .asciz "\n"
.align	4
______5: .word	1
______6: .word	1
______16: .word	0
______17: .word	1
_____20: .asciz "good"
.align	4
_____21: .asciz "\n"
.align	4
______32: .word	0
______33: .word	1
_____36: .asciz "bad"
.align	4
_____37: .asciz "\n"
.align	4
_____38: .asciz "good"
.align	4
_____39: .asciz "\n"
.align	4
______49: .word	0
______50: .word	1
_____53: .asciz "good"
.align	4
_____54: .asciz "\n"
.align	4
______64: .word	0
______65: .word	1
_____68: .asciz "bad"
.align	4
_____69: .asciz "\n"
.align	4
_____70: .asciz "good"
.align	4
_____71: .asciz "\n"
.align	4
______76: .word	0
_____81: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____7 
 nop
ba  _____9 
 nop
ba  _____8 
 nop
_____7:
_____8:
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____11 
 nop
ba  _____9 
 nop
ba  _____12 
 nop
_____11:
_____12:
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____15 
 nop
_____9:
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____15:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____18 
 nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
ba  _____19 
 nop
_____18:
_____19:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-4,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-16,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____23 
 nop
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
ba  _____28 
 nop
ba  _____27 
 nop
_____26:
ba  _____27 
 nop
_____23:
_____27:
_____24:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____31 
 nop
_____28:
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____31:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____34 
 nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
ba  _____35 
 nop
_____34:
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
_____35:
set	1,%l1
cmp	%l1, %g0
be  _____40 
 nop
ba  _____42 
 nop
ba  _____41 
 nop
_____40:
_____41:
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____44 
 nop
ba  _____42 
 nop
ba  _____45 
 nop
_____44:
_____45:
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____48 
 nop
_____42:
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____48:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____51 
 nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
ba  _____52 
 nop
_____51:
_____52:
set	0,%l1
cmp	%l1, %g0
be  _____55 
 nop
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____58 
 nop
ba  _____60 
 nop
ba  _____59 
 nop
_____58:
ba  _____59 
 nop
_____55:
_____59:
_____56:
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____63 
 nop
_____60:
set	1,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____63:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____66 
 nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
ba  _____67 
 nop
_____66:
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
_____67:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 40) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%i0
ret
restore

ret
restore

SAVE.foo = -(92 + 4) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____77 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____78 
 nop
_____77:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____78:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____79 
 nop
ba  _____80 
 nop
_____79:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____81,%o1
call printf
nop
_____80:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

