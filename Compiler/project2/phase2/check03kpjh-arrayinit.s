.global	x, __________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	4
______5: .word	0
______10: .word	16
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "0"
.align	4
_____18: .asciz " is outside legal range [0,4).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "0"
.align	4
_____30: .asciz " is outside legal range [0,4).\n"
.align	4
______32: .word	1
_____35: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______37: .word	4
______39: .word	1
______44: .word	16
_____50: .asciz "Index value of "
.align	4
_____51: .asciz "1"
.align	4
_____52: .asciz " is outside legal range [0,4).\n"
.align	4
______57: .word	0
_____62: .asciz "Index value of "
.align	4
_____63: .asciz "1"
.align	4
_____64: .asciz " is outside legal range [0,4).\n"
.align	4
______66: .word	2
_____69: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______71: .word	4
______73: .word	2
______78: .word	16
_____84: .asciz "Index value of "
.align	4
_____85: .asciz "2"
.align	4
_____86: .asciz " is outside legal range [0,4).\n"
.align	4
______91: .word	0
_____96: .asciz "Index value of "
.align	4
_____97: .asciz "2"
.align	4
_____98: .asciz " is outside legal range [0,4).\n"
.align	4
______100: .word	3
_____103: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______105: .word	4
______107: .word	3
______112: .word	16
_____118: .asciz "Index value of "
.align	4
_____119: .asciz "3"
.align	4
_____120: .asciz " is outside legal range [0,4).\n"
.align	4
______125: .word	0
_____130: .asciz "Index value of "
.align	4
_____131: .asciz "3"
.align	4
_____132: .asciz " is outside legal range [0,4).\n"
.align	4
______134: .word	4
_____137: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______138: .word	0
______144: .word	4
______148: .word	4
______154: .word	16
_____160: .asciz "Index value of "
.align	4
_____161: .asciz " is outside legal range [0,4).\n"
.align	4
______166: .word	0
_____171: .asciz "Index value of "
.align	4
_____172: .asciz " is outside legal range [0,4).\n"
.align	4
_____175: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____176: .asciz "\n"
.align	4
______178: .word	1
______186: .word	0
_____191: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
x:	.skip	16
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	4,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-12,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	16,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	16,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-16,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-16,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	x, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-44,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____34 
 nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	1, %o0
call exit
nop
_____34:
st	%f1, [%l0]
set	4,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-56,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	16,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	16,%l1
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____46 
 nop
set	-60,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____47 
 nop
_____46:
set	1,%l3
set	-60,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____47:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____48 
 nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____49 
 nop
_____48:
_____49:
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____58 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____59 
 nop
_____58:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____59:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____60 
 nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____61 
 nop
_____60:
_____61:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	x, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-88,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____68 
 nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	1, %o0
call exit
nop
_____68:
st	%f1, [%l0]
set	4,%l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-100,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	16,%l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	16,%l1
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____80 
 nop
set	-104,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____81 
 nop
_____80:
set	1,%l3
set	-104,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____81:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____82 
 nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____83 
 nop
_____82:
_____83:
set	0, %l0
set	-116,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____92 
 nop
set	-116,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____93 
 nop
_____92:
set	1,%l3
set	-116,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____93:
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____94 
 nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____95 
 nop
_____94:
_____95:
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	x, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-132,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____102 
 nop
set	_strFmt,%o0
set	_____103,%o1
call printf
nop
set	1, %o0
call exit
nop
_____102:
st	%f1, [%l0]
set	4,%l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-144,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	16,%l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	16,%l1
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____114 
 nop
set	-148,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____115 
 nop
_____114:
set	1,%l3
set	-148,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____115:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____116 
 nop
set	_strFmt,%o0
set	_____118,%o1
call printf
nop
set	_strFmt,%o0
set	_____119,%o1
call printf
nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____117 
 nop
_____116:
_____117:
set	0, %l0
set	-160,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____126 
 nop
set	-160,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____127 
 nop
_____126:
set	1,%l3
set	-160,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____127:
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____128 
 nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
set	_strFmt,%o0
set	_____132,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____129 
 nop
_____128:
_____129:
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	x, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-176,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____136 
 nop
set	_strFmt,%o0
set	_____137,%o1
call printf
nop
set	1, %o0
call exit
nop
_____136:
st	%f1, [%l0]
set	16, %o2
set	x, %o1
add	%g0, %o1, %o1
set	-192, %o0
add	%fp, %o0, %o0
call memmove
nop
set	0,%l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____139,_____140) 
_____139:
set	0, %l0
set	-200,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-208,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	4,%l2
cmp	%l1, %l2
bl  _____145 
 nop
set	-200,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____146 
 nop
_____145:
set	1,%l3
set	-200,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____146:
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____140 
 nop
set	4,%l1
set	-212,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-216,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-220,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-224,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	16,%l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	16,%l1
set	-232,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____156 
 nop
set	-224,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____157 
 nop
_____156:
set	1,%l3
set	-224,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____157:
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____158 
 nop
set	_strFmt,%o0
set	_____160,%o1
call printf
nop
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____161,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____159 
 nop
_____158:
_____159:
set	0, %l0
set	-236,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-240,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-240,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-244,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-240,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____167 
 nop
set	-236,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____168 
 nop
_____167:
set	1,%l3
set	-236,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____168:
set	-236,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____169 
 nop
set	_strFmt,%o0
set	_____171,%o1
call printf
nop
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____172,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____170 
 nop
_____169:
_____170:
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-192, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-248,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-248,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____174 
 nop
set	_strFmt,%o0
set	_____175,%o1
call printf
nop
set	1, %o0
call exit
nop
_____174:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____176,%o1
call printf
nop
set	1,%l1
set	-252,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-256,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-256,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-260,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-264,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-260,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____139 
 nop
_____140:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 264) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____187 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____188 
 nop
_____187:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____188:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____189 
 nop
ba  _____190 
 nop
_____189:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____191,%o1
call printf
nop
_____190:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

