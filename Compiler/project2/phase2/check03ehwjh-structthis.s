.global	FOO.mult, FOO.foo, FOO.printX, __________.MEMLEAK,main
.section	".data"
.align	4
_____5: .asciz "in me.printX()"
.align	4
_____6: .asciz "\n"
.align	4
_____7: .asciz "this.x = (5, "
.align	4
_____10: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____12: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____13: .asciz ")"
.align	4
_____14: .asciz "\n"
.align	4
_____17: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______18: .word	99
_____20: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____21: .asciz "this.x = 99"
.align	4
_____22: .asciz "\n"
.align	4
_____23: .asciz "this.x = (99, "
.align	4
_____26: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____28: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____29: .asciz ")"
.align	4
_____30: .asciz "\n"
.align	4
______32: .word	5
_____34: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____35: .asciz "me.x = (5, "
.align	4
_____38: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____39: .asciz ")"
.align	4
_____40: .asciz "\n"
.align	4
______42: .word	9
_____44: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____45: .asciz "me.y = (9, "
.align	4
_____48: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____49: .asciz ")"
.align	4
_____50: .asciz "\n"
.align	4
_____51: .asciz "me.printX()"
.align	4
_____52: .asciz "\n"
.align	4
_____59: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____60: .asciz "me.x = (99, "
.align	4
_____63: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____64: .asciz ")"
.align	4
_____65: .asciz "\n"
.align	4
_____66: .asciz "me.y = (9, "
.align	4
_____69: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____70: .asciz ")"
.align	4
_____71: .asciz "\n"
.align	4
______76: .word	0
_____81: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-8, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-16,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	5,%l1
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____33 
 nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	1, %o0
call exit
nop
_____33:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-24,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____37 
 nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	1, %o0
call exit
nop
_____37:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-32,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	9,%l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____43 
 nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	1, %o0
call exit
nop
_____43:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-40,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____47 
 nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	1, %o0
call exit
nop
_____47:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	FOO.printX, %l0
add	%g0, %l0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-56, %l0
add	%fp, %l0, %l0
add	4, %l0, %l0
set	-8, %l1
add	%fp, %l1, %l1
st	%l1, [%l0]
set	-56, %l0
add	%fp, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____56 
 nop
mov	%l1, %o0
add	%sp, -0, %sp
ba  _____57 
 nop
_____56:
add	%sp, -0, %sp
_____57:
_____55:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____58 
 nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	1, %o0
call exit
nop
_____58:
call %l1
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-64,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____62 
 nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	1, %o0
call exit
nop
_____62:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	_strFmt,%o0
set	_____66,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-72,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____68 
 nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	1, %o0
call exit
nop
_____68:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 72) & -8

FOO.mult: 
set SAVE.FOO.mult, %g1
save %sp, %g1, %sp
set	-8,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-24,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-24,%l1
add 	%fp, %l1, %l1
ld	[%l1], %i0
ret
restore

ret
restore

SAVE.FOO.mult = -(92 + 24) & -8

FOO.foo: 
set SAVE.FOO.foo, %g1
save %sp, %g1, %sp
set	-8,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
st	%i2, [%l0]
ret
restore

SAVE.FOO.foo = -(92 + 16) & -8

FOO.printX: 
set SAVE.FOO.printX, %g1
save %sp, %g1, %sp
set	-8,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____9 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1, %o0
call exit
nop
_____9:
ld	[%l1], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-16,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____11 
 nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1, %o0
call exit
nop
_____11:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____16 
 nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	1, %o0
call exit
nop
_____16:
ld	[%l1], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-24,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	99,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____19 
 nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	1, %o0
call exit
nop
_____19:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	-8, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____25 
 nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	1, %o0
call exit
nop
_____25:
ld	[%l1], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-32,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____27 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	1, %o0
call exit
nop
_____27:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
ret
restore

SAVE.FOO.printX = -(92 + 32) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____77 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____78 
 nop
_____77:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____78:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____79 
 nop
ba  _____80 
 nop
_____79:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____81,%o1
call printf
nop
_____80:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

