.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .single	0r3.1
______3: .single	0r3.1
_____4: .single	0r4.0
______5: .single	0r4.0
_____12: .asciz "\n"
.align	4
_____19: .asciz "\n"
.align	4
_____26: .single	0r3.0
______29: .single	0r3.0
_____31: .asciz "\n"
.align	4
_____41: .asciz "\n"
.align	4
_____57: .asciz "\n"
.align	4
______62: .word	0
_____67: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	______3,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-20,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-36,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-44,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-48,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-56,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-60,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-68,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-68,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-72,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-80,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-84,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______29,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-92,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-96,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-100,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-104,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-104,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-104,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-108,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-112,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-116,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-116,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-116,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-120,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-124,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-128,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-128,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-128,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-136,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-140,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-148,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-144,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-148,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-152,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-160,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-156,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-160,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-164,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-168,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-168,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-172,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-168,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-172,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-176,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-180,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-180,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-184,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-180,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-184,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-188,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-192,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-192,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-196,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-192,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-196,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-200,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-204,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 204) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____63 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____64 
 nop
_____63:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____64:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____65 
 nop
ba  _____66 
 nop
_____65:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
_____66:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

