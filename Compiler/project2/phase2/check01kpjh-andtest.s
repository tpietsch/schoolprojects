.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	0
______4: .word	5
______5: .word	6
______14: .word	0
______15: .word	1
_____18: .asciz "\n"
.align	4
______32: .word	0
______33: .word	1
_____36: .asciz "\n"
.align	4
______42: .word	1
______57: .word	0
______58: .word	1
_____61: .asciz "\n"
.align	4
_____62: .asciz "\n"
.align	4
______67: .word	0
_____72: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____6 
 nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____8 
 nop
ba  _____10 
 nop
ba  _____9 
 nop
_____8:
ba  _____9 
 nop
_____6:
_____9:
_____7:
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____13 
 nop
_____10:
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____13:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____16 
 nop
set	_boolF,%o1
ba  _____17 
 nop
_____16:
set	_boolT,%o1
_____17:
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____19 
 nop
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
ba  _____28 
 nop
ba  _____27 
 nop
_____26:
ba  _____27 
 nop
_____19:
_____27:
_____20:
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____31 
 nop
_____28:
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____31:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____34 
 nop
set	_boolF,%o1
ba  _____35 
 nop
_____34:
set	_boolT,%o1
_____35:
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-40,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-44,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____39 
 nop
set	1,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-56,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____49 
 nop
set	-64,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____50 
 nop
_____49:
set	1,%l3
set	-64,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____50:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____51 
 nop
ba  _____53 
 nop
ba  _____52 
 nop
_____51:
ba  _____52 
 nop
_____39:
_____52:
_____40:
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____56 
 nop
_____53:
set	1,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____56:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____59 
 nop
set	_boolF,%o1
ba  _____60 
 nop
_____59:
set	_boolT,%o1
_____60:
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 76) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____68 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____69 
 nop
_____68:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____69:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____70 
 nop
ba  _____71 
 nop
_____70:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
_____71:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

