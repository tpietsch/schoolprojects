.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	0
_____4: .asciz "Output format: (expected, actual) "
.align	4
_____5: .asciz "\n"
.align	4
_____6: .asciz "Const fold test"
.align	4
_____7: .asciz "\n"
.align	4
_____8: .asciz "false && false = (false, "
.align	4
_____17: .asciz "false"
.align	4
_____18: .asciz ")"
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "false && true = (false, "
.align	4
_____29: .asciz "false"
.align	4
_____30: .asciz ")"
.align	4
_____31: .asciz "\n"
.align	4
_____32: .asciz "true && false = (false, "
.align	4
_____41: .asciz "false"
.align	4
_____42: .asciz ")"
.align	4
_____43: .asciz "\n"
.align	4
_____44: .asciz "true && true = (true, "
.align	4
_____53: .asciz "true"
.align	4
_____54: .asciz ")"
.align	4
_____55: .asciz "\n"
.align	4
_____56: .asciz "Variable declarations: "
.align	4
_____57: .asciz "\n"
.align	4
_____58: .asciz "bool _true = true, _false = false "
.align	4
_____59: .asciz "\n"
.align	4
_____60: .asciz "_false && _false = (false, "
.align	4
______69: .word	0
______70: .word	1
_____73: .asciz ")"
.align	4
_____74: .asciz "\n"
.align	4
_____75: .asciz "_false && false = (false, "
.align	4
______84: .word	0
______85: .word	1
_____88: .asciz ")"
.align	4
_____89: .asciz "\n"
.align	4
_____90: .asciz "false && _false = (false, "
.align	4
______99: .word	0
______100: .word	1
_____103: .asciz ")"
.align	4
_____104: .asciz "\n"
.align	4
_____105: .asciz "_false && _true = (false, "
.align	4
______114: .word	0
______115: .word	1
_____118: .asciz ")"
.align	4
_____119: .asciz "\n"
.align	4
_____120: .asciz "_true && _false = (false, "
.align	4
______129: .word	0
______130: .word	1
_____133: .asciz ")"
.align	4
_____134: .asciz "\n"
.align	4
_____135: .asciz "_true && _true = (true, "
.align	4
______144: .word	0
______145: .word	1
_____148: .asciz ")"
.align	4
_____149: .asciz "\n"
.align	4
______154: .word	0
_____159: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____9 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____11 
 nop
ba  _____13 
 nop
ba  _____12 
 nop
_____11:
ba  _____12 
 nop
_____9:
_____12:
_____10:
ba  _____16 
 nop
_____13:
_____16:
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____21 
 nop
set	1,%l1
cmp	%l1, %g0
be  _____23 
 nop
ba  _____25 
 nop
ba  _____24 
 nop
_____23:
ba  _____24 
 nop
_____21:
_____24:
_____22:
ba  _____28 
 nop
_____25:
_____28:
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____33 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____35 
 nop
ba  _____37 
 nop
ba  _____36 
 nop
_____35:
ba  _____36 
 nop
_____33:
_____36:
_____34:
ba  _____40 
 nop
_____37:
_____40:
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____45 
 nop
set	1,%l1
cmp	%l1, %g0
be  _____47 
 nop
ba  _____49 
 nop
ba  _____48 
 nop
_____47:
ba  _____48 
 nop
_____45:
_____48:
_____46:
ba  _____52 
 nop
_____49:
_____52:
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____61 
 nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____63 
 nop
ba  _____65 
 nop
ba  _____64 
 nop
_____63:
ba  _____64 
 nop
_____61:
_____64:
_____62:
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____68 
 nop
_____65:
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____68:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____71 
 nop
set	_boolF,%o1
ba  _____72 
 nop
_____71:
set	_boolT,%o1
_____72:
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
set	_strFmt,%o0
set	_____75,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____76 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____78 
 nop
ba  _____80 
 nop
ba  _____79 
 nop
_____78:
ba  _____79 
 nop
_____76:
_____79:
_____77:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____83 
 nop
_____80:
set	1,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____83:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____86 
 nop
set	_boolF,%o1
ba  _____87 
 nop
_____86:
set	_boolT,%o1
_____87:
call printf
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____91 
 nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____93 
 nop
ba  _____95 
 nop
ba  _____94 
 nop
_____93:
ba  _____94 
 nop
_____91:
_____94:
_____92:
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____98 
 nop
_____95:
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____98:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____101 
 nop
set	_boolF,%o1
ba  _____102 
 nop
_____101:
set	_boolT,%o1
_____102:
call printf
nop
set	_strFmt,%o0
set	_____103,%o1
call printf
nop
set	_strFmt,%o0
set	_____104,%o1
call printf
nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____106 
 nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____108 
 nop
ba  _____110 
 nop
ba  _____109 
 nop
_____108:
ba  _____109 
 nop
_____106:
_____109:
_____107:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____113 
 nop
_____110:
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____113:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____116 
 nop
set	_boolF,%o1
ba  _____117 
 nop
_____116:
set	_boolT,%o1
_____117:
call printf
nop
set	_strFmt,%o0
set	_____118,%o1
call printf
nop
set	_strFmt,%o0
set	_____119,%o1
call printf
nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____121 
 nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____123 
 nop
ba  _____125 
 nop
ba  _____124 
 nop
_____123:
ba  _____124 
 nop
_____121:
_____124:
_____122:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____128 
 nop
_____125:
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____128:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____131 
 nop
set	_boolF,%o1
ba  _____132 
 nop
_____131:
set	_boolT,%o1
_____132:
call printf
nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	_strFmt,%o0
set	_____134,%o1
call printf
nop
set	_strFmt,%o0
set	_____135,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____136 
 nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____138 
 nop
ba  _____140 
 nop
ba  _____139 
 nop
_____138:
ba  _____139 
 nop
_____136:
_____139:
_____137:
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____143 
 nop
_____140:
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____143:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____146 
 nop
set	_boolF,%o1
ba  _____147 
 nop
_____146:
set	_boolT,%o1
_____147:
call printf
nop
set	_strFmt,%o0
set	_____148,%o1
call printf
nop
set	_strFmt,%o0
set	_____149,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 32) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____155 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____156 
 nop
_____155:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____156:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____157 
 nop
ba  _____158 
 nop
_____157:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____159,%o1
call printf
nop
_____158:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

