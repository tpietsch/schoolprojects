.global	_0iGlobalUnit1, _0iGlobalInit1, _0iGlobalInit21, _0iGlobalConst1, _0iGlobalConst21,main
.section	".data"
.align	4
______0: .word	2

______1: .word	3

______2: .word	3

______3: .word	3

______4: .word	6

______5: .word	7

______6: .word	8

_____7: .asciz "7"
.align	4
_____8: .asciz " hello "
.align	4
_____9: .asciz "8"
.align	4
______10: .word	3

_____11: .asciz "3"
.align	4
______12: .word	3

_____13: .asciz "3"
.align	4
_____14: .asciz "8"
.align	4
______15: .word	9

.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::iGlobalUnit
_0iGlobalUnit1:	.skip	4
!CodeGen::doVarDecl::iGlobalInit
_0iGlobalInit1:	.skip	4
!CodeGen::doVarDecl::iGlobalInit2
_0iGlobalInit21:	.skip	4
!CodeGen::doVarDecl::iGlobalConst
_0iGlobalConst1:	.skip	4
!CodeGen::doVarDecl::iGlobalConst2
_0iGlobalConst21:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld[%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
! Doing a vardeclass 
! Generating STO 
! declaring a variable 2 
! Doing a vardeclass 
! Generating STO 
! doConst ( 2,______0 ) 
! sto  2 (______0) 
! STO's name is( 2, %g0,______0 ) 
set	2,%l1
! STO's store name is( iGlobalInit, %g0,_0iGlobalInit1 ) 
set	_0iGlobalInit1,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
! declaring a variable iGlobalInit 
! Doing a vardeclass 
! Generating STO 
! STO's name is( iGlobalInit, %g0,_0iGlobalInit1 ) 
set	_0iGlobalInit1,%i0
add 	%g0, %i0, %i0
ld	[%i0], %l1
! STO's store name is( iGlobalInit2, %g0,_0iGlobalInit21 ) 
set	_0iGlobalInit21,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
! declaring a variable 3 
! Doing a vardeclass 
! Generating STO 
! doConst ( 3,______1 ) 
! sto  3 (______1) 
! STO's name is( 3, %g0,______1 ) 
set	3,%l1
! STO's store name is( iGlobalConst, %g0,_0iGlobalConst1 ) 
set	_0iGlobalConst1,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
! declaring a variable iGlobalConst 
! Doing a vardeclass 
! Generating STO 
! doConst ( iGlobalConst,______2 ) 
! sto  iGlobalConst (______2) 
! STO's name is( iGlobalConst, %g0,______2 ) 
set	3,%l1
! STO's store name is( iGlobalConst2, %g0,_0iGlobalConst21 ) 
set	_0iGlobalConst21,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
_init_done:
! handleFuncParams()  
! handleFuncParams() end 
! Doing a vardeclass 
! Generating STO 
! Doing a vardeclass 
! Generating STO 
! doConst ( 3,______3 ) 
! sto  3 (______3) 
! STO's name is( 3, %g0,______3 ) 
set	3,%l1
! STO's store name is( iLocalUnit, %fp,-4 ) 
set	-4,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_intFmt,%o0
! STO's name is( iLocalUnit, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
! STO's name is( iLocalUnit, %fp,-4 ) 
set	-4,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( iLocalUnit2, %fp,-8 ) 
set	-8,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_intFmt,%o0
! STO's name is( iLocalUnit2, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
! declaring a variable 6 
! Doing a vardeclass 
! Generating STO 
! doConst ( 6,______4 ) 
! sto  6 (______4) 
! STO's name is( 6, %g0,______4 ) 
set	6,%l1
! STO's store name is( iLocalInit, %fp,-12 ) 
set	-12,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_intFmt,%o0
! STO's name is( iLocalInit, %fp,-12 ) 
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
! declaring a variable iLocalInit 
! Doing a vardeclass 
! Generating STO 
! STO's name is( iLocalInit, %fp,-12 ) 
set	-12,%i0
add 	%fp, %i0, %i0
ld	[%i0], %l1
! STO's store name is( iLocalInit2, %fp,-16 ) 
set	-16,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_intFmt,%o0
! STO's name is( iLocalInit2, %fp,-16 ) 
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
! declaring a variable iGlobalInit 
! Doing a vardeclass 
! Generating STO 
! STO's name is( iGlobalInit, %g0,_0iGlobalInit1 ) 
set	_0iGlobalInit1,%i0
add 	%g0, %i0, %i0
ld	[%i0], %l1
! STO's store name is( iLocalInit3, %fp,-20 ) 
set	-20,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_intFmt,%o0
! STO's name is( iLocalInit3, %fp,-20 ) 
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
call printf
nop
! declaring a variable 7 
! Doing a vardeclass 
! Generating STO 
! doConst ( 7,______5 ) 
! sto  7 (______5) 
! STO's name is( 7, %g0,______5 ) 
set	7,%l1
! STO's store name is( iLocalConst, %fp,-24 ) 
set	-24,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
! declaring a variable 8 
! Doing a vardeclass 
! Generating STO 
! doConst ( 8,______6 ) 
! sto  8 (______6) 
! STO's name is( 8, %g0,______6 ) 
set	8,%l1
! STO's store name is( iLocalConst4, %fp,-28 ) 
set	-28,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
! declaring a variable iGlobalConst 
! Doing a vardeclass 
! Generating STO 
! doConst ( iGlobalConst,______10 ) 
! sto  iGlobalConst (______10) 
! STO's name is( iGlobalConst, %g0,______10 ) 
set	3,%l1
! STO's store name is( iLocalConst2, %fp,-32 ) 
set	-32,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
! declaring a variable iGlobalConst 
! Doing a vardeclass 
! Generating STO 
! doConst ( iGlobalConst,______12 ) 
! sto  iGlobalConst (______12) 
! STO's name is( iGlobalConst, %g0,______12 ) 
set	3,%l1
! STO's store name is( iLocalConst3, %fp,-36 ) 
set	-36,%i1
add 	%fp, %i1, %i1
st	%l1, [%i1]
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
! doConst ( 9,______15 ) 
! sto  9 (______15) 
! STO's name is( 9, %g0,______15 ) 
set	9,%l1
! STO's store name is( iGlobalUnit, %g0,_0iGlobalUnit1 ) 
set	_0iGlobalUnit1,%i1
add 	%g0, %i1, %i1
st	%l1, [%i1]
set	_intFmt,%o0
! STO's name is( iGlobalUnit, %g0,_0iGlobalUnit1 ) 
set	_0iGlobalUnit1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
call printf
nop
ret
restore

SAVE.main = -(92 + 36) & -8

