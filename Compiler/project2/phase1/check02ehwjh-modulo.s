.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	7
______3: .word	10
______4: .word	33
_____5: .asciz "Output in the form of (expected, actual) "
.align	4
_____6: .asciz "\n"
.align	4
_____7: .asciz "Const Fold: "
.align	4
_____8: .asciz "\n"
.align	4
_____9: .asciz "7 %% 5 = (2, "
.align	4
_____10: .asciz "2"
.align	4
_____11: .asciz ")"
.align	4
_____12: .asciz "\n"
.align	4
_____13: .asciz "5 %% 7 = (5, "
.align	4
_____14: .asciz "5"
.align	4
_____15: .asciz ")"
.align	4
_____16: .asciz "\n"
.align	4
_____17: .asciz "94 %% 76 = (18, "
.align	4
_____18: .asciz "18"
.align	4
_____19: .asciz ")"
.align	4
_____20: .asciz "\n"
.align	4
_____21: .asciz "Variable declarations: "
.align	4
_____22: .asciz "\n"
.align	4
_____23: .asciz "int x = 7, y = 10, z = 33"
.align	4
_____24: .asciz "\n"
.align	4
_____25: .asciz "x %% y = (7, "
.align	4
_____29: .asciz ")"
.align	4
_____30: .asciz "\n"
.align	4
_____31: .asciz "y %% x = (3, "
.align	4
_____35: .asciz ")"
.align	4
_____36: .asciz "\n"
.align	4
_____37: .asciz "z %% x = (5, "
.align	4
_____41: .asciz ")"
.align	4
_____42: .asciz "\n"
.align	4
_____43: .asciz "x %% x = (0, "
.align	4
_____47: .asciz ")"
.align	4
_____48: .asciz "\n"
.align	4
_____49: .asciz "0 %% x = (0, "
.align	4
______51: .word	0
_____54: .asciz ")"
.align	4
_____55: .asciz "\n"
.align	4
______60: .word	0
_____65: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	7,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	33,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-24,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-36,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-48,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-60,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	0,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%o0
set	-68,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-72,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 72) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____61 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____62 
 nop
_____61:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____62:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____63 
 nop
ba  _____64 
 nop
_____63:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
_____64:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

