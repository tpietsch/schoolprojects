.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	2
______3: .word	4
______5: .word	4
______7: .word	3
______12: .word	40
_____18: .asciz "Index value of "
.align	4
_____19: .asciz "3"
.align	4
_____20: .asciz " is outside legal range [0,10).\n"
.align	4
______25: .word	0
_____30: .asciz "Index value of "
.align	4
_____31: .asciz "3"
.align	4
_____32: .asciz " is outside legal range [0,10).\n"
.align	4
______34: .word	5
_____36: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______38: .word	4
______40: .word	1
______45: .word	40
_____51: .asciz "Index value of "
.align	4
_____52: .asciz "1"
.align	4
_____53: .asciz " is outside legal range [0,10).\n"
.align	4
______58: .word	0
_____63: .asciz "Index value of "
.align	4
_____64: .asciz "1"
.align	4
_____65: .asciz " is outside legal range [0,10).\n"
.align	4
______67: .word	0
_____69: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____70: .asciz "array1[0] = 5, "
.align	4
______72: .word	4
______74: .word	3
______79: .word	40
_____85: .asciz "Index value of "
.align	4
_____86: .asciz "3"
.align	4
_____87: .asciz " is outside legal range [0,10).\n"
.align	4
______92: .word	0
_____97: .asciz "Index value of "
.align	4
_____98: .asciz "3"
.align	4
_____99: .asciz " is outside legal range [0,10).\n"
.align	4
_____102: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____103: .asciz "\n"
.align	4
______108: .word	0
_____113: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	2,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-60,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____14 
 nop
set	-64,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____15 
 nop
_____14:
set	1,%l3
set	-64,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____15:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____16 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____17 
 nop
_____16:
_____17:
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____26 
 nop
set	-76,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____27 
 nop
_____26:
set	1,%l3
set	-76,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____27:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____28 
 nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____29 
 nop
_____28:
_____29:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____35 
 nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1, %o0
call exit
nop
_____35:
st	%l1, [%l0]
set	4,%l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-100,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____47 
 nop
set	-104,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____48 
 nop
_____47:
set	1,%l3
set	-104,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____48:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____49 
 nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____50 
 nop
_____49:
_____50:
set	0, %l0
set	-116,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____59 
 nop
set	-116,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____60 
 nop
_____59:
set	1,%l3
set	-116,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____60:
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____61 
 nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____62 
 nop
_____61:
_____62:
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____68 
 nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	1, %o0
call exit
nop
_____68:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
set	4,%l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-140,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-144,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____81 
 nop
set	-144,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____82 
 nop
_____81:
set	1,%l3
set	-144,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____82:
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____83 
 nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____84 
 nop
_____83:
_____84:
set	0, %l0
set	-156,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____93 
 nop
set	-156,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____94 
 nop
_____93:
set	1,%l3
set	-156,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____94:
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____95 
 nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
set	_strFmt,%o0
set	_____99,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____96 
 nop
_____95:
_____96:
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____101 
 nop
set	_strFmt,%o0
set	_____102,%o1
call printf
nop
set	1, %o0
call exit
nop
_____101:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____103,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 168) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____109 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____110 
 nop
_____109:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____110:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____111 
 nop
ba  _____112 
 nop
_____111:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____113,%o1
call printf
nop
_____112:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

