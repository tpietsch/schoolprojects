.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	5
_____3: .single	0r2.75
______4: .single	0r2.75
_____5: .asciz "Variable Declarations"
.align	4
_____6: .asciz "\n"
.align	4
_____7: .asciz "int x = 5"
.align	4
_____8: .asciz "\n"
.align	4
_____9: .asciz "float f = 2.75"
.align	4
_____10: .asciz "\n"
.align	4
_____11: .asciz "Constant test "
.align	4
_____12: .asciz "\n"
.align	4
_____13: .asciz "int op int"
.align	4
_____14: .asciz "\n"
.align	4
_____15: .asciz "1 - 2 = "
.align	4
_____16: .asciz "-1"
.align	4
_____17: .asciz "\n"
.align	4
_____18: .asciz "Float promotion w/consts"
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "1 - 2.1 = "
.align	4
_____21: .single	0r2.1
_____22: .single 0r-1.0999999
_____23: .asciz "\n"
.align	4
_____24: .asciz "2.1 - 1 = "
.align	4
_____25: .single	0r2.1
_____26: .single 0r1.0999999
_____27: .asciz "\n"
.align	4
_____28: .asciz "Variable tests"
.align	4
_____29: .asciz "\n"
.align	4
_____30: .asciz "x - 7 = "
.align	4
______33: .word	7
_____35: .asciz "\n"
.align	4
_____36: .asciz "7 - x = "
.align	4
______38: .word	7
_____41: .asciz "\n"
.align	4
_____42: .asciz "x - 2.1 = "
.align	4
_____43: .single	0r2.1
______46: .single	0r2.1
_____48: .asciz "\n"
.align	4
_____49: .asciz "2.1 - x = "
.align	4
_____50: .single	0r2.1
______52: .single	0r2.1
_____55: .asciz "\n"
.align	4
_____56: .asciz "f - 5 = "
.align	4
______59: .word	5
_____61: .asciz "\n"
.align	4
_____62: .asciz "5 - f = "
.align	4
______64: .word	5
_____67: .asciz "\n"
.align	4
_____68: .asciz "f - x = "
.align	4
_____72: .asciz "\n"
.align	4
_____73: .asciz "x - f = "
.align	4
_____77: .asciz "\n"
.align	4
______82: .word	0
_____87: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______4,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_____22,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_____26,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	7,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	7,%l5
sub	%l4, %l5, %l5
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	7,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	7,%l4
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______46,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-36,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-44,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	______52,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-52,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-56,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	5,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-64,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-68,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	5,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-72,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-80,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-88,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-84,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-92,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-100,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-96,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-96,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-96,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-100,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-104,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 104) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____83 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____84 
 nop
_____83:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____84:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____85 
 nop
ba  _____86 
 nop
_____85:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
_____86:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

