.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	5
______3: .word	2
_____7: .asciz "\n"
.align	4
_____11: .asciz "\n"
.align	4
_____15: .asciz "\n"
.align	4
______18: .word	3
_____20: .asciz "\n"
.align	4
______23: .word	1
_____25: .asciz "\n"
.align	4
______27: .word	6
_____30: .asciz "\n"
.align	4
______35: .word	0
_____40: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-20,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
set	-32,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-44,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-56,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	1,%o1
call .div
nop
mov	%o0, %l2
set	-68,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	6,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%o0
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-80,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 80) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____36 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____37 
 nop
_____36:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____37:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____38 
 nop
ba  _____39 
 nop
_____38:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
_____39:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

