.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	5
______3: .word	10
______6: .word	5
_____8: .asciz " == 10"
.align	4
_____9: .asciz "\n"
.align	4
______11: .word	0
_____17: .asciz " == 5"
.align	4
_____18: .asciz "\n"
.align	4
______20: .word	0
_____26: .asciz " == -15"
.align	4
_____27: .asciz "\n"
.align	4
______29: .word	0
_____35: .asciz " == -50"
.align	4
_____36: .asciz "\n"
.align	4
______38: .word	0
_____44: .asciz " == 0"
.align	4
_____45: .asciz "\n"
.align	4
_____49: .asciz " == 15"
.align	4
_____50: .asciz "\n"
.align	4
_____54: .asciz " == -5"
.align	4
_____55: .asciz "\n"
.align	4
_____59: .asciz " == 50"
.align	4
_____60: .asciz "\n"
.align	4
_____64: .asciz " == 2"
.align	4
_____65: .asciz "\n"
.align	4
______68: .word	4
_____70: .asciz " == 2"
.align	4
_____71: .asciz "\n"
.align	4
______72: .word	10
______74: .word	0
______76: .word	10
_____78: .asciz "-10"
.align	4
_____79: .asciz " == -10"
.align	4
_____80: .asciz "\n"
.align	4
______82: .word	0
_____88: .asciz " == -2"
.align	4
_____89: .asciz "\n"
.align	4
______91: .word	0
______95: .word	0
_____101: .asciz " == 2"
.align	4
_____102: .asciz "\n"
.align	4
______107: .word	0
_____112: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	5,%l5
add	%l4, %l5, %l5
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-44,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	0,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-56,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-68,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	0,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-80,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-92,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	0,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-100,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-104,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-112,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-116,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-124,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-128,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-140,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-144,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-148,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-152,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-156,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-160,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
set	-164,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	4,%o1
call .rem
nop
mov	%o0, %l2
set	-176,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	10,%l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-184,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-188,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	10,%l5
sub	%l4, %l5, %l5
set	-192,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
set	_strFmt,%o0
set	_____80,%o1
call printf
nop
set	0,%l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-200,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-200,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-204,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-208,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-208,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-212,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-208,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-212,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
set	-216,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-216,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	0,%l1
set	-220,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-224,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-224,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-228,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0,%l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-236,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-236,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-240,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-244,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-244,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-240,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-248,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-244,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-248,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
set	-252,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	_strFmt,%o0
set	_____102,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 252) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____108 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____109 
 nop
_____108:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____109:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____110 
 nop
ba  _____111 
 nop
_____110:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____112,%o1
call printf
nop
_____111:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

