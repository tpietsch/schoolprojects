.global	foo, __________.MEMLEAK,main
.section	".data"
.align	4
______4: .word	10
_____5: .asciz "foo i = "
.align	4
______8: .word	1
______11: .word	3
_____12: .asciz "main i = 4 = "
.align	4
______15: .word	1
_____17: .asciz "\n"
.align	4
_____19: .asciz " = 9"
.align	4
_____20: .asciz "\n"
.align	4
_____21: .asciz "main i = 5 = "
.align	4
______24: .word	1
_____26: .asciz "\n"
.align	4
_____28: .asciz " = 8"
.align	4
_____29: .asciz "\n"
.align	4
_____30: .asciz "main i = 6 = "
.align	4
______33: .word	1
_____35: .asciz "\n"
.align	4
_____37: .asciz " = 7"
.align	4
_____38: .asciz "\n"
.align	4
______43: .word	0
_____48: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::i
foo__0i22staticFlag:	.skip	4
foo__0i22:	.skip	4
!CodeGen::doVarDecl::i
main__0i210staticFlag:	.skip	4
main__0i210:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	3,%l1
set	main__0i210,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	main__0i210,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0i210,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	1,%l5
add	%l4, %l5, %l5
set	-12,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	main__0i210,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	main__0i210,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	main__0i210,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0i210,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	1,%l5
add	%l4, %l5, %l5
set	-24,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	main__0i210,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	main__0i210,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	main__0i210,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0i210,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	1,%l5
add	%l4, %l5, %l5
set	-36,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	main__0i210,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	main__0i210,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	foo__0i22staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____3 
 nop
set	10,%l1
set	foo__0i22,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	foo__0i22staticFlag, %l1
st	%l0, [%l1]
_____3:
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	foo__0i22,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	foo__0i22,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	1,%l5
sub	%l4, %l5, %l5
set	-12,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	foo__0i22,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	foo__0i22,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
ret
restore

SAVE.foo = -(92 + 12) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____44 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____45 
 nop
_____44:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____45:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____46 
 nop
ba  _____47 
 nop
_____46:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
_____47:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

