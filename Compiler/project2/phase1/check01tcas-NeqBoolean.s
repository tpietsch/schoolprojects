.global	a, b, c, d, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	2
______4: .word	1
______5: .word	0
______6: .word	1
______7: .word	2
______8: .word	1
_____9: .asciz "z: "
.align	4
_____12: .asciz "\n"
.align	4
______15: .word	2
_____22: .asciz "z: "
.align	4
_____25: .asciz "\n"
.align	4
______26: .word	1
_____27: .asciz "c: "
.align	4
_____30: .asciz "\n"
.align	4
______31: .word	0
_____32: .asciz "c: "
.align	4
_____35: .asciz "\n"
.align	4
_____44: .asciz "c: "
.align	4
_____47: .asciz "\n"
.align	4
_____54: .asciz "c: "
.align	4
_____57: .asciz "\n"
.align	4
______62: .word	0
_____67: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	4
!CodeGen::doVarDecl::b
b:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
!CodeGen::doVarDecl::d
d:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	b,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	d,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____10 
 nop
set	_boolF,%o1
ba  _____11 
 nop
_____10:
set	_boolT,%o1
_____11:
call printf
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	2,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____17 
 nop
set	-16,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____18 
 nop
_____17:
set	1,%l3
set	-16,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____18:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
bne  _____20 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____21 
 nop
_____20:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____21:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____23 
 nop
set	_boolF,%o1
ba  _____24 
 nop
_____23:
set	_boolT,%o1
_____24:
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____28 
 nop
set	_boolF,%o1
ba  _____29 
 nop
_____28:
set	_boolT,%o1
_____29:
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	0,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____33 
 nop
set	_boolF,%o1
ba  _____34 
 nop
_____33:
set	_boolT,%o1
_____34:
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____39 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____40 
 nop
_____39:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____40:
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bne  _____42 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____43 
 nop
_____42:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____43:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____45 
 nop
set	_boolF,%o1
ba  _____46 
 nop
_____45:
set	_boolT,%o1
_____46:
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bne  _____49 
 nop
set	-48,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____50 
 nop
_____49:
set	1,%l3
set	-48,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____50:
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
bne  _____52 
 nop
set	-52,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____53 
 nop
_____52:
set	1,%l3
set	-52,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____53:
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____55 
 nop
set	_boolF,%o1
ba  _____56 
 nop
_____55:
set	_boolT,%o1
_____56:
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 52) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____63 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____64 
 nop
_____63:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____64:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____65 
 nop
ba  _____66 
 nop
_____65:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
_____66:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

