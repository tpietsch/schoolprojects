.global	a, b, c, d, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	2
______4: .word	1
______5: .word	0
______6: .word	1
______7: .word	2
______8: .word	1
_____9: .asciz "z: "
.align	4
_____12: .asciz "\n"
.align	4
______15: .word	2
_____19: .asciz "z: "
.align	4
_____22: .asciz "\n"
.align	4
______23: .word	1
_____24: .asciz "c: "
.align	4
_____27: .asciz "\n"
.align	4
______28: .word	0
_____29: .asciz "c: "
.align	4
_____32: .asciz "\n"
.align	4
_____38: .asciz "c: "
.align	4
_____41: .asciz "\n"
.align	4
______45: .word	2
_____48: .asciz "c: "
.align	4
_____51: .asciz "\n"
.align	4
______56: .word	0
_____61: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	4
!CodeGen::doVarDecl::b
b:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
!CodeGen::doVarDecl::d
d:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	b,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	d,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____10 
 nop
set	_boolF,%o1
ba  _____11 
 nop
_____10:
set	_boolT,%o1
_____11:
call printf
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	2,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____17 
 nop
set	-16,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____18 
 nop
_____17:
set	1,%l3
set	-16,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____18:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____20 
 nop
set	_boolF,%o1
ba  _____21 
 nop
_____20:
set	_boolT,%o1
_____21:
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	1,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____25 
 nop
set	_boolF,%o1
ba  _____26 
 nop
_____25:
set	_boolT,%o1
_____26:
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	0,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____30 
 nop
set	_boolF,%o1
ba  _____31 
 nop
_____30:
set	_boolT,%o1
_____31:
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____36 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____37 
 nop
_____36:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____37:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____39 
 nop
set	_boolF,%o1
ba  _____40 
 nop
_____39:
set	_boolT,%o1
_____40:
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	2,%l2
cmp	%l1, %l2
bne  _____46 
 nop
set	-40,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____47 
 nop
_____46:
set	1,%l3
set	-40,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____47:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____49 
 nop
set	_boolF,%o1
ba  _____50 
 nop
_____49:
set	_boolT,%o1
_____50:
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 48) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____57 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____58 
 nop
_____57:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____58:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____59 
 nop
ba  _____60 
 nop
_____59:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
_____60:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

