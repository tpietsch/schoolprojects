#!/usr/bin/perl
use strict;
use warnings;
use Term::ANSIColor;

# This is the directory where all tests exist.
my $RC_sh             = "../RC";
my $testing_directory = "tests/phase1/";
my $rc_output_suffix  = "out";
my $rc_suffix         = "rc";
my $index;
my $rc_counter;
my @rc_files;

# Let the user know what's happenin'
print "\n Gathering list of files...";

# Actually open the directory
opendir(DIR, $testing_directory) or die $!;

# Iterate through all rc files in the directory
$rc_counter = 0;
while (my $file = readdir(DIR)) {
	# We only want files
	next unless (-f "$testing_directory/$file");

	# Little bit of regex? kthxplox.
	if ($file =~ m/\.$rc_suffix$/) {
		# Files matches regex, add to array.
		$rc_files[$rc_counter] = $file;
		$rc_counter++;
	}
}
close(DIR);

# Did we find any rc files?
if (scalar(@rc_files) == 0) {
	print "no rc files found! Exiting.\n";
	exit 0;
}

# Finished checking that all files have output files associated with them.
print "done.\n";

# We doin tests now, brah!
print " " . colored("Compiling " . scalar(@rc_files) . " source files:", "underline") . "\n";

# Now compile and check each rc file.
for my $i (0 .. $#rc_files) {
	# Compile the rc file.
	`./$RC_sh $testing_directory/$rc_files[$i] > $testing_directory/$rc_files[$i].$rc_output_suffix`;
	print "   - ./$RC_sh $testing_directory/$rc_files[$i] > $testing_directory/$rc_files[$i].$rc_output_suffix\n";
}

