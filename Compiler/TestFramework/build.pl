#!/usr/bin/perl
use strict;
use warnings;
use Term::ANSIColor;
use Time::HiRes qw/ time sleep /;
use POSIX qw(strftime);

# Number of arguments
my $numArgs = $#ARGV;

if ($numArgs < 0 || $numArgs > 1) {
	printf("Yo dawg, you're runnin this script wrong. [$numArgs]\n");
	exit(0);
}

# Declare all variables.
my $arg0  = $ARGV[0];
my $arg1  = $ARGV[1];
my $debug = 0;

# Compile all sources.
if ($arg0 eq "compile") {
	if ($#ARGV == 2 && $arg1 eq "debug") {
		$debug = 1;

	# Sent something after the compile?
	} elsif ($#ARGV == 2) {
		printf("Yo dawg, not sure what '$arg1' means but get that shit outta here.\n");
		exit(0);
	}

	# Now actually compile the sources
	`cd ..;make;cd ../`;

	printf("Moving files around...");
	if ($debug) {
		`cp RCdbg.sh RC`;
	} else {
		`cp RC.sh RC`;
	}

	`chmod 755 RC`;
	printf("finished\n");

# Remove all fiiles int he bin directory.	
} elsif ($arg0 eq "clean") {
	printf("Removing files...");
	`rm -Rf bin`;
	printf("finished\n");

# Create a backup of the entire directory.
} elsif ($arg0 eq "backup") {
	my $directory = `pwd`;
	my $time      = time;
	printf("Creating backup...");
	`zip -r backup_$time.zip $directory`;
	printf("finished\n");

} elsif ($arg0 eq "tar") {

# Unsupported
} else {
}
