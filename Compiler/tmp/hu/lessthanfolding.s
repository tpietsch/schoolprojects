.global	a, b, c, a1, a2, c1, a3, a4, c2, a5, a6, c3, a7, a8, c4, c5, c6, c7, c8, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	2
______3: .word	4
______4: .word	1
_____5: .single	0r7.8
______6: .single	0r7.8
_____7: .single	0r3.5
______8: .single	0r3.5
______9: .word	0
_____10: .single	0r7.8
______11: .single	0r7.8
_____12: .single	0r13.5
______13: .single	0r13.5
______14: .word	1
______15: .word	89
_____16: .single	0r89.0
______17: .single	0r89.0
______18: .word	0
_____19: .single	0r45.78
______20: .single	0r45.78
______21: .word	356
______22: .word	1
_____23: .single	0r5.0
_____24: .single	0r5.0
______25: .word	0
_____26: .single	0r5.0
______27: .word	0
_____28: .single	0r5.0
______29: .word	0
______30: .word	0
_____31: .asciz "Globals "
.align	4
_____32: .asciz "\n"
.align	4
_____35: .asciz "\n"
.align	4
_____38: .asciz "\n"
.align	4
_____41: .asciz "\n"
.align	4
_____44: .asciz "\n"
.align	4
_____47: .asciz "\n"
.align	4
_____50: .asciz "\n"
.align	4
_____53: .asciz "\n"
.align	4
_____56: .asciz "\n"
.align	4
_____59: .asciz "\n"
.align	4
______60: .word	2
______61: .word	4
______62: .word	1
_____63: .single	0r7.8
______64: .single	0r7.8
_____65: .single	0r3.5
______66: .single	0r3.5
______67: .word	0
_____68: .single	0r7.8
______69: .single	0r7.8
_____70: .single	0r13.5
______71: .single	0r13.5
______72: .word	1
______73: .word	89
_____74: .single	0r89.0
______75: .single	0r89.0
______76: .word	0
_____77: .single	0r45.78
______78: .single	0r45.78
______79: .word	356
______80: .word	1
_____81: .single	0r5.0
_____82: .single	0r5.0
______83: .word	0
_____84: .single	0r5.0
______85: .word	0
_____86: .single	0r5.0
______87: .word	0
______88: .word	0
_____89: .asciz "Locals "
.align	4
_____90: .asciz "\n"
.align	4
_____93: .asciz "\n"
.align	4
_____96: .asciz "\n"
.align	4
_____99: .asciz "\n"
.align	4
_____102: .asciz "\n"
.align	4
_____105: .asciz "\n"
.align	4
_____108: .asciz "\n"
.align	4
_____111: .asciz "\n"
.align	4
_____114: .asciz "\n"
.align	4
_____117: .asciz "\n"
.align	4
______122: .word	0
_____127: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	4
!CodeGen::doVarDecl::b
b:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
!CodeGen::doVarDecl::a1
a1:	.skip	4
!CodeGen::doVarDecl::a2
a2:	.skip	4
!CodeGen::doVarDecl::c1
c1:	.skip	4
!CodeGen::doVarDecl::a3
a3:	.skip	4
!CodeGen::doVarDecl::a4
a4:	.skip	4
!CodeGen::doVarDecl::c2
c2:	.skip	4
!CodeGen::doVarDecl::a5
a5:	.skip	4
!CodeGen::doVarDecl::a6
a6:	.skip	4
!CodeGen::doVarDecl::c3
c3:	.skip	4
!CodeGen::doVarDecl::a7
a7:	.skip	4
!CodeGen::doVarDecl::a8
a8:	.skip	4
!CodeGen::doVarDecl::c4
c4:	.skip	4
!CodeGen::doVarDecl::c5
c5:	.skip	4
!CodeGen::doVarDecl::c6
c6:	.skip	4
!CodeGen::doVarDecl::c7
c7:	.skip	4
!CodeGen::doVarDecl::c8
c8:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	2,%l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	b,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______6,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	a1,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______8,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	a2,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	c1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______11,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	a3,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______13,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	a4,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	c2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	89,%l1
set	a5,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______17,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	a6,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	c3,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______20,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	a7,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	356,%l1
set	a8,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	c4,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	c5,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	c6,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	c7,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	c8,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____33 
 nop
set	_boolF,%o1
ba  _____34 
 nop
_____33:
set	_boolT,%o1
_____34:
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	c1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____36 
 nop
set	_boolF,%o1
ba  _____37 
 nop
_____36:
set	_boolT,%o1
_____37:
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	c2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____39 
 nop
set	_boolF,%o1
ba  _____40 
 nop
_____39:
set	_boolT,%o1
_____40:
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	c3,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____42 
 nop
set	_boolF,%o1
ba  _____43 
 nop
_____42:
set	_boolT,%o1
_____43:
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	c4,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____45 
 nop
set	_boolF,%o1
ba  _____46 
 nop
_____45:
set	_boolT,%o1
_____46:
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	c5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____48 
 nop
set	_boolF,%o1
ba  _____49 
 nop
_____48:
set	_boolT,%o1
_____49:
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	c6,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____51 
 nop
set	_boolF,%o1
ba  _____52 
 nop
_____51:
set	_boolT,%o1
_____52:
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	c7,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____54 
 nop
set	_boolF,%o1
ba  _____55 
 nop
_____54:
set	_boolT,%o1
_____55:
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	c8,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____57 
 nop
set	_boolF,%o1
ba  _____58 
 nop
_____57:
set	_boolT,%o1
_____58:
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	2,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______64,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______66,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______69,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______71,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	89,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______75,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-44,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______78,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	356,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____91 
 nop
set	_boolF,%o1
ba  _____92 
 nop
_____91:
set	_boolT,%o1
_____92:
call printf
nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____94 
 nop
set	_boolF,%o1
ba  _____95 
 nop
_____94:
set	_boolT,%o1
_____95:
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____97 
 nop
set	_boolF,%o1
ba  _____98 
 nop
_____97:
set	_boolT,%o1
_____98:
call printf
nop
set	_strFmt,%o0
set	_____99,%o1
call printf
nop
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____100 
 nop
set	_boolF,%o1
ba  _____101 
 nop
_____100:
set	_boolT,%o1
_____101:
call printf
nop
set	_strFmt,%o0
set	_____102,%o1
call printf
nop
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____103 
 nop
set	_boolF,%o1
ba  _____104 
 nop
_____103:
set	_boolT,%o1
_____104:
call printf
nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____106 
 nop
set	_boolF,%o1
ba  _____107 
 nop
_____106:
set	_boolT,%o1
_____107:
call printf
nop
set	_strFmt,%o0
set	_____108,%o1
call printf
nop
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____109 
 nop
set	_boolF,%o1
ba  _____110 
 nop
_____109:
set	_boolT,%o1
_____110:
call printf
nop
set	_strFmt,%o0
set	_____111,%o1
call printf
nop
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____112 
 nop
set	_boolF,%o1
ba  _____113 
 nop
_____112:
set	_boolT,%o1
_____113:
call printf
nop
set	_strFmt,%o0
set	_____114,%o1
call printf
nop
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____115 
 nop
set	_boolF,%o1
ba  _____116 
 nop
_____115:
set	_boolT,%o1
_____116:
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 76) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____123 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____124 
 nop
_____123:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____124:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____125 
 nop
ba  _____126 
 nop
_____125:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____127,%o1
call printf
nop
_____126:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

