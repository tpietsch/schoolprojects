.global	a, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______8: .word	3
______12: .word	4
______18: .word	12
_____24: .asciz "Index value of "
.align	4
_____25: .asciz " is outside legal range [0,3).\n"
.align	4
______30: .word	0
_____35: .asciz "Index value of "
.align	4
_____36: .asciz " is outside legal range [0,3).\n"
.align	4
_____42: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______44: .word	4
______50: .word	12
_____56: .asciz "Index value of "
.align	4
_____57: .asciz " is outside legal range [0,3).\n"
.align	4
______62: .word	0
_____67: .asciz "Index value of "
.align	4
_____68: .asciz " is outside legal range [0,3).\n"
.align	4
_____77: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______79: .word	1
______83: .word	0
______89: .word	3
_____92: .asciz "a: "
.align	4
______94: .word	4
______100: .word	12
_____106: .asciz "Index value of "
.align	4
_____107: .asciz " is outside legal range [0,3).\n"
.align	4
______112: .word	0
_____117: .asciz "Index value of "
.align	4
_____118: .asciz " is outside legal range [0,3).\n"
.align	4
_____121: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____122: .asciz "\n"
.align	4
_____123: .asciz "b: "
.align	4
______125: .word	4
______131: .word	12
_____137: .asciz "Index value of "
.align	4
_____138: .asciz " is outside legal range [0,3).\n"
.align	4
______143: .word	0
_____148: .asciz "Index value of "
.align	4
_____149: .asciz " is outside legal range [0,3).\n"
.align	4
_____152: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____153: .asciz "\n"
.align	4
______155: .word	1
______163: .word	0
_____168: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	12
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____3,_____4) 
_____3:
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	3,%l2
cmp	%l1, %l2
bl  _____9 
 nop
set	-20,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____10 
 nop
_____9:
set	1,%l3
set	-20,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____10:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____4 
 nop
set	4,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-40,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____20 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____21 
 nop
_____20:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____21:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____22 
 nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____23 
 nop
_____22:
_____23:
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____31 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____32 
 nop
_____31:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____32:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____33 
 nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____34 
 nop
_____33:
_____34:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	a, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-80,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____41 
 nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	1, %o0
call exit
nop
_____41:
st	%l1, [%l0]
set	4,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-92,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-96,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____52 
 nop
set	-96,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____53 
 nop
_____52:
set	1,%l3
set	-96,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____53:
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____54 
 nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____55 
 nop
_____54:
_____55:
set	0, %l0
set	-108,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____63 
 nop
set	-108,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____64 
 nop
_____63:
set	1,%l3
set	-108,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____64:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____65 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____66 
 nop
_____65:
_____66:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-124,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-128,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-132,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-140,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-144,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____76 
 nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
set	1, %o0
call exit
nop
_____76:
st	%l1, [%l0]
set	1,%l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-152,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-156,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____3 
 nop
_____4:
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____84,_____85) 
_____84:
set	0, %l0
set	-164,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	3,%l2
cmp	%l1, %l2
bl  _____90 
 nop
set	-164,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____91 
 nop
_____90:
set	1,%l3
set	-164,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____91:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____85 
 nop
set	_strFmt,%o0
set	_____92,%o1
call printf
nop
set	4,%l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-180,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-184,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-188,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____102 
 nop
set	-188,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____103 
 nop
_____102:
set	1,%l3
set	-188,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____103:
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____104 
 nop
set	_strFmt,%o0
set	_____106,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____107,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____105 
 nop
_____104:
_____105:
set	0, %l0
set	-200,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-208,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____113 
 nop
set	-200,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____114 
 nop
_____113:
set	1,%l3
set	-200,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____114:
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____115 
 nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____118,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____116 
 nop
_____115:
_____116:
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	a, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-212,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____120 
 nop
set	_strFmt,%o0
set	_____121,%o1
call printf
nop
set	1, %o0
call exit
nop
_____120:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____122,%o1
call printf
nop
set	_strFmt,%o0
set	_____123,%o1
call printf
nop
set	4,%l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-220,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	-220,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-224,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-228,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-236,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-236,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____133 
 nop
set	-228,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____134 
 nop
_____133:
set	1,%l3
set	-228,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____134:
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____135 
 nop
set	_strFmt,%o0
set	_____137,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____138,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____136 
 nop
_____135:
_____136:
set	0, %l0
set	-240,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-244,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-244,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-248,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-244,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____144 
 nop
set	-240,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____145 
 nop
_____144:
set	1,%l3
set	-240,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____145:
set	-240,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____146 
 nop
set	_strFmt,%o0
set	_____148,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____149,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____147 
 nop
_____146:
_____147:
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-252,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____151 
 nop
set	_strFmt,%o0
set	_____152,%o1
call printf
nop
set	1, %o0
call exit
nop
_____151:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____153,%o1
call printf
nop
set	1,%l1
set	-256,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-260,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-260,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-264,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-268,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-264,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____84 
 nop
_____85:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 268) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____164 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____165 
 nop
_____164:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____165:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____166 
 nop
ba  _____167 
 nop
_____166:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____168,%o1
call printf
nop
_____167:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

