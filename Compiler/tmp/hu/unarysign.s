.global	foo, __________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	0
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____8: .asciz "\n"
.align	4
_____10: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____11: .asciz "\n"
.align	4
_____12: .single	0r5.96
______13: .single	0r5.96
______14: .word	7
______16: .word	0
______18: .single	0r5.96
_____20: .single 0r-5.96
_____21: .asciz "\n"
.align	4
______23: .word	0
______25: .word	3
_____27: .asciz "-3"
.align	4
_____28: .asciz "\n"
.align	4
_____29: .single	0r4.5
______31: .word	0
______33: .single	0r4.5
_____35: .single 0r-4.5
_____36: .asciz "\n"
.align	4
_____37: .single 0r5.96
_____38: .asciz "\n"
.align	4
_____39: .single	0r78.9
______40: .single	0r78.9
______44: .word	0
______46: .single	0r5.96
______48: .single	0r-5.96
_____49: .asciz "\n"
.align	4
______51: .word	0
______53: .word	7
______55: .word	-7
_____56: .asciz "\n"
.align	4
______61: .word	0
_____66: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	______13,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	7,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______18,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-12,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-20,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	_____20,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	3,%l5
sub	%l4, %l5, %l5
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______33,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-36,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-44,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	_____35,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_____37,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	______40,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-48, %l1
add	%fp, %l1, %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	0,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______46,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-56,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-64,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	______48,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-68,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	0,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	7,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	7,%l5
sub	%l4, %l5, %l5
set	-80,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-7,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 84) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-8,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-16,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____9 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1, %o0
call exit
nop
_____9:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
ret
restore

SAVE.foo = -(92 + 16) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____62 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____63 
 nop
_____62:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____63:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____64 
 nop
ba  _____65 
 nop
_____64:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____66,%o1
call printf
nop
_____65:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

