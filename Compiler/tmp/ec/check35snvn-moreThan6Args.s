.global	foo, foo2, __________.MEMLEAK,main
.section	".data"
.align	4
_____4: .asciz "\n"
.align	4
_____7: .asciz "\n"
.align	4
_____8: .asciz "\n"
.align	4
_____9: .asciz "\n"
.align	4
_____10: .asciz "\n"
.align	4
_____13: .asciz "\n"
.align	4
_____14: .asciz "\n"
.align	4
_____15: .asciz "\n"
.align	4
_____16: .asciz "\n"
.align	4
_____20: .asciz "\n"
.align	4
_____24: .asciz "\n"
.align	4
______33: .word	0
______34: .word	1
_____37: .asciz "\n"
.align	4
_____41: .asciz "\n"
.align	4
_____42: .asciz "\n"
.align	4
_____43: .asciz "\n"
.align	4
_____44: .asciz "\n"
.align	4
_____45: .asciz "\n"
.align	4
_____48: .asciz "\n"
.align	4
_____49: .asciz "\n"
.align	4
_____52: .asciz "\n"
.align	4
_____53: .asciz "\n"
.align	4
_____54: .single	0r4.5
_____55: .single	0r5.6
_____56: .single	0r8.45
______59: .word	1
______60: .word	2
______61: .word	3
_____62: .single	0r4.1
______63: .single	0r4.1
_____64: .single	0r5.2
______65: .single	0r5.2
______66: .word	1
_____67: .single	0r7.3
______68: .single	0r7.3
______69: .word	1
______70: .word	9
______76: .word	0
_____81: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	5,%o0
set	1,%o1
set	_____54,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f3
set	_____55,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f4
set	7,%o2
set	0,%o3
set	3,%o4
set	9,%o5
set	_____56,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
st	%l1, [%sp + (92 - 4)]
add	%sp, -4, %sp
set	foo, %l1
call %l1
nop
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______63,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______65,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______68,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	9,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o2
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f3
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f4
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o3
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f5
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o4
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o5
add	%sp, -0, %sp
set	foo2, %l1
call %l1
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
st	%f3, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
st	%f4, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
st	%i2, [%l0]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-32,%l0
add 	%fp, %l0, %l0
st	%i3, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
st	%i4, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
st	%i5, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____5 
 nop
set	_boolF,%o1
ba  _____6 
 nop
_____5:
set	_boolT,%o1
_____6:
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____11 
 nop
set	_boolF,%o1
ba  _____12 
 nop
_____11:
set	_boolT,%o1
_____12:
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-56,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-68,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____25 
 nop
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____27 
 nop
ba  _____29 
 nop
ba  _____28 
 nop
_____27:
ba  _____28 
 nop
_____25:
_____28:
_____26:
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____32 
 nop
_____29:
set	1,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____32:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____35 
 nop
set	_boolF,%o1
ba  _____36 
 nop
_____35:
set	_boolT,%o1
_____36:
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
ret
restore

SAVE.foo = -(92 + 72) & -8

foo2: 
set SAVE.foo2, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
st	%i2, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
st	%f3, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
st	%f4, [%l0]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-32,%l0
add 	%fp, %l0, %l0
st	%i3, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
st	%f5, [%l0]
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-44,%l0
add 	%fp, %l0, %l0
st	%i4, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
st	%i5, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____46 
 nop
set	_boolF,%o1
ba  _____47 
 nop
_____46:
set	_boolT,%o1
_____47:
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____50 
 nop
set	_boolF,%o1
ba  _____51 
 nop
_____50:
set	_boolT,%o1
_____51:
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
ret
restore

SAVE.foo2 = -(92 + 48) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____77 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____78 
 nop
_____77:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____78:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____79 
 nop
ba  _____80 
 nop
_____79:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____81,%o1
call printf
nop
_____80:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

