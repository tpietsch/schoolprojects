.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Variable declarations"
.align	4
_____3: .asciz "\n"
.align	4
______4: .word	1
______5: .word	2
_____6: .single	0r1.1
______7: .single	0r1.1
_____8: .single	0r2.2
______9: .single	0r2.2
_____10: .single	0r1.0
______11: .single	0r1.0
______12: .word	1
______13: .word	0
_____14: .asciz "int i_1 = 1, i_2 = 2 "
.align	4
_____15: .asciz "\n"
.align	4
_____16: .asciz "float f_1_1 = 1.1, f_2_2 = 2.2, f_1point0 = 1.0 "
.align	4
_____17: .asciz "\n"
.align	4
_____18: .asciz "bool _true = true, _false = false "
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "Output format (expected, actual)"
.align	4
_____21: .asciz "\n"
.align	4
_____22: .asciz "Const test"
.align	4
_____23: .asciz "\n"
.align	4
_____24: .asciz "1 == 1 = (true, "
.align	4
_____25: .asciz "true"
.align	4
_____26: .asciz ")"
.align	4
_____27: .asciz "\n"
.align	4
_____28: .asciz "1 == 2 = (false, "
.align	4
_____29: .asciz "false"
.align	4
_____30: .asciz ")"
.align	4
_____31: .asciz "\n"
.align	4
_____32: .asciz "1 == 1.1 = (false, "
.align	4
_____33: .single	0r1.1
_____34: .asciz "false"
.align	4
_____35: .asciz ")"
.align	4
_____36: .asciz "\n"
.align	4
_____37: .asciz "1 == 1.0 = (true, "
.align	4
_____38: .single	0r1.0
_____39: .asciz "true"
.align	4
_____40: .asciz ")"
.align	4
_____41: .asciz "\n"
.align	4
_____42: .asciz "1.1 == 1 = (false, "
.align	4
_____43: .single	0r1.1
_____44: .asciz "false"
.align	4
_____45: .asciz ")"
.align	4
_____46: .asciz "\n"
.align	4
_____47: .asciz "true == false = (false, "
.align	4
_____48: .asciz "false"
.align	4
_____49: .asciz ")"
.align	4
_____50: .asciz "\n"
.align	4
_____51: .asciz "1 == 1.1 = (false, "
.align	4
_____52: .single	0r1.1
_____53: .asciz "false"
.align	4
_____54: .asciz ")"
.align	4
_____55: .asciz "\n"
.align	4
_____56: .asciz "Variable tests"
.align	4
_____57: .asciz "\n"
.align	4
_____58: .asciz "i_1 == 1 = (true, "
.align	4
______62: .word	1
_____67: .asciz ")"
.align	4
_____68: .asciz "\n"
.align	4
_____69: .asciz "i_1 == i_2 = (false, "
.align	4
_____77: .asciz ")"
.align	4
_____78: .asciz "\n"
.align	4
_____79: .asciz "2 == i_1 = (false, "
.align	4
______82: .word	2
_____88: .asciz ")"
.align	4
_____89: .asciz "\n"
.align	4
_____90: .asciz "1 == i_1 = (true, "
.align	4
______93: .word	1
_____99: .asciz ")"
.align	4
_____100: .asciz "\n"
.align	4
_____101: .asciz "f_1_1 == i_1 = (false, "
.align	4
_____109: .asciz ")"
.align	4
_____110: .asciz "\n"
.align	4
_____111: .asciz "i_1 == f_1_1 = (false, "
.align	4
_____119: .asciz ")"
.align	4
_____120: .asciz "\n"
.align	4
_____121: .asciz "f_1_1 == 1.1 = (true, "
.align	4
_____122: .single	0r1.1
______126: .single	0r1.1
_____131: .asciz ")"
.align	4
_____132: .asciz "\n"
.align	4
_____133: .asciz "f_1_1 == 2.2 = (false, "
.align	4
_____134: .single	0r2.2
______138: .single	0r2.2
_____143: .asciz ")"
.align	4
_____144: .asciz "\n"
.align	4
_____145: .asciz "f_1_1 == f_2_2 = (false, "
.align	4
_____153: .asciz ")"
.align	4
_____154: .asciz "\n"
.align	4
_____155: .asciz "1.1 == f_1_1 = (true, "
.align	4
_____156: .single	0r1.1
______159: .single	0r1.1
_____165: .asciz ")"
.align	4
_____166: .asciz "\n"
.align	4
_____167: .asciz "i_1 == i_1 = (true, "
.align	4
_____175: .asciz ")"
.align	4
_____176: .asciz "\n"
.align	4
_____177: .asciz "i_1 == 1.0 = (true, "
.align	4
_____178: .single	0r1.0
______182: .single	0r1.0
_____187: .asciz ")"
.align	4
_____188: .asciz "\n"
.align	4
_____189: .asciz "i_1 == f_1point0 = (true, "
.align	4
_____197: .asciz ")"
.align	4
_____198: .asciz "\n"
.align	4
_____199: .asciz "f_1point0 == i_1 = (true, "
.align	4
_____207: .asciz ")"
.align	4
_____208: .asciz "\n"
.align	4
_____209: .asciz "f_1_1 == f_1_1 = (true, "
.align	4
_____217: .asciz ")"
.align	4
_____218: .asciz "\n"
.align	4
_____219: .asciz "_true == _true = (true, "
.align	4
_____225: .asciz ")"
.align	4
_____226: .asciz "\n"
.align	4
_____227: .asciz "_true == true = (true, "
.align	4
_____233: .asciz ")"
.align	4
_____234: .asciz "\n"
.align	4
_____235: .asciz "true == _true = (true, "
.align	4
_____241: .asciz ")"
.align	4
_____242: .asciz "\n"
.align	4
_____243: .asciz "_true == false = (false, "
.align	4
_____249: .asciz ")"
.align	4
_____250: .asciz "\n"
.align	4
_____251: .asciz "_false == _true = (false, "
.align	4
_____257: .asciz ")"
.align	4
_____258: .asciz "\n"
.align	4
______263: .word	0
_____268: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______7,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______9,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______11,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
be  _____63 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____64 
 nop
_____63:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____64:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____65 
 nop
set	_boolF,%o1
ba  _____66 
 nop
_____65:
set	_boolT,%o1
_____66:
call printf
nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____73 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____74 
 nop
_____73:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____74:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____75 
 nop
set	_boolF,%o1
ba  _____76 
 nop
_____75:
set	_boolT,%o1
_____76:
call printf
nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	2,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____84 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____85 
 nop
_____84:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____85:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____86 
 nop
set	_boolF,%o1
ba  _____87 
 nop
_____86:
set	_boolT,%o1
_____87:
call printf
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____95 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____96 
 nop
_____95:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____96:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____97 
 nop
set	_boolF,%o1
ba  _____98 
 nop
_____97:
set	_boolT,%o1
_____98:
call printf
nop
set	_strFmt,%o0
set	_____99,%o1
call printf
nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	0, %l0
set	-80,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-88,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____105 
 nop
set	-80,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____106 
 nop
_____105:
set	1,%l3
set	-80,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____106:
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____107 
 nop
set	_boolF,%o1
ba  _____108 
 nop
_____107:
set	_boolT,%o1
_____108:
call printf
nop
set	_strFmt,%o0
set	_____109,%o1
call printf
nop
set	_strFmt,%o0
set	_____110,%o1
call printf
nop
set	_strFmt,%o0
set	_____111,%o1
call printf
nop
set	0, %l0
set	-92,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-100,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-96,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-96,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____115 
 nop
set	-92,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____116 
 nop
_____115:
set	1,%l3
set	-92,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____116:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____117 
 nop
set	_boolF,%o1
ba  _____118 
 nop
_____117:
set	_boolT,%o1
_____118:
call printf
nop
set	_strFmt,%o0
set	_____119,%o1
call printf
nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
set	_strFmt,%o0
set	_____121,%o1
call printf
nop
set	0, %l0
set	-104,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______126,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-112,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____127 
 nop
set	-104,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____128 
 nop
_____127:
set	1,%l3
set	-104,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____128:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____129 
 nop
set	_boolF,%o1
ba  _____130 
 nop
_____129:
set	_boolT,%o1
_____130:
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
set	_strFmt,%o0
set	_____132,%o1
call printf
nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	0, %l0
set	-116,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______138,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-124,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____139 
 nop
set	-116,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____140 
 nop
_____139:
set	1,%l3
set	-116,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____140:
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____141 
 nop
set	_boolF,%o1
ba  _____142 
 nop
_____141:
set	_boolT,%o1
_____142:
call printf
nop
set	_strFmt,%o0
set	_____143,%o1
call printf
nop
set	_strFmt,%o0
set	_____144,%o1
call printf
nop
set	_strFmt,%o0
set	_____145,%o1
call printf
nop
set	0, %l0
set	-128,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-136,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____149 
 nop
set	-128,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____150 
 nop
_____149:
set	1,%l3
set	-128,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____150:
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____151 
 nop
set	_boolF,%o1
ba  _____152 
 nop
_____151:
set	_boolT,%o1
_____152:
call printf
nop
set	_strFmt,%o0
set	_____153,%o1
call printf
nop
set	_strFmt,%o0
set	_____154,%o1
call printf
nop
set	_strFmt,%o0
set	_____155,%o1
call printf
nop
set	0, %l0
set	-140,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	______159,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-148,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____161 
 nop
set	-140,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____162 
 nop
_____161:
set	1,%l3
set	-140,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____162:
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____163 
 nop
set	_boolF,%o1
ba  _____164 
 nop
_____163:
set	_boolT,%o1
_____164:
call printf
nop
set	_strFmt,%o0
set	_____165,%o1
call printf
nop
set	_strFmt,%o0
set	_____166,%o1
call printf
nop
set	_strFmt,%o0
set	_____167,%o1
call printf
nop
set	0, %l0
set	-152,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____171 
 nop
set	-152,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____172 
 nop
_____171:
set	1,%l3
set	-152,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____172:
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____173 
 nop
set	_boolF,%o1
ba  _____174 
 nop
_____173:
set	_boolT,%o1
_____174:
call printf
nop
set	_strFmt,%o0
set	_____175,%o1
call printf
nop
set	_strFmt,%o0
set	_____176,%o1
call printf
nop
set	_strFmt,%o0
set	_____177,%o1
call printf
nop
set	0, %l0
set	-164,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______182,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-172,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-168,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-168,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____183 
 nop
set	-164,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____184 
 nop
_____183:
set	1,%l3
set	-164,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____184:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____185 
 nop
set	_boolF,%o1
ba  _____186 
 nop
_____185:
set	_boolT,%o1
_____186:
call printf
nop
set	_strFmt,%o0
set	_____187,%o1
call printf
nop
set	_strFmt,%o0
set	_____188,%o1
call printf
nop
set	_strFmt,%o0
set	_____189,%o1
call printf
nop
set	0, %l0
set	-176,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-184,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-180,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-180,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____193 
 nop
set	-176,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____194 
 nop
_____193:
set	1,%l3
set	-176,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____194:
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____195 
 nop
set	_boolF,%o1
ba  _____196 
 nop
_____195:
set	_boolT,%o1
_____196:
call printf
nop
set	_strFmt,%o0
set	_____197,%o1
call printf
nop
set	_strFmt,%o0
set	_____198,%o1
call printf
nop
set	_strFmt,%o0
set	_____199,%o1
call printf
nop
set	0, %l0
set	-188,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-192,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-192,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-196,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-196,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____203 
 nop
set	-188,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____204 
 nop
_____203:
set	1,%l3
set	-188,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____204:
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____205 
 nop
set	_boolF,%o1
ba  _____206 
 nop
_____205:
set	_boolT,%o1
_____206:
call printf
nop
set	_strFmt,%o0
set	_____207,%o1
call printf
nop
set	_strFmt,%o0
set	_____208,%o1
call printf
nop
set	_strFmt,%o0
set	_____209,%o1
call printf
nop
set	0, %l0
set	-200,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-204,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-204,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-208,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____213 
 nop
set	-200,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____214 
 nop
_____213:
set	1,%l3
set	-200,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____214:
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____215 
 nop
set	_boolF,%o1
ba  _____216 
 nop
_____215:
set	_boolT,%o1
_____216:
call printf
nop
set	_strFmt,%o0
set	_____217,%o1
call printf
nop
set	_strFmt,%o0
set	_____218,%o1
call printf
nop
set	_strFmt,%o0
set	_____219,%o1
call printf
nop
set	0, %l0
set	-212,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____221 
 nop
set	-212,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____222 
 nop
_____221:
set	1,%l3
set	-212,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____222:
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____223 
 nop
set	_boolF,%o1
ba  _____224 
 nop
_____223:
set	_boolT,%o1
_____224:
call printf
nop
set	_strFmt,%o0
set	_____225,%o1
call printf
nop
set	_strFmt,%o0
set	_____226,%o1
call printf
nop
set	_strFmt,%o0
set	_____227,%o1
call printf
nop
set	0, %l0
set	-216,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
be  _____229 
 nop
set	-216,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____230 
 nop
_____229:
set	1,%l3
set	-216,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____230:
set	-216,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____231 
 nop
set	_boolF,%o1
ba  _____232 
 nop
_____231:
set	_boolT,%o1
_____232:
call printf
nop
set	_strFmt,%o0
set	_____233,%o1
call printf
nop
set	_strFmt,%o0
set	_____234,%o1
call printf
nop
set	_strFmt,%o0
set	_____235,%o1
call printf
nop
set	0, %l0
set	-220,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____237 
 nop
set	-220,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____238 
 nop
_____237:
set	1,%l3
set	-220,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____238:
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____239 
 nop
set	_boolF,%o1
ba  _____240 
 nop
_____239:
set	_boolT,%o1
_____240:
call printf
nop
set	_strFmt,%o0
set	_____241,%o1
call printf
nop
set	_strFmt,%o0
set	_____242,%o1
call printf
nop
set	_strFmt,%o0
set	_____243,%o1
call printf
nop
set	0, %l0
set	-224,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____245 
 nop
set	-224,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____246 
 nop
_____245:
set	1,%l3
set	-224,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____246:
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____247 
 nop
set	_boolF,%o1
ba  _____248 
 nop
_____247:
set	_boolT,%o1
_____248:
call printf
nop
set	_strFmt,%o0
set	_____249,%o1
call printf
nop
set	_strFmt,%o0
set	_____250,%o1
call printf
nop
set	_strFmt,%o0
set	_____251,%o1
call printf
nop
set	0, %l0
set	-228,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____253 
 nop
set	-228,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____254 
 nop
_____253:
set	1,%l3
set	-228,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____254:
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____255 
 nop
set	_boolF,%o1
ba  _____256 
 nop
_____255:
set	_boolT,%o1
_____256:
call printf
nop
set	_strFmt,%o0
set	_____257,%o1
call printf
nop
set	_strFmt,%o0
set	_____258,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 228) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____264 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____265 
 nop
_____264:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____265:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____266 
 nop
ba  _____267 
 nop
_____266:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____268,%o1
call printf
nop
_____267:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

