.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	0
_____4: .asciz "Output format (actual, expected) "
.align	4
_____5: .asciz "\n"
.align	4
_____6: .asciz "false || false = (false, "
.align	4
_____15: .asciz "false"
.align	4
_____16: .asciz ")"
.align	4
_____17: .asciz "\n"
.align	4
_____18: .asciz "false || true = (true, "
.align	4
_____27: .asciz "true"
.align	4
_____28: .asciz ")"
.align	4
_____29: .asciz "\n"
.align	4
_____30: .asciz "true || false = (true, "
.align	4
_____39: .asciz "true"
.align	4
_____40: .asciz ")"
.align	4
_____41: .asciz "\n"
.align	4
_____42: .asciz "true || true = (true, "
.align	4
_____51: .asciz "true"
.align	4
_____52: .asciz ")"
.align	4
_____53: .asciz "\n"
.align	4
_____54: .asciz "false || _false = (false, "
.align	4
_____63: .asciz "false"
.align	4
_____64: .asciz ")"
.align	4
_____65: .asciz "\n"
.align	4
_____66: .asciz "_false || true = (true, "
.align	4
_____75: .asciz "true"
.align	4
_____76: .asciz ")"
.align	4
_____77: .asciz "\n"
.align	4
_____78: .asciz "_true || _false = (true, "
.align	4
_____87: .asciz "true"
.align	4
_____88: .asciz ")"
.align	4
_____89: .asciz "\n"
.align	4
_____90: .asciz "_true || _true = (true, "
.align	4
_____99: .asciz "true"
.align	4
_____100: .asciz ")"
.align	4
_____101: .asciz "\n"
.align	4
______106: .word	0
_____111: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____7 
 nop
ba  _____9 
 nop
ba  _____8 
 nop
_____7:
_____8:
set	0,%l1
cmp	%l1, %g0
be  _____10 
 nop
ba  _____9 
 nop
ba  _____11 
 nop
_____10:
_____11:
ba  _____14 
 nop
_____9:
_____14:
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____19 
 nop
ba  _____21 
 nop
ba  _____20 
 nop
_____19:
_____20:
set	1,%l1
cmp	%l1, %g0
be  _____22 
 nop
ba  _____21 
 nop
ba  _____23 
 nop
_____22:
_____23:
ba  _____26 
 nop
_____21:
_____26:
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____31 
 nop
ba  _____33 
 nop
ba  _____32 
 nop
_____31:
_____32:
set	0,%l1
cmp	%l1, %g0
be  _____34 
 nop
ba  _____33 
 nop
ba  _____35 
 nop
_____34:
_____35:
ba  _____38 
 nop
_____33:
_____38:
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____43 
 nop
ba  _____45 
 nop
ba  _____44 
 nop
_____43:
_____44:
set	1,%l1
cmp	%l1, %g0
be  _____46 
 nop
ba  _____45 
 nop
ba  _____47 
 nop
_____46:
_____47:
ba  _____50 
 nop
_____45:
_____50:
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____55 
 nop
ba  _____57 
 nop
ba  _____56 
 nop
_____55:
_____56:
set	0,%l1
cmp	%l1, %g0
be  _____58 
 nop
ba  _____57 
 nop
ba  _____59 
 nop
_____58:
_____59:
ba  _____62 
 nop
_____57:
_____62:
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	_strFmt,%o0
set	_____66,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____67 
 nop
ba  _____69 
 nop
ba  _____68 
 nop
_____67:
_____68:
set	1,%l1
cmp	%l1, %g0
be  _____70 
 nop
ba  _____69 
 nop
ba  _____71 
 nop
_____70:
_____71:
ba  _____74 
 nop
_____69:
_____74:
set	_strFmt,%o0
set	_____75,%o1
call printf
nop
set	_strFmt,%o0
set	_____76,%o1
call printf
nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____79 
 nop
ba  _____81 
 nop
ba  _____80 
 nop
_____79:
_____80:
set	0,%l1
cmp	%l1, %g0
be  _____82 
 nop
ba  _____81 
 nop
ba  _____83 
 nop
_____82:
_____83:
ba  _____86 
 nop
_____81:
_____86:
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____91 
 nop
ba  _____93 
 nop
ba  _____92 
 nop
_____91:
_____92:
set	1,%l1
cmp	%l1, %g0
be  _____94 
 nop
ba  _____93 
 nop
ba  _____95 
 nop
_____94:
_____95:
ba  _____98 
 nop
_____93:
_____98:
set	_strFmt,%o0
set	_____99,%o1
call printf
nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 8) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____107 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____108 
 nop
_____107:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____108:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____109 
 nop
ba  _____110 
 nop
_____109:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____111,%o1
call printf
nop
_____110:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

