.global	x, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	7
_____3: .asciz "\n"
.align	4
______5: .word	0
_____8: .asciz "\n"
.align	4
______10: .word	0
______14: .word	0
_____17: .asciz "\n"
.align	4
_____18: .single	0r2.34
______19: .single	0r2.34
_____20: .asciz "\n"
.align	4
______22: .word	0
_____25: .asciz "\n"
.align	4
______27: .word	0
______31: .word	0
_____34: .asciz "\n"
.align	4
______39: .word	0
_____44: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
x:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	7,%l1
set	x,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-12,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-24,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-36,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	______19,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	0,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-44,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-52,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	0,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-56,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-64,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	0,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-68,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-68,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-76,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 76) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____40 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____41 
 nop
_____40:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____41:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____42 
 nop
ba  _____43 
 nop
_____42:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
_____43:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

