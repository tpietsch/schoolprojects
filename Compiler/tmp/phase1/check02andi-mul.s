.global	i, j, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	5
_____3: .single	0r7.7
______4: .single	0r7.7
______7: .word	6
_____8: .single	0r6.6
______11: .single	0r6.6
______12: .word	5
_____13: .single	0r5.5
______14: .single	0r5.5
_____15: .asciz "3 * 3 = "
.align	4
_____16: .asciz "9"
.align	4
_____17: .asciz "\n"
.align	4
_____18: .asciz "i * k = "
.align	4
_____22: .asciz "\n"
.align	4
_____23: .asciz "i * m = "
.align	4
_____27: .asciz "\n"
.align	4
_____28: .asciz "k * m = "
.align	4
_____32: .asciz "\n"
.align	4
_____33: .asciz "\n"
.align	4
_____34: .asciz "3.0 * 3.0 = "
.align	4
_____35: .single	0r3.0
_____36: .single	0r3.0
_____37: .single 0r9.0
_____38: .asciz "\n"
.align	4
_____39: .asciz "3.01 * 3.3201 = "
.align	4
_____40: .single	0r3.01
_____41: .single	0r3.3201
_____42: .single 0r9.993501
_____43: .asciz "\n"
.align	4
_____44: .asciz "j * l = "
.align	4
_____48: .asciz "\n"
.align	4
_____49: .asciz "j * n = "
.align	4
_____53: .asciz "\n"
.align	4
_____54: .asciz "l * n = "
.align	4
_____58: .asciz "\n"
.align	4
_____59: .asciz "\n"
.align	4
_____60: .asciz "3.0 * 3 = "
.align	4
_____61: .single	0r3.0
_____62: .single 0r9.0
_____63: .asciz "\n"
.align	4
_____64: .asciz "j * m = "
.align	4
_____68: .asciz "\n"
.align	4
_____69: .asciz "l * k = "
.align	4
_____73: .asciz "\n"
.align	4
_____74: .asciz "n * i = "
.align	4
_____78: .asciz "\n"
.align	4
_____79: .asciz "\n"
.align	4
_____80: .asciz "3 * 3.0 = "
.align	4
_____81: .single	0r3.0
_____82: .single 0r9.0
_____83: .asciz "\n"
.align	4
_____84: .asciz "m * j = "
.align	4
_____88: .asciz "\n"
.align	4
_____89: .asciz "k * l = "
.align	4
_____93: .asciz "\n"
.align	4
_____94: .asciz "i * n = "
.align	4
_____98: .asciz "\n"
.align	4
______103: .word	0
_____108: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::i
i:	.skip	4
!CodeGen::doVarDecl::j
j:	.skip	4
!CodeGen::doVarDecl::k
main__0k25staticFlag:	.skip	4
main__0k25:	.skip	4
!CodeGen::doVarDecl::l
main__0l29staticFlag:	.skip	4
main__0l29:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	5,%l1
set	i,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______4,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	j,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
_init_done:
set	main__0k25staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	6,%l1
set	main__0k25,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	main__0k25staticFlag, %l1
st	%l0, [%l1]
_____6:
set	main__0l29staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____10 
 nop
set	______11,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	main__0l29,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	1, %l0
set	main__0l29staticFlag, %l1
st	%l0, [%l1]
_____10:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______14,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	i,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	i,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0k25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-20,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	i,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	i,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-32,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	main__0k25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0k25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-44,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	_____37,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	_____42,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	j,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	j,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	main__0l29,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-56,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	j,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	j,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-68,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	main__0l29,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	main__0l29,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-80,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	_____62,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	j,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	j,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-88,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-84,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-92,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
set	main__0l29,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	main__0l29,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	main__0k25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-100,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-96,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-100,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-104,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	i,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-112,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-112,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-108,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-112,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-116,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
set	_strFmt,%o0
set	_____80,%o1
call printf
nop
set	_____82,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	j,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-124,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-120,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-120,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-120,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-124,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-128,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	main__0k25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0k25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0l29,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-136,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-132,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-132,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-140,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
set	i,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	i,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-148,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-144,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-144,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-144,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-148,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-152,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 152) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____104 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____105 
 nop
_____104:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____105:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____106 
 nop
ba  _____107 
 nop
_____106:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____108,%o1
call printf
nop
_____107:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

