.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	5
______3: .word	10
_____6: .asciz "6 > 7 not true"
.align	4
_____7: .asciz "\n"
.align	4
_____10: .asciz "7 > 6 true"
.align	4
_____11: .asciz "\n"
.align	4
______15: .word	6
_____20: .asciz "ia > 6 not true"
.align	4
_____21: .asciz "\n"
.align	4
______24: .word	6
_____30: .asciz "6 > ia true"
.align	4
_____31: .asciz "\n"
.align	4
______34: .word	4
_____40: .asciz "4 > ia not true"
.align	4
_____41: .asciz "\n"
.align	4
______45: .word	4
_____50: .asciz "ia > 4 true"
.align	4
_____51: .asciz "\n"
.align	4
_____59: .asciz "ia > ib not true"
.align	4
_____60: .asciz "\n"
.align	4
_____68: .asciz "ib > ia true"
.align	4
_____69: .asciz "\n"
.align	4
_____72: .asciz "true"
.align	4
_____73: .asciz "\n"
.align	4
_____76: .asciz "false"
.align	4
_____77: .asciz "\n"
.align	4
______82: .word	0
_____87: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
cmp	%l1, %g0
be  _____4 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
ba  _____5 
 nop
_____4:
_____5:
set	1,%l1
cmp	%l1, %g0
be  _____8 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
ba  _____9 
 nop
_____8:
_____9:
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	6,%l2
cmp	%l1, %l2
bg  _____16 
 nop
set	-12,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____17 
 nop
_____16:
set	1,%l3
set	-12,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____17:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____18 
 nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
ba  _____19 
 nop
_____18:
_____19:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	6,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____26 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____27 
 nop
_____26:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____27:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____28 
 nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
ba  _____29 
 nop
_____28:
_____29:
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	4,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____36 
 nop
set	-36,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____37 
 nop
_____36:
set	1,%l3
set	-36,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____37:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____38 
 nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
ba  _____39 
 nop
_____38:
_____39:
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	4,%l2
cmp	%l1, %l2
bg  _____46 
 nop
set	-48,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____47 
 nop
_____46:
set	1,%l3
set	-48,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____47:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____48 
 nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
ba  _____49 
 nop
_____48:
_____49:
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____55 
 nop
set	-60,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____56 
 nop
_____55:
set	1,%l3
set	-60,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____56:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____57 
 nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
ba  _____58 
 nop
_____57:
_____58:
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____64 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____65 
 nop
_____64:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____65:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____66 
 nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
set	_strFmt,%o0
set	_____69,%o1
call printf
nop
ba  _____67 
 nop
_____66:
_____67:
set	1,%l1
cmp	%l1, %g0
be  _____70 
 nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
ba  _____71 
 nop
_____70:
_____71:
set	0,%l1
cmp	%l1, %g0
be  _____74 
 nop
set	_strFmt,%o0
set	_____76,%o1
call printf
nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
ba  _____75 
 nop
_____74:
_____75:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 80) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____83 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____84 
 nop
_____83:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____84:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____85 
 nop
ba  _____86 
 nop
_____85:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
_____86:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

