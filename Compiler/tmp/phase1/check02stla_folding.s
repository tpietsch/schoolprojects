.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	6
_____4: .asciz "d is 6, "
.align	4
_____5: .asciz "\n"
.align	4
_____6: .single	0r1.2
_____7: .single	0r3.4
_____8: .single	0r5.6
______9: .single	0r10.200001
_____10: .asciz "e is 10.20, "
.align	4
_____11: .asciz "\n"
.align	4
______12: .word	6
______13: .word	7
_____14: .asciz "g is 7, "
.align	4
_____15: .asciz "\n"
.align	4
______16: .word	7
_____17: .asciz "h is 7, "
.align	4
_____18: .asciz "7"
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "c + 12.3 = 13.30, "
.align	4
_____21: .single	0r12.3
_____22: .single 0r13.3
_____23: .asciz "\n"
.align	4
_____24: .asciz "e + 100.2 = 110.50, "
.align	4
_____25: .single	0r100.3
______28: .single	0r100.3
_____30: .asciz "\n"
.align	4
______31: .word	1
______32: .word	0
______41: .word	0
_____42: .asciz "t3 is false, "
.align	4
_____45: .asciz "\n"
.align	4
______50: .word	0
_____55: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	______9,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	6,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	7,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	7,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_____22,%l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______28,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-36,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
cmp	%l1, %g0
be  _____33 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____35 
 nop
ba  _____37 
 nop
ba  _____36 
 nop
_____35:
ba  _____36 
 nop
_____33:
_____36:
_____34:
ba  _____40 
 nop
_____37:
_____40:
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____43 
 nop
set	_boolF,%o1
ba  _____44 
 nop
_____43:
set	_boolT,%o1
_____44:
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 48) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____51 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____52 
 nop
_____51:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____52:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____53 
 nop
ba  _____54 
 nop
_____53:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
_____54:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

