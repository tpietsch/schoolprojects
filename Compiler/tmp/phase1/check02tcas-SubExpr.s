.global	x, y, z, c, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	4
______3: .word	5
______6: .word	5
______10: .word	5
_____15: .asciz "\n"
.align	4
______20: .word	0
_____25: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
x:	.skip	4
!CodeGen::doVarDecl::y
y:	.skip	4
!CodeGen::doVarDecl::z
z:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	4,%l1
set	x,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	z,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	5,%l5
sub	%l4, %l5, %l5
set	-12,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	z,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	z,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	5,%l5
sub	%l4, %l5, %l5
set	-24,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-36,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	x,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 36) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____21 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____22 
 nop
_____21:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____22:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____23 
 nop
ba  _____24 
 nop
_____23:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
_____24:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

