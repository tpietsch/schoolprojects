.global	_0fGlobalUnit1, _0fGlobalInit1, _0fGlobalInit21, _0fGlobalConst1, _0fGlobalConst21,main
.section	".data"
.align	4
______0: .single	0r2.0

______1: .single	0r3.0

______2: .single	0r3.0

______3: .single	0r3.0

______4: .single	0r4.0

______5: .single	0r5.0

_____6: .asciz "5.00"
.align	4
______7: .single	0r3.0

_____8: .asciz "3.00"
.align	4
______9: .single	0r3.0

_____10: .asciz "3.00"
.align	4
_____11: .asciz "6.00"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::fGlobalUnit
_0fGlobalUnit1:	.skip	4
!CodeGen::doVarDecl::fGlobalInit
_0fGlobalInit1:	.skip	4
!CodeGen::doVarDecl::fGlobalInit2
_0fGlobalInit21:	.skip	4
!CodeGen::doVarDecl::fGlobalConst
_0fGlobalConst1:	.skip	4
!CodeGen::doVarDecl::fGlobalConst2
_0fGlobalConst21:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld[%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
! Doing a vardeclass 
! Generating STO 
! declaring a variable 2.00 
! Doing a vardeclass 
! Generating STO 
! doConst ( 2.00,______0 ) 
! sto  2.00 (______0) 
! STO's name is( 2.00, %g0,______0 ) 
set	______0,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fGlobalInit, %g0,_0fGlobalInit1 ) 
set	_0fGlobalInit1,%i1
add 	%g0, %i1, %i1
st	%f1, [%i1]
! declaring a variable fGlobalInit 
! Doing a vardeclass 
! Generating STO 
! STO's name is( fGlobalInit, %g0,_0fGlobalInit1 ) 
set	_0fGlobalInit1,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fGlobalInit2, %g0,_0fGlobalInit21 ) 
set	_0fGlobalInit21,%i1
add 	%g0, %i1, %i1
st	%f1, [%i1]
! declaring a variable 3.00 
! Doing a vardeclass 
! Generating STO 
! doConst ( 3.00,______1 ) 
! sto  3.00 (______1) 
! STO's name is( 3.00, %g0,______1 ) 
set	______1,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fGlobalConst, %g0,_0fGlobalConst1 ) 
set	_0fGlobalConst1,%i1
add 	%g0, %i1, %i1
st	%f1, [%i1]
! declaring a variable fGlobalConst 
! Doing a vardeclass 
! Generating STO 
! doConst ( fGlobalConst,______2 ) 
! sto  fGlobalConst (______2) 
! STO's name is( fGlobalConst, %g0,______2 ) 
set	______2,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fGlobalConst2, %g0,_0fGlobalConst21 ) 
set	_0fGlobalConst21,%i1
add 	%g0, %i1, %i1
st	%f1, [%i1]
_init_done:
! handleFuncParams()  
! handleFuncParams() end 
! Doing a vardeclass 
! Generating STO 
! Doing a vardeclass 
! Generating STO 
! doConst ( 3.00,______3 ) 
! sto  3.00 (______3) 
! STO's name is( 3.00, %g0,______3 ) 
set	______3,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fLocalUnit, %fp,-4 ) 
set	-4,%i1
add 	%fp, %i1, %i1
st	%f1, [%i1]
! STO's name is( fLocalUnit, %fp,-4 ) 
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
! STO's name is( fLocalUnit, %fp,-4 ) 
set	-4,%i0
add 	%fp, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fLocalUnit2, %fp,-8 ) 
set	-8,%i1
add 	%fp, %i1, %i1
st	%f1, [%i1]
! STO's name is( fLocalUnit2, %fp,-8 ) 
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
! declaring a variable 4.00 
! Doing a vardeclass 
! Generating STO 
! doConst ( 4.00,______4 ) 
! sto  4.00 (______4) 
! STO's name is( 4.00, %g0,______4 ) 
set	______4,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fLocalInit, %fp,-12 ) 
set	-12,%i1
add 	%fp, %i1, %i1
st	%f1, [%i1]
! STO's name is( fLocalInit, %fp,-12 ) 
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
! declaring a variable fLocalInit 
! Doing a vardeclass 
! Generating STO 
! STO's name is( fLocalInit, %fp,-12 ) 
set	-12,%i0
add 	%fp, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fLocalInit2, %fp,-16 ) 
set	-16,%i1
add 	%fp, %i1, %i1
st	%f1, [%i1]
! STO's name is( fLocalInit2, %fp,-16 ) 
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
! declaring a variable fGlobalInit 
! Doing a vardeclass 
! Generating STO 
! STO's name is( fGlobalInit, %g0,_0fGlobalInit1 ) 
set	_0fGlobalInit1,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fLocalInit3, %fp,-20 ) 
set	-20,%i1
add 	%fp, %i1, %i1
st	%f1, [%i1]
! STO's name is( fLocalInit3, %fp,-20 ) 
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
! declaring a variable 5.00 
! Doing a vardeclass 
! Generating STO 
! doConst ( 5.00,______5 ) 
! sto  5.00 (______5) 
! STO's name is( 5.00, %g0,______5 ) 
set	______5,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fLocalConst, %fp,-24 ) 
set	-24,%i1
add 	%fp, %i1, %i1
st	%f1, [%i1]
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
! declaring a variable fGlobalConst 
! Doing a vardeclass 
! Generating STO 
! doConst ( fGlobalConst,______7 ) 
! sto  fGlobalConst (______7) 
! STO's name is( fGlobalConst, %g0,______7 ) 
set	______7,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fLocalConst2, %fp,-28 ) 
set	-28,%i1
add 	%fp, %i1, %i1
st	%f1, [%i1]
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
! declaring a variable fGlobalConst 
! Doing a vardeclass 
! Generating STO 
! doConst ( fGlobalConst,______9 ) 
! sto  fGlobalConst (______9) 
! STO's name is( fGlobalConst, %g0,______9 ) 
set	______9,%i0
add 	%g0, %i0, %i0
ld	[%i0], %f1
! STO's store name is( fLocalConst3, %fp,-32 ) 
set	-32,%i1
add 	%fp, %i1, %i1
st	%f1, [%i1]
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
ret
restore

SAVE.main = -(92 + 32) & -8

