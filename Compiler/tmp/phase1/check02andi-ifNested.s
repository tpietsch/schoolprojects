.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____4: .asciz "pass1"
.align	4
_____5: .asciz "\n"
.align	4
_____8: .asciz "fail1"
.align	4
_____9: .asciz "\n"
.align	4
_____10: .asciz "pass2"
.align	4
_____11: .asciz "\n"
.align	4
_____14: .asciz "pass3"
.align	4
_____15: .asciz "\n"
.align	4
_____18: .asciz "pass4"
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "pass5"
.align	4
_____21: .asciz "\n"
.align	4
______26: .word	0
_____31: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	1,%l1
cmp	%l1, %g0
be  _____2 
 nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____6 
 nop
set	_strFmt,%o0
set	_____8,%o1
call printf
nop
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
ba  _____7 
 nop
_____6:
_____7:
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____12 
 nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	1,%l1
cmp	%l1, %g0
be  _____16 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
ba  _____17 
 nop
_____16:
_____17:
ba  _____13 
 nop
_____12:
_____13:
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
ba  _____3 
 nop
_____2:
_____3:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 0) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____27 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____28 
 nop
_____27:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____28:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____29 
 nop
ba  _____30 
 nop
_____29:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
_____30:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

