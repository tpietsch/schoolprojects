.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Output format (expected, actual)"
.align	4
_____3: .asciz "\n"
.align	4
______4: .word	1
______5: .word	2
______6: .word	3
_____7: .single	0r1.1
______8: .single	0r1.1
_____9: .single	0r2.2
______10: .single	0r2.2
_____11: .single	0r3.3
______12: .single	0r3.3
_____13: .asciz "Variable declarations: "
.align	4
_____14: .asciz "\n"
.align	4
_____15: .asciz "int i1 = 1, i2 = 2, i3 = 3 "
.align	4
_____16: .asciz "\n"
.align	4
_____17: .asciz "float f1 = 1.1, f2 = 2.2, f3 = 3.3 "
.align	4
_____18: .asciz "\n"
.align	4
_____19: .asciz "Const test"
.align	4
_____20: .asciz "\n"
.align	4
_____21: .asciz "1 <= 1 = (true , "
.align	4
_____22: .asciz "true"
.align	4
_____23: .asciz ")"
.align	4
_____24: .asciz "\n"
.align	4
_____25: .asciz "1 <= 2 = (true,  "
.align	4
_____26: .asciz "true"
.align	4
_____27: .asciz ")"
.align	4
_____28: .asciz "\n"
.align	4
_____29: .asciz "2 <= 1 = (false, "
.align	4
_____30: .asciz "false"
.align	4
_____31: .asciz ")"
.align	4
_____32: .asciz "\n"
.align	4
_____33: .asciz "1.1 <= 1.1 = (true, "
.align	4
_____34: .single	0r1.1
_____35: .single	0r1.1
_____36: .asciz "true"
.align	4
_____37: .asciz ")"
.align	4
_____38: .asciz "\n"
.align	4
_____39: .asciz "1.1 <= 2.1 = (true, "
.align	4
_____40: .single	0r1.1
_____41: .single	0r2.1
_____42: .asciz "true"
.align	4
_____43: .asciz ")"
.align	4
_____44: .asciz "\n"
.align	4
_____45: .asciz "2.1 <= 1.1 = (false, "
.align	4
_____46: .single	0r2.1
_____47: .single	0r1.1
_____48: .asciz "false"
.align	4
_____49: .asciz ")"
.align	4
_____50: .asciz "\n"
.align	4
_____51: .asciz "Variables tests"
.align	4
_____52: .asciz "\n"
.align	4
_____53: .asciz "i1 <= 1 = (true , "
.align	4
______57: .word	1
_____62: .asciz ")"
.align	4
_____63: .asciz "\n"
.align	4
_____64: .asciz "1 <= i2 = (true,  "
.align	4
______67: .word	1
_____73: .asciz ")"
.align	4
_____74: .asciz "\n"
.align	4
_____75: .asciz "i2 <= i1 = (false, "
.align	4
_____83: .asciz ")"
.align	4
_____84: .asciz "\n"
.align	4
_____85: .asciz "f1 <= 1.1 = (true, "
.align	4
_____86: .single	0r1.1
______90: .single	0r1.1
_____95: .asciz ")"
.align	4
_____96: .asciz "\n"
.align	4
_____97: .asciz "1.1 <= f2 = (true, "
.align	4
_____98: .single	0r1.1
______101: .single	0r1.1
_____107: .asciz ")"
.align	4
_____108: .asciz "\n"
.align	4
_____109: .asciz "f2 <= f1 = (false, "
.align	4
_____117: .asciz ")"
.align	4
_____118: .asciz "\n"
.align	4
_____119: .asciz "Type promotions & mismatches"
.align	4
_____120: .asciz "\n"
.align	4
_____121: .asciz "i1 <= f1 = (true, "
.align	4
_____129: .asciz ")"
.align	4
_____130: .asciz "\n"
.align	4
_____131: .asciz "f1 <= i1 = (false, "
.align	4
_____139: .asciz ")"
.align	4
_____140: .asciz "\n"
.align	4
_____141: .asciz "i1 <= 1 = (true, "
.align	4
______145: .word	1
_____150: .asciz ")"
.align	4
_____151: .asciz "\n"
.align	4
_____152: .asciz "1 <= f1 = (true, "
.align	4
______155: .word	1
_____161: .asciz ")"
.align	4
_____162: .asciz "\n"
.align	4
______167: .word	0
_____172: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______8,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______10,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______12,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
ble  _____58 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____59 
 nop
_____58:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____59:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____60 
 nop
set	_boolF,%o1
ba  _____61 
 nop
_____60:
set	_boolT,%o1
_____61:
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____69 
 nop
set	-40,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____70 
 nop
_____69:
set	1,%l3
set	-40,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____70:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____71 
 nop
set	_boolF,%o1
ba  _____72 
 nop
_____71:
set	_boolT,%o1
_____72:
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
set	_strFmt,%o0
set	_____75,%o1
call printf
nop
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____79 
 nop
set	-52,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____80 
 nop
_____79:
set	1,%l3
set	-52,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____80:
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____81 
 nop
set	_boolF,%o1
ba  _____82 
 nop
_____81:
set	_boolT,%o1
_____82:
call printf
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-68,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-68,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______90,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____91 
 nop
set	-64,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____92 
 nop
_____91:
set	1,%l3
set	-64,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____92:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____93 
 nop
set	_boolF,%o1
ba  _____94 
 nop
_____93:
set	_boolT,%o1
_____94:
call printf
nop
set	_strFmt,%o0
set	_____95,%o1
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	______101,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____103 
 nop
set	-76,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____104 
 nop
_____103:
set	1,%l3
set	-76,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____104:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____105 
 nop
set	_boolF,%o1
ba  _____106 
 nop
_____105:
set	_boolT,%o1
_____106:
call printf
nop
set	_strFmt,%o0
set	_____107,%o1
call printf
nop
set	_strFmt,%o0
set	_____108,%o1
call printf
nop
set	_strFmt,%o0
set	_____109,%o1
call printf
nop
set	0, %l0
set	-88,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-96,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____113 
 nop
set	-88,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____114 
 nop
_____113:
set	1,%l3
set	-88,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____114:
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____115 
 nop
set	_boolF,%o1
ba  _____116 
 nop
_____115:
set	_boolT,%o1
_____116:
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	_strFmt,%o0
set	_____118,%o1
call printf
nop
set	_strFmt,%o0
set	_____119,%o1
call printf
nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
set	_strFmt,%o0
set	_____121,%o1
call printf
nop
set	0, %l0
set	-100,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-104,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-104,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____125 
 nop
set	-100,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____126 
 nop
_____125:
set	1,%l3
set	-100,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____126:
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____127 
 nop
set	_boolF,%o1
ba  _____128 
 nop
_____127:
set	_boolT,%o1
_____128:
call printf
nop
set	_strFmt,%o0
set	_____129,%o1
call printf
nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-116,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-116,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-120,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____135 
 nop
set	-112,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____136 
 nop
_____135:
set	1,%l3
set	-112,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____136:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____137 
 nop
set	_boolF,%o1
ba  _____138 
 nop
_____137:
set	_boolT,%o1
_____138:
call printf
nop
set	_strFmt,%o0
set	_____139,%o1
call printf
nop
set	_strFmt,%o0
set	_____140,%o1
call printf
nop
set	_strFmt,%o0
set	_____141,%o1
call printf
nop
set	0, %l0
set	-124,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
ble  _____146 
 nop
set	-124,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____147 
 nop
_____146:
set	1,%l3
set	-124,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____147:
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____148 
 nop
set	_boolF,%o1
ba  _____149 
 nop
_____148:
set	_boolT,%o1
_____149:
call printf
nop
set	_strFmt,%o0
set	_____150,%o1
call printf
nop
set	_strFmt,%o0
set	_____151,%o1
call printf
nop
set	_strFmt,%o0
set	_____152,%o1
call printf
nop
set	0, %l0
set	-136,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-144,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-140,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____157 
 nop
set	-136,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____158 
 nop
_____157:
set	1,%l3
set	-136,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____158:
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____159 
 nop
set	_boolF,%o1
ba  _____160 
 nop
_____159:
set	_boolT,%o1
_____160:
call printf
nop
set	_strFmt,%o0
set	_____161,%o1
call printf
nop
set	_strFmt,%o0
set	_____162,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 144) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____168 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____169 
 nop
_____168:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____169:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____170 
 nop
ba  _____171 
 nop
_____170:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____172,%o1
call printf
nop
_____171:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

