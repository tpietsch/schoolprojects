.global	a, b, c, d, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______3: .word	1
______4: .word	1
______5: .word	0
______6: .word	1
______8: .word	0
_____9: .asciz "y: "
.align	4
_____12: .asciz "\n"
.align	4
______14: .word	0
_____15: .asciz "y: "
.align	4
_____18: .asciz "\n"
.align	4
______20: .word	1
_____21: .asciz "x: "
.align	4
_____24: .asciz "\n"
.align	4
_____26: .asciz "x: "
.align	4
_____29: .asciz "\n"
.align	4
_____31: .asciz "a: "
.align	4
_____34: .asciz "\n"
.align	4
_____36: .asciz "a: "
.align	4
_____39: .asciz "\n"
.align	4
______44: .word	0
_____49: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	4
!CodeGen::doVarDecl::b
b:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
!CodeGen::doVarDecl::d
d:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	0,%l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	b,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	d,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	1,%l5
xor	%l4, %l5, %l5
set	-8,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____10 
 nop
set	_boolF,%o1
ba  _____11 
 nop
_____10:
set	_boolT,%o1
_____11:
call printf
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	1,%l5
xor	%l4, %l5, %l5
set	-16,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____16 
 nop
set	_boolF,%o1
ba  _____17 
 nop
_____16:
set	_boolT,%o1
_____17:
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	0,%l5
xor	%l4, %l5, %l5
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____22 
 nop
set	_boolF,%o1
ba  _____23 
 nop
_____22:
set	_boolT,%o1
_____23:
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	a,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-24,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____27 
 nop
set	_boolF,%o1
ba  _____28 
 nop
_____27:
set	_boolT,%o1
_____28:
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	a,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-28,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____32 
 nop
set	_boolF,%o1
ba  _____33 
 nop
_____32:
set	_boolT,%o1
_____33:
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	a,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	a,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	a,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____37 
 nop
set	_boolF,%o1
ba  _____38 
 nop
_____37:
set	_boolT,%o1
_____38:
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 32) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____45 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____46 
 nop
_____45:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____46:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____47 
 nop
ba  _____48 
 nop
_____47:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
_____48:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

