.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	5
______4: .word	0
______6: .word	9
______8: .word	-9
______9: .word	12
_____10: .asciz "5+(-9) = -4, "
.align	4
_____14: .asciz "\n"
.align	4
_____15: .asciz "-9-5 = -14, "
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "12-(-9)+5 = 26, "
.align	4
_____27: .asciz "\n"
.align	4
_____28: .asciz "5-((-9)+12) = 2, "
.align	4
_____35: .asciz "\n"
.align	4
_____36: .asciz "5*(-9) = -45, "
.align	4
_____40: .asciz "\n"
.align	4
_____41: .asciz "5*12 = 60, "
.align	4
_____45: .asciz "\n"
.align	4
_____46: .asciz "5/12 = 0, "
.align	4
_____50: .asciz "\n"
.align	4
_____51: .asciz "12/5 = 2, "
.align	4
_____55: .asciz "\n"
.align	4
_____56: .asciz "12/(-9) = -1, "
.align	4
_____60: .asciz "\n"
.align	4
_____61: .asciz "5/(-9) = 0, "
.align	4
_____65: .asciz "\n"
.align	4
_____66: .asciz "12%5 = 2, "
.align	4
_____70: .asciz "\n"
.align	4
_____71: .asciz "(-9)%5 = -4, "
.align	4
_____75: .asciz "\n"
.align	4
_____79: .asciz "5^12 = 9, "
.align	4
_____80: .asciz "\n"
.align	4
_____84: .asciz "5|12 = 13, "
.align	4
_____85: .asciz "\n"
.align	4
_____89: .asciz "5&12 = 4, "
.align	4
_____90: .asciz "\n"
.align	4
______95: .word	0
_____100: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	9,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l4
set	9,%l5
sub	%l4, %l5, %l5
set	-16,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-9,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-36,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-48,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-60,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-68,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-72,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-80,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-84,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-92,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-96,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-104,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-108,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-112,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-116,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-120,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-124,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-128,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
set	-132,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-140,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
set	-144,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-152,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
set	-156,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-160,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-164,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .div
nop
mov	%o0, %l2
set	-168,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	_strFmt,%o0
set	_____66,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-172,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-176,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-180,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-184,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-184,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-188,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-184,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-188,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .rem
nop
mov	%o0, %l2
set	-192,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____75,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-200,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-200,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-200,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-204,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-208,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____80,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-212,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-212,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-212,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-216,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	-220,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-224,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-224,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-224,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-228,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
and	%l4, %l5, %l5
set	-232,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-232,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 232) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____96 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____97 
 nop
_____96:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____97:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____98 
 nop
ba  _____99 
 nop
_____98:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
_____99:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

