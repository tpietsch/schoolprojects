.global	i, j, k, f, g, h, b, c, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	2
_____4: .single	0r1.1
______5: .single	0r1.1
______6: .word	2
______7: .word	1
______8: .word	0
_____9: .asciz "i = "
.align	4
_____10: .asciz "\n"
.align	4
_____11: .asciz "j = "
.align	4
_____12: .asciz "\n"
.align	4
_____13: .asciz "k = "
.align	4
_____14: .asciz "\n"
.align	4
_____15: .asciz "f = "
.align	4
_____16: .asciz "\n"
.align	4
_____17: .asciz "g = "
.align	4
_____18: .asciz "\n"
.align	4
_____19: .asciz "h = "
.align	4
_____20: .asciz "\n"
.align	4
_____21: .asciz "b = "
.align	4
_____24: .asciz "\n"
.align	4
_____25: .asciz "c = "
.align	4
_____28: .asciz "\n"
.align	4
______33: .word	0
_____38: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::i
i:	.skip	4
!CodeGen::doVarDecl::j
j:	.skip	4
!CodeGen::doVarDecl::k
k:	.skip	4
!CodeGen::doVarDecl::f
f:	.skip	4
!CodeGen::doVarDecl::g
g:	.skip	4
!CodeGen::doVarDecl::h
h:	.skip	4
!CodeGen::doVarDecl::b
b:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	i,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	j,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	2,%l1
set	g,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	g,%l1
add 	%g0, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	g,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
set	1,%l1
set	b,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____9,%o1
call printf
nop
set	i,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	j,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	k,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	h,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	b,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____22 
 nop
set	_boolF,%o1
ba  _____23 
 nop
_____22:
set	_boolT,%o1
_____23:
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____26 
 nop
set	_boolF,%o1
ba  _____27 
 nop
_____26:
set	_boolT,%o1
_____27:
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 0) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____34 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____35 
 nop
_____34:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____35:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____36 
 nop
ba  _____37 
 nop
_____36:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
_____37:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

