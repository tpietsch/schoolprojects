.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	2
______4: .word	3
______5: .word	4
______6: .word	6
______7: .word	5073
______8: .word	926
______9: .word	911
______10: .word	8675309
______11: .word	789
______12: .word	10
_____13: .asciz "Output format (expected, actual)"
.align	4
_____14: .asciz "\n"
.align	4
_____15: .asciz "1 | 2 = (3, "
.align	4
_____16: .asciz "3"
.align	4
_____17: .asciz ")"
.align	4
_____18: .asciz "\n"
.align	4
_____19: .asciz "2 | 4 = (6, "
.align	4
_____20: .asciz "6"
.align	4
_____21: .asciz ")"
.align	4
_____22: .asciz "\n"
.align	4
_____23: .asciz "3 | 4 = (7, "
.align	4
_____24: .asciz "7"
.align	4
_____25: .asciz ")"
.align	4
_____26: .asciz "\n"
.align	4
_____27: .asciz "6 | 4 = (6, "
.align	4
_____28: .asciz "6"
.align	4
_____29: .asciz ")"
.align	4
_____30: .asciz "\n"
.align	4
_____31: .asciz "8 | 2 = (10, "
.align	4
_____32: .asciz "10"
.align	4
_____33: .asciz ")"
.align	4
_____34: .asciz "\n"
.align	4
_____35: .asciz "5073 | 926 = (5087, "
.align	4
_____36: .asciz "5087"
.align	4
_____37: .asciz ")"
.align	4
_____38: .asciz "\n"
.align	4
_____39: .asciz "911 | 8675309 = (8675311, "
.align	4
_____40: .asciz "8675311"
.align	4
_____41: .asciz ")"
.align	4
_____42: .asciz "\n"
.align	4
_____43: .asciz "789 | 10 = (799, "
.align	4
_____44: .asciz "799"
.align	4
_____45: .asciz ")"
.align	4
_____46: .asciz "\n"
.align	4
_____47: .asciz "1 | i_2 = (3, "
.align	4
______49: .word	1
_____52: .asciz ")"
.align	4
_____53: .asciz "\n"
.align	4
_____54: .asciz "i_2 | 4 = (6, "
.align	4
______57: .word	4
_____59: .asciz ")"
.align	4
_____60: .asciz "\n"
.align	4
_____61: .asciz "i_3 | i_4 = (7, "
.align	4
_____65: .asciz ")"
.align	4
_____66: .asciz "\n"
.align	4
_____67: .asciz "i_6 | i_4 = (6, "
.align	4
_____71: .asciz ")"
.align	4
_____72: .asciz "\n"
.align	4
_____73: .asciz "8 | i_2 = (10, "
.align	4
______75: .word	8
_____78: .asciz ")"
.align	4
_____79: .asciz "\n"
.align	4
_____80: .asciz "i_5073 | i_926 = (5087, "
.align	4
_____84: .asciz ")"
.align	4
_____85: .asciz "\n"
.align	4
_____86: .asciz "i_911 | i_8675309 = (8675311, "
.align	4
_____90: .asciz ")"
.align	4
_____91: .asciz "\n"
.align	4
_____92: .asciz "i_789 | i_10 = (799, "
.align	4
_____96: .asciz ")"
.align	4
_____97: .asciz "\n"
.align	4
______102: .word	0
_____107: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	5073,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	926,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	911,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8675309,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	789,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	1,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	-56,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	4,%l5
or	%l4, %l5, %l5
set	-68,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	-80,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	_strFmt,%o0
set	_____66,%o1
call printf
nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	-92,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	8,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l4
set	-100,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	-104,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
set	_strFmt,%o0
set	_____80,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-112,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	-116,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	_strFmt,%o0
set	_____85,%o1
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-120,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-124,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	-128,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	_strFmt,%o0
set	_____91,%o1
call printf
nop
set	_strFmt,%o0
set	_____92,%o1
call printf
nop
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
or	%l4, %l5, %l5
set	-140,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 140) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____103 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____104 
 nop
_____103:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____104:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____105 
 nop
ba  _____106 
 nop
_____105:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____107,%o1
call printf
nop
_____106:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

