.global	foo0, foo1, foo2, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Inside foo0()"
.align	4
_____3: .asciz "\n"
.align	4
______5: .word	1
_____6: .asciz "Inside foo1()"
.align	4
_____7: .asciz "\n"
.align	4
_____8: .single	0r2.22
______10: .single	0r2.22
_____11: .asciz "Inside foo2()"
.align	4
_____12: .asciz "\n"
.align	4
______14: .word	1
_____18: .asciz "Should print 1: "
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "Should print 2.22: "
.align	4
_____21: .asciz "\n"
.align	4
_____22: .asciz "Should print true: "
.align	4
_____25: .asciz "\n"
.align	4
______28: .word	9
_____35: .asciz "Should print 10: "
.align	4
_____36: .asciz "\n"
.align	4
_____37: .asciz "Should print 1.00: "
.align	4
_____38: .asciz "\n"
.align	4
_____39: .asciz "Should print false: "
.align	4
_____42: .asciz "\n"
.align	4
______47: .word	0
_____52: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
add	%sp, -0, %sp
set	foo0, %l1
call %l1
nop
set	-4,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
add	%sp, -0, %sp
set	foo1, %l1
call %l1
nop
set	-12,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
add	%sp, -0, %sp
set	foo2, %l1
call %l1
nop
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____23 
 nop
set	_boolF,%o1
ba  _____24 
 nop
_____23:
set	_boolT,%o1
_____24:
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
add	%sp, -0, %sp
set	foo0, %l1
call %l1
nop
set	-28,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	9,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	9,%l4
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-40,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
add	%sp, -0, %sp
set	foo0, %l1
call %l1
nop
set	-44,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-48,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
add	%sp, -0, %sp
set	foo2, %l1
call %l1
nop
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-52,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-56,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____40 
 nop
set	_boolF,%o1
ba  _____41 
 nop
_____40:
set	_boolT,%o1
_____41:
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 56) & -8

foo0: 
set SAVE.foo0, %g1
save %sp, %g1, %sp
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%i0
ret
restore

ret
restore

SAVE.foo0 = -(92 + 4) & -8

foo1: 
set SAVE.foo1, %g1
save %sp, %g1, %sp
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	______10,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l1
add 	%fp, %l1, %l1
ld	[%l1], %i0
ret
restore

ret
restore

SAVE.foo1 = -(92 + 4) & -8

foo2: 
set SAVE.foo2, %g1
save %sp, %g1, %sp
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%i0
ret
restore

ret
restore

SAVE.foo2 = -(92 + 4) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____48 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____49 
 nop
_____48:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____49:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____50 
 nop
ba  _____51 
 nop
_____50:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
_____51:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

