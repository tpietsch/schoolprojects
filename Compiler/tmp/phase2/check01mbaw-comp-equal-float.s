.global	ba, bb, ga, gb, _0_____617, _0ga819, _0_____10111, bc, _0_____14115, _0gb16117, _0_____18119, bd, _0_____22123, _0ga24125, _0_____26127, be, _0_____30131, _0ga32133, _0_____34135, bg, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______3: .word	1
______4: .word	5
______5: .word	10
_____38: .asciz "01ba: false == "
.align	4
_____41: .asciz "\n"
.align	4
_____42: .asciz "02bb: true == "
.align	4
_____45: .asciz "\n"
.align	4
_____46: .asciz "03bc: false == "
.align	4
_____49: .asciz "\n"
.align	4
_____50: .asciz "04bd: true == "
.align	4
_____53: .asciz "\n"
.align	4
_____54: .asciz "05be: true == "
.align	4
_____57: .asciz "\n"
.align	4
_____58: .asciz "06bg: false == "
.align	4
_____61: .asciz "\n"
.align	4
______62: .word	5
______63: .word	10
_____64: .asciz "testing greater than"
.align	4
_____65: .asciz "\n"
.align	4
_____73: .asciz " == false07"
.align	4
_____74: .asciz "\n"
.align	4
_____82: .asciz " != true08"
.align	4
_____83: .asciz "\n"
.align	4
_____91: .asciz "ia == ib is false"
.align	4
_____92: .asciz "\n"
.align	4
_____100: .asciz "09ib != ia is true"
.align	4
_____101: .asciz "\n"
.align	4
_____102: .asciz "testing greater than or equal to (both at same value)"
.align	4
_____103: .asciz "\n"
.align	4
______104: .word	5
_____112: .asciz " == true10"
.align	4
_____113: .asciz "\n"
.align	4
_____121: .asciz " == false11"
.align	4
_____122: .asciz "\n"
.align	4
_____130: .asciz "ia == ic is true12"
.align	4
_____131: .asciz "\n"
.align	4
_____139: .asciz "ic == ia is true13"
.align	4
_____140: .asciz "\n"
.align	4
_____143: .asciz "5 == 6 is false"
.align	4
_____144: .asciz "\n"
.align	4
_____147: .asciz "6 != 5 is true14"
.align	4
_____148: .asciz "\n"
.align	4
_____151: .asciz "5 == 5 is true15"
.align	4
_____152: .asciz "\n"
.align	4
_____155: .asciz "5 != 5 is false"
.align	4
_____156: .asciz "\n"
.align	4
______161: .word	0
_____166: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::ba
ba:	.skip	4
!CodeGen::doVarDecl::bb
bb:	.skip	4
!CodeGen::doVarDecl::ga
ga:	.skip	4
!CodeGen::doVarDecl::gb
gb:	.skip	4
!CodeGen::doVarDecl::_____6
_0_____617:	.skip	4
!CodeGen::doVarDecl::ga8
_0ga819:	.skip	4
!CodeGen::doVarDecl::_____10
_0_____10111:	.skip	4
!CodeGen::doVarDecl::bc
bc:	.skip	4
!CodeGen::doVarDecl::_____14
_0_____14115:	.skip	4
!CodeGen::doVarDecl::gb16
_0gb16117:	.skip	4
!CodeGen::doVarDecl::_____18
_0_____18119:	.skip	4
!CodeGen::doVarDecl::bd
bd:	.skip	4
!CodeGen::doVarDecl::_____22
_0_____22123:	.skip	4
!CodeGen::doVarDecl::ga24
_0ga24125:	.skip	4
!CodeGen::doVarDecl::_____26
_0_____26127:	.skip	4
!CodeGen::doVarDecl::be
be:	.skip	4
!CodeGen::doVarDecl::_____30
_0_____30131:	.skip	4
!CodeGen::doVarDecl::ga32
_0ga32133:	.skip	4
!CodeGen::doVarDecl::_____34
_0_____34135:	.skip	4
!CodeGen::doVarDecl::bg
bg:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	0,%l1
set	ba,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	bb,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	ga,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	gb,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0ga819,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0ga819,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	gb,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____10111,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0ga819,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____10111,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____12 
 nop
set	_0_____617,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	_0_____617,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____13:
set	_0_____617,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bc,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	gb,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0gb16117,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	gb,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0gb16117,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____18119,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0gb16117,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____18119,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____20 
 nop
set	_0_____14115,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____21 
 nop
_____20:
set	1,%l3
set	_0_____14115,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____21:
set	_0_____14115,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bd,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0ga24125,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0ga24125,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____26127,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0ga24125,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____26127,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____28 
 nop
set	_0_____22123,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____29 
 nop
_____28:
set	1,%l3
set	_0_____22123,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____29:
set	_0_____22123,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	be,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0ga32133,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0ga32133,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____34135,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0ga32133,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____34135,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____36 
 nop
set	_0_____30131,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____37 
 nop
_____36:
set	1,%l3
set	_0_____30131,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____37:
set	_0_____30131,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bg,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	ba,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____39 
 nop
set	_boolF,%o1
ba  _____40 
 nop
_____39:
set	_boolT,%o1
_____40:
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	bb,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____43 
 nop
set	_boolF,%o1
ba  _____44 
 nop
_____43:
set	_boolT,%o1
_____44:
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	bc,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____47 
 nop
set	_boolF,%o1
ba  _____48 
 nop
_____47:
set	_boolT,%o1
_____48:
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	bd,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____51 
 nop
set	_boolF,%o1
ba  _____52 
 nop
_____51:
set	_boolT,%o1
_____52:
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	be,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____55 
 nop
set	_boolF,%o1
ba  _____56 
 nop
_____55:
set	_boolT,%o1
_____56:
call printf
nop
set	_strFmt,%o0
set	_____57,%o1
call printf
nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	bg,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____59 
 nop
set	_boolF,%o1
ba  _____60 
 nop
_____59:
set	_boolT,%o1
_____60:
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____69 
 nop
set	-12,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____70 
 nop
_____69:
set	1,%l3
set	-12,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____70:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____71 
 nop
set	_boolF,%o1
ba  _____72 
 nop
_____71:
set	_boolT,%o1
_____72:
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____78 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____79 
 nop
_____78:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____79:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____80 
 nop
set	_boolF,%o1
ba  _____81 
 nop
_____80:
set	_boolT,%o1
_____81:
call printf
nop
set	_strFmt,%o0
set	_____82,%o1
call printf
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____87 
 nop
set	-36,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____88 
 nop
_____87:
set	1,%l3
set	-36,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____88:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____89 
 nop
set	_strFmt,%o0
set	_____91,%o1
call printf
nop
set	_strFmt,%o0
set	_____92,%o1
call printf
nop
ba  _____90 
 nop
_____89:
_____90:
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____96 
 nop
set	-48,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____97 
 nop
_____96:
set	1,%l3
set	-48,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____97:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____98 
 nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
ba  _____99 
 nop
_____98:
_____99:
set	_strFmt,%o0
set	_____102,%o1
call printf
nop
set	_strFmt,%o0
set	_____103,%o1
call printf
nop
set	5,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____108 
 nop
set	-64,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____109 
 nop
_____108:
set	1,%l3
set	-64,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____109:
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____110 
 nop
set	_boolF,%o1
ba  _____111 
 nop
_____110:
set	_boolT,%o1
_____111:
call printf
nop
set	_strFmt,%o0
set	_____112,%o1
call printf
nop
set	_strFmt,%o0
set	_____113,%o1
call printf
nop
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____117 
 nop
set	-76,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____118 
 nop
_____117:
set	1,%l3
set	-76,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____118:
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____119 
 nop
set	_boolF,%o1
ba  _____120 
 nop
_____119:
set	_boolT,%o1
_____120:
call printf
nop
set	_strFmt,%o0
set	_____121,%o1
call printf
nop
set	_strFmt,%o0
set	_____122,%o1
call printf
nop
set	0, %l0
set	-88,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____126 
 nop
set	-88,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____127 
 nop
_____126:
set	1,%l3
set	-88,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____127:
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____128 
 nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
ba  _____129 
 nop
_____128:
_____129:
set	0, %l0
set	-100,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____135 
 nop
set	-100,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____136 
 nop
_____135:
set	1,%l3
set	-100,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____136:
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____137 
 nop
set	_strFmt,%o0
set	_____139,%o1
call printf
nop
set	_strFmt,%o0
set	_____140,%o1
call printf
nop
ba  _____138 
 nop
_____137:
_____138:
set	0,%l1
cmp	%l1, %g0
be  _____141 
 nop
set	_strFmt,%o0
set	_____143,%o1
call printf
nop
set	_strFmt,%o0
set	_____144,%o1
call printf
nop
ba  _____142 
 nop
_____141:
_____142:
set	1,%l1
cmp	%l1, %g0
be  _____145 
 nop
set	_strFmt,%o0
set	_____147,%o1
call printf
nop
set	_strFmt,%o0
set	_____148,%o1
call printf
nop
ba  _____146 
 nop
_____145:
_____146:
set	1,%l1
cmp	%l1, %g0
be  _____149 
 nop
set	_strFmt,%o0
set	_____151,%o1
call printf
nop
set	_strFmt,%o0
set	_____152,%o1
call printf
nop
ba  _____150 
 nop
_____149:
_____150:
set	0,%l1
cmp	%l1, %g0
be  _____153 
 nop
set	_strFmt,%o0
set	_____155,%o1
call printf
nop
set	_strFmt,%o0
set	_____156,%o1
call printf
nop
ba  _____154 
 nop
_____153:
_____154:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 108) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____162 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____163 
 nop
_____162:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____163:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____164 
 nop
ba  _____165 
 nop
_____164:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____166,%o1
call printf
nop
_____165:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

