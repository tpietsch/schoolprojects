.global	bT, bF, _0_____415, bc, _0_____819, bd, _0_____12113, be, _0_____16117, bg, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	0
_____20: .asciz "01bc: false == "
.align	4
_____23: .asciz "\n"
.align	4
_____24: .asciz "02bd: true == "
.align	4
_____27: .asciz "\n"
.align	4
_____28: .asciz "03bT: true == "
.align	4
_____31: .asciz "\n"
.align	4
_____32: .asciz "04bF: false == "
.align	4
_____35: .asciz "\n"
.align	4
_____36: .asciz "05be: true == "
.align	4
_____39: .asciz "\n"
.align	4
_____40: .asciz "06bg: true == "
.align	4
_____43: .asciz "\n"
.align	4
______44: .word	1
______45: .word	0
_____46: .asciz "testing greater than"
.align	4
_____47: .asciz "\n"
.align	4
_____53: .asciz " == false07"
.align	4
_____54: .asciz "\n"
.align	4
_____60: .asciz " != true08"
.align	4
_____61: .asciz "\n"
.align	4
_____67: .asciz "ia == ib is false"
.align	4
_____68: .asciz "\n"
.align	4
_____74: .asciz "09ib != ia is true"
.align	4
_____75: .asciz "\n"
.align	4
_____76: .asciz "testing greater than or equal to (both at same value)"
.align	4
_____77: .asciz "\n"
.align	4
_____83: .asciz " == true10"
.align	4
_____84: .asciz "\n"
.align	4
_____90: .asciz " == false11"
.align	4
_____91: .asciz "\n"
.align	4
_____97: .asciz "ia == true is true12"
.align	4
_____98: .asciz "\n"
.align	4
_____104: .asciz "false == ia is false"
.align	4
_____105: .asciz "\n"
.align	4
_____108: .asciz "true == true is true13"
.align	4
_____109: .asciz "\n"
.align	4
_____112: .asciz "true != true is false"
.align	4
_____113: .asciz "\n"
.align	4
_____116: .asciz "false == false is true14"
.align	4
_____117: .asciz "\n"
.align	4
_____120: .asciz "false != false is false"
.align	4
_____121: .asciz "\n"
.align	4
______126: .word	0
_____131: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::bT
bT:	.skip	4
!CodeGen::doVarDecl::bF
bF:	.skip	4
!CodeGen::doVarDecl::_____4
_0_____415:	.skip	4
!CodeGen::doVarDecl::bc
bc:	.skip	4
!CodeGen::doVarDecl::_____8
_0_____819:	.skip	4
!CodeGen::doVarDecl::bd
bd:	.skip	4
!CodeGen::doVarDecl::_____12
_0_____12113:	.skip	4
!CodeGen::doVarDecl::be
be:	.skip	4
!CodeGen::doVarDecl::_____16
_0_____16117:	.skip	4
!CodeGen::doVarDecl::bg
bg:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	bT,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	bF,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____6 
 nop
set	_0_____415,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____7 
 nop
_____6:
set	1,%l3
set	_0_____415,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____7:
set	_0_____415,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bc,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____10 
 nop
set	_0_____819,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____11 
 nop
_____10:
set	1,%l3
set	_0_____819,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____11:
set	_0_____819,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bd,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____14 
 nop
set	_0_____12113,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____15 
 nop
_____14:
set	1,%l3
set	_0_____12113,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____15:
set	_0_____12113,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	be,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____18 
 nop
set	_0_____16117,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____19 
 nop
_____18:
set	1,%l3
set	_0_____16117,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____19:
set	_0_____16117,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bg,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	bc,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____21 
 nop
set	_boolF,%o1
ba  _____22 
 nop
_____21:
set	_boolT,%o1
_____22:
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	bd,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____25 
 nop
set	_boolF,%o1
ba  _____26 
 nop
_____25:
set	_boolT,%o1
_____26:
call printf
nop
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	bT,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____29 
 nop
set	_boolF,%o1
ba  _____30 
 nop
_____29:
set	_boolT,%o1
_____30:
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	bF,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____33 
 nop
set	_boolF,%o1
ba  _____34 
 nop
_____33:
set	_boolT,%o1
_____34:
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	be,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____37 
 nop
set	_boolF,%o1
ba  _____38 
 nop
_____37:
set	_boolT,%o1
_____38:
call printf
nop
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	bg,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____41 
 nop
set	_boolF,%o1
ba  _____42 
 nop
_____41:
set	_boolT,%o1
_____42:
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____49 
 nop
set	-12,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____50 
 nop
_____49:
set	1,%l3
set	-12,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____50:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____51 
 nop
set	_boolF,%o1
ba  _____52 
 nop
_____51:
set	_boolT,%o1
_____52:
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____56 
 nop
set	-16,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____57 
 nop
_____56:
set	1,%l3
set	-16,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____57:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____58 
 nop
set	_boolF,%o1
ba  _____59 
 nop
_____58:
set	_boolT,%o1
_____59:
call printf
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____63 
 nop
set	-20,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____64 
 nop
_____63:
set	1,%l3
set	-20,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____64:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____65 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
ba  _____66 
 nop
_____65:
_____66:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____70 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____71 
 nop
_____70:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____71:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____72 
 nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
set	_strFmt,%o0
set	_____75,%o1
call printf
nop
ba  _____73 
 nop
_____72:
_____73:
set	_strFmt,%o0
set	_____76,%o1
call printf
nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
be  _____79 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____80 
 nop
_____79:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____80:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____81 
 nop
set	_boolF,%o1
ba  _____82 
 nop
_____81:
set	_boolT,%o1
_____82:
call printf
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
bne  _____86 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____87 
 nop
_____86:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____87:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____88 
 nop
set	_boolF,%o1
ba  _____89 
 nop
_____88:
set	_boolT,%o1
_____89:
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	_strFmt,%o0
set	_____91,%o1
call printf
nop
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
be  _____93 
 nop
set	-36,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____94 
 nop
_____93:
set	1,%l3
set	-36,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____94:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____95 
 nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	_strFmt,%o0
set	_____98,%o1
call printf
nop
ba  _____96 
 nop
_____95:
_____96:
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____100 
 nop
set	-40,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____101 
 nop
_____100:
set	1,%l3
set	-40,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____101:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____102 
 nop
set	_strFmt,%o0
set	_____104,%o1
call printf
nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
ba  _____103 
 nop
_____102:
_____103:
set	1,%l1
cmp	%l1, %g0
be  _____106 
 nop
set	_strFmt,%o0
set	_____108,%o1
call printf
nop
set	_strFmt,%o0
set	_____109,%o1
call printf
nop
ba  _____107 
 nop
_____106:
_____107:
set	0,%l1
cmp	%l1, %g0
be  _____110 
 nop
set	_strFmt,%o0
set	_____112,%o1
call printf
nop
set	_strFmt,%o0
set	_____113,%o1
call printf
nop
ba  _____111 
 nop
_____110:
_____111:
set	1,%l1
cmp	%l1, %g0
be  _____114 
 nop
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
ba  _____115 
 nop
_____114:
_____115:
set	0,%l1
cmp	%l1, %g0
be  _____118 
 nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
set	_strFmt,%o0
set	_____121,%o1
call printf
nop
ba  _____119 
 nop
_____118:
_____119:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 40) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____127 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____128 
 nop
_____127:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____128:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____129 
 nop
ba  _____130 
 nop
_____129:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
_____130:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

