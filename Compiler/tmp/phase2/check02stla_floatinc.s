.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .single	0r5.3
______3: .single	0r5.3
_____4: .single	0r1.2
______5: .single	0r1.2
______7: .word	1
_____11: .asciz "x++ x=6.30, "
.align	4
_____12: .asciz "\n"
.align	4
______14: .word	1
_____18: .asciz "++x x = 7.30, "
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "z + ++x = 9.50, "
.align	4
______22: .word	1
_____29: .asciz "\n"
.align	4
_____30: .asciz "x = 8.30, "
.align	4
_____31: .asciz "\n"
.align	4
_____32: .asciz "x++ + z = 9.50, "
.align	4
______34: .word	1
_____41: .asciz "\n"
.align	4
_____42: .asciz "x = 9.30, "
.align	4
_____43: .asciz "\n"
.align	4
_____44: .asciz "x++ + ++z = 11.50, "
.align	4
______46: .word	1
______51: .word	1
_____58: .asciz "\n"
.align	4
_____59: .asciz "x = 10.30, "
.align	4
_____60: .asciz "\n"
.align	4
_____61: .asciz "z = 2.20, "
.align	4
_____62: .asciz "\n"
.align	4
______67: .word	0
_____72: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	______3,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-12,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-20,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-28,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-36,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	1,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-44,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-52,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-56,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-68,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	1,%l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-72,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-72,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-76,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-80,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-84,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-88,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-92,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-96,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	1,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-104,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-100,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-100,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-104,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-108,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-112,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-120,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-116,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-116,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-120,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-124,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-128,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-136,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-140,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 140) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____68 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____69 
 nop
_____68:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____69:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____70 
 nop
ba  _____71 
 nop
_____70:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____72,%o1
call printf
nop
_____71:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

