.global	bad, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "BAD! -- SHOULD NOT PRINT!"
.align	4
_____3: .asciz "\n"
.align	4
______5: .word	1
______6: .word	1
______7: .word	0
______19: .word	0
______20: .word	1
_____21: .asciz "SHOULD NOT PRINT!"
.align	4
_____22: .asciz "\n"
.align	4
______32: .word	0
______33: .word	1
_____36: .asciz "Print 0"
.align	4
_____37: .asciz "\n"
.align	4
______46: .word	0
______47: .word	1
______57: .word	0
______58: .word	1
_____61: .asciz "Print 1"
.align	4
_____62: .asciz "\n"
.align	4
______71: .word	0
______72: .word	1
______82: .word	0
______83: .word	1
_____86: .asciz "SHOULD NOT PRINT!"
.align	4
_____87: .asciz "\n"
.align	4
_____88: .asciz "Done."
.align	4
_____89: .asciz "\n"
.align	4
______94: .word	0
_____99: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____8,_____9) 
_____8:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____10 
 nop
add	%sp, -0, %sp
set	bad, %l1
call %l1
nop
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____13 
 nop
ba  _____15 
 nop
ba  _____14 
 nop
_____13:
ba  _____14 
 nop
_____10:
_____14:
_____11:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____18 
 nop
_____15:
set	1,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____18:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____9 
 nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
ba  _____8 
 nop
_____9:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____23 
 nop
ba  _____25 
 nop
ba  _____24 
 nop
_____23:
_____24:
add	%sp, -0, %sp
set	bad, %l1
call %l1
nop
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____27 
 nop
ba  _____25 
 nop
ba  _____28 
 nop
_____27:
_____28:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____31 
 nop
_____25:
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____31:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____34 
 nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
ba  _____35 
 nop
_____34:
_____35:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____38 
 nop
ba  _____40 
 nop
ba  _____39 
 nop
_____38:
_____39:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____41 
 nop
ba  _____40 
 nop
ba  _____42 
 nop
_____41:
_____42:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____45 
 nop
_____40:
set	1,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____45:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____48 
 nop
ba  _____50 
 nop
ba  _____49 
 nop
_____48:
_____49:
add	%sp, -0, %sp
set	bad, %l1
call %l1
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-32,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____52 
 nop
ba  _____50 
 nop
ba  _____53 
 nop
_____52:
_____53:
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____56 
 nop
_____50:
set	1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____56:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____59 
 nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
ba  _____60 
 nop
_____59:
_____60:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____63 
 nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____65 
 nop
ba  _____67 
 nop
ba  _____66 
 nop
_____65:
ba  _____66 
 nop
_____63:
_____66:
_____64:
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____70 
 nop
_____67:
set	1,%l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____70:
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____73 
 nop
add	%sp, -0, %sp
set	bad, %l1
call %l1
nop
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-44,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____76 
 nop
ba  _____78 
 nop
ba  _____77 
 nop
_____76:
ba  _____77 
 nop
_____73:
_____77:
_____74:
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____81 
 nop
_____78:
set	1,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____81:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____84 
 nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
ba  _____85 
 nop
_____84:
_____85:
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
ret
restore

call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 48) & -8

bad: 
set SAVE.bad, %g1
save %sp, %g1, %sp
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%i0
ret
restore

ret
restore

SAVE.bad = -(92 + 4) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____95 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____96 
 nop
_____95:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____96:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____97 
 nop
ba  _____98 
 nop
_____97:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____99,%o1
call printf
nop
_____98:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

