.global	ba, bb, ga, gb, _0_____617, _0ga819, _0_____10111, bc, _0_____14115, _0gb16117, _0_____18119, bd, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	0
______4: .word	5
______5: .word	10
_____22: .asciz "ba: true == "
.align	4
_____25: .asciz "\n"
.align	4
_____26: .asciz "bb: false == "
.align	4
_____29: .asciz "\n"
.align	4
_____30: .asciz "bc: false == "
.align	4
_____33: .asciz "\n"
.align	4
_____34: .asciz "bd: true == "
.align	4
_____37: .asciz "\n"
.align	4
______38: .word	5
______39: .word	10
_____40: .asciz "testing greater than"
.align	4
_____41: .asciz "\n"
.align	4
_____49: .asciz " == false"
.align	4
_____50: .asciz "\n"
.align	4
_____58: .asciz " == true"
.align	4
_____59: .asciz "\n"
.align	4
_____67: .asciz "ia > ib is false"
.align	4
_____68: .asciz "\n"
.align	4
_____76: .asciz "ib > ia is true"
.align	4
_____77: .asciz "\n"
.align	4
_____78: .asciz "testing greater than or equal to"
.align	4
_____79: .asciz "\n"
.align	4
_____87: .asciz " == false"
.align	4
_____88: .asciz "\n"
.align	4
_____96: .asciz " == true"
.align	4
_____97: .asciz "\n"
.align	4
_____105: .asciz "ia >= ib is false"
.align	4
_____106: .asciz "\n"
.align	4
_____114: .asciz "ib >= ia is true"
.align	4
_____115: .asciz "\n"
.align	4
_____116: .asciz "testing greater than or equal to (both at same value)"
.align	4
_____117: .asciz "\n"
.align	4
______118: .word	5
_____126: .asciz " == true"
.align	4
_____127: .asciz "\n"
.align	4
_____135: .asciz " == true"
.align	4
_____136: .asciz "\n"
.align	4
_____144: .asciz "ia >= ic is true"
.align	4
_____145: .asciz "\n"
.align	4
_____153: .asciz "ic >= ia is true"
.align	4
_____154: .asciz "\n"
.align	4
_____157: .asciz "5 >= 6 is false"
.align	4
_____158: .asciz "\n"
.align	4
_____161: .asciz "6 >= 5 is true"
.align	4
_____162: .asciz "\n"
.align	4
_____165: .asciz "5 >= 5 is true"
.align	4
_____166: .asciz "\n"
.align	4
______171: .word	0
_____176: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::ba
ba:	.skip	4
!CodeGen::doVarDecl::bb
bb:	.skip	4
!CodeGen::doVarDecl::ga
ga:	.skip	4
!CodeGen::doVarDecl::gb
gb:	.skip	4
!CodeGen::doVarDecl::_____6
_0_____617:	.skip	4
!CodeGen::doVarDecl::ga8
_0ga819:	.skip	4
!CodeGen::doVarDecl::_____10
_0_____10111:	.skip	4
!CodeGen::doVarDecl::bc
bc:	.skip	4
!CodeGen::doVarDecl::_____14
_0_____14115:	.skip	4
!CodeGen::doVarDecl::gb16
_0gb16117:	.skip	4
!CodeGen::doVarDecl::_____18
_0_____18119:	.skip	4
!CodeGen::doVarDecl::bd
bd:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	ba,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	bb,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	ga,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	gb,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0ga819,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0ga819,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	gb,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____10111,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0ga819,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____10111,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____12 
 nop
set	_0_____617,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	_0_____617,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____13:
set	_0_____617,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bc,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	gb,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0gb16117,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	gb,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0gb16117,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	ga,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____18119,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_0gb16117,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_0_____18119,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____20 
 nop
set	_0_____14115,%l2
add 	%g0, %l2, %l2
st	%g0, [%l2]
ba  _____21 
 nop
_____20:
set	1,%l3
set	_0_____14115,%l2
add 	%g0, %l2, %l2
st	%l3, [%l2]
_____21:
set	_0_____14115,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	bd,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	ba,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____23 
 nop
set	_boolF,%o1
ba  _____24 
 nop
_____23:
set	_boolT,%o1
_____24:
call printf
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	bb,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____27 
 nop
set	_boolF,%o1
ba  _____28 
 nop
_____27:
set	_boolT,%o1
_____28:
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	bc,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____31 
 nop
set	_boolF,%o1
ba  _____32 
 nop
_____31:
set	_boolT,%o1
_____32:
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	bd,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____35 
 nop
set	_boolF,%o1
ba  _____36 
 nop
_____35:
set	_boolT,%o1
_____36:
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	5,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	10,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____45 
 nop
set	-12,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____46 
 nop
_____45:
set	1,%l3
set	-12,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____46:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____47 
 nop
set	_boolF,%o1
ba  _____48 
 nop
_____47:
set	_boolT,%o1
_____48:
call printf
nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____54 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____55 
 nop
_____54:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____55:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____56 
 nop
set	_boolF,%o1
ba  _____57 
 nop
_____56:
set	_boolT,%o1
_____57:
call printf
nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____63 
 nop
set	-36,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____64 
 nop
_____63:
set	1,%l3
set	-36,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____64:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____65 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	_strFmt,%o0
set	_____68,%o1
call printf
nop
ba  _____66 
 nop
_____65:
_____66:
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____72 
 nop
set	-48,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____73 
 nop
_____72:
set	1,%l3
set	-48,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____73:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____74 
 nop
set	_strFmt,%o0
set	_____76,%o1
call printf
nop
set	_strFmt,%o0
set	_____77,%o1
call printf
nop
ba  _____75 
 nop
_____74:
_____75:
set	_strFmt,%o0
set	_____78,%o1
call printf
nop
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____83 
 nop
set	-60,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____84 
 nop
_____83:
set	1,%l3
set	-60,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____84:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____85 
 nop
set	_boolF,%o1
ba  _____86 
 nop
_____85:
set	_boolT,%o1
_____86:
call printf
nop
set	_strFmt,%o0
set	_____87,%o1
call printf
nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____92 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____93 
 nop
_____92:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____93:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____94 
 nop
set	_boolF,%o1
ba  _____95 
 nop
_____94:
set	_boolT,%o1
_____95:
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	_strFmt,%o0
set	_____97,%o1
call printf
nop
set	0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____101 
 nop
set	-84,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____102 
 nop
_____101:
set	1,%l3
set	-84,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____102:
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____103 
 nop
set	_strFmt,%o0
set	_____105,%o1
call printf
nop
set	_strFmt,%o0
set	_____106,%o1
call printf
nop
ba  _____104 
 nop
_____103:
_____104:
set	0, %l0
set	-96,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____110 
 nop
set	-96,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____111 
 nop
_____110:
set	1,%l3
set	-96,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____111:
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____112 
 nop
set	_strFmt,%o0
set	_____114,%o1
call printf
nop
set	_strFmt,%o0
set	_____115,%o1
call printf
nop
ba  _____113 
 nop
_____112:
_____113:
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	5,%l1
set	-108,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____122 
 nop
set	-112,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____123 
 nop
_____122:
set	1,%l3
set	-112,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____123:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____124 
 nop
set	_boolF,%o1
ba  _____125 
 nop
_____124:
set	_boolT,%o1
_____125:
call printf
nop
set	_strFmt,%o0
set	_____126,%o1
call printf
nop
set	_strFmt,%o0
set	_____127,%o1
call printf
nop
set	0, %l0
set	-124,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____131 
 nop
set	-124,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____132 
 nop
_____131:
set	1,%l3
set	-124,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____132:
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____133 
 nop
set	_boolF,%o1
ba  _____134 
 nop
_____133:
set	_boolT,%o1
_____134:
call printf
nop
set	_strFmt,%o0
set	_____135,%o1
call printf
nop
set	_strFmt,%o0
set	_____136,%o1
call printf
nop
set	0, %l0
set	-136,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____140 
 nop
set	-136,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____141 
 nop
_____140:
set	1,%l3
set	-136,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____141:
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____142 
 nop
set	_strFmt,%o0
set	_____144,%o1
call printf
nop
set	_strFmt,%o0
set	_____145,%o1
call printf
nop
ba  _____143 
 nop
_____142:
_____143:
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____149 
 nop
set	-148,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____150 
 nop
_____149:
set	1,%l3
set	-148,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____150:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____151 
 nop
set	_strFmt,%o0
set	_____153,%o1
call printf
nop
set	_strFmt,%o0
set	_____154,%o1
call printf
nop
ba  _____152 
 nop
_____151:
_____152:
set	0,%l1
cmp	%l1, %g0
be  _____155 
 nop
set	_strFmt,%o0
set	_____157,%o1
call printf
nop
set	_strFmt,%o0
set	_____158,%o1
call printf
nop
ba  _____156 
 nop
_____155:
_____156:
set	1,%l1
cmp	%l1, %g0
be  _____159 
 nop
set	_strFmt,%o0
set	_____161,%o1
call printf
nop
set	_strFmt,%o0
set	_____162,%o1
call printf
nop
ba  _____160 
 nop
_____159:
_____160:
set	1,%l1
cmp	%l1, %g0
be  _____163 
 nop
set	_strFmt,%o0
set	_____165,%o1
call printf
nop
set	_strFmt,%o0
set	_____166,%o1
call printf
nop
ba  _____164 
 nop
_____163:
_____164:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 156) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____172 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____173 
 nop
_____172:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____173:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____174 
 nop
ba  _____175 
 nop
_____174:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____176,%o1
call printf
nop
_____175:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

