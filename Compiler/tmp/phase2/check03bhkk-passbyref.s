.global	x, y, _00415, _09.25718, _0_____10111, f, foo1, swap, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
_____3: .single	0r9.25
______6: .word	0
______9: .single	0r9.25
______12: .single	0r-9.25
_____13: .asciz "Inside foo1()\n"
.align	4
_____15: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____17: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____19: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____21: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____23: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____25: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______26: .word	10
_____27: .asciz "Starting test...\n\n"
.align	4
_____30: .asciz "Expected: 10\n"
.align	4
_____31: .asciz "Actual "
.align	4
_____32: .asciz "\n\n"
.align	4
_____35: .asciz "Expected: 20\n"
.align	4
_____36: .asciz "Actual "
.align	4
_____37: .asciz "\n\n"
.align	4
_____38: .asciz "Calling swap(x,y)...\n\n"
.align	4
_____42: .asciz "Expected: x = 20, y = 10\n"
.align	4
_____43: .asciz "Actual: x = "
.align	4
_____44: .asciz ", y = "
.align	4
_____45: .asciz "\n\n"
.align	4
______46: .word	25
_____47: .asciz "Calling swap(turnip, x)...\n\n"
.align	4
_____51: .asciz "Expected: turnip = 20, x = 25\n"
.align	4
_____52: .asciz "Actual: turnip = "
.align	4
_____53: .asciz ", x = "
.align	4
_____54: .asciz "\n"
.align	4
_____55: .asciz "End tests...\n"
.align	4
______60: .word	0
_____65: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
x:	.skip	4
!CodeGen::doVarDecl::y
y:	.skip	4
!CodeGen::doVarDecl::04
_00415:	.skip	4
!CodeGen::doVarDecl::9.257
_09.25718:	.skip	4
!CodeGen::doVarDecl::_____10
_0_____10111:	.skip	4
!CodeGen::doVarDecl::f
f:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	x,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	_00415,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______9,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	_09.25718,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_00415,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	_00415,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
set	_00415,%l3
add 	%g0, %l3, %l3
ld	[%l3], %f4
set	_09.25718,%l3
add 	%g0, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	_0_____10111,%l3
add 	%g0, %l3, %l3
st	%f5, [%l3]
set	______12,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
_init_done:
set	10,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____27,%o1
call printf
nop
set	x, %l1
add	%g0, %l1, %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
add	%sp, -0, %sp
set	foo1, %l1
call %l1
nop
set	-12,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	y, %l1
add	%g0, %l1, %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	20,%o1
add	%sp, -0, %sp
set	foo1, %l1
call %l1
nop
set	-20,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	_strFmt,%o0
set	_____36,%o1
call printf
nop
set	y,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	x, %l1
add	%g0, %l1, %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	y, %l1
add	%g0, %l1, %l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
add	%sp, -0, %sp
set	swap, %l1
call %l1
nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	y,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	25,%l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	-32, %l1
add	%fp, %l1, %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	x, %l1
add	%g0, %l1, %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
add	%sp, -0, %sp
set	swap, %l1
call %l1
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
call __________.MEMLEAK
nop
set	y,%l1
add 	%g0, %l1, %l1
ld	[%l1], %i0
ret
restore

call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 40) & -8

foo1: 
set SAVE.foo1, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____14 
 nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	1, %o0
call exit
nop
_____14:
st	%l1, [%l0]
set	-4,%l1
add 	%fp, %l1, %l1
ld	[%l1], %l0
cmp	%l0, %g0
bne  _____16 
 nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	1, %o0
call exit
nop
_____16:
ld	[%l0], %i0
ret
restore

ret
restore

SAVE.foo1 = -(92 + 8) & -8

swap: 
set SAVE.swap, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____18 
 nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	1, %o0
call exit
nop
_____18:
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____20 
 nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	1, %o0
call exit
nop
_____20:
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____22 
 nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	1, %o0
call exit
nop
_____22:
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____24 
 nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
_____24:
st	%l1, [%l0]
ret
restore

SAVE.swap = -(92 + 12) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____61 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____62 
 nop
_____61:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____62:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____63 
 nop
ba  _____64 
 nop
_____63:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
_____64:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

