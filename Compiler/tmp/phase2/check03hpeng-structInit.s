.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	5
_____5: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____7: .single	0r2.2
______8: .single	0r2.2
_____10: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______12: .word	0
_____14: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____17: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____18: .asciz "\n"
.align	4
_____21: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____22: .asciz "\n"
.align	4
_____25: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____28: .asciz "\n"
.align	4
______33: .word	0
_____38: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	-12, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-36,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	5,%l1
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____4 
 nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	1, %o0
call exit
nop
_____4:
st	%l1, [%l0]
set	-12, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-48,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	______8,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____9 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1, %o0
call exit
nop
_____9:
st	%f1, [%l0]
set	-12, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-60,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	0,%l1
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____13 
 nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	1, %o0
call exit
nop
_____13:
st	%l1, [%l0]
set	12, %o2
set	-12, %o1
add	%fp, %o1, %o1
set	-24, %o0
add	%fp, %o0, %o0
call memmove
nop
set	-24, %l1
add	%fp, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-72,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____16 
 nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	1, %o0
call exit
nop
_____16:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	-24, %l1
add	%fp, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-84,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____20 
 nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	1, %o0
call exit
nop
_____20:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	-24, %l1
add	%fp, %l1, %l1
set	8, %l2
add	%l2, %l1, %l1
set	-96,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____24 
 nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
_____24:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____26 
 nop
set	_boolF,%o1
ba  _____27 
 nop
_____26:
set	_boolT,%o1
_____27:
call printf
nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 96) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____34 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____35 
 nop
_____34:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____35:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____36 
 nop
ba  _____37 
 nop
_____36:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
_____37:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

