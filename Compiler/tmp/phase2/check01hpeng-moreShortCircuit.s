.global	t, f, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	0
______7: .word	1
______13: .word	0
______14: .word	1
______18: .word	0
______24: .word	0
______25: .word	1
_____28: .asciz "should be printed"
.align	4
_____29: .asciz "\n"
.align	4
_____30: .asciz "shoud be true: "
.align	4
_____33: .asciz " "
.align	4
_____34: .asciz "\n"
.align	4
______39: .word	0
_____44: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::t
t:	.skip	4
!CodeGen::doVarDecl::f
f:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	1,%l1
set	t,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	f,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____4 
 nop
ba  _____6 
 nop
ba  _____5 
 nop
_____4:
_____5:
set	1,%l1
set	f,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____8 
 nop
ba  _____6 
 nop
ba  _____9 
 nop
_____8:
_____9:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____12 
 nop
_____6:
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____12:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____15 
 nop
ba  _____17 
 nop
ba  _____16 
 nop
_____15:
_____16:
set	0,%l1
set	f,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____19 
 nop
ba  _____17 
 nop
ba  _____20 
 nop
_____19:
_____20:
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____23 
 nop
_____17:
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____23:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
ba  _____27 
 nop
_____26:
_____27:
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____31 
 nop
set	_boolF,%o1
ba  _____32 
 nop
_____31:
set	_boolT,%o1
_____32:
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 8) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____40 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____41 
 nop
_____40:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____41:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____42 
 nop
ba  _____43 
 nop
_____42:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
_____43:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

