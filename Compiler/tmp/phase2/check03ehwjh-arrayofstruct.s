.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	8
______5: .word	0
______10: .word	40
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "0"
.align	4
_____18: .asciz " is outside legal range [0,5).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "0"
.align	4
_____30: .asciz " is outside legal range [0,5).\n"
.align	4
_____34: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______35: .word	6
_____37: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______39: .word	8
______41: .word	1
______46: .word	40
_____52: .asciz "Index value of "
.align	4
_____53: .asciz "1"
.align	4
_____54: .asciz " is outside legal range [0,5).\n"
.align	4
______59: .word	0
_____64: .asciz "Index value of "
.align	4
_____65: .asciz "1"
.align	4
_____66: .asciz " is outside legal range [0,5).\n"
.align	4
_____70: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______71: .word	10
_____73: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______75: .word	8
______77: .word	0
______82: .word	40
_____88: .asciz "Index value of "
.align	4
_____89: .asciz "0"
.align	4
_____90: .asciz " is outside legal range [0,5).\n"
.align	4
______95: .word	0
_____100: .asciz "Index value of "
.align	4
_____101: .asciz "0"
.align	4
_____102: .asciz " is outside legal range [0,5).\n"
.align	4
_____106: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____108: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____109: .asciz "\n"
.align	4
______111: .word	8
______113: .word	1
______118: .word	40
_____124: .asciz "Index value of "
.align	4
_____125: .asciz "1"
.align	4
_____126: .asciz " is outside legal range [0,5).\n"
.align	4
______131: .word	0
_____136: .asciz "Index value of "
.align	4
_____137: .asciz "1"
.align	4
_____138: .asciz " is outside legal range [0,5).\n"
.align	4
_____142: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____144: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____145: .asciz "\n"
.align	4
______150: .word	0
_____155: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	8,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-52,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-84, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____33 
 nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	1, %o0
call exit
nop
_____33:
ld	[%l1], %l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-92,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	6,%l1
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____36 
 nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
set	1, %o0
call exit
nop
_____36:
st	%l1, [%l0]
set	8,%l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-104,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-108,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____48 
 nop
set	-108,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____49 
 nop
_____48:
set	1,%l3
set	-108,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____49:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____50 
 nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____51 
 nop
_____50:
_____51:
set	0, %l0
set	-120,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____60 
 nop
set	-120,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____61 
 nop
_____60:
set	1,%l3
set	-120,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____61:
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____62 
 nop
set	_strFmt,%o0
set	_____64,%o1
call printf
nop
set	_strFmt,%o0
set	_____65,%o1
call printf
nop
set	_strFmt,%o0
set	_____66,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____63 
 nop
_____62:
_____63:
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-136, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____69 
 nop
set	_strFmt,%o0
set	_____70,%o1
call printf
nop
set	1, %o0
call exit
nop
_____69:
ld	[%l1], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4, %l2
add	%l2, %l1, %l1
set	-144,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	10,%l1
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____72 
 nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
set	1, %o0
call exit
nop
_____72:
st	%l1, [%l0]
set	8,%l1
set	-148,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-156,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-160,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____84 
 nop
set	-160,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____85 
 nop
_____84:
set	1,%l3
set	-160,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____85:
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____86 
 nop
set	_strFmt,%o0
set	_____88,%o1
call printf
nop
set	_strFmt,%o0
set	_____89,%o1
call printf
nop
set	_strFmt,%o0
set	_____90,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____87 
 nop
_____86:
_____87:
set	0, %l0
set	-172,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____96 
 nop
set	-172,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____97 
 nop
_____96:
set	1,%l3
set	-172,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____97:
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____98 
 nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	_strFmt,%o0
set	_____101,%o1
call printf
nop
set	_strFmt,%o0
set	_____102,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____99 
 nop
_____98:
_____99:
set	-156,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-188,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-188, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____105 
 nop
set	_strFmt,%o0
set	_____106,%o1
call printf
nop
set	1, %o0
call exit
nop
_____105:
ld	[%l1], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-196,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____107 
 nop
set	_strFmt,%o0
set	_____108,%o1
call printf
nop
set	1, %o0
call exit
nop
_____107:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____109,%o1
call printf
nop
set	8,%l1
set	-200,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-208,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-212,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-220,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____120 
 nop
set	-212,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____121 
 nop
_____120:
set	1,%l3
set	-212,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____121:
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____122 
 nop
set	_strFmt,%o0
set	_____124,%o1
call printf
nop
set	_strFmt,%o0
set	_____125,%o1
call printf
nop
set	_strFmt,%o0
set	_____126,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____123 
 nop
_____122:
_____123:
set	0, %l0
set	-224,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-228,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____132 
 nop
set	-224,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____133 
 nop
_____132:
set	1,%l3
set	-224,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____133:
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____134 
 nop
set	_strFmt,%o0
set	_____136,%o1
call printf
nop
set	_strFmt,%o0
set	_____137,%o1
call printf
nop
set	_strFmt,%o0
set	_____138,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____135 
 nop
_____134:
_____135:
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-240,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-240, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____141 
 nop
set	_strFmt,%o0
set	_____142,%o1
call printf
nop
set	1, %o0
call exit
nop
_____141:
ld	[%l1], %l1
set	-248,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4, %l2
add	%l2, %l1, %l1
set	-248,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-248,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____143 
 nop
set	_strFmt,%o0
set	_____144,%o1
call printf
nop
set	1, %o0
call exit
nop
_____143:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____145,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 248) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____151 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____152 
 nop
_____151:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____152:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____153 
 nop
ba  _____154 
 nop
_____153:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____155,%o1
call printf
nop
_____154:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

