.global	funval, __________.MEMLEAK,main
.section	".data"
.align	4
_____3: .asciz "parameter x = 5, "
.align	4
_____4: .asciz "\n"
.align	4
_____5: .asciz "y = 23.01, "
.align	4
_____6: .asciz "\n"
.align	4
_____7: .asciz "z = true, "
.align	4
_____10: .asciz "\n"
.align	4
______11: .word	12
_____16: .asciz "parameter x = 12, "
.align	4
_____17: .asciz "\n"
.align	4
_____18: .asciz "y = 35.01, "
.align	4
_____19: .asciz "\n"
.align	4
_____20: .asciz "z = false, "
.align	4
_____23: .asciz "\n"
.align	4
_____24: .single	0r23.01
______27: .word	0
______32: .word	0
_____37: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	5,%o0
set	_____24,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f3
set	1,%o1
add	%sp, -0, %sp
set	funval, %l1
call %l1
nop
call __________.MEMLEAK
nop
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%i0
ret
restore

call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 4) & -8

funval: 
set SAVE.funval, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%f3, [%l0]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____8 
 nop
set	_boolF,%o1
ba  _____9 
 nop
_____8:
set	_boolT,%o1
_____9:
call printf
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	12,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-24,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-28,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-32,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____21 
 nop
set	_boolF,%o1
ba  _____22 
 nop
_____21:
set	_boolT,%o1
_____22:
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
ret
restore

SAVE.funval = -(92 + 32) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____33 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____34 
 nop
_____33:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____34:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____35 
 nop
ba  _____36 
 nop
_____35:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____37,%o1
call printf
nop
_____36:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

