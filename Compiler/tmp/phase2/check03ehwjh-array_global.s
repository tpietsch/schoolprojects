.global	array, __________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	4
______5: .word	0
______10: .word	40
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "0"
.align	4
_____18: .asciz " is outside legal range [0,10).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "0"
.align	4
_____30: .asciz " is outside legal range [0,10).\n"
.align	4
______32: .word	0
_____34: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______36: .word	4
______38: .word	1
______43: .word	40
_____49: .asciz "Index value of "
.align	4
_____50: .asciz "1"
.align	4
_____51: .asciz " is outside legal range [0,10).\n"
.align	4
______56: .word	0
_____61: .asciz "Index value of "
.align	4
_____62: .asciz "1"
.align	4
_____63: .asciz " is outside legal range [0,10).\n"
.align	4
______65: .word	1
_____67: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______69: .word	4
______71: .word	2
______76: .word	40
_____82: .asciz "Index value of "
.align	4
_____83: .asciz "2"
.align	4
_____84: .asciz " is outside legal range [0,10).\n"
.align	4
______89: .word	0
_____94: .asciz "Index value of "
.align	4
_____95: .asciz "2"
.align	4
_____96: .asciz " is outside legal range [0,10).\n"
.align	4
______98: .word	2
_____100: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______102: .word	4
______104: .word	3
______109: .word	40
_____115: .asciz "Index value of "
.align	4
_____116: .asciz "3"
.align	4
_____117: .asciz " is outside legal range [0,10).\n"
.align	4
______122: .word	0
_____127: .asciz "Index value of "
.align	4
_____128: .asciz "3"
.align	4
_____129: .asciz " is outside legal range [0,10).\n"
.align	4
______131: .word	3
_____133: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____134: .asciz "Output format (expected, actual)"
.align	4
_____135: .asciz "\n"
.align	4
_____136: .asciz "array[0] = (0, "
.align	4
______138: .word	4
______140: .word	0
______145: .word	40
_____151: .asciz "Index value of "
.align	4
_____152: .asciz "0"
.align	4
_____153: .asciz " is outside legal range [0,10).\n"
.align	4
______158: .word	0
_____163: .asciz "Index value of "
.align	4
_____164: .asciz "0"
.align	4
_____165: .asciz " is outside legal range [0,10).\n"
.align	4
_____168: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____169: .asciz ")"
.align	4
_____170: .asciz "\n"
.align	4
_____171: .asciz "array[1] = (1, "
.align	4
______173: .word	4
______175: .word	1
______180: .word	40
_____186: .asciz "Index value of "
.align	4
_____187: .asciz "1"
.align	4
_____188: .asciz " is outside legal range [0,10).\n"
.align	4
______193: .word	0
_____198: .asciz "Index value of "
.align	4
_____199: .asciz "1"
.align	4
_____200: .asciz " is outside legal range [0,10).\n"
.align	4
_____203: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____204: .asciz ")"
.align	4
_____205: .asciz "\n"
.align	4
_____206: .asciz "array[2] = (2, "
.align	4
______208: .word	4
______210: .word	2
______215: .word	40
_____221: .asciz "Index value of "
.align	4
_____222: .asciz "2"
.align	4
_____223: .asciz " is outside legal range [0,10).\n"
.align	4
______228: .word	0
_____233: .asciz "Index value of "
.align	4
_____234: .asciz "2"
.align	4
_____235: .asciz " is outside legal range [0,10).\n"
.align	4
_____238: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____239: .asciz ")"
.align	4
_____240: .asciz "\n"
.align	4
______245: .word	0
_____250: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::array
array:	.skip	40
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	4,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-12,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-16,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-16,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-28,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-28,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-28,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____33 
 nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	1, %o0
call exit
nop
_____33:
st	%l1, [%l0]
set	4,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-52,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____45 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____46 
 nop
_____45:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____46:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____47 
 nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____48 
 nop
_____47:
_____48:
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____57 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____58 
 nop
_____57:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____58:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____59 
 nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____60 
 nop
_____59:
_____60:
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____66 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	1, %o0
call exit
nop
_____66:
st	%l1, [%l0]
set	4,%l1
set	-84,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-92,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-96,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____78 
 nop
set	-96,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____79 
 nop
_____78:
set	1,%l3
set	-96,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____79:
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____80 
 nop
set	_strFmt,%o0
set	_____82,%o1
call printf
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____81 
 nop
_____80:
_____81:
set	0, %l0
set	-108,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-112,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____90 
 nop
set	-108,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____91 
 nop
_____90:
set	1,%l3
set	-108,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____91:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____92 
 nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
set	_strFmt,%o0
set	_____95,%o1
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____93 
 nop
_____92:
_____93:
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____99 
 nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	1, %o0
call exit
nop
_____99:
st	%l1, [%l0]
set	4,%l1
set	-124,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-132,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-136,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____111 
 nop
set	-136,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____112 
 nop
_____111:
set	1,%l3
set	-136,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____112:
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____113 
 nop
set	_strFmt,%o0
set	_____115,%o1
call printf
nop
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____114 
 nop
_____113:
_____114:
set	0, %l0
set	-148,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____123 
 nop
set	-148,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____124 
 nop
_____123:
set	1,%l3
set	-148,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____124:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____125 
 nop
set	_strFmt,%o0
set	_____127,%o1
call printf
nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
set	_strFmt,%o0
set	_____129,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____126 
 nop
_____125:
_____126:
set	-132,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____132 
 nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	1, %o0
call exit
nop
_____132:
st	%l1, [%l0]
set	_strFmt,%o0
set	_____134,%o1
call printf
nop
set	_strFmt,%o0
set	_____135,%o1
call printf
nop
set	_strFmt,%o0
set	_____136,%o1
call printf
nop
set	4,%l1
set	-164,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-172,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-176,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-184,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____147 
 nop
set	-176,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____148 
 nop
_____147:
set	1,%l3
set	-176,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____148:
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____149 
 nop
set	_strFmt,%o0
set	_____151,%o1
call printf
nop
set	_strFmt,%o0
set	_____152,%o1
call printf
nop
set	_strFmt,%o0
set	_____153,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____150 
 nop
_____149:
_____150:
set	0, %l0
set	-188,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____159 
 nop
set	-188,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____160 
 nop
_____159:
set	1,%l3
set	-188,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____160:
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____161 
 nop
set	_strFmt,%o0
set	_____163,%o1
call printf
nop
set	_strFmt,%o0
set	_____164,%o1
call printf
nop
set	_strFmt,%o0
set	_____165,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____162 
 nop
_____161:
_____162:
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-200,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____167 
 nop
set	_strFmt,%o0
set	_____168,%o1
call printf
nop
set	1, %o0
call exit
nop
_____167:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____169,%o1
call printf
nop
set	_strFmt,%o0
set	_____170,%o1
call printf
nop
set	_strFmt,%o0
set	_____171,%o1
call printf
nop
set	4,%l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-208,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-212,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-216,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-220,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-224,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____182 
 nop
set	-216,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____183 
 nop
_____182:
set	1,%l3
set	-216,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____183:
set	-216,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____184 
 nop
set	_strFmt,%o0
set	_____186,%o1
call printf
nop
set	_strFmt,%o0
set	_____187,%o1
call printf
nop
set	_strFmt,%o0
set	_____188,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____185 
 nop
_____184:
_____185:
set	0, %l0
set	-228,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-236,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-232,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____194 
 nop
set	-228,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____195 
 nop
_____194:
set	1,%l3
set	-228,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____195:
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____196 
 nop
set	_strFmt,%o0
set	_____198,%o1
call printf
nop
set	_strFmt,%o0
set	_____199,%o1
call printf
nop
set	_strFmt,%o0
set	_____200,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____197 
 nop
_____196:
_____197:
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-240,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-240,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____202 
 nop
set	_strFmt,%o0
set	_____203,%o1
call printf
nop
set	1, %o0
call exit
nop
_____202:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____204,%o1
call printf
nop
set	_strFmt,%o0
set	_____205,%o1
call printf
nop
set	_strFmt,%o0
set	_____206,%o1
call printf
nop
set	4,%l1
set	-244,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-248,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-252,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-256,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	40,%l1
set	-260,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-264,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	40,%l1
set	-264,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____217 
 nop
set	-256,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____218 
 nop
_____217:
set	1,%l3
set	-256,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____218:
set	-256,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____219 
 nop
set	_strFmt,%o0
set	_____221,%o1
call printf
nop
set	_strFmt,%o0
set	_____222,%o1
call printf
nop
set	_strFmt,%o0
set	_____223,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____220 
 nop
_____219:
_____220:
set	0, %l0
set	-268,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-272,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-272,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-276,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-272,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____229 
 nop
set	-268,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____230 
 nop
_____229:
set	1,%l3
set	-268,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____230:
set	-268,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____231 
 nop
set	_strFmt,%o0
set	_____233,%o1
call printf
nop
set	_strFmt,%o0
set	_____234,%o1
call printf
nop
set	_strFmt,%o0
set	_____235,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____232 
 nop
_____231:
_____232:
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	array, %l2
add	%g0, %l2, %l2
add	%l1, %l2, %l1
set	-280,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-280,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____237 
 nop
set	_strFmt,%o0
set	_____238,%o1
call printf
nop
set	1, %o0
call exit
nop
_____237:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____239,%o1
call printf
nop
set	_strFmt,%o0
set	_____240,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 280) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____246 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____247 
 nop
_____246:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____247:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____248 
 nop
ba  _____249 
 nop
_____248:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____250,%o1
call printf
nop
_____249:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

