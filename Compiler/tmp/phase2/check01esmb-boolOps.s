.global	b1, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	10
______3: .word	5
_____11: .asciz "PRINT0: "
.align	4
_____14: .asciz "\n"
.align	4
_____15: .asciz "BAD: "
.align	4
_____18: .asciz "\n"
.align	4
_____26: .asciz "PRINT1: "
.align	4
_____29: .asciz "\n"
.align	4
_____30: .asciz "BAD: "
.align	4
_____33: .asciz "\n"
.align	4
_____41: .asciz "BAD: "
.align	4
_____44: .asciz "\n"
.align	4
_____45: .asciz "PRINT2: "
.align	4
_____48: .asciz "\n"
.align	4
_____56: .asciz "BAD: "
.align	4
_____59: .asciz "\n"
.align	4
_____60: .asciz "PRINT3: "
.align	4
_____63: .asciz "\n"
.align	4
_____71: .asciz "PRINT4: "
.align	4
_____74: .asciz "\n"
.align	4
_____83: .asciz "PRINT5: "
.align	4
_____86: .asciz "\n"
.align	4
______91: .word	0
_____96: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::b1
b1:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	10,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-4,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	5,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-8,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____7 
 nop
set	-12,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____8 
 nop
_____7:
set	1,%l3
set	-12,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____8:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	b1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____9 
 nop
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____12 
 nop
set	_boolF,%o1
ba  _____13 
 nop
_____12:
set	_boolT,%o1
_____13:
call printf
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
ba  _____10 
 nop
_____9:
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____16 
 nop
set	_boolF,%o1
ba  _____17 
 nop
_____16:
set	_boolT,%o1
_____17:
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
_____10:
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____22 
 nop
set	-24,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____23 
 nop
_____22:
set	1,%l3
set	-24,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____23:
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	b1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____24 
 nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____27 
 nop
set	_boolF,%o1
ba  _____28 
 nop
_____27:
set	_boolT,%o1
_____28:
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
ba  _____25 
 nop
_____24:
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____31 
 nop
set	_boolF,%o1
ba  _____32 
 nop
_____31:
set	_boolT,%o1
_____32:
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
_____25:
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-44,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____37 
 nop
set	-36,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____38 
 nop
_____37:
set	1,%l3
set	-36,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____38:
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	b1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____39 
 nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____42 
 nop
set	_boolF,%o1
ba  _____43 
 nop
_____42:
set	_boolT,%o1
_____43:
call printf
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
ba  _____40 
 nop
_____39:
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____46 
 nop
set	_boolF,%o1
ba  _____47 
 nop
_____46:
set	_boolT,%o1
_____47:
call printf
nop
set	_strFmt,%o0
set	_____48,%o1
call printf
nop
_____40:
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-56,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____52 
 nop
set	-48,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____53 
 nop
_____52:
set	1,%l3
set	-48,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____53:
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	b1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____54 
 nop
set	_strFmt,%o0
set	_____56,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____57 
 nop
set	_boolF,%o1
ba  _____58 
 nop
_____57:
set	_boolT,%o1
_____58:
call printf
nop
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
ba  _____55 
 nop
_____54:
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____61 
 nop
set	_boolF,%o1
ba  _____62 
 nop
_____61:
set	_boolT,%o1
_____62:
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
_____55:
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-68,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
be  _____67 
 nop
set	-60,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____68 
 nop
_____67:
set	1,%l3
set	-60,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____68:
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	b1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____69 
 nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____72 
 nop
set	_boolF,%o1
ba  _____73 
 nop
_____72:
set	_boolT,%o1
_____73:
call printf
nop
set	_strFmt,%o0
set	_____74,%o1
call printf
nop
ba  _____70 
 nop
_____69:
_____70:
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____78 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____79 
 nop
_____78:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____79:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	b1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	b1,%l3
add 	%g0, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-84,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____81 
 nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	b1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____84 
 nop
set	_boolF,%o1
ba  _____85 
 nop
_____84:
set	_boolT,%o1
_____85:
call printf
nop
set	_strFmt,%o0
set	_____86,%o1
call printf
nop
ba  _____82 
 nop
_____81:
_____82:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 84) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____92 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____93 
 nop
_____92:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____93:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____94 
 nop
ba  _____95 
 nop
_____94:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
_____95:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

