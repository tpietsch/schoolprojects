.global	c, e, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	6
______3: .word	2
______4: .word	3
_____5: .single	0r4.1
______6: .single	0r4.1
_____10: .asciz "\n"
.align	4
_____14: .asciz "\n"
.align	4
_____21: .asciz "\n"
.align	4
______22: .word	4
_____24: .single	0r3.0
______25: .single	0r3.0
______26: .word	7
______27: .word	8
_____29: .single 0r7.0
_____30: .asciz "\n"
.align	4
______35: .word	0
_____40: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
!CodeGen::doVarDecl::e
e:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	6,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	c,%l1
add 	%g0, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	c,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
set	2,%l1
set	e,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	e,%l1
add 	%g0, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	e,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
_init_done:
set	3,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______6,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-12,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-20,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-32,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-32,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-36,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o0
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %o1
call .mul
nop
mov	%o0, %l2
set	-48,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-56,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-60,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	4,%l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-68,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-72,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	7,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-76,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	8,%l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-80,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	e,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	c,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_____29,%l0
ld	[%l0], %f0
call printFloat
nop
set	e,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 80) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____36 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____37 
 nop
_____36:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____37:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____38 
 nop
ba  _____39 
 nop
_____38:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
_____39:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

