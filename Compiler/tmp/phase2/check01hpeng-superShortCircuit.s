.global	t, f, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	0
______4: .word	1
______48: .word	0
______55: .word	0
______56: .word	1
_____59: .asciz "should still be true: "
.align	4
_____62: .asciz "\n"
.align	4
______107: .word	0
______113: .word	0
______114: .word	1
_____117: .asciz "should still be true: "
.align	4
_____120: .asciz "\n"
.align	4
______125: .word	0
_____130: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::t
t:	.skip	4
!CodeGen::doVarDecl::f
f:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	t,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	f,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
cmp	%l1, %g0
be  _____5 
 nop
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	0,%l5
xor	%l4, %l5, %l5
set	-8,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	1,%l1
cmp	%l1, %g0
be  _____8 
 nop
ba  _____10 
 nop
ba  _____9 
 nop
_____8:
ba  _____9 
 nop
_____5:
_____9:
_____6:
ba  _____13 
 nop
_____10:
_____13:
set	1,%l1
cmp	%l1, %g0
be  _____14 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____16 
 nop
ba  _____18 
 nop
ba  _____17 
 nop
_____16:
_____17:
set	1,%l1
cmp	%l1, %g0
be  _____19 
 nop
ba  _____18 
 nop
ba  _____20 
 nop
_____19:
_____20:
ba  _____23 
 nop
_____18:
_____23:
set	1,%l1
cmp	%l1, %g0
be  _____24 
 nop
ba  _____26 
 nop
ba  _____25 
 nop
_____24:
ba  _____25 
 nop
_____14:
_____25:
_____15:
ba  _____29 
 nop
_____26:
_____29:
set	1,%l1
cmp	%l1, %g0
be  _____30 
 nop
set	1,%l1
cmp	%l1, %g0
be  _____32 
 nop
ba  _____34 
 nop
ba  _____33 
 nop
_____32:
ba  _____33 
 nop
_____30:
_____33:
_____31:
ba  _____37 
 nop
_____34:
_____37:
set	1,%l1
cmp	%l1, %g0
be  _____38 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____40 
 nop
ba  _____42 
 nop
ba  _____41 
 nop
_____40:
ba  _____41 
 nop
_____38:
_____41:
_____39:
ba  _____45 
 nop
_____42:
_____45:
set	0,%l1
cmp	%l1, %g0
be  _____46 
 nop
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____49 
 nop
ba  _____51 
 nop
ba  _____50 
 nop
_____49:
ba  _____50 
 nop
_____46:
_____50:
_____47:
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____54 
 nop
_____51:
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____54:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____57 
 nop
ba  _____58 
 nop
_____57:
_____58:
set	_strFmt,%o0
set	_____59,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____60 
 nop
set	_boolF,%o1
ba  _____61 
 nop
_____60:
set	_boolT,%o1
_____61:
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	0,%l1
cmp	%l1, %g0
be  _____63 
 nop
ba  _____65 
 nop
ba  _____64 
 nop
_____63:
_____64:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	1,%l5
xor	%l4, %l5, %l5
set	-16,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0,%l1
cmp	%l1, %g0
be  _____67 
 nop
ba  _____65 
 nop
ba  _____68 
 nop
_____67:
_____68:
ba  _____71 
 nop
_____65:
_____71:
set	0,%l1
cmp	%l1, %g0
be  _____72 
 nop
ba  _____74 
 nop
ba  _____73 
 nop
_____72:
_____73:
set	1,%l1
cmp	%l1, %g0
be  _____75 
 nop
set	0,%l1
cmp	%l1, %g0
be  _____77 
 nop
ba  _____74 
 nop
ba  _____78 
 nop
_____77:
ba  _____78 
 nop
_____75:
_____78:
_____76:
ba  _____81 
 nop
_____74:
_____81:
set	0,%l1
cmp	%l1, %g0
be  _____82 
 nop
ba  _____84 
 nop
ba  _____83 
 nop
_____82:
_____83:
ba  _____87 
 nop
_____84:
_____87:
set	0,%l1
cmp	%l1, %g0
be  _____88 
 nop
ba  _____90 
 nop
ba  _____89 
 nop
_____88:
_____89:
set	0,%l1
cmp	%l1, %g0
be  _____91 
 nop
ba  _____90 
 nop
ba  _____92 
 nop
_____91:
_____92:
ba  _____95 
 nop
_____90:
_____95:
set	0,%l1
cmp	%l1, %g0
be  _____96 
 nop
ba  _____98 
 nop
ba  _____97 
 nop
_____96:
_____97:
set	1,%l1
cmp	%l1, %g0
be  _____99 
 nop
ba  _____98 
 nop
ba  _____100 
 nop
_____99:
_____100:
ba  _____103 
 nop
_____98:
_____103:
set	1,%l1
cmp	%l1, %g0
be  _____104 
 nop
ba  _____106 
 nop
ba  _____105 
 nop
_____104:
_____105:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____108 
 nop
ba  _____106 
 nop
ba  _____109 
 nop
_____108:
_____109:
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____112 
 nop
_____106:
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____112:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____115 
 nop
ba  _____116 
 nop
_____115:
_____116:
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____118 
 nop
set	_boolF,%o1
ba  _____119 
 nop
_____118:
set	_boolT,%o1
_____119:
call printf
nop
set	_strFmt,%o0
set	_____120,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 20) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____126 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____127 
 nop
_____126:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____127:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____128 
 nop
ba  _____129 
 nop
_____128:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____130,%o1
call printf
nop
_____129:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

