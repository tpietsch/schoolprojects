.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______3: .word	0
_____6: .asciz "fail1"
.align	4
_____7: .asciz "\n"
.align	4
_____18: .asciz "fail2"
.align	4
_____19: .asciz "\n"
.align	4
______28: .word	0
______29: .word	1
______38: .word	0
______39: .word	1
_____42: .asciz "fail3"
.align	4
_____43: .asciz "\n"
.align	4
_____44: .asciz "pass"
.align	4
_____45: .asciz "\n"
.align	4
______47: .word	1
_____51: .asciz "Number passed is: "
.align	4
_____52: .asciz "/1"
.align	4
_____53: .asciz "\n"
.align	4
______58: .word	0
_____63: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
cmp	%l1, %g0
be  _____4 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
ba  _____5 
 nop
_____4:
set	0,%l1
cmp	%l1, %g0
be  _____8 
 nop
set	1,%l1
cmp	%l1, %g0
be  _____10 
 nop
ba  _____12 
 nop
ba  _____11 
 nop
_____10:
ba  _____11 
 nop
_____8:
_____11:
_____9:
ba  _____15 
 nop
_____12:
_____15:
set	0,%l1
cmp	%l1, %g0
be  _____16 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
ba  _____17 
 nop
_____16:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____20 
 nop
ba  _____22 
 nop
ba  _____21 
 nop
_____20:
_____21:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____23 
 nop
ba  _____22 
 nop
ba  _____24 
 nop
_____23:
_____24:
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____27 
 nop
_____22:
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____27:
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____30 
 nop
ba  _____32 
 nop
ba  _____31 
 nop
_____30:
_____31:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____33 
 nop
ba  _____32 
 nop
ba  _____34 
 nop
_____33:
_____34:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____37 
 nop
_____32:
set	1,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____37:
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____40 
 nop
set	_strFmt,%o0
set	_____42,%o1
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
ba  _____41 
 nop
_____40:
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	_strFmt,%o0
set	_____45,%o1
call printf
nop
set	1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-28,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____41:
_____17:
_____5:
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	_strFmt,%o0
set	_____53,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 32) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____59 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____60 
 nop
_____59:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____60:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____61 
 nop
ba  _____62 
 nop
_____61:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
_____62:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

