.global	i1, i2, f1, f2, f3, f4, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	1
______3: .word	2
_____4: .single	0r1.0
______5: .single	0r1.0
_____6: .single	0r2.0
______7: .single	0r2.0
______8: .single	0r2.0
_____9: .single	0r1.0
______10: .single	0r1.0
______11: .word	3
_____12: .single	0r1.0
______13: .word	0
_____14: .asciz "a: "
.align	4
_____17: .asciz "\n"
.align	4
______18: .word	1
_____19: .asciz "b: "
.align	4
_____22: .asciz "\n"
.align	4
_____23: .single	0r1.0
______26: .single	0r1.0
_____30: .asciz "c: "
.align	4
_____33: .asciz "\n"
.align	4
______37: .word	2
_____40: .asciz "d: "
.align	4
_____43: .asciz "\n"
.align	4
_____49: .asciz "e: "
.align	4
_____52: .asciz "\n"
.align	4
_____58: .asciz "e: "
.align	4
_____61: .asciz "\n"
.align	4
______66: .word	0
_____71: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::i1
i1:	.skip	4
!CodeGen::doVarDecl::i2
i2:	.skip	4
!CodeGen::doVarDecl::f1
f1:	.skip	4
!CodeGen::doVarDecl::f2
f2:	.skip	4
!CodeGen::doVarDecl::f3
f3:	.skip	4
!CodeGen::doVarDecl::f4
f4:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	1,%l1
set	i1,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	i2,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f1,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f2,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______7,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f3,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______8,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f4,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
_init_done:
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	______10,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	3,%l1
set	-28,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____14,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____15 
 nop
set	_boolF,%o1
ba  _____16 
 nop
_____15:
set	_boolT,%o1
_____16:
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	1,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____20 
 nop
set	_boolF,%o1
ba  _____21 
 nop
_____20:
set	_boolT,%o1
_____21:
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	______26,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	i1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-40,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-40,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____28 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____29 
 nop
_____28:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____29:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____31 
 nop
set	_boolF,%o1
ba  _____32 
 nop
_____31:
set	_boolT,%o1
_____32:
call printf
nop
set	_strFmt,%o0
set	_____33,%o1
call printf
nop
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	2,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-52,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-52,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____38 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____39 
 nop
_____38:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____39:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____41 
 nop
set	_boolF,%o1
ba  _____42 
 nop
_____41:
set	_boolT,%o1
_____42:
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f1,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f2,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____47 
 nop
set	-56,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____48 
 nop
_____47:
set	1,%l3
set	-56,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____48:
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____50 
 nop
set	_boolF,%o1
ba  _____51 
 nop
_____50:
set	_boolT,%o1
_____51:
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-72,%l1
add 	%fp, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	-72,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____56 
 nop
set	-68,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____57 
 nop
_____56:
set	1,%l3
set	-68,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____57:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____59 
 nop
set	_boolF,%o1
ba  _____60 
 nop
_____59:
set	_boolT,%o1
_____60:
call printf
nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 76) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____67 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____68 
 nop
_____67:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____68:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____69 
 nop
ba  _____70 
 nop
_____69:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____71,%o1
call printf
nop
_____70:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

