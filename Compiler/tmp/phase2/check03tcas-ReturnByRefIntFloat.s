.global	i, f, foo, mutate, passf, realpassf, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	23
_____3: .single	0r23.0
______4: .single	0r23.0
______5: .word	42
_____7: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____10: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____12: .asciz "i = 23 = "
.align	4
_____15: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____16: .asciz "\n"
.align	4
_____21: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____22: .asciz "i = 42 = "
.align	4
_____23: .asciz "\n"
.align	4
_____24: .asciz "f = 23.00 = "
.align	4
_____25: .asciz "\n"
.align	4
_____27: .single	0r42.0
______28: .single	0r42.0
_____30: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____31: .asciz "f = 42.00 = "
.align	4
_____32: .asciz "\n"
.align	4
_____35: .single	0r1287.0
______36: .single	0r1287.0
_____38: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____39: .asciz "f = 1287.00 = "
.align	4
_____40: .asciz "\n"
.align	4
______45: .word	0
_____50: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::i
i:	.skip	4
!CodeGen::doVarDecl::f
f:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	23,%l1
set	i,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______4,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
_init_done:
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	-4,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	-8,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____14 
 nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	1, %o0
call exit
nop
_____14:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
add	%sp, -0, %sp
set	foo, %l1
call %l1
nop
set	-12,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-12, %l1
add	%fp, %l1, %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____20 
 nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	1, %o0
call exit
nop
_____20:
ld	[%l0], %o0
add	%sp, -0, %sp
set	mutate, %l1
call %l1
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	i,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f3
add	%sp, -0, %sp
set	passf, %l1
call %l1
nop
set	-20,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	______28,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____29 
 nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
_____29:
st	%f1, [%l0]
set	_strFmt,%o0
set	_____31,%o1
call printf
nop
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	f, %l1
add	%g0, %l1, %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
add	%sp, -0, %sp
set	realpassf, %l1
call %l1
nop
set	-28,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	______36,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____37 
 nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	1, %o0
call exit
nop
_____37:
st	%f1, [%l0]
set	_strFmt,%o0
set	_____39,%o1
call printf
nop
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 28) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	i, %i0
add	%g0, %i0, %i0
ret
restore

ret
restore

SAVE.foo = -(92 + 0) & -8

mutate: 
set SAVE.mutate, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	42,%l1
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	1, %o0
call exit
nop
_____6:
st	%l1, [%l0]
ret
restore

SAVE.mutate = -(92 + 4) & -8

passf: 
set SAVE.passf, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%f3, [%l0]
set	f, %i0
add	%g0, %i0, %i0
ret
restore

ret
restore

SAVE.passf = -(92 + 8) & -8

realpassf: 
set SAVE.realpassf, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-4, %i0
add	%fp, %i0, %i0
cmp	%i0, %g0
bne  _____9 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1, %o0
call exit
nop
_____9:
ld	[%i0], %i0
ret
restore

ret
restore

SAVE.realpassf = -(92 + 4) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____46 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____47 
 nop
_____46:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____47:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____48 
 nop
ba  _____49 
 nop
_____48:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
_____49:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

