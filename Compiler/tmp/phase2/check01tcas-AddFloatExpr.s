.global	x, y, z, c, f, a, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	4
______3: .word	5
_____4: .single	0r5.5
______5: .single	0r5.5
_____6: .single	0r5.5
______7: .single	0r5.5
______10: .single	0r5.5
______20: .word	5
_____25: .asciz "\n"
.align	4
______30: .word	0
_____35: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
x:	.skip	4
!CodeGen::doVarDecl::y
y:	.skip	4
!CodeGen::doVarDecl::z
z:	.skip	4
!CodeGen::doVarDecl::c
c:	.skip	4
!CodeGen::doVarDecl::f
f:	.skip	4
!CodeGen::doVarDecl::a
a:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	4,%l1
set	x,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	x,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	z,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	5,%l1
set	c,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	______5,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	f,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	______7,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	a,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
_init_done:
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______10,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-12,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-24,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-36,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	5,%l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-44,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-44,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-48,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-56,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-56,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-60,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-60,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	f,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 60) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____31 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____32 
 nop
_____31:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____32:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____33 
 nop
ba  _____34 
 nop
_____33:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
_____34:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

