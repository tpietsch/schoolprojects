.global	__________.MEMLEAK,main
.section	".data"
.align	4
_____2: .single	0r2.5
______3: .single	0r2.5
_____4: .single	0r1.0
______6: .word	0
______8: .single	0r1.0
______10: .single	0r-1.0
_____11: .asciz "2.5+(-1.0) = 1.5, "
.align	4
_____15: .asciz "\n"
.align	4
_____16: .asciz "2.5 - (-1.0) = 3.5, "
.align	4
_____20: .asciz "\n"
.align	4
_____21: .asciz "2.5 * (-1.0) = -2.5, "
.align	4
_____25: .asciz "\n"
.align	4
_____26: .asciz "2.5 / (-1.0) = -2.5, "
.align	4
_____30: .asciz "\n"
.align	4
_____38: .asciz "a>b, "
.align	4
_____46: .asciz "\n"
.align	4
_____54: .asciz "b>a, wrong"
.align	4
_____55: .asciz "\n"
.align	4
______62: .word	0
_____67: .asciz "a+b>0, "
.align	4
______74: .word	0
_____79: .asciz "\n"
.align	4
_____80: .single	0r12.2
______83: .single	0r12.2
______88: .word	0
_____93: .asciz "b-12.2>0, wrong"
.align	4
_____94: .asciz "\n"
.align	4
_____95: .single	0r2.4
______99: .single	0r2.4
_____104: .asciz "2.5>=2.4, "
.align	4
_____105: .single	0r2.4
______109: .single	0r2.4
_____114: .asciz "\n"
.align	4
_____115: .single	0r8.9
_____116: .single	0r8.9
_____119: .asciz "8.9>=8.9, "
.align	4
_____120: .single	0r8.9
_____121: .single	0r8.9
_____122: .asciz "true"
.align	4
_____123: .asciz "\n"
.align	4
_____131: .asciz "a>=b is true, "
.align	4
_____139: .asciz "\n"
.align	4
_____147: .asciz "b<=a is true, "
.align	4
_____155: .asciz "\n"
.align	4
_____156: .single	0r1.0
______158: .word	0
______160: .single	0r1.0
_____164: .asciz "-1.0 >= 0 is false, wrong"
.align	4
_____165: .asciz "\n"
.align	4
______169: .word	0
_____174: .asciz "-1.0 <= 0 is true, "
.align	4
______178: .word	0
_____183: .asciz "\n"
.align	4
_____191: .asciz "b<a is true, "
.align	4
_____199: .asciz "\n"
.align	4
_____200: .single	0r3.9
_____203: .asciz "3.9 < 10 is true, "
.align	4
_____204: .single	0r3.9
_____205: .asciz "true"
.align	4
_____206: .asciz "\n"
.align	4
_____217: .asciz "a>b is true"
.align	4
_____218: .asciz "\n"
.align	4
_____229: .asciz "b>a is false"
.align	4
_____230: .asciz "\n"
.align	4
_____241: .asciz "a>b is false, wrong"
.align	4
_____242: .asciz "\n"
.align	4
_____253: .asciz "b<a is false, wrong"
.align	4
_____254: .asciz "\n"
.align	4
_____262: .asciz "a!=b"
.align	4
_____263: .asciz "\n"
.align	4
_____274: .asciz "a!=b is true"
.align	4
_____275: .asciz "\n"
.align	4
_____287: .asciz "a>b==!false"
.align	4
_____288: .asciz "\n"
.align	4
_____297: .asciz "not b>a is true"
.align	4
_____298: .asciz "\n"
.align	4
______302: .word	0
______310: .word	0
______319: .word	0
______320: .word	1
_____323: .asciz "a<0 and b<0, wrong"
.align	4
_____324: .asciz "\n"
.align	4
______328: .word	0
______337: .word	0
______345: .word	0
______346: .word	1
_____349: .asciz "a>0 or b >0 is true"
.align	4
_____350: .asciz "\n"
.align	4
______352: .word	0
______357: .word	0
_____362: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	______3,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-4,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______8,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-12,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-8,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-16,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	______10,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-24,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-28,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-32,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____15,%o1
call printf
nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-44,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-52,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-48,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-52,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fmuls	%f0, %f1, %f2
set	-56,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-60,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-64,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-60,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f0
set	-64,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f1
fdivs	%f0, %f1, %f2
set	-68,%l3
add 	%fp, %l3, %l3
st	%f2, [%l3]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-76,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-80,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____34 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____35 
 nop
_____34:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____35:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____36 
 nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-88,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-92,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-92,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____42 
 nop
set	-84,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____43 
 nop
_____42:
set	1,%l3
set	-84,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____43:
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____44 
 nop
set	_boolF,%o1
ba  _____45 
 nop
_____44:
set	_boolT,%o1
_____45:
call printf
nop
set	_strFmt,%o0
set	_____46,%o1
call printf
nop
ba  _____37 
 nop
_____36:
_____37:
set	0, %l0
set	-96,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-100,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-100,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-104,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-100,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-104,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____50 
 nop
set	-96,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____51 
 nop
_____50:
set	1,%l3
set	-96,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____51:
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____52 
 nop
set	_strFmt,%o0
set	_____54,%o1
call printf
nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
ba  _____53 
 nop
_____52:
_____53:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-108,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-112,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-108,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-112,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-116,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	0, %l0
set	-120,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-124,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-116,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-124,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-128,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____63 
 nop
set	-120,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____64 
 nop
_____63:
set	1,%l3
set	-120,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____64:
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____65 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-132,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-136,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-132,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-136,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-140,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	0, %l0
set	-144,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-148,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-140,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-148,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-152,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-152,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____75 
 nop
set	-144,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____76 
 nop
_____75:
set	1,%l3
set	-144,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____76:
set	-144,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____77 
 nop
set	_boolF,%o1
ba  _____78 
 nop
_____77:
set	_boolT,%o1
_____78:
call printf
nop
set	_strFmt,%o0
set	_____79,%o1
call printf
nop
ba  _____66 
 nop
_____65:
_____66:
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-156,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______83,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-160,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-156,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-160,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-164,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	0, %l0
set	-168,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-172,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-172,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-176,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-172,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____89 
 nop
set	-168,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____90 
 nop
_____89:
set	1,%l3
set	-168,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____90:
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____91 
 nop
set	_strFmt,%o0
set	_____93,%o1
call printf
nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
ba  _____92 
 nop
_____91:
_____92:
set	0, %l0
set	-180,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-184,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-184,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______99,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-188,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____100 
 nop
set	-180,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____101 
 nop
_____100:
set	1,%l3
set	-180,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____101:
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____102 
 nop
set	_strFmt,%o0
set	_____104,%o1
call printf
nop
set	0, %l0
set	-192,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-196,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-196,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	______109,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-200,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____110 
 nop
set	-192,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____111 
 nop
_____110:
set	1,%l3
set	-192,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____111:
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____112 
 nop
set	_boolF,%o1
ba  _____113 
 nop
_____112:
set	_boolT,%o1
_____113:
call printf
nop
set	_strFmt,%o0
set	_____114,%o1
call printf
nop
ba  _____103 
 nop
_____102:
_____103:
set	1,%l1
cmp	%l1, %g0
be  _____117 
 nop
set	_strFmt,%o0
set	_____119,%o1
call printf
nop
set	_strFmt,%o0
set	_____122,%o1
call printf
nop
set	_strFmt,%o0
set	_____123,%o1
call printf
nop
ba  _____118 
 nop
_____117:
_____118:
set	0, %l0
set	-204,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-208,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-208,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-212,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____127 
 nop
set	-204,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____128 
 nop
_____127:
set	1,%l3
set	-204,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____128:
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____129 
 nop
set	_strFmt,%o0
set	_____131,%o1
call printf
nop
set	0, %l0
set	-216,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-220,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-220,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-224,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-220,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-224,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bge  _____135 
 nop
set	-216,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____136 
 nop
_____135:
set	1,%l3
set	-216,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____136:
set	-216,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____137 
 nop
set	_boolF,%o1
ba  _____138 
 nop
_____137:
set	_boolT,%o1
_____138:
call printf
nop
set	_strFmt,%o0
set	_____139,%o1
call printf
nop
ba  _____130 
 nop
_____129:
_____130:
set	0, %l0
set	-228,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-232,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-232,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-236,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-232,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-236,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____143 
 nop
set	-228,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____144 
 nop
_____143:
set	1,%l3
set	-228,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____144:
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____145 
 nop
set	_strFmt,%o0
set	_____147,%o1
call printf
nop
set	0, %l0
set	-240,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-244,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-244,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-248,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-244,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-248,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____151 
 nop
set	-240,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____152 
 nop
_____151:
set	1,%l3
set	-240,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____152:
set	-240,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____153 
 nop
set	_boolF,%o1
ba  _____154 
 nop
_____153:
set	_boolT,%o1
_____154:
call printf
nop
set	_strFmt,%o0
set	_____155,%o1
call printf
nop
ba  _____146 
 nop
_____145:
_____146:
set	0,%l1
set	-252,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	______160,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-256,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-252,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-252,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-252,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-256,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fsubs	%f4, %f5, %f5
set	-260,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	0,%l1
cmp	%l1, %g0
be  _____162 
 nop
set	_strFmt,%o0
set	_____164,%o1
call printf
nop
set	_strFmt,%o0
set	_____165,%o1
call printf
nop
ba  _____163 
 nop
_____162:
_____163:
set	0, %l0
set	-264,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-268,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-268,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-272,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-272,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-272,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-268,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-272,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____170 
 nop
set	-264,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____171 
 nop
_____170:
set	1,%l3
set	-264,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____171:
set	-264,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____172 
 nop
set	_strFmt,%o0
set	_____174,%o1
call printf
nop
set	0, %l0
set	-276,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-280,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-280,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-284,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-284,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-284,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-280,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-284,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____179 
 nop
set	-276,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____180 
 nop
_____179:
set	1,%l3
set	-276,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____180:
set	-276,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____181 
 nop
set	_boolF,%o1
ba  _____182 
 nop
_____181:
set	_boolT,%o1
_____182:
call printf
nop
set	_strFmt,%o0
set	_____183,%o1
call printf
nop
ba  _____173 
 nop
_____172:
_____173:
set	0, %l0
set	-288,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-292,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-292,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-296,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-292,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-296,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____187 
 nop
set	-288,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____188 
 nop
_____187:
set	1,%l3
set	-288,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____188:
set	-288,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____189 
 nop
set	_strFmt,%o0
set	_____191,%o1
call printf
nop
set	0, %l0
set	-300,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-304,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-304,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-308,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-304,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-308,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____195 
 nop
set	-300,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____196 
 nop
_____195:
set	1,%l3
set	-300,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____196:
set	-300,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____197 
 nop
set	_boolF,%o1
ba  _____198 
 nop
_____197:
set	_boolT,%o1
_____198:
call printf
nop
set	_strFmt,%o0
set	_____199,%o1
call printf
nop
ba  _____190 
 nop
_____189:
_____190:
set	1,%l1
cmp	%l1, %g0
be  _____201 
 nop
set	_strFmt,%o0
set	_____203,%o1
call printf
nop
set	_strFmt,%o0
set	_____205,%o1
call printf
nop
set	_strFmt,%o0
set	_____206,%o1
call printf
nop
ba  _____202 
 nop
_____201:
_____202:
set	0, %l0
set	-312,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-316,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-316,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-320,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-316,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-320,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____210 
 nop
set	-312,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____211 
 nop
_____210:
set	1,%l3
set	-312,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____211:
set	0, %l0
set	-324,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-312,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
be  _____213 
 nop
set	-324,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____214 
 nop
_____213:
set	1,%l3
set	-324,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____214:
set	-324,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____215 
 nop
set	_strFmt,%o0
set	_____217,%o1
call printf
nop
set	_strFmt,%o0
set	_____218,%o1
call printf
nop
ba  _____216 
 nop
_____215:
_____216:
set	0, %l0
set	-328,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-332,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-332,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-336,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-332,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-336,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____222 
 nop
set	-328,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____223 
 nop
_____222:
set	1,%l3
set	-328,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____223:
set	0, %l0
set	-340,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-328,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____225 
 nop
set	-340,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____226 
 nop
_____225:
set	1,%l3
set	-340,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____226:
set	-340,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____227 
 nop
set	_strFmt,%o0
set	_____229,%o1
call printf
nop
set	_strFmt,%o0
set	_____230,%o1
call printf
nop
ba  _____228 
 nop
_____227:
_____228:
set	0, %l0
set	-344,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-348,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-348,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-352,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-348,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-352,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____234 
 nop
set	-344,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____235 
 nop
_____234:
set	1,%l3
set	-344,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____235:
set	0, %l0
set	-356,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-344,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____237 
 nop
set	-356,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____238 
 nop
_____237:
set	1,%l3
set	-356,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____238:
set	-356,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____239 
 nop
set	_strFmt,%o0
set	_____241,%o1
call printf
nop
set	_strFmt,%o0
set	_____242,%o1
call printf
nop
ba  _____240 
 nop
_____239:
_____240:
set	0, %l0
set	-360,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-364,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-364,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-368,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-364,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-368,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____246 
 nop
set	-360,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____247 
 nop
_____246:
set	1,%l3
set	-360,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____247:
set	0, %l0
set	-372,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-360,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____249 
 nop
set	-372,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____250 
 nop
_____249:
set	1,%l3
set	-372,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____250:
set	-372,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____251 
 nop
set	_strFmt,%o0
set	_____253,%o1
call printf
nop
set	_strFmt,%o0
set	_____254,%o1
call printf
nop
ba  _____252 
 nop
_____251:
_____252:
set	0, %l0
set	-376,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-380,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-380,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-384,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-380,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-384,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____258 
 nop
set	-376,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____259 
 nop
_____258:
set	1,%l3
set	-376,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____259:
set	-376,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____260 
 nop
set	_strFmt,%o0
set	_____262,%o1
call printf
nop
set	_strFmt,%o0
set	_____263,%o1
call printf
nop
ba  _____261 
 nop
_____260:
_____261:
set	0, %l0
set	-388,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-392,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-392,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-396,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-392,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-396,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bne  _____267 
 nop
set	-388,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____268 
 nop
_____267:
set	1,%l3
set	-388,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____268:
set	0, %l0
set	-400,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-388,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
be  _____270 
 nop
set	-400,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____271 
 nop
_____270:
set	1,%l3
set	-400,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____271:
set	-400,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____272 
 nop
set	_strFmt,%o0
set	_____274,%o1
call printf
nop
set	_strFmt,%o0
set	_____275,%o1
call printf
nop
ba  _____273 
 nop
_____272:
_____273:
set	0, %l0
set	-404,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-408,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-408,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-412,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-408,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-412,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____279 
 nop
set	-404,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____280 
 nop
_____279:
set	1,%l3
set	-404,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____280:
set	0, %l0
set	-416,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	0,%l5
xor	%l4, %l5, %l5
set	-416,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-420,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-404,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	1,%l2
cmp	%l1, %l2
be  _____283 
 nop
set	-420,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____284 
 nop
_____283:
set	1,%l3
set	-420,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____284:
set	-420,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____285 
 nop
set	_strFmt,%o0
set	_____287,%o1
call printf
nop
set	_strFmt,%o0
set	_____288,%o1
call printf
nop
ba  _____286 
 nop
_____285:
_____286:
set	0, %l0
set	-424,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-428,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-428,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-432,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-428,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-432,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____292 
 nop
set	-424,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____293 
 nop
_____292:
set	1,%l3
set	-424,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____293:
set	0, %l0
set	-436,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l4
set	-424,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
xor	%l4, %l5, %l5
set	-436,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-436,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____295 
 nop
set	_strFmt,%o0
set	_____297,%o1
call printf
nop
set	_strFmt,%o0
set	_____298,%o1
call printf
nop
ba  _____296 
 nop
_____295:
_____296:
set	0, %l0
set	-440,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-444,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-444,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-448,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-448,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-448,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-444,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-448,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____303 
 nop
set	-440,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____304 
 nop
_____303:
set	1,%l3
set	-440,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____304:
set	-440,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____305 
 nop
set	0, %l0
set	-452,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-456,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-456,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-460,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-460,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-460,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-456,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-460,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bl  _____311 
 nop
set	-452,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____312 
 nop
_____311:
set	1,%l3
set	-452,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____312:
set	-452,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____313 
 nop
ba  _____315 
 nop
ba  _____314 
 nop
_____313:
ba  _____314 
 nop
_____305:
_____314:
_____306:
set	0, %l0
set	-464,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-464,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____318 
 nop
_____315:
set	1,%l1
set	-464,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____318:
set	-464,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____321 
 nop
set	_strFmt,%o0
set	_____323,%o1
call printf
nop
set	_strFmt,%o0
set	_____324,%o1
call printf
nop
ba  _____322 
 nop
_____321:
_____322:
set	0, %l0
set	-468,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-472,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-472,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-476,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-476,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-476,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-472,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-476,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____329 
 nop
set	-468,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____330 
 nop
_____329:
set	1,%l3
set	-468,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____330:
set	-468,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____331 
 nop
ba  _____333 
 nop
ba  _____332 
 nop
_____331:
_____332:
set	0, %l0
set	-480,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-484,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	-484,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0,%l1
set	-488,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-488,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-488,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-484,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-488,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
bg  _____338 
 nop
set	-480,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____339 
 nop
_____338:
set	1,%l3
set	-480,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____339:
set	-480,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____340 
 nop
ba  _____333 
 nop
ba  _____341 
 nop
_____340:
_____341:
set	0, %l0
set	-492,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-492,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____344 
 nop
_____333:
set	1,%l1
set	-492,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
_____344:
set	-492,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____347 
 nop
set	_strFmt,%o0
set	_____349,%o1
call printf
nop
set	_strFmt,%o0
set	_____350,%o1
call printf
nop
ba  _____348 
 nop
_____347:
_____348:
call __________.MEMLEAK
nop
set	0,%l1
set	-496,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%i0
ret
restore

call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 496) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____358 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____359 
 nop
_____358:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____359:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____360 
 nop
ba  _____361 
 nop
_____360:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____362,%o1
call printf
nop
_____361:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

