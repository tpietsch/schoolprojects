.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	4
______5: .word	0
______10: .word	16
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "0"
.align	4
_____18: .asciz " is outside legal range [0,4).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "0"
.align	4
_____30: .asciz " is outside legal range [0,4).\n"
.align	4
______32: .word	2
_____34: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______36: .word	4
______38: .word	1
______43: .word	16
_____49: .asciz "Index value of "
.align	4
_____50: .asciz "1"
.align	4
_____51: .asciz " is outside legal range [0,4).\n"
.align	4
______56: .word	0
_____61: .asciz "Index value of "
.align	4
_____62: .asciz "1"
.align	4
_____63: .asciz " is outside legal range [0,4).\n"
.align	4
______65: .word	4
_____67: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______69: .word	4
______71: .word	2
______76: .word	16
_____82: .asciz "Index value of "
.align	4
_____83: .asciz "2"
.align	4
_____84: .asciz " is outside legal range [0,4).\n"
.align	4
______89: .word	0
_____94: .asciz "Index value of "
.align	4
_____95: .asciz "2"
.align	4
_____96: .asciz " is outside legal range [0,4).\n"
.align	4
______98: .word	6
_____100: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______102: .word	4
______104: .word	3
______109: .word	16
_____115: .asciz "Index value of "
.align	4
_____116: .asciz "3"
.align	4
_____117: .asciz " is outside legal range [0,4).\n"
.align	4
______122: .word	0
_____127: .asciz "Index value of "
.align	4
_____128: .asciz "3"
.align	4
_____129: .asciz " is outside legal range [0,4).\n"
.align	4
______131: .word	8
_____133: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______134: .word	3
_____136: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______137: .word	0
______143: .word	4
_____149: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______151: .word	1
_____155: .asciz "\n"
.align	4
______157: .word	8
______159: .word	0
______164: .word	32
_____170: .asciz "Index value of "
.align	4
_____171: .asciz "0"
.align	4
_____172: .asciz " is outside legal range [0,4).\n"
.align	4
______177: .word	0
_____182: .asciz "Index value of "
.align	4
_____183: .asciz "0"
.align	4
_____184: .asciz " is outside legal range [0,4).\n"
.align	4
_____188: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______189: .word	2
_____191: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______193: .word	8
______195: .word	1
______200: .word	32
_____206: .asciz "Index value of "
.align	4
_____207: .asciz "1"
.align	4
_____208: .asciz " is outside legal range [0,4).\n"
.align	4
______213: .word	0
_____218: .asciz "Index value of "
.align	4
_____219: .asciz "1"
.align	4
_____220: .asciz " is outside legal range [0,4).\n"
.align	4
_____224: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______225: .word	4
_____227: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______229: .word	8
______231: .word	2
______236: .word	32
_____242: .asciz "Index value of "
.align	4
_____243: .asciz "2"
.align	4
_____244: .asciz " is outside legal range [0,4).\n"
.align	4
______249: .word	0
_____254: .asciz "Index value of "
.align	4
_____255: .asciz "2"
.align	4
_____256: .asciz " is outside legal range [0,4).\n"
.align	4
_____260: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______261: .word	6
_____263: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______265: .word	8
______267: .word	3
______272: .word	32
_____278: .asciz "Index value of "
.align	4
_____279: .asciz "3"
.align	4
_____280: .asciz " is outside legal range [0,4).\n"
.align	4
______285: .word	0
_____290: .asciz "Index value of "
.align	4
_____291: .asciz "3"
.align	4
_____292: .asciz " is outside legal range [0,4).\n"
.align	4
_____296: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______297: .word	8
_____299: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______300: .word	0
______306: .word	4
_____313: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____315: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______317: .word	1
_____321: .asciz "\n"
.align	4
______326: .word	0
_____331: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	4,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-28,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	16,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	16,%l1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____33 
 nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	1, %o0
call exit
nop
_____33:
st	%l1, [%l0]
set	4,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-68,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	16,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	16,%l1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____45 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____46 
 nop
_____45:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____46:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____47 
 nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____48 
 nop
_____47:
_____48:
set	0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____57 
 nop
set	-84,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____58 
 nop
_____57:
set	1,%l3
set	-84,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____58:
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____59 
 nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____60 
 nop
_____59:
_____60:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____66 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	1, %o0
call exit
nop
_____66:
st	%l1, [%l0]
set	4,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-108,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	16,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	16,%l1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____78 
 nop
set	-112,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____79 
 nop
_____78:
set	1,%l3
set	-112,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____79:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____80 
 nop
set	_strFmt,%o0
set	_____82,%o1
call printf
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____81 
 nop
_____80:
_____81:
set	0, %l0
set	-124,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____90 
 nop
set	-124,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____91 
 nop
_____90:
set	1,%l3
set	-124,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____91:
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____92 
 nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
set	_strFmt,%o0
set	_____95,%o1
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____93 
 nop
_____92:
_____93:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	6,%l1
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____99 
 nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	1, %o0
call exit
nop
_____99:
st	%l1, [%l0]
set	4,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-148,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-152,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	16,%l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	16,%l1
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____111 
 nop
set	-152,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____112 
 nop
_____111:
set	1,%l3
set	-152,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____112:
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____113 
 nop
set	_strFmt,%o0
set	_____115,%o1
call printf
nop
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____114 
 nop
_____113:
_____114:
set	0, %l0
set	-164,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____123 
 nop
set	-164,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____124 
 nop
_____123:
set	1,%l3
set	-164,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____124:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____125 
 nop
set	_strFmt,%o0
set	_____127,%o1
call printf
nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
set	_strFmt,%o0
set	_____129,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____126 
 nop
_____125:
_____126:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%l1
set	-176,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____132 
 nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	1, %o0
call exit
nop
_____132:
st	%l1, [%l0]
set	-16, %l0
add	%fp, %l0, %l0
set	-192,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	3,%l1
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____135 
 nop
set	_strFmt,%o0
set	_____136,%o1
call printf
nop
set	1, %o0
call exit
nop
_____135:
st	%l1, [%l0]
set	0,%l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____138,_____139) 
_____138:
set	0, %l0
set	-200,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-208,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-204,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	4,%l2
cmp	%l1, %l2
bl  _____144 
 nop
set	-200,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____145 
 nop
_____144:
set	1,%l3
set	-200,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____145:
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____139 
 nop
set	0, %l0
set	-212,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	4,%l4
set	-192,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-212,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-216,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-216,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-192,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-216,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____148 
 nop
set	_strFmt,%o0
set	_____149,%o1
call printf
nop
set	1, %o0
call exit
nop
_____148:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	1,%l1
set	-220,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-224,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-224,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-228,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-196,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-232,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-228,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____138 
 nop
_____139:
set	_strFmt,%o0
set	_____155,%o1
call printf
nop
set	8,%l1
set	-268,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-272,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-276,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-280,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	32,%l1
set	-284,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-276,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-288,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	32,%l1
set	-288,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____166 
 nop
set	-280,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____167 
 nop
_____166:
set	1,%l3
set	-280,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____167:
set	-280,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____168 
 nop
set	_strFmt,%o0
set	_____170,%o1
call printf
nop
set	_strFmt,%o0
set	_____171,%o1
call printf
nop
set	_strFmt,%o0
set	_____172,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____169 
 nop
_____168:
_____169:
set	0, %l0
set	-292,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-276,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-296,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-276,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-296,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-300,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-296,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____178 
 nop
set	-292,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____179 
 nop
_____178:
set	1,%l3
set	-292,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____179:
set	-292,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____180 
 nop
set	_strFmt,%o0
set	_____182,%o1
call printf
nop
set	_strFmt,%o0
set	_____183,%o1
call printf
nop
set	_strFmt,%o0
set	_____184,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____181 
 nop
_____180:
_____181:
set	-276,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-264, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-308,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-308, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____187 
 nop
set	_strFmt,%o0
set	_____188,%o1
call printf
nop
set	1, %o0
call exit
nop
_____187:
ld	[%l1], %l1
set	-316,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-316,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	2,%l1
set	-316,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____190 
 nop
set	_strFmt,%o0
set	_____191,%o1
call printf
nop
set	1, %o0
call exit
nop
_____190:
st	%l1, [%l0]
set	8,%l1
set	-320,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-324,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-328,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-332,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	32,%l1
set	-336,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-328,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-340,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	32,%l1
set	-340,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____202 
 nop
set	-332,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____203 
 nop
_____202:
set	1,%l3
set	-332,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____203:
set	-332,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____204 
 nop
set	_strFmt,%o0
set	_____206,%o1
call printf
nop
set	_strFmt,%o0
set	_____207,%o1
call printf
nop
set	_strFmt,%o0
set	_____208,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____205 
 nop
_____204:
_____205:
set	0, %l0
set	-344,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-328,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-348,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-328,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-348,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-352,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-348,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____214 
 nop
set	-344,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____215 
 nop
_____214:
set	1,%l3
set	-344,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____215:
set	-344,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____216 
 nop
set	_strFmt,%o0
set	_____218,%o1
call printf
nop
set	_strFmt,%o0
set	_____219,%o1
call printf
nop
set	_strFmt,%o0
set	_____220,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____217 
 nop
_____216:
_____217:
set	-328,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-264, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-360,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-360, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____223 
 nop
set	_strFmt,%o0
set	_____224,%o1
call printf
nop
set	1, %o0
call exit
nop
_____223:
ld	[%l1], %l1
set	-368,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-368,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	4,%l1
set	-368,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____226 
 nop
set	_strFmt,%o0
set	_____227,%o1
call printf
nop
set	1, %o0
call exit
nop
_____226:
st	%l1, [%l0]
set	8,%l1
set	-372,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-376,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-380,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-384,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	32,%l1
set	-388,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-380,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-392,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	32,%l1
set	-392,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____238 
 nop
set	-384,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____239 
 nop
_____238:
set	1,%l3
set	-384,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____239:
set	-384,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____240 
 nop
set	_strFmt,%o0
set	_____242,%o1
call printf
nop
set	_strFmt,%o0
set	_____243,%o1
call printf
nop
set	_strFmt,%o0
set	_____244,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____241 
 nop
_____240:
_____241:
set	0, %l0
set	-396,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-380,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-400,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-380,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-400,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-404,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-400,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____250 
 nop
set	-396,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____251 
 nop
_____250:
set	1,%l3
set	-396,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____251:
set	-396,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____252 
 nop
set	_strFmt,%o0
set	_____254,%o1
call printf
nop
set	_strFmt,%o0
set	_____255,%o1
call printf
nop
set	_strFmt,%o0
set	_____256,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____253 
 nop
_____252:
_____253:
set	-380,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-264, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-412,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-412, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____259 
 nop
set	_strFmt,%o0
set	_____260,%o1
call printf
nop
set	1, %o0
call exit
nop
_____259:
ld	[%l1], %l1
set	-420,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-420,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	6,%l1
set	-420,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____262 
 nop
set	_strFmt,%o0
set	_____263,%o1
call printf
nop
set	1, %o0
call exit
nop
_____262:
st	%l1, [%l0]
set	8,%l1
set	-424,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	3,%l1
set	-428,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	8,%o0
set	3,%o1
call .mul
nop
mov	%o0, %l2
set	-432,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-436,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	32,%l1
set	-440,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-432,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-444,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	32,%l1
set	-444,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____274 
 nop
set	-436,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____275 
 nop
_____274:
set	1,%l3
set	-436,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____275:
set	-436,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____276 
 nop
set	_strFmt,%o0
set	_____278,%o1
call printf
nop
set	_strFmt,%o0
set	_____279,%o1
call printf
nop
set	_strFmt,%o0
set	_____280,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____277 
 nop
_____276:
_____277:
set	0, %l0
set	-448,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-432,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-452,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-432,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-452,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-456,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-452,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____286 
 nop
set	-448,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____287 
 nop
_____286:
set	1,%l3
set	-448,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____287:
set	-448,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____288 
 nop
set	_strFmt,%o0
set	_____290,%o1
call printf
nop
set	_strFmt,%o0
set	_____291,%o1
call printf
nop
set	_strFmt,%o0
set	_____292,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____289 
 nop
_____288:
_____289:
set	-432,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-264, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-464,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-464, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____295 
 nop
set	_strFmt,%o0
set	_____296,%o1
call printf
nop
set	1, %o0
call exit
nop
_____295:
ld	[%l1], %l1
set	-472,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-472,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	8,%l1
set	-472,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____298 
 nop
set	_strFmt,%o0
set	_____299,%o1
call printf
nop
set	1, %o0
call exit
nop
_____298:
st	%l1, [%l0]
set	-264, %l0
add	%fp, %l0, %l0
set	-504,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	0,%l1
set	-508,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
! beginning of while loop(_____301,_____302) 
_____301:
set	0, %l0
set	-512,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-508,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-516,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-508,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-516,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%l1
set	-520,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-516,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	4,%l2
cmp	%l1, %l2
bl  _____307 
 nop
set	-512,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____308 
 nop
_____307:
set	1,%l3
set	-512,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____308:
set	-512,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____302 
 nop
set	0, %l0
set	-524,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	8,%l4
set	-504,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-524,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-528,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-504,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-528,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-524,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-504,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-528, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____312 
 nop
set	_strFmt,%o0
set	_____313,%o1
call printf
nop
set	1, %o0
call exit
nop
_____312:
ld	[%l1], %l1
set	-536,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-536,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-536,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____314 
 nop
set	_strFmt,%o0
set	_____315,%o1
call printf
nop
set	1, %o0
call exit
nop
_____314:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	1,%l1
set	-540,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-508,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-544,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l4
set	-544,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-548,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-508,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-552,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-548,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-508,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
ba  _____301 
 nop
_____302:
set	_strFmt,%o0
set	_____321,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 552) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____327 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____328 
 nop
_____327:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____328:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____329 
 nop
ba  _____330 
 nop
_____329:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____331,%o1
call printf
nop
_____330:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

