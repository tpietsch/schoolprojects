.global	foo, add, sub, _0x112, _0y113, fp, __________.MEMLEAK,main
.section	".data"
.align	4
_____2: .asciz "Welcome to function pointers "
.align	4
_____3: .asciz "\n"
.align	4
______5: .word	1
_____18: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____23: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____24: .asciz "\n"
.align	4
_____29: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____30: .asciz "\n"
.align	4
______35: .word	0
_____40: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
_0x112:	.skip	4
!CodeGen::doVarDecl::y
_0y113:	.skip	4
!CodeGen::doVarDecl::fp
fp:	.skip	8
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	_0x112,%l0
add 	%g0, %l0, %l0
st	%i0, [%l0]
set	_0y113,%l0
add 	%g0, %l0, %l0
st	%i1, [%l0]
_init_done:
set	foo, %l1
set	fp,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	fp, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
set	fp, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____15 
 nop
mov	%l1, %o0
set	4,%o1
set	3,%o2
add	%sp, -0, %sp
ba  _____16 
 nop
_____15:
set	4,%o0
set	3,%o1
add	%sp, -0, %sp
_____16:
_____14:
set	fp,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____17 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
_____17:
call %l1
nop
set	-4,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	add, %l1
set	fp,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	fp, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
set	fp, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____20 
 nop
mov	%l1, %o0
set	4,%o1
set	3,%o2
add	%sp, -0, %sp
ba  _____21 
 nop
_____20:
set	4,%o0
set	3,%o1
add	%sp, -0, %sp
_____21:
_____19:
set	fp,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____22 
 nop
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	1, %o0
call exit
nop
_____22:
call %l1
nop
set	-8,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	sub, %l1
set	fp,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	fp, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
st	%g0, [%l1]
set	fp, %l0
add	%g0, %l0, %l0
add	4, %l0, %l1
ld	[%l1], %l1
cmp	%l1, %g0
beq  _____26 
 nop
mov	%l1, %o0
set	4,%o1
set	3,%o2
add	%sp, -0, %sp
ba  _____27 
 nop
_____26:
set	4,%o0
set	3,%o1
add	%sp, -0, %sp
_____27:
_____25:
set	fp,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
bne  _____28 
 nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	1, %o0
call exit
nop
_____28:
call %l1
nop
set	-12,%l0
add 	%fp, %l0, %l0
st	%o0, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 12) & -8

foo: 
set SAVE.foo, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	_strFmt,%o0
set	_____2,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____3,%o1
call printf
nop
set	1,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%i0
ret
restore

ret
restore

SAVE.foo = -(92 + 12) & -8

add: 
set SAVE.add, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-20,%l1
add 	%fp, %l1, %l1
ld	[%l1], %i0
ret
restore

ret
restore

SAVE.add = -(92 + 20) & -8

sub: 
set SAVE.sub, %g1
save %sp, %g1, %sp
set	-4,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
sub	%l4, %l5, %l5
set	-20,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	-20,%l1
add 	%fp, %l1, %l1
ld	[%l1], %i0
ret
restore

ret
restore

SAVE.sub = -(92 + 20) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____36 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____37 
 nop
_____36:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____37:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____38 
 nop
ba  _____39 
 nop
_____38:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____40,%o1
call printf
nop
_____39:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

