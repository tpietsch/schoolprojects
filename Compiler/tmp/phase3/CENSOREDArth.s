.global	g, f, __________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	0
______3: .word	1
_____4: .asciz "g "
.align	4
_____5: .asciz "\n"
.align	4
______7: .word	1
_____11: .asciz "g++ "
.align	4
_____12: .asciz "\n"
.align	4
______15: .word	1
_____17: .asciz "g + 1 "
.align	4
_____18: .asciz "\n"
.align	4
_____19: .asciz "f "
.align	4
_____20: .asciz "\n"
.align	4
_____24: .asciz "g + f"
.align	4
_____25: .asciz "\n"
.align	4
______30: .word	0
_____35: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::g
g:	.skip	4
!CodeGen::doVarDecl::f
f:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
set	0,%l1
set	g,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	g,%l1
add 	%g0, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	g,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
set	1,%l1
set	f,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	f,%l1
add 	%g0, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	f,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
_init_done:
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____5,%o1
call printf
nop
set	1,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-8,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-4,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-4,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-8,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-12,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-16,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	g,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____11,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____12,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-20,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	1,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-24,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-20,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-28,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	g,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-32,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	f,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-36,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-32,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-40,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f1
set	g,%l0
add 	%g0, %l0, %l0
st	%f1, [%l0]
set	_strFmt,%o0
set	_____24,%o1
call printf
nop
set	g,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 40) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____31 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____32 
 nop
_____31:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____32:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____33 
 nop
ba  _____34 
 nop
_____33:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
_____34:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

