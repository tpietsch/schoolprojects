.global	__________.MEMLEAK,main
.section	".data"
.align	4
______3: .word	4
______5: .word	0
______10: .word	12
_____16: .asciz "Index value of "
.align	4
_____17: .asciz "0"
.align	4
_____18: .asciz " is outside legal range [0,3).\n"
.align	4
______23: .word	0
_____28: .asciz "Index value of "
.align	4
_____29: .asciz "0"
.align	4
_____30: .asciz " is outside legal range [0,3).\n"
.align	4
______32: .word	0
_____34: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______36: .word	4
______38: .word	1
______43: .word	12
_____49: .asciz "Index value of "
.align	4
_____50: .asciz "1"
.align	4
_____51: .asciz " is outside legal range [0,3).\n"
.align	4
______56: .word	0
_____61: .asciz "Index value of "
.align	4
_____62: .asciz "1"
.align	4
_____63: .asciz " is outside legal range [0,3).\n"
.align	4
______65: .word	1
_____67: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______69: .word	4
______71: .word	2
______76: .word	12
_____82: .asciz "Index value of "
.align	4
_____83: .asciz "2"
.align	4
_____84: .asciz " is outside legal range [0,3).\n"
.align	4
______89: .word	0
_____94: .asciz "Index value of "
.align	4
_____95: .asciz "2"
.align	4
_____96: .asciz " is outside legal range [0,3).\n"
.align	4
______98: .word	2
_____100: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______102: .word	4
______104: .word	0
______109: .word	12
_____115: .asciz "Index value of "
.align	4
_____116: .asciz "0"
.align	4
_____117: .asciz " is outside legal range [0,3).\n"
.align	4
______122: .word	0
_____127: .asciz "Index value of "
.align	4
_____128: .asciz "0"
.align	4
_____129: .asciz " is outside legal range [0,3).\n"
.align	4
_____133: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____137: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____138: .asciz "\n"
.align	4
_____142: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____143: .asciz "\n"
.align	4
_____147: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____148: .asciz "\n"
.align	4
_____152: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____153: .asciz "\n"
.align	4
_____155: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____156: .asciz "\n"
.align	4
______161: .word	0
_____166: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	4,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-28,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-40,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____12 
 nop
set	-32,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____13 
 nop
_____12:
set	1,%l3
set	-32,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____13:
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____14 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____15 
 nop
_____14:
_____15:
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-48,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____24 
 nop
set	-44,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____25 
 nop
_____24:
set	1,%l3
set	-44,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____25:
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____26 
 nop
set	_strFmt,%o0
set	_____28,%o1
call printf
nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	_strFmt,%o0
set	_____30,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____27 
 nop
_____26:
_____27:
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____33 
 nop
set	_strFmt,%o0
set	_____34,%o1
call printf
nop
set	1, %o0
call exit
nop
_____33:
st	%l1, [%l0]
set	4,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	1,%o1
call .mul
nop
mov	%o0, %l2
set	-68,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-80,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-80,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____45 
 nop
set	-72,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____46 
 nop
_____45:
set	1,%l3
set	-72,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____46:
set	-72,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____47 
 nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	_strFmt,%o0
set	_____50,%o1
call printf
nop
set	_strFmt,%o0
set	_____51,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____48 
 nop
_____47:
_____48:
set	0, %l0
set	-84,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-88,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-92,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-88,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____57 
 nop
set	-84,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____58 
 nop
_____57:
set	1,%l3
set	-84,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____58:
set	-84,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____59 
 nop
set	_strFmt,%o0
set	_____61,%o1
call printf
nop
set	_strFmt,%o0
set	_____62,%o1
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____60 
 nop
_____59:
_____60:
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-96,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	1,%l1
set	-96,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____66 
 nop
set	_strFmt,%o0
set	_____67,%o1
call printf
nop
set	1, %o0
call exit
nop
_____66:
st	%l1, [%l0]
set	4,%l1
set	-100,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-104,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	2,%o1
call .mul
nop
mov	%o0, %l2
set	-108,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-112,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-116,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-120,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-120,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____78 
 nop
set	-112,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____79 
 nop
_____78:
set	1,%l3
set	-112,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____79:
set	-112,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____80 
 nop
set	_strFmt,%o0
set	_____82,%o1
call printf
nop
set	_strFmt,%o0
set	_____83,%o1
call printf
nop
set	_strFmt,%o0
set	_____84,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____81 
 nop
_____80:
_____81:
set	0, %l0
set	-124,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-128,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-132,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-128,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____90 
 nop
set	-124,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____91 
 nop
_____90:
set	1,%l3
set	-124,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____91:
set	-124,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____92 
 nop
set	_strFmt,%o0
set	_____94,%o1
call printf
nop
set	_strFmt,%o0
set	_____95,%o1
call printf
nop
set	_strFmt,%o0
set	_____96,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____93 
 nop
_____92:
_____93:
set	-108,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-136,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	2,%l1
set	-136,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____99 
 nop
set	_strFmt,%o0
set	_____100,%o1
call printf
nop
set	1, %o0
call exit
nop
_____99:
st	%l1, [%l0]
set	4,%l1
set	-140,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-144,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	4,%o0
set	0,%o1
call .mul
nop
mov	%o0, %l2
set	-148,%l3
add 	%fp, %l3, %l3
st	%l2, [%l3]
set	0, %l0
set	-152,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	12,%l1
set	-156,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-160,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	12,%l1
set	-160,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l2
cmp	%l1, %l2
ble  _____111 
 nop
set	-152,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____112 
 nop
_____111:
set	1,%l3
set	-152,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____112:
set	-152,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____113 
 nop
set	_strFmt,%o0
set	_____115,%o1
call printf
nop
set	_strFmt,%o0
set	_____116,%o1
call printf
nop
set	_strFmt,%o0
set	_____117,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____114 
 nop
_____113:
_____114:
set	0, %l0
set	-164,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-168,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-172,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-168,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bl  _____123 
 nop
set	-164,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____124 
 nop
_____123:
set	1,%l3
set	-164,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____124:
set	-164,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____125 
 nop
set	_strFmt,%o0
set	_____127,%o1
call printf
nop
set	_strFmt,%o0
set	_____128,%o1
call printf
nop
set	_strFmt,%o0
set	_____129,%o1
call printf
nop
set	1, %o0
call exit
nop
ba  _____126 
 nop
_____125:
_____126:
set	-148,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12, %l2
add	%fp, %l2, %l2
add	%l1, %l2, %l1
set	-176,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-180,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-176, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____132 
 nop
set	_strFmt,%o0
set	_____133,%o1
call printf
nop
set	1, %o0
call exit
nop
_____132:
ld	[%l1], %l1
set	-180,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-180,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-184,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	4,%l4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-184,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-188,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-188,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-184,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-188,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____136 
 nop
set	_strFmt,%o0
set	_____137,%o1
call printf
nop
set	1, %o0
call exit
nop
_____136:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____138,%o1
call printf
nop
set	0, %l0
set	-192,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	4,%l4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-192,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-196,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-196,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-192,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____141 
 nop
set	_strFmt,%o0
set	_____142,%o1
call printf
nop
set	1, %o0
call exit
nop
_____141:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____143,%o1
call printf
nop
set	0, %l0
set	-200,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-200,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-204,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-204,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-200,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____146 
 nop
set	_strFmt,%o0
set	_____147,%o1
call printf
nop
set	1, %o0
call exit
nop
_____146:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____148,%o1
call printf
nop
set	0, %l0
set	-208,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4,%l4
set	-16,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-208,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	0, %l0
set	-212,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-212,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-208,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-212,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____151 
 nop
set	_strFmt,%o0
set	_____152,%o1
call printf
nop
set	1, %o0
call exit
nop
_____151:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____153,%o1
call printf
nop
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____154 
 nop
set	_strFmt,%o0
set	_____155,%o1
call printf
nop
set	1, %o0
call exit
nop
_____154:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____156,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 212) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____162 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____163 
 nop
_____162:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____163:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____164 
 nop
ba  _____165 
 nop
_____164:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____166,%o1
call printf
nop
_____165:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

