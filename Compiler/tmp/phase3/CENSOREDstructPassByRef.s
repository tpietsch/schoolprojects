.global	struct, func, __________.MEMLEAK,main
.section	".data"
.align	4
_____4: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______5: .word	4
_____7: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
______8: .word	3
_____10: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____16: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____17: .asciz " "
.align	4
_____20: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____21: .asciz "\n"
.align	4
_____22: .asciz "\n"
.align	4
______27: .word	0
_____32: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::struct
struct:	.skip	8
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	struct, %l1
add	%g0, %l1, %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-4, %l1
add	%fp, %l1, %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o0
set	-16,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
add	%sp, -0, %sp
set	func, %l1
call %l1
nop
set	struct, %l1
add	%g0, %l1, %l1
set	0, %l2
add	%l2, %l1, %l1
set	-24,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____15 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	1, %o0
call exit
nop
_____15:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	struct, %l1
add	%g0, %l1, %l1
set	4, %l2
add	%l2, %l1, %l1
set	-32,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____19 
 nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	1, %o0
call exit
nop
_____19:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 32) & -8

func: 
set SAVE.func, %g1
save %sp, %g1, %sp
set	-8,%l0
add 	%fp, %l0, %l0
st	%i0, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
st	%i1, [%l0]
set	-8, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____3 
 nop
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	1, %o0
call exit
nop
_____3:
ld	[%l1], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l2
add	%l2, %l1, %l1
set	-20,%l2
add 	%fp, %l2, %l2
st	%l1, [%l2]
set	4,%l1
set	-20,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	1, %o0
call exit
nop
_____6:
st	%l1, [%l0]
set	3,%l1
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____9 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1, %o0
call exit
nop
_____9:
st	%l1, [%l0]
ret
restore

SAVE.func = -(92 + 20) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____28 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____29 
 nop
_____28:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____29:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____30 
 nop
ba  _____31 
 nop
_____30:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
_____31:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

