.global	__________.MEMLEAK,main
.section	".data"
.align	4
______4: .word	4
______7: .word	3
______8: .word	3
______12: .word	0
_____17: .asciz "x is "
.align	4
_____18: .asciz "\n"
.align	4
_____19: .asciz "y is "
.align	4
_____20: .asciz "\n"
.align	4
_____21: .asciz "g is "
.align	4
_____22: .asciz "\n"
.align	4
______24: .word	-1
______29: .word	-1
______38: .word	0
_____43: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
!CodeGen::doVarDecl::x
main__0x22staticFlag:	.skip	4
main__0x22:	.skip	4
!CodeGen::doVarDecl::y
main__0y25staticFlag:	.skip	4
main__0y25:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	main__0x22staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____3 
 nop
set	4,%l1
set	main__0x22,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	main__0x22staticFlag, %l1
st	%l0, [%l1]
_____3:
set	main__0y25staticFlag, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____6 
 nop
set	3,%l1
set	main__0y25,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	1, %l0
set	main__0y25staticFlag, %l1
st	%l0, [%l1]
_____6:
set	main__0y25,%l1
add 	%g0, %l1, %l1
ld	[%l1], %f2
fitos %f2, %f2
set	main__0y25,%l0
add 	%g0, %l0, %l0
st	%f2, [%l0]
set	3,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	main__0x22,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0x22,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
bg  _____13 
 nop
set	-8,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____14 
 nop
_____13:
set	1,%l3
set	-8,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____14:
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____15 
 nop
set	_strFmt,%o0
set	_____17,%o1
call printf
nop
set	main__0x22,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	main__0y25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____20,%o1
call printf
nop
set	_strFmt,%o0
set	_____21,%o1
call printf
nop
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____22,%o1
call printf
nop
set	-1,%l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0x22,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-1,%l4
set	-24,%l3
add 	%fp, %l3, %l3
ld	[%l3], %l5
add	%l4, %l5, %l5
set	-28,%l3
add 	%fp, %l3, %l3
st	%l5, [%l3]
set	main__0x22,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-28,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	main__0x22,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
set	-1,%l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	main__0y25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-40,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %f2
fitos %f2, %f2
set	-36,%l0
add 	%fp, %l0, %l0
st	%f2, [%l0]
set	-36,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f4
set	-40,%l3
add 	%fp, %l3, %l3
ld	[%l3], %f5
fadds	%f4, %f5, %f5
set	-44,%l3
add 	%fp, %l3, %l3
st	%f5, [%l3]
set	main__0y25,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-48,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	-44,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	main__0y25,%l0
add 	%g0, %l0, %l0
st	%l1, [%l0]
add	%sp, -0, %sp
set	main, %l1
call %l1
nop
ba  _____16 
 nop
_____15:
_____16:
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 48) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____39 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____40 
 nop
_____39:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____40:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____41 
 nop
ba  _____42 
 nop
_____41:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
_____42:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

