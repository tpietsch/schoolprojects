.global	__________.MEMLEAK,main
.section	".data"
.align	4
______2: .word	123
_____4: .asciz "*j = 123 = "
.align	4
_____6: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____7: .asciz " = "
.align	4
_____10: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____13: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____16: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____18: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____19: .asciz "\n"
.align	4
_____20: .single	0r45.6
______21: .single	0r45.6
_____23: .asciz "*g = 45.60 = "
.align	4
_____25: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____26: .asciz " = "
.align	4
_____29: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____32: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____35: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____38: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____41: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____43: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____44: .asciz "\n"
.align	4
______45: .word	1
_____47: .asciz "*c = true = "
.align	4
_____49: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____52: .asciz " = "
.align	4
_____55: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____58: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____60: .asciz "Attempt to dereference NULL pointer.\n"
.align	4
_____63: .asciz "\n"
.align	4
______68: .word	0
_____73: .asciz " memory leak(s) detected in heap space.\n"
.align	4
.section	".bss"
.align	4
_init:	.skip 4
!CodeGen::doVarDecl::_____0
initfuncname0_____001staticFlag:	.skip	4
initfuncname0_____001:	.skip	4
.section ".rodata"
_intFmt:	.asciz "%d"
_strFmt:	.asciz "%s"
_boolT:	.asciz "true"
_boolF:	.asciz "false"
.section	".text"
.align	4
main: 
set SAVE.main, %g1
save %sp, %g1, %sp
set _init, %l0
ld [%l0], %l1
cmp %l1, %g0
bne _init_done
mov 1, %l1
st %l1, [%l0]
_init_done:
set	123,%l1
set	-4,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-8,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-4, %l1
add	%fp, %l1, %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-12,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____4,%o1
call printf
nop
set	-12,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____5 
 nop
set	_strFmt,%o0
set	_____6,%o1
call printf
nop
set	1, %o0
call exit
nop
_____5:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____7,%o1
call printf
nop
set	0, %l0
set	-16,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-12, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____9 
 nop
set	_strFmt,%o0
set	_____10,%o1
call printf
nop
set	1, %o0
call exit
nop
_____9:
ld	[%l1], %l1
set	-16,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-20,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-16, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____12 
 nop
set	_strFmt,%o0
set	_____13,%o1
call printf
nop
set	1, %o0
call exit
nop
_____12:
ld	[%l1], %l1
set	-20,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-24,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-20, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____15 
 nop
set	_strFmt,%o0
set	_____16,%o1
call printf
nop
set	1, %o0
call exit
nop
_____15:
ld	[%l1], %l1
set	-24,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-24,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____17 
 nop
set	_strFmt,%o0
set	_____18,%o1
call printf
nop
set	1, %o0
call exit
nop
_____17:
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____19,%o1
call printf
nop
set	______21,%l0
add 	%g0, %l0, %l0
ld	[%l0], %f1
set	-28,%l0
add 	%fp, %l0, %l0
st	%f1, [%l0]
set	0, %l0
set	-32,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-28, %l1
add	%fp, %l1, %l1
set	-32,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-36,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-32,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-36,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____23,%o1
call printf
nop
set	-36,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____24 
 nop
set	_strFmt,%o0
set	_____25,%o1
call printf
nop
set	1, %o0
call exit
nop
_____24:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____26,%o1
call printf
nop
set	0, %l0
set	-40,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-36, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____28 
 nop
set	_strFmt,%o0
set	_____29,%o1
call printf
nop
set	1, %o0
call exit
nop
_____28:
ld	[%l1], %l1
set	-40,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-44,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-40, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____31 
 nop
set	_strFmt,%o0
set	_____32,%o1
call printf
nop
set	1, %o0
call exit
nop
_____31:
ld	[%l1], %l1
set	-44,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-48,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-44, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____34 
 nop
set	_strFmt,%o0
set	_____35,%o1
call printf
nop
set	1, %o0
call exit
nop
_____34:
ld	[%l1], %l1
set	-48,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-52,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-48, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____37 
 nop
set	_strFmt,%o0
set	_____38,%o1
call printf
nop
set	1, %o0
call exit
nop
_____37:
ld	[%l1], %l1
set	-52,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-56,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-52, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____40 
 nop
set	_strFmt,%o0
set	_____41,%o1
call printf
nop
set	1, %o0
call exit
nop
_____40:
ld	[%l1], %l1
set	-56,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-56,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____42 
 nop
set	_strFmt,%o0
set	_____43,%o1
call printf
nop
set	1, %o0
call exit
nop
_____42:
ld	[%l0], %f0
call printFloat
nop
set	_strFmt,%o0
set	_____44,%o1
call printf
nop
set	0, %l0
set	-60,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	1,%l1
set	-60,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-64,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-60, %l1
add	%fp, %l1, %l1
set	-64,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-68,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-64,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	-68,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	_strFmt,%o0
set	_____47,%o1
call printf
nop
set	-68,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____48 
 nop
set	_strFmt,%o0
set	_____49,%o1
call printf
nop
set	1, %o0
call exit
nop
_____48:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____50 
 nop
set	_boolF,%o1
ba  _____51 
 nop
_____50:
set	_boolT,%o1
_____51:
call printf
nop
set	_strFmt,%o0
set	_____52,%o1
call printf
nop
set	0, %l0
set	-72,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-68, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____54 
 nop
set	_strFmt,%o0
set	_____55,%o1
call printf
nop
set	1, %o0
call exit
nop
_____54:
ld	[%l1], %l1
set	-72,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0, %l0
set	-76,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	-72, %l1
add	%fp, %l1, %l1
cmp	%l1, %g0
bne  _____57 
 nop
set	_strFmt,%o0
set	_____58,%o1
call printf
nop
set	1, %o0
call exit
nop
_____57:
ld	[%l1], %l1
set	-76,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-76,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l0
cmp	%l0, %g0
bne  _____59 
 nop
set	_strFmt,%o0
set	_____60,%o1
call printf
nop
set	1, %o0
call exit
nop
_____59:
ld	[%l0], %l1
set	_strFmt,%o0
cmp	%l1, %g0
bne  _____61 
 nop
set	_boolF,%o1
ba  _____62 
 nop
_____61:
set	_boolT,%o1
_____62:
call printf
nop
set	_strFmt,%o0
set	_____63,%o1
call printf
nop
call __________.MEMLEAK
nop
ret
restore

SAVE.main = -(92 + 76) & -8

__________.MEMLEAK: 
set SAVE.__________.MEMLEAK, %g1
save %sp, %g1, %sp
set	0, %l0
set	-4,%l1
add 	%fp, %l1, %l1
st	%l0, [%l1]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %l1
set	-8,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	0,%l1
set	-12,%l0
add 	%fp, %l0, %l0
st	%l1, [%l0]
set	-8,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
set	0,%l2
cmp	%l1, %l2
be  _____69 
 nop
set	-4,%l2
add 	%fp, %l2, %l2
st	%g0, [%l2]
ba  _____70 
 nop
_____69:
set	1,%l3
set	-4,%l2
add 	%fp, %l2, %l2
st	%l3, [%l2]
_____70:
set	-4,%l0
add 	%fp, %l0, %l0
ld	[%l0], %l1
cmp	%l1, %g0
be  _____71 
 nop
ba  _____72 
 nop
_____71:
set	initfuncname0_____001,%l0
add 	%g0, %l0, %l0
ld	[%l0], %o1
set	_intFmt,%o0
call printf
nop
set	_strFmt,%o0
set	_____73,%o1
call printf
nop
_____72:
ret
restore

SAVE.__________.MEMLEAK = -(92 + 12) & -8

