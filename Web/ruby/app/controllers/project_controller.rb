class ProjectController < ApplicationController

	def refresh
	  item = Specialization.find(params[:id])
	  row = "row" + item.spec_key.to_s
	  count = "count" + item.spec_key.to_s
	  render :update do |page|
	    page[row].replace :partial=>"printtable", :locals => {:s => item}
	    page[count].replace :partial=>"printcount", :locals => {:s => item}
	  end
	end
end
