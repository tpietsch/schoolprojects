class Applicant < ActiveRecord::Base
        set_table_name "applicants"
        set_primary_key "app_key"
        belongs_to :specializations, :foreign_key => "spec_key"
end
