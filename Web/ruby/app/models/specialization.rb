class Specialization < ActiveRecord::Base
        set_table_name "specializations"
        has_many :applicants, :foreign_key => "spec_key"
        set_primary_key "spec_key"
end

