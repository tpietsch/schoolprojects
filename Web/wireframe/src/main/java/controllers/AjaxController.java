package controllers;

import database.models.SavedEntity;
import database.repositories.SavedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by tpietsch on 3/12/16.
 */
@Controller
public class AjaxController {

    @Autowired
    SavedRepository savedRepository;

    @ResponseBody
    @Transactional
    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE,path = "/save.ajax",method = RequestMethod.POST,headers="Accept=application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public SavedEntity save(@RequestBody SavedEntity savedEntity) {
        return savedRepository.save(savedEntity);
    }

    @ResponseBody
    @Transactional
    @RequestMapping(path = "/load.ajax",method = RequestMethod.GET)
    public SavedEntity load(HttpServletRequest request) {
        List<SavedEntity> savedEntityList  = savedRepository.findBySavedKey(request.getParameter("key"));
        if(org.apache.commons.collections.CollectionUtils.isEmpty(savedEntityList)){
            return  null;
        }
        return savedEntityList.iterator().next();
    }


    @ResponseBody
    @Transactional
    @RequestMapping(path = "/list.ajax",method = RequestMethod.GET)
    public List<SavedEntity> list() {
       return savedRepository.findAll();
    }


    @ResponseBody
    @Transactional
    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE,path = "/delete.ajax",method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String delete(@RequestBody SavedEntity savedEntity) {
        savedRepository.delete(savedEntity);
        return "Success";
    }



}
