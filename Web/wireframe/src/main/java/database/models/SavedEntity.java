package database.models;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Created by tpietsch on 3/12/16.
 */
@Entity
@Table(name = "saved", schema = "wireframe", catalog = "")
public class SavedEntity {
    private int savedId;
    private String savedKey;
    private byte[] body;

    @Id
    @Column(name = "saved_id")
    public int getSavedId() {
        return savedId;
    }

    public void setSavedId(int savedId) {
        this.savedId = savedId;
    }

    @Basic
    @Column(name = "saved_key")
    public String getSavedKey() {
        return savedKey;
    }

    public void setSavedKey(String savedKey) {
        this.savedKey = savedKey;
    }

    @Basic
    @Column(name = "body")
    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SavedEntity that = (SavedEntity) o;

        if (savedId != that.savedId) return false;
        if (savedKey != null ? !savedKey.equals(that.savedKey) : that.savedKey != null) return false;
        if (!Arrays.equals(body, that.body)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = savedId;
        result = 31 * result + (savedKey != null ? savedKey.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(body);
        return result;
    }
}
