package database.repositories;

import database.models.SavedEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by tpietsch on 3/12/16.
 */
public interface SavedRepository extends JpaRepository<SavedEntity,Integer> {

    @Query("select a from SavedEntity  a where a.savedKey = ?1")
    List<SavedEntity> findBySavedKey(String savedKey);
}
