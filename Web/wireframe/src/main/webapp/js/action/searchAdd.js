$(document).ready(function() {
	var availableTags = [
		{ label: "Accordin", id: "accordin" },
		{ label: "Browser", id: "browser" },
		{ label: "Button", id: "btn" },
		{ label: "Button bar", id: "btn_bar" },
		{ label: "Checkbox", id: "checkbox" },
		{ label: "Combobox", id: "combobox" },
		{ label: "Date Picker", id: "date_picker" },
		{ label: "Date Stepper", id: "date_stepper" },
		{ label: "Format bar", id: "format_bar" },
		{ label: "Link Bar", id: "link_bar" },
		{ label: "List", id: "list" },
		{ label: "Image", id: "image" },
		{ label: "Menu", id: "menu" },
		{ label: "Radio button", id: "radio_btn" },
		{ label: "Search", id: "search" },
		{ label: "Section title", id: "section_title" },
		{ label: "Table", id: "table" },
		{ label: "Tab Bar", id: "tab_bar" },
		{ label: "Textfield", id: "textfield" },
		{ label: "Text Box", id: "textbox" },
		{ label: "Vertical Tab", id: "vrt_tab" }
	];
	$("#searchinput").autocomplete({
		source: availableTags,
		selectFirst: true,
		delay: 0,
		select: function( event, ui ) {
			var category=$(".element1").filter("#"+ui.item.id).children().attr("data-category");
			$("#pad").append($(".element1").filter("#"+ui.item.id).children().children().clone());
			$("#pad").children().addClass("item");
			$("#pad").children().remove(".title");
			$(".item").resizable().draggable().click( function () {
				$('.item').removeClass('sel');
				$(this).addClass('sel');
			});
			$(".element1").filter("#"+ui.item.id).children().attr("data-category", category+" recently");
		},
	});
});