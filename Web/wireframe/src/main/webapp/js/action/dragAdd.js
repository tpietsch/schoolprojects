$(document).ready(function() {
	$("#pad").droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		accept: '.element1',
		drop: function(event, ui) {
			var category=ui.draggable.children().attr("data-category");
			var temp = $(ui.helper).children().children().clone();
			$(this).append(temp);
			$(this).children().addClass("item");
			$(this).children().remove(".title");
			$(".item").resizable().draggable().click(
				function () {
					$('.item').removeClass('sel');
					$(this).addClass('sel');
			});;
			ui.draggable.children().attr("data-category", category+" recently");
		}
	}).click( function(e) {
		var atr = $(".sel").attr("style");
		$(".sel").attr("style",atr+";left: "+ (e.pageX-50) +"px ;top: "+ (e.pageY-100) + "px");
	});;
	$(".element1").draggable({
		helper: "clone",
		revert: "invalid"
	});
}); 