#include <fstream>						
#include <iostream>					
#include <string>					
#include <cstdlib>			
#include <list>			

using namespace std ;

#include "topSort.hpp"						

int
main ( int argc , char * * argv ) 
{
	string iname ; 				
	ifstream idata ;				
	list<unsigned> mySort ;
	unsigned n , temp , i ;

	if ( argc > 1 ) {
					iname = argv[1] ; 				// strcpy ( iname , argv[1] ) ;
	} else {
					cerr << argv[0] << " requires one input file" << endl ; 	
					exit ( 0 ) ;
	}

	idata.open ( iname.data() , ios::in ) ;			
	if ( ! idata.is_open() ) {
					cerr << "can't open " << iname << endl ; 	
					exit ( 0 ) ; 
	}
        
	cout << iname << endl ;	
	idata >> n ; 
	idata.seekg ( 0 , ios::beg ) ;			


	mySort = topSortAdjacencyMatrix ( idata ) ;
	if ( mySort.size() == n+1 ) {
					cout << "Partial order is inconsistent.  These could be sorted    adjacency matrix" << endl ;
					temp = mySort.front() ;		
					mySort.pop_front() ;
					for ( i = 0 ; i < temp ; i++ ) {
									cout << mySort.front() << " " ;
									mySort.pop_front() ;
					}
					cout << endl ;
					cout << "These could not be sorted" << endl ;
					while ( mySort.size() > 0 ) {
									cout << mySort.front() << " " ;
									mySort.pop_front() ;
					}
					cout << endl ;
	} else {
					cout << "Topological sorting -- adjacency matrix" << endl ;
					while ( mySort.size() > 0 ) {
									cout << mySort.front() << " " ;
									mySort.pop_front() ;
					}
					cout << endl ;
	}

	idata.close() ;
	idata.open ( iname.data() , ios::in ) ;			
	mySort = topSortAdjacencyLists ( idata ) ;
	if ( mySort.size() == n+1 ) {
					cout << "Partial order is inconsistent.  These could be sorted    adjacency lists" << endl ;
					temp = mySort.front() ;		
					mySort.pop_front() ;
					for ( i = 0 ; i < temp ; i++ ) {
									cout << mySort.front() << " " ;
									mySort.pop_front() ;
					}
					cout << endl ;
					cout << "These could not be sorted" << endl ;
					while ( mySort.size() > 0 ) {
									cout << mySort.front() << " " ;
									mySort.pop_front() ;
					}
					cout << endl ;
	} else {
					cout << "Topological sorting -- adjacency lists" << endl ;
					while ( mySort.size() > 0 ) {
									cout << mySort.front() << " " ;
									mySort.pop_front() ;
					}
					cout << endl ;
	}

}
