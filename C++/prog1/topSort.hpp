

#include <istream>
#include <iostream>
#include <list>

#ifndef TOPSORT
#define TOPSORT

list<unsigned> & topSortAdjacencyLists ( istream & input ) ;

list<unsigned> & topSortAdjacencyMatrix ( istream & input ) ;

#include "topSort.cpp"

#endif


