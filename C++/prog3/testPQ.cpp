
#include <iostream>
#include <string>
#include "PQFloydWilliams.hpp"

using namespace std ;

class Data {
 
	public :
		Data ( double priority , const char * n ) : prio(priority) , name(n) { }
		Data ( ) { } ;

		void 
		print ( ostream & out ) const 
		{ 
			out.precision(4) ; out.setf( ios:: fixed ) ; 
			out << prio << " " ;
			out.flags( ios::left ) ; out.width ( 13 ) ;
			out << name ;
		}
		
	  double prio ;
	  string name ;

} ;

ostream & operator << ( ostream & out , const Data & rhs ) 
{
	rhs.print(out) ;   return out ;
}

int
main ( void )
{

	PQueue<Data> thing(20) ;
	Data myData ;
	unsigned mySize ;

	Data data[] = { Data( 0.01226 , "first" ) , Data (  0.234 , "second" ) , Data ( 0.346 , "third" )  , Data ( 0.229 , "fourth" ) ,
					 Data( 0.813 , "fifth" ) , Data (  0.348 , "sixth" ) , Data ( 0.345 , "seventh" )  , Data ( 0.99 , "eighth" ) ,
					 Data( 0.13 , "ninth" ) , Data (  0.847 , "tenth" ) , Data ( 0.999 , "eleventh" )  , Data ( 0.06432, "twelfth" ) ,
					 Data( 0.235 , "thirteenth" ) , Data (  0.692 , "fourteenth" ) , Data ( 0.45 , "fifteenth" )  , Data ( 0.99 , "sixteenth" ) ,
					 Data( 0.742 , "seventeenth" ) , Data (  0.742 , "eighteenth" ) , Data ( 0.50 , "nineteenth" )  , Data ( 0.9888 , "twentieth" ) } ;



	for ( int j = 0 ; j < 18 ; j++ ) {
	
		thing.insertPQ( data[j] , data[j].prio ) ;
	}
	//cout << endl << thing << endl ;
	cout << "==========================================" << endl ;
	for ( int k = 0 ; k < 12 ; k++ ) {

	//cout<<thing.key[k]<<endl;

        }
	

	for ( int j = 1 ; j < 12 ; j++ ) {
	 	

		thing.fetchPQ( myData ) ;

		cout << myData << " " ;
		if ( j % 5 == 0 ) cout << endl ; 
	}
	cout << endl << "=========================================" << endl ;
	//cout << endl << thing << endl ; 
	cout << "==========================================" << endl ;
}
