#include <vector>
#include <iostream>

using namespace std ;

template <class DATA>
PQueue<DATA> :: PQueue ( unsigned int size ) 
{
  this->size=size;
  insert=0;
  heap.resize(size);
  key.resize(size);
  

}

template <class DATA>
bool PQueue<DATA> :: fetchPQ ( DATA & dddd )
{
 if(insert == 0)
 {
   return false;
 }
 insert--;

 dddd=heap[0];
 heap[0]=heap[insert];
 key[0]=key[insert];
 key[insert]=0;
 //heap[0]=NULL;

 trickleDown(0);

 return true;
}
	

template <class DATA>
bool PQueue<DATA> :: insertPQ ( DATA & dddd , double rrrr ) 
{
   insert++;

    if(insert > size)
  {
     //heap.resize(size+1);
     //key.resize(size+1);
     insert--;
     return false;
     
  }

  key[insert-1]=rrrr;
  heap[insert-1]=dddd;
    
  bubbleUp(insert-1);

  return true;
}



template <class DATA>
void PQueue<DATA> :: trickleDown(int i)
      {

        int left = 2*i+1;
	int right = 2*i+2;

        //cheking for any children
	if(left < insert)
          {
       
            //cheking for both children
            if(insert > right)
	      {
                     
	      
		  //cheking if parent is less then both children
	          if((key[i]<key[left] < 0)&& 
		  	(key[i]<key[right]))
	            {
	              //DATA tempL = heap[left];
		      //DATA tempR = heap[right];
                    
		    //cheking which child is larger and replacing
                    if(key[left]>key[right])
      		      {
		        //replacing left child with parent

		        DATA temp=heap[i];
			double tmp=key[i];

		        heap[i]=heap[left];
			key[i]=key[left];


		        heap[left]=temp;
			key[left]=tmp;

		        trickleDown(left);

		      }

		    else
		      {
		        //replacing right child with parent

		        DATA temp=heap[i];
			double tmp=key[i];

		        heap[i]=heap[right];
			key[i]=key[right];


		        heap[right]=temp;
			key[right]=tmp;

		        trickleDown(right);

		      }
         
	           }

		 if(key[i]<key[right])
	          {
		    //replaceing parnet child
	           	DATA temp=heap[i];
			double tmp=key[i];

		        heap[i]=heap[right];
			key[i]=key[right];


		        heap[right]=temp;
			key[right]=tmp;

		        trickleDown(right);

	          }


	      }

	        //cheking if left child is less than
	       if(key[i]<key[left])
	          {
		    //replaceing parnet child
	                DATA temp=heap[i];
			double tmp=key[i];

		        heap[i]=heap[left];
			key[i]=key[left];


		        heap[left]=temp;
			key[left]=tmp;

		        trickleDown(left);
	          }

	    }

	//base case no children  	
	else
	  {
	    return;
	  }
        

      }

  /**
   * Bubbels up children if they are greater than there parnets.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the Heap12 parents are all greater tahn children.
   * @param o the Object to be compared
   */

template <class DATA>
void PQueue<DATA> :: bubbleUp(int i)
      {
        if(i > 0)
	  {
	    //recrsive part cheking if child should replace parent
	    if(key[i]>key[(i-1)/2])
	      {
	        DATA temp=heap[i];
		double tmp=key[i];

		heap[i]=heap[(i-1)/2];
		
		heap[(i-1)/2]=temp;



		key[i]=key[(i-1)/2];
		key[(i-1)/2]=tmp;

		bubbleUp((i-1)/2);
	      }
	    //base case for array of size greater than 0
	    else
	      {
	        return;
	      }
	  }
	//base Case for 0 array elemnts
	else
	  {
	    return;
	  }

      }

  







/*


// The printPQ operation is OPTIONAL work.

template <class DATA>

void PQueue<DATA> :: printPQ ( ostream & out ) const
{
  cout << "priority queue contains " ;
    
	
	// look at the PQ tests for some very useful hints...
}*/
