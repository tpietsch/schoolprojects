
/* * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *	Disjoint Subsets union-find 	winter 2012
 *
 *	< your name >
 *
 * * * * * * * * * * * * * * * * * * * * * * * * */ 

#ifndef UNIVERSE 
#define UNIVERSE 

#include <vector>

using namespace std ;


class DisjointSubsets { 

	public :
		explicit DisjointSubsets ( unsigned numberElements = 5 ) ;

		unsigned findDS ( unsigned ) ;

		void unionDS ( unsigned , unsigned ) ;

	private :

	unsigned size;

	//vector<DATA> ds;

	vector<unsigned> parent;

        
	
	
	} ;

#include "DisjointSubsets.cpp"

#endif
