
/* * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *	Disjoint Subsets union-find 	Winter 2012
 *
 *      <name here>
 *
 * * * * * * * * * * * * * * * * * * * * * * * * */ 

DisjointSubsets :: DisjointSubsets ( unsigned numberElements ) 
{

    size=numberElements;
  
    parent.resize(size);
   

    for(int i=0;i<size;i++)
    {
      parent[i]=i;
     
    }


  
}


 unsigned DisjointSubsets ::findDS ( unsigned k ) 
{
  if(parent[k] == k)
  {
    return k;
  }

  unsigned x= findDS(parent[k]);

  return x;
  
}


void DisjointSubsets :: unionDS ( unsigned k , unsigned j ) 
{
  bool flag=false;
  if(k>size || j> size)
{
  return;
}
  
  if(findDS(j) == j && findDS(k) == k)
  {
    for(int i=0;i<size;i++)
    {
    if(parent[i] == k)
    {
      parent[i]=j;
     
      
    }
  }

  }
 
  else
return;

 /* for(int i=0;i<size;i++)
  {
    if(parent[i] == k)
    {
      parent[i]=j;
      flag=true;
      
    }
  }
*/

  }
