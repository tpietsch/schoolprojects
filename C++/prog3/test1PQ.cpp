#include <cstdio>
#include <cstdlib>
#include <string>

#include "PQFloydWilliams.hpp"

int MyTestPQ()
{
  PQueue<int> pq(1000) ;
  #define maxElements 1000
  int data[ maxElements ];
  int i,j,temp;
  srandom(0);

  if ( pq.fetchPQ( i ) == true ) {
    printf("Error: Your fetchPQ was empty, but fetchPQ returned true!\n");
    return -1;
  }

  /* first, fill up the data[] array so that data[i] == i */
  for( i = 0 ; i < maxElements ; i++){
   //cout<<"trever4"<<endl;
  data[i] = i;
}
  /* second, shuffle the array */
  for( i = 0 ; i < maxElements-1 ; i++)
  {
    j = i+1+(random() % (maxElements-i-1));  /* pick a place to swap with */
    temp = data[i]; data[i] = data[j]; data[j] = temp; /* do the swap */

     }

  /* third, insert all of them into the PQueue */
  for( i = 0 ; i < maxElements ; i++)
  {
      printf("Inserting item #%d, (priority == data == %d)\r", i, data[i] );
    
    
    if ( pq.insertPQ( data[i] , data[i] ) == false )
    {
      printf("\nError: Cannot insert the %dth element (of value %d)!\n", i, data[i]);
      printf("[Stopping.]\n"); return -1;
      }

        
      }

  printf("\nDone inserting items 0 through %d\t\t\t\t\t\t\t\t\n", maxElements-1);
   
 

  /* forth, check that we can fetch ALL of them (and that they're in order) */
  for( i = maxElements-1 ; i >= 0 ; i-- ) /* go backwards, since we expect higher data to come first */
  {
    printf("Fetching item #%d ... (going backwards...)\r", i );


    if ( pq.fetchPQ( j ) == false )
      printf("\nError: Could not fetch the %dth element!\n", i );
    else if ( j != i )
      printf("\nError: I expected item #%d to have value of %d (they should be sorted),"
	     "but instead I got %d\n", i , i , j );
    else continue;

    printf("[Stopping.]\n"); return -1;
  }

  printf("\nDone fetching items %d through 0\t\t\t\t\t\t\n", maxElements-1);

  return 0;
}


int 
main( void ) 
{
				if ( MyTestPQ() == 0 )
    printf("No obvious errors in PQueue found!  Good job.\n");
  else
    printf("There was some errors in your PQueue; Try to fix them and run again.\n");

  return 0;
}

