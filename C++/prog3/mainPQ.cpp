#include <cmath>
#include <cstdio>
#include <iostream>
#include "PQFloydWilliams.hpp"

using namespace std ;

unsigned rNum [] = { 550782, 535176, 1104830, 1216057, 1405516, 1169576, 
		1172368, 83090, 2355115, 2366128, 379954, 644502, 468387, 890598,
 		1536880, 411926, 207412, 1456070, 2373041, 516716, 1920858, 1226442,
 		1956135, 2332621, 1085469, 1245600, 2044270, 2215429, 1818807 } ;


int
main ( )
{
       	


	PQueue<unsigned> pq(30) ;
	unsigned ii , mySize , j ;
	int i;

	for ( i = 1 ; i < 25 ; i++ ) 
		pq.insertPQ ( rNum[i] , (double)rNum[i] ) ;

		cout << "==============================" << endl ;
	for ( j = 0 ; j < 10 ; j++ ) 
		if ( pq.fetchPQ( ii ) ) {
			cout.width(3) ; cout.flags ( ios::right ) ;
			cout << j << ": " << ii << endl ;	       // this outputs unsigned ii
		}
	cout << "==============================" << endl ;

	while ( pq.fetchPQ ( ii ) ) 
		cout << ii << endl ;
	cout << "==============================" << endl ;
}
