#include <ctime>
#include <list>
#include <string> 
#include <iostream>
#include <fstream>
#include "Wall.hpp"
#include "PQFloydWilliams.hpp"
#include "DisjointSubsets.hpp"
#include "createMaze.hpp"

using namespace std ;

int
main ( void )
{
	string name ; 
	int r , c , i , j , count , goal , entry ; 
	int goalc ; 

	list<Wall> maze ;		// stack of walls to be demolished. 
	Wall wall ;

	ofstream fout ; 

	cout << "maze creator\n  number of rows? " ;
	cin >> r ;

	cout << "  number of columns "  ;
	cin >> c ;

	PQueue<Wall> pq (2*r*c-r-c+1) ;

	cout << "entry along bottom edge:  0 <= entry < columns " ;
	cin >> entry ;

	cout << "exit along top edge:  0 <= goal < columns "  ;
	cin >> goalc ;

	cout << "output file name: " ;
	cin >> name ;

	fout.open ( name.data() , fstream::out ) ;
	if ( ! fout.is_open() ) {
		cerr << "can't open output file\n" ;
		exit(0) ;
	}

	srandom ( 10 ) ;
	// srandom ( time(NULL) ) ;	// get different mazes using time(NULL)

	fout << name<< "\t " << r << " rows x " << c << " columns" << endl ;
	fout << "start\t " << entry << endl ;
	fout << "end\t " << goalc << "\t " << goalc+(r-1)*c << endl ; 


	for ( i = 0 ; i < r ; i++ ) {
		for ( j = 0 ; j < c ; j++ ) {
			wall.room1 = i*c+j ;
			if ( j < c-1 ) {
				wall.room2 = wall.room1+1 ;
				pq.insertPQ ( wall , (double)random() ) ;
			}
			if ( i < r-1 ) {
				wall.room2 = wall.room1+c ;
				pq.insertPQ ( wall , (double)random() ) ;
			}
		}
	}

	//  cout << pq << endl ;			// show your priority queue here.

	maze = createMaze ( r , c , pq ) ;
	
	list<Wall> :: iterator itr ;

	for ( itr = maze.begin() ; itr != maze.end() ; itr++ ) {
		wall = *itr ;
		fout << wall.room1 << " " << wall.room2 << endl ;
	}
	fout.flush ( ) ;         
}
