
/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
 *
 *	createMaze.hpp			Winter 2011
 *
 *
 * = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */


#ifndef CREATEMAZE
#define CREATEMAZE


#include <list>
#include "PQFloydWilliams.hpp"
#include "Wall.hpp"

list<Wall> &
createMaze ( int r , int c , PQueue<Wall> & pq ) ;

#include "createMaze.cpp"

#endif
