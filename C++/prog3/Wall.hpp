#ifndef WALL
#define WALL

#include <iostream>

using namespace std ;

struct Wall {
	unsigned room1 , room2 ;
} ;

ostream & operator << ( ostream & out , const Wall & rhs ) 
{
	out.width(3) ; out.flags( ios::right) ;
	out << rhs.room1 << " " ;
	out.width(3) ; out.flags( ios::right) ;
	out << rhs.room2 << "   " ;
	return out ;
}

#endif
