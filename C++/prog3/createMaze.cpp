/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
 *
 *	cse100				Winter 2012
 *
 *	create_maze function
 *
 *	inputs:	 number of rows r and number of columns c
 *      	 priority queue pq containing the wall demolition
 *		 probabilities.	 the priority queue contains Wall records.
 *
 *	returns: list of Wall records; contains those walls to be
 *		 demolished.
 *
 *	
 * = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

#include "PQFloydWilliams.hpp"
#include "DisjointSubsets.hpp"
#include <list>
#include "Wall.hpp"
#include <iostream>




list<Wall> &
createMaze ( int r , int c , PQueue<Wall> & pq ) 
{

 	int i=0;
	unsigned j,k;
	
list<Wall> *wList = new list<Wall>;
	
	//wList->resize((r*c)-1);

	Wall w;

	DisjointSubsets build((r*c));
        
	while(i<(r*c)-1 && pq.fetchPQ(w))
	{

	 k=build.findDS(w.room1);
	 j=build.findDS(w.room2);	
         
	 if(k != j)
	 {

	 
        build.unionDS(k,j);
	wList->push_front(w);
	 i++;
	}
	
	 
	 	   
	 }

	
	//pq.fetchPQ(w);




	
	 cout << wList->size()<<"  " <<i<<endl;;
        	
	  

	

	return *wList;
}
