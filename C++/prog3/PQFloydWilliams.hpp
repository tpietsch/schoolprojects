#ifndef PQUEUE
#define PQUEUE

#include <vector> 
#include <iostream>

using namespace std ;

template <class DATA>
class PQueue {
	public:
	        

		bool insertPQ ( DATA & dddd , double rank ) ;
		bool fetchPQ ( DATA & data ) ;
		explicit PQueue ( unsigned int size ) ;



    		 void printPQ ( ostream & out ) const ;   

	private:
	
		unsigned int size;
		
		unsigned int insert;

		vector<DATA> heap;

		vector<double> key;

		
		void trickleDown(int i);
		void bubbleUp(int i);



		
		
		
		
} ;

template <class DATA>
ostream & operator << ( ostream & out , const PQueue<DATA> & pq ) 
{
	//pq.printPQ(out) ; return out ;
}

#include "PQFloydWilliams.cpp"

#endif
