#include <cstdio>
#include <cstdlib>
#include <cassert>

#include "DisjointSubsets.hpp"


int testDS()
{
  int i,j,n,temp;
  int root = -999;
  int label1 , label2 , even_root, odd_root;
  #define NUM_INSERTS 1000
  int data[NUM_INSERTS];

  DisjointSubsets ds(NUM_INSERTS) ;
  srandom(0);

  /* create data in order */
  for(i = 0 ; i < NUM_INSERTS ; i++) data[i] = i;

  /* shuffle the array */
  for(i = 0 ; i < NUM_INSERTS - 1 ; i++)
  {
    j = i+1+(random() % (NUM_INSERTS-i-1));  /* pick a place to swap with */
    temp = data[i]; data[i] = data[j]; data[j] = temp; /* do the swap */
  }


  printf("This program should be done in a couple seconds..."
	 "if not, you may have an error.\n"
	 "(Make sure path compression and link-by-rank were implemented correctly.)\n\n");
  printf("Done constructing the DisjointSubsets.\n");


  /* for each integer, make sure it is the root of itself
     (that is how it should be initialized by makeDS) */
  for(i = 0 ; i < NUM_INSERTS ; i++)
  {
    if ( ( root = ds.findDS( data[i] ) ) != data[i] )
      printf("Error: Could not findDS(%d)!\n", data[i] );
    else if ( data[i] != root )
      printf("Error: Expecting %d to be the root of itself, but instead findDS set root=%d.\n", data[i] , root );
    else
      continue;

    return -1;
  }
  printf("Done checking that it was initialized properly.\n");

  
  /* We pick random pairs of numbers, and link them together if they are both odd or both even */
  for(n = 0 ; n < 10000 ; n++) /* doesn't really matter how many times we pick pairs */
  {
    i = random() % NUM_INSERTS;
    j = random() % NUM_INSERTS;
    if (i == j) continue; /* we want i != j */

    label1 = ds.findDS( data[i] );
    label2 = ds.findDS( data[j] );   
    if ( (label1 % 2) == (label2 % 2)  /* if they're both odd or both even */
	 && label1 != label2 )             /* and they're currently not in the same subset */
      ds.unionDS( label1 , label2 );        /* then link them together */
  }

  /* statistically, we probably don't need to do anything more; but let's make sure we covered all pairs */
  /* go through each pair, joining them together if they are both odd or both even */
  for(i = 0 ; i < NUM_INSERTS ; i++)
    for(j = i+1 ; j < NUM_INSERTS ; j++)
    {
      label1 = ds.findDS( data[i] );
      label2 = ds.findDS( data[j] );

      if ( (label1 % 2) == (label2 % 2)  /* if they're both odd or both even */
	   && label1 != label2 )             /* and they're currently not in the same subset */
        ds.unionDS( label1 , label2 );        /* then link them together */
    }

  printf("Done linking ALL the ODDS together and the EVENS together.\n");

  /* see what the root of the even's and odd's are */
  even_root = ds.findDS( 0 );
  odd_root = ds.findDS( 1 );


  /* now check that the above joining resulted in 2 disjoint sets */
  /* every integer must either belong to the EVENS or the ODDS  */
  for(i = 0 ; i < NUM_INSERTS ; i++)
  {
    root = ds.findDS( data[i] );

    if ( data[i] % 2 == 0 )
      assert( root == even_root);
    else
      assert( root == odd_root);
  }

  printf("Done checking that every integer either belonged to the ODDS or the EVENS.\n");

  // delete ds ;
  // if ( ds != NULL ) 
	// printf ( "\nError: I expected a NULL value for ds here\n" ) ;
  // else {
  	// printf("Done freeing the DisjointSubsets.\n");
  	// printf("\nDone. No obvious errors found.\n");
  // }
  printf("\nDone. No obvious errors found.   Nice work.\n");
  return 0;
}


int main()
{
  return testDS();
}
