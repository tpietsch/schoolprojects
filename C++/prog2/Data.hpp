#include <iostream>
#include <string>


using namespace std ;

class Data {
	public : 
	explicit Data ( ) { data = 0 ; }
	Data ( unsigned d , const char * n ) { data = d ; name = n ; }
	Data ( unsigned d ) { data = d ; }
	operator unsigned ( ) { return data ; } 
	Data & operator = ( const Data & rhs ) { data = rhs.data ; name = rhs.name ; }
	bool operator != ( const Data & rhs ) { return data != rhs.data ; } 
	void print ( ostream & out ) const { out << data << "    " << name << " " ; }
	unsigned data ;
	string name ;
} ;


