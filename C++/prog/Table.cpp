/*
  : Trever Pietsch
  : A09150869
  : cs100waa
  : HW2
*/


#include <vector>
#include <list>
#include <ostream>

using namespace std ;


template <class DATA> Table<DATA>::Table(unsigned int size)
  {
    table.resize(size);

    entry=0;   

    this->size=size;
    
  }

template<class DATA> void Table<DATA>::clear ( )
  {

    for(int i=0;i<size;i++)

    table[i].clear();
  
    entry=0;
  
  }

template <class DATA> bool Table<DATA>::insert (DATA & data )
  {

    unsigned int key= unsigned(data); 

    unsigned int mod= (key)%(this->size);

    typename list<DATA>::iterator it;

     
    for(it = table[mod].begin(); it != table[mod].end();it++)
      {
        if(! (*it != data))
        return false;
      }
      
    (table[mod]).push_back(data);

         
    entry++;

    return true;
  }

template <class DATA> bool Table<DATA>::remove ( DATA & data )
  { 
    unsigned int key= unsigned(data);
    
    unsigned int mod= (key)%(this->size);
      
    typename list<DATA>::iterator it;
       
        
    for(it = table[mod].begin();it != ( table[mod].end());it++)
      {
        if(! (*it != data))
          {
            it = table[mod].erase (it);
            it--;
            entry--;
            return true;
          }
      }

    


    return false;
  }

template <class DATA> bool Table<DATA>::access ( DATA & data )
  {
    unsigned int i;

    unsigned int key= unsigned(data);
    
    unsigned int mod= (key)%(this->size);
       
    typename list<DATA>::iterator it;
           
 
    for(it = table[mod].begin();it != (table[mod].end());it++)
      {

        if(!(*it != data))
          {
    
            data = *it;
    
            table[mod].push_front(data);

            table[mod].erase(it);
    
            return true;
          }
      }

    return false;

  
  }

template<class DATA> Perform Table<DATA>::perform ( )const
  {
  
    float x,y,z,i;

    typename list<DATA>::iterator it;

    Perform p;
   
    i=z=x=y=0;

    for(i=0;i<size;i++)
      {
        y=table[i].size();

        x=(y*(y+1))/2;

        z=x+z;
      }
    
    p.successful[0] = z/entry;

    i=z=y=x=0;

    for(i=0;i<size;i++)
      {
        y=table[i].size();
     
     
        if(table[i].empty())
          {
            y=1;
          }

        z=y+z;      
      }
    
    p.unsuccessful[0]=z/size;
  
    i=z=y=x=0;
    z=size;

    for(i=0;i<size;i++)
      {
        if(!(table[i].empty()))
          {
            x++;
          }
      }
    
    y=((z-1)/z);
    x=1;

    for(i=0;i<entry;i++)
      {
        x=y*x;
      }

    p.unsuccessful[1]=x+(entry/z);

    p.successful[1]=1+(entry-1)/(2*z);

    return p;
  }

template <class DATA> 
list<DATA>& Table<DATA>::getOne ( unsigned int i )
  {
  
    return table[i];
  
  }
