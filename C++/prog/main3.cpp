#include <iostream>
#include <cstdio>
#include <cmath>
#include "Table.hpp"
#include "MyData.hpp" 

MyData myData[]  = { MyData(92014,"bev tuxin") , MyData(92093,"herb jones") , MyData(16428,"diane lake") , 
		MyData(12345, "willie yuu"), MyData(23455,"darrell cruz") , MyData(34567,"jehan roma") , 
		MyData(16428,"patti bliss"), MyData(56789,"alex kronkov") , MyData(92014,"beth smith") ,
		MyData(78901,"alan knox") , MyData(89012, "khanh tran") , MyData(90123,"ray bass") , 
		MyData(23471,"steve finny") , MyData(23423, "lee park") , MyData(711711,"phil bose") } ;

using namespace std ;

void
showit ( Table<MyData> & test , unsigned size )
{
	for ( unsigned int i = 0 ; i < size ; i++ ) {
		list<MyData> lll ;
		list<MyData> :: iterator it ;
		lll = test.getOne(i) ;
		cout << "list " ;
		// cout.width(3) ;  cout.flags( ios::right ) ;
		cout << i << endl ;
		unsigned j ;
		for ( j = 0 , it = lll.begin( ) ; it != lll.end() ; it++ , j++ ) {
			cout.width(3) ;  cout.flags( ios::right ) ;
			cout << j << ":   " << *it << endl ;
		}
	}
}

int 
main ( void ) 
{
	Table<MyData> table(4);     
	int i , j ;
	Perform pp ;
	
	for ( j = 0 ; j < 10 ; j++ ) 
		table.insert ( myData[j] ) ;

	cout << endl ;
	showit(table,4) ;

	pp = table.perform() ;
	cout.setf(ios::fixed,ios::floatfield) ;
	cout << " average ssl is " << pp.successful[0] << "     average usl is " << pp.unsuccessful[0] << endl ; 
	cout << "expected ssl is " << pp.successful[1] << "    expected usl is " << pp.unsuccessful[1] << endl << endl ; 

}
