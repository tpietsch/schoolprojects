/*
  : Trever Pietsch
  : A09150869
  : cs100waa
  : HW2
*/



#include <vector>
#include <list>
#include <ostream>

using namespace std ;

typedef struct { double successful[2] , unsuccessful[2] ; } Perform ;


template <class DATA>
class Table {
private :
 	
        
	 vector< list<DATA> > table;       
	 unsigned int size;
	 unsigned int entry;
	

public :
	explicit Table ( unsigned int size = 5 ) ;
	void clear ( ) ;
	bool insert ( DATA & data ) ;
	bool remove ( DATA & data ) ;
	bool access ( DATA & data ) ;
	Perform perform ( ) const ;
	list<DATA> & getOne ( unsigned int i ) ;		

} ;
#include"Table.cpp"


