
#include <iostream>
#include <ctime>
#include "Table.hpp"

using namespace std ;

int
main ( unsigned argc , char * * argv )
{
	int i , m , k ;
	unsigned size , samples = 200 ;
	double successful0 , successful1 , unsuccessful0 , unsuccessful1 ;

	if ( argc == 2 ) 
		size = atoi ( argv[1] ) ;
	else {
		cerr << argv[0] << " requires one <size> parameter" << endl ;
		exit(1) ;
	}

	Table<unsigned int> test(size) ;
	
	unsigned ddd ;
	Perform ppp ;

	// srandom(time(0)) ;					// gives a different execution each time.	
	srandom(12345) ;					// same execution each time.
	
	printf ( "   load      average    expected   average    expected\n" ) ;
	printf ( "  factor       ssl         ssl       usl         usl  \n" ) ;
	printf ( "%10.6f %10.6f %10.6f %10.6f %10.6f    Separate Chaining\n" , 0.0 , 1.0 , 1.0 , 1.0 , 1.0 ) ;
	for ( m = 1 ; m <= 10*size ; m++ ) {			// m is number of elements
		successful0 = unsuccessful0 = successful1 = unsuccessful1 = 0 ;
		for ( k = 0 ; k < samples ; k++ ) {
			test.clear() ;
			for ( i = 0 ; i < m ; i++ ) {
				ddd = random () ;		
				//printf("Calling insert \n");
				test.insert(ddd) ; 
			}
			ppp = test.perform( ) ;
			successful0 += ppp.successful[0] ;
			successful1 += ppp.successful[1] ;
			unsuccessful0 += ppp.unsuccessful[0] ;
			unsuccessful1 += ppp.unsuccessful[1] ;
		}
		double alpha ;
		alpha = (double)m/size ;
		printf ( "%10.6f %10.6f %10.6f %10.6f %10.6f    Separate Chaining\n" , alpha , successful0/samples , successful1/samples , 
						unsuccessful0/samples , unsuccessful1/samples ) ;
		fflush ( stdout ) ;
	}
}
