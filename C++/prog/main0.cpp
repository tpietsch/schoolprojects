#include <iostream>
#include <string>
#include "Table.hpp"

using namespace std ;

class Data {
	public : 
	explicit Data ( ) { data = 0 ; }
	Data ( unsigned d , const char * n ) { data = d ; name = n ; }
	Data ( unsigned d ) { data = d ; }
	operator unsigned ( ) { return data ; } 
	Data & operator = ( const Data & rhs ) { data = rhs.data ; name = rhs.name ; }
	bool operator != ( const Data & rhs ) { return data != rhs.data ; } 
	void print ( ostream & out ) const { out << data << "    " << name << " " ; }
	unsigned data ;
	string name ;
} ;

ostream & operator << ( ostream & out , const Data & rhs ) 
{
	rhs.print(out) ;
	return out ;
}

void
showit ( Table<Data> & test , unsigned size )
{
        
	for ( unsigned i = 0 ; i < size ; i++ ) {
		list<Data> lll ;
		list<Data> :: iterator it ;
		lll = test.getOne(i) ;
		cout << "list " ;
		// cout.width(3) ;  cout.flags( ios::right ) ;
		cout << i << endl ;
		unsigned j ;
		for ( j = 0 , it = lll.begin( ) ; it != lll.end() ; it++ , j++ ) {
			cout.setf(ios::fixed,ios::floatfield) ;
			// cout.width(3) ;  cout.flags( ios::right ) ;
			cout << j << ":   " << *it << endl ;
		}
	}
}

int
main ( )
{
	int i ;
	unsigned size = 3 ;
	Data d [] = { Data ( 20 , "cow" ) , Data(12,"pig") , Data(16,"dog") , Data (18,"ape" ) , Data(22,"yak") , Data(18,"ape") } ;
	Perform ppp ;
	Data dd ( 22 ) , ddd( 18) ;
	
	Table<Data> test(size) ;

	for ( i = 0 ; i < 6 ; i++ )
		test.insert ( d[i] ) ;

	showit( test , size ) ;
	ppp = test.perform() ;
	cout << "\naverage ssl " << ppp.successful[0] << "     average usl " << ppp.unsuccessful[0] << endl << endl ;

	test.access ( dd ) ;
	showit( test , size ) ;
	cout << endl ;
	test.remove ( ddd ) ;
	showit( test , size ) ;
	ppp = test.perform() ;
	cout << "\naverage ssl " << ppp.successful[0] << "     average usl " << ppp.unsuccessful[0] << endl ;

}
