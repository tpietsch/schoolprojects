#include <iostream>
#include <cmath>
#include "Table.hpp"

#define Number 8
#define TableSize 4
Perform pp ;

unsigned x[Number] = { 1234 , 2345 , 2342 , 6781 , 234 , 9982 , 235 , 3589 } ;

using namespace std ;

void
showit ( Table<unsigned int> & test , unsigned size )
{
	for ( unsigned int i = 0 ; i < size ; i++ ) {
		list<unsigned int> lll ;
		list<unsigned int> :: iterator it ;
		lll = test.getOne(i) ;
		cout << "list " ;
		// cout.width(3) ;  cout.flags( ios::right ) ;
		cout << i << endl ;
		unsigned j ;
		for ( j = 0 , it = lll.begin( ) ; it != lll.end() ; it++ , j++ ) {
			cout.width(3) ;  cout.flags( ios::right ) ;
			cout << j << ":   " ;
			cout.width(4) ;  cout.flags( ios::right ) ;
			cout << *it << endl ;
		}
	}
}

int 
main ( void ) 
{
	Table<unsigned int> myTable(TableSize) ;
	unsigned a = 6781 , b = 7766 , c = 98 ;

	for ( int j = 0 ; j < Number ;  j++ ) {
			myTable.insert ( x[j] ) ;
	}

	showit( myTable , TableSize ) ;
	 cout << endl ;

	if ( myTable.access(a) ) 
		cout << "record 6781 is present" << endl ;
	if ( ! myTable.access(b) ) 
		cout << "record 7766 is not present" << endl ;
	if ( ! myTable.access(c) ) 
		cout << "record 98 is not present" << endl ;
	if ( ! myTable.insert ( a ) )
		cout << "duplicate record attempted: 6781" << endl ;

	pp = myTable.perform() ;
	cout.setf(ios::fixed,ios::floatfield) ;
	cout << " average ssl is " << pp.successful[0] << "     average usl is " << pp.unsuccessful[0] << endl ; 
	cout << "expected ssl is " << pp.successful[1] << "    expected usl is " << pp.unsuccessful[1] << endl << endl ; 
	showit( myTable , TableSize ) ;
	
	myTable.clear() ;
	cout << endl ;
	showit( myTable , TableSize ) ;

}
