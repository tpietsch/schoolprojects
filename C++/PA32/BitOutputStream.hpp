#include <iostream>
#include <fstream>
#include <bitset>

using namespace std;

class BitOutputStream
{

  public:

    char buf;
    int nbits;
    bitset<8> bit;
    std::ofstream &out;

  public:
    
    BitOutputStream(std::ofstream &os):out(os)
    {    
      buf=nbits=0;
    }


    void flush()
    {
      out.put(buf);
      out.flush();
      buf=nbits=0;
    }


    void writeBit(int i)
    {

     int b= 7-nbits;
     bit[b]=i;
     

     if(nbits != 8)
     {
     buf=buf+(char)i;
     nbits++;
     }
     else
     flush();
    }

};
    

