import scala.collection.immutable.HashMap
import scala.util.matching.Regex
import Crypt._
import java.io.PrintWriter



case class Entry ( account   : String
                 , password  : String
                 , uid       : Int
                 , gid       : Int
                 , gecos     : String
                 , directory : String
                 , shell     : String
                 )
object Entry {
  
  def apply(line: String) : Entry = {
  
    var a=line.split(""":""")
    val ent=new Entry(a(0),a(1),a(2).toInt,a(3).toInt,a(4),a(5),a(6))
    ent
     }
}

object Crack {

  def transformReverse(w: String) : Iterator[String] = {    
    Iterator(w,w.reverse)
  }
  


   def transformCapitalize(w: String) : Iterator[String] = {
      if(w.length == 0){Iterator("")}
     else{
      for(a <- Iterator(w(0),w(0).toUpper); b <- transformCapitalize(w.tail)) yield(a+b) 
      }
      }
  



 def transformDigits(w:String) : Iterator[String] = {
 val map=HashMap('o'-> List("0"),'z' ->List( "2"),'a'->List("4"),'b'->List("6","8"),
 'i'->List("1"),'l'->List("1"),'e'->List("3"),'s'->List("5"),'t'->List("7"),
 'g'->List("9"),'q'->List("9"))

   if(w.length == 0){Iterator("")}
    else{
     if(map.contains(w(0).toLower))
     {
     for(a <- Iterator(w(0)) ++ map(w(0).toLower).toIterator;
      b <- transformDigits(w.tail)) yield(a+b)      
        }
	else
	{
	for(a <- Iterator(w(0));
          b <- transformDigits(w.tail)) yield(a+b)
	}
 }
 }




  def checkPassword(plain: String, enc: String) : Boolean = 
    Crypt.crypt(enc, plain) == enc



  def candidateWords(file: String) =
    Words.wordsMatchingRegexp(file, new Regex("""^.{6,8}$"""))



  // scala> Crack("passwd", "words", "soln.1")
  // goto> scala Crack passwd words soln.1



/////////TODO  -- add double nest loop combs before triple next -- remove passwrods that have been found


  def apply(pwdFile: String, wordsFile: String, outFile: String) : Unit = {

     val out=new PrintWriter(outFile)
                                             
     var it=scala.io.Source.fromFile("words").getLines().map(s => s.toLowerCase).toList.distinct.filter(x => (x.length() >= 6)&&(x.length() <= 8))
     
     var pass = (for(c <- (scala.io.Source.fromFile(pwdFile).getLines()))yield(Entry(c))).toList

     val start = System.nanoTime

     for(i <- it)
      {              
         val it2 = transformReverse(i)
         for(j <- it2)  
           {
                for(p <- pass)
               {

                 if(checkPassword(j,p.password)) 
                 {
                 
                 pass = pass.filterNot(x => (x.account).equals(p.account))    
                 it = it.filterNot(x => x.equals(i))
                //out.println((System.nanoTime - start)/1000000000.0/60.0)
                 
                // System.out.println(p.account+"="+j)
                 out.println(p.account+"="+j)
                


                

                 }
                 
                 out.flush 
                          
              }        
           }
         }
        

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      var it3 = transformCapitalize("trever")
      var it4 = transformCapitalize("trever")

      for(i <- it)
      {              
         val it2 = transformReverse(i)
         for(k <- it2)  
           {
                      

         it3 = transformCapitalize(k)
        
         it4 = transformDigits(k)

         for((j,l) <- it3 zip it4)  
           {
               
               it3 = it3.filterNot(x => x.equals(j))
               it4 = it4.filterNot(x => x.equals(l))

               for(p <- pass)
               {

                 if(checkPassword(j,p.password)) 
                 {
                  it = it.filterNot(x => x.equals(i))
                 pass = pass.filterNot(x => (x.account).equals(p.account))    
                
                // System.out.println("reverse and caps")
                 out.println((System.nanoTime - start)/1000000000.0/60.0)
                 //System.out.println(p.account+"="+j)
                 out.println(p.account+"2="+j)
                

                 }

                 if(checkPassword(l,p.password)) 
                 {
                  it = it.filterNot(x => x.equals(i))
                 pass = pass.filterNot(x => (x.account).equals(p.account))    
                 
                // System.out.println("reverse and caps")
                 out.println((System.nanoTime - start)/1000000000.0/60.0)
                 //System.out.println(p.account+"="+j)
                 out.println(p.account+"3="+l)
                

                 }
                 
                 out.flush 
                          
              }    
            }
             
             for(j <- it3)  
            { 

                for(p <- pass)
               {

            if(checkPassword(j,p.password)) 
                 {
                  it = it.filterNot(x => x.equals(i))
                 pass = pass.filterNot(x => (x.account).equals(p.account))    
                 
                // System.out.println("reverse and caps")
                 out.println((System.nanoTime - start)/1000000000.0/60.0)
                 //System.out.println(p.account+"="+j)
                 out.println(p.account+"4="+j)
                
}
                 }
          }

               for(j <- it4)  
            { 

          for(p <- pass)
               {

            if(checkPassword(j,p.password)) 
                 {
                  it = it.filterNot(x => x.equals(i))
                 pass = pass.filterNot(x => (x.account).equals(p.account))    
                 
                // System.out.println("reverse and caps")
                 out.println((System.nanoTime - start)/1000000000.0/60.0)
                 //System.out.println(p.account+"="+j)
                 out.println(p.account+"5="+j)
                
                }
                 }
          }
         }
       }

         out.println((System.nanoTime - start)/1000000000.0/60.0)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       /*  
        for(i <- it)
        {              
         val it2 = transformReverse(i)
         for(k <- it2)  
           {
                      

         val it3 = transformCapitalize(k)
         for(j <- it3)  
           {
              

         val it4 = transformDigits(j)
         for(l <- it4)  
           {
               
               for(p <- pass)
               {

                 if(checkPassword(l,p.password)) 
                 {
                 
                 pass = pass.filterNot(x => (x.account).equals(p.account))    
                 it = it.filterNot(x => x.equals(i))
                 
                 out.println(p.account+"="+l)

                 }
                 
                 out.flush 
                          
              }    
            }
          }
          }
         
       }*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }

  def main(args: Array[String]) = { 
    println("Begin: Cracking Passwords")
    apply(args(0), args(1), args(2))
    println("Done: Cracking Passwords")
  }
}


