import scala.collection.immutable.HashMap
import scala.util.matching.Regex
import java.io.PrintWriter
import Lines._



object Words {

  def apply(file: String) : Iterator[String] =  {
   val it=scala.io.Source.fromFile(file).getLines().map(s => s.toLowerCase)
    it
      }



  def groupFreq[A,B](xs: Iterator[A], f: A => B): HashMap[B, Int] = {
      
      val freqMap = new HashMap[B,Int]
      var b = freqMap
  
      for(x <- xs)
      {
        b+= f(x) -> (b.getOrElse(f(x), 0) + 1)       
        }

        b      
     }
    



  val inti = List(1,21,5,39,12,7,92)




  def isEven(x: Int): String =
    if ((x % 2) == 0) { "Even" } else { "Odd" } 

  def sizeFreq(file: String): HashMap[Int, Int] = {
  
  def f(x: String) = x.length

  var it = apply(file)

  groupFreq(it, f)
  
    
  }

  def charFreq(file: String): HashMap[Char, Int] = 
  {
    val it = apply(file)
    val a = it.toList
    val b = a.mkString

    val chars   = b.iterator

    val grouper = (x:Char) => x  

    groupFreq(chars, grouper) 
  }



  
  
  def wordsOfSize(file: String, size: Int) : Iterator[String] =
  {
    val it = apply(file).filter(x=> x.length == size)
    it      
    }



  def wordsWithAllVowels(file: String): Iterator[String] = 

  {
    val it = apply(file).filter(x => (x.contains('a') && x.contains('e')&& x.contains('i')&& x.contains('o')&& x.contains('u')))
    it
  }
     


  def wordsWithNoVowels(file: String): Iterator[String] = 
  {
    val it = apply(file).filter(x => !(x.contains('a') || x.contains('e')|| x.contains('i')|| x.contains('o')|| x.contains('u')))
    it
  }
  


   def wordsMatchingRegexp(file: String, re: Regex): Iterator[String] =
  {
    val it=apply(file).filter(x=> re.pattern.matcher(x).matches)
    it
  }
}




// vim: set ts=2 sw=2 et:


/*


*/
