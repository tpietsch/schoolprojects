import Words._
import Crack._

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

case class CSE130Test(name: String, points: Int, answer: Any, thnk: () => Any)

object Test { 

  val tests: List[CSE130Test] = List(/* RAVI: FILL THIS IN */)

  def runTests(): (Int, Int) = { 
    var points = 0
    var total = 0
    for (t <- tests) {
      total  += t.points 
      val res = t.thnk()
      if (res.toString == t.answer.toString) { 
        points += t.points
      }
    }
    (points, total)
  }

  def apply(): Unit = {
    val (points, total) = runTests()
    println("130>> Results %d out of %d".format(points, total))
    println("130>> Compiled")
  }

  def main(args: Array[String]) = 
    apply()

}
// vim: set ts=2 sw=2 et:

