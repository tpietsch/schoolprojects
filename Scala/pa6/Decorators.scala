
object Decorators {
    
  object profile { 
    /// map keeps names of cuntions and how many times they have been called
    private var cm: Map[String, Int] =  Map().withDefault(_ => 0)

    def count(name: String) = 
      cm(name)
   
    def reset(name: String) = 
      cm += name -> 0

    def apply[A, B](name: String)(f: A => B) = new Function1[A, B] {
      def apply(x: A) = {
        val n = cm(name)
        cm += name -> (1 + n)
        f(x)
      }
    }
  }
 
  object trace {
    
    private var d: Int = 0
    private val lev: String = "| "
   
    def apply[A, B](name: String)(f: A => B) : Function1[A, B] = new Function1[A, B] {
      // You may add more fields here
      def apply (x: A): B = {
        try{
        val call = ",- "+name+"("+x.toString+")"
        for(i <- 0 until d){
          print(lev)
        }
        print(call)
        println("")
        d = d + 1
        val res = f(x)
        val post = "`- "+res.toString
        d = d - 1
        for(i <- 0 until d){
          print(lev)
        }
        print(post)
        println("") 
        res
      }
      catch{
        case e =>d = d - 1 
                  throw(e)
                  
                 
      }



      
    }
  } 
}
  
  object memo {
      
       

    def apply[A, B](f: A => B) : Function1[A, B] = new Function1[A, B] {
    var cm: Map[ (A => B,A), B] =  Map()
      
      def apply (x: A): B = {
        
        if(!(cm.contains((f,x)))){
       
          val b = (f,x) -> f(x)
          cm = cm + b
          cm(f,x)   
           

        }
        else{

         val b = cm((f,x))
         b

        } 
           
      
      }
    }
  }

}


