/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 6
 * Date:       May 19,2011
 *
 * File: Triangle.java
 *
 * Souces of Help: tutors in lab
 *
 * class for creating shapes specificly triangels
 *
 */
import java.awt.*;
public class Triangle extends Shape
{
private Point p1;
private Point p2;
private Point p3;
 /* traingel()
  *
  *intilazes super lcas name of shape
  *Parameters : ()
  *Returns:     
  */

public Triangle()
{
super("Triangle");
}
 /* traingel()
  *
  *intilazes variables of shape
  *Parameters : (Point p1,Point p2,Point p3)
  *Returns:     
  */


public Triangle(Point p1,Point p2,Point p3)
{
this.p1=p1;
this.p2=p2;
this.p3=p3;
}
 /* traingel()
  *
  *maks a copy of traingel shape
  *Parameters : (Triangle tri)
  *Returns:     
  */


public Triangle(Triangle tri)
{
this.p1=new Point(tri.getP1());
this.p2=new Point(tri.getP2());
this.p3=new Point(tri.getP3());

}
/* move
  *
  *takes object and moves it according to the change parameters
  *Parameters : (int xDelta,int yDelta)
  *Returns:     void
  */


public void move (int xDelta,int yDelta)
{
this.getP1().move(xDelta,yDelta);
this.getP2().move(xDelta,yDelta);
this.getP3().move(xDelta,yDelta);

}
  /* toString()
  *
  *creates srting version of object
  *
  *
  *Parameters : ()
  *Returns:     String
  */

public String toString()
{
return "Triangle: (" + getP1() + "," + getP2() + "," + getP3() + ")";
}

 /* equals(Object o)
  *
  *
  *checks if two object are exactly the same
  *
  *Parameters : (Object o)
  *Returns:     boolean
  */
public boolean equals(Object o)
{
//cheking for nuller refernce
if(o==null)
{
return false;
}
else if(this.getClass()!=o.getClass())
{
return false;
}
//cheking if objects are exactly the same
if(this.getP1().equals(((Triangle)o).getP1())&&this.getP2().equals(((Triangle)o).getP2())&&this.getP3().equals(((Triangle)o).getP3()))

{
return true;
}

return false;



}

 /* draw(Graphics g,Color c, boolean fill)
  *
  *meathod draws a shape of object based on dimenshons that where initilized
  *
  *Parameters : (Graphics g,Color c,boolean fill)
  *Returns:     void
  */

public void draw(Graphics g, Color c,boolean fill)
{
//cheking for null reference
if(c==null)
{
g.setColor(Color.black);
}
else
{
//setting color of graphics object
g.setColor(c);
}
//dawing lines for triangel
g.drawLine(this.getP1().getX(),this.getP1().getY(),this.getP2().getX(),this.getP2().getY());
g.drawLine(this.getP2().getX(),this.getP2().getY(),this.getP3().getX(),this.getP3().getY());
g.drawLine(this.getP3().getX(),this.getP3().getY(),this.getP1().getX(),this.getP1().getY());
}

/* setP4()
  *
  *this meathod sets the value of objects instance variable
  *
  *
  *Parameters : (Point p4)
  *Returns:     
  */

private void setP1(Point p1)
{
this.p1=p1;
}

/* setP2()
  *
  *this meathod sets the value of objects instance variable
  *
  *
  *Parameters : (Point p2)
  *Returns:     
  */
private void setP2(Point p2)
{
this.p2=p2;

}

 
 /* setP3()
  *
  *this meathod sets the value of objects instance variable
  *
  *
  *Parameters : (Point p3)
  *Returns:     
  */
private void setP3(Point p3)
{
this.p3=p3;
}

 /* getP1()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     Point p1
  */
public Point getP1()
{
return this.p1;
}


 /* getP2()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     Point p2
  */
public Point getP2()
{
return this.p2;
}


 /* getP3()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     Point p3
  */
public Point getP3()
{
return this.p3;
}

}
