/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 6
 * Date:       May 19,2011
 *
 * File: Line.java
 *
 * Souces of Help: tutors in lab
 *
 * class for creating lines
 *
 */


import java.awt.*;

public class Line extends Shape
{
private Point start;
private Point end;
 /* public line
  *
  *ctor for lines initializes superclass to "line"
  *
  *Parameters : ()
  *Returns:     
  */

public Line()
{
super("line");
}
 /* public line
  *
  *ctor for lines initialize start and end point of line
  *
  *Parameters : (Point start,Point end)
  *Returns:     
  */


public Line (Point start,Point end)
{
this.start=start;
this.end=end;
}

/* public line
  *
  *ctor for makeing copies of lines
  *
  *Parameters : (line)
  *Returns:     
  */

public Line(Line line)
{
this.start=new Point(line.getStart());
this.end=new Point(line.getEnd());
}
/* move
  *
  *takes object and moves it according to the change parameters
  *Parameters : (int xDelta,int yDelta)
  *Returns:     void
  */


public void move(int xDelta, int yDelta)
{
this.getStart().move(xDelta,yDelta);
this.getEnd().move(xDelta,yDelta);
}
 /* toString()
  *
  *creates srting version of object
  *
  *
  *Parameters : ()
  *Returns:     String
  */

public  String toString()
{
return "Line: (" + getStart() + "," + getEnd() + ")";
}

/* equals(Object o)
  *
  *
  *checks if two object are exactly the same
  *
  *Parameters : (Object o)
  *Returns:     boolean
  */

public boolean equals(Object o)
{
//checking for nulle refernce
if(o==null)
{
return false;
}
else if(this.getClass()!=o.getClass())
{
return false;
}
//cheking for same type

else if(this.getClass()!=o.getClass())
{
//checking for sam instances of object
if(this.getStart().equals(((Line)o).getStart())&&this.getEnd().equals(((Line)o).getEnd()))
{
return true;
}
}
return false;
}


 /* draw(Graphics g,Color c, boolean fill)
  *
  *meathod draws a shape of object based on dimenshons that where initilized
  *
  *Parameters : (Graphics g,Color c,boolean fill)
  *Returns:     void
  */


public void draw(Graphics g,Color c, boolean fill)
{
//setting color fo g object
if(c==null)
{
g.setColor(Color.black);
}
else
{
g.setColor(c);
}
//draing line with g object
g.drawLine(this.getStart().getX(),this.getStart().getY(),this.getEnd().getX(),this.getEnd().getY());
}

 /* setStart(Point p)
  *
  *meathod allows teh objects instance variable to be set
  *
  *
  *Parameters : (Point p)
  *Returns:     void
  */
private void setStart(Point p)
{
this.start=p;
}

 /* setEnd(Point p)
  *
  *meathod allows teh objects instance variable to be set
  *
  *
  *Parameters : (Point p)
  *Returns:     void
  */
private void setEnd(Point p)
{
this.end=p;
}
/* getStart()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     Point
  */

public Point getStart()
{
return this.start;
}
/* getEnd()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     Point
  */


public Point getEnd()
{
return this.end;
}
}

