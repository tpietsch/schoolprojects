/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 6
 * Date:       May 19,2011
 *
 * File: ARectangle.java
 *
 * Souces of Help: tutors in lab
 *
 * class for creating shapes specificly squares
 *
 */


import java.awt.*;
public class Square extends ARectangle
{
private int side;
/* Square()
  *
  *sets location default and name
  *Parameters : ()
  *Returns:     
  */


public Square()
{
super("Square",0,0);
}
/* Square()
  *
  *intializes sides size of square and location or upper left corner
  *Parameters : (int x,int y, int side)
  *Returns:     
  */


public Square(int x,int y,int side)
{
super("Square",x,y);
this.side=side;


}
/* Square()
  *
  *intitises sides size of square and lovation or upper left corner
  *Parameters : (Point upperLeft,int side)
  *Returns:     
  */


public Square(Point upperLeft,int side)
{
super("Square",upperLeft);
this.side=side;
}
 /* Square()
  *
  *creates copy of square object
  *Parameters : (Square r)
  *Returns:     
  */


public Square(Square r)
{
super(r);
this.side=r.side;
}
  /* toString()
  *
  *creates srting version of object
  *
  *
  *Parameters : ()
  *Returns:     String
  */

public String toString()
{
return "Square: (" + getSide() + "," + getSide() + ")";
}
 /* equals(Object o)
  *
  *
  *checks if two object are exactly the same
  *
  *Parameters : (Object o)
  *Returns:     boolean
  */
public boolean equals(Object o)
{
//cheking for nuller refernce
if(o==null)
{
return false;
}
else if(this.getClass()!=o.getClass())
{
return false;
}
//cheking for same exact objects
if(this.getSide()==((Square)o).getSide())
{
return true;

}
return false;

}

 /* draw(Graphics g,Color c, boolean fill)
  *
  *meathod draws a shape of object based on dimenshons that where initilized
  *
  *Parameters : (Graphics g,Color c,boolean fill)
  *Returns:     void
  */

public void draw(Graphics g,Color c, boolean fill)
{
//setting color of grpahics object
if(c==null)
{
g.setColor(Color.black);
}
else
{
g.setColor(c);
}
//deicding to creat filled or empty shape
if(fill==true)
{
g.fillRect(super.getUpperLeft().getX(),super.getUpperLeft().getY(),this.getSide(),this.getSide());
}
else
{
g.drawRect(super.getUpperLeft().getX(),super.getUpperLeft().getY(),this.getSide(),this.getSide());
}
}
/* move
  *
  *takes object and moves it according to the change parameters
  *Parameters : (int xDelta,int yDelta)
  *Returns:     void
  */


public void move(int xDelta,int yDelta)
{
super.getUpperLeft().move(xDelta,yDelta);
}

 /* setHeight(int s)
  *
  *this meathod sets the value of objects instance variable
  *
  *
  *Parameters : (int s)
  *Returns:     
  */
private void setSide(int s)
{
this.side=s;
}
 /* getSide()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     int side size
  */
public int getSide()
{
return this.side;
}
}
