/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 6
 * Date:       May 19,2011
 *
 * File: Circle.java
 *
 * Souces of Help: tutors in lab
 *
 * class for creating shapes specificly circles
 *
 */
import java.awt.*;
public class Circle extends Shape
{
private Point center;
private int radius;

 /* Circle
  *
  *ctor calls super intilazez object name
  *
  *Parameters : ()
  *Returns:     
  */

public Circle()
{
super("Circle");
}

 /* Circle
  *
  *ctor calls super intilases center and redius of object
  *
  *Parameters : (Point center,int radius)
  *Returns:     
  */

public Circle(Point center,int radius)
{
this.center=center;
this.radius=radius;
}
 /* Circle
  *
  *ctor creatscopy of circle 
  *
  *Parameters : (Circle c)
  *Returns:     
  */

public Circle(Circle c)
{
//setting this objects another objects variables
this.setRadius(c.getRadius());
this.setCenter(new Point(c.getCenter()));

}

  /* move
  *
  *takes object and moves it according to the change parameters
  *Parameters : (int xDelta,int yDelta)
  *Returns:     void
  */

public void move(int xDelta,int yDelta)
{
//calling move on point to move this object
this.getCenter().move(xDelta,yDelta);
}
  /* toString()
  *
  *creates srting version of object
  *
  *
  *Parameters : ()
  *Returns:     String
  */


public String toString()
{
return "Circle: (" + getCenter() + "," + getRadius() + ")";
}
/* equals(Object o)
  *
  *
  *checks if two object are exactly the same
  *
  *Parameters : (Object o)
  *Returns:     boolean
  */

public boolean equals(Object o)
{
//chekcing for null reference
if(o==null)
{
return false;
}
//checking if types ar equal
else if(this.getClass()!=o.getClass())
{
return false;
}
//chekcing if instances of objects are the same
if(this.getCenter().equals(((Circle)o).getCenter())&&this.getRadius()==((Circle)o).getRadius())
{
return true;
}

else
{
return false;
}
}

 /* draw(Graphics g,Color c, boolean fill)
  *
  *meathod draws a shape of object based on dimenshons that where initilized
  *
  *Parameters : (Graphics g,Color c,boolean fill)
  *Returns:     void
  */

public void draw(Graphics g,Color c, boolean fill)
{
//setting color of graphics object
if(c==null)
{
g.setColor(Color.black);
}
else
{
g.setColor(c);
}
//chekcing to draw a shape filled with color or not
if(fill==true)
{
//draing shape
g.fillOval(this.getCenter().getX()-radius,this.getCenter().getY()-radius,radius*2,radius*2);
}
else
{
//drawing shape
g.drawOval(this.getCenter().getX()-radius,this.getCenter().getY()-radius,radius*2,radius*2);
}

}

 /* setCenter(Point p)
  *
  *meathod allows teh objects instance variable to be set
  *
  *
  *Parameters : (Point p)
  *Returns:     void
  */
private void setCenter(Point p)
{
this.center=p;
}

 /* setRadius(int r)
  *
  *meathod allows teh objects instance variable to be set
  *
  *
  *Parameters : (int r)
  *Returns:     void
  */

private void setRadius(int r)
{
this.radius=r;
}

 /* getCenter()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     Point
  */

public Point getCenter()
{
return this.center;
}
 /* getRadius()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     int
  */

public int getRadius()
{
return this.radius;
}
}

