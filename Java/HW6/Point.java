/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 6
 * Date:       May 19,2011
 *
 * File: Point.java
 *
 * Souces of Help: tutors in lab
 *
 * class for creating points
 *
 */


public class Point
{
private int x;
private int y;
 /* point
  *
  *initilaize point obejcs variables to zero
  *Parameters : ()
  *Returns:    
  */

public Point()
{

this.x=0;
this.y=0;
}
 /* point
  *
  *initializes point objects vaiables 
  *Parameters : (intx,inty)
  *Returns:    
  */

public Point(int x,int y)
{
this.x=x;
this.y=y;

}

 /* point
  *
  *initializes point objects vaiables 
  *Parameters : (Point point)
  *Returns:    
  */

public Point(Point point)
{
this.setX(point.getX());
this.setY(point.getY());
}

/* setX()
  *
  *this meathod sets the value of objects instance variable
  *
  *
  *Parameters : (int)
  *Returns:     
  */

private void setX(int x)
{
this.x=x;
}
/* setY(int y)
  *
  *this meathod sets the value of objects instance variable
  *
  *
  *Parameters : (int y)
  *Returns:     
  */

private void setY(int y)
{
this.y=y;
}

 /* move
  *
  *takes object and moves it according to the change parameters
  *Parameters : (int xDelta,int yDelta)
  *Returns:     void
  */


public void move(int xDelta,int yDelta)
{
this.setX(this.getX()+xDelta);
this.setY(this.getY()+yDelta);

}
 /* toString()
  *
  *creates srting version of object
  *
  *
  *Parameters : ()
  *Returns:     String
  */


public String toString()
{
return "Point: (" + getX() + "," + getY() + ")";
}

/* equals(Object o)
  *
  *
  *checks if two object are exactly the same
  *
  *Parameters : (Object o)
  *Returns:     boolean
  */

public boolean equals(Object o)
{
//cheking for null reference
if(o==null)
{
return false;
}
else if(this.getClass()!=o.getClass())
{
return false;
}
//chekcing if types rae amse with same instances
if(this.getX()==((Point)o).getX()&&this.getY()==((Point)o).getY())
{
return true;
}

return false;

}
/* getX()
  *
  *this meathod gets the value of objects instanace variable
  *
  *
  *Parameters : ()
  *Returns:     int
  */
public int getX()
{
return this.x;
}
/* getY()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     int
  */


public int getY()
{
return this.y;
}

}

