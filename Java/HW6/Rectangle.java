/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 6
 * Date:       May 19,2011
 *
 * File: Rectangle.java
 *
 * Souces of Help: tutors in lab
 *
 * class for creating shapes specificly Rectangles
 *
 */


import java.awt.*;

public class Rectangle extends ARectangle
{
private int width;
private int height;
/* Rectangle()
  *
  *initialize super calss name and location of upper left coere
  *initilaize recangle dimensions
  *Parameters : ()
  *Returns:     
  */
public Rectangle()
{
super("Rectangle",0,0);
this.width=0;
this.height=0;
}
  /* Rectangle(Poit upperLeft,int width,int hegiht)
  *
  *initialize super calss name and location of upper left coere
  *intialize sie of triabgle dimensions
  *Parameters : (int x,int y,int width,int height)
  *Returns:     
  */

public Rectangle(int x,int y,int width,int height)
{
super("Rectangle",x,y);

this.width=width;
this.height=height;

}
  /* Rectangle(Poit upperLeft,int width,int hegiht)
  *
  *initialize super calss name and location of upper left coere
  *intialize sie of triabgle dimensions
  *Parameters :( Poit upperLeft,int width,int hegiht)  
  *Returns:     
  */


public Rectangle(Point upperLeft,int width,int height)
{
super("Rectagnle",upperLeft);
this.width=width;
this.height=height;
}
  /* Rectangle()
  *
  *creates copy of  object
  *Parameters : (Rectangle r)
  *Returns:     
  */


public Rectangle(Rectangle r)
{
this.width=r.width;
this.height=r.height;
}
 /* toString()
  *
  *creates srting version of object
  *
  *
  *Parameters : ()
  *Returns:     String
  */

public String toString()
{
return "Rectangle: (" + getWidth() + "," + getHeight() + ")";
}
/* equals(Object o)
  *
  *
  *checks if two object are exactly the same
  *
  *Parameters : (Object o)
  *Returns:     boolean
  */
public boolean equals(Object o)
{
//cheking for neull refence
if(o==null)
{
return false;
}
else if(this.getClass()!=o.getClass())
{
return false;
}
//cheking if types aer same with smae instances
if(this.getWidth()==((Rectangle)o).getWidth()&&this.getHeight()==((Rectangle)o).getHeight())

{
return true;
}

return false;

}

 /* draw(Graphics g,Color c, boolean fill)
  *
  *meathod draws a shape of object based on dimenshons that where initilized
  *
  *Parameters : (Graphics g,Color c,boolean fill)
  *Returns:     void
  */


public void draw(Graphics g,Color c,boolean fill)
{
//seting graphcs object color
if(c==null)
{
g.setColor(Color.black);
}
else
{
g.setColor(c);
}
//deciding on creaing filed or empyt shape
if(fill==true)
{
g.fillRect(super.getUpperLeft().getX(),super.getUpperLeft().getY(),this.getWidth(),this.getHeight());
}
else
{
g.drawRect(super.getUpperLeft().getX(),super.getUpperLeft().getY(),this.getWidth(),this.getHeight());
}

}
/* move
  *
  *takes object and moves it according to the change parameters
  *Parameters : (int xDelta,int yDelta)
  *Returns:     void
  */


public void move(int xDelta,int yDelta)
{
super.getUpperLeft().move(xDelta,yDelta);
}


 /* setWidth()
  *
  *this meathod sets the value of objects instance variable
  *
  *
  *Parameters : (int g)
  *Returns:     
  */
private void setWidth(int w)
{
this.width=w;
}
 /* setHeight()
  *
  *this meathod sets the value of objects instance variable
  *
  *
  *Parameters : (int g)
  *Returns:     
  */

private void setHeight(int g)
{
this.height=g;
}
/* getWidth()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     int
  */

public int getWidth()
{
return this.width;
}
/* getHeight()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     int
  */
public int getHeight()
{
return this.height;
}
}
