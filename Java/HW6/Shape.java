/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 6
 * Date:       May 19,2011
 *
 * File: Shape.java
 *
 * Souces of Help: tutors in lab
 *
 * abstract class for creating shapes 
 *
 */
import java.awt.*;
public abstract class Shape
{
private String name;
 /* traingel()
  *
  *intilazes super lcas name of shape
  *Parameters : ()
  *Returns:     
  */

public Shape()
{
this.name="";
}
 /* shape()
  *
  *intilazes super lcas name of shape
  *Parameters : (Stirng name)
  *Returns:     
  */

public Shape(String name)
{
this.name=name;
}
 /*getName()
  *
  *gets name of object
  *Parameters : ()
  *Returns:   name  
  */


public String getName()
{
return this.name;
}

 /* move()
  *
  *abstract meathod does nothing
  *Parameters : (int xDelta,int yDetlta)
  *Returns:    void
  */


public abstract void move(int xDelta,int yDelta);
 /* draw()
  *
  *abstract meathod deos nothing
  *Parameters : (Graphics g,Color c,boolean fill)
  *Returns:     void
  */


public abstract void draw(Graphics g,Color c,boolean fill);
}
