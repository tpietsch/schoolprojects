/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 6
 * Date:       May 19,2011
 *
 * File: ARectangle.java
 *
 * Souces of Help: tutors in lab
 *
 * abstract class for creating shapes specificly rectagnles 
 *
 */
public abstract class ARectangle extends Shape
  {
  //declaring instance variable
  private Point upperLeft;
    /* ARecangle
     *
     *This constructio initializez the upper left corner of a box
     *
     *
     *Parameters : none
     *Returns:     
     */

     public ARectangle()
       {
         //calling superclass ctor
         super();
//initializing instance of object 
this.upperLeft=new Point(0,0);
}
 /* Arectangle
  *
  *ctor initilizes upper left point
  *ctor initializ name of super class
  *
  *Parameters : (String name,int x,int y))
  *Returns:     
  */

public ARectangle(String name,int x,int y)
{
//calling supper ctor
super(name);
//setting upperleft
this.upperLeft=new Point(x,y);


}
 /* ARectangle
  *
  *copy contructor
  *
  *Parameters : (String name, File[] files)
  *Returns:     
  */

public ARectangle(ARectangle r)
{
this.setUpperLeft(new Point(r.getUpperLeft()));

}
 /* ARectangle
  *
  *initialize name of super class
  *initliaze upper left
  *
  *Parameters : (String name, Point upperLeft)
  *Returns:    
  */

public ARectangle(String name,Point upperLeft)
{
//calling name super
super("name");

this.upperLeft=upperLeft;
}
 /* Arectangle
  *
  *takes object and moves it according to the change parameters
  *Parameters : (int xDelta,int yDelta)
  *Returns:     
  */

public ARectangle(int xDelta,int yDelta)
{
this.getUpperLeft().move(xDelta,yDelta);

}
 /* toString()
  *
  *creates srting version of object
  *
  *
  *Parameters : ()
  *Returns:     String
  */

public String toString()
{
return "ARectangle: (" + getUpperLeft() + "," + getUpperLeft() + ")";
}
 /* equals(Object o)
  *
  *
  *checks if two object are exactly the same
  *
  *Parameters : (Object o)
  *Returns:     boolean
  */

public boolean equals(Object o)
{
//checking null refernce
if(o==null)
{
return false;
}
//checking if types are same
else if(this.getClass()!=o.getClass())
{
return false;
}
//checking of instances of objecs are same

if(this.getUpperLeft().equals(((ARectangle)o).getUpperLeft()))
{
return true;

}
return false;

}
 /* setUpperLeft(Point p)
  *
  *meathod allows teh objects instance variable to be set
  *
  *
  *Parameters : (Point p)
  *Returns:     void
  */

private void setUpperLeft(Point p)
{
this.upperLeft=p;
}
 /* getUpperLeft()
  *
  *this meathod gets the value of objects instance variable
  *
  *
  *Parameters : ()
  *Returns:     Point
  */

public Point getUpperLeft()
{
return this.upperLeft;
}
}
