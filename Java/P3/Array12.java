/*Name:      Trever Pietsch
 *Account:   cs12ubo
 *Studen ID: A09150869
 *HW:        Programming Assignment 3
 *Date:      7/14/2011
 */


import java.util.*;
import java.lang.*;

/*
 *Class Array12<E>
 *
 *This class creats Array 12 object
 *  
 */


public class Array12<E> implements LimCapList<E>
  {
    //delcatin and initializ calss variables
    private E[] array;
    private int size;
    private int capacity;
    private int front = 0;
    private int back  = 0;

    /** public Array12(int capacity)
     *
     *meathod  : constics array12 object
     *Parameters : 
     *Returns:     
     */

    public Array12(int capacity)
      {  
        //creating array of generic objects
        this.array=(E[]) new Object[capacity];
	//capacity  initialization
	this.capacity=capacity;
      }
    
    /** public int size()
     *
     *meathod  : returns size of Array12
     *Parameters : 
     *Returns:     int
     */
    public int size()
      {
       
       return this.size;
      }

    /** public int capacity()
     *
     *meathod  : returns capacity of Array12
     *Parameters : 
     *Returns:     int
     */

    public int capacity()
      {
        return this.capacity;
      }

   /** public boolean addFront(E o)
     *
     *meathod  : adds an element at Front of Array12
     *Parameters : E o
     *Returns:     boolean
     */

    public boolean addFront(E o) 
      {
        
        //cheink for adding null elemnt
        if(o == null && this.size < this.capacity)
	  {
	    //exception thrown if try to add null
	    throw new NullPointerException();
	    
	  }
        //cheking is you can add another one
        if(this.size == this.capacity)
	  {
	    return false;
	  }
                
	//setting front to bject
        this.array[front] = o ;
        //moving size
	this.size = (this.size)+1;
        //moving front
	this.front=(front+capacity-1)%capacity;

        return true;
      }
    /** public boolean addBack(E o)
     *
     *meathod  : adds an element at end of Array12
     *Parameters : E o
     *Returns:     boolean
     */

    public boolean addBack(E o) 
      {
          //cheking if try to add null
         if(o == null && this.size < this.capacity)
	  {
	    //throws if you ttry to add null object
	    throw new NullPointerException() ;
		  }
        //checking if room to add elements
        if(this.size == this.capacity)
	  {
	    return false;
	  }
	 
	//setting back to object
	this.array[(back+1)%capacity] = o ;

        //incementing size
	this.size = (this.size)+1;

        //moving back     
	this.back = (back+1)%capacity;

	return true;

        
      }
    /** public E removeFront()
     *
     *meathod  : removes element fron front of array
     *Parameters : 
     *Returns:     E
     */

    public E removeFront()
      {
        //is size is equal only null object exsist
        if(this.size == 0)
	  {
	    return null;  
	  }
	
        //decrementing size
	this.size = this.size-1;
        
        //gettng current front
        E temp = array[(front+1)%capacity];

	//setting currecnt front to be remnoved
        array[(front+1)%capacity]=null;

	//decremeint front
	this.front=(front+1)%capacity;


        return temp;
      

        
        
      }
    /** public E removeBack()
     *
     *meathod  : removes element fron end of array
     *Parameters : 
     *Returns:     E
     */


    public E removeBack()
      {
        //ceking if onlynull elemnts exist
        if(this.size == 0)
	  {
	    return null;  
	  }
	
        //decremenitng size
	this.size = this.size-1;
       
        //gettting object at back
        E temp = array[((back)%capacity)];

	//setting back to null
        array[back]=null;

	//decremingint back
	this.back=((back+capacity-1)%capacity);


		
	return temp;

      }

    /** public E peekFront()
     *
     *meathod  : returns front of array
     *Parameters : 
     *Returns:     E
     */


    public E peekFront()
      {

        if(size ==0)
	  {
	    return null;
	  }

	else
	  {
	    return array[(front+capacity+1)%capacity];
	  }
      }

    /** public E peekBack()
     *
     *meathod  : returns back of array
     *Parameters : 
     *Returns:     E
     */


    public E peekBack()
      {
        if(size == 0)
	  {
	    return null;
          }

	else
	  {
            return array[(back)%capacity];
	  }
      }
    /** public boolean equals(Object o)
     *
     *meathod  : checks for object equality
     *Parameters : Object o
     *Returns:     boolean
     */

    public boolean equals(Object o)
      {
        //cheking if can cast objects
        if(!( o instanceof Array12))
	  {
	    
	    return false;
	  }
        //casting object to be compared
	Array12 temp=(Array12)o;

        //size must be equals
	if(temp.size != this.size)
	  {
	    
	    return false;
	  }

	//cheking all elemenst of array for equaliity
        for(int i=1; i < this.size ;i++)
	  {

            if(!((this.array[(front+i)%capacity]).equals(temp.array[(temp.front+i)%temp.capacity])))
	      {
	         return false;
 	      }

	  }

        return true;
      }

   


       
  }
