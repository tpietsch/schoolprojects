/*Name:      Trever Pietsch
 *Account:   cs12ubo
 *Studen ID: A09150869
 *HW:        Programming Assignment 3
 *Date:      7/14/2011
 */

import java.*;

/*
 *Class Stack12<e>
 *
 *This class allows for creation of stack data structrue
 *  
 */

public class Stack12<E> implements LimCapStack<E>
  {

    //declaring stack insace variable
    private Array12<E> stack ;
    private int size ;
    private int capacity ;

    /**
     *public Stack12(int cap)
     *
     *constuctor
     *
     */
    public Stack12(int cap)
      { 
        //makes array12 adn initilzez array 12 cap
        stack=new Array12<E>(cap);
	capacity=cap;
      }

    /** public int capacity()
     *
     *meathod  : returns capacity of stack
     *Parameters : 
     *Returns:     int
     */

    public int capacity()
      {
        return capacity;
      }

    /** public int size()
     *
     *meathod  : returns size of stack
     *Parameters : 
     *Returns:     int
     */

    public int size()
      {
        
        return stack.size();
      }

    /** public boolean push(E o)
     *
     *meathod  : adds an element at end of Array12
     *Parameters : E o
     *Returns:     boolean
     */


    public boolean push(E e)
      {
        return stack.addBack(e); 
      }

    /** public E pop()
     *
     *meathod  : removes element fron end of array
     *Parameters : 
     *Returns:     E
     */

    public E pop()
      {
        return stack.removeBack();
      }


    /** public E peek()
     *
     *meathod  : returns top of stack
     *Parameters : 
     *Returns:     E
     */

    public E peek()
      {
        return stack.peekBack();
      }

    /** public boolean equals(Object o)
     *
     *meathod  : checks for object equality
     *Parameters : Object o
     *Returns:     boolean
     */

    public boolean equals(Object o)
      {
        //checking if object are comparable
        if(!( o instanceof Stack12))
	  {
	    return false;
	  }
        //casting
	Stack12 temp=(Stack12)o;
       
        //sizes must be euqla for object to be equal
	if((temp.size) != (this.size))
	  {
	    return false;
	  }
        //checking if the arrays in stack are equal
	if(!(this.stack).equals(temp.stack))
	  {
	    return false;
	  }

	return true;

        
      }
  }
