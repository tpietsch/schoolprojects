/*Name:      Trever Pietsch
 *Account:   cs12ubo
 *Studen ID: A09150869
 *HW:        Programming Assignment 3
 *Date:      7/14/2011
 */

import java.util.*;
import javax.swing.*;

/*
 *Class LimCaptListTester
 *
 *This class test prper method implementation of Array12
 *  
 */

public class LimCapListTester extends junit.framework.TestCase
{
  //declaring/initialize some calss variable to be used in tests

  Array12<String> a1=new Array12<String>(0);
  Array12<String> a2=new Array12<String>(1);
  Array12<String> a3=new Array12<String>(2);
  Array12<String> a4=new Array12<String>(4);
  Array12<String> a5=new Array12<String>(5);
  Array12<String> a6=new Array12<String>(6);
  Array12<String> a7=new Array12<String>(8);
  Array12<String> a8=new Array12<String>(10);

 /* main meathod()
  *
  *This meathod drives the program
  *Open junit tester to test tests
  *Parameters : String[]{"LimCapListTester"}
  *Returns:     void
  */


  public static void main(String []args)
    {
     //opening junit gui version
     junit.swingui.TestRunner.main(new String[]{"LimCapListTester"});
   
    }

  public void testSize()
    {

      //checking if initializes sizes are equals
      assertTrue(a1.size() == 0);
      assertTrue(a8.size() == 0);

      //cahnging size

      a8.addFront("");

      assertTrue(a8.size()==1);

      a8.addFront("");
      a8.addFront("");
      a8.addFront("");
      a8.addFront("");
      a8.addFront("");
      a8.addFront("");
      a8.addFront("");
      a8.addFront("");
      a8.addFront("");
     
       //checking if changes size is equal to number of chagnes
      assertTrue(a8.size() == 10);
      

      assertFalse(a1.addFront(""));
      assertFalse(a1.addBack(""));

    }

  public void testAdd()
    {
      assertFalse(a1.addFront("1"));

      a2.addFront("1");
      a2.addFront("2");

      assertTrue(a2.peekBack().equals("1"));
   
      //assertFalse(a5.removeFront());

      a5.addFront("1");
      a5.addFront("2");
      a5.addFront("3");
      a5.addFront("4");
      a5.addFront("5");
      a5.addFront("6");
       
      assertFalse(a5.addFront("7"));
      assertTrue(a5.peekFront().equals("5"));
      assertTrue(a5.size() == 5);
      
      a4.addBack("1");
      a4.addFront("2");
      a4.addFront("3");
      a4.addFront("4");
      //cheking if expections work properly
      try{
        
	a8.addFront(null);
	fail("Should Throw NullPointerException");

      }
      catch(NullPointerException e)
        {
	}
      try{

        a8.addBack(null);
	fail("Should Throw NullPointerException");

      }
      catch(NullPointerException e)
      {
      }

     

    }

  public void testPeek()
    {
      //adding to lsit
      a7.addFront("1");
      a7.addFront("2");
      a7.addFront("3");
      a7.addFront("4");
      a7.addFront("5");
      a7.addFront("6");

      //chcking if ends are proper
      assertTrue(a7.peekBack().equals("1"));
      assertFalse(a7.peekBack().equals("6"));
      assertFalse(a7.peekBack().equals("2"));

      assertTrue(a7.peekFront().equals("6"));
      assertFalse(a7.peekFront().equals("1"));
      assertFalse(a7.peekFront().equals("5"));

      a7.removeFront();

      //cheking if remove works
      assertTrue(a7.peekFront().equals("5"));

      a2.removeFront();
      a2.removeFront();
      a2.addFront("1");
      assertTrue(a2.peekFront().equals("1"));
      assertTrue(a2.peekBack().equals("1"));

 
      //cheking if back and front are same
      assertTrue(a2.peekFront().equals("1"));
      assertTrue(a2.peekBack().equals("1"));

      a2.removeBack();

      assertFalse(a2.peekFront().equals("1"));
      assertFalse(a2.peekBack().equals("1"));


      a3.addFront("1");
      a3.addFront("2");

      assertTrue(a3.peekFront().equals("1"));
      assertTrue(a3.peekBack().equals("2"));





    }

  public void testCapacity()
    {
      //initialize list capaciies
      Array12<String> a9=new Array12<String>(10);
      Array12<String> a10=new Array12<String>(0);
      Array12<String> a11=new Array12<String>(1);


      //cheking if initiales is correct
      assertTrue(a9.capacity() == 10);
      assertTrue(a10.capacity() == 0);
      assertTrue(a11.capacity() == 1);

      assertFalse(a9.capacity() == 1);
      assertFalse(a10.capacity() == 1);
      assertFalse(a11.capacity() == 3);

      
    }

  public void testEquals()
    {

      //initialize arrays
      Array12<String> a9=new Array12<String>(10);
      Array12<String> a10=new Array12<String>(5);
      Array12<String> a11=new Array12<String>(5);

      //arrays are all empty and should all be equal to begin with
      assertTrue(a9.equals(a10) && a9.equals(a11) && a10.equals(a11) );
      

      //altering arays of differenct capacity
      a9.addFront("1");
      a9.addFront("2");
      a9.addFront("3");
      a9.addBack("1");
      a9.removeBack();
      a9.removeBack();
      a9.removeFront();

      a10.addFront("1");
      a10.addFront("2");
      a10.addFront("3");
      a10.addFront("4");
      a10.addFront("5");

      a10.removeBack();
      a10.removeFront();
      a10.removeFront();
      a10.removeFront();
      

      //cheking equalisty
      assertTrue(a10.size() == a9.size());
      assertTrue(a10.equals(a9));

      a10.addFront("2");
      a10.addFront("");
      a10.addFront("");
      a10.addFront("");
      a10.addFront("");
      a10.addFront("");
      a10.addFront("");
      a10.addFront("");
      a10.addFront("");
      a10.addFront("");
      a10.addFront("");
      a10.addFront("");

      assertFalse(a10.equals(a9));
   
      //alterin array of same capacity
      a11.addFront("1");
      a11.addBack("2");
      a11.addBack("3");
      a11.addFront("4");
      a11.addFront("5");
      a11.removeFront();
      a11.removeFront();
      a11.removeFront();
      a11.removeBack();
      assertTrue(a11.equals(a9));











      
    }
}
