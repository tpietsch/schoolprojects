/*Name:      Trever Pietsch
 *Account:   cs12ubo
 *Studen ID: A09150869
 *HW:        Programming Assignment 3
 *Date:      7/14/2011
 */



import java.*;

/*
 *Class Queue12
 *
 *This class test creates a Queue data Structure
 *  
 */


public class Queue12<E> implements LimCapQueue<E>
  {
    //delcaring calss variables
    private Array12<E> que ;
    private int size ;
    private int capacity ;

    /** public Queue12
     *
     *meathod  : constucs a Queue12 object
     *Parameters : 
     *Returns:     
     */

    public Queue12(int cap)
      {
        que=new Array12<E>(cap);
	capacity=cap;
      }

    /** public int capacity()
     *
     *meathod  : returns capacity of queue
     *Parameters : 
     *Returns:     int
     */

    public int capacity()
      {
        return capacity;
      }
   /** public int size()
     *
     *meathod  : returns size of queue
     *Parameters : 
     *Returns:     int
     */
    public int size()
      {
        
        return que.size();
      }

    /** public boolean enqueue(E o)
     *
     *meathod  : adds an element at end of Array12
     *Parameters : E o
     *Returns:     boolean
     */

    public boolean enqueue(E e)
      {
        return que.addBack(e); 
      }

    /** public E dequeue()
     *
     *meathod  : removes element fron front of array
     *Parameters : 
     *Returns:     E
     */
    public E dequeue()
      {
        return que.removeFront();
      }
      
    /** public E peek()
     *
     *meathod  : returns front of queu
     *Parameters : 
     *Returns:     E
     */


    public E peek()
      {
        return que.peekFront();
      }

    /** public boolean equals(Object o)
     *
     *meathod  : checks for object equality
     *Parameters : Object o
     *Returns:     boolean
     */

    public boolean equals(Object o)
      {
        //checking if object can be casted
        if(!( o instanceof Queue12))
	  {
	    return false;
	  }
        //casting 
	Queue12 temp=(Queue12)o;


        //sizes must be equal for size to be equal
	if((temp.size) != (this.size))
	  {
	    return false;
	  }
        //cehcking if array insstances are equals
	if(!(this.que).equals(temp.que))
	  {
	    return false;
	  }

	return true;

        
      }
  }
