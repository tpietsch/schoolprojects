import java.lang.Exception.*;

public class EmptyQueueException extends RuntimeException
  {
    public EmptyQueueException()
      {
        super();
      }
    public EmptyQueueException(String error)
      {
        super(" "+error);
      }
  }
