import java.util.Scanner;

/** A simple command line calculator.
 *  Requires classes that solve assignment P6, UCSD CSE 12 Winter 2010.
 *  @author Paul Kube
 */
public class Calculator {

  public static void main(String args[]) {

    Scanner in = new Scanner(System.in);
    String line;

    System.out.println("Welcome to the Calculator.");
    System.out.println("Type an expression, then press Enter.");
    System.out.println("An empty line exits the program.\n");
    System.out.print("CALC: ");

    while(!((line = in.nextLine()).isEmpty())) {
      ASTNode root = Expr.parse(line);
      if(root == null) {
	System.out.println("SYNTAX ERROR somewhere on that line!");
      } else {
	System.out.println("= " + root.eval());
      }
      System.out.print("CALC: ");
    }

    System.out.println("Thanks for using the Calculator!  See you later.");
  }
}
