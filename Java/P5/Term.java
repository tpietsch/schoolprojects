public class Term extends ASTNode
  {
   

    public static Term parse(String line)
      {
                
         return new Term();
      }
      
    public double eval()
      {
       
	if(tm != null && fact == null)
	  {
	    return tm.eval();
	  }

	if(tm != null && fact != null)
	  {
	    if("*".equals(this.op))
	      {
	        return fact.eval()*tm.eval();
	      }
	    else
	      {
	        return tm.eval()/fact.eval();
	      }
	  }
	if(tm == null && fact != null)
	  {
	    return fact.eval();
	  }

	else return Double.parseDouble(line);
      }
  }
