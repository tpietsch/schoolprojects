public class Constant extends ASTNode
  {
    private double eval;

    public Constant(double i)
      {
        eval= i;
      }
    public static Constant parse(String line)
      {
        line= line.trim();

      try{

        double eval=Double.parseDouble(line);
	
        return new Constant(eval);

      }

      catch(NumberFormatException e)
      {
       return null;

      }
      
      }
    
    public double eval()
      {
          return eval;
      }
  }

