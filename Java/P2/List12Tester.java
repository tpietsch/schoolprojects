/*Name:      Trever Pietsch
 *Account:   cs12ubo
 *Studen ID: A09150869
 *HW:        Programming Assignment 1
 *Date:      6/30/2011
 */

import java.util.*;
import javax.swing.*;

/*
 *Class List12Tester
 *
 *This class test prper method implementation of List12Class
 *  Meathods
 */

public class List12Tester extends junit.framework.TestCase
{
  /* main meathod()
  *
  *This meathod drives the program
  *Open junit tester to test tests
  *Parameters : String[]{"List12Tester"}
  *Returns:     void
  */

  public static void main(String []args)
    {
     //opening junit gui version
     junit.swingui.TestRunner.main(new String[]{"List12Tester"});
   
    }

  /* public void testContains()
  *
  *meathod tests List<E> implementation of contains
  *Parameters : none
  *Returns:     void
  */

  public void testContains() 
    {
      //creating list
      List<String> theL = new List12<String>();
      //adding strings
      theL.add("And");
      theL.add("Bob");
      //junit tests
      assertTrue(theL.contains("And"));
      assertTrue(theL.contains("Bob"));
      assertFalse(theL.contains("Car"));

         }

  /* public void testGet()
  *
  *meathod tests List<E> implementation of get
  *Parameters : none
  *Returns:     void
  */

  public void testGet()
    {
      //creating list
      List<String> theL = new List12<String>();

      //adding to list
      theL.add("And");
      theL.add("Bob");
      theL.add("Car");
      theL.add("blob");

      //junit test if get is gettig correct strings
      assertEquals(theL.get(0),"And");
      assertEquals(theL.get(1),"Bob");
      assertEquals(theL.get(2),"Car");
      assertEquals(theL.get(theL.size()-1),"blob");

      //testing for out of bounds exceptions
      //for less then zero and greater than size
      try
        {
	  
	  theL.get(theL.size()+10000);
          

	  fail("did not throw IndexOutOfBoundsExcpetion");
	}
      catch(IndexOutOfBoundsException e)
        {

	}
      try
        {
	  theL.get(-10000);

	  fail("did not throw IndexOutOfBoundsExcpetion");
	}
      catch(IndexOutOfBoundsException e)
        {

	}
      try
        {
	  theL.get(theL.size()+9);
	           

	  fail("did not throw IndexOutOfBoundsExcpetion");
	}
      catch(IndexOutOfBoundsException e)
        {

	}
    

    }

  /* public void testAdd()
  *
  *meathod tests List<E> implementation of add
  *Parameters : none
  *Returns:     void
  */

  public void testAdd() 
    {

      //creating list
      List<String> theL = new List12<String>();

      //adding to list
      theL.add("blob");

      //cheking if contains added unit
      assertTrue(theL.contains("blob"));
      assertFalse(theL.contains("owejnofjnqjw"));

      //adding more  
      theL.add(0,"And");

      //cheking if added at proper location
      assertEquals(theL.get(0),"And");

      
      //cheking for exception throw for out of bounds
      try
        {
	  
	  theL.get(theL.size()+10000);
          

	  fail("did not throw IndexOutOfBoundsExcpetion");
	}
      catch(IndexOutOfBoundsException e)
        {

	}
      try
        {
	  theL.get(-10000);

	  fail("did not throw IndexOutOfBoundsExcpetion");
	}
      catch(IndexOutOfBoundsException e)
        {

	}
      try
        {
	  theL.get(theL.size()+9);
	           

	  fail("did not throw IndexOutOfBoundsExcpetion");
	}
      catch(IndexOutOfBoundsException e)
        {

	}
    
    theL.clear();

    //cheking if add can add for a large list
    int j=0;
    for(int i=1000;j<i;j++)
      {
        theL.add(("j="+j));
      }
    
    //cheking if iterated through whole list
    assertTrue(theL.size()==1000);

    //cheking if added  right thing in right spot
    assertTrue(theL.get(theL.size()-1).equals("j="+(j-1)));

    }

  /* public void testClear()
  *
  *meathod tests List<E> implementation of clear
  *Parameters : none
  *Returns:     void
  */

  public void testClear()
    {

      //ceating list
      List<String> theL = new List12<String>();

      //filling list
      theL.add("And");
      theL.add("Bob");
      theL.add("Car");
      theL.add("blob");

      //clearing list
      theL.clear();

      //list should not conating any of the added elements
      assertFalse(theL.contains("Blob"));
      assertFalse(theL.contains("Bob"));
      assertFalse(theL.contains("And"));
      assertFalse(theL.contains("Car"));

      //list size should be zero
      assertTrue(theL.size()==0);
    
    }

  /* public void testEquals()
  *
  *meathod tests List<E> implementation of equals
  *Parameters : none
  *Returns:     void
  */

  public void testEquals()
    {
       //creating two lists
       List<String> theC = new List12<String>();
       List<String> theL = new List12<String>();
       List<String> theB = new List12<String>();

       //adding same el;emts in both lists
       theC.add("craziness");
       theL.add("And");
       theL.add("Bob");
       theB.add("And");
       theB.add("Bob");

       //equlas should return same hashcode
       int k=theL.hashCode();
       int b=theB.hashCode();
       int c=theC.hashCode();

       //junit check for same hascode
       assertTrue(b==k);
       assertFalse(b==c);
    }

  /* public void testHash()
  *
  *meathod tests List<E> implementation of hash
  *Parameters : none
  *Returns:     void
  */

  public void testHash()
    {
       //creating 3 lists
       List<String> theL = new List12<String>();
       List<String> theB = new List12<String>();
       List<String> theC=new List12<String>();

       //adding same thing to list B and L different add to list C
       theC.add("somethign wierd");
       theL.add("And");
       theL.add("Bob");
       theB.add("And");
       theB.add("Bob");

       //cheking if B==L and C!=B hashcode should return equals
       assertTrue(theL.equals(theB));
       assertFalse(theB.equals(theC));

      
    }

  /* public void testIndexOf()
  *
  *meathod tests List<E> implementation of indexOf
  *Parameters : none
  *Returns:     void
  */

  public void testIndexOf()
    {

      //creayting list
      List<String> theL = new List12<String>();

      //adding to list
      theL.add("Bob");
      theL.add("And");

      //cheking if added elemts are in lisst and not added ones are not
      assertTrue(0==theL.indexOf("Bob"));
      assertFalse(1==theL.indexOf("Bob"));
      assertFalse(0==theL.indexOf("And"));
      assertTrue(1==theL.indexOf("And"));


    }

  /* public void testIterator()
  *
  *meathod tests List<E> implementation of iterator
  *Parameters : none
  *Returns:     void
  */

  public void testIterator()
    {
      List<String> theL = new List12<String>();
      theL.add("bob");
      theL.add("car");
      theL.add("and");
      Iterator<String> iter=theL.iterator();
      try
        {
	  iter.remove();
	  fail("did not throw IllegalStateException");
	}
      catch(IllegalStateException e)
        {
	}
      assertTrue(iter.hasNext());
      assertEquals(iter.next(),"bob");
      iter.next();
      iter.next();
      assertFalse(iter.hasNext());


    }

  /* public void testIsEmpty()
  *
  *meathod tests List<E> implementation of isEmpty
  *Parameters : none
  *Returns:     void
  */

  public void testIsEmpty()
    {
      //creatin list
      List<String> theL = new List12<String>();

      //adding list
      theL.add("And");
      theL.add("Bob");
      theL.add("Car");

      //size shoudl be greatr than 0 because we added stuff
      assertFalse(theL.size()==0);
      //list is not empyut
      assertFalse(theL.isEmpty());
      //cleard list
      theL.clear();

      //list should be empty and zero size
      assertTrue(theL.isEmpty());
      assertTrue(theL.size()==0);


    }
  
  /* public void testRemove()
  *
  *meathod tests List<E> implementation of remove
  *Parameters : none
  *Returns:     void
  */

  public void testRemove()
    {
      //creating list
      List<String> theL = new List12<String>();

      //filling list
      theL.add("And");
      theL.add("Bob");
      theL.add("Car");
      
      //remove from list
      theL.remove("And");

      //list shouid contain bob car but not And
      assertFalse(theL.contains("And"));
      assertTrue(theL.contains("Bob"));
      assertTrue(theL.contains("Car"));
      theL.remove("Bob");
      theL.remove("Car");
      theL.remove("something else");
      //list shoudl be empty
      assertTrue(theL.isEmpty());
    }
    
  /* public void testSet()
  *
  *meathod tests List<E> implementation of set
  *Parameters : none
  *Returns:     void
  */

  public void testSet()
    {
      //creasing list
      List<String> theL = new List12<String>();

      //filling list
      theL.add("And");
      theL.add("Bob");

      //setting list at speicif indecies
      theL.add(0,"Car");
      theL.set(0,"Blob");
      theL.set(theL.size()-1,"Cake");

      //replacing indexes does not chegn size
      assertFalse(theL.size()==4);
      assertTrue(theL.size()==3);
      assertEquals(theL.get(theL.size()-1),"Cake");

      //should conating the string at these intervals
      assertEquals(theL.get(0),"Blob");
      assertEquals(theL.get(1),"And");
      //shoudl not contain car
      assertFalse(theL.contains("Car"));

      //trying index out of bounds greater than size and less then zero

      try
        {
	  
	  theL.get(theL.size()+10000);
          

	  fail("did not throw IndexOutOfBoundsExcpetion");
	}
      catch(IndexOutOfBoundsException e)
        {

	}
      try
        {
	  theL.get(-10000);

	  fail("did not throw IndexOutOfBoundsExcpetion");
	}
      catch(IndexOutOfBoundsException e)
        {

	}
      try
        {
	  theL.get(theL.size()+9);
	           

	  fail("did not throw IndexOutOfBoundsExcpetion");
	}
      catch(IndexOutOfBoundsException e)
        {

	}


    }

  /* public void testSize()
  *
  *meathod tests List<E> implementation of size
  *Parameters : none
  *Returns:     void
  */

  public void testSize()
    {
     //creatin list
      List<String> theL = new List12<String>();

     // addig to list
      theL.add("And");
      theL.add("Bob");
      theL.add(0,"Car");

      //replacing  index should not increase size
      theL.set(0,"Blob");
      theL.set(theL.size()-1,"Cake");
      //size should be 4
      assertFalse(theL.size()==5);
      assertTrue(theL.size()==3);
      //clear
      theL.clear();
      //list should be empty
      assertTrue(theL.size()==0);

    }
}
