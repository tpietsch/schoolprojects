import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.*;

/**
 *Name:      @author Trever Pietsch
 *Account:   @author cs12ubo
 *Studen ID: A09150869
 *HW:        Programming Assignment 1
 *Date:      7/7/2011
 *

 *Class TimeList
 *
 *This class calculates time of list implementation vs arraylist
 *  
 */


public class TimeList
  {
    /** main meathod()
  *
  *This meathod drives the program
  *
  *Parameters : String[]args
  *Returns:     void
  */

    public static void main(String[]args)
      { 
         //initializing and decaltin local variablkes
         String command=args.toString();
         int n=1000;
	 int start=2000;
	 int end=50000;
	 double runs=1000;
	 double count=0;
	 double secs=0;
         double secsSd=0;
	 double secsAvg=0;
	 double secsTwo=0;
         //cheking conmmand line input
	 if(command.equals("linked front"))
	   {
	     TimeList list=new TimeList();
	    	     for(; start <= end ; start = start + n )
	     {
	     //loopinbg thgorugh runs
             for(int i=0;i<runs;i++)
	     {
	     
	     secsTwo=list.linkedPrint(start, true);
             //gathering data
	     secsAvg=secsAvg+secsTwo;
             secsSd=(secsTwo*secsTwo)+secsSd;


	     }
	     //calulaiting average and standar deviation
             secsAvg=secsAvg/runs;
	     double j=(secsSd/runs)-(secsAvg*secsAvg);
             secsSd=Math.sqrt(j);
             System.out.printf("%5d%10f%15f\n",start,secsAvg,secsSd);

	     }


	   }
         //cheking commanbd line input
	 else if(command.equals("linked back"))
	   {
	    TimeList list=new TimeList();
	    	     for(; start <= end ; start = start + n )
	     {
	     //looping thorugh runs

             for(int i=0;i<runs;i++)
	     {
	     
	     secsTwo=list.linkedPrint(start, false);
             //gathering data
	     secsAvg=secsAvg+secsTwo;
             secsSd=secsTwo*secsTwo+secsSd;


	     }
             secsAvg=secsAvg/runs;
	     double j=(secsSd/runs)-(secsAvg*secsAvg);
             secsSd=Math.sqrt(j);
             System.out.printf("%5d%10f%15f\n",start,secsAvg,secsSd);

	     }




	   }
	 //cheking command lin input
	 else if(command.equals("array front"))
	   {
	    
	     TimeList list=new TimeList();
	    	     for(; start <= end ; start = start + n )
	     {
	    //looping thourgh runs
             for(int i=0;i<runs;i++)
	     {
	     
	     secsTwo=list.arrayPrint(start, true);

	     secsAvg=secsAvg+secsTwo;
             secsSd=secsTwo*secsTwo+secsSd;


	     }
             secsAvg=secsAvg/runs;
	     double j=(secsSd/runs)-(secsAvg*secsAvg);
             secsSd=Math.sqrt(j);
             System.out.printf("%5d%10f%15f\n",start,secsAvg,secsSd);

	     }
	   }
	   //chaking command line input
	 else 	   
	   {
	     TimeList list=new TimeList();
	     //looping through everythousand
	    	     for(; start <= end ; start = start + n )
	     {
	     //looping throught runs
             for(int i=0;i<runs;i++)
	     {
	     
	     secsTwo=list.arrayPrint(start, false);
             //gathering data to calculate
	     secsAvg=secsAvg+secsTwo;
             secsSd=(secsTwo*secsTwo)+secsSd;
             

	     }

	     //calculating average
             secsAvg=secsAvg/runs;
	     double j=(secsSd/runs)-(secsAvg*secsAvg);
             secsSd=Math.sqrt(j);
	     //printing statement
             System.out.printf("%5d%10f%15f\n",start,secsAvg,secsSd);

	     }
	   }



      }
/** public void linkedPrint()
  *
  *meathod gets time for list creation and initialization
  *Parameters : double N , boolean B
  *Returns:     double
  */


    public double linkedPrint(double N,boolean B)
      {
        System.gc();
        long start = System.nanoTime();

        List<String> theL = new List12<String>();
        for(int i=0; i<N && B; i++) {
        theL.add(0,null);
        }
       theL.clear();
       for(int i=0; i<N && !B; i++) {
       theL.add(i,null);
         }
	
	

        long end = System.nanoTime();
        double seconds = (end - start) / 1.0e9;
        return seconds;
        

      }

      /** public double arrayPrint
  *
  *meathod calculates time for list creation 
  *Parameters : double N, boolean B
  *Returns:     double
  */


    public double arrayPrint(double N, boolean B)
      {
        System.gc();
        long start = System.nanoTime();
        List<String> theA = new ArrayList<String>();
        for(int i=0; i<N && B; i++) {
        theA.add(0,null);
        }
         theA.clear();
         for(int i=0; i<N && !B; i++) {
         theA.add(i,null);
          }
	
        
        long end = System.nanoTime();
        double seconds = (end - start) / 1.0e9;
        return seconds;


      }
  }
