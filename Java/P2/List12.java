import java.util.*;
import java.io.*;
import java.lang.*;

/**
 *Name:      @author Trever Pietsch
 *Account:   @author cs12ubo
 *Studen ID: A09150869
 *HW:        Programming Assignment 1
 *Date:      7/7/2011
 *

 *Class List12
 *
 *This class implements list interface
 *  
 */

public class List12<E> implements java.util.List<E>
  {
    //creating instance variables of class
    private Node<E> head;
    private int size;
    
    /** public boolean add(E o)
     *
     *meathod  : adds an element at begining of list
     *Parameters : E o
     *Returns:     boolean
     */

    public boolean add(E o)
      {
        //creating new node in list
        Node<E> node=new Node<E>(o,head.getSuccessor());
        head.next=node;
	
        size++;

        return true;

      }
    

    /** public void add(int index, E elemnt)
      *
      *meathod adds at specified inex
      *Parameters : int index E emelnt
      *Returns:     void
      */

    public void add(int index, E element) 
      {
       if(index<0 || index>size) throw new IndexOutOfBoundsException();
        Node<E> cursor=head;

	//loop to iterato till correct index
	while(--index>0)
	  {
	    cursor=cursor.getSuccessor();
	  }
	//switching node pointers
	Node<E> node=new Node<E>(element,null);
	node.setSuccessor(cursor.getSuccessor());
        cursor.setSuccessor(node);
	//list size increased
	size++;

      }


    /** public void clear()
      *
      *meathod clears list of objects
      *Parameters : none
      *Returns:     void
      */

    public void clear()
      {
        //setting dummy head to be nul
        head.setSuccessor(null);
	//resetting size
        size=0;
      }

    /** public boolean contains
      *
      *checks of object is contained in list
      *Parameters : Object o
      *Returns:     boolean
      */

    public boolean contains(Object o)
      {
        
	Node<E> cursor=head;
        int index=0;
        //iterating until object is reached  
        while(!(cursor.getData().equals(o)) && index < this.size)
          {
	    cursor=cursor.getSuccessor();
            index++;
          }
	    //if object is ot reached then index>size
	    if(index>this.size)
	      {
	        return false;
	      }
	     return true;
        	 
      }


    /** public boolean equals
      *
      * checks if two object are equal in list
      *Parameters : Object o
      *Returns:     boolean
      */

    public boolean equals(Object o)
      {
        List12 obj;
	//cheking if the object can be compared
        if(! (o instanceof List))
	  {
            return false;
	  }
	else
	  {
	    //casting object
	    obj = (List12<E>)o;
	  }
        //size mnust be equal or list cannont be
        if(obj.size() != this.size())
	  {

	    return false;
	  }
        else 
	  {
	    //creating iterators to compare each elemtn in list
	    Iterator itA=this.iterator();
	    Iterator itB=obj.iterator();
	    while(itA.hasNext())
	      {
	        if(itA != itB)
	        return false;
	      }
	  }
	  
	 
	return true;
	    
      
      }
    /** public E get(int index)
      *
      * returns elemtn in array at specified inedxz
      *Parameters : iont index
      *Returns:     E
      */

    public E get(int index)
      {
        Node<E> cursor=head;
	index=index+1;
	//looping till corect indexz
	while(--index>0)
	  {
	    cursor=cursor.getSuccessor();
	  }
        //returning data of node at index
        return cursor.getData();
      }

    /** public int hashCode()
      *
      * returns hashCOde of  array
      *Parameters : none
      *Returns:     int
      */

    public int hashCode()
      {
        int hashCode = 1;
        Iterator i = this.iterator();
	//calulates int hashCode value
        while (i.hasNext())
	  {
            Object obj = i.next();
            hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode());
          }
	return hashCode;
      }

    /** public int index of
      *
      *meathod gets first index of specific elemtn
      *Parameters : objec o
      *Returns:     int
      */

    public int indexOf(Object o)
      {
        
        Node<E> cursor=head;
        int index=0;
        //loops while object has not been reached
        while(!(cursor.getData().equals(o)) && index < this.size)
          {
	    cursor=cursor.getSuccessor();
            index++;
          }
	//if object has not been reached return -1
	if(index>this.size)
	  {
	    return -1;
	  }
	return index;
            
                          
        
      }
    /** public boolean isEmpty
      *
      *cheacks if list is empty
      *Parameters : none
      *Returns:     boolean
      */

    public boolean isEmpty()
      {
        //chekcs if head is pointing to null
        return head.getSuccessor() == null;

      }

    /** public Iterator iterator()
      *
      *meathod creates iterator for List12
      *Parameters : none
      *Returns:     iterator
      */

    public Iterator iterator()
      {
        return null;
      }

    /** public boolean remove
      *
      *removes node in list
      *Parameters : object o
      *Returns:     boolean
      */

    public boolean remove(Object o)
      {
         int tmp=this.size;
         Node<E> cursor=head;
         Node<E> cursor2=head;
	 //looping for size of aray
         while(--tmp>0)
           {
	     //if object equals parameter then change pointers
             if(cursor.getData().equals(o))
               {
                 cursor2.setSuccessor(cursor.getSuccessor());
                 return true;
                }
             cursor2=cursor;
             cursor=cursor.getSuccessor();
             
           }
	
        //decreasing size by removed elemnet
        this.size=this.size-1;
        return true;

      }
    /** public E set
      *
      *sets a node to parameter
      *Parameters : int index, E element
      *Returns:     E
      */

    public E set(int index, E element)
      {
       if(index<0 || index>size) throw new IndexOutOfBoundsException();
        Node<E> cursor=head;
	index=index+1;
	//looping till index is reached
	while((index--)>0)
	  {
	    cursor=cursor.getSuccessor();
	  }
	//switching pointers to proper nodes
        Node<E> cursor2=cursor;
	cursor.setData(element);
		
	

        return cursor2.getData();
      }

    /** public int size()
      *
      *returns size of array object
      *Parameters : none
      *Returns:     int size
      */

    public int size()
      {
        
        return this.size;
      
      }
  
  
/**Class Node
 *
 *Class creates node to aid in implementaiont of List interafce
 * 
 */

private static class Node<E>
  {

    private E data;
    private Node<E> next;
    /** public Node()
      *
      *mnode constrauctor initialize variables to null
      *Parameters : none
      *Returns:     
      */

    public Node()
      {
        this.data=null;
        this.next=null;
      }
    /** public Node()
      *
      *
      *Parameters : n(E element,Node<E> the SUccesor)
      *Returns:     
      */

    public Node(E theElement,Node<E> theSuccessor)
      {
        this.data=theElement;
	this.next=theSuccessor;
      }

    /** public Node<E> getSuccessopr
      *
      *gets next Node point of This node
      *Parameters : none
      *Returns:     Node<E>
      */

    public Node<E> getSuccessor()
      {
        return this.next;
      }

    /** public void setSuccessor
      *
      *sets Next node pointer
      *Parameters : none
      *Returns:     void
      */
    
    public void setSuccessor(Node<E> n)
      {
        this.next=n;
      }

    /** public E getSata()
      *
      *gets data of noe lement
      *Parameters : none
      *Returns:     E
      */

    public E getData()
      {
        return this.data;
      }

    /** public void setData(E t)
      *
      *sets data in Node Object
      *Parameters : E t
      *Returns:     void
      */

    public void setData(E t)
      {
        this.data=t;
      }
  }

/**Class Iterator
 *
 *creates iterator to be used in implemetning List12 iterator
 *  
 */

  private class Iterator implements java.util.Iterator<E>
    {
      private Node<E> pred;
      private Node<E> lastReturned;
      private Node<E> next;

      /** public void testContains()
        *
        *meathod tests List<E> implementation of contains
        *Parameters : none
        *Returns:     void
        */

      public Iterator()
        {
	  this.next=head.next;
	}

      /** public void testContains()
        *
        *meathod tests List<E> implementation of contains
        *Parameters : none
        *Returns:     void
        */

      public E next()
        {
	  if(next == null) throw new NoSuchElementException();
	  lastReturned = next;
	  return lastReturned.data;

	}

      /** public void testContains()
        *
        *meathod tests List<E> implementation of contains
        *Parameters : none
        *Returns:     void
        */

      public boolean hasNext()
        {
	  return next != null;
	}
      /** public void testContains()
        *
        *meathod tests List<E> implementation of contains
        *Parameters : none
        *Returns:     void
        */

      public void remove()
        {
	  if(lastReturned == null) throw new IllegalStateException();

	  lastReturned = null;
	}

    }



    public boolean addAll(Collection c) 
       throws UnsupportedOperationException
      {	
      return true;

      }
    public boolean addAll(int index,Collection c) 
        throws UnsupportedOperationException
      {
      return true;

      }
    public boolean containsAll(Collection c) 
     throws UnsupportedOperationException
      {
      return true;
 
      }
    public int lastIndexOf(Object o) 
         throws UnsupportedOperationException
      {   
      return 0;

      }
    public ListIterator listIterator() 
        throws UnsupportedOperationException
      {
      return null;

      }
    public ListIterator listIterator(int index) 
          throws UnsupportedOperationException
      {  
      return null;

      }
    public E remove(int index) 
      throws UnsupportedOperationException
      {     
      return null;

      }
    public boolean removeAll(Collection c) 
         throws UnsupportedOperationException
      {    
      return true;

      }
    public boolean retainAll(Collection c)  
        throws UnsupportedOperationException
      {  
      return true;

      }
    public List subList(int fromIndex, int toIndex) 
           throws UnsupportedOperationException
      {  
      return null;

      }
    public Object[] toArray() 
         throws UnsupportedOperationException
      { 
      return null;

      }
      
    public Object[] toArray(Object[]a) 
       throws UnsupportedOperationException
      {  
      return null;
      }
      


  }
