
/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 3
 * Date:       April 21,2011
 *
 * File: NumberGuessingGame.java
 *
 * Souces of Help: tutors in lab(Mike and Sumin)
 *
 * Java application allow user to play a guessing game with a random number
 */


import java.util.Random;
import java.util.Scanner;
import javax.swing.*;

/**
 * class NumberGuessingGame.java
 *
 * This class creates a random object for a user to guess against
 */


public class NumberGuessingGame
{

  /* main meathod()
  *
  *This meathod creates a random number for a user to input a range and guess
  *within the range until correct.The calculates 
  *average number of tries to guess correct
  *Parameters : String[]args
  *Returns:     void
  */

  public static void main(String[] args)
    {       
      //prompts user for input
      System.out.print("I am thinking of a number. Would you care to guess?");
      System.out.print("\nType any character for yes, EOF for no: ");

      //creates scanner object to be used in main
      Scanner scan=new Scanner(System.in);

      //declaring variable to use later in main
      int games=0;
      int triesTotal=0;    

      //loop to iterate while user whats to play games
      while(scan.hasNext())
        {
          //move scanner token
          scan.next(); 

          //declaring variables
          int minNumber;
          int maxNumber;
          
          //promp user for min number input
          String min=("\nEnter the minimum number for the game: ");
          //calling method to get integer from user input
          minNumber=getInt(scan,min);

          //prompt user for max input
          String max=("Enter the maximum number for the game: ");
          //calling meathod to get integer from user input
          maxNumber=getInt(scan,max);

          //if min is less than max print error message
          if(minNumber>=maxNumber)
            {
              System.out.println("Minimum value should be less than maximum"+
                "value.");
            }

          //create random number and call meathods for user to play game
          else
          {

            //counting games
            games++;

            //creates random object
            Random randNum=new Random();

            //creates range for random integer
            int guessNum=minNumber+randNum.nextInt(maxNumber-minNumber+1);

            //calls meathod to allow user to guess some numbers till correct
            int tries=guessNumber(scan,guessNum);

            //counting tries for this game
            triesTotal=triesTotal+tries;

          }
            
          //prompt user if they want to play again
          System.out.print("\nWould you like to play again? <any character"+
            "for yes EOF to quit>: ");
          
        }

      //if user plays no games print out the following
      if(games==0)
        {
          System.out.println("\nYou played a total of 0 games");       
        }
       
      //if user plays games pring out average
      else
        {
          calculateAvg(triesTotal,games);
        }


    }

 /* getInt( ,)
  *
  *This meathod takes user input until it in an integer
  *returns the unteger the user inputs
  *
  *Parameters : (Scanner console, String propmt)
  *Returns:     integer
  */

  public static int getInt(Scanner console,String prompt)
    {

      //prompt user
      System.out.print(prompt); 

      //check if user does not input an integer
      while(!console.hasNextInt())
        {
 
        //moves scanner token
        console.next();

        //error messgae if no integer input
        System.out.print("Not an integter. Try again: ");

        //reprompt
        System.out.print(prompt);
        }

      //returns proper user input
      return console.nextInt();

    }

  /* guessNumber( , )
   *
   *This meathod takes in a random number and compares to user guess
   *meathod reurn number of time user take to guess correct answer
   *
   *Parameters : Scanner console, int guessNum
   *Returns:     int
   */

  public static int guessNumber(Scanner console,int guessNum)
    {

      //initialses tries to at least one try
      int tries=1;

      //prompt user
      System.out.print("Guess the number: ");

      //calls meathod to allow user to guess a number
      int guess=getInt(console,"");

      //if guees does not match random number
      while(guess!=guessNum)
        {    
     
          //check if guess is larger than random
          if(guess>guessNum)
            {
             
              //prompts userif too high
              System.out.print("Too high. Try again: ");
              
              //calls meathod to let user guess again
              guess=getInt(console,"");
              
              //increment tries
              tries++;

            }

          //chekc is guess is lower than random number
          if(guess<guessNum)
            {
              //prompts user is too low
              System.out.print("Too low. Try again: ");
              
              //allows user to guess again
              guess=getInt(console,"");
              tries++;

            }
        }
      
      //print out when user gets it correct adn how many tries it took
      System.out.print("\nCongradualtions!");
      System.out.print("\nYou needed "+tries+" tries\n");

      return tries;
    }

 /* calculateAvg
  *
  *this take in tries and games and calculates how many ,on average,tries
  *the user took to guess correct and prints it out
  *
  *Parameters :  int tries, int games
  *Returns:     void
  */

  public static void calculateAvg(int tries,int games)
    {
      //calculates average
      float average=((float)tries/games);

      //prints out number of games played
      System.out.print("\nYou played a total of "+games+" games");

      //prints out average number to two decimal places
      System.out.printf("\nYour average number of guesses per game"+
        "is %.2f\n",average);
    }



}
