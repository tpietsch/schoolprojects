/*Name:       Trever Pietsch
 *Account:    cs8wiu
 *Assignment: HW1
 *Date:       4/7/11
 *
 *File Name:  Triangles.java
 *
 *Purpose: program draws inverted triangles using the asterisk '*' symbol
 *using a size inputed by user
*/
import java.util.Scanner;

public class Triangles
{
 
  /*
   * main() meathed is the driver
   *parameters: String[]args
   *return:  none
  */

  public static void main(String[]args)
  {
    //prompts user to input triangle size
    System.out.print("Enter the size of the triangles to display: ");
    Scanner input=new Scanner(System.in);
    
    //creates intiger from user input 
    int triangleSize=input.nextInt();
    
     //checks for correct input
     for(;triangleSize<2;)
      {
      
      //reprompts user for new input until within correct range
      System.out.println("Triangle size must be > 1; Try again.\n");
      System.out.print("Enter the size of the triangles to display: ");
      input=new Scanner(System.in);
      triangleSize=input.nextInt();

      }
     
      //this loop iterates over the rows to be printed
      for(int i=0;i<triangleSize;i++)
      {
        System.out.print("\n");
        
        //Draws triangle 1 '*'
        for(int j=triangleSize;j>=triangleSize-i;j--)
          {
        //    System.out.print('*');
          }
         
        //Draws triangle 1 spaces
        for(int j=0;j<triangleSize-i;j++)
          {
        //     System.out.print(' ');
          }
       
        //Draws triangle 2 '*'     
        for(int j=triangleSize-i;j>0;j--)
          {
            System.out.print(' ');
          }
        
        //Draws triangle 2 spaces
        for(int j=triangleSize-i;j<triangleSize;j++)
          {
            System.out.print('*');
          }

        //Draws triangle 3 spaces
        for(int j=triangleSize;j>=triangleSize-i;j--)
          {
            System.out.print('*');
          }
       
        //Draws triangle 3 '*'
        for(int j=0;j<triangleSize-i;j++)
          {
            System.out.print(' ');
          }
        
        //Draws triangle 4 spaces    
        for(int j=0;j<triangleSize-i;j++)
          {
        //    System.out.print(' ');
          }
       
         //Draws triangle 4 '*'
        for(int j=triangleSize;j>=triangleSize-i;j--)
          {
         //   System.out.print('*');
          }
       
        }
      //creates new line for next iteration
      System.out.print("\n");  
  } 
}

