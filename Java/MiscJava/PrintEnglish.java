/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 8
 * Date:       May 2,2011
 *
 * File: PrintEnglish.java
 *
 * Souces of Help: tutors in lab summin
 *
 * Java application that prints out the equivalent phrase
 * for a number using base number
 */




import java.util.*;

/**
 * class PrintEnglish.java
 *
 * This class allows user to print out number equivalent of base input
 */


public class PrintEnglish
{
/* main meathod()
  *
  *This meathod drives the program
  * initializes ser input variables
  *Parameters : String[]args
  *Returns:     void
  */


public static void main(String[]args)
{

//initializ emty strings for negative number case
String string=new String("");
String string2=new String("");

//scanner for user unput
Scanner scan=new Scanner(System.in);
//promting user
System.out.print("Please endter a number(EOF to quit): ");

//user inputs number
if(scan.hasNextInt())
{

//setting number to user input
//setting user input
int number=scan.nextInt();

//promting for base
System.out.print("Please enter a base for the number: ");
//setting base to user intut
int base=scan.nextInt();

//checking if base is in range
while(base<2||base>36)
{

//repromting user for nrew input
System.out.print("Base value needs to be between 2 and 36. Try again: ");
base=scan.nextInt();

}

//cheking for negative number
if(number<0)
{

//initialing negative number situation
string2="-";
string="minus ";
//changing number to positive
number=Math.abs(number);

}
//printing out tilte for informatio 
System.out.print(string2+""+number+" base "+base+" is "+string);
//caling meathod to preform recursive calculation
printEnglishNumToBase(number,base);
//new line
System.out.print("\n");
}
}


 /* printEnglishNumToBase(int number,int base)
  *
  *meathod takes in a number and base and prints out string equivalent
  *of the calulation using recursion
  *
  *Parameters : (int number,int base)
  *Returns:     void
  */

public static void printEnglishNumToBase(int number, int base)
{
//initializng data array for number to word equvalents
String[] array={"zero","one","two","three","four","five","six","seven","eight","nine"
,"A","B","C","D","E","F","G","H","I","J","K","L","M"
,"N","O","P","Q","R","S","T","U","V","W","X","Y","Z",};

//checking for recursive case
if(number>=base)
{


int remainder=number%base;
int division=number/base;

	//recursing
	printEnglishNumToBase(division,base);
//getting value from array to prin out
System.out.print(array[remainder]+" ");



}
//base case

else
{
         //base case means number is less htan base so print that
	System.out.print(array[number]+" ");
	
}




}


}
