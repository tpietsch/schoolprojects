/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 2
 * Date:       April 14,2011
 *
 * File: EnglishToPigLatin.java
 *
 * Souces of Help: tutors in lab(Mike and Sumin)
 *
 * Java application to convert any english word into pig latin
 */

import java.util.Scanner;

/**
 * class EnlgishToPigLatin.java
 *
 *	This class converts english words input by a user-into pig latin
 */

public class EnglishToPigLatin
{

 /* main meathod()
  *
  *This meathod asks user for input words and convertes the words
  *into pig latin
  *
  *Parameters : String[]args
  *Returns:     void
  */

  public static void main(String[]args)
    { 
      //initializez newWord to empty string
      String newWord="";
      //Pompst user for input and reads it
      System.out.println("Enter the text to be converted to"+
          " Pig Latin,EOF to quit: ");
      Scanner inputString= new Scanner(System.in);
      //loops through until no more input is present
      while(inputString.hasNextLine())
         {
            //creats a string out of entire input from scanner
            String str = inputString.nextLine();
            //calls meathod englishToPigLatin onto newWord
            newWord=englishToPigLatin(str);
            //prints out converted word sequence
            System.out.println("In Pig Latin, that is:\n"+newWord);
            //repompt user for more input or to quit
            System.out.println("Enter more text to be converted"+
		" to Pig Latin, EOF to quit:");
            

         }
             }
 /* englishToPigLatin(String)--class EnglishToPigLatin
  *
  *This meathod takes a word and converts it into pig latin
  *
  *
  *Parameters : String
  *Returns:     String
  */
  
  public static String englishToPigLatin(String s)
    {
      //creating empty string for new word
      String newWord="";
      //scanner to scan in each line
      Scanner scanned=new Scanner(s);
      
      //checking if the indivual line has a token
      while(scanned.hasNext())
        {

          //getting an individual string word from string sequence
          String sTring=scanned.next();

          //declaring and initializ all local variables
          String tempString=null;
          String part1=null;
          String part2=null;
          String getPart1=null;
          String getPart2=null;


          //looping through the letters of the string word
          for(int index=0;index < sTring.length();index++)
            {
         
              //getting character at index in string
              char getChar=sTring.charAt(index);
              //changin character to lower case so easier to compare to vowels
              getChar=Character.toLowerCase(getChar);
              //getting boolean value for index being Y or not
              boolean isLetterY=(getChar=='y'||getChar=='Y');

              //checking if first letter is vowel not including Y
              if(isVowel(getChar,(index==0&&!isLetterY)))
                {
                  //first letter is vowel add -ay and capitalize first letter
                  tempString=sTring;
                  char getCharPart2=tempString.charAt(0);
                  getPart2=Character.toString(getCharPart2);
                  getPart2=getPart2.toUpperCase();
                  tempString=(getPart2+tempString.substring
                    (1,tempString.length())+"-ay");
                  break;
                }
           
        
              //cheking for next letter to be vowel including y
              if(isVowel(getChar,index>0))
                {
                  //take apart string at vowel letter
                  part1=sTring.substring(0,index);
                  part2=sTring.substring(index,sTring.length());

                  //captialze first letter of first substring
                  char getCharPart2=part2.charAt(0);
                  getPart2=Character.toString(getCharPart2);
                  getPart2=getPart2.toUpperCase();
               
                

                  //lower case first letter ofsecond substring
                  char getCharPart1=part1.charAt(0);
                  part1=part1.substring(1,part1.length());
                  getPart1=Character.toString(getCharPart1);
                  getPart1=getPart1.toLowerCase();
                  part2=part2.substring(1,part2.length());

                  //puts together corrected two string to form pig latin word
                  tempString=(getPart2+part2+"-"+getPart1+part1+"ay");
       

                  break;
                }

              //if string contains no vowels then do this
              else
                {
             
                   //sets temp string to new string wiht now vowels
                   tempString=sTring;
                   char getCharPart2=tempString.charAt(0);
                   getPart2=Character.toString(getCharPart2);
                   //upper case first letter
                   getPart2=getPart2.toUpperCase();
                   tempString=(getPart2+tempString.substring
                        (1,tempString.length()));
               
                }



        }
      //new word plus next word to creat new string of pig latin translations
      newWord=(newWord+tempString+" ");

    } 
   
    return newWord;
    
  }

 /* isVowel(char,boolean)- class englishToPigLatin
  *
  *This meathod checks to see if the first letter of a word is a vowel
  *including cases of Y after the first letter
  *
  *Parameters : vowel, value
  *Returns:     Boolean
  */

  public static boolean isVowel(char vowel,boolean value)
    {
      if((vowel=='y'||vowel=='Y'||vowel=='a'||vowel=='e'||
           vowel=='i'||vowel=='o'||vowel=='u')&& value)
        {
          return true;
        }
            else
        {
        
          return false;
        }
    }
  }
