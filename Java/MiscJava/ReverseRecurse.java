
/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 8
 * Date:       June,2,2011
 *
 * File: ReverseRecurse.java
 *
 * Souces of Help: tutors in lab summin
 *
 * this program takes a series of numbers and reverses them
 */



import java.util.*;
/**
 * class ReverseRecurse.java
 *
 * this class allows users to create arrays of numbers and print them out in reverse order
 * using revcursive teequniques
 */


public class ReverseRecurse
  {
 /* public init[] intiArray() 
  *
  *this meathod promst user for input
  *initializes array size and contents
  *
  *Parameters : ()
  *Returns:     constructor
  */

   public int[] initArray()
      {
        //creting scanner for user input
        Scanner scan=new Scanner(System.in);
        //print out prommpt
        System.out.print("Maximum number of intigers wish to enter? ");
        //chekcing if input does nothave next
       if(!scan.hasNextInt())
              {
 		//exit
                System.exit(0);
              }
      //setting numer to input
        int maxNum=scan.nextInt();
     //if impoper input
       while(maxNum<=0)
       {
//repromting user
System.out.print("muste enter value >0:try again ");
maxNum=scan.nextInt();
}
        //setting size of aray
        int[] numArr=new int[maxNum];
                int i=0;
//prompting for filling array

System.out.print("Enter up to "+maxNum+" integets:");

    //filling array with user input
        while(i>numArr.length)
          {
             while(scan.hasNextInt())
              {

                if(i>numArr.length)
                  {
                    break;
                  }
	        numArr[i]=scan.nextInt();
	        i++;
              
          }

        }

for(i=0;i<numArr.length&&scan.hasNextInt();i++)
{
numArr[i]=scan.nextInt();

}
//cheking if array has empty indecies
if(i<numArr.length)
{
 int[] replace=new int[i];
//replacing array with new array with correct input
for(int j=0;j<replace.length;j++)
{

replace[j]=numArr[j];

}
return replace;

}

        return numArr;
      }

 /* printArray(int[]array)
  *
  *prints out values in array
  *
  *Parameters : (inti[]array)
  *Returns:     void
  */

    public void printArray(int[] array)
      {
//if array is emtpy
if(array.length==0)
{
System.out.print("Emtpy array\n");
}
//printing array that is passed in
else{
        for(int i=0;i<array.length;i++)
 {

        System.out.print(array[i]+" ");
}
System.out.print("\n"); 
}     
      
      }
 /* reverse
  *
  *reverses array based on where you start
  *high and low at
  *
  *Parameters : (inti[]originaArray,int low,int high)
  *Returns:     void
  */

    public void reverse(int[]originalArray,int low,int high)
      {
              
        //cheking if index overlap
        if(low<=high)
          {
              //swapping
              int temp=originalArray[low];
              originalArray[low]=originalArray[high];
               originalArray[high]=temp;
        
            //recursing
            reverse(originalArray,low+1,high-1);

          }
       
      }

 /* public int[]reverse(int[]originalArray)
  *
  *reverses array based on aray input using recursion of course
  *
  *
  *Parameters : (int []originalArray)
  *Returns:     int[] 
  */


    public int[] reverse(int []originalArray)
      { 
         //cheking if array lingth can be swapped,bas case
         if(originalArray.length<=1)
           {
              return originalArray;
           }
 //initializng arrays to be used
                  int[] reversedArray=new int[originalArray.length];
         int[] middle=new int[originalArray.length-2];
             //moveing to new array

             reversedArray[0]=originalArray[originalArray.length-1];
               reversedArray[reversedArray.length-1]=originalArray[0];
              //getting middle of parameter array
                System.arraycopy(originalArray,1,middle,0,middle.length);
                 //recursing overmiddle
                 System.arraycopy(reverse(middle),0,reversedArray,1,middle.length);
             //reutrning copy array
              return reversedArray;

        
       }



  }
