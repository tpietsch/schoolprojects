/*Name:       Trever Pietsch
 *Account:    cs8wiu
 *Assignment: HW1
 *Date:       4/7/11
 *
 *File Name:  TwoSmallest.java
 *
 *Purpose: gets two smallest integers from user input
 */

import java.util.Scanner;

public class TwoSmallest
{

  /*
   * main() meathed is the driver
   *parameters: String[]args
   *return:  none
  */

  public static void main(String[]args)
    {
    //promtps user to enter integer series
    System.out.print("Enter a series of integers; EOF to Quit. ");
    Scanner inputNumberSeries=new Scanner(System.in);
      
    //initializing and declaring values to be used
    int j=0;
    int smallestValue=0;
    int smallest2Value=0;
    int nextComparisonNumber;

    //loops until the no more intigers can be read from input
    while(inputNumberSeries.hasNextInt())
      {
        //getting next number from scanner
        nextComparisonNumber = inputNumberSeries.nextInt();
        //setting first token to be smallest value
	if(j==0)
	  {
	    smallestValue=nextComparisonNumber;
            j++;
    	  }
        //checking if more than one token exists
        if(inputNumberSeries.hasNextInt())
          {
            //setting second smallest value to second token(for first loop)
            smallest2Value=nextComparisonNumber;
            j++;
            //if second value is greater than first switch them
            if(smallest2Value<smallestValue)
              {
	        int k=smallestValue;
                smallestValue=smallest2Value;
                smallest2Value=k;
              }
            }
          //using j as a counter if tokens are greater than 1 do this          
          if (j>1)
            { 
              //if next token is less then second switch them
              if (nextComparisonNumber<=smallest2Value)
                {
                if(!(nextComparisonNumber==smallestValue))
                  {
                    smallest2Value=nextComparisonNumber;                                      
                  }
                }
              //check again if new second smalles is smaoler than first
              if(smallest2Value<smallestValue)
		{
	          int k=smallestValue;
                  smallestValue=smallest2Value;
                  smallest2Value=k;
                }
             }
      }
    //checks how many tokens if greater than zero to print correct phrase
    if(j>1&&smallestValue!=smallest2Value)
      {
        System.out.print("\nSmallest distinc number entered was "
  	+smallestValue+"");
        
        System.out.print("\nSecond smallest distinc number"
        +"entered was "+smallest2Value+"\n");
      }
    else 
      {
        System.out.print("\nSmallest distinc number entered was "
        +smallestValue+"\n");
      }
    //if no entries are made no numbers are printed
    if(j==0)   
     {
       System.out.print("\nNo numbers entered.\n");
     }
  }
}
