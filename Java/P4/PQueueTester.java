/**Name:      @author Trever Pietsch
 *Account:   cs12ubo
 *Studen ID: A09150869
 *HW:        Programming Assignment 4
 *Date:      7/22/2011
 */

import java.util.*;
import javax.swing.*;

/**
 * Heap12 class
 *
 *this class test proper implementation of heap12
 *
 */



public class PQueueTester extends junit.framework.TestCase
  {
    public static void main(String[]args)
      {
        junit.swingui.TestRunner.main(new String[]{"PQueueTester"});
      }	 
    
  /**
   * Determine if this heap12 contains the same elemnts as Object o.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the Heap12 is unchanged.
   * @return <tt>true</tt> if this Heap12 is equal to the 
   * pramaeter, <tt>false</tt> otherwise.
   */

    public void testEquals()
      {
        
        Heap12<Integer> heap1=new Heap12<Integer>();
	Heap12<Integer> heap2=new Heap12<Integer>();
	
	assertTrue(heap1.equals(heap2));

	heap1.add(new Integer(9));
	heap2.add(new Integer(9));

	assertTrue(heap1.equals(heap2));
        
	heap1.add(new Integer(10));

	assertFalse(heap1.equals(heap2));

	heap1.remove();

	assertTrue(heap1.equals(heap2));

	
	      
      }

  /**
   * Adds the specified element to this Heap12.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the element has been added to this Heap12,
   * none of the other elements have been changed,
   * and the size is increased by 1.
   * @throws NullPointerException if the specified element is <tt>null</tt>.
   */


    public void testAdd()
      {
        Heap12<Integer> heap1=new Heap12<Integer>();
	Heap12<Integer> heap2=new Heap12<Integer>();

	for(int i=1;i < 1000;i++)
	  {
            heap1.add(new Integer(i%(1000000)));
	    heap2.add(new Integer(i%(1000000)));



	    heap1.add(new Integer(i%(843/i)));
	    heap1.add(new Integer(i%(843/i)));


	    heap2.add(new Integer(i%(843/i)));
	    heap2.add(new Integer(i%(843/i)));


	  }

	assertTrue(heap1.equals(heap2));
	assertTrue(heap1.peek().equals(1000000));

	try
	  {
	    heap2.add(null);
	    fail("Does not throw null pointer exception");
	  }
	catch(NullPointerException e)
	  {
	  }

      }

  /**
   * Removes and returns the highest priority element in this Heap12.
   * If more than one element has the same highest priority, one
   * of them is removed and returned.
   * <br>PRECONDITION: this Heap12 contains at least one element.
   * <br>POSTCONDITION: the highest priority element has been removed from
   * this Heap12, none of the other elements have been changed,
   * and the size is decreased by 1.
   * @return The highest priority element in this Heap12, or <tt>null</tt> if none.
   */

    public void testRemove()
      {
        Heap12<Integer> heap1=new Heap12<Integer>();
	Heap12<Integer> heap2=new Heap12<Integer>();
    
        heap2.add(new Integer(4));
	heap2.add(new Integer(5));
	heap2.add(new Integer(6));
	heap2.add(new Integer(1));
	heap2.add(new Integer(2));
	heap2.add(new Integer(3));
	heap2.add(new Integer(0));
	heap2.add(new Integer(0));
	heap2.add(new Integer(600));
        



	assertTrue(heap2.remove().equals(600));
	assertTrue(heap2.remove().equals(6));
	assertTrue(heap2.remove().equals(5));
	assertTrue(heap2.remove().equals(4));
	assertTrue(heap2.remove().equals(3));
	assertTrue(heap2.remove().equals(2));
	assertTrue(heap2.remove().equals(1));
	assertTrue(heap2.remove().equals(0));
	assertTrue(heap2.remove().equals(0));

	assertTrue(heap2.remove()== (null));

	assertTrue(heap1.remove() == null);


      }
  /**
   * Returns the element in this Heap12 that would be returned by <tt>remove()</tt>.
   * <br>PRECONDITION: this Heap12 contains at least one element.
   * <br>POSTCONDITION: the Heap12 is unchanged.
   * @return The highest priority element in this Heap12, or <tt>null</tt> if none.
   * @see #remove()
   */

    public void testPeek()
      {
        Heap12<Integer> heap1=new Heap12<Integer>();
	Heap12<Integer> heap2=new Heap12<Integer>();

	heap2.add(new Integer(4));
	heap2.add(new Integer(5));
	heap2.add(new Integer(6));
	heap2.add(new Integer(1));
	heap2.add(new Integer(2));
	heap2.add(new Integer(3));
	heap2.add(new Integer(0));
	heap2.add(new Integer(0));
	heap2.add(new Integer(600));
        



	assertTrue(heap2.peek().equals(600));
	assertTrue(heap2.remove().equals(600));

	
	assertTrue(heap2.peek().equals(6));
	assertTrue(heap2.remove().equals(6));

	assertTrue(heap2.peek().equals(5));
	assertFalse(heap2.remove().equals(6));

	assertTrue(heap2.peek().equals(4));
	assertFalse(heap2.remove().equals(600));

	assertTrue(heap2.peek().equals(3));
	assertFalse(heap2.peek().equals(2));
	 
	heap2.add(new Integer(4000));

	assertTrue(heap2.peek().equals(4000));

	assertTrue(heap1.peek() == null);


      }
     
  /**
   * Determine if this heap12 contains any elements.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the Heap12 is unchanged.
   * @return <tt>true</tt> if this Heap12 is empty, <tt>false</tt> otherwise.
   */

     public void testIsEmpty()
      {

        Heap12<Integer> heap1=new Heap12<Integer>();
	Heap12<Integer> heap2=new Heap12<Integer>();

	assertTrue(heap1.isEmpty());
	heap1.add(new Integer(9));

	assertFalse(heap1.isEmpty());

	heap1.remove();

	assertTrue(heap1.isEmpty());

      }

  /**
   * Returns the number of elements in this Heap12.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the Heap12 is unchanged.
   * @return the number of elements in this Heap12.
   */

    public void testSize()
      {
        Heap12<Integer> heap1=new Heap12<Integer>();
	Heap12<Integer> heap2=new Heap12<Integer>();

	assertTrue(heap1.size() == 0);

	heap1.add(new Integer(88));

	assertTrue(heap1.size() == 1);

	heap1.remove();

	assertTrue(heap1.size() == 0 );

	for(int i=0 ; i < 500 ; i++)
	  {
	    heap1.add(new Integer( i ));
	  }
	
	assertTrue(heap1.size() == 500);

      }


  /**
   * Sorts a passed array fron smallest to largest.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the E[] a is sorted from smallest to largets
   * @param a E[] array
   */

    public void testSort()
      {

        //testing sort with known array contents

        Integer[] a1 = new Integer[6];
	Integer[] a2 = new Integer[6];

	for(int i=0; i<6 ; i++)
	  {
	    a1[i]=i;
	  }

        a2[0]= 3;
	a2[1]= 5;
        a2[2]= 1;
        a2[3]= 0;
	a2[4]= 2;
	a2[5]= 4;

	Heap12.sort(a2);

	for(int i=0; i<6;i++)
	  {
	    assertTrue(a1[i].equals(a2[i]));
	  }

	a2[3]=9;

	Heap12.sort(a2);

	assertFalse(a2[5].equals(a1[5]));

        //sorting with random

	Random r=new Random();
	 
	Integer[] a3=new Integer[1000];

	for(int i=0; i< 1000; i++)
	  {
            a3[i]=r.nextInt(1000);
           }

	Heap12.sort(a3);

	for(int i=1; i<(1000) ; i++)
	  {
	    System.out.println(a3[i-1]+"  "+a3[i]);
	    assertTrue(a3[i-1].compareTo(a3[i])<=0);
	  }


      }

  }
