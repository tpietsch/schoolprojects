/**Name:      @author Trever Pietsch
 *Account:   cs12ubo
 *Studen ID: A09150869
 *HW:        Programming Assignment 4
 *Date:      7/22/2011
 */



/**
 * A <tt>Heap12</tt> is a priority queue.
 *
 * this class creates a Heap12 object using ca omplete
 * binary sturture 
 */


public class Heap12<E extends Comparable<? super E>> implements PQueue<E>
  { 
    //declaring class variable
    private Comparable[] heap;
    private int size;

    //this number is a buffer to prevent resizing too many times
    private final static int number = 10 ;
    

    //default constructor to initialize heap array length
    public Heap12()
      {
        heap=new Comparable[5];
      }

    //construtor to assist in sort meathod
    private Heap12(Comparable[] array)
      {
        heap=array;
      }

  /**
   * Adds the specified element to this Heap12.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the element has been added to this Heap12,
   * none of the other elements have been changed,
   * and the size is increased by 1.
   * @param e The element to add.
   * @throws NullPointerException if the specified element is <tt>null</tt>.
   */

    public void add(E e)
      {
        if(e == null)
	  {
	    throw new NullPointerException();
	  }

        //resizing array
	if((this.size) >= heap.length)
	  {
	    Comparable[] temp = new Comparable[(heap.length*2)];
            //copy originalarray to resized array
	    for(int i = 0; i < (this.size); i++)
	      {
	        temp[i]=this.heap[i];	        
	      }

	    heap=temp;

	    temp=null;
	  }
	//adding element to array
        heap[size]=e;
        //incrementing size
	size++;
        
	//sorting heap
	bubbleUp(size-1);

      }
    
    
  /**
   * Removes and returns the highest priority element in this Heap12.
   * If more than one element has the same highest priority, one
   * of them is removed and returned.
   * <br>PRECONDITION: this Heap12 contains at least one element.
   * <br>POSTCONDITION: the highest priority element has been removed from
   * this Heap12, none of the other elements have been changed,
   * and the size is decreased by 1.
   * @return The highest priority element in this Heap12, or
   * <tt>null</tt> if none.
   */

    public E remove()
      {
        if(size == 0)
	  {
	    return null;
	  }
	  
	//return variable
	Comparable tmp = heap[0];

        heap[0]=heap[size-1];

	heap[size-1]=null;
     
        //decremenitng size
	size--;


        //resizing array if too small
	if(size < (heap.length)/2)
	  {
	    Comparable[] temp = new Comparable[((heap.length)+number)/2];

            //copying orig array to smaller new array
	    for(int i = 0; i < (this.size); i++)
	      {
	        temp[i]=this.heap[i];	        
	      }

	    heap=temp;

            temp=null;
	  }


	trickleDown(0);
	
        return (E)tmp;
      }


  /**
   * Returns the element in this Heap12 that would be 
   * returned by <tt>remove()</tt>.
   * <br>PRECONDITION: this Heap12 contains at least one element.
   * <br>POSTCONDITION: the Heap12 is unchanged.
   * @return The highest priority element in this Heap12, or 
   * <tt>null</tt> if none.
   * @see #remove()
   */

    public E peek()
      {
        return (E)heap[0];
      }
    

  /**
   * Returns the number of elements in this Heap12.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the Heap12 is unchanged.
   * @return the number of elements in this Heap12.
   */

    public int size()
      {
        return this.size;
      }

  /**
   * Determine if this heap12 contains any elements.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the Heap12 is unchanged.
   * @return <tt>true</tt> if this Heap12 is empty, <tt>false</tt> otherwise.
   */

    public boolean isEmpty()
      {
        if(this.size == 0)
	  {
	    return true;
	  }

	else
	  {
	    return false;
	  }

   
      }

      
  /**
   * Determine if this heap12 contains the same elemnts as Object o.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the Heap12 is unchanged.
   * @param o the Object to be compared
   * @return <tt>true</tt> if this Heap12 is equal to the 
   * pramaeter, <tt>false</tt> otherwise.
   */

    public boolean equals(Object o)
      {
         //cheking if can cast objects
        if(!( o instanceof Heap12))
	  {
	    
	    return false;
	  }
        //casting object to be compared
	Heap12 temp=(Heap12)o;

        //size must be equals
	if(temp.size != this.size)
	  {
	    
	    return false;
	  }

	int theSmallerSize;
        
	//checking to see how long to iterator loop for

	if(this.size < temp.size)
	  {
	    theSmallerSize=this.size;
	  }
	else
	  {
	    theSmallerSize=temp.size;
	  }

	//cheking all elemenst of array for equaliity
        for(int i=0; i < theSmallerSize ;i++)
	  {

            if(this.heap[i].compareTo(temp.heap[i]) != 0)
	      {
	         return false;
 	      }

	  }

        return true;
      }


  /**
   * Sorts a passed array fron smallest to largest.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the E[] a is sorted from smallest to largets
   * @param a E[] array
   */
    public static <E extends Comparable<? super E>> void sort(E[] a)
      {
        Heap12<E> sortHeap=new Heap12<E>(a);


        //adding elemnts to heap
	for(int i=0;i< a.length;i++)
	  {
	    sortHeap.add(a[i]);
	  }

        //adding removed Heap elements to back or array
	for(int i=a.length-1;i>=0;i--)
	  {
	    a[i]=sortHeap.remove();
	  }

      }

  /**
   * Determine if this heap12 contains the same elemnts as Object o.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the Heap12 parents are all greater tahn children
   * @param i int index
   */


    private void trickleDown(int i)
      {

        int left = 2*i+1;
	int right = 2*i+2;

        //cheking for any children
	if(left < size)
          {
       
            //cheking for both children
            if(size > right)
	      {
                     
	      
		  //cheking if parent is less then both children
	          if((heap[i].compareTo(heap[left]) < 0) && 
		  	(heap[i].compareTo(heap[right]) < 0))
	            {
	              Comparable tempL = heap[left];
		      Comparable tempR = heap[right];
                    
		    //cheking which child is larger and replacing
                    if(tempL.compareTo(tempR)>0)
      		      {
		        //replacing left child with parent

		        Comparable temp=heap[i];

		        heap[i]=heap[left];

		        heap[left]=temp;

		        trickleDown(left);

		      }

		    else
		      {
		        //replasing right child with parent
		        Comparable temp=heap[i];

		        heap[i]=heap[right];

		        heap[right]=temp;

		        trickleDown(right);
		      }

         
	           }

		 if(heap[i].compareTo(heap[right]) < 0)
	          {
		    //replaceing parnet child
	            Comparable temp=heap[i];

		    heap[i]=heap[right];

		    heap[right]=temp;

		    trickleDown(right);
	          }


	      }

	        //cheking if left child is less than
	       if(heap[i].compareTo(heap[left]) < 0)
	          {
		    //replaceing parnet child
	            Comparable temp=heap[i];

		    heap[i]=heap[left];

		    heap[left]=temp;

		    trickleDown(left);
	          }

	    }

	//base case no children  	
	else
	  {
	    return;
	  }
        

      }

  /**
   * Bubbels up children if they are greater than there parnets.
   * <br>PRECONDITION: none
   * <br>POSTCONDITION: the Heap12 parents are all greater tahn children.
   * @param o the Object to be compared
   */

    private void bubbleUp(int i)
      {
        if(i > 0)
	  {
	    //recrsive part cheking if child should replace parent
	    if(heap[i].compareTo(heap[(i-1)/2])>0)
	      {
	        Comparable temp=heap[i];

		heap[i]=heap[(i-1)/2];

		heap[(i-1)/2]=temp;

		bubbleUp((i-1)/2);
	      }
	    //base case for array of size greater than 0
	    else
	      {
	        return;
	      }
	  }
	//base Case for 0 array elemnts
	else
	  {
	    return;
	  }

      }

  }
