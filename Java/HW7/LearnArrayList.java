/*Name:       Trever Pietsch
 *Account:    cs8wiu
 *Assignment: HW7
 *Date:       5/26/11
 *
 *File Name:  LearnArrayList.java
 *
 *Purpose: The purpose of this file is to alllow 
 *user to input fill and choose how
 *to manpulate the strings in side the file
 */

import java.util.*;
import java.io.*;

/**
 * class LearnArrayList.java
 *
 * this class allows user to choose
 * from a set of options how
 * to manipulate a series of strings in a file
 */

public class LearnArrayList
  {
  /*
   * main() meathod is the driver
   *parameters: String[]args
   *return:  none
   */

    public static void main(String[]args)throws FileNotFoundException
      {
        //prompts user for entry
        System.out.print("Would you like to play a game(EOF to quit)?");

        //creates scanner for user input
        Scanner scan=new Scanner(System.in);

        //loops while ser has input
        while(scan.hasNext())
          {
            //prompts user for filename
            System.out.print("Enter the filename(include extension)for data:");

            //moves scanner token
            scan.next();

            //creates string of user input
            String fileName=scan.next();

            //calls print menue to get user choice of array manipualrtion
            int choice=printMenu(scan);

            //creats array list to pass into meathods
            ArrayList<String> list=new ArrayList<String>();

            //scans file
            Scanner scanFile=new Scanner(new File(fileName));

            int n=0;
            //loops while fiel has next
            while(scanFile.hasNext())
              {
                //addint objects in file to list
                list.add(scanFile.next());
              }
            //switch to call meathods from user selection
            switch(choice)
              {
                case 1:
                System.out.print("\n"+removeShorterStrings(list)+"\n");
                break;
                case 2:
                System.out.print("\n"+removeInRange(list)+"\n");
                break;
		case 3:
		System.out.print("\n"+interLeave(list)+"\n");
		break;
		case 4:
		System.out.print("\n"+reverseN(list,n)+"\n");
		break;
		case 5:
		System.out.print(""+reverseFile(fileName)+"");
		break;
		default:
		System.out.print("Not a valid selection\n");
		break;
	      }
            //promts if user wants to play again
            System.out.print("Continue(EOF to quit)?");
          }
        }

  /* printMenu
   *
   *this meathods prompts user for selection of data manipulation
   *
   *
   *Parameters : (Scanner scan)
   *Returns:     int
   */

  public static int printMenu(Scanner scan)
    {
      //printing out options for user
      System.out.print("\nWould you like to:\n");
      System.out.print(" 1.Remove shorter strings from each contiguous pair of words\n");
      System.out.print(" 2.Remove words in certain range\n");
      System.out.print(" 3.Interleave words from two files\n");
      System.out.print(" 4.Reverse each set of n contigous words\n");
      System.out.print(" 5.Reverse contents of a file(line by line & words per line)\n");
      System.out.print("Please make a selection:");

      //gets next user input if int
      int console=scan.nextInt();

      //returns user input
      return console;
    }

 /* removeShorterStrings
  *
  *This meathod removes shoter string of two sequential strings
  *
  *Parameters : (ArrayList<String> list)
  *Returns:     ArrayList<String>()
  */

  public static ArrayList<String> removeShorterStrings(ArrayList<String> list)
    {
      //cheking if size is even
      if(list.size()%2!=0)
        {
          //add emtpy string to make line even
          list.add("");
        }
      //looping through size 
      for(int i=0;i<list.size();i++)
        {
          //setting int valueto be length of element in array
          int one=(list.get(i)).length();
          int two=(list.get(i+1)).length();

          //compare lenth of array elemts remove smaller one
          if(one<two||one==two)
            {
              list.remove(i);
            }
          else if(one>two)
            {
              list.remove(i+1);
            }
        }
      //return altered list
      return list;
    }
  /* removeInRange
   *
   *removes strings lexicographily in range of user input
   *
   *
   *Parameters : (ArrayList<String> list)
   *Returns:     ArrayList<String> ()
   */

  public static ArrayList<String> removeInRange(ArrayList<String> list)
    {
      //prompting user for intput
      System.out.print("Enter begginning string:");

      //creating scanner for user input
      Scanner scan=new Scanner(System.in);

      //setting start word to first user input
      String startWord=scan.next();

      //promting user for second input
      System.out.print("Enter ending string:");

      //setting edn word to second word input
      String endWord=scan.next();

      //getting first letter of strings
      String start=(startWord.substring(0,1));
      String end=(endWord.substring(0,1));

      //creating an array lsit to place words in range in
      ArrayList<String> listTwo=new ArrayList<String>();

      //for length of array parameter compare all elemints
      for(int i=0;i<list.size();i++)
        {

          String subArray=(list.get(i)).substring(0,1);
          //comparing first letter of words and objects in array
          if((subArray.compareTo(start)<=0||subArray.compareTo(end)>=0))
            {

              listTwo.add(list.get(i));

            }
        }
      //returning new list
      return listTwo;
    }

  /* interLeave
   *
   *This meathod takes two array lists and merges them together
   *
   *Parameters : (ArrayList<String> list)
   *Returns:     ArrayList<String>()
   */

  public static ArrayList<String> interLeave(ArrayList<String> list)
    throws FileNotFoundException
    {
      //creating scnner to put merging file in
      Scanner scan=new Scanner(System.in);

      //promting user for input
      System.out.print("Enter the second filename(include extension)to interleave data:");

      //creating strign of user input
      String fileName=scan.next();

      //creating array lsit for new file 
      ArrayList<String> listTwo=new ArrayList<String>();
      //array lsit for mergin two list togther
      ArrayList<String> newList=new ArrayList<String>();

      //scanner to scan throug file
      Scanner scanFile=new Scanner(new File(fileName));
      int count=0;

      //while scanner has another token place int array lsit
      while(scanFile.hasNext())
        {
          listTwo.add(scanFile.next());
        }

      //comparting sizes of array
      if(listTwo.size()>=list.size())
        {

          //if litTwo is > list then the leftoever will just be added onto the end
          for(int i=0;i<list.size()*2;i=i+2)
            {

              //mergin lists topgether
              newList.add(i,list.get(count));
	      newList.add(i+1,listTwo.get(count));
              //counting merges
	      count++;

 	    }
          
          for(int i=count;i<listTwo.size();i++)
            {

              //adding leftover to end of list
              newList.add(i+list.size(),listTwo.get(i));

            }
        }

      //if list>listTwo merge list and leftoever of list to end
      else
        {

          for(int i=0;i<listTwo.size()*2;i=i+2)
            {

              //merging lsits
              newList.add(i,listTwo.get(count));
	      newList.add(i+1,list.get(count));
              count++;

            }

          for(int i=count;i<list.size();i++)
            {

              //adding left oever to edn of list
              newList.add(listTwo.size()+i,list.get(i));

            }
        }

    //returning new megerd list
    return newList;

  }
  /* reverseN
   *
   *This meathod interchnages N elements in the array parameter
   *
   *Parameters : (ArrayList<String> list,int n)
   *Returns:     ArrayList<String>()
   */
  public static ArrayList<String> reverseN(ArrayList<String> list,int n)
    {
 
      //creatings scanner for n value
      Scanner scan=new Scanner(System.in);

      //promtps user for n calue
      System.out.print("Enter a number to reverse each successive sequence of values:");

      //setting n to user input
      n=scan.nextInt();

      //if n<1 is the same as n=1
      if(n<1)
        {
          n=1;
        }

      //initializnr arays list to move objects to
      ArrayList<String> listTwo=new ArrayList<String>();

      //cheking for extra elemst int array to not be changed
      int remainder=list.size()%n;

      //for size-leftover loop
      for(int i=1; i<=list.size()-remainder;i=i+n)
        {

          //loopint through n to interchange n words
          for(int j=n-1;j>=0;j--)
            {

              //add to list 
              listTwo.add(list.get(i+j-1));

            }
        }
      //isif there is extra in list
      if(list.size()%n!=0)
        {
          //loop placing extra in order at end of list
          for(int i=list.size()-remainder;i<list.size();i++)
            {

              listTwo.add(list.get(i));

            }
        }
    //returns new list
    return listTwo;
  }

  /* reverseFile
   *
   *This meathod makes words in a file read backwards
   *
   *Parameters : (String fileName)
   *Returns:     String
   */
  public static String reverseFile(String fileName) throws FileNotFoundException
    {
      //print new line
      System.out.print("\n");

      //new scanner to scan file
      Scanner scan=new Scanner(new File(fileName));

      //initializing empty string
      String string="";

      //creating list of list of strings
      ArrayList<ArrayList<String>> list=new ArrayList<ArrayList<String>>();

      //while file has next line
      while(scan.hasNextLine())
        {
          //creat array list of strings
          ArrayList<String> listTwo=new ArrayList<String>();

          //scanner scanning thourhg words in line
          Scanner words=new Scanner(scan.nextLine());

          //incremnt counter
          int i=0;
          //if line has more words
          while(words.hasNext())
            {

              //add last words to begining of line
              listTwo.add(0,words.next());

            }
          //add line beofre first line
          list.add(0,listTwo);

          //if list has empyt line remove it
          if((list.get(i)).size()==0)
            {

              //remove moved elemnt
              list.remove(i);

            }
          //increment counter
          i++;
        }
      //loop to add to string
      for(int i=0;i<list.size();i++)
        {

          //creating array of reversed lines from file
          ArrayList<String>listTwo=list.get(i);

          //adding lines in reverse order
          for(int j=0;j<listTwo.size();j++)

            {
              string=string+listTwo.get(j)+" ";
            }

          //adding new line to string
          string=string+"\n";

        }
      //returning string
      return string;
    }
}
