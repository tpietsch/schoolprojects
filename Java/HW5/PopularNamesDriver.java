/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 5
 * Date:       May 19,2011
 *
 * File: PopularNamesDriver.java
 *
 * Souces of Help: tutors in lab
 *
 * Java application allow user to get the statistcs of a baby-name used
 *ovewr the last 130 years
 */

import java.util.*;
import java.io.*;
import java.awt.*;
/**
 * class PopularNamesDriver.java
 *
 * This class compares a user name input to historical name data
 * and prints the statistcs of the input name along whith
 * graph
 */

public class PopularNamesDriver
{
    //declaring class variables
    public final static int MARGIN=100;
    public final static int START_YEAR=1880;
    public final static int END_YEAR=2009;
    //public final static int YEAR_DIFF=1;
    public final static int LEGEND_OFFSET=100;
    /* main(String[]args)
     *
     *This meathod drives the program by calling relavent
     *classes,meathods,constructors,and initializing user
     *input
     *Parameters : (String[]args)
     *Returns:     void
     */

    public static void main(String[]args) 
      //thros exception
      throws FileNotFoundException
      {
        
        String name=null;
        //creating name object
        PopularName nameIn=new PopularName();
        //creates scanner object
        Scanner scan=new Scanner(System.in);
        //promps user
        System.out.print("Would you like to view statistics"+
	  " of a name (EOF to quit)? ");
        //loops while user inputs	
        while(scan.hasNext())
          {
            //moves scanner
            scan.next();
            //prompt for name input
            System.out.print("\nPlease enter the baby name for"+
	      " which data is to be displayed:");
            //creates string from scanner input
            name=new String(scan.next());
            //prompts for getInt meathod
            String promptStart="Enter start year of birth:";
            String promptEnd="Enter end year of birth:";
            //calling getInt to get start year
            int  startYear=getInt(scan,promptStart);
            //loops while invalid input
            while(startYear<START_YEAR||startYear>END_YEAR)
              {
                System.out.println("Start year should be between "
		  +START_YEAR+" and "+END_YEAR+"(inclusive)");
                startYear=getInt(scan,promptStart);
              }
            //calling getInt for end year
            int endYear=getInt(scan,promptEnd);
            //loops while invalid input
            while(endYear>END_YEAR||endYear<START_YEAR||endYear<startYear)
              {
                System.out.println("End year should be between "
		  +startYear+" and "+END_YEAR+"(inclusive)");
                endYear=getInt(scan,promptEnd);
              }
            //creating file array
            File[] files=new File[((endYear-startYear))+1];
            //loop to place files in array
            for(int i=0;i<files.length;i++)
              {
                //creating file object
                File file=new File("yob"+(i+startYear)+".txt");
                //placing file object in array
                files[i]=(file);
              }
            //creating name object to initialize object variables
            PopularName nameInput=new PopularName(name,files,
              startYear,endYear);
            //calls meathod to print out info
            nameInput.printStatistics(name,files);
            int width=550;
            //if year span is greater than default width increase it
            if(((endYear-startYear)*15)+100>550)
              {
                width=(endYear-startYear)*15;
              }
            //creating window
            DrawingPanel panel=new DrawingPanel(width+2*MARGIN,550+2*MARGIN);
            //creating graphics object to draw on panel
            Graphics graph=panel.getGraphics();
            //getting name from object
	    String currentName=nameInput.getName();
            //creating header for graph
	    graph.drawString("Displaying graph for "+currentName,84,29);
	    graph.drawString("Years "+startYear+" to "+endYear,84,53);
            int year=0;
            //creating graph legend and template
            for(int i=0;i<=500;)
              {
                graph.setColor(Color.black);
                graph.drawString("Rank "+year,0,i+LEGEND_OFFSET);
                graph.drawLine(0,i+LEGEND_OFFSET,
		  LEGEND_OFFSET+(2*width),i+LEGEND_OFFSET);
                i=i+50;
                year=year+100;
              }
            //calling meathods to plot and draw graph
            nameInput.plotFemaleGraph(graph);
            nameInput.plotMaleGraph(graph); 
            nameInput.drawMaleGraph(graph);
            nameInput.drawFemaleGraph(graph);
            //reprompt user
	    System.out.print("\nContinue (EOF to quit)?");
        }
      }
    /* getInt(Scanner scan,String prompt
     *
     *makes sure proper integer input is recieved
     *Parameters : (Scanner console,String prompt)
     *Returns:     int
     */

    public static int getInt(Scanner console,String prompt)
      {
        //prompt user
        System.out.print(prompt); 
        //check if user does not input an integer
        while(!console.hasNextInt())
          {
            //moves scanner token
            console.next();
            //error messgae if no integer input
            System.out.print("Not an integter. Try again:\n");
            //reprompt
            System.out.print(prompt);
          }
        //returns proper user input
        return console.nextInt();
      }
    }
