/*
 * Name:       Trever Pietsch
 * Account:    cs8wiu
 * Student ID: A09150869
 * HW:         Homework 5
 * Date:       May 19,2011
 *
 * File: PopularName.java
 *
 * Souces of Help: tutors in lab
 *
 * Java application allow user to get the statistcs of a baby-name used
 *ovewr the last 130 years
 */
import java.awt.*;
import java.util.*;
import java.io.*;
/**
 * class PopularName.java
 *
 * This class compares a user name input to historical name data
 * and prints the statistcs of the input name
 */
public class PopularName
  {
    
    //initializing and declaring class variables
    private String name;
    private  int START_YEAR;
    private int END_YEAR;
    private  int[]FEMALE_RANK;
    private int[]MALE_RANK;
    private int MARGIN=100;
    /* 
     *PopularName()
     *
     *constructor initialses name to empty string
     *
     *Parameters : 
     *Returns:     
     */

    public PopularName()
      {
        this.name=""; 
      }
    /* PopularName
     *
     *This constructor initialzes object variables
     *
     *
     *Parameters : (String name,File[]yobFiles,int startYear,int endYear)
     *Returns:     
     */

    public PopularName(String name1,File[]yobFiles,int startYear,int endYear)
      throws FileNotFoundException
      {
        //setting this objects class variable to user input start
        this.START_YEAR=startYear;
        //setting this object class END_YEAR to user endYear
        this.END_YEAR=endYear;
        //setting object name to user input name
        this.name=name1;
        //creating file array with yobFile length for male and female
        FEMALE_RANK=new int[yobFiles.length];
        MALE_RANK=new int[yobFiles.length];
        //calls meathod to fill class arrays with relavent ranks
        populateRankArrays(name,START_YEAR,END_YEAR,
          yobFiles,FEMALE_RANK,MALE_RANK);
      }
    /* String getName()
     *
     *This meathod returns name of object
     *
     *
     *Parameters : 
     *Returns:     
     */

    public String getName()
      {
        return this.name;
      }
    /* printStatistics(,)
     *
     *This meathod takes a file of arrays and compars the user input
     *and prints out relevant statistcs accosiated with user input
     *
     *Parameters : (String name, File[] files)
     *Returns:     void
     */
    public void printStatistics(String name,File[] files)
      throws FileNotFoundException
      {
        //print out headline for output
        System.out.println("\nStatistics for the baby name '"+name+
          "' from "+START_YEAR+" to "+END_YEAR+":");
        System.out.print("Female      Male\n");
        System.out.printf("%s%6s%6s%6s\n","Year","Rank","Year","Rank");
        //loop through file array
        for(int i=0;i<files.length;i++)
          {
            //creates new scanner object
            Scanner scan=new Scanner(files[i]); 
            //initializing all local variable
            int year=i+START_YEAR;
            String numberMale=null;
            String numberFemale=null;
            String gender=null;
            String fileName=null;
            int femaleRank=1001;
            int maleRank=1001;
            String number=null;
            int line=1;
            int compareRank=0;
            int rank=1;
            boolean genderFlag=true;
            while(scan.hasNextLine())
              {
                //creating scanner to read file
                String fileLine=scan.nextLine(); 
                //getting indexes of commas
                int firstComma=fileLine.indexOf(",");
                int secondComma=fileLine.lastIndexOf(",");
                //getting gender in line
                gender=fileLine.substring(firstComma+1,secondComma);
                //getting name in file line
                fileName=fileLine.substring(0,firstComma);
                //getting number in file line
                number=fileLine.substring(secondComma+1,fileLine.length());
                //creating variable to save previous number of names in year
                int save= compareRank;
                //getting number from string
                compareRank=Integer.parseInt(number);
                //changing rank when numbers are differnt for names
              if(save!=compareRank)
                {
                  //setting rank to line
                  rank=line;
                }
              //cheking name and gender for female
              if(name.equalsIgnoreCase(fileName)&&gender.equalsIgnoreCase("f"))
                {
                  //setting female rank
                  femaleRank=rank;
                  //getting number of names from string
                  numberFemale=fileLine.substring(secondComma+1,
                    fileLine.length());
                }
              //boolean flag to reset count
              if(gender.equalsIgnoreCase("m")&&genderFlag==true)
                {
                  //changin boolean value
                  genderFlag=false;
                  line=1;
                }
              //cheking name and gender for males
              if(name.equalsIgnoreCase(fileName)&&gender.equalsIgnoreCase("m"))
                {
                  //setting male rank
                  maleRank=rank;
                  //setting number to number in line     
                  numberMale=fileLine.substring(secondComma+1,
                  fileLine.length());
                }   
              //counting line number
              line++;
            }
          //print if female has a rank set
          if(femaleRank!=1001)
            {
              System.out.printf("%s%6d  ",year,
                  femaleRank);
            }
          //print if no feamale rank
          else
            {
              System.out.printf("%s%6d  ",year,
                  femaleRank);
            }
          //print if male has rank set
          if(maleRank!=1001)
            {
              System.out.printf("%2s%6d\n",year,
                  maleRank);
            }
          //print of no male rank sert
          else
            {
              System.out.printf("%2s%6d\n",year,
                  maleRank);
            }
        } 
      }
    /*populateRankArrays 
     *
     *
     *
     *
     *Parameters : (String name,int startYear,int endYear,
     *File[]yobFiles,int[]femaleRank,int[]maleRank)
     *Returns:     void
     */

  public static void populateRankArrays(String name,int startYear,int endYear,
          File[]yobFiles,int[]femaleRank,int []maleRank) 
    throws FileNotFoundException
    {
    //looping thourhg file array
    for(int i=0;i<yobFiles.length;i++)
      {
        //creates new scanner object
        Scanner scan=new Scanner(yobFiles[i]); 
        //initializing all local variable
        String numberMale=null;
        String numberFemale=null;
        int year=i+startYear;
        String gender=null;
        String fileName=null;
        int femalerank=1001;
        int malerank=1001;
        String number=null;
        int line=1;
        int compareRank=0;
        int rank=1;
        boolean genderFlag=true;
        while(scan.hasNextLine())
        {
          //creating scanner to read file
          String fileLine=scan.nextLine(); 
          //getting indexes of commas
          int firstComma=fileLine.indexOf(",");
          int secondComma=fileLine.lastIndexOf(",");
          //getting gender in line
          gender=fileLine.substring(firstComma+1,secondComma);
          //getting name in file line
          fileName=fileLine.substring(0,firstComma);
          //getting number in file line
          number=fileLine.substring(secondComma+1,fileLine.length());
          //creating variable to save previous number of names in year
          int save= compareRank;
          //getting number from string
          compareRank=Integer.parseInt(number);
          //changing rank when numbers are differnt for names
          if(save!=compareRank)
            {
              //setting rank to line
              rank=line;
            }
          //cheking name and gender for female
          if(name.equalsIgnoreCase(fileName)&&gender.equalsIgnoreCase("f"))
            {
              //femaleRank[line]=rank;
              //setting female rank
              femalerank=rank;
              //getting number of names from string
              numberFemale=fileLine.substring(secondComma+1,
              fileLine.length());
            }
          //boolean flag to reset count
          if(gender.equalsIgnoreCase("m")&&genderFlag==true)
            {
              //changin boolean value
              genderFlag=false;

              line=1;
            }
          //cheking name and gender for males
          if(name.equalsIgnoreCase(fileName)&&
                  gender.equalsIgnoreCase("m"))
            {
              //maleRank[line]=rank;
              //setting male rank
              malerank=rank;
              //setting number to number in line     
              numberMale=fileLine.substring(secondComma+1,
              fileLine.length());
            }  
            //counting line number
            line++;
          }
          //placing ranks in arrays
          femaleRank[i]=femalerank;
          maleRank[i]=malerank;
        }
      }
    /*plotMaleGraph
     *
     *plots array ranks on graph
     *
     *
     *Parameters : (Graphics g)
     *Returns:     void
     */

  public void plotMaleGraph(Graphics g)
    {
      //distance between points per year
      int pixel=15;
      //intitalizin year counter
      int year=START_YEAR;
      //setting color of graphics objecto
      g.setColor(Color.blue);
      //looping though array to plot points
      for(int i=0;i<MALE_RANK.length;i++)
        {
          //draing points on graph
          g.fillOval(MARGIN-2+pixel,(MALE_RANK[i]+1)/2+MARGIN-2,4,4);
          String yearString=""+year+"";
          //checking if year is a multiple of 5 to place on graph
          if(year%5==0)
            {
              g.drawString(yearString,MARGIN+pixel,(MALE_RANK[i]+1)/2+MARGIN);
            }
          //iterating count for year and pixel
          year++;
          pixel=pixel+15;
        }
    }
    /* plotFemalGraph
     *
     *plots female array points on graph
     *
     *
     *Parameters : (Graphics g)
     *Returns:     void
     */

  public void plotFemaleGraph(Graphics g)
    {
      //initialinz pixel count to interate between years in array
      int pixel=15;
      int year=START_YEAR;
      //setting color of graphics object
      g.setColor(Color.pink);
      //looping though female array to plot points
      for(int i=0;i<FEMALE_RANK.length;i++)
        {
          g.fillOval(MARGIN-2+pixel,(FEMALE_RANK[i]+1)/2+MARGIN-2,4,4);
          //if year is multiple of 5 place it on graph
          if(year%5==0)
            {
              String yearString=""+year+"";
              g.drawString(yearString,MARGIN+pixel,
                (FEMALE_RANK[i]+1)/2+MARGIN);
            }
          //count pixel and year
          pixel=pixel+15;
          year++;
        }
    }
    /* drawMaleGraph
     *
     *draws lines for male array points on graph
     *
     *
     *Parameters : (Graphics)
     *Returns:     void
     */

  public void drawMaleGraph(Graphics g)
    {
      int pixel=15;
      //looping though array of points to draw line between points
      for(int i=0;i<MALE_RANK.length-1;)
        {
          //chagning color of graphics object
          g.setColor(Color.blue);
          //drawing line between point i and i+1
          g.drawLine(MARGIN+pixel,(MALE_RANK[i]+1)/2+
            MARGIN,MARGIN+pixel+15,(MALE_RANK[i+1]+1)/2+MARGIN);
          //counting pixels and i
          pixel=pixel+15;
          i++;
        }
    }
    /* drawFemaleGraph
     *
     *draws lines between points on graph
     *
     *
     *Parameters : (Graphics g)
     *Returns:     void
     */

  public void drawFemaleGraph(Graphics g)
    {
      int pixel=15;
      //looping though array to draw line bewtween points
      for(int i=0;i<FEMALE_RANK.length-1;)
        {
          //changin color of graphics obj
          g.setColor(Color.pink);
          //draw line between point i and i+1
          g.drawLine(MARGIN+pixel,(FEMALE_RANK[i]+1)/2+
            MARGIN,MARGIN+pixel+15,(FEMALE_RANK[i+1]+1)/2+MARGIN);
          //count pixel and i
          pixel=pixel+15;
          i++;
        }
      }
    }

