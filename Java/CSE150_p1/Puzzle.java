import java.util.Vector;
import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.util.Stack;
import java.util.Queue;

public class Puzzle{

	final static String op1 = "BFS";
	final static String op2 = "DFS";
	final static String op3 = "ID";
	final static String op4 = "A_Star";
	final static String op5 = "Greedy";

	final static String[] optionsList = {op1,op2,op3,op4,op5};

	final static String[] moves = {"West","East","South","North"};

	final static String delimiter = ",";

	static int zeroRow;
	static int zeroColumn;

	static String lastMove =""; 
	
public static void main(String [] args){
	Vector<Vector<Integer>> puzzle = new Vector<Vector<Integer>>();
    Vector<Vector<Integer>> puzzleSolution = new Vector<Vector<Integer>>();

		if(args.length > 1)
		{
			String fileString = args[0];
			String option1 = args[1];
			String option2 = "";	


			boolean optionFlag = false;

			for(int i = 0; i < optionsList.length; i++){

				if(optionsList[i].equalsIgnoreCase(option1))
				{
					optionFlag = true;
				}
			}

			if(optionFlag)
			{

			System.out.println("Solving Puzzle Using "+option1+".....");

		try{

			File file = new File(fileString);
			Scanner scan = new Scanner(file);

			int i = 0;
			int j = 0;
			int sol = 0;
			
			while(scan.hasNextLine())
			{	
				
				Vector<Integer> row = new Vector<Integer>();
				Vector<Integer> rowSolution = new Vector<Integer>();


				String line=scan.nextLine();
				
				String[] parse=line.split(delimiter);
				
				

				while(j < parse.length)
				{
					

					Integer tile = new Integer(Integer.parseInt(parse[j]));

					if((Integer.parseInt(parse[j])) == 0)
					{
						zeroRow = i ;
						zeroColumn = j ;
					}

					row.add(j,tile);
 					rowSolution.add(j,new Integer(sol));
 					


					
 					sol++;
					j++;
					
				}

				puzzle.add(i,row);
				puzzleSolution.add(i,rowSolution);

				j = 0;
				
				i++;
			}		

			scan.close();

			if(option1.equalsIgnoreCase(op1))
			{

				BFS(puzzle,puzzleSolution);
				System.exit(0);
			}
			
			else if(option1.equalsIgnoreCase(op2))
			{
				option2 = args[2];
				DFS(puzzle,puzzleSolution,option2);
				System.exit(0);	
			}


			else
			{
				System.out.println("Check Your Input. It may be invalid");
				System.exit(1);
			}	    

		}

		catch(FileNotFoundException e)
		{
			System.out.println("Check Your Input. Missing File argument");
			System.exit(1);
		}

		}

	    }

    }



    public static void DFS(Vector<Vector<Integer>> puzzle,Vector<Vector<Integer>> puzzleSolution,String op)
	{

		System.out.println(puzzle.toString()+" needs to become \n"+puzzleSolution.toString()+" using DFS");

		Integer tmp = Integer.parseInt(op);

		int depth=tmp.intValue();


		Stack<Vector<Vector<Integer>>> stack = new Stack<Vector<Vector<Integer>>>();
		Vector<Vector<Integer>> tmp2 = puzzle;
		Vector<Vector<Integer>> tmp3 = puzzle;

		Stack<Integer> dep = new Stack<Integer>();
		Stack<String> moveList = new Stack<String>();
		Stack<String> expList = new Stack<String>();

		int nodeExp = 1;

		String path = "";

		stack.push(puzzle);
		dep.push(new Integer(depth));
		moveList.push("");

		while(!stack.empty()){

			tmp2=stack.peek();
			int d = dep.peek() - 1;
			
			

			for(int i = 0 ;i < moves.length ;i++)
			{
			
				if(stack.peek().equals(puzzleSolution)){
					path = path + " " + moveList.peek();
					String [] e = path.split(" ");
					path = "";
					for(String s : e)
					{
						if(s.length() > 0)
						path = path + s.charAt(0);
					}
					//System.out.println(stack.peek().toString());
					System.out.println("Solution length "+ path.length());
					System.out.println("Nodes Expanded "+ nodeExp);	
					System.out.println("Solution Path "+ path);
					

					System.exit(0);
				}

				if(canMove(tmp2,moves[i])){
				
     			tmp3 = movePuzzle(tmp2,moves[i]);
				stack.push(tmp3);
				
				//System.out.println(tmp3.toString());
				moveList.push(moves[i]);
				dep.push(new Integer(d));
				

				}	

				
				
				 
		    }

			path = path + " " + moveList.peek();
		    if(d == 0)
		    {

		    	stack.pop();
				dep.pop();
				moveList.pop();

				path = path.substring(0,path.length() - 2);
		    }

		    if(d < 0){
		    	System.out.println("Not Deep ENough\n\n");
		    	path = path + " " + moveList.peek();
					String [] e = path.split(" ");
					path = "";
					for(String s : e)
					{
						if(s.length() > 0)
						path = path + s.charAt(0);
					}
					System.out.println(stack.peek().toString());
					System.out.println("Solution length "+ path.length());
					System.out.println("Nodes Expanded "+ nodeExp);	
					System.out.println("Solution Path "+ path);
					
		    	System.exit(0);

		    }

		    nodeExp++;
		   	
		  


		}
		
   


		}


public static void BFS(Vector<Vector<Integer>> puzzle,Vector<Vector<Integer>> puzzleSolution){

	
	    Vector<Vector<Vector<Integer>>> queue = new Vector<Vector<Vector<Integer>>>();
    	Vector<Vector<Integer>> moved = movePuzzle(puzzle,"");
    	Vector<String> last = new Vector<String>();
    	Vector<String> path = new Vector<String>();
    	



    	String tmp = "";
    	last.add("");
    	path.add("");
		queue.add(moved);

		int nodeExp = 0;
		 
	
		while(!(queue.elementAt(0).equals(puzzleSolution)))
		{

			Vector<Vector<Integer>> moved2 = queue.elementAt(0);

			String l = last.elementAt(0);
			String p = path.elementAt(0);
			

			lastMove = l;

			for(int i = 0 ;i < moves.length ;i++)
			{
		
				if(canMove(queue.elementAt(0),moves[i])){
				
				moved = movePuzzle(moved2,moves[i]);

				queue.add(moved);
				last.add(moves[i]);
				
				path.add(p+"  "+moves[i]);

				}		
				 
		    }
           

		   	nodeExp++;
		   	
		    queue.remove(0);
		    path.remove(0);
		    last.remove(0);


		 
		   
		}	
					

					
					String o = path.elementAt(0).toString();
				
					String [] e = o.split(" ");
					o = "";
					for(String s : e)
					{
						if(s.length() > 0)
						o = o + s.charAt(0);
					}

					
					System.out.println("Solution length " + o.length());
					System.out.println("Nodes Expanded "+ nodeExp);	
					System.out.println("Solution Path "+ o);
		
   


	}




public static Vector<Vector<Integer>> movePuzzle(Vector<Vector<Integer>> puzzle,String direction){


		Vector<Vector<Integer>> t = new Vector<Vector<Integer>>();
		int sol = 0 ;

		for(int i = 0; i < puzzle.size()  ;i ++){
			    Vector<Integer> row = new Vector<Integer>();
				

				for( int j = 0 ; j < puzzle.elementAt(0).size();j++)
				{
					Integer tile = puzzle.elementAt(i).elementAt(j);
					row.add(j,tile);
					if(puzzle.elementAt(i).elementAt(j).equals(new Integer(0)))
					{
					
					}
				}

				     sol++;
				t.add(i,row);
 					
 				}
 				//System.out.println(t.equals(puzzle));
 				

            
			if(direction.equalsIgnoreCase("East")){

				t.elementAt(zeroRow).set(zeroColumn,t.elementAt(zeroRow).elementAt(zeroColumn + 1));
				t.elementAt(zeroRow).set(zeroColumn + 1,new Integer(0)) ;

				//zeroColumn = zeroColumn + 1 ;

			}

			else if(direction.equalsIgnoreCase("West")){
				
				t.elementAt(zeroRow).set(zeroColumn,t.elementAt(zeroRow).elementAt(zeroColumn - 1));
				t.elementAt(zeroRow).set(zeroColumn - 1,new Integer(0)) ;

				//zeroColumn = zeroColumn - 1 ;
			}

			else if(direction.equalsIgnoreCase("North")){

				
				t.elementAt(zeroRow).set(zeroColumn,t.elementAt(zeroRow - 1).elementAt(zeroColumn));
				t.elementAt(zeroRow - 1).set(zeroColumn,new Integer(0)) ;

				//zeroRow = zeroRow - 1;
				
			}

			else if(direction.equalsIgnoreCase("South")){
				
				t.elementAt(zeroRow).set(zeroColumn,t.elementAt(zeroRow + 1).elementAt(zeroColumn));
				t.elementAt(zeroRow + 1).set(zeroColumn,new Integer(0)) ;

				//zeroRow = zeroRow + 1;

			}

			//lastMove = direction;

		

		return t;

	}

	public static boolean canMove(Vector<Vector<Integer>> puzzle,String direction){
		Vector<Vector<Integer>> t = new Vector<Vector<Integer>>();
		int sol = 0 ;
		
		for(int i = 0; i < puzzle.size()  ;i ++){
			    Vector<Integer> row = new Vector<Integer>();
				

				for( int j = 0 ; j < puzzle.elementAt(0).size();j++)
				{
					Integer tile = puzzle.elementAt(i).elementAt(j);
					row.add(j,tile);
					if(puzzle.elementAt(i).elementAt(j).equals(new Integer(0)))
					{
						zeroRow = i;
						zeroColumn = j;
					}
				}

				     sol++;
				t.add(i,row);
 					
 				}

		//System.out.println(t.toString()+"zeroRow="+zeroRow+"zeroColumn="+zeroColumn);
		if(direction.equalsIgnoreCase("West") && !lastMove.equalsIgnoreCase("East") && zeroColumn != 0){

			return true;
		}

		else if(direction.equalsIgnoreCase("East") && !lastMove.equalsIgnoreCase("West") && zeroColumn != puzzle.elementAt(0).size() - 1){
			return true;
		}

		else if(direction.equalsIgnoreCase("North") && !lastMove.equalsIgnoreCase("South") && zeroRow != 0){
			return true;
		}

		else if(direction.equalsIgnoreCase("South") && !lastMove.equalsIgnoreCase("North") && zeroRow != puzzle.size() - 1 ){
			return true;
		}
		else
		return false;
	}

	public static void write(String s){

	}

}