{
  open Nano        (* nano.ml *)
  open NanoParse   (* nanoParse.ml from nanoParse.mly *)
}

rule token = parse
  | [' ' '\t' '\r' '\n']              {token lexbuf}
  | "true"                            {True}
  | "false"                           {False}
  | ['0'-'9']+ as l                   {Num (int_of_string l)}
  | "let"                             {LET}
  | "rec"                             {REC}
  | "="                               {EQ}
  | "in"                              {IN}
  | "fun"                             {FUN}
  | "->"                              {ARROW}
  | "if"                              {IF}
  | "then"                            {THEN}
  | "else"                            {ELSE}
  | "+"                               {PLUS}
  | "-"                               {MINUS}
  | "*"                               {MUL}
  | "/"                               {DIV}
  | "<"                               {LT}
  | "<="                              {LE}
  | "!="                              {NE}
  | "&&"                              {AND}
  | "||"                              {OR}
  | "("                               {LPAREN}
  | ")"                               {RPAREN}
  | "["                               {LBRAC}
  | "]"                               {RBRAC}
  | ";"                               {SEMI}
  | "::"                              {COLONCOLON}
  | ['a'-'z' 'A'-'Z' '0'-'9']* as l   {Id (l)}
  | eof                               {EOF}
  | _                                 {raise (MLFailure ("Illegal Character '"^(Lexing.lexeme lexbuf)^"'"))}

 


