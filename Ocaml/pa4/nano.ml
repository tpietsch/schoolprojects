exception MLFailure of string

type binop = 
  Plus 
| Minus 
| Mul 
| Div 
| Eq 
| Ne 
| Lt 
| Le 
| And 
| Or          
| Cons

type expr =   
  Const of int 
| True   
| False      
| NilExpr
| Var of string    
| Bin of expr * binop * expr 
| If  of expr * expr * expr
| Let of string * expr * expr 
| App of expr * expr 
| Fun of string * expr    
| Letrec of string * expr * expr
	
type value =  
  Int of int		
| Bool of bool          
| Closure of env * string option * string * expr 
| Nil                    
| Pair of value * value     

and env = (string * value) list

let binopToString op = 
  match op with
      Plus -> "+" 
    | Minus -> "-" 
    | Mul -> "*" 
    | Div -> "/"
    | Eq -> "="
    | Ne -> "!="
    | Lt -> "<"
    | Le -> "<="
    | And -> "&&"
    | Or -> "||"
    | Cons -> "::"

let rec valueToString v = 
  match v with 
    Int i -> 
      Printf.sprintf "%d" i
  | Bool b -> 
      Printf.sprintf "%b" b
  | Closure (evn,fo,x,e) -> 
      let fs = match fo with None -> "Anon" | Some fs -> fs in
      Printf.sprintf "{%s,%s,%s,%s}" (envToString evn) fs x (exprToString e)
  | Pair (v1,v2) -> 
      Printf.sprintf "(%s::%s)" (valueToString v1) (valueToString v2) 
  | Nil -> 
      "[]"

and envToString evn =
  let xs = List.map (fun (x,v) -> Printf.sprintf "%s:%s" x (valueToString v)) evn in
  "["^(String.concat ";" xs)^"]"

and exprToString e =
  match e with
      Const i ->
        Printf.sprintf "%d" i
    | True -> 
        "true" 
    | False -> 
        "false"
    | Var x -> 
        x
    | Bin (e1,op,e2) -> 
        Printf.sprintf "%s %s %s" 
        (exprToString e1) (binopToString op) (exprToString e2)
    | If (e1,e2,e3) -> 
        Printf.sprintf "if %s then %s else %s" 
        (exprToString e1) (exprToString e2) (exprToString e3)
    | Let (x,e1,e2) -> 
        Printf.sprintf "let %s = %s in \n %s" 
        x (exprToString e1) (exprToString e2) 
    | App (e1,e2) -> 
        Printf.sprintf "(%s %s)" (exprToString e1) (exprToString e2)
    | Fun (x,e) -> 
        Printf.sprintf "fun %s -> %s" x (exprToString e) 
    | Letrec (x,e1,e2) -> 
        Printf.sprintf "let rec %s = %s in \n %s" 
        x (exprToString e1) (exprToString e2) 

(*********************** Some helpers you might need ***********************)

let rec fold f base args = 
  match args with [] -> base
    | h::t -> fold f (f(base,h)) t

let listAssoc (k,l) = 
  fold (fun (r,(t,v)) -> if r = None && k=t then Some v else r) None l

(*********************** Your code starts here ****************************)

let lookup (x,evn) = match listAssoc(x,evn) with 
 Some z -> z
| None -> raise (MLFailure("variable not bound: "^x))


let rec eval (evn,e) = match e with 
  Const z -> Int z
| True  -> Bool true
| False  -> Bool false
| NilExpr -> Nil
| Var x -> lookup(x,evn)   

| Bin (x,y,z) -> (match y with 
    Plus -> (let x = eval (evn,x) in let z = eval (evn,z) in match (x,z) with 
                                              |(Int z,Int g) -> Int (g + z)
                                              |_ -> raise (MLFailure("Type Error")))

  | Minus  -> (let x = eval (evn,x) in let z = eval (evn,z) in match (x,z) with 
                                              |(Int z,Int g) -> Int (z-g)
                                              |_ -> raise (MLFailure("Type Error")))

  | Mul  -> (let x = eval (evn,x) in let z = eval (evn,z) in match (x,z) with 
                                              |(Int z,Int g) -> Int (z*g)
                                              |_ -> raise (MLFailure("Type Error")))

  | Div ->(let x = eval (evn,x) in let z = eval (evn,z) in match (x,z) with 
                                              |(Int z,Int g) -> Int (z/g)
                                              |_ -> raise (MLFailure("Type Error")))

  |Eq -> if eval(evn,x) = eval(evn,z) then Bool true else Bool false

  |Ne -> if eval(evn,x) = eval(evn,z) then Bool false else Bool true

  |Lt -> (match (eval(evn,x),eval(evn,z)) with (Int x,Int y) -> Bool (x<y)
                                                |_ -> raise (MLFailure("Type Error")))

  |Le -> (match (eval(evn,x),eval(evn,z)) with (Int x,Int y) -> Bool (x<=y)
                                                |_ -> raise (MLFailure("Type Error")))

  |And -> (match (eval(evn,x),eval(evn,z)) with (Bool x,Bool y) -> Bool (x&&y)
                                                |_ -> raise (MLFailure("Type Error")))

  |Or -> (match (eval(evn,x),eval(evn,z)) with (Bool x,Bool y) -> Bool (x||y)
                                                |_ -> raise (MLFailure("Type Error"))))

|If (x,y,z)-> (match eval(evn,x) with Bool x -> if x then eval(evn,y) else eval(evn,z)
                                      |_ -> raise (MLFailure("Type Error")))

|Let (x,y,z)-> let f = eval(evn,y) in let evn = (x,f)::evn in eval(evn,z)

| Letrec (x,y,z) ->(let n = eval(evn,y) in match n with 
 Closure(e,v,r,t) -> eval(((x,Closure (e,Some x,r,t))::evn),z)
| _ -> eval((x,n)::evn ,z))

|App (x,y) -> (let f = eval(evn,x) in let z = eval(evn,y) in match f with 
                                                                      Closure(evn,None,e,r) -> eval((e,z)::evn,r)
                                                                    | Closure(evn,Some x,e,r) -> eval((x,f)::(e,z)::evn,r)
                                                                    | _ -> raise (MLFailure("Type Error")))

|Fun (x,y) -> Closure (evn,None,x,y)

 


(**********************     Testing Code  ******************************)
