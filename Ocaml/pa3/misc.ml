(* CSE 130: Programming Assignment 3
 * misc.ml
 *)

(* For this assignment, you may use the following library functions:

   List.map
   List.fold_left
   List.fold_right
   List.split
   List.combine
   List.length
   List.append
   List.rev

   See http://caml.inria.fr/pub/docs/manual-ocaml/libref/List.html for
   documentation.
*)



(* Do not change the skeleton code! The point of this assignment is to figure
 * out how the functions can be written this way (using fold). You may only
 * replace the   failwith "to be implemented"   part. *)



(*****************************************************************)
(******************* 1. Warm Up   ********************************)
(*****************************************************************)

let sqsum xs = 
  let f a x = a + x*x in
  let base = 0 in
    List.fold_left f base xs

let pipe fs = 
  let f a x = fun d -> x (a d)  in
  let base = fun y  -> y  in
    List.fold_left f base fs

let rec sepConcat sep sl = match sl with 
  | [] -> ""
  | h :: t -> 
      let f a x = a ^ sep ^ x in
      let base = List.hd sl in
      let l = List.tl sl in
        List.fold_left f base l 

let stringOfList f l = "[" ^ sepConcat ";" (List.map f l) ^ "]";;

(*****************************************************************)
(******************* 2. Big Numbers ******************************)
(**********************************let z = a + x in let w = z/10 in let f = z mod 10 in (w,f)*******************************)

let rec clone x n = let n = n - 1 in if n < 0 then [] else x::clone x n

let rec padZero l1 l2 = let x = abs ((List.length l1) - (List.length l2)) in
if List.length l1 > List.length l2 then (l1,(clone 0 x)@l2) else ((clone 0 x)@l1,l2)

let rec removeZero l = match l with
 |[]->l
 |x::xs -> match x with | 0 -> removeZero xs
                        | _ -> l

let bigAdd l1 l2 = 
  let add (l1, l2) = 
    let f a x = 
    let (z,q) = x in
    let (w,e) = a in ((w+z+q)/10,(w+z+q)mod 10::e) in
    let base = (0,[]) in
    let args = let List.rev (List.combine l1 l2) in
    let (_, res) = List.fold_left f base args in
      res
  in 
    removeZero(add (padZero l1 l2)) 

let rec mulByDigit i l = match i with 
  0 -> [0]
| 1 ->  l
| _ -> let i = i - 1 in mulByDigit (i) (bigAdd l l)

let bigMul l1 l2 = 
  let f a x = 
  let (z,q) = x in
  let (w,e) = a in 
  let base = (0,[]) in
  let args = let (f,y) = (padZero l1 l2) in let rec makeList = match f with 
  | [] -> []
  | x::xs -> match y with 
          |[] -> []
          | t::ts -> 


  in List.combine l1 l2  in
  let (_, res) = List.fold_left f base args in
    res
