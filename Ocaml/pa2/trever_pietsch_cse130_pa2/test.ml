(* CSE 130 PA 2. Autotester *)

#use "misc.ml"
#use "expr.ml" 
#use "art.ml"
#use "tester.ml" 

let sampleTests =
  [
  (fun () -> mkTest
     assoc
     (-1, "william", [("ranjit",85);("william",23);("moose",44)])
     23
     "sample: assoc 1"
  );
  (fun () -> mkTest 
    assoc
    (-1, "bob", [("ranjit",85);("william",23);("moose",44)])
    (-1)
    "sample: assoc 2"
  ); 
  (fun () -> mkTest 
    removeDuplicates
    [1;6;2;4;12;2;13;6;9]
    [1;6;2;4;12;13;9]
    "sample: removeDuplicates 2"
  );
  (fun () -> mkTest 
    removeDuplicates
    [1;1;1]
    [1]
    "sample: removeDuplicates 2"
  );

  (fun () -> mkTest 
    wwhile 
    ((fun x -> let xx = x*x*x in (xx, xx < 100)), 2) 
    512 
    "sample: wwhile 1"
  ); 
  (fun () -> mkTest 
	fixpoint
    ((fun x -> truncate (1e6 *. cos (1e-6 *. float x))), 0)
    739085
    "sample: fixpoint 1"
  ); 
 
 (fun () -> mkTest 
   emitGrayscale
   (eval_fn sampleExpr, 150,"sample")
   ()
   "sample: eval_fn 1: manual"
 ); 
 (fun () -> mkTest 
   emitGrayscale
   (eval_fn sampleExpr2, 150,"sample2")
   ()
   "sample: eval_fn 2: manual"
 );
 
 (fun () -> mkTest 
   (fun () -> doRandomGray (g1 ()))
   ()
   ()
   "sample: gray 1 : manual"
 );
 (fun () -> mkTest 
   (fun () -> doRandomGray (g2 ()))
   ()
   ()
   "sample: gray 2 : manual"
 );
 (fun () -> mkTest 
   (fun () -> doRandomGray (g3 ()))
   ()
   ()
   "sample: gray 3 : manual"
 );

 (fun () -> mkTest 
   (fun () -> doRandomColor (c1 ()))
   ()
   ()
   "sample: color 1 : manual"
 );
 (fun () -> mkTest 
   (fun () -> doRandomColor (c2 ()))
   ()
   ()
   "sample: color 2 : manual"
 );
 (fun () -> mkTest 
   (fun () -> doRandomColor (c3 ()))
   ()
   ()
   "sample: color 3 : manual"
 )] 

(*130*************************************************************)
(*130**************** BEGIN MODIFY *******************************)
(*130*************************************************************)

let yourTests = 
  [ 
  (fun () -> mkTest
     assoc
     (-1, "trever", [])
    (-1)
     "assoc 1"
  );
  (fun () -> mkTest
     assoc
     (-1, "trever", [("trever",23);("trever",34)])
    (23)
     "assoc 2"
  );
  (fun () -> mkTest
     assoc
     (-1, "", [("trever",23);("trever",34)])
    (-1) 
     "assoc 3"
  );
  (fun () -> mkTest
     assoc
     (-1, "wergwregwergwerg", [("trevfdbsber",23);("trwefwefwvver",4);("trwefwef",3);("t",34)])
    (-1)
     "assoc 4"
  );
  (fun () -> mkTest
     assoc
    (-1, "", [("trever",23);("",34)])
    (34) 
     "assoc 5"
  );

  (fun () -> mkTest 
    removeDuplicates
    [1;6;2;4;12;13;9] 
    [1;6;2;4;12;13;9] 
     "removeDuplicates 1"
  );
  (fun () -> mkTest 
    removeDuplicates
    [] 
    [] 
     "removeDuplicates 2"
  );
  (fun () -> mkTest 
    removeDuplicates
    [1;2;3;1;2;3;1;2;3;1;2;3]
    [1;2;3]
     "removeDuplicates 3"
  );
  (fun () -> mkTest 
    removeDuplicates
    [3;2;1;3;2;1;3;2;1]
    [3;2;1] 
     "removeDuplicates 4"
  );
  (fun () -> mkTest 
    removeDuplicates
    [1;2;4;7;44;5;2;3;1;6;5;33;44;6;7;777;3;7;777;1;4]
    [1;2;4;7;44;5;3;6;33;777]
     "removeDuplicates 5"
  );

  (fun () -> mkTest 
    wwhile 
    ((fun x -> let xx = x+x+x in (xx, xx < 100)), 6) 
    162  
    "wwhile 1"
  );
  (fun () -> mkTest 
    wwhile 
    ((fun x -> let x = 2*x in (x, x < 6)), 3) 
    6 
    "wwhile 2"
  );
  (fun () -> mkTest 
     wwhile 
    ((fun x -> let x = 4+5+6+7-x in (x, x = 44)), 2) 
    20
    "wwhile 3"
  );
  (fun () -> mkTest 
    wwhile 
    ((fun x -> let x = x in (x, x > x)), 2) 
    2
    "wwhile 4"
  );
  (fun () -> mkTest 
    wwhile 
    ((fun x -> let x = -20 + x in (x, x > 0)), (-30)) 
    (-50)
    "wwhile 5"
  );
  
  (fun () -> mkTest 
    fixpoint
    ((fun x -> x), 5)
    5
    "fixpoint 1"
  );
  (fun () -> mkTest 
     fixpoint
    ((fun x -> x mod 3), 101)
    2
    "fixpoint 2"
  );
  (fun () -> mkTest 
    fixpoint
    ((fun x -> if (x = 0) then 500 else x mod 3), 321)
    2
    "fixpoint 3"
  );
  (fun () -> mkTest 
    fixpoint
    ((fun x -> if((x*x)mod 3 > 3) then x/2 else 2*x), 24)
    0
    "fixpoint 4"
  );
  (fun () -> mkTest 
    fixpoint
    ((fun x -> x*x), 0)
    0 
    "fixpoint 5"
  );
  ]

(*130*************************************************************)
(*130**************** END MODIFY *********************************)
(*130*************************************************************)

let doTest f = 
  try f () with ex -> 
    Printf.sprintf "WARNING: INVALID TEST THROWS EXCEPTION!!: %s \n\n"
    (Printexc.to_string ex)

let _ =
  let report = List.map doTest (sampleTests @ yourTests) in
  let _ = List.iter print130 (report@([scoreMsg()])) in
  let _ = print130 ("Compiled\n") in
  (!score, !max)

