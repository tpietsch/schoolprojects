(*
 * expr.ml
 * cse130
 * based on code by Chris Stone
 *)

type expr = 
    VarX
  | VarY
  | Sine     of expr
  | Cosine   of expr
  | Average  of expr * expr
  | Times    of expr * expr
  | Thresh   of expr * expr * expr * expr
  | Select of expr * expr * expr
  | Minus   of expr * expr	

(* exprToString : expr -> string
   Complete this function to convert an expr to a string 
*)
let rec exprToString e = match e with
    VarX -> "x"
  | VarY -> "y"
  | Sine s -> "sin(pi*" ^ exprToString s ^ ")"
  | Cosine s -> "cos(pi*" ^ exprToString s ^ ")"
  | Average (s,d) -> "((" ^ exprToString s  ^ "+" ^ exprToString d ^ ")/2)"
  | Times  (s,d) -> "" ^ exprToString s  ^ "*" ^  exprToString d ^ ""
  | Thresh (s,d,e,r) -> "("^exprToString s  ^ "<" ^ exprToString d ^ "?" ^ exprToString e ^ ":" ^ exprToString r ^")"
  | Select (s,d,e) -> "("^exprToString s  ^ "<Zero?" ^ exprToString d ^ ":" ^ exprToString e ^")" 
  | Minus (s,d) -> "("^exprToString s  ^ ">" ^ exprToString d ^ "?" ^ exprToString s ^ "-" ^ exprToString d  ^ ":" ^ exprToString d ^ "-" ^ exprToString s ^")"

(* build functions:
     Use these helper functions to generate elements of the expr
     datatype rather than using the constructors directly.  This
     provides a little more modularity in the design of your program *)

let buildX()                       = VarX
let buildY()                       = VarY
let buildSine(e)                   = Sine(e)
let buildCosine(e)                 = Cosine(e)
let buildAverage(e1,e2)            = Average(e1,e2)
let buildTimes(e1,e2)              = Times(e1,e2)
let buildThresh(a,b,a_less,b_less) = Thresh(a,b,a_less,b_less)
let buildMinus(e1,e2)              = Minus(e1,e2)
let buildSelect(e1,e2,e3)          = Select(e1,e2,e3)


let pi = 4.0 *. atan 1.0

(* eval : expr -> real*real -> real
   Evaluator for expressions in x and y *)



let rec eval (e,x,y) = match e with
    VarX -> x
  | VarY -> y
  | Sine s -> sin(pi *. eval(s,x,y))
  | Cosine s -> cos(pi *. eval(s,x,y))
  | Average (s,d) -> (eval(s,x,y) +. eval(d,x,y))/.(2.0)
  | Times  (s,d) -> eval(s,x,y) *. eval(d,x,y)
  | Thresh (s,d,e,r) -> if eval(s,x,y) < eval(d,x,y) then eval(e,x,y) else eval(r,x,y)
  | Select (s,d,e) -> if(eval(s,x,y) < 0.0) then eval(d,x,y) else eval(e,x,y)
  | Minus (s,d) -> if(eval(s,x,y) > eval(d,x,y)) then eval(s,x,y) -. eval(d,x,y) else eval(d,x,y) -. eval(s,x,y)

let eval_fn e (x,y) = 
  let rv = eval (e,x,y) in
  assert (-1.0 <= rv && rv <= 1.0);
  rv



let sampleExpr =
      buildCosine(buildSine(buildTimes(buildCosine(buildAverage(buildCosine(
      buildX()),buildTimes(buildCosine (buildCosine (buildAverage
      (buildTimes (buildY(),buildY()),buildCosine (buildX())))),
      buildCosine (buildTimes (buildSine (buildCosine
      (buildY())),buildAverage (buildSine (buildX()), buildTimes
      (buildX(),buildX()))))))),buildY())))

let sampleExpr2 =
  buildThresh(buildX(),buildY(),buildSine(buildX()),buildCosine(buildY()))


(************** Add Testing Code Here ***************)
