/*	User-level thread system
 *
 */

#include <setjmp.h>
 #include <string.h>

#include "aux.h"
#include "umix.h"
#include "mythreads.h"

static int MyInitThreadsCalled = 0;	/* 1 if MyInitThreads called, else 0 */

static int curr = 0;
static int threadCount = 0;
static int lastSched = 0;
static struct thread {	
						
	int valid;			
	jmp_buf env;
	jmp_buf back;
	void (*func)();	
	int param;

} thread[MAXTHREADS];

struct queue {	
	struct queue *next;
	int thread;				
};

static struct queue *front;
static struct queue *rear;

//(struct queue*)malloc(sizeof(struct queue));

static struct queue head;

#define STACKSIZE	65536		/* maximum size of thread stack */

/*	MyInitThreads () initializes the thread package.  Must be the first
 *	function called by any user program that uses the thread package.
 */

 void CreateStack(int i) {
 	
 	if(i == MAXTHREADS){

 		char s[STACKSIZE];

 		if (((int) &s[STACKSIZE-1]) - ((int) &s[0]) + 1 != STACKSIZE) {
		Printf ("Stack space reservation failed\n");
		Exit ();
		}
		return;
 	}

 	else {

 	

 		if(setjmp(thread[i].back) == 0) {

 		char s[STACKSIZE];

 		if (((int) &s[STACKSIZE-1]) - ((int) &s[0]) + 1 != STACKSIZE) {
		Printf ("Stack space reservation failed\n");
		Exit ();
		}
 		CreateStack(i + 1);
		
		}
		else{

		(thread[front->thread].func)(thread[front->thread].param);
		MyExitThread();

		}


		
	
 	}

 }

void MyInitThreads ()
{
	
	
	CreateStack(0);

	int i;

	if (MyInitThreadsCalled) {                /* run only once */
		Printf ("InitThreads: should be called only once\n");
		Exit ();
	}

	for (i = 0; i < MAXTHREADS; i++) {	/* initialize thread table */
		thread[i].valid = 0;
	}

	

	thread[0].valid = 1;			/* initialize thread 0 */

	
	front = (struct queue*)malloc(sizeof(struct queue));
	rear = front;
	
	front->thread = 0;
	rear->thread = 0;
	front->next = rear;
	rear->next = front;
	threadCount++;

	MyInitThreadsCalled = 1;

}

/*	MySpawnThread (func, param) spawns a new thread to execute
 *	func (param), where func is a function with no return value and
 *	param is an integer parameter.  The new thread does not begin
 *	executing until another thread yields to it.
 */

int MySpawnThread (func, param)
	void (*func)();		
	int param;
{
	int i;
	if(threadCount == MAXTHREADS){

		return -1;
	}
	if (! MyInitThreadsCalled) {
		Printf ("SpawnThread: Must call InitThreads first\n");
		Exit ();
		}

		for(i = lastSched%MAXTHREADS ;i < MAXTHREADS ;i++) {

		if(!thread[i].valid){

			thread[i].valid = 1;
			thread[i].func = func;
			thread[i].param = param;

			memcpy(thread[i].env,thread[i].back,sizeof(jmp_buf));

			struct queue *tmp = (struct queue*)malloc(sizeof(struct queue));

			thread[i].func = func;
			thread[i].param=param;

			tmp->thread = i;
			tmp->next = front;

			rear->next = tmp;
			rear = tmp;

			threadCount++;
			lastSched = i;
			return i;

			}

		}
		for(i = 0 ;i < MAXTHREADS ;i++) {

		if(!thread[i].valid){

			thread[i].valid = 1;
			thread[i].func = func;
			thread[i].param = param;

			memcpy(thread[i].env,thread[i].back,sizeof(jmp_buf));

			struct queue *tmp = (struct queue*)malloc(sizeof(struct queue));

			thread[i].func = func;
			thread[i].param=param;

			tmp->thread = i;
			tmp->next = front;
			
			rear->next = tmp;
			rear = tmp;

			threadCount++;
			lastSched = i;
			return i;

			}

		}
return -1;
}

int MyYieldThread (t)
	int t;		
{
	int i;
	i = 0;
	if (! MyInitThreadsCalled) {
		Printf ("YieldThread: Must call InitThreads first\n");
		Exit ();
	}

	if (t < 0 || t >= MAXTHREADS) {
		Printf ("YieldThread: %d is not a valid thread ID\n", t);
		return (-1);
	}
	if (! thread[t].valid) {
		Printf ("YieldThread: Thread %d does not exist\n", t);
		return (-1);
	}


	if(t == front->thread){
		
		return t;
	}

    if ((setjmp (thread[front->thread].env)) == 0) {

    struct queue *tmp = (struct queue*)malloc(sizeof(struct queue));
	struct queue *prev =(struct queue*)malloc(sizeof(struct queue));
	


	tmp = front;
	prev = tmp;

	while(tmp->thread != t){
	
		prev = tmp;
		tmp = tmp->next;
	



		if(i == MAXTHREADS + 1){
		
		break;
		}

		i++;


	}
	
	
	prev->next = tmp->next;	
	tmp->next = front->next;
	rear = front;		
	front = tmp;
	rear->next = front;
	//Printf("\n\n%d\n\n",t);
    longjmp (thread[t].env, 1);
    }
   // Printf("\n\n%d\n\n",t);

    
}

/*	MyGetThread () returns ID of currently running thread.
 */

int MyGetThread ()
{
	if (! MyInitThreadsCalled) {
		Printf ("GetThread: Must call InitThreads first\n");
		Exit ();
	}

 	return front->thread;
}

void MySchedThread ()
{

	if (! MyInitThreadsCalled) {
		Printf ("SchedThread: Must call InitThreads first\n");
		Exit ();
	}
	
	MyYieldThread(front->next->thread);
		
}

/*	MyExitThread () causes the currently running thread to exit.
 */

void MyExitThread ()
{
	int i;

	if (! MyInitThreadsCalled) {
		Printf ("ExitThread: Must call InitThreads first\n");
		Exit ();
	}

	//memcpy(thread[front->thread].env,thread[front->thread].back,sizeof(jmp_buf));
	
	thread[front->thread].valid = 0;
	threadCount--;

	if(threadCount == 0){
		Exit();
	}



if ((setjmp (thread[front->thread].env)) == 0) {

	front = front->next;
	rear->next = front;
	//Printf("last env thread jumped to %d",front->thread);
    longjmp (thread[front->thread].env, 1);
    }
}
