/* mykernel2.c: your portion of the kernel (last modified 10/18/09)
 *
 *	Below are procedures that are called by other parts of the kernel.
 *	Your ability to modify the kernel is via these procedures.  You may
 *	modify the bodies of these procedures any way you wish (however,
 *	you cannot change the interfaces).
 * 
 */

#include "aux.h"
#include "sys.h"
#include "mykernel2.h"

#define TIMERINTERVAL 100	


int fifoIndex;
int fifoIndexLowest;
int lastSched;
float totalRequested;

int flag;

int num;
static struct {
	int valid;		
	int pid;		
	int order;
	float util;
	int existQ;
	int runQ;
	int moved;
	float requested;
}proctab[MAXPROCS];

static struct {
	int valid;		
	int pid;		
	int order;
	int existQ;
	int runQ;
	int moved;
	float requested;
	float util;
}procsched[MAXPROCS];

int addToProc(int pid){
int i;
for (i = 0; i < MAXPROCS; i++) {	

if (!proctab[i].valid ) {

 	 proctab[i].valid = 1;
  	 proctab[i].pid = pid;
     proctab[i].runQ = 0;
     proctab[i].existQ = 0;
     proctab[i].requested = 0;
     proctab[i].util = 0;
     proctab[i].moved = 0;
     proctab[i].order = fifoIndex;

     return 1;
		}
		}
Printf("noe avail slots");
return 0;
}
void switchToSched(int pid){

	int i;
	int j;
	int r;
	int e;
	int d;
	int ftab;
	int fsched;
    float g;
	float o;
	fsched = i = r = e = d = ftab = 0;

	for (i = 0; i < MAXPROCS ; ++i)
	{

		if(proctab[i].pid == pid){

			ftab = 1;

			r = proctab[i].runQ;
			e = proctab[i].existQ;
			o = proctab[i].util;
			g = proctab[i].requested;

			proctab[i].runQ = 0;
			proctab[i].requested = 0;
			proctab[i].existQ = 0;
			proctab[i].util = 0;
			proctab[i].moved = 1;
}
}
for (i = 0; i < MAXPROCS ; ++i)
	{

		if(!procsched[i].valid){
			procsched[i].valid = 1;
				procsched[i].pid = pid;
				procsched[i].runQ = r;
				procsched[i].existQ = e;
				procsched[i].requested = g ;
				return;
}
}

}

int clearFromSched(int pid){

	int i;

	for (i = 0; i < MAXPROCS ; ++i)
	{
		if(procsched[i].pid == pid){
			//Printf("\nPID %d with UTILITY %f\n",procsched[i].pid,procsched[i].util);
			Printf("\nPID %d UTIL %f\n",procsched[i].pid,procsched[i].util);
			procsched[i].valid = 0;
  	 		procsched[i].pid = 0;
     		procsched[i].runQ = 0;
     		procsched[i].existQ = 0;
     		procsched[i].requested = 0;
    		procsched[i].util = 0;
    		clearFromTab(pid);
			return 1;
		}
	}
	

	return 0;
}

int clearFromTab(int pid){

	int i;

	

	for (i = 0; i < MAXPROCS ; ++i)
	{	

		if(proctab[i].pid == pid){
			Printf("\nPID %d UTIL %f\n",procsched[i].pid,procsched[i].util);

			proctab[i].valid = 0;
  		    proctab[i].pid = 0;
     		proctab[i].runQ = 0;
     		proctab[i].existQ = 0;
     		proctab[i].requested = 0;
     		proctab[i].util = 0;   
     		for (i = 0; i < MAXPROCS ; ++i)
	{	
	}	
			return 1;
		}
	}
	return 0;
}

void ageProcs(){
	int i;
for(i = 0;i<MAXPROCS;i++){
		if(proctab[i].valid){
			proctab[i].existQ++;
		}

		if(procsched[i].valid){
			procsched[i].existQ++;
		}
	}
}


/*	InitSched () is called when kernel starts up.  First, set the
 *	scheduling policy (see sys.h).  Make sure you follow the rules
 *	below on where and how to set it.  Next, initialize all your data
 *	structures (such as the process table).  Finally, set the timer
 *	to interrupt after a specified number of ticks.
 */

void InitSched ()
{
	int i;

	/* First, set the scheduling policy.  You should only set it
	 * from within this conditional statement.  While you are working
	 * on this assignment, GetSchedPolicy will return NOSCHEDPOLICY,
	 * and so the condition will be true and you may set the scheduling
	 * policy to whatever you choose (i.e., you may replace ARBITRARY).
	 * After the assignment is over, during the testing phase, we will
	 * have GetSchedPolicy return the policy we wish to test, and so
	 * the condition will be false and SetSchedPolicy will not be
	 * called, thus leaving the policy to whatever we chose to test.
	 */
	if (GetSchedPolicy () == NOSCHEDPOLICY) {	/* leave as is */
		SetSchedPolicy (PROPORTIONAL);		/* set policy here */
	}
	
		
	/* Initialize all your data structures here */
	for (i = 0; i < MAXPROCS; i++) {
		proctab[i].valid = 0;
		procsched[i].valid = 0;

	}
	
	totalRequested = 0;
	/* Set the timer last */
	SetTimer (TIMERINTERVAL);
}


/*	StartingProc (pid) is called by the kernel when the process
 *	identified by pid is starting.  This allows you to record the
 *	arrival of a new process in the process table, and allocate
 *	any resources (if necessary).  Returns 1 if successful, 0 otherwise.
 */

int StartingProc (pid)
	int pid;
{
	int add;
	int i;
	add = 0;
	

	if(GetSchedPolicy() == LIFO){
		DoSched();
	}

	for (i = 0; i < MAXPROCS; i++) {	

if (!proctab[i].valid ) {
	 
 	 proctab[i].valid = 1;
  	 proctab[i].pid = pid;
     proctab[i].runQ = 0;
     proctab[i].existQ = 0;
     proctab[i].requested = 0;
     proctab[i].util = 0;
     proctab[i].moved = 0;
     proctab[i].order = fifoIndex;
	fifoIndex++;
	num++;
     return (1);
		}
}
	Printf ("Error in StartingProc: no free table entries\n");
	return (0);
}
			

/*	EndingProc (pid) is called by the kernel when the process
 *	identified by pid is ending.  This allows you to update the
 *	process table accordingly, and deallocate any resources (if
 *	necessary).  Returns 1 if successful, 0 otherwise.
 */


int EndingProc (pid)
	int pid;
{
	int k;
	int j;
	

	j = 0;
	k = 0;

	j = clearFromSched(pid);
	k = clearFromTab(pid);
	num--;
	if( j == 1 || k == 1){
		
		return(1);
	}
	
	Printf ("Error in EndingProc: can't find process %d\n", pid);
	return (0);
}
/*
void roundRob(){
	int i;

	for(i = 0;i < MAXPROCS ; i++){
		Printf("\n Number P %d",num);
		Printf("\nPID %d VALID %d Last %d <= Current I %d",proctab[i].pid,proctab[i].valid,lastSched,i);
		if(proctab[i].valid){
			if(lastSched == MAXPROCS){
				lastSched = 0;
			}
			if(i >= lastSched){

				lastSched = i + 1;
				proctab[i].runQ++;
				return proctab[i].pid;
			}


		} 
		else{
			if(i == MAXPROCS - 1){
				lastSched = 0;
				i = -1;
			}
		} 
		if(num == 0){
			break;
		}

	}
	return;
}

/*	SchedProc () is called by kernel when it needs a decision for
 *	which process to run next.  It calls the kernel function
 *	GetSchedPolicy () which will return the current scheduling policy
 *	which was previously set via SetSchedPolicy (policy). SchedProc ()
 *	should return a process id, or 0 if there are no processes to run.
 */
int SchedProc ()
{
	int i;
	int z;
	int j;
	int flag;
	int rrLap;
	int previd;
	int curr;
	float f;
	float tmp;


	switch (GetSchedPolicy ()) {

	case ARBITRARY:

		for (i = 0; i < MAXPROCS; i++) {
			if (proctab[i].valid) {
				return (proctab[i].pid);
			}
		}
		break;

	case FIFO:
		
		for (i = 0; i < MAXPROCS; i++) {
			if (proctab[i].valid && proctab[i].order == fifoIndexLowest) {
				fifoIndexLowest++;
				return (proctab[i].pid);
			}
		}

		fifoIndexLowest++;
		break;

	case LIFO:	
		
		
		j = 0;
		for (i = 0 ; i < MAXPROCS ; i++) {
			if(proctab[i].valid){

				if(proctab[i].order > j){

					j = proctab[i].order;
				}
			}
		}

		for (i = 0 ; i < MAXPROCS ; i++) {
			if(proctab[i].valid){
				if(proctab[i].order == j){
					
					return(proctab[i].pid);
				}
			}
		}

		break;

	case ROUNDROBIN:

			for(i = 0;i < MAXPROCS ; i++){
		//Printf("\n Number P %d",num);
		//Printf("\nPID %d VALID %d Last %d <= Current I %d",proctab[i].pid,proctab[i].valid,lastSched,i);
		if(proctab[i].valid){
			if(lastSched == MAXPROCS){
				lastSched = 0;
			}
			if(i >= lastSched){

				lastSched = i + 1;
				proctab[i].runQ++;
				return proctab[i].pid;
			}


		} 
		else{
			if(i == MAXPROCS - 1){
				lastSched = 0;
				i = -1;
			}
		} 
		if(num == 0){
			break;
		}

	}

		break;

	case PROPORTIONAL:	

		f = 1.0;
		
		//schedualing case
		for (i = 0; i < MAXPROCS; i++) {

		if (procsched[i].valid){
			
			procsched[i].util = (float)procsched[i].runQ/(float)procsched[i].existQ;

			tmp = procsched[i].util/procsched[i].requested;
            
            
			previd = procsched[i].pid;
			if( tmp < f){
			
				f = tmp;
			    z = i;
			}

			}

		}

		if(f < 1.0){
			procsched[z].runQ++;
			return procsched[z].pid;
		}

			for(i = 0;i < MAXPROCS ; i++){
		//Printf("\n Number P %d",num);
		//Printf("\nPID %d VALID %d Last %d <= Current I %d",proctab[i].pid,proctab[i].valid,lastSched,i);
		if(proctab[i].valid){
			if(lastSched == MAXPROCS){
				lastSched = 0;
			}
			if(i >= lastSched){

				lastSched = i + 1;
				proctab[i].runQ++;
				return proctab[i].pid;
			}


		} 
		else{
			if(i == MAXPROCS - 1){
				lastSched = 0;
				i = -1;
			}
		} 
		if(num == 0){
			break;
		}

	}
		
		//no more round robin all scheduals met min case
		for (i = 0; i < MAXPROCS; i++) {

		if (procsched[i].valid){
			
			procsched[i].runQ++;
			return procsched[i].pid;

			}

		}
		
		break;
		}

	
	return (0);
}




/*	HandleTimerIntr () is called by the kernel whenever a timer
 *	interrupt occurs.
 */

void HandleTimerIntr ()
{
	int i;
	ageProcs();
	SetTimer (TIMERINTERVAL);
	switch (GetSchedPolicy ()) {	/* is policy preemptive? */

	case ROUNDROBIN:		/* ROUNDROBIN is preemptive */
	case PROPORTIONAL:		/* PROPORTIONAL is preemptive */

		DoSched ();		/* make scheduling decision */
		break;

	default:			/* if non-preemptive, do nothing */
		break;
	}
}

/*	MyRequestCPUrate (pid, m, n) is called by the kernel whenever a process
 *	identified by pid calls RequestCPUrate (m, n).  This is a request for
 *	a fraction m/n of CPU time, effectively running on a CPU that is m/n
 *	of the rate of the actual CPU speed.  m of every n quantums should
 *	be allocated to the calling process.  Both m and n must be greater
 *	than zero, and m must be less than or equal to n.  MyRequestCPUrate
 *	should return 0 if successful, i.e., if such a request can be
 *	satisfied, otherwise it should return -1, i.e., error (including if
 *	m < 1, or n < 1, or m > n).  If MyRequestCPUrate fails, it should
 *	have no effect on scheduling of this or any other process, i.e., as
 *	if it were never called.
 */

int MyRequestCPUrate (pid, m, n)
	int pid;
	int m;
	int n;
	
{
	int i;
	float k;	
	float h;
	if(GetSchedPolicy() == PROPORTIONAL){
	for(i = 0 ;i < MAXPROCS; i++){
	if(proctab[i].pid == pid){
		if(m > 0 && n > 0 && m <= n){

		h = (float)m/(float)n ;
		k = totalRequested + h;

		if( k <= 1.0){
			proctab[i].requested = h;
			switchToSched(pid);
			totalRequested = k;

			}

			return(0);

		}

	}
	if(procsched[i].pid == pid){

		if(m > 0 && n > 0 && m <= n){

		h = (float)m/(float)n ;
		k = totalRequested + h - procsched[i].requested;
		if( k <= 1){
			procsched[i].requested = h;
			totalRequested = k;
			}

			return(0);

		}

	}
  }
}
return (-1);
}


	

