/* mykernel2.c: your portion of the kernel (last modified 10/18/09)
 *
 *	Below are procedures that are called by other parts of the kernel.
 *	Your ability to modify the kernel is via these procedures.  You may
 *	modify the bodies of these procedures any way you wish (however,
 *	you cannot change the interfaces).
 * 
 */

#include "aux.h"
#include "sys.h"
#include "mykernel2.h"

#define TIMERINTERVAL 1	/* in ticks (tick = 10 msec) */

/*	A sample process table.  You may change this any way you wish.
 */

int fifoIndex;
int fifoIndexLowest;
int lastSched;

//round rob for prop
int last;

//for schedualing percentage of CPU
float totalRequested;

//total processes exiisiting
int num;

//total processes that have CPU requestes
int numReq;


static struct {
	int valid;		/* is this entry valid: 1 = yes, 0 = no */
	int pid;		/* process id (as provided by kernel) */
	int order;

	//utility = runQ/ageQ
	int runQ;
	int existQ;
	float util;

	//m/n
	float requested;

	//utility/requested
	float ratio;

	//made a request flag
	int reqMade;

} proctab[MAXPROCS];


/*	InitSched () is called when kernel starts up.  First, set the
 *	scheduling policy (see sys.h).  Make sure you follow the rules
 *	below on where and how to set it.  Next, initialize all your data
 *	structures (such as the process table).  Finally, set the timer
 *	to interrupt after a specified number of ticks.
 */

void InitSched ()
{
	int i;

	/* First, set the scheduling policy.  You should only set it
	 * from within this conditional statement.  While you are working
	 * on this assignment, GetSchedPolicy will return NOSCHEDPOLICY,
	 * and so the condition will be true and you may set the scheduling
	 * policy to whatever you choose (i.e., you may replace ARBITRARY).
	 * After the assignment is over, during the testing phase, we will
	 * have GetSchedPolicy return the policy we wish to test, and so
	 * the condition will be false and SetSchedPolicy will not be
	 * called, thus leaving the policy to whatever we chose to test.
	 */
	if (GetSchedPolicy () == NOSCHEDPOLICY) {	/* leave as is */
		SetSchedPolicy (PROPORTIONAL);		/* set policy here */
	}
		
	/* Initialize all your data structures here */
	for (i = 0; i < MAXPROCS; i++) {
		proctab[i].valid = 0;

	}

	/* Set the timer last */
	SetTimer (TIMERINTERVAL);
}


/*	StartingProc (pid) is called by the kernel when the process
 *	identified by pid is starting.  This allows you to record the
 *	arrival of a new process in the process table, and allocate
 *	any resources (if necessary).  Returns 1 if successful, 0 otherwise.
 */

int StartingProc (pid)
	int pid;
{
	int i;
	if(GetSchedPolicy() == LIFO){
		DoSched();
	}

	for (i = 0; i < MAXPROCS; i++) {	
	
		if (! proctab[i].valid) {
			proctab[i].valid = 1;
			proctab[i].pid = pid;
			proctab[i].order = fifoIndex;

			//pro initilize vars
			proctab[i].runQ = 0;
			proctab[i].existQ = 0;
			proctab[i].util = 0;
			proctab[i].requested = 0; 
			proctab[i].ratio = 0;
			proctab[i].reqMade = 0;
			fifoIndex++;
			num++;


			
			return (1);
		}
	}

	Printf ("Error in StartingProc: no free table entries\n");
	return (0);
}
			

/*	EndingProc (pid) is called by the kernel when the process
 *	identified by pid is ending.  This allows you to update the
 *	process table accordingly, and deallocate any resources (if
 *	necessary).  Returns 1 if successful, 0 otherwise.
 */


int EndingProc (pid)
	int pid;
{
	int i;
	
	for (i = 0; i < MAXPROCS; i++) {
		if (proctab[i].valid && proctab[i].pid == pid) {
			proctab[i].valid = 0;
			num--;
			if(proctab[i].reqMade == 1){
				numReq--;
			}
			return (1);
		}
	}

	Printf ("Error in EndingProc: can't find process %d\n", pid);
	return (0);
}


/*	SchedProc () is called by kernel when it needs a decision for
 *	which process to run next.  It calls the kernel function
 *	GetSchedPolicy () which will return the current scheduling policy
 *	which was previously set via SetSchedPolicy (policy). SchedProc ()
 *	should return a process id, or 0 if there are no processes to run.
 */

int SchedProc ()
{
	int i;
	int j;
	int pid;
	float f;

	
	switch (GetSchedPolicy ()) {

	case ARBITRARY:

		for (i = 0; i < MAXPROCS; i++) {
			if (proctab[i].valid) {
				return (proctab[i].pid);
			}
		}
		break;

	case FIFO:
		
		for (i = 0; i < MAXPROCS; i++) {
			if (proctab[i].valid && proctab[i].order == fifoIndexLowest) {
				fifoIndexLowest++;
				return (proctab[i].pid);
			}
		}

		fifoIndexLowest++;
		DoSched();

		break;

	case LIFO:	
		
		
		j = 0;
		for (i = 0 ; i < MAXPROCS ; i++) {
			if(proctab[i].valid){

				if(proctab[i].order > j){

					j = proctab[i].order;
				}
			}
		}

		for (i = 0 ; i < MAXPROCS ; i++) {
			if(proctab[i].valid){
				if(proctab[i].order == j){
					
					return(proctab[i].pid);
				}
			}
		}

		break;





	case ROUNDROBIN:
	

	if(lastSched == MAXPROCS){
			lastSched = 0;
			}
	
	for(i = lastSched ;i < MAXPROCS; i++){
		if(num == 0){
			break;
		}
		lastSched++;
		
		if(lastSched == MAXPROCS){
			i = 0;
			lastSched = 1;
			}

		if(proctab[i].valid){
			//lastSched = i;
			return proctab[i].pid;
		}
			
		
		}

		break;



	case PROPORTIONAL:	


	f = 1.0;	

		for(i = 0;i<MAXPROCS;i++){

			if(proctab[i].valid){
				//inc existed count and update utility

				if(proctab[i].reqMade){		

				proctab[i].util = (float)proctab[i].runQ/(float)proctab[i].existQ;

				if(proctab[i].util > 0){


				}
				
				if(proctab[i].util/proctab[i].requested  < f){
					//proctab[i].runQ++;
					f = proctab[i].util/proctab[i].requested;
					pid = i;
					//return proctab[i].pid;
				}
			}
		}
	}

	if(i != 0){
		proctab[i].runQ++;
		return (proctab[i].pid);

	}


if(lastSched == MAXPROCS){
			lastSched = 0;
			}
	
	for(i = lastSched ;i < MAXPROCS; i++){
		if(num == 0){
			break;
		}
		lastSched++;
		
		if(lastSched == MAXPROCS){
			i = 0;
			lastSched = 1;
			}

		if(proctab[i].valid){
			proctab[i].runQ++;
			return proctab[i].pid;
		}
			}
		
	
		}
	
	return (0);
}


/*	HandleTimerIntr () is called by the kernel whenever a timer
 *	interrupt occurs.
 */

void HandleTimerIntr ()
{
	int i;
	SetTimer (TIMERINTERVAL);
	for(i = 0;i<MAXPROCS;i++){
		if(proctab[i].valid){
			proctab[i].existQ++;
			//Printf("\n%d pid exist for %d",proctab[i].pid,proctab[i].existQ);
		}
	}
	switch (GetSchedPolicy ()) {	/* is policy preemptive? */

	case ROUNDROBIN:		/* ROUNDROBIN is preemptive */
	case PROPORTIONAL:		/* PROPORTIONAL is preemptive */

		DoSched ();		/* make scheduling decision */
		break;

	default:			/* if non-preemptive, do nothing */
		break;
	}
}

/*	MyRequestCPUrate (pid, m, n) is called by the kernel whenever a process
 *	identified by pid calls RequestCPUrate (m, n).  This is a request for
 *	a fraction m/n of CPU time, effectively running on a CPU that is m/n
 *	of the rate of the actual CPU speed.  m of every n quantums should
 *	be allocated to the calling process.  Both m and n must be greater
 *	than zero, and m must be less than or equal to n.  MyRequestCPUrate
 *	should return 0 if successful, i.e., if such a request can be
 *	satisfied, otherwise it should return -1, i.e., error (including if
 *	m < 1, or n < 1, or m > n).  If MyRequestCPUrate fails, it should
 *	have no effect on scheduling of this or any other process, i.e., as
 *	if it were never called.
 */

int MyRequestCPUrate (pid, m, n)
	int pid;
	int m;
	int n;
	
{
	int i;
	float k;	
	float h;

	for(i = 0 ;i < MAXPROCS; i++){
	if(proctab[i].pid == pid){

		if(m > 0 && n > 0 && m <= n){

		h = (float)m/(float)n ;
		k = totalRequested + h - proctab[i].requested;
		
		if( k <= 1){
			proctab[i].requested = h;
			totalRequested = k;
			proctab[i].reqMade = 1;
			return(0);

		}

	}
}

}

return (-1);
	
}
