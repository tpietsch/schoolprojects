/* Programming Assignment 2: SlowPrintf Calibration
 *
 * Calibrate SlowPrintf.  Take output and recompile kernel.
 */

#include <stdio.h>
#include "aux.h"
#include "umix.h"

void Main ()
{
	CalibrateSlowPrintf ();
}
