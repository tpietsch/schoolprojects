/* mykernel.c: your portion of the kernel
 *
 *	Below are procedures that are called by other parts of the kernel.
 *	Your ability to modify the kernel is via these procedures.  You may
 *	modify the bodies of these procedures any way you wish (however,
 *	you cannot change the interfaces).
 */

#include "aux.h"
#include "sys.h"
#include "mykernel3.h"

#define FALSE 0
#define TRUE 1

/*	A sample semaphore table.  You may change this any way you wish.
 */

static struct {

	int valid;	/* Is this a valid entry (was sem allocated)? */
	int value;	/* value of semaphore */
    int initV;

    int waitIndex;
	int signalIndex;

	struct {

    int valid;
    int process;
    int index;

    } procs[MAXPROCS];


} semtab[MAXSEMS];



/*	InitSem () is called when kernel starts up.  Initialize data
 *	structures (such as the semaphore table) and call any initialization
 *	procedures here.
 */

void InitSem ()
{
	int s,i;

	/* modify or add code any way you wish */

	for (s = 0; s < MAXSEMS; s++) {		/* mark all sems free */
		semtab[s].valid = FALSE;
		for(i = 0; i < MAXPROCS;i++){
			semtab[s].procs[i].valid = FALSE;
			semtab[s].waitIndex = 0;
			semtab[s].signalIndex = 0;
		}
	}
}

/*	MySeminit (p, v) is called by the kernel whenever the system
 *	call Seminit (v) is called.  The kernel passes the initial
 * 	value v, along with the process ID p of the process that called
 *	Seminit.  MySeminit should allocate a semaphore (find a free entry
 *	in semtab and allocate), initialize that semaphore's value to v,
 *	and then return the ID (i.e., index of the allocated entry).
 */

int MySeminit (p, v)
	int p, v;
{
	int s;

	/* modify or add code any way you wish */

	for (s = 0; s < MAXSEMS; s++) {
		if (semtab[s].valid == FALSE) {
			break;
		}
	}
	if (s == MAXSEMS) {
		Printf ("No free semaphores\n");
		return (-1);
	}

	semtab[s].valid = TRUE;
	semtab[s].value = v;

	return (s);
}

/*	MyWait (p, s) is called by the kernel whenever the system call
 *	Wait (s) is called.
 */

void MyWait (p, s)
	int p, s;
{
	/* modify or add code any way you wish */

    int i;
  if(semtab[s].valid){
    semtab[s].value--;

    if(semtab[s].value < 0){

    for(i = 0; i < MAXPROCS;i++){
    	if(!semtab[s].procs[i].valid){
    			semtab[s].procs[i].valid = TRUE;
    			semtab[s].procs[i].process = p;
    			semtab[s].procs[i].index = semtab[s].waitIndex;
    			semtab[s].value--;
    			break;
    		}
    	}
    	semtab[s].waitIndex++;
    	Block(p);
    }

    
	}

}

/*	MySignal (p, s) is called by the kernel whenever the system call
 *	Signal (s) is called.
 */

void MySignal (p, s)
	int p, s;
{
	/* modify or add code any way you wish */
	int i;
	if(semtab[s].valid){

		semtab[s].value++;

    if(semtab[s].value < 0){  

    for(i = 0; i < MAXPROCS;i++){
    	if(semtab[s].procs[i].valid && semtab[s].procs[i].index == semtab[s].signalIndex ){ 
    			Unblock(semtab[s].procs[i].process);
    			semtab[s].procs[i].process = FALSE;
    			semtab[s].procs[i].valid = FALSE; 
    			semtab[s].signalIndex++; 		
    		}
    	}
    }

	}

	
}

