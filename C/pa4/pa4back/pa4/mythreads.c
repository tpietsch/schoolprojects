/*	User-level thread system
 *
 */

#include <setjmp.h>
 #include <string.h>

#include "aux.h"
#include "umix.h"
#include "mythreads.h"

static int MyInitThreadsCalled = 0;	/* 1 if MyInitThreads called, else 0 */

static int curr = 0;
static int index = 0;
static int fifo = 0;
static int threadCount = 0;
static int lastSched = 0;


static struct thread {	
						/* thread table */
	int valid;			/* 1 if entry is valid, else 0 */
	jmp_buf env;
	jmp_buf back;
	void (*func)();	/* f saves func on top of stack */
	int param;
	int index;
	/* current context */

} thread[MAXTHREADS];



#define STACKSIZE	65536	s	/* maximum size of thread stack */

/*	MyInitThreads () initializes the thread package.  Must be the first
 *	function called by any user program that uses the thread package.
 */

 void CreateStack(int i) {
 	
 	if(i == MAXTHREADS){

 		char s[STACKSIZE];

 		if (((int) &s[STACKSIZE-1]) - ((int) &s[0]) + 1 != STACKSIZE) {
		Printf ("Stack space reservation failed\n");
		Exit ();
		}
		//Printf("Currbase %d\n",curr);
		return;
 	}

 	else {

 		char s[STACKSIZE];

 		if (((int) &s[STACKSIZE-1]) - ((int) &s[0]) + 1 != STACKSIZE) {
		Printf ("Stack space reservation failed\n");
		Exit ();
		}

 		if(setjmp(thread[i].back) == 0) {


 		CreateStack(i + 1);
 		//Printf("\nJumped %d\n",i);
		
		}
		else{

		//Printf("CurrRecAbove %d\n",curr);
		(thread[curr].func)(thread[curr].param);
		//Printf("CurrRec %d\n",curr);
		MyExitThread();

	}		
		
	
 	}

 //	Printf("Curr returing %d\n",curr);
 }

void MyInitThreads ()
{
	
	
	CreateStack(0);
	
	int i;

	if (MyInitThreadsCalled) {                /* run only once */
		Printf ("InitThreads: should be called only once\n");
		Exit ();
	}

	for (i = 0; i < MAXTHREADS; i++) {	/* initialize thread table */
		thread[i].valid = 0;
	}

	

	thread[0].valid = 1;			/* initialize thread 0 */
	thread[0].index = index;
	index++;
	MyInitThreadsCalled = 1;



}

/*	MySpawnThread (func, param) spawns a new thread to execute
 *	func (param), where func is a function with no return value and
 *	param is an integer parameter.  The new thread does not begin
 *	executing until another thread yields to it.
 */

int MySpawnThread (func, param)
	void (*func)();		
	int param;
{
	int i;

	if(MAXTHREADS == threadCount){
		//return -1;
	}

	Printf("\nMax %d LasSchedual %d",MAXTHREADS,lastSched);

	if (! MyInitThreadsCalled) {
		Printf ("SpawnThread: Must call InitThreads first\n");
		Exit ();
	}

		for(i = lastSched ;i < MAXTHREADS ;i++) {

		if(!thread[i].valid){

		memcpy(thread[i].env,thread[i].back,sizeof(jmp_buf));

		thread[i].param = param;
		thread[i].func = func;
		thread[i].valid = 1;
		thread[i].index = index;
		index++;
		threadCount++;
		lastSched = i;
		return i;
		}
		if(i == MAXTHREADS - 1){
			i = 0;
		}
	
	}
return 0;
}

int MyYieldThread (t)
	int t;		
{

	if (! MyInitThreadsCalled) {
		Printf ("YieldThread: Must call InitThreads first\n");
		Exit ();
	}

	if (t < 0 || t >= MAXTHREADS) {
		Printf ("YieldThread: %d is not a valid thread ID\n", t);
		return (-1);
	}
	if (! thread[t].valid) {
		Printf ("YieldThread: Thread %d does not exist\n", t);
		return (-1);
	}
	
	thread[curr].index = index;
	index++;

    if ((setjmp (thread[curr].env)) == 0) {
    		curr = t;
          	longjmp (thread[t].env, 1);
    }
    
}

/*	MyGetThread () returns ID of currently running thread.
 */

int MyGetThread ()
{
	if (! MyInitThreadsCalled) {
		Printf ("GetThread: Must call InitThreads first\n");
		Exit ();
	}

 	return curr;
}

int Fifo(){

	int i;
	int j;
	int tmp = index + 1;

	for(i = 0;i< MAXTHREADS;i++){
		
		if(thread[i].valid && i != curr){
			

			if(thread[i].index < tmp){

				tmp = thread[i].index;
				j = i;
				
			}
				
			}
		}
		return j;
}

void MySchedThread ()
{

	if (! MyInitThreadsCalled) {
		Printf ("SchedThread: Must call InitThreads first\n");
		Exit ();
	}

	if(threadCount < 0) {
		Exit();
	}

	MyYieldThread(Fifo());
			
}

/*	MyExitThread () causes the currently running thread to exit.
 */

void MyExitThread ()
{
	
	if (! MyInitThreadsCalled) {
		Printf ("ExitThread: Must call InitThreads first\n");
		Exit ();
	}

	memcpy(thread[curr].env,thread[curr].back,sizeof(jmp_buf));
	thread[curr].valid = 0;
	threadCount--;

	MySchedThread();
}
