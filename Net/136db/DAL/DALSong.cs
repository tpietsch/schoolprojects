﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;  // must add this...
using System.Configuration; // must add this... make sure to add "System.Configuration" first
using System.Data.SqlClient; // must add this...
using System.Data; // must add this...

namespace DAL
{
    public static class DALSong
    {
        static string connection_string = ConfigurationManager.AppSettings["dsn"];

        public static void ClearDatabase(ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);

            try
            {
                string strSQL = "clear_database";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }

        public static Song GetSongDetail(string songid, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            Song song = null;

            try
            {
                string strSQL = "get_song_data";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@songid", SqlDbType.Int));

                mySA.SelectCommand.Parameters["@songid"].Value = songid;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);


                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                song = new Song();

                song.id = myDS.Tables[0].Rows[0]["id"].ToString();
                string[] album_data = GetAlbumDetails(song.id, ref errors);
                song.album_name = album_data[0];
                song.album_year = album_data[1];
                song.genre_name = GetGenreDetails(song.id, ref errors);
                song.artist_name = GetArtistDetails(song.id, ref errors);
                String userID = myDS.Tables[0].Rows[0]["user_key"].ToString();
                song.user = DALUser.GetUserDetail(userID, ref errors);
                song.download_count = myDS.Tables[0].Rows[0]["download_count"].ToString();
                song.name = myDS.Tables[0].Rows[0]["name"].ToString();
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }

            return song;
        }


        public static Song InsertSong(Song song, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            try
            {
                string strSQL = "insert_song";
                Song newSong = new Song();

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@albumName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@albumYear", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@genreName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@artistName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@userId", SqlDbType.Int));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@songName", SqlDbType.VarChar, 50));

                mySA.SelectCommand.Parameters["@albumName"].Value = song.album_name;
                mySA.SelectCommand.Parameters["@albumYear"].Value = song.album_year;
                mySA.SelectCommand.Parameters["@genreName"].Value = song.genre_name;
                mySA.SelectCommand.Parameters["@artistName"].Value = song.artist_name;
                mySA.SelectCommand.Parameters["@userId"].Value = song.user.id;
                mySA.SelectCommand.Parameters["@songName"].Value = song.name;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                newSong.id = myDS.Tables[0].Rows[0]["id"].ToString();
                string[] album_data = GetAlbumDetails(newSong.id, ref errors);
                newSong.album_name = album_data[0];
                newSong.album_year = album_data[1];
                newSong.genre_name = GetGenreDetails(newSong.id, ref errors);
                newSong.artist_name = GetArtistDetails(newSong.id, ref errors);
                String userID = myDS.Tables[0].Rows[0]["user_key"].ToString();
                newSong.user = DALUser.GetUserDetail(userID, ref errors);
                newSong.download_count = myDS.Tables[0].Rows[0]["download_count"].ToString();
                newSong.name = myDS.Tables[0].Rows[0]["name"].ToString();

                return newSong;

            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }


        public static Song UpdateSong(Song song, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            try
            {
                string strSQL = "update_song";
                Song updatedSong = new Song();

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@songid", SqlDbType.Int));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@albumName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@albumYear", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@genreName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@artistName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@userId", SqlDbType.Int));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@songName", SqlDbType.VarChar, 50));

                mySA.SelectCommand.Parameters["@songid"].Value = song.id;
                mySA.SelectCommand.Parameters["@albumName"].Value = song.album_name;
                mySA.SelectCommand.Parameters["@albumYear"].Value = song.album_year;
                mySA.SelectCommand.Parameters["@genreName"].Value = song.genre_name;
                mySA.SelectCommand.Parameters["@artistName"].Value = song.artist_name;
                mySA.SelectCommand.Parameters["@userid"].Value = song.user.id;
                mySA.SelectCommand.Parameters["@songName"].Value = song.name;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                updatedSong.id = myDS.Tables[0].Rows[0]["id"].ToString();
                string[] album_data = GetAlbumDetails(updatedSong.id, ref errors);
                updatedSong.album_name = album_data[0];
                updatedSong.album_year = album_data[1];
                updatedSong.genre_name = GetGenreDetails(updatedSong.id, ref errors);
                updatedSong.artist_name = GetArtistDetails(updatedSong.id, ref errors);
                String userID = myDS.Tables[0].Rows[0]["user_key"].ToString();
                updatedSong.user = DALUser.GetUserDetail(userID, ref errors);
                updatedSong.download_count = myDS.Tables[0].Rows[0]["download_count"].ToString();
                updatedSong.name = myDS.Tables[0].Rows[0]["name"].ToString();

                return updatedSong;
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }


        public static void DeleteSong(string songid, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            try
            {
                string strSQL = "delete_song";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@songid", SqlDbType.Int));

                mySA.SelectCommand.Parameters["@songid"].Value = songid;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }



        public static void IncrementDLCount(string songid, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            try
            {
                string strSQL = "update_download_count";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@songid", SqlDbType.Int));

                mySA.SelectCommand.Parameters["@songid"].Value = songid;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }

        public static int GetDLCount(string songid, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            int dlCount = -1;
            try
            {
                string strSQL = "get_download_count";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@songid", SqlDbType.Int));

                mySA.SelectCommand.Parameters["@songid"].Value = songid;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                if (myDS.Tables[0].Rows.Count == 0)
                    return dlCount;

                dlCount = int.Parse(myDS.Tables[0].Rows[0]["download_count"].ToString());

            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }

            return dlCount;
        }


        public static List<Song> GetSongsByName(string name, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            Song song = null;
            List<Song> songList = new List<Song>();

            try
            {
                string strSQL = "select_by_song_name";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@songName", SqlDbType.VarChar, 50));

                mySA.SelectCommand.Parameters["@songName"].Value = name;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                for (int i = 0; i < myDS.Tables[0].Rows.Count; i++)
                {
                    song = new Song();
                    song.id = myDS.Tables[0].Rows[i]["id"].ToString();
                    string[] album_data = GetAlbumDetails(song.id, ref errors);
                    song.album_name = album_data[0];
                    song.album_year = album_data[1];
                    song.genre_name = GetGenreDetails(song.id, ref errors);
                    song.artist_name = GetArtistDetails(song.id, ref errors);
                    String userID = myDS.Tables[0].Rows[i]["user_key"].ToString();
                    song.user = DALUser.GetUserDetail(userID, ref errors);
                    song.download_count = myDS.Tables[0].Rows[i]["download_count"].ToString();
                    song.name = myDS.Tables[0].Rows[i]["name"].ToString();
                    songList.Add(song);
                }
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }

            return songList;
        }

        public static List<Song> GetSongsByUser(string email, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            Song song = null;
            List<Song> songList = new List<Song>();

            try
            {
                string strSQL = "select_by_user_email";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50));


                mySA.SelectCommand.Parameters["@email"].Value = email;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                for (int i = 0; i < myDS.Tables[0].Rows.Count; i++)
                {
                    song = new Song();
                    song.id = myDS.Tables[0].Rows[i]["id"].ToString();
                    string[] album_data = GetAlbumDetails(song.id, ref errors);
                    song.album_name = album_data[0];
                    song.album_year = album_data[1];
                    song.genre_name = GetGenreDetails(song.id, ref errors);
                    song.artist_name = GetArtistDetails(song.id, ref errors);
                    String userID = myDS.Tables[0].Rows[i]["user_key"].ToString();
                    song.user = DALUser.GetUserDetail(userID, ref errors);
                    song.download_count = myDS.Tables[0].Rows[i]["download_count"].ToString();
                    song.name = myDS.Tables[0].Rows[i]["name"].ToString();
                    songList.Add(song);
                }

                return songList;
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }

        }

        public static List<Song> GetSongsByGenre(string genreName, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            Song song = null;
            List<Song> songList = new List<Song>();

            try
            {
                string strSQL = "select_by_genre";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@genreName", SqlDbType.VarChar, 50));

                mySA.SelectCommand.Parameters["@genreName"].Value = genreName;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                for (int i = 0; i < myDS.Tables[0].Rows.Count; i++)
                {
                    song = new Song();
                    song.id = myDS.Tables[0].Rows[i]["id"].ToString();
                    string[] album_data = GetAlbumDetails(song.id, ref errors);
                    song.album_name = album_data[0];
                    song.album_year = album_data[1];
                    song.genre_name = GetGenreDetails(song.id, ref errors);
                    song.artist_name = GetArtistDetails(song.id, ref errors);
                    String userID = myDS.Tables[0].Rows[i]["user_key"].ToString();
                    song.user = DALUser.GetUserDetail(userID, ref errors);
                    song.download_count = myDS.Tables[0].Rows[i]["download_count"].ToString();
                    song.name = myDS.Tables[0].Rows[i]["name"].ToString();
                    songList.Add(song);
                }
                return songList;
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }

        }

        public static List<Song> GetSongsByArtist(string artistName, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            Song song = null;
            List<Song> songList = new List<Song>();

            try
            {
                string strSQL = "select_by_artist";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@artistName", SqlDbType.VarChar, 50));

                mySA.SelectCommand.Parameters["@artistName"].Value = artistName;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                for (int i = 0; i < myDS.Tables[0].Rows.Count; i++)
                {
                    song = new Song();
                    song.id = myDS.Tables[0].Rows[i]["id"].ToString();
                    string[] album_data = GetAlbumDetails(song.id, ref errors);
                    song.album_name = album_data[0];
                    song.album_year = album_data[1];
                    song.genre_name = GetGenreDetails(song.id, ref errors);
                    song.artist_name = GetArtistDetails(song.id, ref errors);
                    String userID = myDS.Tables[0].Rows[i]["user_key"].ToString();
                    song.user = DALUser.GetUserDetail(userID, ref errors);
                    song.download_count = myDS.Tables[0].Rows[i]["download_count"].ToString();
                    song.name = myDS.Tables[0].Rows[i]["name"].ToString();
                    songList.Add(song);
                }
                return songList;
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }

        private static string GetArtistDetails(string songid, ref List<string> errors)
        {
            {
                SqlConnection conn = new SqlConnection(connection_string);
                string artist_name = null;

                try
                {
                    string strSQL = "get_artist_data";

                    SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                    mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                    mySA.SelectCommand.Parameters.Add(new SqlParameter("@songid", SqlDbType.Int));

                    mySA.SelectCommand.Parameters["@songid"].Value = songid;

                    DataSet myDS = new DataSet();
                    mySA.Fill(myDS);

                    if (myDS.Tables[0].Rows.Count == 0)
                        return null;


                    artist_name = myDS.Tables[0].Rows[0]["name"].ToString();
                    return artist_name;
                }
                catch (Exception e)
                {
                    errors.Add("Error: " + e.ToString());
                    return null;
                }
                finally
                {
                    conn.Dispose();
                    conn = null;
                }
            }
        }

        private static string[] GetAlbumDetails(string songid, ref List<string> errors)
        {
            {
                SqlConnection conn = new SqlConnection(connection_string);
                string[] album_data = {null, null};

                try
                {
                    string strSQL = "get_album_data";

                    SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                    mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                    mySA.SelectCommand.Parameters.Add(new SqlParameter("@songid", SqlDbType.Int));

                    mySA.SelectCommand.Parameters["@songid"].Value = songid;

                    DataSet myDS = new DataSet();
                    mySA.Fill(myDS);

                    if (myDS.Tables[0].Rows.Count == 0)
                        return null;


                    album_data[0] = myDS.Tables[0].Rows[0]["name"].ToString();
                    album_data[1] = myDS.Tables[0].Rows[0]["year"].ToString();
                    return album_data;
                }
                catch (Exception e)
                {
                    errors.Add("Error: " + e.ToString());
                    return null;
                }
                finally
                {
                    conn.Dispose();
                    conn = null;
                }
            }
        }

        private static string GetGenreDetails(string songid, ref List<string> errors)
        {
            {
                SqlConnection conn = new SqlConnection(connection_string);
                string genre_name = null;

                try
                {
                    string strSQL = "get_genre_data";

                    SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                    mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                    mySA.SelectCommand.Parameters.Add(new SqlParameter("@songid", SqlDbType.Int));

                    mySA.SelectCommand.Parameters["@songid"].Value = songid;

                    DataSet myDS = new DataSet();
                    mySA.Fill(myDS);

                    if (myDS.Tables[0].Rows.Count == 0)
                        return null;

                    
                    genre_name = myDS.Tables[0].Rows[0]["name"].ToString();
                    return genre_name;
                }
                catch (Exception e)
                {
                    errors.Add("Error: " + e.ToString());
                    return null;
                }
                finally
                {
                    conn.Dispose();
                    conn = null;
                }
            }
        }

        public static List<Song> GetSongsByAlbum(string albumName, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            Song song = null;
            List<Song> songList = new List<Song>();

            try
            {
                string strSQL = "select_by_album";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@albumName", SqlDbType.VarChar, 50));

                mySA.SelectCommand.Parameters["@albumName"].Value = albumName;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                for (int i = 0; i < myDS.Tables[0].Rows.Count; i++)
                {
                    song = new Song();
                    song.id = myDS.Tables[0].Rows[i]["id"].ToString();
                    string[] album_data = GetAlbumDetails(song.id, ref errors);
                    song.album_name = album_data[0];
                    song.album_year = album_data[1];
                    song.genre_name = GetGenreDetails(song.id, ref errors);
                    song.artist_name = GetArtistDetails(song.id, ref errors);
                    String userID = myDS.Tables[0].Rows[i]["user_key"].ToString();
                    song.user = DALUser.GetUserDetail(userID, ref errors);
                    song.download_count = myDS.Tables[0].Rows[i]["download_count"].ToString();
                    song.name = myDS.Tables[0].Rows[i]["name"].ToString();
                    songList.Add(song);
                }
                return songList;
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }

        public static List<Song> GetSongsByYear(string albumYear, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            Song song = null;
            List<Song> songList = new List<Song>();

            try
            {
                string strSQL = "select_by_year";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@albumYear", SqlDbType.VarChar, 50));

                mySA.SelectCommand.Parameters["@albumYear"].Value = albumYear;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                for (int i = 0; i < myDS.Tables[0].Rows.Count; i++)
                {
                    song = new Song();
                    song.id = myDS.Tables[0].Rows[i]["id"].ToString();
                    string[] album_data = GetAlbumDetails(song.id, ref errors);
                    song.album_name = album_data[0];
                    song.album_year = album_data[1];
                    song.genre_name = GetGenreDetails(song.id, ref errors);
                    song.artist_name = GetArtistDetails(song.id, ref errors);
                    String userID = myDS.Tables[0].Rows[i]["user_key"].ToString();
                    song.user = DALUser.GetUserDetail(userID, ref errors);
                    song.download_count = myDS.Tables[0].Rows[i]["download_count"].ToString();
                    song.name = myDS.Tables[0].Rows[i]["name"].ToString();
                    songList.Add(song);
                }
                return songList;
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }

        public static List<Song> GetSongsByUser(User user, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            Song song = null;
            List<Song> songList = new List<Song>();

            try
            {
                string strSQL = "select_by_user";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                mySA.SelectCommand.Parameters.Add(new SqlParameter("@first_name", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@last_name", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@birthDay", SqlDbType.Date));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@state", SqlDbType.VarChar, 2));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@country", SqlDbType.VarChar, 50));


                mySA.SelectCommand.Parameters["@first_name"].Value = user.first_name;
                mySA.SelectCommand.Parameters["@last_name"].Value = user.last_name;
                mySA.SelectCommand.Parameters["@birthDay"].Value = user.birthDay;
                mySA.SelectCommand.Parameters["@state"].Value = user.state;
                mySA.SelectCommand.Parameters["@country"].Value = user.country;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                for (int i = 0; i < myDS.Tables[0].Rows.Count; i++)
                {
                    song = new Song();
                    song.id = myDS.Tables[0].Rows[i]["id"].ToString();
                    string[] album_data = GetAlbumDetails(song.id, ref errors);
                    song.album_name = album_data[0];
                    song.album_year = album_data[1];
                    song.genre_name = GetGenreDetails(song.id, ref errors);
                    song.artist_name = GetArtistDetails(song.id, ref errors);
                    String userID = myDS.Tables[0].Rows[i]["user_key"].ToString();
                    song.user = DALUser.GetUserDetail(userID, ref errors);
                    song.download_count = myDS.Tables[0].Rows[i]["download_count"].ToString();
                    song.name = myDS.Tables[0].Rows[i]["name"].ToString();
                    songList.Add(song);
                }

                return songList;
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }

        }
    }
}
