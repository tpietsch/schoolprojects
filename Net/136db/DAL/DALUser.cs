﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;  // must add this...
using System.Configuration; // must add this... make sure to add "System.Configuration" first
using System.Data.SqlClient; // must add this...
using System.Data; // must add this...

namespace DAL
{
    public static class DALUser
    {
        public static string connection_string = ConfigurationManager.AppSettings["dsn"];

        public static User InsertUser(User user, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            try
            {
                string strSQL = "insert_user";
                User newUser = new User();

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@firstName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@lastName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@birth", SqlDbType.Date));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@state", SqlDbType.VarChar, 2));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@country", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@access", SqlDbType.Int));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50));

                mySA.SelectCommand.Parameters["@firstName"].Value = user.first_name;
                mySA.SelectCommand.Parameters["@lastName"].Value = user.last_name;
                mySA.SelectCommand.Parameters["@birth"].Value = user.birthDay;
                mySA.SelectCommand.Parameters["@state"].Value = user.state;
                mySA.SelectCommand.Parameters["@country"].Value = user.country;
                mySA.SelectCommand.Parameters["@access"].Value = user.access_level;
                mySA.SelectCommand.Parameters["@password"].Value = user.password;
                mySA.SelectCommand.Parameters["@email"].Value = user.email;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                newUser.id = myDS.Tables[0].Rows[0]["id"].ToString();
                newUser.first_name = myDS.Tables[0].Rows[0]["first_name"].ToString();
                newUser.last_name = myDS.Tables[0].Rows[0]["last_name"].ToString();
                newUser.birthDay = myDS.Tables[0].Rows[0]["birthDay"].ToString();
                newUser.state = myDS.Tables[0].Rows[0]["state"].ToString();
                newUser.country = myDS.Tables[0].Rows[0]["country"].ToString();
                newUser.access_level = myDS.Tables[0].Rows[0]["access_level"].ToString();
                newUser.password = myDS.Tables[0].Rows[0]["password"].ToString();
                newUser.email = myDS.Tables[0].Rows[0]["email"].ToString();
                
                return newUser;

            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }


        public static User UpdateUser(User user, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            try
            {
                string strSQL = "update_user";
                User updatedUser = new User();

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@firstName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@lastName", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@birth", SqlDbType.Date));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@state", SqlDbType.VarChar, 2));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@country", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@access", SqlDbType.Int));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar, 50));
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50));

                mySA.SelectCommand.Parameters["@userid"].Value = user.id;
                mySA.SelectCommand.Parameters["@firstName"].Value = user.first_name;
                mySA.SelectCommand.Parameters["@lastName"].Value = user.last_name;
                mySA.SelectCommand.Parameters["@birth"].Value = user.birthDay;
                mySA.SelectCommand.Parameters["@state"].Value = user.state;
                mySA.SelectCommand.Parameters["@country"].Value = user.country;
                mySA.SelectCommand.Parameters["@access"].Value = user.access_level;
                mySA.SelectCommand.Parameters["@password"].Value = user.password;
                mySA.SelectCommand.Parameters["@email"].Value = user.email;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);

                updatedUser.id = myDS.Tables[0].Rows[0]["id"].ToString();
                updatedUser.first_name = myDS.Tables[0].Rows[0]["first_name"].ToString();
                updatedUser.last_name = myDS.Tables[0].Rows[0]["last_name"].ToString();
                updatedUser.birthDay = myDS.Tables[0].Rows[0]["birthDay"].ToString();
                updatedUser.state = myDS.Tables[0].Rows[0]["state"].ToString();
                updatedUser.country = myDS.Tables[0].Rows[0]["country"].ToString();
                updatedUser.access_level = myDS.Tables[0].Rows[0]["access_level"].ToString();
                updatedUser.password = myDS.Tables[0].Rows[0]["password"].ToString();
                updatedUser.email = myDS.Tables[0].Rows[0]["email"].ToString();

                return updatedUser;

            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }


        public static User GetUserDetail(string userid, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            User user = null;

            try
            {
                string strSQL = "get_user_data";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int));

                mySA.SelectCommand.Parameters["@userid"].Value = Int32.Parse(userid);

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);


                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                user = new User();

                user.id = myDS.Tables[0].Rows[0]["id"].ToString();
                user.first_name = myDS.Tables[0].Rows[0]["first_name"].ToString();
                user.last_name = myDS.Tables[0].Rows[0]["last_name"].ToString();
                string[] birthDateTime = myDS.Tables[0].Rows[0]["birthDay"].ToString().Split(' ');
                string[] date = birthDateTime[0].Split('/');
                if (date[0].Length == 1)
                {
                    date[0] = "0" + date[0];
                }
                if (date[1].Length == 1)
                {
                    date[1] = "0" + date[1];
                }
                user.birthDay = String.Join("/", date);
                user.state = myDS.Tables[0].Rows[0]["state"].ToString();
                user.country = myDS.Tables[0].Rows[0]["country"].ToString();
                user.access_level = myDS.Tables[0].Rows[0]["access_level"].ToString();
                user.password = myDS.Tables[0].Rows[0]["password"].ToString();
                user.email = myDS.Tables[0].Rows[0]["email"].ToString();
                return user;

            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
                return null;
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }

            
        }

        public static void ClearDatabase(ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);

            try
            {
                string strSQL = "clear_database";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }

        public static List<User> GetUserList(ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            User user = null;
            List<User> userList = new List<User>();

            try
            {
                string strSQL = "get_user_list";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);


                if (myDS.Tables[0].Rows.Count == 0)
                    return null;

                for (int x = 0; x < myDS.Tables[0].Rows.Count; x++)
                {
                    user = new User();

                    if (myDS.Tables[0].Rows[x]["access_level"].ToString() == "0")
                    {

                        user.id = myDS.Tables[0].Rows[x]["id"].ToString();
                        user.first_name = myDS.Tables[0].Rows[x]["first_name"].ToString();
                        user.last_name = myDS.Tables[0].Rows[x]["last_name"].ToString();
                        string[] birthDateTime = myDS.Tables[0].Rows[0]["birthDay"].ToString().Split(' ');
                        user.birthDay = birthDateTime[0];
                        user.state = myDS.Tables[0].Rows[x]["state"].ToString();
                        user.country = myDS.Tables[0].Rows[x]["country"].ToString();
                        user.access_level = myDS.Tables[0].Rows[x]["access_level"].ToString();
                        user.password = myDS.Tables[0].Rows[x]["password"].ToString();
                        user.email = myDS.Tables[0].Rows[x]["email"].ToString();

                        userList.Add(user);
                    }
                }
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }

            return userList;
        }

        public static void DeleteUser(string userid, ref List<string> errors)
        {
            SqlConnection conn = new SqlConnection(connection_string);

            try
            {
                string strSQL = "delete_user";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int));

                mySA.SelectCommand.Parameters["@userid"].Value = userid;

                DataSet myDS = new DataSet();
                mySA.Fill(myDS);
            }
            catch (Exception e)
            {
                errors.Add("Error: " + e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }
        }
    }
}
