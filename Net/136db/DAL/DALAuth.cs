﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;  // must add this...
using System.Configuration; // must add this... make sure to add "System.Configuration" first
using System.Data.SqlClient; // must add this...
using System.Data; // must add this...

namespace DAL
{
  public static class DALAuth
  {
    static string connection_string = ConfigurationManager.AppSettings["dsn"];

    public static Logon Authenticate(string email, string password, ref List<string> errors)
    {
      Logon logon = new Logon();
      logon.id = "";
      logon.access_level = "-1";

      SqlConnection conn = new SqlConnection(connection_string);
      try
      {
        string strSQL = "get_login_info";

        SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
        mySA.SelectCommand.CommandType = CommandType.StoredProcedure;
        mySA.SelectCommand.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50));
        mySA.SelectCommand.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar, 50));

        mySA.SelectCommand.Parameters["@email"].Value = email;
        mySA.SelectCommand.Parameters["@password"].Value = password;

        DataSet myDS = new DataSet();
        mySA.Fill(myDS);
        
        if(myDS.Tables.Count > 0)
        {
            if (myDS.Tables[0].Rows.Count > 0)
            {
              logon.access_level = myDS.Tables[0].Rows[0]["access_level"].ToString();
              logon.id = myDS.Tables[0].Rows[0]["id"].ToString();
            }
            else
            {
                logon.id = "-1";
                logon.access_level = "-1";
            }
        }
        else
        {
                logon.id = "-1";
                logon.access_level = "-1";
        }
      }
      catch (Exception e)
      {
        errors.Add("Error: " + e.ToString());
      }
      finally
      {
        conn.Dispose();
        conn = null;
      }

      return logon;
    }

    public static void ClearDatabase(ref List<string> errors)
    {
        SqlConnection conn = new SqlConnection(connection_string);

        try
        {
            string strSQL = "clear_database";

            SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
            mySA.SelectCommand.CommandType = CommandType.StoredProcedure;

            DataSet myDS = new DataSet();
            mySA.Fill(myDS);
        }
        catch (Exception e)
        {
            errors.Add("Error: " + e.ToString());
        }
        finally
        {
            conn.Dispose();
            conn = null;
        }
    }

  }
}
