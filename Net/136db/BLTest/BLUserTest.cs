
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using DomainModel;

namespace BLTest
{
  [TestClass]
  public class BLUserTest
  {
    public BLUserTest()
    {
        List<string> errors = new List<string>();
        BLUser.ClearDatabase(ref errors);
    }

    private TestContext testContextInstance;

    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion

    [TestMethod]
    public void InsertUserErrorTest()
    {
      List<string> errors = new List<string>();

      BLUser.InsertUser(null, ref errors);
      Assert.AreEqual(1, errors.Count);

      errors = new List<string>();

      User user = new User();
      user.first_name = "a";
      BLUser.InsertUser(user, ref errors);
      Assert.AreEqual(3, errors.Count);

      errors = new List<string>();

      user.last_name = "a";
      BLUser.InsertUser(user, ref errors);
      Assert.AreEqual(2, errors.Count);

      errors = new List<string>();

      user.state = "a";
      BLUser.InsertUser(user, ref errors);
      Assert.AreEqual(1, errors.Count);

      errors = new List<string>();
      user.state = "";
      user.country = "a";
      BLUser.InsertUser(user, ref errors);
      Assert.AreEqual(1, errors.Count);
    }

    [TestMethod]
    public void UserErrorTest()
    {
      List<string> errors = new List<string>();

      BLUser.GetUserDetail(null, ref errors);
      Assert.AreEqual(1, errors.Count);
    }

    [TestMethod]
    public void DeleteUserErrorTest()
    {
      List<string> errors = new List<string>();

      BLUser.DeleteUser(null, ref errors);
      Assert.AreEqual(1, errors.Count);
    }

    [TestMethod]
    public void UpdateUserErrorTest()
    {
      List<string> errors = new List<string>();

      BLUser.UpdateUser(null, ref errors);
      Assert.AreEqual(1, errors.Count);

      errors = new List<string>();

      User user = new User();
      user.first_name = "";
      BLUser.UpdateUser(user, ref errors);
      Assert.AreEqual(1, errors.Count);

      errors = new List<string>();

      user = new User();
      user.last_name = "";
      BLUser.UpdateUser(user, ref errors);
      Assert.AreEqual(1, errors.Count);


      errors = new List<string>();

      user = new User();
      user.id = "";
      BLUser.UpdateUser(user, ref errors);
      Assert.AreEqual(1, errors.Count);

    }





  }
}
