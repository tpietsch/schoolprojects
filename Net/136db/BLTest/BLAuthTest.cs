
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using DomainModel;

namespace BLTest
{
  [TestClass]
  public class BLAuthTest
  {
    public BLAuthTest()
    {
        List<string> errors = new List<string>();
        BLAuth.ClearDatabase(ref errors);
    }

    private TestContext testContextInstance;

    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion

    [TestMethod]
    public void InsertValidAuthTest()
    {
      List<string> errors = new List<string>();

      BLAuth.Authenticate( "456.com", "password",  ref errors);
      Assert.AreEqual(1, errors.Count);

    }

    [TestMethod]
    public void InsertInvalidAuthEmailTest()
    {
      List<string> errors = new List<string>();

      BLAuth.Authenticate(null, "1234", ref errors);
      Assert.AreEqual(1, errors.Count);

      errors = new List<string>();

      BLAuth.Authenticate("1234@adf", "1234", ref errors);
      Assert.AreEqual(1, errors.Count);


      errors = new List<string>();

      BLAuth.Authenticate("asdf.asdf", "1234", ref errors);
      Assert.AreEqual(1, errors.Count);


      errors = new List<string>();

      BLAuth.Authenticate("1234", "1234", ref errors);
      Assert.AreEqual(1, errors.Count);
    }

  }

}
