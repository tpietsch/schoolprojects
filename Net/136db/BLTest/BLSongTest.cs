﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using DomainModel;

namespace BLTest
{
  [TestClass]
  public class BLSongTest
  {
      public BLSongTest()
      {
          List<string> errors = new List<string>();
          BLSong.ClearDatabase(ref errors);
      }

    private TestContext testContextInstance;

    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion
    
    [TestMethod]
    public void InsertSongTest()
    {
         
        List<string> errors = new List<string>();

        Song test = new Song();
        test.album_year = "abcd";

        BLSong.InsertSong(test,ref errors);

        Assert.AreEqual(1, errors.Count);
    }
    
    
    [TestMethod]
    public void InsertSongNullTest()
    {
      
        List<string> errors = new List<string>();

        Song song = null;
        BLSong.InsertSong(song,ref errors);
        Assert.AreEqual(1, errors.Count);
        
   }

    
    [TestMethod]
    public void UpdateSongTest()
    {

          List<string> errors = new List<string>();


          Song test = new Song();
          test.album_year = "abcd";

          BLSong.UpdateSong(test, ref errors);
          Assert.AreEqual(1, errors.Count);

          test = null;

          errors = new List<string>();
          BLSong.UpdateSong(test, ref errors);
          Assert.AreEqual(1, errors.Count);   
    }
    

    [TestMethod]
    public void DeleteSongTest()
    {
      List<string> errors = new List<string>();
      string song = null;
      BLSong.DeleteSong(song,ref errors);
      Assert.AreEqual(1, errors.Count);
    }

    [TestMethod]
    public void IncrementDLCountTest()
    {
        List<string> errors = new List<string>();
        string song = null;
        BLSong.IncrementDLCount(song, ref errors);
        Assert.AreEqual(1, errors.Count);

    }

    [TestMethod]
    public void GetDLCountTest()
    {
        List<string> errors = new List<string>();
        string song = null;
        BLSong.GetDLCount(song, ref errors);
        Assert.AreEqual(1, errors.Count);

    }

    [TestMethod]
    public void GetSongsByNameTest()
    {
        List<string> errors = new List<string>();
        string song = null;
        BLSong.GetSongsByName(song, ref errors);
        Assert.AreEqual(1, errors.Count);
    }
      
    [TestMethod]
    public void GetSongsByGenreTest()
    {
        List<string> errors = new List<string>();
        string song = null;
        BLSong.GetSongsByGenre(song, ref errors);
        Assert.AreEqual(1, errors.Count);

    }

    [TestMethod]
    public void GetSongsByArtistTest()
    {
        List<string> errors = new List<string>();
        string song = null;
        BLSong.GetSongsByArtist(song, ref errors);
        Assert.AreEqual(1, errors.Count);

    }

    [TestMethod]
    public void GetSongsByUserTest()
    {
        List<string> errors = new List<string>();
        string email = null;
        BLSong.GetSongsByUser(email, ref errors);
        Assert.AreEqual(1, errors.Count);
    }

    public void GetSongsByAlbumTest()
    {
        List<string> errors = new List<string>();
        string song = "Prince";
        BLSong.GetSongsByAlbum(song, ref errors);
        Assert.AreEqual(1, errors.Count);

        errors = new List<string>();
        song = null;
        BLSong.GetSongsByAlbum(song, ref errors);
        Assert.AreEqual(1, errors.Count);

        errors = new List<string>();
        song = "Prince";
        BLSong.GetSongsByAlbum(song, ref errors);
        Assert.AreEqual(1, errors.Count);
    }
  }
}
