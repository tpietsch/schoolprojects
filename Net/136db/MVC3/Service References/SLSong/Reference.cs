﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.225
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC3.SLSong {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Song", Namespace="http://schemas.datacontract.org/2004/07/DomainModel")]
    [System.SerializableAttribute()]
    public partial class Song : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string album_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string album_yearField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string artist_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string download_countField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string genre_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private MVC3.SLSong.User userField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string album_name {
            get {
                return this.album_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.album_nameField, value) != true)) {
                    this.album_nameField = value;
                    this.RaisePropertyChanged("album_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string album_year {
            get {
                return this.album_yearField;
            }
            set {
                if ((object.ReferenceEquals(this.album_yearField, value) != true)) {
                    this.album_yearField = value;
                    this.RaisePropertyChanged("album_year");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string artist_name {
            get {
                return this.artist_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.artist_nameField, value) != true)) {
                    this.artist_nameField = value;
                    this.RaisePropertyChanged("artist_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string download_count {
            get {
                return this.download_countField;
            }
            set {
                if ((object.ReferenceEquals(this.download_countField, value) != true)) {
                    this.download_countField = value;
                    this.RaisePropertyChanged("download_count");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string genre_name {
            get {
                return this.genre_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.genre_nameField, value) != true)) {
                    this.genre_nameField = value;
                    this.RaisePropertyChanged("genre_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string id {
            get {
                return this.idField;
            }
            set {
                if ((object.ReferenceEquals(this.idField, value) != true)) {
                    this.idField = value;
                    this.RaisePropertyChanged("id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                if ((object.ReferenceEquals(this.nameField, value) != true)) {
                    this.nameField = value;
                    this.RaisePropertyChanged("name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public MVC3.SLSong.User user {
            get {
                return this.userField;
            }
            set {
                if ((object.ReferenceEquals(this.userField, value) != true)) {
                    this.userField = value;
                    this.RaisePropertyChanged("user");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="User", Namespace="http://schemas.datacontract.org/2004/07/DomainModel")]
    [System.SerializableAttribute()]
    public partial class User : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string access_levelField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string birthDayField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string countryField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string emailField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string first_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string last_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string passwordField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string stateField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string access_level {
            get {
                return this.access_levelField;
            }
            set {
                if ((object.ReferenceEquals(this.access_levelField, value) != true)) {
                    this.access_levelField = value;
                    this.RaisePropertyChanged("access_level");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string birthDay {
            get {
                return this.birthDayField;
            }
            set {
                if ((object.ReferenceEquals(this.birthDayField, value) != true)) {
                    this.birthDayField = value;
                    this.RaisePropertyChanged("birthDay");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string country {
            get {
                return this.countryField;
            }
            set {
                if ((object.ReferenceEquals(this.countryField, value) != true)) {
                    this.countryField = value;
                    this.RaisePropertyChanged("country");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string email {
            get {
                return this.emailField;
            }
            set {
                if ((object.ReferenceEquals(this.emailField, value) != true)) {
                    this.emailField = value;
                    this.RaisePropertyChanged("email");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string first_name {
            get {
                return this.first_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.first_nameField, value) != true)) {
                    this.first_nameField = value;
                    this.RaisePropertyChanged("first_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string id {
            get {
                return this.idField;
            }
            set {
                if ((object.ReferenceEquals(this.idField, value) != true)) {
                    this.idField = value;
                    this.RaisePropertyChanged("id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string last_name {
            get {
                return this.last_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.last_nameField, value) != true)) {
                    this.last_nameField = value;
                    this.RaisePropertyChanged("last_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string password {
            get {
                return this.passwordField;
            }
            set {
                if ((object.ReferenceEquals(this.passwordField, value) != true)) {
                    this.passwordField = value;
                    this.RaisePropertyChanged("password");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string state {
            get {
                return this.stateField;
            }
            set {
                if ((object.ReferenceEquals(this.stateField, value) != true)) {
                    this.stateField = value;
                    this.RaisePropertyChanged("state");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SLSong.ISLSong")]
    public interface ISLSong {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/GetSongDetails", ReplyAction="http://tempuri.org/ISLSong/GetSongDetailsResponse")]
        MVC3.SLSong.Song GetSongDetails(string songid, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/InsertSong", ReplyAction="http://tempuri.org/ISLSong/InsertSongResponse")]
        MVC3.SLSong.Song InsertSong(MVC3.SLSong.Song song, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/UpdateSong", ReplyAction="http://tempuri.org/ISLSong/UpdateSongResponse")]
        void UpdateSong(MVC3.SLSong.Song song, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/DeleteSong", ReplyAction="http://tempuri.org/ISLSong/DeleteSongResponse")]
        void DeleteSong(string songid, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/IncrementDLCount", ReplyAction="http://tempuri.org/ISLSong/IncrementDLCountResponse")]
        void IncrementDLCount(string songid, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/GetDLCount", ReplyAction="http://tempuri.org/ISLSong/GetDLCountResponse")]
        int GetDLCount(string songid, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/GetSongsByName", ReplyAction="http://tempuri.org/ISLSong/GetSongsByNameResponse")]
        MVC3.SLSong.Song[] GetSongsByName(string name, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/GetSongsByUserEmail", ReplyAction="http://tempuri.org/ISLSong/GetSongsByUserEmailResponse")]
        MVC3.SLSong.Song[] GetSongsByUserEmail(string email, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/GetSongsByUser", ReplyAction="http://tempuri.org/ISLSong/GetSongsByUserResponse")]
        MVC3.SLSong.Song[] GetSongsByUser(MVC3.SLSong.User user, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/GetSongsByGenre", ReplyAction="http://tempuri.org/ISLSong/GetSongsByGenreResponse")]
        MVC3.SLSong.Song[] GetSongsByGenre(string genreName, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/GetSongsByArtist", ReplyAction="http://tempuri.org/ISLSong/GetSongsByArtistResponse")]
        MVC3.SLSong.Song[] GetSongsByArtist(string artistName, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/GetSongsByAlbum", ReplyAction="http://tempuri.org/ISLSong/GetSongsByAlbumResponse")]
        MVC3.SLSong.Song[] GetSongsByAlbum(string albumName, ref string[] errors);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLSong/GetSongsByYear", ReplyAction="http://tempuri.org/ISLSong/GetSongsByYearResponse")]
        MVC3.SLSong.Song[] GetSongsByYear(string albumYear, ref string[] errors);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISLSongChannel : MVC3.SLSong.ISLSong, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SLSongClient : System.ServiceModel.ClientBase<MVC3.SLSong.ISLSong>, MVC3.SLSong.ISLSong {
        
        public SLSongClient() {
        }
        
        public SLSongClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SLSongClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SLSongClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SLSongClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public MVC3.SLSong.Song GetSongDetails(string songid, ref string[] errors) {
            return base.Channel.GetSongDetails(songid, ref errors);
        }
        
        public MVC3.SLSong.Song InsertSong(MVC3.SLSong.Song song, ref string[] errors) {
            return base.Channel.InsertSong(song, ref errors);
        }
        
        public void UpdateSong(MVC3.SLSong.Song song, ref string[] errors) {
            base.Channel.UpdateSong(song, ref errors);
        }
        
        public void DeleteSong(string songid, ref string[] errors) {
            base.Channel.DeleteSong(songid, ref errors);
        }
        
        public void IncrementDLCount(string songid, ref string[] errors) {
            base.Channel.IncrementDLCount(songid, ref errors);
        }
        
        public int GetDLCount(string songid, ref string[] errors) {
            return base.Channel.GetDLCount(songid, ref errors);
        }
        
        public MVC3.SLSong.Song[] GetSongsByName(string name, ref string[] errors) {
            return base.Channel.GetSongsByName(name, ref errors);
        }
        
        public MVC3.SLSong.Song[] GetSongsByUserEmail(string email, ref string[] errors) {
            return base.Channel.GetSongsByUserEmail(email, ref errors);
        }
        
        public MVC3.SLSong.Song[] GetSongsByUser(MVC3.SLSong.User user, ref string[] errors) {
            return base.Channel.GetSongsByUser(user, ref errors);
        }
        
        public MVC3.SLSong.Song[] GetSongsByGenre(string genreName, ref string[] errors) {
            return base.Channel.GetSongsByGenre(genreName, ref errors);
        }
        
        public MVC3.SLSong.Song[] GetSongsByArtist(string artistName, ref string[] errors) {
            return base.Channel.GetSongsByArtist(artistName, ref errors);
        }
        
        public MVC3.SLSong.Song[] GetSongsByAlbum(string albumName, ref string[] errors) {
            return base.Channel.GetSongsByAlbum(albumName, ref errors);
        }
        
        public MVC3.SLSong.Song[] GetSongsByYear(string albumYear, ref string[] errors) {
            return base.Channel.GetSongsByYear(albumYear, ref errors);
        }
    }
}
