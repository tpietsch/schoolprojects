﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC3.Models;

namespace MVC3.Controllers
{
    public class AdminController : Controller
    {
        private static int songNameSort = 0;
        private static int songArtistSort = 0;
        private static int songIdSort = 0;
        private static int userIdSort = 0;
        private static int userEmailSort = 0;

        public ActionResult SortByUserId()
        {
            List<PLUser> userList = UserClientService.GetUserList();
            if (userEmailSort >= 1)
            {
                userEmailSort = 0;
                userIdSort = 1;
                List<PLUser> sortList = userList.OrderBy(x => x.id).ToList();
                return View("Index", sortList);
            }
            else
            {
                if (userIdSort == 1)
                {
                    userIdSort = 2;
                    List<PLUser> sortList = userList.OrderByDescending(x => x.id).ToList();
                    return View("Index", sortList);
                }
                else
                {
                    userIdSort = 1;
                    List<PLUser> sortList = userList.OrderBy(x => x.id).ToList();
                    return View("Index", sortList);
                }
            }
        }

        public ActionResult SortByUserEmail()
        {
            List<PLUser> userList = UserClientService.GetUserList();
            if (userIdSort >= 1)
            {
                userIdSort = 0;
                userEmailSort = 1;
                List<PLUser> sortList = userList.OrderBy(x => x.email).ToList();
                return View("Index", sortList);
            }
            else
            {
                if (userEmailSort == 1)
                {
                    userEmailSort = 2;
                    List<PLUser> sortList = userList.OrderByDescending(x => x.email).ToList();
                    return View("Index", sortList);
                }
                else
                {
                    userEmailSort = 1;
                    List<PLUser> sortList = userList.OrderBy(x => x.email).ToList();
                    return View("Index", sortList);
                }
            }
        }

        public ActionResult SortBySongName()
        {
            PLUser user = UserClientService.GetUserDetail(Request.QueryString["user_id"].ToString());
            List<PLSong> songList = SongClientService.GetSongsByUser(user);
            if (songArtistSort >= 1 || songIdSort >= 1)
            {
                songArtistSort = 0;
                songIdSort = 0;
                songNameSort = 1;
                List<PLSong> sortList = songList.OrderBy(x => x.id).ToList();
                return View("UserSongs", sortList);
            }
            else
            {
                if (songNameSort == 1)
                {
                    songNameSort = 2;
                    List<PLSong> sortList = songList.OrderByDescending(x => x.name).ToList();
                    return View("UserSongs", sortList);
                }
                else
                {
                    songNameSort = 1;
                    List<PLSong> sortList = songList.OrderBy(x => x.name).ToList();
                    return View("UserSongs", sortList);
                }
            }
        }

        public ActionResult SortBySongArtist()
        {
            PLUser user = UserClientService.GetUserDetail(Request.QueryString["user_id"].ToString());
            List<PLSong> songList = SongClientService.GetSongsByUser(user);
            if (songIdSort >= 1 || songNameSort >= 1)
            {
                songIdSort = 0;
                songNameSort = 0;
                songArtistSort = 1;
                List<PLSong> sortList = songList.OrderBy(x => x.id).ToList();
                return View("UserSongs", sortList);
            }
            else
            {
                if (songArtistSort == 1)
                {
                    songArtistSort = 2;
                    List<PLSong> sortList = songList.OrderByDescending(x => x.artist_name).ToList();
                    return View("UserSongs", sortList);
                }
                else
                {
                    songArtistSort = 1;
                    List<PLSong> sortList = songList.OrderBy(x => x.artist_name).ToList();
                    return View("UserSongs", sortList);
                }
            }
        }

        public ActionResult Index()
        {
            userIdSort = 0;
            userEmailSort = 0;
            List<PLUser> userList = UserClientService.GetUserList();
            return View(userList);
        }

        public ActionResult UserDetails()
        {
            PLUser user = UserClientService.GetUserDetail(Request.QueryString["user_id"].ToString());
            return RedirectToAction("GetUserDetails", "User", user);
        }

        public ActionResult UpdateUserDetails()
        {
            PLUser user = UserClientService.GetUserDetail(Request.QueryString["user_id"].ToString());
            return RedirectToAction("UpdateUserDetails", "User", user);
        }

        public ActionResult UserDelete()
        {
            string user_id = Request.QueryString["user_id"];
            UserClientService.DeleteUser(user_id);
            return RedirectToAction("Index", "Admin");
        }

        public ActionResult UserSongs()
        {
            songNameSort = 0;
            songArtistSort = 0;
            songIdSort = 0;
            PLUser currUser = UserClientService.GetUserDetail(Request.QueryString["user_id"].ToString());
            return RedirectToAction("UserIndex", "User", currUser);
        }

        public ActionResult Details()
        {
            return RedirectToAction("Details", "User");
        }

        public ActionResult UpdateDetails()
        {
            PLUser user = UserClientService.GetUserDetail(Session["id"].ToString());
            return RedirectToAction("UpdateUserDetails", "User", user);
        }
    }
}
