﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC3.Models; // this is required...

namespace MVC3.Controllers
{
    public class CreateAccountController : Controller
    {
        //
        // GET: /Login/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            try
            {
                string email = collection["email"].Trim();
                string password = collection["password"].Trim();
                string first_name = collection["first_name"].Trim();
                string last_name = collection["last_name"].Trim();
                string birthDay = collection["birthDay"].Trim();
                string country = collection["country"].Trim();
                string state = collection["state"].Trim();
                string access_level = collection["access_level"].Trim();
                PLUser newUser = new PLUser();
                newUser.email = email;
                newUser.password = password;
                newUser.first_name = first_name;
                newUser.last_name = last_name;
                newUser.birthDay = birthDay;
                newUser.country = country;
                if (state == null || state == "")
                {
                    newUser.state = "NA";
                }
                else
                {
                    newUser.state = collection["state"];
                }
                newUser.access_level = access_level;
                UserClientService.InsertUser(newUser);

                return RedirectToAction("Index", "Login");
            }
            catch
            {
                return View("Index");
            }
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            Session["access_level"] = null;
            Session["id"] = null;
            Session["email"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}
