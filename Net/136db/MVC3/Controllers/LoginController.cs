﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC3.Models; // this is required...

namespace MVC3.Controllers
{
	public class LoginController : Controller
	{
		//
		// GET: /Login/
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Index(FormCollection collection)
		{
			try
			{
                string email = collection["email"].Trim();
                string password = collection["password"].Trim();
				PLLogon logon = LogonClientService.Validate(email, password);

				if (logon.id.Equals("-1"))
				{
					// ViewBag is a way to pass info from controller to the view page.
					ViewBag.message = "Invalid login";
				}
				else
				{
					Session["access_level"] = logon.access_level;
					Session["id"] = logon.id;
					Session["email"] = email;

					if (Session["access_level"].Equals("1"))
					{
						return RedirectToAction("Index", "Admin");
					}
                    else if (Session["access_level"].Equals("0"))
					{
						return RedirectToAction("Index", "User", null);
					}
				}
			}
			catch
			{
				return View("Index");
			}
			return View("Index");
		}

		public ActionResult LogOff()
		{
            Session["id"] = null;
            Session["access_level"] = null;
            Session["search_term"] = null;
            Session["search_by"] = null;
            Session["email"] = null;
			return RedirectToAction("Index", "Home");
		}
    }
}
