﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC3.Models;

namespace MVC3.Controllers
{
  public class UserController : Controller
  {
      private static int nameSort = 0;
      private static int artistSort = 0;
      private static int idSort = 0;

      public ActionResult UserIndex(PLUser user)
      {
          List<PLSong> songList = SongClientService.GetSongsByUser(user);
          return View("Index", songList);
      }

      public ActionResult Index()
      {
          nameSort = 0;
          artistSort = 0;
          idSort = 0;
          PLUser user = UserClientService.GetUserDetail(Session["id"].ToString());
          List<PLSong> songList = SongClientService.GetSongsByUser(user);
          return View(songList);
      }

      public ActionResult Details()
      {
          PLUser user = UserClientService.GetUserDetail(Session["id"].ToString());
          return View(user);
      }

      public ActionResult GetUserDetails(PLUser user)
      {
          return View("Details", user);
      }

      public ActionResult UpdateUserDetails(PLUser user)
      {
          return View("UpdateDetails", user);
      }

      public ActionResult UpdateDetails()
      {
          PLUser user = UserClientService.GetUserDetail(Session["id"].ToString());
          return View(user);
      }

      public ActionResult SortBySongName()
      {
          List<PLSong> songs = new List<PLSong>();
          if (Session["access_level"].ToString() == "1")
          {
              PLUser user = UserClientService.GetUserDetail(Request.QueryString["user_id"].ToString());
              songs = SongClientService.GetSongsByUser(user);
          }
          else
          {
              PLUser user = UserClientService.GetUserDetail(Session["id"].ToString());
              songs = SongClientService.GetSongsByUser(user);
          }

          if (artistSort >= 1 || idSort >= 1)
          {
              artistSort = 0;
              idSort = 0;
              nameSort = 1;
              List<PLSong> sortList = songs.OrderBy(x => x.name).ToList();
              return View("Index", sortList);
          }
          else
          {
              if (nameSort == 1)
              {
                  nameSort = 2;
                  List<PLSong> sortList = songs.OrderByDescending(x => x.name).ToList();
                  return View("Index", sortList);
              }
              else
              {
                  nameSort = 1;
                  List<PLSong> sortList = songs.OrderBy(x => x.name).ToList();
                  return View("Index", sortList);
              }
          }
      }

      public ActionResult SortBySongId()
      {
          List<PLSong> songs = new List<PLSong>();
          if (Session["access_level"].ToString() == "1")
          {
              PLUser user = UserClientService.GetUserDetail(Request.QueryString["user_id"].ToString());
              songs = SongClientService.GetSongsByUser(user);
          }
          else
          {
              PLUser user = UserClientService.GetUserDetail(Session["id"].ToString());
              songs = SongClientService.GetSongsByUser(user);
          }

          if (artistSort >= 1 || nameSort >= 1)
          {
              artistSort = 0;
              nameSort = 0;
              idSort = 1;
              List<PLSong> sortList = songs.OrderBy(x => x.id).ToList();
              return View("Index", sortList);
          }
          else
          {
              if (idSort == 1)
              {
                  idSort = 2;
                  List<PLSong> sortList = songs.OrderByDescending(x => x.id).ToList();
                  return View("Index", sortList);
              }
              else
              {
                  idSort = 1;
                  List<PLSong> sortList = songs.OrderBy(x => x.id).ToList();
                  return View("Index", sortList);
              }
          }
      }

      public ActionResult SortBySongArtist()
      {
          List<PLSong> songs = new List<PLSong>();
          if (Session["access_level"].ToString() == "1")
          {
              PLUser user = UserClientService.GetUserDetail(Request.QueryString["user_id"].ToString());
              songs = SongClientService.GetSongsByUser(user);
          }
          else
          {
              PLUser user = UserClientService.GetUserDetail(Session["id"].ToString());
              songs = SongClientService.GetSongsByUser(user);
          }

          if (nameSort >= 1 || idSort >= 1)
          {
              nameSort = 0;
              idSort = 0;
              artistSort = 1;
              List<PLSong> sortList = songs.OrderBy(x => x.artist_name).ToList();
              return View("Index", sortList);
          }
          else
          {
              if (artistSort == 1)
              {
                  artistSort = 2;
                  List<PLSong> sortList = songs.OrderByDescending(x => x.artist_name).ToList();
                  return View("Index", sortList);
              }
              else
              {
                  artistSort = 1;
                  List<PLSong> sortList = songs.OrderBy(x => x.artist_name).ToList();
                  return View("Index", sortList);
              }
          }
      }

      [HttpPost]
      public ActionResult UpdateDetails(FormCollection collection)
      {
          PLUser updateUser = new PLUser();
          updateUser.id = collection["user_id"].ToString();
          updateUser.access_level = collection["access_level"].ToString();
          updateUser.email = collection["email"].Trim();
          updateUser.password = collection["password"].Trim();
          updateUser.first_name = collection["first_name"].Trim();
          updateUser.last_name = collection["last_name"].Trim();
          updateUser.birthDay = collection["birthDay"].Trim();
          updateUser.country = collection["country"].Trim();
          if (collection["state"] == null)
          {
              updateUser.state = collection["state"].Trim();
          }
          else
          {
              updateUser.state = "NONE";
          }
          updateUser.access_level = Session["access_level"].ToString();

          UserClientService.UpdateUser(updateUser);
          return RedirectToAction("Index", "User");
      }

  }
}

