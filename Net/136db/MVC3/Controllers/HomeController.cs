﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC3.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult LogIn()
    {
      return View();
    }

    public ActionResult CreateAccount()
    {
        return View();
    }

    public ActionResult Index()
    {
      return View();
    }
  }

}
