﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using MVC3.Models;

namespace MVC3.Controllers
{

    public class SongController : Controller
    {
        private static int nameSort = 0;
        private static int idSort = 0;
        private static int artistSort = 0;

        public ActionResult Index()
        {
            string id = Request.QueryString["song_id"];
            PLSong song = SongClientService.GetSongDetail(id);
            return View(song);
        }

        public ActionResult UpdateDetails()
        {
            string id = Request.QueryString["song_id"];
            PLSong song = SongClientService.GetSongDetail(id);
            return View(song);
        }

        public ActionResult Upload()
        {
            return View();
        }

        public ActionResult Search()
        {
            Session["search_term"] = null;
            Session["search_by"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Search(FormCollection collection)
        {
            string search_term = collection["search_term"].ToString().Trim();
            string search_by = collection["search_by"].ToString().Trim();
            List<PLSong> songList = new List<PLSong>();

            if (search_by == "user")
            {
                songList = SongClientService.GetSongsByUser(search_term);
            }
            else if (search_by == "artist")
            {
                songList = SongClientService.GetSongsByArtist(search_term);
            }
            else if (search_by == "genre")
            {
                songList = SongClientService.GetSongsByGenre(search_term);
            }
            else if (search_by == "album")
            {
                songList = SongClientService.GetSongsByAlbum(search_term);
            }
            else
            {
                songList = SongClientService.GetSongsByYear(search_term);
            }

            Session["search_term"] = search_term;
            Session["search_by"] = search_by;

            return View("SearchResults", songList);
        }



        public ActionResult Delete()
        {
            string song_id = Request.QueryString["song_id"].ToString();
            SongClientService.DeleteSong(song_id);

            string[] files = Directory.GetFiles(Server.MapPath("~/Content/uploads/"), "*", SearchOption.TopDirectoryOnly);
            string path = "";
            for (int x = 0; x < files.Length; x++)
            {
                if (Path.GetFileNameWithoutExtension(files[x]) == song_id)
                {
                    path = files[x];
                }
            }

            System.IO.File.Delete(path);

            if (Session["access_level"].ToString() == "0")
            {
                return RedirectToAction("Index", "User");
            }
            if (Session["access_level"].ToString() == "1")
            {
                return RedirectToAction("Index", "Admin");
            }
            return RedirectToAction("LogOff", "Login");
        }

        [HttpPost]
        public ActionResult Upload(FormCollection collection, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                string user_id = Session["id"].ToString();
                PLUser songUser = UserClientService.GetUserDetail(user_id);

                string name = collection["name"].Trim();
                string artist_name = collection["artist_name"].Trim();
                string album_name = collection["album_name"].Trim();
                string album_year = collection["album_year"].Trim();
                string genre_name = collection["genre_name"].Trim();

                PLSong newSong = new PLSong();
                newSong.name = name;
                newSong.artist_name = artist_name;
                newSong.album_name = album_name;
                newSong.album_year = album_year;
                newSong.genre_name = genre_name;
                newSong.user = songUser;
                newSong = SongClientService.InsertSong(newSong);

                string fileNameExtension = Path.GetExtension(file.FileName);
                string path = Path.Combine(Server.MapPath("~/Content/uploads"), (newSong.id + fileNameExtension));
                file.SaveAs(path);
            }

            return RedirectToAction("Index", "User");
        }

        public ActionResult Download()
        {
            string song_id = Request.QueryString["song_id"].ToString();

            SongClientService.IncrementDLCount(song_id);
            string[] files = Directory.GetFiles(Server.MapPath("~/Content/uploads/"), "*", SearchOption.TopDirectoryOnly);
            string path = "";
            for (int x = 0; x < files.Length; x++)
            {
                if (Path.GetFileNameWithoutExtension(files[x]) == song_id)
                {
                    path = files[x];
                }
            }
            return File(path, "Application/" + Path.GetExtension(path).Replace(".", ""), Path.GetFileName(path));
        }

        [HttpPost]
        public ActionResult UpdateDetails(FormCollection collection)
        {
            string id = collection["id"];
            string name = collection["name"].Trim();
            string user_id = collection["user_id"];
            string artist_name = collection["artist_name"].Trim();
            PLUser songUser = UserClientService.GetUserDetail(user_id);
            string album_name = collection["album_name"].Trim();
            string album_year = collection["album_year"].Trim();
            string genre_name = collection["genre_name"].Trim();
            string download_count = collection["download_count"];
            PLSong updatedSong = new PLSong();
            updatedSong.id = id;
            updatedSong.name = name;
            updatedSong.artist_name = artist_name;
            updatedSong.album_name = album_name;
            updatedSong.album_year = album_year;
            updatedSong.genre_name = genre_name;
            updatedSong.download_count = download_count;
            updatedSong.user = songUser;
            SongClientService.UpdateSong(updatedSong);

            return RedirectToAction("Index", "Song", new { song_id = Request.QueryString["song_id"] });
        }

        public ActionResult SortBySongName()
        {
            string search_term = Session["search_term"].ToString().Trim();
            string search_by = Session["search_by"].ToString().Trim();
            List<PLSong> songList = new List<PLSong>();

            if (search_by == "user")
            {
                songList = SongClientService.GetSongsByUser(search_term);
            }
            else if (search_by == "artist")
            {
                songList = SongClientService.GetSongsByArtist(search_term);
            }
            else if (search_by == "genre")
            {
                songList = SongClientService.GetSongsByGenre(search_term);
            }
            else if (search_by == "album")
            {
                songList = SongClientService.GetSongsByAlbum(search_term);
            }
            else
            {
                songList = SongClientService.GetSongsByYear(search_term);
            }

            if (artistSort >= 1 || idSort >= 1)
            {
                artistSort = 0;
                idSort = 0;
                nameSort = 1;
                List<PLSong> sortList = songList.OrderBy(x => x.name).ToList();
                return View("SearchResults", sortList);
            }
            else
            {
                if (nameSort == 1)
                {
                    nameSort = 2;
                    List<PLSong> sortList = songList.OrderByDescending(x => x.name).ToList();
                    return View("SearchResults", sortList);
                }
                else
                {
                    nameSort = 1;
                    List<PLSong> sortList = songList.OrderBy(x => x.name).ToList();
                    return View("SearchResults", sortList);
                }
            }
        }

        public ActionResult SortBySongId()
        {
            string search_term = Session["search_term"].ToString().Trim();
            string search_by = Session["search_by"].ToString().Trim();
            List<PLSong> songList = new List<PLSong>();

            if (search_by == "user")
            {
                songList = SongClientService.GetSongsByUser(search_term);
            }
            else if (search_by == "artist")
            {
                songList = SongClientService.GetSongsByArtist(search_term);
            }
            else if (search_by == "genre")
            {
                songList = SongClientService.GetSongsByGenre(search_term);
            }
            else if (search_by == "album")
            {
                songList = SongClientService.GetSongsByAlbum(search_term);
            }
            else
            {
                songList = SongClientService.GetSongsByYear(search_term);
            }

            if (artistSort >= 1 || nameSort >= 1)
            {
                artistSort = 0;
                nameSort = 0;
                idSort = 1;
                List<PLSong> sortList = songList.OrderBy(x => x.id).ToList();
                return View("SearchResults", sortList);
            }
            else
            {
                if (idSort == 1)
                {
                    idSort = 2;
                    List<PLSong> sortList = songList.OrderByDescending(x => x.id).ToList();
                    return View("SearchResults", sortList);
                }
                else
                {
                    idSort = 1;
                    List<PLSong> sortList = songList.OrderBy(x => x.id).ToList();
                    return View("SearchResults", sortList);
                }
            }
        }

        public ActionResult SortBySongArtist()
        {
            string search_term = Session["search_term"].ToString().Trim();
            string search_by = Session["search_by"].ToString().Trim();
            List<PLSong> songList = new List<PLSong>();

            if (search_by == "user")
            {
                songList = SongClientService.GetSongsByUser(search_term);
            }
            else if (search_by == "artist")
            {
                songList = SongClientService.GetSongsByArtist(search_term);
            }
            else if (search_by == "genre")
            {
                songList = SongClientService.GetSongsByGenre(search_term);
            }
            else if (search_by == "album")
            {
                songList = SongClientService.GetSongsByAlbum(search_term);
            }
            else
            {
                songList = SongClientService.GetSongsByYear(search_term);
            }

            if (nameSort >= 1 || idSort >= 1)
            {
              nameSort = 0;
              idSort = 0;
              artistSort = 1;
              List<PLSong> sortList = songList.OrderBy(x => x.artist_name).ToList();
              return View("SearchResults", sortList);
            }
            else
            {
              if (artistSort == 1)
              {
                  artistSort = 2;
                  List<PLSong> sortList = songList.OrderByDescending(x => x.artist_name).ToList();
                  return View("SearchResults", sortList);
              }
              else
              {
                  artistSort = 1;
                  List<PLSong> sortList = songList.OrderBy(x => x.artist_name).ToList();
                  return View("SearchResults", sortList);
              }
          }
      }
    }

         
}

