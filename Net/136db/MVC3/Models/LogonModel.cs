﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC3.Models
{
  public class PLLogon
  {
    public string id { get; set; }

    public string access_level { get; set; }

    [Required]
    [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$")]
    public string email { get; set; }
    
    [Required]
    public string password { get; set; }
  }

  public static class LogonClientService
  {
    public static PLLogon Validate(string email, string password)
    {
      SLAuth.ISLAuth AuthClient = new SLAuth.SLAuthClient();
      string[] errors = new string[0];

      SLAuth.Logon logon = AuthClient.Authenticate(email, password, ref errors);

      PLLogon PLLogon = DTO_to_PL(logon);
      return PLLogon;
    }

    private static PLLogon DTO_to_PL(SLAuth.Logon logon)
    {
      PLLogon PLLogon = new PLLogon();
      PLLogon.id = logon.id;
      PLLogon.access_level = logon.access_level;

      return PLLogon;
    }

  }
}