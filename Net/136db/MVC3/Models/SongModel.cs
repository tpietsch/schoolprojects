﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC3.Models
{
  public class PLSong
  {
     [Required]
     [DisplayName("Search")]
     public string search_term { get; set; }

    [Required]
    [DisplayName("ID")]
    public string id { get; set; }

    [Required]
    [DisplayName("Name")]
    public string name { get; set; }

    [Required]
    [DisplayName("Artist Name")]
    public string artist_name { get; set; }

    [Required]
    [DisplayName("Album Name")]
    public string album_name { get; set; }

    [Required]
    [DisplayName("Album Year")]
    [RegularExpression("[0-9][0-9][0-9][0-9]", ErrorMessage="The album year must be a valid 4-digit year")]
    public string album_year { get; set; }

    [Required]
    [DisplayName("Genre Name")]
    public string genre_name { get; set; }

    [Required]
    [DisplayName("Download Count")]
    public string download_count { get; set; }

    [Required]
    [DisplayName("User")]
    public PLUser user { get; set; }

  }

  public static class SongClientService
  {
    public static PLSong GetSongDetail(string id)
    {
        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];

        SLSong.Song song = SLSong.GetSongDetails(id, ref errors);

        return DTO_to_PL(song);
    }

    public static PLSong InsertSong(PLSong song)
    {
        SLSong.Song newSong = DTO_to_SL(song);

        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];

        newSong = SLSong.InsertSong(newSong, ref errors);

        return DTO_to_PL(newSong);
    }

    public static void UpdateSong(PLSong song)
    {
        SLSong.Song updatedSong = DTO_to_SL(song);

        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];

        SLSong.UpdateSong(updatedSong, ref errors);
    }

    public static void DeleteSong(string id)
    {
        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];

        SLSong.DeleteSong(id, ref errors);
    }

    public static void IncrementDLCount(string id)
    {
        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];

        SLSong.IncrementDLCount(id, ref errors);
    }

    public static int GetDLCount(string id)
    {
        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];

        return SLSong.GetDLCount(id, ref errors);
    }

    public static List<PLSong> GetSongsByName(string name)
    {
        List<PLSong> songList = new List<PLSong>();

        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];
        SLSong.Song[] slSongList = SLSong.GetSongsByName(name, ref errors);

        if (slSongList != null && slSongList.Length > 0)
        {
            foreach (SLSong.Song s in slSongList)
            {
                PLSong song = DTO_to_PL(s);
                songList.Add(song);
            }

            return songList;
        }
        return new List<PLSong>();
    }

    public static List<PLSong> GetSongsByUser(string email)
    {
        List<PLSong> songList = new List<PLSong>();

        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];
        SLSong.Song[] slSongList = SLSong.GetSongsByUserEmail(email, ref errors);

        if (slSongList != null && slSongList.Length > 0)
        {
            foreach (SLSong.Song s in slSongList)
            {
                PLSong song = DTO_to_PL(s);
                songList.Add(song);
            }

            return songList;
        }
        return new List<PLSong>();
    }

    public static List<PLSong> GetSongsByUser(PLUser user)
    {
        List<PLSong> songList = new List<PLSong>();

        SLSong.ISLSong SLSong = new SLSong.SLSongClient();



        string[] errors = new string[0];
        SLSong.Song[] slSongList = SLSong.GetSongsByUser(DTO_to_SL(user), ref errors);

        if (slSongList != null && slSongList.Length > 0)
        {
            foreach (SLSong.Song s in slSongList)
            {
                PLSong song = DTO_to_PL(s);
                songList.Add(song);
            }

            return songList;
        }
        return new List<PLSong>();
    }

    public static List<PLSong> GetSongsByGenre(string genre)
    {
        List<PLSong> songList = new List<PLSong>();

        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];
        SLSong.Song[] slSongList = SLSong.GetSongsByGenre(genre, ref errors);

        if (slSongList != null && slSongList.Length > 0)
        {
            foreach (SLSong.Song s in slSongList)
            {
                PLSong song = DTO_to_PL(s);
                songList.Add(song);
            }

            return songList;
        }
        return new List<PLSong>();
    }

    public static List<PLSong> GetSongsByArtist(string artist)
    {
        List<PLSong> songList = new List<PLSong>();

        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];
        SLSong.Song[] slSongList = SLSong.GetSongsByArtist(artist, ref errors);

        if (slSongList != null && slSongList.Length > 0)
        {
            foreach (SLSong.Song s in slSongList)
            {
                PLSong song = DTO_to_PL(s);
                songList.Add(song);
            }

            return songList;
        }
        return new List<PLSong>();
    }

    public static List<PLSong> GetSongsByYear(string year)
    {
        List<PLSong> songList = new List<PLSong>();

        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];
        SLSong.Song[] slSongList = SLSong.GetSongsByYear(year, ref errors);

        if (slSongList != null && slSongList.Length > 0)
        {
            foreach (SLSong.Song s in slSongList)
            {
                PLSong song = DTO_to_PL(s);
                songList.Add(song);
            }

            return songList;
        }
        return new List<PLSong>();
    }

    public static List<PLSong> GetSongsByAlbum(string album)
    {
        List<PLSong> songList = new List<PLSong>();

        SLSong.ISLSong SLSong = new SLSong.SLSongClient();

        string[] errors = new string[0];
        SLSong.Song[] slSongList = SLSong.GetSongsByAlbum(album, ref errors);

        if (slSongList != null && slSongList.Length > 0)
        {
            foreach (SLSong.Song s in slSongList)
            {
                PLSong song = DTO_to_PL(s);
                songList.Add(song);
            }

            return songList;
        }
        return new List<PLSong>();
    }

    public static PLSong DTO_to_PL(SLSong.Song s)
    {
      PLSong mySong = new PLSong();

      if (s.id != null)
      {
          mySong.id = s.id;
      }
      mySong.name = s.name;
      mySong.artist_name = s.artist_name;
      mySong.album_name = s.album_name;
      mySong.album_year = s.album_year;
      mySong.genre_name = s.genre_name;
      mySong.download_count = s.download_count;
      mySong.user = DTO_to_PL(s.user);

      return mySong;
    }

    public static PLUser DTO_to_PL(SLSong.User u)
    {
        PLUser myUser = new PLUser();

        myUser.id = u.id;
        myUser.first_name = u.first_name;
        myUser.last_name = u.last_name;
        myUser.country = u.country;
        myUser.state = u.state;
        myUser.email = u.email;
        myUser.password = u.password;
        myUser.birthDay = u.birthDay;
        myUser.access_level = u.access_level;

        return myUser;
    }

    public static SLSong.Song DTO_to_SL(PLSong s)
    {
        SLSong.Song mySong = new SLSong.Song();

        if (s.id != null)
        {
            mySong.id = s.id;
        }
        mySong.name = s.name;
        mySong.artist_name = s.artist_name;
        mySong.album_name = s.album_name;
        mySong.album_year = s.album_year;
        mySong.genre_name = s.genre_name;
        mySong.download_count = s.download_count;
        mySong.user = DTO_to_SL(s.user);

        return mySong;
    }

    public static SLSong.User DTO_to_SL(PLUser u)
    {
        SLSong.User myUser = new SLSong.User();

        myUser.id = u.id;
        myUser.first_name = u.first_name;
        myUser.last_name = u.last_name;
        myUser.country = u.country;
        myUser.state = u.state;
        myUser.birthDay = u.birthDay;
        myUser.email = u.email;
        myUser.password = u.password;
        myUser.access_level = u.access_level;

        return myUser;
    }
  }
}