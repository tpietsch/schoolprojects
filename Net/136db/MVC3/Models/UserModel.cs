﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System;

namespace MVC3.Models
{
  public class PLUser
  {
    [DisplayName("User ID")]
    public string id { get; set; }

    [Required]
    [DisplayName("First Name")]
    [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Your first name must consist of only letters.")]
    public string first_name { get; set; }

    [Required]
    [DisplayName("Last Name")]
    [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Your last name must consist of only letters.")]
    public string last_name { get; set; }

    [Required]
    [DisplayName("Country")]
    [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Your country must consist of only letters.")]
    public string country { get; set; }

    [DisplayName("State")]
    [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Your state must consist of only letters.")]
    public string state { get; set; }

    [Required]
    [DisplayName("Birth Date")]
    [RegularExpression(@"((0[13578]|1[02])/(0[1-9]|[12]\d|3[01])/\d{4})|((0[469]|11])/(0[1-9]|[12]\d|30)/\d{4})|((02)/(0[1-9]|1\d|2\d)/\d\d[0248][048]|\d\d[13579][26])|((02)/(0[1-9]|1\d|2[0-8]/\d\d[0248][1235679]|\d\d[13579][01345789]))", ErrorMessage = "Your Birth Date must consist of only numbers and must follow the format MM/DD/YYYY")]
    public string birthDay { get; set; }

    [Required]
    [DisplayName("E-mail")]
    [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage="Your e-mail must be valid.")]
    public string email { get; set; }

    [Required]
    [DisplayName("Password")]
    [ScaffoldColumn(false)] // don't show this in <%: Html.DisplayForModel() %>
    public string password { get; set; }
    
    [Required]
    [DisplayName("Access Level")]
    [RegularExpression("[01]", ErrorMessage="Enter 0 for user access, or enter 1 for admin access.")]
    public string access_level { get; set; }
  }

  public static class UserClientService
  {
    public static List<PLUser> GetUserList()
    {
      List<PLUser> userList = new List<PLUser>();

      SLUser.ISLUser SLUser = new SLUser.SLUserClient();

      string[] errors = new string[0];
      SLUser.User[] usersLoaded = SLUser.GetUserList(ref errors);

      foreach (SLUser.User u in usersLoaded)
      {
        PLUser user = DTO_to_PL(u);
        userList.Add(user);
      }

      return userList;
    }

    /// <summary>
    /// create a new User
    /// </summary>
    /// <param name="s"></param>
    public static void InsertUser(PLUser u)
    {
      SLUser.User newUser = DTO_to_SL(u);

      SLUser.ISLUser SLUserClient = new SLUser.SLUserClient();
      string[] errors = new string[0];
      SLUserClient.InsertUser(newUser, ref errors);
    }

    /// <summary>
    /// update existing User info
    /// </summary>
    /// <param name="s"></param>
    public static void UpdateUser(PLUser u)
    {
      SLUser.User updatedUser = DTO_to_SL(u);

      SLUser.ISLUser SLUser = new SLUser.SLUserClient();
      string[] errors = new string[0];
      SLUser.UpdateUser(updatedUser, ref errors);
    }

    /// <summary>
    /// Get User detail
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static PLUser GetUserDetail(string id)
    {
      SLUser.ISLUser SLUser = new SLUser.SLUserClient();

      string[] errors = new string[0];
      SLUser.User newUser = SLUser.GetUser(id, ref errors);

      // this is the data transfer object code...
      return DTO_to_PL(newUser);
    }

    /// <summary>
    /// call service layer's delete User method
    /// </summary>
    /// <param name="id"></param>
    public static void DeleteUser(string id)
    {
      SLUser.ISLUser SLUser = new SLUser.SLUserClient();
      string[] errors = new string[0];
      SLUser.DeleteUser(id, ref errors);
    }

    /// <summary>
    /// This is the data transfer object for user.
    /// Converting business layer user object to presentation layer user object
    /// </summary>
    /// <param name="User"></param>
    /// <returns></returns>
    private static PLUser DTO_to_PL(SLUser.User user)
    {
      PLUser PLUser = new Models.PLUser();
      PLUser.id = user.id;
      PLUser.first_name = user.first_name;
      PLUser.last_name = user.last_name;
      PLUser.country = user.country;
      PLUser.state = user.state;
      PLUser.email = user.email;
      PLUser.birthDay = user.birthDay;
      PLUser.password = user.password;
      PLUser.access_level = user.access_level;

      return PLUser;
    }

    /// <summary>
    /// this is data transfer object for User.
    /// Converting from presentation layer User object to business layer User object
    /// </summary>
    /// <param name="User"></param>
    /// <returns></returns>
    private static SLUser.User DTO_to_SL(PLUser user)
    {
      SLUser.User SLUser = new MVC3.SLUser.User();
      if (user.id != null)
      {
          SLUser.id = user.id;
      }
      SLUser.first_name = user.first_name;
      SLUser.last_name = user.last_name;
      SLUser.country = user.country;
      SLUser.birthDay = user.birthDay;
      SLUser.state = user.state;
      SLUser.email = user.email;
      SLUser.password = user.password;
      SLUser.access_level = user.access_level;

      return SLUser;
    }
  }
}