﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using DomainModel;

namespace DALTest
{
  [TestClass]
  public class DALSongTest
  {
    [TestMethod]
    public void InsertSongTest()
    {
        //initialize our test by clearing the database of all data
        //initializing our next_id_table and adding a user to our database
        List<string> errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();

        //create a new user object to add to the table
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";

        //call the insert method on our user - note that we reassign our
        //user to the result of this method so that the user object is
        //updated with the id that the user has been assigned in the database
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);
        Assert.AreEqual(0, errors.Count);

        //create our test song to add to the database.
        Song song = new Song();
        song.album_name = "album test";
        song.album_year = "2012";
        song.genre_name = "genre test";
        song.artist_name = "artist test";
        song.name = "song name test";
        song.user = user;

        errors = new List<string>();

        //insert our song into the database
        song = DALSong.InsertSong(song, ref errors);
        Assert.AreEqual(0, errors.Count);

        errors = new List<string>();

        //here, assuming our accessor method works, we retrieve the song from the database under
        //a different variable name
        Song songVerify= DALSong.GetSongDetail(song.id, ref errors);
        
        //now we compare our original song with the retrieved song to ensure that all the fields match.
        Assert.AreEqual(0, errors.Count);
        Assert.AreEqual(song.album_name,songVerify.album_name);
        Assert.AreEqual(song.album_year, songVerify.album_year);
        Assert.AreEqual(song.artist_name, songVerify.artist_name);
        Assert.AreEqual(song.download_count,songVerify.download_count);
        Assert.AreEqual(song.genre_name, songVerify.genre_name);
        Assert.AreEqual(song.id,songVerify.id);
        Assert.AreEqual(song.name,songVerify.name);
        Assert.AreEqual(song.user.id, songVerify.user.id);

        errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();
    }

    [TestMethod]
    public void UpdateSongTest()
    {
        //initialize our test by clearing the database of all data
        //initializing our next_id_table and adding a user to our database
        List<string> errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();

        //create two new user objects to add to the table
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";

        User user2 = new User();
        user2.first_name = "first2";
        user2.last_name = " last2";
        user2.birthDay = "12/30/2005 12:00:00 AM";
        user2.state = "";
        user2.country = "Spain";
        user2.access_level = "2";
        user2.password = "pass12345";
        user2.email = "m@www.com";

        //call the insert method on our user - note that we reassign our
        //user to the result of this method so that the user object is
        //updated with the id that the user has been assigned in the database
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);
        user2 = DALUser.InsertUser(user2, ref errors);
        Assert.AreEqual(0, errors.Count);

        //create our test song to add to the database.
        Song song = new Song();
        song.album_name = "album test";
        song.album_year = "2012";
        song.genre_name = "genre test";
        song.artist_name = "artist test";
        song.name = "song name test";
        song.user = user;

        errors = new List<string>();

        //insert our song into the database
        song = DALSong.InsertSong(song, ref errors);

        //now, let's update the song by changing the fields
        //and then calling the updatesong method.
        song.album_name = "album test2";
        song.album_year = "2011";
        song.genre_name = "genre test2";
        song.artist_name = "artist test2";
        song.name = "song name test2";
        song.user = user;

        errors = new List<string>();
        song = DALSong.UpdateSong(song, ref errors);

        //now, let's retrieve this updated song from our database
        //under the songVerify object and ensure that the fields match
        //our updated song.
        Song songVerify = DALSong.GetSongDetail(song.id, ref errors);
        Assert.AreEqual(0, errors.Count);
        Assert.AreEqual(song.album_name, songVerify.album_name);
        Assert.AreEqual(song.album_year, songVerify.album_year);
        Assert.AreEqual(song.artist_name, songVerify.artist_name);
        Assert.AreEqual(song.download_count, songVerify.download_count);
        Assert.AreEqual(song.genre_name, songVerify.genre_name);
        Assert.AreEqual(song.id, songVerify.id);
        Assert.AreEqual(song.name, songVerify.name);
        Assert.AreEqual(song.user.id, songVerify.user.id);

        errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();
    }

    [TestMethod]
    public void DeleteSongTest()
    {
        //initialize our test by clearing the database of all data
        //initializing our next_id_table and adding a user to our database
        List<string> errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();

        //create a new user object to add to the table
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";

        //call the insert method on our user - note that we reassign our
        //user to the result of this method so that the user object is
        //updated with the id that the user has been assigned in the database
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);
        Assert.AreEqual(0, errors.Count);

        //create our test song to add to the database.
        Song song = new Song();
        song.album_name = "album test";
        song.album_year = "2012";
        song.genre_name = "genre test";
        song.artist_name = "artist test";
        song.name = "song name test";
        song.user = user;

        errors = new List<string>();

        //insert our song into the database
        song = DALSong.InsertSong(song, ref errors);
        Assert.AreEqual(0, errors.Count);

        errors = new List<string>();

        //now, let's delete that song from our database
        DALSong.DeleteSong(song.id, ref errors);
        Assert.AreEqual(0, errors.Count);

        errors = new List<string>();

        //let's attempt to retrieve the song we've just deleted from our database.
        //this should return a null object assuming our accessor method works as intended.
        Song verifySong = DALSong.GetSongDetail(song.id, ref errors);
        Assert.AreEqual(0, errors.Count);

        //finally, we check to see if verifySong object is equal to null - if so, this test passes.
        Assert.AreEqual(null, verifySong);

        errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();
    }
    
    [TestMethod]
    public void GetSongsByGenreTest()
    {
        //initialize our test by clearing the database of all data
        //initializing our next_id_table and adding a user to our database
        List<string> errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();

        //create two new user objects to add to the table
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";

        User user2 = new User();
        user2.first_name = "first2";
        user2.last_name = " last2";
        user2.birthDay = "12/30/2005 12:00:00 AM";
        user2.state = "";
        user2.country = "Spain";
        user2.access_level = "2";
        user2.password = "pass12345";
        user2.email = "m@www.com";

        //call the insert method on our user - note that we reassign our
        //user to the result of this method so that the user object is
        //updated with the id that the user has been assigned in the database
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);
        user2 = DALUser.InsertUser(user2, ref errors);
        Assert.AreEqual(0, errors.Count);
       
        //now let's create three songs and insert them into our database.
        Song song = new Song();
        song.album_name = "album test1";
        song.album_year = "2012";
        song.genre_name = "genre test";
        song.artist_name = "artist test1";
        song.name = "song name test1";
        song.user = user;

        Song song2 = new Song();
        song2.album_name = "album test2";
        song2.album_year = "2011";
        song2.genre_name = "genre test";
        song2.artist_name = "artist test2";
        song2.name = "song name test2";
        song2.user = user2;

        Song song3 = new Song();
        song3.album_name = "album test3";
        song3.album_year = "2010";
        song3.genre_name = "genre test3";
        song3.artist_name = "artist test3";
        song3.name = "song name test3";
        song3.user = user;

        song = DALSong.InsertSong(song, ref errors);
        song2 = DALSong.InsertSong(song2, ref errors);
        song3 = DALSong.InsertSong(song3, ref errors);
        Assert.AreEqual(0, errors.Count);

        //now, let's call our GetSongsByGenre method and ensure that 
        //the number of songs and each song retrieved holds the expected information.
        List<Song> genreList = DALSong.GetSongsByGenre("genre test", ref errors);
        Assert.AreEqual(2, genreList.Count);
        Assert.AreEqual(song.name, genreList[0].name);
        Assert.AreEqual(song.artist_name, genreList[0].artist_name);
        Assert.AreEqual(song.genre_name, genreList[0].genre_name);
        Assert.AreEqual(song.album_name, genreList[0].album_name);
        Assert.AreEqual(song.album_year, genreList[0].album_year);
        Assert.AreEqual(song.download_count, genreList[0].download_count);
        Assert.AreEqual(song.user.id, genreList[0].user.id);
        Assert.AreEqual(song2.name, genreList[1].name);
        Assert.AreEqual(song2.artist_name, genreList[1].artist_name);
        Assert.AreEqual(song2.genre_name, genreList[1].genre_name);
        Assert.AreEqual(song2.album_name, genreList[1].album_name);
        Assert.AreEqual(song2.album_year, genreList[1].album_year);
        Assert.AreEqual(song2.download_count, genreList[1].download_count);
        Assert.AreEqual(song2.user.id, genreList[1].user.id);

        errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();
    }

    [TestMethod]
    public void GetSongsByAlbumTest()
    {
        //initialize our test by clearing the database of all data
        //initializing our next_id_table and adding a user to our database
        List<string> errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();

        //create two new user objects to add to the table
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";

        User user2 = new User();
        user2.first_name = "first2";
        user2.last_name = " last2";
        user2.birthDay = "12/30/2005 12:00:00 AM";
        user2.state = "";
        user2.country = "Spain";
        user2.access_level = "2";
        user2.password = "pass12345";
        user2.email = "m@www.com";

        //call the insert method on our user - note that we reassign our
        //user to the result of this method so that the user object is
        //updated with the id that the user has been assigned in the database
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);
        user2 = DALUser.InsertUser(user2, ref errors);
        Assert.AreEqual(0, errors.Count);
       
        //now let's create three songs and insert them into our database.
        Song song = new Song();
        song.album_name = "album test";
        song.album_year = "2012";
        song.genre_name = "genre test1";
        song.artist_name = "artist test1";
        song.name = "song name test1";
        song.user = user;

        Song song2 = new Song();
        song2.album_name = "album test2";
        song2.album_year = "2011";
        song2.genre_name = "genre test";
        song2.artist_name = "artist test2";
        song2.name = "song name test2";
        song2.user = user2;

        Song song3 = new Song();
        song3.album_name = "album test";
        song3.album_year = "2012";
        song3.genre_name = "genre test3";
        song3.artist_name = "artist test3";
        song3.name = "song name test3";
        song3.user = user;

        song = DALSong.InsertSong(song, ref errors);
        song2 = DALSong.InsertSong(song2, ref errors);
        song3 = DALSong.InsertSong(song3, ref errors);
        Assert.AreEqual(0, errors.Count);

        //now, let's call our GetSongsByAlbum method and ensure that 
        //the number of songs and each song retrieved holds the expected information.
        List<Song> genreList = DALSong.GetSongsByAlbum("album test", ref errors);
        Assert.AreEqual(2, genreList.Count);
        Assert.AreEqual(song.name, genreList[0].name);
        Assert.AreEqual(song.artist_name, genreList[0].artist_name);
        Assert.AreEqual(song.genre_name, genreList[0].genre_name);
        Assert.AreEqual(song.album_name, genreList[0].album_name);
        Assert.AreEqual(song.album_year, genreList[0].album_year);
        Assert.AreEqual(song.download_count, genreList[0].download_count);
        Assert.AreEqual(song.user.id, genreList[0].user.id);
        Assert.AreEqual(song3.name, genreList[1].name);
        Assert.AreEqual(song3.artist_name, genreList[1].artist_name);
        Assert.AreEqual(song3.genre_name, genreList[1].genre_name);
        Assert.AreEqual(song3.album_name, genreList[1].album_name);
        Assert.AreEqual(song3.album_year, genreList[1].album_year);
        Assert.AreEqual(song3.download_count, genreList[1].download_count);
        Assert.AreEqual(song3.user.id, genreList[1].user.id);

        errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();
    }

    [TestMethod]
    public void GetSongsByArtistTest()
    {
        //initialize our test by clearing the database of all data
        //initializing our next_id_table and adding a user to our database
        List<string> errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();

        //create two new user objects to add to the table
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";

        User user2 = new User();
        user2.first_name = "first2";
        user2.last_name = " last2";
        user2.birthDay = "12/30/2005 12:00:00 AM";
        user2.state = "";
        user2.country = "Spain";
        user2.access_level = "2";
        user2.password = "pass12345";
        user2.email = "m@www.com";

        //call the insert method on our user - note that we reassign our
        //user to the result of this method so that the user object is
        //updated with the id that the user has been assigned in the database
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);
        user2 = DALUser.InsertUser(user2, ref errors);
        Assert.AreEqual(0, errors.Count);
       
        //now let's create three songs and insert them into our database.
        Song song = new Song();
        song.album_name = "album test1";
        song.album_year = "2012";
        song.genre_name = "genre test1";
        song.artist_name = "artist test";
        song.name = "song name test1";
        song.user = user;

        Song song2 = new Song();
        song2.album_name = "album test2";
        song2.album_year = "2011";
        song2.genre_name = "genre test2";
        song2.artist_name = "artist test";
        song2.name = "song name test2";
        song2.user = user2;

        Song song3 = new Song();
        song3.album_name = "album test3";
        song3.album_year = "2010";
        song3.genre_name = "genre test3";
        song3.artist_name = "artist test3";
        song3.name = "song name test3";
        song3.user = user;

        song = DALSong.InsertSong(song, ref errors);
        song2 = DALSong.InsertSong(song2, ref errors);
        song3 = DALSong.InsertSong(song3, ref errors);
        Assert.AreEqual(0, errors.Count);

        //now, let's call our GetSongsByArtist method and ensure that 
        //the number of songs and each song retrieved holds the expected information.
        List<Song> genreList = DALSong.GetSongsByArtist("artist test", ref errors);
        Assert.AreEqual(2, genreList.Count);
        Assert.AreEqual(song.name, genreList[0].name);
        Assert.AreEqual(song.artist_name, genreList[0].artist_name);
        Assert.AreEqual(song.genre_name, genreList[0].genre_name);
        Assert.AreEqual(song.album_name, genreList[0].album_name);
        Assert.AreEqual(song.album_year, genreList[0].album_year);
        Assert.AreEqual(song.download_count, genreList[0].download_count);
        Assert.AreEqual(song.user.id, genreList[0].user.id);
        Assert.AreEqual(song2.name, genreList[1].name);
        Assert.AreEqual(song2.artist_name, genreList[1].artist_name);
        Assert.AreEqual(song2.genre_name, genreList[1].genre_name);
        Assert.AreEqual(song2.album_name, genreList[1].album_name);
        Assert.AreEqual(song.album_year, genreList[0].album_year);
        Assert.AreEqual(song2.download_count, genreList[1].download_count);
        Assert.AreEqual(song2.user.id, genreList[1].user.id);

        errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();
    }

    [TestMethod]
    public void GetSongsByNameTest()
    {
        //initialize our test by clearing the database of all data
        //initializing our next_id_table and adding a user to our database
        List<string> errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();

        //create two new user objects to add to the table
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";

        User user2 = new User();
        user2.first_name = "first2";
        user2.last_name = " last2";
        user2.birthDay = "12/30/2005 12:00:00 AM";
        user2.state = "";
        user2.country = "Spain";
        user2.access_level = "2";
        user2.password = "pass12345";
        user2.email = "m@www.com";

        //call the insert method on our user - note that we reassign our
        //user to the result of this method so that the user object is
        //updated with the id that the user has been assigned in the database
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);
        user2 = DALUser.InsertUser(user2, ref errors);
        Assert.AreEqual(0, errors.Count);
       
        //now let's create three songs and insert them into our database.
        Song song = new Song();
        song.album_name = "album test1";
        song.album_year = "2012";
        song.genre_name = "genre test1";
        song.artist_name = "artist test1";
        song.name = "song name test";
        song.user = user;

        Song song2 = new Song();
        song2.album_name = "album test2";
        song2.album_year = "2011";
        song2.genre_name = "genre test2";
        song2.artist_name = "artist test2";
        song2.name = "song name test";
        song2.user = user2;

        Song song3 = new Song();
        song3.album_name = "album test3";
        song3.album_year = "2010";
        song3.genre_name = "genre test3";
        song3.artist_name = "artist test3";
        song3.name = "song name test";
        song3.user = user;

        song = DALSong.InsertSong(song, ref errors);
        song2 = DALSong.InsertSong(song2, ref errors);
        song3 = DALSong.InsertSong(song3, ref errors);
        Assert.AreEqual(0, errors.Count);

        //now, let's call our GetSongsByName method and ensure that 
        //the number of songs and each song retrieved holds the expected information.
        List<Song> genreList = DALSong.GetSongsByName("song name test", ref errors);
        Assert.AreEqual(3, genreList.Count);
        Assert.AreEqual(song.name, genreList[0].name);
        Assert.AreEqual(song.artist_name, genreList[0].artist_name);
        Assert.AreEqual(song.genre_name, genreList[0].genre_name);
        Assert.AreEqual(song.album_name, genreList[0].album_name);
        Assert.AreEqual(song.album_year, genreList[0].album_year);
        Assert.AreEqual(song.download_count, genreList[0].download_count);
        Assert.AreEqual(song.user.id, genreList[0].user.id);
        Assert.AreEqual(song2.name, genreList[1].name);
        Assert.AreEqual(song2.artist_name, genreList[1].artist_name);
        Assert.AreEqual(song2.genre_name, genreList[1].genre_name);
        Assert.AreEqual(song2.album_name, genreList[1].album_name);
        Assert.AreEqual(song.album_year, genreList[0].album_year);
        Assert.AreEqual(song2.download_count, genreList[1].download_count);
        Assert.AreEqual(song2.user.id, genreList[1].user.id);
        Assert.AreEqual(song3.name, genreList[2].name);
        Assert.AreEqual(song3.artist_name, genreList[2].artist_name);
        Assert.AreEqual(song3.genre_name, genreList[2].genre_name);
        Assert.AreEqual(song3.album_name, genreList[2].album_name);
        Assert.AreEqual(song.album_year, genreList[0].album_year);
        Assert.AreEqual(song3.download_count, genreList[2].download_count);
        Assert.AreEqual(song3.user.id, genreList[2].user.id);

        errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();
    }
    
    [TestMethod]
    public void GetSongsByUserTest()
    {
        //initialize our test by clearing the database of all data
        //initializing our next_id_table and adding a user to our database
        List<string> errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();

        //create two new user objects to add to the table
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";

        User user2 = new User();
        user2.first_name = "first2";
        user2.last_name = " last2";
        user2.birthDay = "12/30/2005 12:00:00 AM";
        user2.state = "";
        user2.country = "Spain";
        user2.access_level = "2";
        user2.password = "pass12345";
        user2.email = "m@www.com";

        //call the insert method on our user - note that we reassign our
        //user to the result of this method so that the user object is
        //updated with the id that the user has been assigned in the database
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);
        user2 = DALUser.InsertUser(user2, ref errors);
        Assert.AreEqual(0, errors.Count);
       
        //now let's create three songs and insert them into our database.
        Song song = new Song();
        song.album_name = "album test1";
        song.album_year = "2012";
        song.genre_name = "genre test1";
        song.artist_name = "artist test1";
        song.name = "song name test1";
        song.user = user;

        Song song2 = new Song();
        song2.album_name = "album test2";
        song2.album_year = "2011";
        song2.genre_name = "genre test2";
        song2.artist_name = "artist test2";
        song2.name = "song name test2";
        song2.user = user;

        Song song3 = new Song();
        song3.album_name = "album test3";
        song3.album_year = "2010";
        song3.genre_name = "genre test3";
        song3.artist_name = "artist test3";
        song3.name = "song name test3";
        song3.user = user2;
        
        song = DALSong.InsertSong(song, ref errors);
        song2 = DALSong.InsertSong(song2, ref errors);
        song3 = DALSong.InsertSong(song3, ref errors);
        Assert.AreEqual(0, errors.Count);

        //now, let's call our GetSongsByGenre method and ensure that 
        //the number of songs and each song retrieved holds the expected information.
        List<Song> genreList = DALSong.GetSongsByUser(user.email, ref errors);
        Assert.AreEqual(2, genreList.Count);
        Assert.AreEqual(song.name, genreList[0].name);
        Assert.AreEqual(song.artist_name, genreList[0].artist_name);
        Assert.AreEqual(song.genre_name, genreList[0].genre_name);
        Assert.AreEqual(song.album_name, genreList[0].album_name);
        Assert.AreEqual(song.album_year, genreList[0].album_year);
        Assert.AreEqual(song.download_count, genreList[0].download_count);
        Assert.AreEqual(song.user.id, genreList[0].user.id);
        Assert.AreEqual(song2.name, genreList[1].name);
        Assert.AreEqual(song2.artist_name, genreList[1].artist_name);
        Assert.AreEqual(song2.genre_name, genreList[1].genre_name);
        Assert.AreEqual(song2.album_name, genreList[1].album_name);
        Assert.AreEqual(song.album_year, genreList[0].album_year);
        Assert.AreEqual(song2.download_count, genreList[1].download_count);
        Assert.AreEqual(song2.user.id, genreList[1].user.id);

        errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();
    }

    [TestMethod]
    public void IncrementDLCountTest()
    {
        //initialize our test by clearing the database of all data
        //initializing our next_id_table and adding a user to our database
        List<string> errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();

        //create two new user objects to add to the table
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";

        //call the insert method on our user - note that we reassign our
        //user to the result of this method so that the user object is
        //updated with the id that the user has been assigned in the database
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);
        Assert.AreEqual(0, errors.Count);

        //now let's create a song and insert it into our database.
        Song song = new Song();
        song.album_name = "album test1";
        song.album_year = "2012";
        song.genre_name = "genre test1";
        song.artist_name = "artist test1";
        song.name = "song name test1";
        song.user = user;
        errors = new List<string>();
        song = DALSong.InsertSong(song, ref errors);
        Assert.AreEqual(0, errors.Count);

        //now let's make sure the GetDLCount method works.
        //when a song is added to the database, by default, the download count is 0.
        errors = new List<string>();
        int dlCount = DALSong.GetDLCount(song.id, ref errors);
        Assert.AreEqual(0, errors.Count);
        Assert.AreEqual(0, dlCount);

        //now, let's increment the DL count
        errors = new List<string>();
        DALSong.IncrementDLCount(song.id, ref errors);
        Assert.AreEqual(0, errors.Count);

        //now let's confirm that the DLCount updated.
        errors = new List<string>();
        dlCount = DALSong.GetDLCount(song.id, ref errors);
        Assert.AreEqual(0, errors.Count);
        Assert.AreEqual(1, dlCount);

        errors = new List<string>();
        DALSong.ClearDatabase(ref errors);
        errors = new List<string>();
    }
  }
}
