using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using DomainModel;

namespace DALUserTest
{
  /// <summary>
  /// Summary description for UnitTest1
  /// </summary>
  [TestClass]
  public class DALUserTest
  {
    public DALUserTest()
    {
      //
      // TODO: Add constructor logic here
      //
    }

    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion

    [TestMethod]
    public void InsertUserTest()
    {
      //remove all data from the database and initialize our next_id_table
      List<string> errors = new List<string>();
      DALUser.ClearDatabase(ref errors);
      errors = new List<string>();
        
      //create a new user object to add to the table
      User user = new User();
      user.first_name = "first";
      user.last_name = "last";
      user.birthDay = "1/2/2003 12:00:00 AM";
      user.state = "CA";
      user.country = "USA";
      user.access_level = "1";
      user.password = "pass1234";
      user.email = "miles@123.com";
      
      //call the insert method on our user - note that we reassign our
      //user to the result of this method so that the user object is
      //updated with the id that the user has been assigned in the database
      errors = new List<string>();
      user = DALUser.InsertUser(user, ref errors);

      //check for errors
      Assert.AreEqual(0, errors.Count);
    
      //we assume that our accessor method works - we retrieve the user
      //we just added using its user.id field
      User verifyUser = DALUser.GetUserDetail(user.id, ref errors);

      //now we compare and ensure that the user we pulled from the database
      //and the user we created are the same.
      Assert.AreEqual(0, errors.Count);
      Assert.AreEqual(user.first_name, verifyUser.first_name);
      Assert.AreEqual(user.last_name, verifyUser.last_name);
      Assert.AreEqual(user.id, verifyUser.id);
      Assert.AreEqual(user.birthDay, verifyUser.birthDay);
      Assert.AreEqual(user.state, verifyUser.state);
      Assert.AreEqual(user.country, verifyUser.country);
      Assert.AreEqual(user.access_level, verifyUser.access_level);
      Assert.AreEqual(user.password, verifyUser.password);
      Assert.AreEqual(user.email, verifyUser.email);

      errors = new List<string>();
      DALUser.ClearDatabase(ref errors);
      errors = new List<string>();
    }

    [TestMethod]
    public void UpdateUserTest()
    {
        //remove all data from the database and initialize our next_id_table
        List<string> errors = new List<string>();
        DALUser.ClearDatabase(ref errors);
        errors = new List<string>();

        //here we create our user and insert it into the database - we assign
        //the user to the result of the insert method so that its updated with
        //its id field.
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);

        //now we're changing all the information in the user
        //object [save for the id]
        user.first_name = "first2";
        user.last_name = " last2";
        user.birthDay = "12/30/2005 12:00:00 AM";
        user.state = "";
        user.country = "Spain";
        user.access_level = "2";
        user.password = "pass12345";
        user.email = "m@www.com";
      
        //here we call the update user method with our user
        //with changed fields but with the same id.
        errors = new List<string>();
        DALUser.UpdateUser(user, ref errors);

        //now, assuming our accessor method works - we retrieve that user
        //from the database using his id - we then compare the updated user
        //with the user we've pulled from our database.  they should be equal.
        User verifyUser = DALUser.GetUserDetail(user.id, ref errors);
        Assert.AreEqual(0, errors.Count);
        Assert.AreEqual(user.first_name, verifyUser.first_name);
        Assert.AreEqual(user.last_name, verifyUser.last_name);
        Assert.AreEqual(user.id, verifyUser.id);
        Assert.AreEqual(user.birthDay, verifyUser.birthDay);
        Assert.AreEqual(user.state, verifyUser.state);
        Assert.AreEqual(user.country, verifyUser.country);
        Assert.AreEqual(user.access_level, verifyUser.access_level);
        Assert.AreEqual(user.password, verifyUser.password);
        Assert.AreEqual(user.email, verifyUser.email);

        errors = new List<string>();
        DALUser.ClearDatabase(ref errors);
        errors = new List<string>();
    }

    [TestMethod]
    public void DeleteeUserTest()
    {
        //remove all data from the database and initialize our next_id_table
        List<string> errors = new List<string>();
        DALUser.ClearDatabase(ref errors);
        errors = new List<string>();

        //here we create our user and add him to our database.
        User user = new User();
        user.first_name = "first";
        user.last_name = "last";
        user.birthDay = "1/2/2003 12:00:00 AM";
        user.state = "CA";
        user.country = "USA";
        user.access_level = "1";
        user.password = "pass1234";
        user.email = "miles@123.com";
        errors = new List<string>();
        user = DALUser.InsertUser(user, ref errors);

        //now we delete our user.
        DALUser.DeleteUser(user.id, ref errors);

        //our attempt to retrieve this user from the database should result
        //in a null object
        User verifyEmptyUser = DALUser.GetUserDetail(user.id, ref errors);

        //assuming there are no errors and the user doesn't exist in the database,
        //this test method should pass.
        Assert.AreEqual(0, errors.Count); 
        Assert.AreEqual(null, verifyEmptyUser);

        errors = new List<string>();
        DALUser.ClearDatabase(ref errors);
        errors = new List<string>();
    }
  }
}
