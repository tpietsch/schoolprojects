﻿CREATE TABLE [dbo].[Song](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[artist_key] [int] NOT NULL,
	[album_key] [int] NULL,
	[genre_key] [int] NOT NULL,
	[download_count] [int] NOT NULL,
	[user_key] [int] NOT NULL,
	CONSTRAINT [FK_Song_Album] FOREIGN KEY([album_key]) REFERENCES [dbo].[Album] ([id]),
	CONSTRAINT [FK_Song_Artist] FOREIGN KEY([artist_key]) REFERENCES [dbo].[Artist] ([id]),
	CONSTRAINT [FK_Song_Genre] FOREIGN KEY([genre_key]) REFERENCES [dbo].[Genre] ([id]),
	CONSTRAINT [FK_Song_User] FOREIGN KEY([user_key]) REFERENCES [dbo].[User] ([id]),
 CONSTRAINT [PK_Song] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
























































