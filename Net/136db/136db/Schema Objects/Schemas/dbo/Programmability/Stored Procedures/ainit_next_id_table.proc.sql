﻿CREATE PROCEDURE [dbo].[init_next_id_table]

WITH EXECUTE AS CALLER
AS
BEGIN
	DELETE FROM [next_id_table]
	INSERT INTO [next_id_table](table_name, next_id) VALUES ('Album', 0)
	INSERT INTO [next_id_table](table_name, next_id) VALUES ('Artist', 0)
	INSERT INTO [next_id_table](table_name, next_id) VALUES ('Genre', 0)
	INSERT INTO [next_id_table](table_name, next_id) VALUES ('Song', 0)
	INSERT INTO [next_id_table](table_name, next_id) VALUES ('User', 0)
END






























































