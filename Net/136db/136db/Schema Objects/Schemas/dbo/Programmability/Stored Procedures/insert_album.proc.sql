﻿CREATE PROCEDURE [dbo].[insert_album]
@albumName VARCHAR (50), @albumYear INT, @albumid INT OUTPUT
WITH EXECUTE AS CALLER
AS
BEGIN
	SET @albumid = NULL
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SELECT @albumid = id FROM Album WHERE name = @albumName AND year = @albumYear
	IF (@albumid IS NULL)
	BEGIN
		EXEC get_next_id 'Album', @albumid OUTPUT
		insert into Album (id, name, year) values (@albumid, @albumName, @albumYear)
	END
    -- Insert statements for procedure here	
END






























































