﻿CREATE PROCEDURE [dbo].[insert_song]
@albumName VARCHAR (50), @albumYear INT, @genreName VARCHAR (50), @artistName VARCHAR (50), @userId INT, @songName VARCHAR (50)
WITH EXECUTE AS CALLER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @albumid AS INT
	DECLARE @artistid AS INT
	DECLARE @genreid as INT
	DECLARE @songid as INT
	SET @songid = NULL
	
	EXEC insert_album @albumName, @albumYear, @albumid OUTPUT
	EXEC insert_artist @artistname, @artistid OUTPUT
	EXEC insert_genre @genreName, @genreid OUTPUT

	SELECT @songid = id FROM [Song] WHERE name = @songName AND artist_key = @artistid AND album_key = @albumid AND genre_key = @genreid AND user_key = @userid
	IF (@songid IS NULL)
	BEGIN
		EXEC get_next_id 'Song', @songid OUTPUT
		insert into song (id, name,artist_key,album_key,genre_key,download_count,user_key) values (@songid, @songName,@artistid,@albumid,@genreid,0,@userId)	
	END
	SELECT * FROM Song WHERE id = @songid
	
END






























































