﻿CREATE PROCEDURE [dbo].[select_by_genre]
@genreName VARCHAR (50)
WITH EXECUTE AS CALLER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select s.id, 
		   s.name, 
		   s.artist_key, 
		   s.album_key, 
		   s.genre_key, 
		   s.download_count, 
		   s.user_key
	from Song s, Genre g
	where s.genre_key = g.id AND g.name = @genreName
	
END






























































