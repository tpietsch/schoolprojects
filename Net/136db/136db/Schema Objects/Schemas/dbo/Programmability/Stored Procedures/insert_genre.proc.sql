﻿CREATE PROCEDURE [dbo].[insert_genre]
@genreName VARCHAR (50), @genreid INT OUTPUT
WITH EXECUTE AS CALLER
AS
BEGIN
	SET @genreid = NULL
	SELECT @genreid = id FROM Genre WHERE name = @genreName
	IF (@genreid IS NULL)
	BEGIN
		EXEC get_next_id 'Genre', @genreid OUTPUT
		insert into genre (id, name) values (@genreid, @genreName)
		-- Insert statements for procedure here
	END	
END






























































