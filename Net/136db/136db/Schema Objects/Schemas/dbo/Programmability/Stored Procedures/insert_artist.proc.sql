﻿CREATE PROCEDURE [dbo].[insert_artist]
@artistName VARCHAR (50), @artistid INT OUTPUT
WITH EXECUTE AS CALLER
AS
BEGIN
	SET @artistid = NULL
	SELECT @artistid = id FROM Artist WHERE name = @artistName
	IF (@artistid IS NULL)
	BEGIN
		EXEC get_next_id 'Artist', @artistid OUTPUT
		insert into artist (id, name) values (@artistid, @artistName)
	END	
END






























































