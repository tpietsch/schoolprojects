﻿CREATE PROCEDURE [dbo].[delete_song]
@songid INT
WITH EXECUTE AS CALLER
AS
BEGIN
	-- here we declare our local variables
	-- ids store the ids of artists, albums, and genres
	-- counts store the number of songs that have the same artist, genre, album id
	DECLARE @albumid AS INT
	DECLARE @albumcount AS INT
	DECLARE @genreid AS INT
	DECLARE @genrecount AS INT
	DECLARE @artistid AS INT
	DECLARE @artistcount AS INT
	
	-- we want to select the id's of the artist, album, and genre for the 
	-- song we are going to delete
	SELECT @albumid = album_key,
           @genreid = genre_key,
           @artistid = artist_key
    FROM
			[Song]
	WHERE
		   id = @songid
	
	-- here, we count how many records have the same album, genre, artist id.
	-- we do this, because we'll want to delete the album, genre, artist from 
	-- our respective tables if this song is the only song that uses said id's.
	SELECT @albumcount = COUNT(*) FROM [Song] WHERE album_key = @albumid
	SELECT @genrecount = COUNT(*) FROM [Song] WHERE genre_key = @genreid
	SELECT @artistcount = COUNT(*) FROM [Song] WHERE artist_key = @artistid
	
	-- finally, let's delete the song from our table.
	DELETE FROM [Song] WHERE id = @songid
	
	-- here we run our stored procedures to delete the respective artists
	-- genres and albums if there is only one song that has the same id 
	-- [because that means that we're deleting the only song with these id's]
	IF (@albumcount = 1)
	BEGIN
		EXEC delete_album @albumid
	END
	IF (@genrecount = 1)
	BEGIN
		EXEC delete_genre @genreid
	END
	IF (@artistcount = 1)
	BEGIN
		EXEC delete_artist @artistid
	END
END






























































