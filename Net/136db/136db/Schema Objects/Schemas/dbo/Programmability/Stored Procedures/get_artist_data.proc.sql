﻿CREATE PROCEDURE [dbo].[get_artist_data]
	@songid AS INT
AS
BEGIN
	DECLARE @artist_key AS INT
	SET @artist_key = -1
	SELECT @artist_key = artist_key FROM [Song] WHERE id = @songid
	SELECT * FROM [Artist] WHERE id = @artist_key
END














