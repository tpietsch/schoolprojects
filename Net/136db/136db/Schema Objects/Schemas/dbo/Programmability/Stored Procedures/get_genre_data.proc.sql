﻿CREATE PROCEDURE [dbo].[get_genre_data]
	@songid AS INT
AS
BEGIN
	DECLARE @genre_key AS INT
	SET @genre_key = -1
	SELECT @genre_key = genre_key FROM [Song] WHERE id = @songid
	SELECT * FROM [Genre] WHERE id = @genre_key
END














