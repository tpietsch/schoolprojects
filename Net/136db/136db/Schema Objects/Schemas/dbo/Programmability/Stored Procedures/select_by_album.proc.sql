﻿CREATE PROCEDURE [dbo].[select_by_album]
@albumName VARCHAR (50)
WITH EXECUTE AS CALLER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select s.id, 
		   s.name, 
		   s.artist_key, 
		   s.album_key, 
		   s.genre_key, 
		   s.download_count, 
		   s.user_key
	from Song s, Album a
	where s.album_key = a.id AND a.name = @albumName
	
END






























































