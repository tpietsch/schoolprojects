﻿CREATE PROCEDURE [dbo].[select_by_user]
@first_name VARCHAR (50), @last_name VARCHAR (50), @birthDay DATE, @state VARCHAR (2), @country VARCHAR (50)
WITH EXECUTE AS CALLER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select s.id, 
		   s.name, 
		   s.artist_key, 
		   s.album_key, 
		   s.genre_key, 
		   s.download_count, 
		   s.user_key
	from Song s, [User] u
	where s.user_key = u.id AND u.first_name = @first_name AND u.last_name = @last_name AND u.birthDay = @birthDay AND u.state = @state AND u.country = @country
	
END






























































