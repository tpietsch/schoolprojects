﻿CREATE PROCEDURE [dbo].[clear_database]

WITH EXECUTE AS CALLER
AS
BEGIN
	DELETE FROM [Song]
	DELETE FROM [User]
	DELETE FROM [Artist]
	DELETE FROM [Album]
	DELETE FROM [Genre]
	DELETE FROM [Error]
	DELETE FROM [next_id_table]
	EXEC [dbo].[init_next_id_table]
END






























































