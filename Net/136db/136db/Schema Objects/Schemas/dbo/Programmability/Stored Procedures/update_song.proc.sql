﻿CREATE PROCEDURE [dbo].[update_song]
@songid INT, @albumName VARCHAR (50), @albumYear INT, @genreName VARCHAR (50), @artistName VARCHAR (50), @userid INT, @songName VARCHAR (50)
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @artistOldId AS INT
	DECLARE @genreOldId AS INT
	DECLARE @albumOldId AS INT
	DECLARE @artistNewId AS INT
	DECLARE @genreNewId AS INT
	DECLARE @albumNewId AS INT
	DECLARE @artistcount AS INT
	DECLARE @albumcount AS INT
	DECLARE @genrecount AS INT
	
	SELECT 
		@artistOldId = artist_key,
		@genreOldId = genre_key,
		@albumOldId = album_key
	FROM
		Song
	WHERE
		id = @songid
	
	EXEC insert_album @albumName, @albumYear, @albumNewId OUTPUT
	EXEC insert_genre @genreName, @genreNewId OUTPUT
	EXEC insert_artist @artistName, @artistNewId OUTPUT
	 
	SELECT @artistcount = COUNT(*) FROM [Song] WHERE artist_key = @artistOldId
	SELECT @albumcount = COUNT(*) FROM [Song] WHERE album_key = @albumOldId
	SELECT @genrecount = COUNT(*) FROM [Song] WHERE genre_key = @genreOldId
	
	UPDATE Song 
	SET 
		name = @songname,
		artist_key = @artistNewId,
		genre_key = @genreNewId,
		album_key = @albumNewId,
		user_key = @userid
	WHERE id = @songid
	
	IF (@albumcount = 1 AND @albumOldId != @albumNewId)
	BEGIN
		EXEC delete_album @albumOldId
	END
	IF (@artistcount = 1 AND @artistOldId != @artistNewId)
	BEGIN
		EXEC delete_artist @artistOldId
	END
	IF (@genrecount = 1 AND @genreOldId != @genreNewId)
	BEGIN
		EXEC delete_genre @genreOldId
	END
	
	SELECT * FROM Song WHERE id = @songid
END






























































