﻿CREATE PROCEDURE [dbo].[insert_user]
@firstName VARCHAR (50), @lastName VARCHAR (50), @birth DATE, @state VARCHAR (2), @country VARCHAR (50), @access INT, @password VARCHAR (50), @email VARCHAR (50)
WITH EXECUTE AS CALLER
AS
BEGIN

	DECLARE @userid INT
	SET @userid = NULL
	
	SELECT @userid = id FROM [User] WHERE first_name = @firstName AND last_name = @lastName AND birthDay = @birth AND state = @state AND country = @country AND access_level = @access AND password = @password AND email = @email
	IF (@userid IS NULL)
	BEGIN	
		EXEC [dbo].[get_next_id] 'User', @userid OUTPUT
		insert into [User] (id, first_name,last_name,birthDay,[state],country,access_level,[password],email) values (@userid, @firstName,
		@lastName ,
		@birth ,
		@state ,
		@country ,
		@access ,
		@password ,
		@email)
	END

	SELECT * FROM [User] WHERE id = @userid
	
END






























































