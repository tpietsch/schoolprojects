﻿
USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/22/2012 14:24:53 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/22/2012 14:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/22/2012 14:50:59 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/22/2012 14:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/22/2012 14:57:38 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/22/2012 14:57:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/22/2012 14:57:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/22/2012 14:57:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/22/2012 14:57:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/22/2012 14:57:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/22/2012 14:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/22/2012 15:08:11 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/22/2012 15:08:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/22/2012 15:11:13 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/22/2012 15:11:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/22/2012 15:11:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/22/2012 15:11:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/22/2012 15:11:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/22/2012 15:11:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/22/2012 15:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/22/2012 15:14:39 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/22/2012 15:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/30/2012 08:01:04 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/30/2012 08:01:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 08/30/2012 08:01:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/30/2012 08:01:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/30/2012 08:01:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/30/2012 08:01:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/30/2012 08:01:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/30/2012 08:01:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/30/2012 08:39:08 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/30/2012 08:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/30/2012 08:50:40 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/30/2012 08:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/30/2012 08:59:21 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/30/2012 08:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/30/2012 08:59:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/30/2012 08:59:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/30/2012 08:59:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/30/2012 08:59:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/30/2012 14:46:33 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/30/2012 14:46:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/30/2012 14:46:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 08/30/2012 14:46:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/30/2012 14:46:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/30/2012 14:46:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/30/2012 14:46:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/30/2012 14:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [cse136]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/30/2012 16:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [136db]    Script Date: 09/04/2012 20:19:45 ******/
CREATE DATABASE [136db] ON  PRIMARY 
( NAME = N'136db_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\136db.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'136db_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\136db.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [136db] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [136db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [136db] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [136db] SET ANSI_NULLS OFF
GO
ALTER DATABASE [136db] SET ANSI_PADDING OFF
GO
ALTER DATABASE [136db] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [136db] SET ARITHABORT OFF
GO
ALTER DATABASE [136db] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [136db] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [136db] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [136db] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [136db] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [136db] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [136db] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [136db] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [136db] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [136db] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [136db] SET  ENABLE_BROKER
GO
ALTER DATABASE [136db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [136db] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [136db] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [136db] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [136db] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [136db] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [136db] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [136db] SET  READ_WRITE
GO
ALTER DATABASE [136db] SET RECOVERY FULL
GO
ALTER DATABASE [136db] SET  MULTI_USER
GO
ALTER DATABASE [136db] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [136db] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'136db', N'ON'
GO
USE [136db]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 09/04/2012 20:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 09/04/2012 20:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 09/04/2012 20:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 09/04/2012 20:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 09/04/2012 20:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/04/2012 20:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 09/04/2012 20:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [136db]    Script Date: 09/04/2012 20:33:00 ******/
CREATE DATABASE [136db] ON  PRIMARY 
( NAME = N'136db_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\136db.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'136db_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\136db.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [136db] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [136db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [136db] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [136db] SET ANSI_NULLS OFF
GO
ALTER DATABASE [136db] SET ANSI_PADDING OFF
GO
ALTER DATABASE [136db] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [136db] SET ARITHABORT OFF
GO
ALTER DATABASE [136db] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [136db] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [136db] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [136db] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [136db] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [136db] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [136db] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [136db] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [136db] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [136db] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [136db] SET  ENABLE_BROKER
GO
ALTER DATABASE [136db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [136db] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [136db] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [136db] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [136db] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [136db] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [136db] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [136db] SET  READ_WRITE
GO
ALTER DATABASE [136db] SET RECOVERY FULL
GO
ALTER DATABASE [136db] SET  MULTI_USER
GO
ALTER DATABASE [136db] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [136db] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'136db', N'ON'
GO
USE [136db]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 09/04/2012 20:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 09/04/2012 20:33:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 09/05/2012 20:32:22 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136_dat', FILENAME = N'C:\MSSQL\Data\cse136.mdf' , SIZE = 24832KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\MSSQL\Data\cse136.ldf' , SIZE = 24832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK ON
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS ON
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC ON
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION FORCED
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY TORN_PAGE_DETECTION
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  Table [dbo].[Song]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 09/05/2012 20:32:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF

GO

USE [master]
GO
/****** Object:  Database [136db]    Script Date: 09/06/2012 19:36:16 ******/
CREATE DATABASE [136db] ON  PRIMARY 
( NAME = N'136db_dat', FILENAME = N'C:\MSSQL\Data\136db.mdf' , SIZE = 24832KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'136db_log', FILENAME = N'C:\MSSQL\Data\136db.ldf' , SIZE = 24832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [136db] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [136db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [136db] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [136db] SET ANSI_NULLS OFF
GO
ALTER DATABASE [136db] SET ANSI_PADDING OFF
GO
ALTER DATABASE [136db] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [136db] SET ARITHABORT OFF
GO
ALTER DATABASE [136db] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [136db] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [136db] SET AUTO_SHRINK ON
GO
ALTER DATABASE [136db] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [136db] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [136db] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [136db] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [136db] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [136db] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [136db] SET RECURSIVE_TRIGGERS ON
GO
ALTER DATABASE [136db] SET  DISABLE_BROKER
GO
ALTER DATABASE [136db] SET AUTO_UPDATE_STATISTICS_ASYNC ON
GO
ALTER DATABASE [136db] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [136db] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [136db] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [136db] SET PARAMETERIZATION FORCED
GO
ALTER DATABASE [136db] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [136db] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [136db] SET  READ_WRITE
GO
ALTER DATABASE [136db] SET RECOVERY FULL
GO
ALTER DATABASE [136db] SET  MULTI_USER
GO
ALTER DATABASE [136db] SET PAGE_VERIFY TORN_PAGE_DETECTION
GO
ALTER DATABASE [136db] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'136db', N'ON'
GO
USE [136db]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 09/06/2012 19:36:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 09/06/2012 19:36:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 09/06/2012 19:36:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 09/06/2012 19:36:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 09/06/2012 19:36:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/06/2012 19:36:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[Song]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 09/06/2012 19:36:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO

USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 09/06/2012 20:15:02 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136_dat', FILENAME = N'C:\MSSQL\Data\cse136.mdf' , SIZE = 24832KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\MSSQL\Data\cse136.ldf' , SIZE = 24832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK ON
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS ON
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC ON
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION FORCED
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY TORN_PAGE_DETECTION
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  Table [dbo].[Song]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_genre_data]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_artist_data]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_album_data]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 09/06/2012 20:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF

GO

USE [cse136]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  Table [dbo].[Song]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_genre_data]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_artist_data]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_album_data]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 09/07/2012 17:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_year]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 09/07/2012 17:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF

GO

USE [cse136]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  Table [dbo].[Song]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_genre_data]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_artist_data]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_album_data]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_year]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 09/07/2012 18:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF

GO

USE [cse136]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 09/07/2012 18:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 09/07/2012 18:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 09/07/2012 18:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 09/07/2012 18:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 09/07/2012 18:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/07/2012 18:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  Table [dbo].[Song]    Script Date: 09/07/2012 18:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_genre_data]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_artist_data]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_album_data]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_year]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_user_email]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 09/07/2012 18:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF

GO

USE [cse136]
GO
/****** Object:  Table [dbo].[next_id_table]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Error]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[init_next_id_table]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_error]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  Table [dbo].[Song]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_errors]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_next_id]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_genre_data]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_artist_data]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[get_album_data]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[clear_database]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_year]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_user_email]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[get_song_data]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 09/08/2012 14:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF

GO
