﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization; // this is required

namespace DomainModel
{
  [DataContract]
  public class User
  {
    [DataMember]
    public string id = "";
    
    [DataMember]
    public string first_name = "";
    
    [DataMember]
    public string last_name = "";

    [DataMember]
    public string birthDay = "";

    [DataMember]
    public string state = "";

    [DataMember]
    public string country = "";

    [DataMember]
    public string access_level = "";

    [DataMember]
    public string password = "";

    [DataMember]
    public string email = "";
  }
}
