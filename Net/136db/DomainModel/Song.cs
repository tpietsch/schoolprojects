﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization; // this is required

namespace DomainModel
{
    [DataContract]
    public class Song
    {
        [DataMember]
        public string id = "";

        [DataMember]
        public string name = "";

        [DataMember]
        public string artist_name = "";

        [DataMember]
        public string album_name = "";

        [DataMember]
        public string album_year = "";

        [DataMember]
        public string genre_name = "";

        [DataMember]
        public string download_count = "";

        [DataMember]
        public User user = new User();
    }
}
