﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DAL;
using DomainModel;

namespace BL
{
  public static class BLUser
  {
    public static User InsertUser(User user, ref List<string> errors)
    {
        if (user == null)
        {
            errors.Add("User cannot be null");
        }
        else
        {
            Regex regex = new Regex("^[a-zA-Z]+$");
            Match match = regex.Match(user.first_name);

            if (!(match.Success))
            {
                errors.Add("First name must contain only letters.");
            }

            match = regex.Match(user.last_name);

            if (!(match.Success))
            {
                errors.Add("Last name must contain only letters.");
            }

            match = regex.Match(user.state);

            if (!(match.Success))
            {
                errors.Add("State must contain only letters.");
            }

            match = regex.Match(user.country);

            if (!(match.Success))
            {
                errors.Add("Country must contain only letters.");
            }

            regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            match = regex.Match(user.email);

            if(!(match.Success))
            {
                errors.Add("E-mail address must be valid");
            }

            if(user.password.Length == 0)
            {
                errors.Add("Password must be of length greater than 0");
            }
        }

        if (errors.Count > 0)
        {
            AsynchLog.LogNow(errors);
            return null;
        }
        return DALUser.InsertUser(user, ref errors);
    }

    public static void UpdateUser(User user, ref List<string> errors)
    {
        if (user == null)
        {
            errors.Add("User cannot be null");
        }
        else
        {

            Regex regex = new Regex("^[a-zA-Z]+$");
            Match match = regex.Match(user.first_name);

            if (!(match.Success))
            {
                errors.Add("First name must contain only letters.");
            }

            match = regex.Match(user.last_name);

            if (!(match.Success))
            {
                errors.Add("Last name must contain only letters.");
            }

            match = regex.Match(user.country);

            if (!(match.Success))
            {
                errors.Add("Country must contain only letters.");
            }

            match = regex.Match(user.state);

            if(user.country != "USA" || user.country != "US" || user.country != "United States of America" || user.country != "United States")
            {
                if (!(match.Success))
                {
                    errors.Add("State must contain only letters.");
                }
            }

            regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            match = regex.Match(user.email);

            if(!(match.Success))
            {
                errors.Add("E-mail address must be valid");
            }

            if(user.password.Length == 0)
            {
                errors.Add("Password must be of length greater than 0");
            }
        }
        if (errors.Count > 0)
        {
          AsynchLog.LogNow(errors);
          return;
        }

        DALUser.UpdateUser(user, ref errors);
    }

    public static User GetUserDetail(string id, ref List<string> errors)
    {
      if (id == null)
      {
        errors.Add("Invalid user ID");
      }

      if (errors.Count > 0)
      {
          AsynchLog.LogNow(errors);
          return null;
      }

      return (DALUser.GetUserDetail(id, ref errors));
    }

    public static void DeleteUser(string id, ref List<string> errors)
    {
      if (id == null)
      {
        errors.Add("Invalid user ID");
      }

      if (errors.Count > 0)
      {
          AsynchLog.LogNow(errors);
          return;
      }

      DALUser.DeleteUser(id, ref errors);
    }

    public static List<User> GetUserList(ref List<string> errors)
    {
        if (errors.Count > 0)
        {
            AsynchLog.LogNow(errors);
            return null;
        }

        return DALUser.GetUserList(ref errors);
    }

    public static void ClearDatabase(ref List<string> errors)
    {
        DALUser.ClearDatabase(ref errors);
    }

  }
}
