﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DAL;
using DomainModel;

namespace BL
{
  public static class BLSong
  {
      public static Song GetSongDetails(string songid, ref List<string> errors)
      {
          if (songid == null || Convert.ToInt32(songid) < 0)
          {
              errors.Add("Invalid Song Id");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return null;
          }

          return DALSong.GetSongDetail(songid, ref errors);
      }

      public static Song InsertSong(Song song, ref List<string> errors)
      {
          if (song == null)
          {
              errors.Add("Invalid song");
          }
          else
          {
              Regex pattern = new Regex("[0-9][0-9][0-9][0-9]");
              Match match = pattern.Match(song.album_year);

              if (!(match.Success))
              {
                  errors.Add("Invalid album year");
              }
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return null;
          }

          return DALSong.InsertSong(song, ref errors);
      }

      public static void UpdateSong(Song song, ref List<string> errors)
      {
          if (song == null)
          {
              errors.Add("Invalid song");
          }
          else
          {
              Regex pattern = new Regex("[0-9][0-9][0-9][0-9]");
              Match match = pattern.Match(song.album_year);

              if (!(match.Success))
              {
                  errors.Add("Invalid album year");
              }
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return;
          }

          DALSong.UpdateSong(song, ref errors);
      }

      public static void DeleteSong(string songid, ref List<string> errors)
      {
          if (songid == null)
          {
            errors.Add("Invalid song id");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return;
          }

          DALSong.DeleteSong(songid, ref errors);
      }

      public static void IncrementDLCount(string songid, ref List<string> errors)
      {
          if (songid == null)
          {
            errors.Add("Invalid song id");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return;
          }

          DALSong.IncrementDLCount(songid, ref errors);
      }

      public static int GetDLCount(string songid, ref List<string> errors)
      {
          if (songid == null)
          {
            errors.Add("Invalid song id");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return -1;
          }

          return DALSong.GetDLCount(songid, ref errors);
      }

      public static List<Song> GetSongsByName(string name, ref List<string> errors)
      {
          if (name == null)
          {
            errors.Add("Invalid song name");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return null;
          }

          return DALSong.GetSongsByName(name, ref errors);
      }

      public static List<Song> GetSongsByUser(string email, ref List<string> errors)
      {
          if (email == null)
          {
            errors.Add("Invalid user");
          }
          Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
          Match match = regex.Match(email);

          if (!(match.Success))
          {
              errors.Add("E-mail address must be valid");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return null;
          }

          return DALSong.GetSongsByUser(email, ref errors);
      }

      public static List<Song> GetSongsByUser(User user, ref List<string> errors)
      {
          if (user == null)
          {
              errors.Add("Invalid user");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return null;
          }

          return DALSong.GetSongsByUser(user, ref errors);
      }

      public static List<Song> GetSongsByGenre(string genreName, ref List<string> errors)
       {
           if (genreName == null)
          {
            errors.Add("Invalid genre name");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return null;
          }

          return DALSong.GetSongsByGenre(genreName, ref errors);
      }

      public static List<Song> GetSongsByArtist(string artistName, ref List<string> errors)
      {
          if (artistName == null)
          {
            errors.Add("Invalid artist name");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return null;
          }

          return DALSong.GetSongsByArtist(artistName, ref errors);
      }

      public static List<Song> GetSongsByAlbum(string albumName, ref List<string> errors)
      {
          if (albumName == null)
          {
              errors.Add("Invalid album information");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return null;
          }

          return DALSong.GetSongsByAlbum(albumName, ref errors);
      }

      public static List<Song> GetSongsByYear(string albumYear, ref List<string> errors)
      {
          if (albumYear == null)
          {
              errors.Add("Invalid album information");
          }

          Regex pattern = new Regex("[0-9][0-9][0-9][0-9]");
          Match match = pattern.Match(albumYear);

          if (!(match.Success))
          {
              errors.Add("Invalid album year");
          }

          if (errors.Count > 0)
          {
              AsynchLog.LogNow(errors);
              return null;
          }

          return DALSong.GetSongsByYear(albumYear, ref errors);
      }

      public static void ClearDatabase(ref List<string> errors)
      {
          DALSong.ClearDatabase(ref errors);
      }

  }
}
