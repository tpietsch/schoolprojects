﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DAL;
using DomainModel;

namespace BL
{
	public static class BLAuth
	{
		public static Logon Authenticate(string email, string password, ref List<string> errors)
		{
            if (email == null || password == null)
            {
                errors.Add("Login information must be non-null");
            }
            else
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);
                if (email == null)
                {
                    errors.Add("Email cannot be null");
                }
                else
                {
                    if (!(match.Success))
                    {
                        errors.Add("E-mail must be a valid address");
                    }
                    else
                    {
                        if (password.Length == 0)
                        {
                            errors.Add("Password cannot be null or empty");
                        }
                    }
                }
            }
			if (errors.Count > 0)
			{
				AsynchLog.LogNow(errors);
				return null;
			}

			return DALAuth.Authenticate(email, password, ref errors);
		}

        public static void ClearDatabase(ref List<string> errors)
        {
            DALAuth.ClearDatabase(ref errors);
        }

	}
}
