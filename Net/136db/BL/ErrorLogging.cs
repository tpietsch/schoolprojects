﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration; // must add this... make sure to add "System.Configuration" first
using System.Data.SqlClient;
using System.Data;

///
/// Note, in real world practice, each class definition should be in its own
/// file because the logic is much more complicated.  For 136, it's all in one
/// file for simplicity.
///
namespace BL
{
	// this is the interface definition
	public interface IErrorLogging
	{
		void LogError(List<string> errorList);
	}

	public class LogToFile : IErrorLogging
	{
		public void LogError(List<string> errorList)
		{
            string path = ConfigurationManager.AppSettings["fileErrorPath"];

            System.IO.StreamWriter file = new System.IO.StreamWriter(path, true); 

            for (int x = 0; x < errorList.Count; x++)
            {
                file.WriteLine(errorList[x]);
            }

            file.Close();
		}
	}

	public class LogToDB : IErrorLogging
	{
		public void LogError(List<string> errorList)
		{
            string connection_string = ConfigurationManager.AppSettings["dsn"];

			SqlConnection conn = new SqlConnection(connection_string);

            try
            {
                string strSQL = "insert_error";

                SqlDataAdapter mySA = new SqlDataAdapter(strSQL, conn);
                mySA.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySA.SelectCommand.Parameters.Add(new SqlParameter("@error", SqlDbType.VarChar, -1));

                for (int x = 0; x < errorList.Count; x++)
                {
                    mySA.SelectCommand.Parameters["@error"].Value = errorList[x];
                    DataSet myDS = new DataSet();
                    mySA.Fill(myDS);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                conn.Dispose();
                conn = null;
            }


		}
	}

	public class ErrorLogFactory
	{

		static string logDestination = ConfigurationManager.AppSettings["logDestination"];
		public IErrorLogging logInstance = null;

		public IErrorLogging GetErrorLogInstance()
		{
			switch (logDestination)
			{
				case "file":
					logInstance = new LogToFile();
					break;
				case "db":
					logInstance = new LogToDB();
					break;
				default:
					break;
			}
			return logInstance;
		}
	}

	public class AsynchLog
	{
		delegate void MethodDelegate(List<string> strError);

		public static void LogNow(List<string> strError)
		{
			IErrorLogging log = new ErrorLogFactory().GetErrorLogInstance();

			MethodDelegate callGenerateFileAsync = new MethodDelegate(log.LogError);
			IAsyncResult ar = callGenerateFileAsync.BeginInvoke(strError, null, null);
		}
	}

}
