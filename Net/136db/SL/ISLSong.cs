﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DomainModel;
using BL;

namespace SL
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISLSchedule" in both code and config file together.
  [ServiceContract]
  public interface ISLSong
  {
        [OperationContract]
        Song GetSongDetails(string songid, ref List<string> errors);

        [OperationContract]
        Song InsertSong(Song song, ref List<string> errors);

        [OperationContract]
        void UpdateSong(Song song, ref List<string> errors);

        [OperationContract]
        void DeleteSong(string songid, ref List<string> errors);

        [OperationContract]
        void IncrementDLCount(string songid, ref List<string> errors);

        [OperationContract]
        int GetDLCount(string songid, ref List<string> errors);

        [OperationContract]
        List<Song> GetSongsByName(string name, ref List<string> errors);

        [OperationContract]
        List<Song> GetSongsByUserEmail(string email, ref List<string> errors);

        [OperationContract]
        List<Song> GetSongsByUser(User user, ref List<string> errors);

        [OperationContract]
        List<Song> GetSongsByGenre(string genreName, ref List<string> errors);

        [OperationContract]
        List<Song> GetSongsByArtist(string artistName, ref List<string> errors);

        [OperationContract]
        List<Song> GetSongsByAlbum(string albumName, ref List<string> errors);

        [OperationContract]
        List<Song> GetSongsByYear(string albumYear, ref List<string> errors);
  }
}
