﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DomainModel;
using BL;

namespace SL
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SLSchedule" in code, svc and config file together.
  public class SLSong : ISLSong
  {
      public Song GetSongDetails(string songid, ref List<string> errors)
      {
          return BLSong.GetSongDetails(songid, ref errors);
      }

      public Song InsertSong(Song song, ref List<string> errors)
      {
          return BLSong.InsertSong(song, ref errors);
      }

      public void UpdateSong(Song song, ref List<string> errors)
      {
          BLSong.UpdateSong(song, ref errors);
      }

      public void DeleteSong(string songid, ref List<string> errors)
      {
          BLSong.DeleteSong(songid, ref errors);
      }

      public void IncrementDLCount(string songid, ref List<string> errors)
      {
          BLSong.IncrementDLCount(songid, ref errors);
      }

      public int GetDLCount(string songid, ref List<string> errors)
      {
          return BLSong.GetDLCount(songid, ref errors);
      }

      public List<Song> GetSongsByName(string name, ref List<string> errors)
      {
          return BLSong.GetSongsByName(name, ref errors);
      }

      public List<Song> GetSongsByUserEmail(string email, ref List<string> errors)
      {
          return BLSong.GetSongsByUser(email, ref errors);
      }

      public List<Song> GetSongsByUser(User user, ref List<string> errors)
      {
          return BLSong.GetSongsByUser(user, ref errors);
      }

      public List<Song> GetSongsByGenre(string genreName, ref List<string> errors)
      {
          return BLSong.GetSongsByGenre(genreName, ref errors);
      }

      public List<Song> GetSongsByArtist(string artistName, ref List<string> errors)
      {
          return BLSong.GetSongsByArtist(artistName, ref errors);
      }

      public List<Song> GetSongsByAlbum(string albumName, ref List<string> errors)
      {
          return BLSong.GetSongsByAlbum(albumName, ref errors);
      }

      public List<Song> GetSongsByYear(string albumYear, ref List<string> errors)
      {
          return BLSong.GetSongsByYear(albumYear, ref errors);
      }
  }
}
