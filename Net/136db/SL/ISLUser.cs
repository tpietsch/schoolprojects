﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DomainModel; // this is required
using BL; // this is required

namespace SL
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
  [ServiceContract]
  public interface ISLUser
  {
      [OperationContract]
      void InsertUser(User user, ref List<string> errors);

      [OperationContract]
      void UpdateUser(User user, ref List<string> errors);

      [OperationContract]
      User GetUser(string id, ref List<string> errors);

      [OperationContract]
      void DeleteUser(string id, ref List<string> errors);

      [OperationContract]
      List<User> GetUserList(ref List<string> errors);
  }
}
