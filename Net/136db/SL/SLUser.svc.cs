﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DomainModel;
using BL;

namespace SL
{
  public class SLUser: ISLUser
  {
      public void InsertUser(User user, ref List<string> errors)
      {
          BLUser.InsertUser(user, ref errors);
      }

      public void UpdateUser(User user, ref List<string> errors)
      {
          BLUser.UpdateUser(user, ref errors);
      }

      public User GetUser(string id, ref List<string> errors)
      {
          return (BLUser.GetUserDetail(id, ref errors));
      }

      public void DeleteUser(string id, ref List<string> errors)
      {
          BLUser.DeleteUser(id, ref errors);
      }

      public List<User> GetUserList(ref List<string> errors)
      {
          return BLUser.GetUserList(ref errors);
      }
  }
}
