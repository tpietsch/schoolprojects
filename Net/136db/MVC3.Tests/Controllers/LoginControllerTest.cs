﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web;
using System.Web.Mvc;
using MVC3;
using MVC3.Controllers;
using MVC3.Models;


namespace MVC3.Tests.Controllers
{
    [TestClass]
    public class LoginControllerTest
    {
        [TestMethod]
        public void LoginTest()
        {
            FormCollection createFormValues = new FormCollection();
            LoginController controller = new LoginController();

            createFormValues.Add("email", "usertest@test.com");
            createFormValues.Add("password", "test");
            createFormValues.Add("access_level", "0");

            RedirectToRouteResult result = controller.Index(createFormValues) as RedirectToRouteResult;

            string action = result.RouteValues["action"].ToString();
            string controllerString = result.RouteValues["controller"].ToString();

            Assert.AreEqual("Index", action);
            Assert.AreEqual("Login", controllerString);

            CreateAccountController setup = new CreateAccountController();

            createFormValues = new FormCollection();

            createFormValues.Add("first_name", "user");
            createFormValues.Add("last_name", "test");
            createFormValues.Add("email", "usertest@test.com");
            createFormValues.Add("password", "test");
            createFormValues.Add("birthDay", "01/01/2001");
            createFormValues.Add("state", "CA");
            createFormValues.Add("country", "USA");
            createFormValues.Add("access_level", "0");

            result = setup.Index(createFormValues) as RedirectToRouteResult;

            controller = new LoginController();

            createFormValues = new FormCollection();

            createFormValues.Add("email", "usertest@test.com");
            createFormValues.Add("password", "test");

            result = controller.Index(createFormValues) as RedirectToRouteResult;

            action = result.RouteValues["action"].ToString();
            controllerString = result.RouteValues["controller"].ToString();

            Assert.Equals("Index", action);
            Assert.Equals("User", controllerString);

            setup = new CreateAccountController();

            createFormValues = new FormCollection();

            createFormValues.Add("first_name", "admin");
            createFormValues.Add("last_name", "test");
            createFormValues.Add("email", "admintest@test.com");
            createFormValues.Add("password", "test");
            createFormValues.Add("birthDay", "01/01/2001");
            createFormValues.Add("state", "CA");
            createFormValues.Add("country", "USA");
            createFormValues.Add("access_level", "1");

            result = setup.Index(createFormValues) as RedirectToRouteResult;

            controller = new LoginController();

            createFormValues = new FormCollection();

            createFormValues.Add("email", "admintest@test.com");
            createFormValues.Add("password", "test");

            result = controller.Index(createFormValues) as RedirectToRouteResult;

            action = result.RouteValues["action"].ToString();
            controllerString = result.RouteValues["controller"].ToString();

            Assert.Equals("Index", action);
            Assert.Equals("Admin", controllerString);

            
        }
    }

    internal class SessionCache
    {
        public static void Store(string key, object val)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Session[key] = val;
            }
        }

        public static object Retrieve(string key)
        {
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Session[key];
            }

            return null;
        }
    }
}
