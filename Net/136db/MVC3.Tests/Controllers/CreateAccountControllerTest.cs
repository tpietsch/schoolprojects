﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using MVC3;
using MVC3.Controllers;
using MVC3.Models;

namespace MVC3.Tests.Controllers
{
    [TestClass]
    public class CreateAccountControllerTest
    {
        [TestMethod]
        public void CreateAccountTest()
        {
            CreateAccountController controller = new CreateAccountController();

            FormCollection createFormValues = new FormCollection();

            createFormValues.Add("first_name", "user");
            createFormValues.Add("last_name", "test");
            createFormValues.Add("email", "usertest@test.com");
            createFormValues.Add("password", "test");
            createFormValues.Add("birthDay", "01/01/2001");
            createFormValues.Add("state", "CA");
            createFormValues.Add("country", "USA");
            createFormValues.Add("access_level", "0");

            RedirectToRouteResult result = controller.Index(createFormValues) as RedirectToRouteResult;

            string action = result.RouteValues["action"].ToString();
            Assert.AreEqual("Index", action);

            controller = new CreateAccountController();

            createFormValues = new FormCollection();

            createFormValues.Add("first_name", "admin");
            createFormValues.Add("last_name", "test");
            createFormValues.Add("email", "admintest@test.com");
            createFormValues.Add("password", "test");
            createFormValues.Add("birthDay", "01/01/2001");
            createFormValues.Add("state", "CA");
            createFormValues.Add("country", "USA");
            createFormValues.Add("access_level", "1");

            result = controller.Index(createFormValues) as RedirectToRouteResult;

            action = result.RouteValues["action"].ToString();
            Assert.AreEqual("Index", action);
        }
    }
}
