﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.225
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC3.Tests.SLAuth {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SLAuth.ISLAuth")]
    public interface ISLAuth {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISLAuth/Authenticate", ReplyAction="http://tempuri.org/ISLAuth/AuthenticateResponse")]
        MVC3.SLAuth.Logon Authenticate(string email, string password, ref string[] errors);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISLAuthChannel : MVC3.Tests.SLAuth.ISLAuth, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SLAuthClient : System.ServiceModel.ClientBase<MVC3.Tests.SLAuth.ISLAuth>, MVC3.Tests.SLAuth.ISLAuth {
        
        public SLAuthClient() {
        }
        
        public SLAuthClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SLAuthClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SLAuthClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SLAuthClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public MVC3.SLAuth.Logon Authenticate(string email, string password, ref string[] errors) {
            return base.Channel.Authenticate(email, password, ref errors);
        }
    }
}
