USE [master]
GO
/****** Object:  Database [cse136]    Script Date: 08/21/2012 16:27:59 ******/
CREATE DATABASE [cse136] ON  PRIMARY 
( NAME = N'cse136', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cse136_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\cse136_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cse136] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cse136].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cse136] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [cse136] SET ANSI_NULLS OFF
GO
ALTER DATABASE [cse136] SET ANSI_PADDING OFF
GO
ALTER DATABASE [cse136] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [cse136] SET ARITHABORT OFF
GO
ALTER DATABASE [cse136] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [cse136] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [cse136] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [cse136] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [cse136] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [cse136] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [cse136] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [cse136] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [cse136] SET  DISABLE_BROKER
GO
ALTER DATABASE [cse136] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [cse136] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [cse136] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [cse136] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [cse136] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [cse136] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [cse136] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [cse136] SET  READ_WRITE
GO
ALTER DATABASE [cse136] SET RECOVERY FULL
GO
ALTER DATABASE [cse136] SET  MULTI_USER
GO
ALTER DATABASE [cse136] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [cse136] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'cse136', N'ON'
GO
USE [cse136]
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/21/2012 16:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[birthDay] [date] NULL,
	[state] [varchar](2) NULL,
	[country] [varchar](50) NOT NULL,
	[access_level] [int] NULL,
	[password] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 08/21/2012 16:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Artist](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Artist] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 08/21/2012 16:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Album](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[year] [date] NULL,
 CONSTRAINT [PK_Album] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 08/21/2012 16:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Genre](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Genre] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[delete_genre]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_genre]
	@genreid INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE FROM [Genre] WHERE id = @genreid
END
GO
/****** Object:  StoredProcedure [dbo].[delete_artist]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_artist]
	@artistid INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE FROM [Artist] WHERE id = @artistid
END
GO
/****** Object:  StoredProcedure [dbo].[delete_album]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_album]
	@albumid INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE FROM [Album] WHERE id = @albumid
END
GO
/****** Object:  StoredProcedure [dbo].[delete_user]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[delete_user]
	-- Add the parameters for the stored procedure here
	@userid INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	DELETE FROM [User] WHERE id = @userid
	
END
GO
/****** Object:  Table [dbo].[Song]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Song](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[artist_key] [int] NOT NULL,
	[album_key] [int] NULL,
	[genre_key] [int] NOT NULL,
	[download_count] [int] NOT NULL,
	[user_key] [int] NOT NULL,
 CONSTRAINT [PK_Song] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[update_user]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_user]
	@userid int,
	@firstName varchar(50),
	@lastName varchar(50),
	@birth date,
	@state varchar(2),
	@country varchar(50),
	@access int,
	@password varchar(50),
	@email varchar(50)
AS
BEGIN
	UPDATE [User] 
	SET 
		first_name = @firstName, 
		last_name = @lastName, 
		birthDay = @birth, 
		state = @state, 
		country = @country, 
		access_level = @access, 
		password = @password, 
		email = @email
	WHERE id = @userid
END
GO
/****** Object:  StoredProcedure [dbo].[insert_genre]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[insert_genre]
	-- Add the parameters for the stored procedure here
	@genreName varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	insert into genre (name) values (@genreName)
	select @@IDENTITY
    -- Insert statements for procedure here
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[insert_artist]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[insert_artist]
	-- Add the parameters for the stored procedure here
	@artistName varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	insert into artist (name) values (@artistName)
	select @@IDENTITY
    -- Insert statements for procedure here
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[insert_album]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insert_album]
	-- Add the parameters for the stored procedure here
	@albumName varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	insert into Album (name) values (@albumName)
	select @@IDENTITY
    -- Insert statements for procedure here
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[get_user_list]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_user_list]
AS
BEGIN
	SELECT * FROM [User]
END
GO
/****** Object:  StoredProcedure [dbo].[get_user_data]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_user_data]
	@userid int
AS
BEGIN
	SELECT * FROM [User] WHERE id = @userid
END
GO
/****** Object:  StoredProcedure [dbo].[get_login_info]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_login_info]
	@email AS VARCHAR(50),
	@password AS VARCHAR(50),
	@userid AS INT OUTPUT,
	@access_level AS INT OUTPUT
AS
BEGIN
	
	SET @userid = -1
	SET @access_level = -1
	
	IF((SELECT id FROM [User] WHERE email = @email AND password = @password) IS NOT NULL)
	BEGIN
		SELECT
			@userid = id,
			@access_level = access_level
		FROM
			[User]
		WHERE
			email = @email AND
			password = @password
	END
			
		
END
GO
/****** Object:  StoredProcedure [dbo].[insert_user]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[insert_user]
	-- Add the parameters for the stored procedure here
	@firstName varchar(50),
	@lastName varchar(50),
	@birth date,
	@state varchar(2),
	@country varchar(50),
	@access int,
	@password varchar(50),
	@email varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	insert into [User] (first_name,last_name,birthDay,[state],country,access_level,[password],email) values (@firstName,
	@lastName ,
	@birth ,
	@state ,
	@country ,
	@access ,
	@password ,
	@email)
	select @@IDENTITY
    -- Insert statements for procedure here
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[insert_song]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[insert_song]
	-- Add the parameters for the stored procedure here
	@albumId int,
	@genreId int,
	@artistId int,
	@userId int,
	@songName varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	insert into song (name,artist_key,album_key,genre_key,download_count,user_key) values (@songName,@artistId,@albumId,@genreId,0,@userId)
	select @@IDENTITY
    -- Insert statements for procedure here
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[get_download_count]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_download_count]
	@songid int
AS
BEGIN
	SELECT download_count FROM [Song] WHERE id = @songid
END
GO
/****** Object:  StoredProcedure [dbo].[update_song]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_song]
	@songid int,
	@albumid int,
	@genreid int,
	@artistid int,
	@userid int,
	@songname varchar(50)
AS
BEGIN
	UPDATE [Song] 
	SET 
		name = @songname,
		artist_key = @artistid,
		genre_key = @genreid,
		album_key = @albumid,
		user_key = @userid
	WHERE id = @songid
END
GO
/****** Object:  StoredProcedure [dbo].[update_download_count]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[update_download_count]
	-- Add the parameters for the stored procedure here
	@songId int
AS
BEGIN
	update [song] set download_count = download_count + 1 where id = @songId
END
GO
/****** Object:  StoredProcedure [dbo].[select_by_user]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[select_by_user]
	-- Add the parameters for the stored procedure here
	@userID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select *
	from Song s
	where s.user_key = @userID
	
END
GO
/****** Object:  StoredProcedure [dbo].[select_by_song_name]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[select_by_song_name]
	-- Add the parameters for the stored procedure here
	@songName	varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select *
	from Song s
	where s.name = @songName
	
END
GO
/****** Object:  StoredProcedure [dbo].[select_by_genre]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[select_by_genre]
	-- Add the parameters for the stored procedure here
	@genreID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select *
	from Song s
	where s.genre_key = @genreID
	
END
GO
/****** Object:  StoredProcedure [dbo].[select_by_artist]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[select_by_artist]
	-- Add the parameters for the stored procedure here
	@artistID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select *
	from Song s
	where s.artist_key = @artistID
	
END
GO
/****** Object:  StoredProcedure [dbo].[select_by_album]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[select_by_album]
	-- Add the parameters for the stored procedure here
	@albumID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select *
	from Song s
	where s.album_key = @albumID
	
END
GO
/****** Object:  StoredProcedure [dbo].[delete_song]    Script Date: 08/21/2012 16:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delete_song]
	@songid int
AS
BEGIN
	-- here we declare our local variables
	-- ids store the ids of artists, albums, and genres
	-- counts store the number of songs that have the same artist, genre, album id
	DECLARE @albumid AS INT
	DECLARE @albumcount AS INT
	DECLARE @genreid AS INT
	DECLARE @genrecount AS INT
	DECLARE @artistid AS INT
	DECLARE @artistcount AS INT
	
	-- we want to select the id's of the artist, album, and genre for the 
	-- song we are going to delete
	SELECT @albumid = album_key,
           @genreid = genre_key,
           @artistid = artist_key
    FROM
			[Song]
	WHERE
		   id = @songid
	
	-- here, we count how many records have the same album, genre, artist id.
	-- we do this, because we'll want to delete the album, genre, artist from 
	-- our respective tables if this song is the only song that uses said id's.
	SELECT @albumcount = COUNT(*) FROM [Song] WHERE album_key = @albumid
	SELECT @genrecount = COUNT(*) FROM [Song] WHERE genre_key = @genreid
	SELECT @artistcount = COUNT(*) FROM [Song] WHERE artist_key = @artistid
	
	-- here we run our stored procedures to delete the respective artists
	-- genres and albums if there is only one song that has the same id 
	-- [because that means that we're deleting the only song with these id's]
	IF (@albumcount = 1)
	BEGIN
		EXEC [dbo].[delete_album] @albumid
	END
	IF (@genrecount = 1)
	BEGIN
		EXEC [dbo].[delete_genre] @genreid
	END
	IF (@artistcount = 1)
	BEGIN
		EXEC [dbo].[delete_artist] @artistid
	END
	
	-- finally, let's delete the song from our table.
	DELETE FROM [Song] WHERE id = @songid
END
GO
/****** Object:  ForeignKey [FK_Song_Album]    Script Date: 08/21/2012 16:28:00 ******/
ALTER TABLE [dbo].[Song]  WITH CHECK ADD  CONSTRAINT [FK_Song_Album] FOREIGN KEY([album_key])
REFERENCES [dbo].[Album] ([id])
GO
ALTER TABLE [dbo].[Song] CHECK CONSTRAINT [FK_Song_Album]
GO
/****** Object:  ForeignKey [FK_Song_Artist]    Script Date: 08/21/2012 16:28:00 ******/
ALTER TABLE [dbo].[Song]  WITH CHECK ADD  CONSTRAINT [FK_Song_Artist] FOREIGN KEY([artist_key])
REFERENCES [dbo].[Artist] ([id])
GO
ALTER TABLE [dbo].[Song] CHECK CONSTRAINT [FK_Song_Artist]
GO
/****** Object:  ForeignKey [FK_Song_Genre]    Script Date: 08/21/2012 16:28:00 ******/
ALTER TABLE [dbo].[Song]  WITH CHECK ADD  CONSTRAINT [FK_Song_Genre] FOREIGN KEY([genre_key])
REFERENCES [dbo].[Genre] ([id])
GO
ALTER TABLE [dbo].[Song] CHECK CONSTRAINT [FK_Song_Genre]
GO
/****** Object:  ForeignKey [FK_Song_User]    Script Date: 08/21/2012 16:28:00 ******/
ALTER TABLE [dbo].[Song]  WITH CHECK ADD  CONSTRAINT [FK_Song_User] FOREIGN KEY([user_key])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Song] CHECK CONSTRAINT [FK_Song_User]
GO
